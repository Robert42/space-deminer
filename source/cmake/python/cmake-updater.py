#!/usr/bin/env python
#
# Usage:
# ./cmake-updater.py --dest-file ./cmake/space-deminer-source-files.cmake
#                    --search-dirs ./include ./src
#                    --extensions .cpp .h .inl
#                    --exclude ./src/framework/window/implementation
#
#

import os
import sys

linux_sourcefiles = []
macintosh_sourcefiles = []
windows_sourcefiles = []

general_sourcegroups = []
linux_sourcegroups = []
macintosh_sourcegroups = []
windows_sourcegroups = []
  
def string_begins_with(haystack, needle):
  return haystack.find(needle) == 0
  
def string_remove_beginning(s, beginning_to_remove):
  beginning_length = len(beginning_to_remove)
  if string_begins_with(s, beginning_to_remove):
    return s[beginning_length:]
  return s

class SourceGroup:
  def __init__(self, directory):
    directory = string_remove_beginning(directory, "./")
    self.directory =  directory.replace("\\", "\\\\").replace("/", "\\\\")
    self.files = []
  def __lt__(self, other):
    return self.directory < other.directory
  def append_file(self, file):
    self.files.append(file)
  def shouldbewritten(self):
    return len(self.directory)>0 and len(self.files)>0
  def write(self, f):
    if self.shouldbewritten():
      f.write("source_group(" + self.directory + "\n")
      f.write("FILES\n")
      self.files = sorted(self.files)
      for source_file in self.files:
        f.write("  "+prepare_source_file_path(source_file)+"\n")
      f.write(")\n")
    

def is_supported_file(filename, supported_extensions):
  filename_base, filename_extension = os.path.splitext(filename)
      
  for supported_extension in supported_extensions:
    if filename_extension == supported_extension:
      return 1
  return 0
    

def has_extension(filename, extension):
  filename_base, filename_extension = os.path.splitext(filename)
  return filename_extension == extension;
  
def string_contains(haystack, needle):
  return haystack.find(needle) != -1
  
def is_supported_directory(directory, excluded_directories):
  for excluded_directory in excluded_directories:
    if string_contains(directory, excluded_directory):
      return 0
  return 1

def search_fo_source_files_in_one_directory(root, supported_extensions, excluded_directories):
  global general_sourcegroups
  global linux_sourcegroups
  global macintosh_sourcegroups
  global windows_sourcegroups
  all_source_files = []
  
  for root,directories,files in os.walk(root):
    root = root.replace("\\", "/")
    general_source_group = SourceGroup(root)
    general_sourcegroups.append(general_source_group)
    linux_source_group = SourceGroup(root)
    linux_sourcegroups.append(linux_source_group)
    macintosh_source_group = SourceGroup(root)
    macintosh_sourcegroups.append(macintosh_source_group)
    windows_source_group = SourceGroup(root)
    windows_sourcegroups.append(windows_source_group)
  
    for filename in files:
      filename = filename.replace("\\", "/")
      use_this_file = is_supported_file(filename, supported_extensions) and is_supported_directory(root, excluded_directories)
      joined_path = os.path.join(root, filename)
      joined_path = joined_path.replace("\\", "/")
      
      global linux_sourcefiles
      global macintosh_sourcefiles
      global windows_sourcefiles
      
      if string_contains(joined_path, 'linux') and use_this_file:
        linux_sourcefiles.append(joined_path)
        linux_source_group.append_file(joined_path)
        use_this_file = 0
      
      if (string_contains(joined_path, 'macintosh') or has_extension(filename, ".mm")) and use_this_file:
        macintosh_sourcefiles.append(joined_path)
        macintosh_source_group.append_file(joined_path)
        use_this_file = 0
      
      if string_contains(joined_path, 'windows') and use_this_file:
        windows_sourcefiles.append(joined_path)
        windows_source_group.append_file(joined_path)
        use_this_file = 0

      if string_contains(joined_path, 'unix') and use_this_file:
        linux_sourcefiles.append(joined_path)
        linux_source_group.append_file(joined_path)
        macintosh_sourcefiles.append(joined_path)
        macintosh_source_group.append_file(joined_path)
        use_this_file = 0
    
      if use_this_file:
        all_source_files.append(joined_path)
        general_source_group.append_file(joined_path)
      
  return all_source_files
  
  
def search_fo_source_files(root_directories, supported_extensions, excluded_directories):
  all_source_files = []
  
  for root in root_directories:
    all_source_files.extend(search_fo_source_files_in_one_directory(root, supported_extensions, excluded_directories))
      
  return all_source_files
  
  
def prepare_source_file_path(source_file):
  global source_root
  return os.path.normpath(os.path.join(source_root, source_file))
  
def write_source_files(f, source_files):
  global target_var_name

  source_files = sorted(source_files)
  
  f.write("set("+target_var_name+" ${"+target_var_name+"}\n")
  for source_file in source_files:
    f.write("  " + prepare_source_file_path(source_file) + "\n")
  
  f.write(")\n")
  
  
def write_cmake_os_decide_if_statement(f, os):
  f.write("if(CMAKE_SYSTEM_NAME STREQUAL \"" + os + "\")\n")
  
def write_os_dependent_source_files(f, source_files, os):
  if len(source_files) > 0:
    write_cmake_os_decide_if_statement(f, os)
    write_source_files(f, source_files)
    f.write("endif()\n\n")
  

dest_file = ""
source_group_dest_file = ""
target_var_name = "SPACEDEMINER_SOURCE_FILES"
source_root = "${CMAKE_CURRENT_SOURCE_DIR}"
search_dirs = []
supported_extensions = []
excluded_directories = []
appended_cmake_lines = []
cmake_lines = []
mode = "--python-script"
for arg in sys.argv:
  if arg == "--dest-file":
    mode = arg
    continue
  if arg == "--source-groups-dest-file":
    mode = arg
    continue
  elif arg == "--search-dirs":
    mode = arg
    continue
  elif arg == "--extensions":
    mode = arg
    continue
  elif arg == "--exclude":
    mode = arg
    continue
  elif arg == "--append-cmake":
    mode = arg
    continue
  elif arg == "--target-varname":
    mode = arg
    continue
  elif arg == "--source-root":
    mode = arg
    continue
  
  if mode == "--dest-file":
    dest_file = arg
  elif mode == "--source-groups-dest-file":
    source_group_dest_file = arg
  elif mode == "--search-dirs":
    search_dirs.append(arg)
  elif mode == "--extensions":
    supported_extensions.append(arg)
  elif mode == "--exclude":
    excluded_directories.append(arg)
  elif mode == "--append-cmake":
    cmake_lines.append(arg)
  elif mode == "--target-varname":
    target_var_name = arg
  elif mode == "--source-root":
    source_root = arg

with open(dest_file, "w") as f:
  
  source_files = search_fo_source_files(search_dirs, supported_extensions, excluded_directories)
      
  write_source_files(f, source_files)
  
  f.write("\n")
  
  write_os_dependent_source_files(f, macintosh_sourcefiles, 'Darwin')
  write_os_dependent_source_files(f, linux_sourcefiles, 'Linux')
  write_os_dependent_source_files(f, windows_sourcefiles, 'Windows')
  
  for cmake_line in cmake_lines:
    f.write(cmake_line + "\n")
    
    
def write_source_groups(f, sourcegroups):
  sourcegroups = sorted(sourcegroups)
  for group in sourcegroups:
    group.write(f)

def write_os_dependent_sourcegroups(f, sourcegroups, os):
  shouldbewritten = 0
  
  for g in sourcegroups:
    if g.shouldbewritten():
      shouldbewritten = 1
      break

  if shouldbewritten:
    write_cmake_os_decide_if_statement(f, os)
    write_source_groups(f, sourcegroups)
    f.write("endif()\n\n")

with open(source_group_dest_file, "w") as f:
  write_source_groups(f, general_sourcegroups)
  
  write_os_dependent_sourcegroups(f, macintosh_sourcegroups, 'Darwin')
  write_os_dependent_sourcegroups(f, linux_sourcegroups, 'Linux')
  write_os_dependent_sourcegroups(f, windows_sourcegroups, 'Windows')
