#!/usr/bin/env python

resultMode = "RELEASE";

try:

  import sys
  from PyQt4.QtGui import *
  from PyQt4.QtCore import *

  class ModeChooser:
    def __init__(self):
      self.dialog = QDialog()
      self.dialog.setWindowTitle("Choose Build-Type")
      
      layout = QVBoxLayout(self.dialog)
      layout.setSpacing(8)
      layout.setMargin(16)
      self.dialog.setLayout(layout)
      
      label = QLabel("Choose the build mode:")
      layout.addWidget(label)

      btnDebug = QPushButton("Debug")
      QObject.connect(btnDebug, SIGNAL("clicked()"), lambda  : self.chooseMode("DEBUG"))
      layout.addWidget(btnDebug)

      btnRelease = QPushButton("Release")
      QObject.connect(btnRelease, SIGNAL("clicked()"), lambda  : self.chooseMode("RELEASE"))
      layout.addWidget(btnRelease)

      btnMinSizeRel = QPushButton("MinSizeRel")
      QObject.connect(btnMinSizeRel, SIGNAL("clicked()"), lambda  : self.chooseMode("MINSIZEREL"))
      layout.addWidget(btnMinSizeRel)

      btnRelWithDbg = QPushButton("ReleaseWithDebugInfo")
      QObject.connect(btnRelWithDbg, SIGNAL("clicked()"), lambda  : self.chooseMode("RELWITHDEBINFO"))
      layout.addWidget(btnRelWithDbg)
      
      label2 = QLabel(text="(If you don't know,\nwhich one to choose,\nchoose \"Release\")")
      layout.addWidget(label2)
      
    def run(self):
      self.dialog.exec_()
      
    def chooseMode(self, mode):
      global resultMode
    
      resultMode = mode
      self.dialog.accept()

  qapp = QApplication(sys.argv)
      
  modeChooser = ModeChooser()
  modeChooser.run()

except ImportError:
  # The following workaround is taken from stackoverflow
  # http://stackoverflow.com/a/11621141/2301866
  try:
    # for Python2
    from Tkinter import *
  except ImportError:
    # for Python3
    from tkinter import *

  class ModeChooser:
    def __init__(self, root):
      self.frame = Frame(root)
      self.frame.pack()
      
      label = Label(self.frame, text="Choose the build mode:")
      label.pack(side=TOP)
      
      label2 = Label(self.frame, text="(If you don't know,\nwhich one to choose,\nchoose \"RELEASE\")")
      label2.pack(side=BOTTOM)

      btnDebug = Button(self.frame, text="Debug", command = lambda  : self.chooseMode("DEBUG"))
      btnDebug.pack(side=TOP)

      btnRelease = Button(self.frame, text="Release", command = lambda  : self.chooseMode("RELEASE"))
      btnRelease.pack(side=TOP)

      btnRelease = Button(self.frame, text="MinSizeRel", command = lambda  : self.chooseMode("MINSIZEREL"))
      btnRelease.pack(side=TOP)

      btnRelease = Button(self.frame, text="ReleaseWithDebugInfo", command = lambda  : self.chooseMode("RELWITHDEBINFO"))
      btnRelease.pack(side=TOP)
      
    def chooseMode(self, mode):
      global resultMode
    
      resultMode = mode
      self.frame.quit()

  root = Tk()
      
  modeChooser = ModeChooser(root)

  root.mainloop()

print(resultMode)
