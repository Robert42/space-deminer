source_group(framework\\window\\implementation\\sdl
FILES
  ${CMAKE_CURRENT_SOURCE_DIR}/framework/window/implementation/sdl/cursor.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/framework/window/implementation/sdl/cursor.h
  ${CMAKE_CURRENT_SOURCE_DIR}/framework/window/implementation/sdl/keyboard-input-device-impl.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/framework/window/implementation/sdl/keyboard-input-device-impl.h
  ${CMAKE_CURRENT_SOURCE_DIR}/framework/window/implementation/sdl/keyboard-input-device-impl.test.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/framework/window/implementation/sdl/mouse-input-device-impl.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/framework/window/implementation/sdl/mouse-input-device-impl.h
  ${CMAKE_CURRENT_SOURCE_DIR}/framework/window/implementation/sdl/no-sdl-gl-context.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/framework/window/implementation/sdl/render-window-impl.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/framework/window/implementation/sdl/render-window-impl.h
  ${CMAKE_CURRENT_SOURCE_DIR}/framework/window/implementation/sdl/sdl-error.h
  ${CMAKE_CURRENT_SOURCE_DIR}/framework/window/implementation/sdl/sdl-gl-context.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/framework/window/implementation/sdl/user-input-impl.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/framework/window/implementation/sdl/user-input-impl.h
)
if(CMAKE_SYSTEM_NAME STREQUAL "Darwin")
source_group(framework\\window\\implementation\\sdl
FILES
  ${CMAKE_CURRENT_SOURCE_DIR}/framework/window/implementation/sdl/get-macintosh-window-handle.mm
)
endif()

if(CMAKE_SYSTEM_NAME STREQUAL "Linux")
source_group(framework\\window\\implementation\\sdl
FILES
  ${CMAKE_CURRENT_SOURCE_DIR}/framework/window/implementation/sdl/get-linux-window-handle.cpp
)
endif()

