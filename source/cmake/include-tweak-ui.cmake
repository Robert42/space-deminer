set(TWEAKUI_ROOT ${CMAKE_CURRENT_SOURCE_DIR}/external/TweakUI)

add_subdirectory(${TWEAKUI_ROOT})
include_directories(SYSTEM ${TWEAKUI_ROOT}/include)
