include(cmake/source-groups/dependencies.cmake)
include(cmake/source-groups/base.cmake)
include(cmake/source-groups/framework.cmake)
include(cmake/source-groups/space-deminer.cmake)

include(cmake/source-groups/sdl-window-implementation.cmake)
