if(CMAKE_CXX_COMPILER_ID STREQUAL Clang)

# Activate all warnings and treat them as errors
set(WARNING_FLAGS "-Wall -Wextra")

#enable stack protector
set(PCH_RELEVANT_FLAGS "${PCH_RELEVANT_FLAGS} -fstack-protector-all")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 ${WARNING_FLAGS}")

if(APPLE)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++")
  if(CMAKE_EXE_LINKER_FLAGS STREQUAL " ")
    set(CMAKE_EXE_LINKER_FLAGS "-stdlib=libc++")
  else()
    list(APPEND CMAKE_EXE_LINKER_FLAGS "-stdlib=libc++")
  endif()
endif()

set(PRECOMPILER_HEADER "dependencies.h")

set(KNOWN_COMPILER 1)
endif()
