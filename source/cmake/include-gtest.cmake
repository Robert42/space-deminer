set(gtest_force_shared_crt 1)
set(GTEST_ROOT ${CMAKE_CURRENT_SOURCE_DIR}/external/gtest-1.6.0)

# Include GTest (see http://stackoverflow.com/a/8522991/2301866)
add_subdirectory(${GTEST_ROOT})
add_definitions("-DGTEST_USE_OWN_TR1_TUPLE=1")
include_directories(${GTEST_ROOT}/include)

# If updating to a new gtest version, please take first a look at the following commits
# cc123056f80fe954febb1c3d61a081cf86d12a95
# 98c88d3efbbdfe25fd5e892378350901d160907d
# 6bdc0da1bf207ddc69f85662967f27f7d37a2704
