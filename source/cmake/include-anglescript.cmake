set(ANGLESCRIPT_ROOT ${CMAKE_CURRENT_SOURCE_DIR}/external/angelscript)

add_definitions("-DAS_USE_NAMESPACE")

include_directories(SYSTEM ${ANGLESCRIPT_ROOT}/sdk/angelscript/include)
include_directories(SYSTEM ${ANGLESCRIPT_ROOT}/sdk/add_on)
add_subdirectory(${ANGLESCRIPT_ROOT})
