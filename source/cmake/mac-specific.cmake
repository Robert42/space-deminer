if(CMAKE_SYSTEM_NAME STREQUAL "Darwin")

set(CMAKE_OSX_ARCHITECTURES x86_64 CACHE STRING "The supported OS X Architecture" FORCE)

macro(add_symlink_from_mac_app_to_ogre_plugin dest_dir plugin_name)
  set(FRAMEWORK_ROOT $ENV{HOME}/Library/Frameworks/${plugin_name}.framework)

  if(NOT EXISTS ${FRAMEWORK_ROOT})
    set(FRAMEWORK_ROOT /Library/Frameworks/${plugin_name}.framework)
  endif()

  set(plugin_path ${FRAMEWORK_ROOT}/${plugin_name})

  create_symlink(${plugin_path} ${dest_dir} ${plugin_name}.dylib)
endmacro()

macro(prepare_mac_app base_path)
  set(resources ${base_path}/space-deminer.app/Contents/Resources)

  file(MAKE_DIRECTORY ${resources})
  file(MAKE_DIRECTORY ${resources}/lib)
  
  create_symlink(../../../../../assets ${resources} assets)

  add_symlink_from_mac_app_to_ogre_plugin(${resources}/lib RenderSystem_GL)
  add_symlink_from_mac_app_to_ogre_plugin(${resources}/lib Plugin_ParticleFX)
  add_symlink_from_mac_app_to_ogre_plugin(${resources}/lib Plugin_PCZSceneManager)
  add_symlink_from_mac_app_to_ogre_plugin(${resources}/lib Plugin_OctreeZone)
  add_symlink_from_mac_app_to_ogre_plugin(${resources}/lib Plugin_OctreeSceneManager)
endmacro()

set(SPACEDEMINER_SOURCE_FILES MACOSX_BUNDLE ${SPACEDEMINER_SOURCE_FILES})

set(MACOSX_BUNDLE_BUNDLE_NAME "Space-Deminer")

prepare_mac_app(${CMAKE_CURRENT_SOURCE_DIR}/../bin/Debug)
prepare_mac_app(${CMAKE_CURRENT_SOURCE_DIR}/../bin/Release)
prepare_mac_app(${CMAKE_CURRENT_SOURCE_DIR}/../bin/MinSizeRel)
prepare_mac_app(${CMAKE_CURRENT_SOURCE_DIR}/../bin/RelWithDebInfo)

file(REMOVE ${CMAKE_CURRENT_SOURCE_DIR}/../assets/assets)


endif()
