if(NOT CMAKE_SYSTEM_NAME STREQUAL "Windows")

macro(create_symlink target_path link_directory link_name)
  if((NOT EXISTS ${link_directory}/${target_path}) AND (NOT EXISTS ${target_path}))
    message(FATAL_ERROR "Couldn't create symlink: Destination ${target_path} doesn't exist")
  endif()

  execute_process(COMMAND ln -sf ${target_path} ${link_directory}/${link_name})
endmacro()

macro(filename_from_path VARNAME FILENAME)
  string(REGEX REPLACE "^.*/([^/]*)$"  "\\1" ${VARNAME} ${FILENAME})
endmacro()

endif()
