include(cmake/source-files/dependencies.cmake)
include(cmake/source-files/base.cmake)
include(cmake/source-files/framework.cmake)
include(cmake/source-files/space-deminer.cmake)

include(cmake/window-implementation.cmake)

set(SPACEDEMINER_SOURCE_FILES
  ${SPACEDEMINER_DEPENDENCY_SOURCE_FILES}
  ${SPACEDEMINER_BASE_SOURCE_FILES}
  ${SPACEDEMINER_FRAMEWORK_SOURCE_FILES}
  ${SPACEDEMINER_FRAMEWORK_WINDOW_SOURCE_FILES}
  ${SPACEDEMINER_SOURCE_FILES}
)
