if(CMAKE_CXX_COMPILER_ID STREQUAL GNU)

# Activate all warnings and treat them as errors
set(WARNING_FLAGS "-Wall -Wextra -Wno-sign-compare")

#enable stack protector
set(PCH_RELEVANT_FLAGS "${PCH_RELEVANT_FLAGS} -std=c++11 -fstack-protector-all ${Qt5Widgets_EXECUTABLE_COMPILE_FLAGS} -frtti")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${WARNING_FLAGS}")

set(PRECOMPILER_HEADER "dependencies.h")

set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -flto -fwhole-program -DQT_NO_DEBUG -fno-use-linker-plugin")
set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO} -flto -fwhole-program -DQT_NO_DEBUG -fno-use-linker-plugin")
set(CMAKE_CXX_FLAGS_MINSIZEREL "${CMAKE_CXX_FLAGS_MINSIZEREL} -flto -fwhole-program -DQT_NO_DEBUG -fno-use-linker-plugin")
set(CMAKE_EXE_LINKER_FLAGS_RELEASE "${CMAKE_EXE_LINKER_FLAGS_RELEASE} -flto -fwhole-program -fno-use-linker-plugin")
set(CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO "${CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO} -flto -fwhole-program -fno-use-linker-plugin")
set(CMAKE_EXE_LINKER_FLAGS_MINSIZEREL "${CMAKE_EXE_LINKER_FLAGS_MINSIZEREL} -flto -fwhole-program -fno-use-linker-plugin")

set(KNOWN_COMPILER 1)
endif()

