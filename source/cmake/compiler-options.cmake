option(SPACEDEMINER_MULTITHREADING "Flag, whether Space-Deminer should use multithreading" ON)

if(SPACEDEMINER_MULTITHREADING)
set(SPACEDEMINER_MULTITHREADING_VALUE 1)
else()
set(SPACEDEMINER_MULTITHREADING_VALUE 0)
endif()

set(PCH_RELEVANT_FLAGS "${PCH_RELEVANT_FLAGS} -DSPACEDEMINER_MULTITHREADING=${SPACEDEMINER_MULTITHREADING_VALUE}")

if(CMAKE_GENERATOR STREQUAL "Visual Studio 6"
   OR CMAKE_GENERATOR STREQUAL "Visual Studio 7"
   OR CMAKE_GENERATOR STREQUAL "Visual Studio 7 .NET 2003"
   OR CMAKE_GENERATOR STREQUAL "Visual Studio 8 2005"
   OR CMAKE_GENERATOR STREQUAL "Visual Studio 9 2008"
   OR CMAKE_GENERATOR STREQUAL "Visual Studio 10")
  message(FATAL_ERROR "${CMAKE_GENERATOR} is not supported, please use at least Visual Studio 11.")
endif()


if($ENV{SPACEDEMINER_DEVELOPER})
  add_definitions(-DSPACEDEMINER_DEVELOPER)
  execute_process(COMMAND python ${PYTHON_DIR}/test-python.py
                  OUTPUT_VARIABLE TEST_PYTHON_RESULT
                  OUTPUT_STRIP_TRAILING_WHITESPACE)
  if(NOT TEST_PYTHON_RESULT STREQUAL "PYTHON_IS_RUNNING")
    message(FATAL_ERROR "Not able to run python scripts. please make sure python is correctly installed and in you PATH variable.")
  endif()
endif()

# Recognize Debug/Release mode
# if there's any debug information a variable DEBINFO gets set with value 1
# otherwise, there's no variable DEBINFO set
if(CMAKE_BUILD_TYPE STREQUAL "DEBUG")
  message(STATUS "Recognized Build type 'DEBUG'")
elseif(CMAKE_BUILD_TYPE STREQUAL "RELEASE")
  message(STATUS "Recognized Build type 'RELEASE'")
elseif(CMAKE_BUILD_TYPE STREQUAL "MINSIZEREL")
  message(STATUS "Recognized Build type 'MINSIZEREL'")
elseif(CMAKE_BUILD_TYPE STREQUAL "RELWITHDEBINFO")
  message(STATUS "Recognized Build type 'RELWITHDEBINFO'")
else()
  set(CMAKE_BUILD_TYPE RELEASE)
  
  if(($ENV{SPACEDEMINER_DEVELOPER})
     AND (NOT CMAKE_GENERATOR STREQUAL "Visual Studio 11")
     AND (NOT CMAKE_GENERATOR STREQUAL "Xcode"))
    execute_process(COMMAND python ${PYTHON_DIR}/choose-build-mode.py
                    OUTPUT_VARIABLE CMAKE_BUILD_TYPE
                    OUTPUT_STRIP_TRAILING_WHITESPACE)
    set(CMAKE_BUILD_TYPE ${CMAKE_BUILD_TYPE} CACHE STRING "Build Type" FORCE)
  endif()
                  
  message(STATUS "Using Build type '${CMAKE_BUILD_TYPE}'")
endif(CMAKE_BUILD_TYPE STREQUAL "DEBUG")


# collect the source files
if($ENV{SPACEDEMINER_DEVELOPER})

  execute_process(COMMAND python ${PYTHON_DIR}/cmake-updater.py
                      --dest-file ./cmake/source-files/dependencies.cmake
                      --source-groups-dest-file ./cmake/source-groups/dependencies.cmake
                      --search-dirs .
                      --extensions .cpp .h .inl
                      --exclude ./base ./cmake ./external ./framework ./space-deminer
                      --target-varname SPACEDEMINER_DEPENDENCY_SOURCE_FILES
                  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                  OUTPUT_QUIET)

  execute_process(COMMAND python ${PYTHON_DIR}/cmake-updater.py
                      --dest-file ./cmake/source-files/base.cmake
                      --source-groups-dest-file ./cmake/source-groups/base.cmake
                      --search-dirs ./base
                      --extensions .cpp .h .inl
                      --target-varname SPACEDEMINER_BASE_SOURCE_FILES
                  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                  OUTPUT_QUIET)

  execute_process(COMMAND python ${PYTHON_DIR}/cmake-updater.py
                      --dest-file ./cmake/source-files/framework.cmake
                      --source-groups-dest-file ./cmake/source-groups/framework.cmake
                      --search-dirs ./framework
                      --extensions .cpp .h .inl
                      --exclude ./framework/window/implementation
                      --target-varname SPACEDEMINER_FRAMEWORK_SOURCE_FILES
                  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                  OUTPUT_QUIET)

  execute_process(COMMAND python ${PYTHON_DIR}/cmake-updater.py
                      --dest-file ./cmake/source-files/sdl-window-implementation.cmake
                      --source-groups-dest-file ./cmake/source-groups/sdl-window-implementation.cmake
                      --search-dirs ./framework/window/implementation/sdl
                      --extensions .cpp .h .inl .mm
                      --append-cmake "find_package(SDL2 REQUIRED)"
                      --target-varname SPACEDEMINER_FRAMEWORK_SDLWINDOW_SOURCE_FILES
                  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                  OUTPUT_QUIET)

  execute_process(COMMAND python ${PYTHON_DIR}/cmake-updater.py
                      --dest-file ./cmake/source-files/space-deminer.cmake
                      --source-groups-dest-file ./cmake/source-groups/space-deminer.cmake
                      --search-dirs ./space-deminer
                      --extensions .cpp .h .inl
                      --target-varname SPACEDEMINER_SOURCE_FILES
                  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                  OUTPUT_QUIET)

  add_definitions(-DRUNTIME_ASSERTION_DEVELOPER_WARNING)
endif()


set(KNOWN_COMPILER 0)

include(cmake/compiler-options.gnu.cmake)
include(cmake/compiler-options.clang.cmake)
include(cmake/compiler-options.msvc.cmake)

if(NOT 1 EQUAL ${KNOWN_COMPILER})
message(FATAL_ERROR "Unknown CMAKE_CXX_COMPILER_ID \"${CMAKE_CXX_COMPILER_ID}\"")
endif()

# Check, whether the path contains spaces
unset(SPACE_POSITION)
string(FIND ${CMAKE_CURRENT_SOURCE_DIR} " " SPACE_POSITION)
if(NOT SPACE_POSITION EQUAL -1)
message(WARNING "The current path contains whitespace ('\\x20')")
endif()
unset(SPACE_POSITION)
string(FIND ${CMAKE_CURRENT_SOURCE_DIR} "\t" SPACE_POSITION)
if(NOT SPACE_POSITION EQUAL -1)
message(WARNING "The current path contains whitespace ('\\t')")
endif()


set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${PCH_RELEVANT_FLAGS}")

