#!/bin/bash

rm -rf sdk/add_on/autowrapper/generator
rm -rf sdk/angelscript/lib
rm -rf sdk/angelscript/projects
rm -rf sdk/docs
rm -rf sdk/samples
rm -rf sdk/tests

rm -f sdk/angelscript/source/*.asm
rm -f sdk/angelscript/source/*.S

if [ ! -d ./sdk/add_on/angelscript-add-on ] ; then
  mv sdk/add_on sdk/angelscript-add-on
  mkdir sdk/add_on
  mv sdk/angelscript-add-on sdk/add_on/angelscript-add-on
fi

echo "set(ANGLESCRIPT_SOURCE_FILES" > source-files.txt
find . | grep -iE '\.(cpp|h)$' | sort >> source-files.txt
echo ")" >> source-files.txt

for f in $(find . | grep -iE '\.(cpp|h)$')
do
  dos2unix $f
  cat $f | sed -E 's/(\/\/.*)TODO/\1todo/g' | sed -E 's/(\/\/.*)NOTE/\1note/g' > $f.changed-by-robert
  mv -f $f.changed-by-robert $f
done

