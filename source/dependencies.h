#ifndef _DEPENDENCIES_H_
#define _DEPENDENCIES_H_

#ifdef __clang__
#pragma clang system_header
#endif

#define BOOST_BIND_PLACEHOLDERS_HPP_INCLUDED

#if defined(__GNUC__) && defined(__GNUC_MINOR__)
#if __GNUC__ >= 4 && __GNUC_MINOR__ >= 8

#define CXX_TYPEID_NAME_SUPPORTED
#include <cxxabi.h>

#endif
#endif


#ifndef SPACEDEMINER_MULTITHREADING
#define SPACEDEMINER_MULTITHREADING 1
#endif


// ==== Ogre3D ====
#define OGRE_DOUBLE_PRECISION 0
#include <Ogre.h>
#include <OgreErrorDialog.h>
#include <OgreScriptTranslator.h>


// ==== GLM ====
#define GLM_FORCE_RADIANS
#define GLM_FORCE_CXX11
#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/random.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/epsilon.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/random.hpp>
#include <glm/gtx/io.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/perpendicular.hpp>


// ==== Google-TestFramework ====
#include <gtest/gtest.h>


// ==== Qt ====
#include <QString>
#include <QVector>
#include <QList>
#include <QSet>
#include <QRegularExpression>
#include <QFile>
#include <QTextStream>
#include <QBuffer>
#include <QProcess>
#include <QTextCodec>
#include <QtGui/QPainter>
#include <QtSvg/QSvgGenerator>


// ==== Gorilla ====
#define GORILLA_ASSERT(condition, message) assert(condition && message)
#include <Gorilla/Gorilla.h>


// ==== Angelscript ====
#include <angelscript.h>
#include <angelscript-add-on/scripthelper/scripthelper.h>
#include <angelscript-add-on/scriptstdstring/scriptstdstring.h>
#include <angelscript-add-on/scriptbuilder/scriptbuilder.h>


// ==== Boost ====
#include <boost/version.hpp>
#include <boost/filesystem.hpp>
#include <boost/exception/all.hpp>
#include <boost/algorithm/string/replace.hpp>


// ==== Standart C++ Library ====
#include <cstdint>
#include <functional>
#include <algorithm>
#include <list>
#include <forward_list>
#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include <string>
#include <cassert>
#include <limits>
#include <algorithm>
#include <memory>
#include <chrono>
#include <thread>
#include <mutex>
#include <type_traits>
#include <cstdlib>


// ==== SFML ====

#include <SFML/System/Utf.hpp>


// ==== TweakUI ====
#include <TweakUI.h>


// ==== CEGUI ====
#include <CEGUI/String.h>


// ==== Ogre3D ====
namespace Base {


static_assert(sizeof(Ogre::Real)==4, "Ogre::Real is not a 4 byte float as expected");
typedef Ogre::Real real;

inline Ogre::Vector2 min(const Ogre::Vector2& a, const Ogre::Vector2& b)
{
  return Ogre::Vector2(std::min<Ogre::Real>(a.x, b.x),
                       std::min<Ogre::Real>(a.y, b.y));
}

inline Ogre::Vector2 max(const Ogre::Vector2& a, const Ogre::Vector2& b)
{
  return Ogre::Vector2(std::max<Ogre::Real>(a.x, b.x),
                       std::max<Ogre::Real>(a.y, b.y));
}

extern const Ogre::String UnitTest_ResourceGroup;


}

namespace Ogre {

inline Ogre::Vector2 Vector2_cast(const glm::vec2& v)
{
  return Vector2(v.x, v.y);
}

inline Ogre::Vector3 Vector3_cast(const glm::vec3& v)
{
  return Vector3(v.x, v.y, v.z);
}

inline Ogre::Vector4 Vector4_cast(const glm::vec4& v)
{
  return Vector4(v.x, v.y, v.z, v.w);
}

inline Ogre::ColourValue ColourValue_cast(const glm::vec3& c)
{
  return ColourValue(c.r, c.g, c.b, 1.f);
}

inline Ogre::ColourValue ColourValue_cast(const glm::vec4& c)
{
  return ColourValue(c.r, c.g, c.b, c.a);
}

inline Ogre::Quaternion Quaternion_cast(const glm::quat& q)
{
  return Quaternion(q.w, q.x, q.y, q.z);
}


}



// ==== GLM ====
static_assert(GLM_VERSION >= 95, "glm version below 0.9.5 are not supported");
static_assert(sizeof(glm::vec3::value_type) == 4, "glm::vec3::value_type is not a float as expected");

namespace glm {
namespace detail {

#define ADD_PRINTTO_FOR_GLM_TYPE(type) \
  void PrintTo(const type& vector, std::ostream* o);

#define ADD_MINMAX_FOR_GLM_TYPE(type) \
inline type min(const type &a, const type &b) \
{ \
  return ::glm::min(a, b); \
} \
inline type max(const type &a, const type &b) \
{ \
  return ::glm::max(a, b); \
} \
inline type clamp(const type &a, const type &b, const type &c) \
{ \
  return ::glm::clamp(a, b, c); \
}

#define ADD_ABS_FOR_GLM_TYPE(type) \
inline type abs(const type &a) \
{ \
  return ::glm::abs(a); \
}

#define ADD_HELPER_FOR_GLM_MATRIX_TYPE(type) \
    ADD_PRINTTO_FOR_GLM_TYPE(type)

#define ADD_HELPER_FOR_GLM_VECTOR_TYPE(type) \
    ADD_PRINTTO_FOR_GLM_TYPE(type) \
    ADD_MINMAX_FOR_GLM_TYPE(type) \
    ADD_ABS_FOR_GLM_TYPE(type)

#define ADD_HELPER_FOR_GLM_IVECTOR_TYPE(type) \
    ADD_PRINTTO_FOR_GLM_TYPE(type) \
    ADD_MINMAX_FOR_GLM_TYPE(type) \
    ADD_ABS_FOR_GLM_TYPE(type)

ADD_HELPER_FOR_GLM_VECTOR_TYPE(vec2)
ADD_HELPER_FOR_GLM_VECTOR_TYPE(vec3)
ADD_HELPER_FOR_GLM_VECTOR_TYPE(vec4)
ADD_HELPER_FOR_GLM_IVECTOR_TYPE(ivec2)
ADD_HELPER_FOR_GLM_IVECTOR_TYPE(ivec3)
ADD_HELPER_FOR_GLM_IVECTOR_TYPE(ivec4)
ADD_HELPER_FOR_GLM_IVECTOR_TYPE(uvec2)
ADD_HELPER_FOR_GLM_IVECTOR_TYPE(uvec3)
ADD_HELPER_FOR_GLM_IVECTOR_TYPE(uvec4)

ADD_HELPER_FOR_GLM_MATRIX_TYPE(mat2x2)
ADD_HELPER_FOR_GLM_MATRIX_TYPE(mat2x3)
ADD_HELPER_FOR_GLM_MATRIX_TYPE(mat2x4)
ADD_HELPER_FOR_GLM_MATRIX_TYPE(mat3x2)
ADD_HELPER_FOR_GLM_MATRIX_TYPE(mat3x3)
ADD_HELPER_FOR_GLM_MATRIX_TYPE(mat3x4)
ADD_HELPER_FOR_GLM_MATRIX_TYPE(mat4x2)
ADD_HELPER_FOR_GLM_MATRIX_TYPE(mat4x3)
ADD_HELPER_FOR_GLM_MATRIX_TYPE(mat4x4)

#undef ADD_HELPER_FOR_GLM_MATRIX_TYPE
#undef ADD_HELPER_FOR_GLM_VECTOR_TYPE
#undef ADD_HELPER_FOR_GLM_IVECTOR_TYPE
#undef ADD_PRINTTO_FOR_GLM_TYPE
#undef ADD_MINMAX_FOR_GLM_TYPE
#undef ADD_ABS_FOR_GLM_TYPE

}
}

namespace Base {
typedef glm::vec2 vec2;
typedef glm::vec3 vec3;
typedef glm::vec4 vec4;
typedef glm::dvec2 dvec2;
typedef glm::dvec3 dvec3;
typedef glm::dvec4 dvec4;
typedef glm::bvec2 bvec2;
typedef glm::bvec3 bvec3;
typedef glm::bvec4 bvec4;
typedef glm::ivec2 ivec2;
typedef glm::ivec3 ivec3;
typedef glm::ivec4 ivec4;
typedef glm::uvec2 uvec2;
typedef glm::uvec3 uvec3;
typedef glm::uvec4 uvec4;

typedef glm::mat2 mat2;
typedef glm::mat3 mat3;
typedef glm::mat4 mat4;

typedef glm::mat2x2 mat2x2;
typedef glm::mat2x3 mat2x3;
typedef glm::mat2x4 mat2x4;
typedef glm::mat3x2 mat3x2;
typedef glm::mat3x3 mat3x3;
typedef glm::mat3x4 mat3x4;
typedef glm::mat4x2 mat4x2;
typedef glm::mat4x3 mat4x3;
typedef glm::mat4x4 mat4x4;

typedef glm::quat quaternion;

using glm::radians;
using glm::degrees;
using glm::sin;
using glm::cos;
using glm::tan;
using glm::asin;
using glm::acos;
using glm::atan;
using glm::sinh;
using glm::cosh;
using glm::tanh;
using glm::asinh;
using glm::acosh;
using glm::atanh;

using glm::pow;
using glm::exp;
using glm::log;
using glm::exp2;
using glm::log2;
using glm::sqrt;
using glm::inversesqrt;

using glm::sign;
using glm::floor;
using glm::trunc;
using glm::round;
using glm::roundEven;
using glm::ceil;
using glm::fract;
using glm::mod;
using glm::mix;
using glm::step;
using glm::smoothstep;
using glm::isnan;
using glm::isinf;
using glm::floatBitsToInt;
using glm::floatBitsToUint;
using glm::fma;
using glm::frexp;
using glm::ldexp;

using glm::length;
using glm::distance;
using glm::dot;
using glm::cross;
using glm::normalize;
using glm::faceforward;
using glm::reflect;
using glm::refract;

using glm::matrixCompMult;
using glm::outerProduct;
using glm::transpose;
using glm::determinant;
using glm::inverse;

using glm::epsilonEqual;
using glm::epsilonNotEqual;

using glm::degrees;
using glm::radians;

using glm::perp;


const real e = 2.718281828459045235360287471352662497757247;
const real pi = 3.141592653589793238462643383279502884197169;
const real half_pi = 1.570796326794896619231321691639751442098585;
const real two_pi = 6.283185307179586476925286766559005768394339;
const real ln_ten = 2.302585092994045684017991454684364207601101;
const real ln_two = 0.693147180559945309417232121458176568075500;
const real one = 1.0;
const real one_over_pi = 0.318309886183790671537767526745028724068919;
const real one_over_sqrt_two = 0.707106781186547524400844362104849039284836;
const real quarter_pi = 0.785398163397448309615660845819875721049292;
const real sqrt_five = 2.236067977499789696409173668731276235440618;
const real sqrt_pi = 1.772453850905516027298167483341145182797549;
const real sqrt_three = 1.732050807568877293527446341505872366942805;
const real sqrt_two = 1.414213562373095048801688724209698078569672;
const real sqrt_two_pi = 2.506628274631000502415765284811045253006987;
const real third = 0.333333333333333333333333333333333333333333;
const real two_over_pi = 0.636619772367581343075535053490057448137839;
const real two_over_sqrt_pi = 1.128379167095512573896158903121545171688101;
const real two_thirds = 0.666666666666666666666666666666666666666666;
const real zero = 0.0;

const vec2 vec2_unitX = vec2(1, 0);
const vec2 vec2_unitY = vec2(0, 1);

const vec3 vec3_unitX = vec3(1, 0, 0);
const vec3 vec3_unitY = vec3(0, 1, 0);
const vec3 vec3_unitZ = vec3(0, 0, 1);

const quaternion quaternion_noRotation = glm::angleAxis(0.f, vec3(1, 0, 0));

inline vec2 vec2_cast(const Ogre::Vector2& v){return vec2(v.x, v.y);}
inline vec3 vec3_cast(const Ogre::Vector3& v){return vec3(v.x, v.y, v.z);}
inline vec4 vec4_cast(const Ogre::Vector4& v){return vec4(v.x, v.y, v.z, v.w);}
inline vec4 vec4_cast(const Ogre::ColourValue& c){return vec4(c.r, c.g, c.b, c.a);}

template<typename T>
inline real squareLength(const T& a)
{
  return dot(a, a);
}

template<typename T>
inline real squareDistance(const T& a, const T& b)
{
  return squareLength(a-b);
}

inline bool isnan(const vec2& v)
{
  return std::isnan(v.x) || std::isnan(v.y);
}

inline bool isnan(const vec3& v)
{
  return isnan(v.x) || isnan(v.y) || std::isnan(v.z);
}

inline bool isnan(const vec4& v)
{
  return std::isnan(v.x) || std::isnan(v.y) || std::isnan(v.z) || std::isnan(v.w);
}




}


// ==== Google-TestFramework ====
#ifdef GTEST_INCLUDE_GTEST_GTEST_H_
#define USING_GTEST
#endif

namespace Base {
namespace Private {


inline ::testing::AssertionResult CmpHelperVec2EQ(const char* expected_expression,
                                                  const char* actual_expression,
                                                  const vec2& expected,
                                                  const vec2& actual)
{
  typedef vec2::value_type RawType;
  typedef ::testing::internal::Float Float;

  if(Float(expected.x).AlmostEquals(Float(actual.x)) &&
     Float(expected.y).AlmostEquals(Float(actual.y)))
    return ::testing::AssertionSuccess();

  ::std::stringstream expected_ss;
  expected_ss << std::setprecision(std::numeric_limits<RawType>::digits10 + 2)
              << expected;

  ::std::stringstream actual_ss;
  actual_ss << std::setprecision(std::numeric_limits<RawType>::digits10 + 2)
            << actual;

  return EqFailure(expected_expression,
                   actual_expression,
                   ::testing::internal::StringStreamToString(&expected_ss),
                   ::testing::internal::StringStreamToString(&actual_ss),
                   false);
}

inline ::testing::AssertionResult CmpHelperVec3EQ(const char* expected_expression,
                                                  const char* actual_expression,
                                                  const vec3& expected,
                                                  const vec3& actual)
{
  typedef vec3::value_type RawType;
  typedef ::testing::internal::Float Float;

  if(Float(expected.x).AlmostEquals(Float(actual.x)) &&
     Float(expected.y).AlmostEquals(Float(actual.y)) &&
     Float(expected.z).AlmostEquals(Float(actual.z)))
    return ::testing::AssertionSuccess();

  ::std::stringstream expected_ss;
  expected_ss << std::setprecision(std::numeric_limits<RawType>::digits10 + 2)
              << expected;

  ::std::stringstream actual_ss;
  actual_ss << std::setprecision(std::numeric_limits<RawType>::digits10 + 2)
            << actual;

  return EqFailure(expected_expression,
                   actual_expression,
                   ::testing::internal::StringStreamToString(&expected_ss),
                   ::testing::internal::StringStreamToString(&actual_ss),
                   false);
}

inline ::testing::AssertionResult CmpHelperVec4EQ(const char* expected_expression,
                                                  const char* actual_expression,
                                                  const vec4& expected,
                                                  const vec4& actual)
{
  typedef vec4::value_type RawType;
  typedef ::testing::internal::Float Float;

  if(Float(expected.x).AlmostEquals(Float(actual.x)) &&
     Float(expected.y).AlmostEquals(Float(actual.y)) &&
     Float(expected.z).AlmostEquals(Float(actual.z)) &&
     Float(expected.w).AlmostEquals(Float(actual.w)))
    return ::testing::AssertionSuccess();

  ::std::stringstream expected_ss;
  expected_ss << std::setprecision(std::numeric_limits<RawType>::digits10 + 2)
              << expected;

  ::std::stringstream actual_ss;
  actual_ss << std::setprecision(std::numeric_limits<RawType>::digits10 + 2)
            << actual;

  return EqFailure(expected_expression,
                   actual_expression,
                   ::testing::internal::StringStreamToString(&expected_ss),
                   ::testing::internal::StringStreamToString(&actual_ss),
                   false);
}

// The following functions are based on the DoubleNearPredFormat implementaion of the google test framework
inline ::testing::AssertionResult CmpHelperVec2NEAR(const char* expected_expression,
                                                    const char* actual_expression,
                                                    const char* abs_error_expr,
                                                    const vec2& expected,
                                                    const vec2& actual,
                                                    real abs_error)
{
  vec2 difference = abs(expected-actual);
  bvec2 success(difference.x <= abs_error,
                difference.y <= abs_error);

  if(success == bvec2(true))
    return ::testing::AssertionSuccess();

  return ::testing::AssertionFailure()
      << "The componentwise difference between " << expected_expression << " and " << actual_expression
      << " is " << difference << ", which exceeds " << vec2(abs_error) << ", where\n"
      << expected_expression << " evaluates to " << expected << ",\n"
      << actual_expression << " evaluates to " << actual << ", and\n"
      << abs_error_expr << " evaluates to " << abs_error << ".";
}

inline ::testing::AssertionResult CmpHelperVec3NEAR(const char* expected_expression,
                                                    const char* actual_expression,
                                                    const char* abs_error_expr,
                                                    const vec3& expected,
                                                    const vec3& actual,
                                                    real abs_error)
{
  vec3 difference = abs(expected-actual);
  bvec3 success(difference.x <= abs_error,
                difference.y <= abs_error,
                difference.z <= abs_error);

  if(success == bvec3(true))
    return ::testing::AssertionSuccess();

  return ::testing::AssertionFailure()
      << "The componentwise difference between " << expected_expression << " and " << actual_expression
      << " is " << difference << ", which exceeds " << vec3(abs_error) << ", where\n"
      << expected_expression << " evaluates to " << expected << ",\n"
      << actual_expression << " evaluates to " << actual << ", and\n"
      << abs_error_expr << " evaluates to " << abs_error << ".";
}

inline ::testing::AssertionResult CmpHelperVec4NEAR(const char* expected_expression,
                                                    const char* actual_expression,
                                                    const char* abs_error_expr,
                                                    const vec4& expected,
                                                    const vec4& actual,
                                                    real abs_error)
{
  vec4 difference = abs(expected-actual);
  bvec4 success(difference.x <= abs_error,
                difference.y <= abs_error,
                difference.z <= abs_error,
                difference.w <= abs_error);

  if(success == bvec4(true))
    return ::testing::AssertionSuccess();

  return ::testing::AssertionFailure()
      << "The componentwise difference between " << expected_expression << " and " << actual_expression
      << " is " << difference << ", which exceeds " << vec4(abs_error) << ", where\n"
      << expected_expression << " evaluates to " << expected << ",\n"
      << actual_expression << " evaluates to " << actual << ", and\n"
      << abs_error_expr << " evaluates to " << abs_error << ".";
}

} // namespace Private


bool fastTest();

} // namespace Base

#define EXPECT_VEC2_EQ(expected, actual) EXPECT_PRED_FORMAT2(::Base::Private::CmpHelperVec2EQ, expected, actual)
#define ASSERT_VEC2_EQ(expected, actual) ASSERT_PRED_FORMAT2(::Base::Private::CmpHelperVec2EQ, expected, actual)
#define EXPECT_VEC3_EQ(expected, actual) EXPECT_PRED_FORMAT2(::Base::Private::CmpHelperVec3EQ, expected, actual)
#define ASSERT_VEC3_EQ(expected, actual) ASSERT_PRED_FORMAT2(::Base::Private::CmpHelperVec3EQ, expected, actual)
#define EXPECT_VEC4_EQ(expected, actual) EXPECT_PRED_FORMAT2(::Base::Private::CmpHelperVec4EQ, expected, actual)
#define ASSERT_VEC4_EQ(expected, actual) ASSERT_PRED_FORMAT2(::Base::Private::CmpHelperVec4EQ, expected, actual)
#define EXPECT_VEC2_NEAR(expected, actual, abs_error) EXPECT_PRED_FORMAT3(::Base::Private::CmpHelperVec2NEAR, expected, actual, abs_error)
#define ASSERT_VEC2_NEAR(expected, actual, abs_error) ASSERT_PRED_FORMAT3(::Base::Private::CmpHelperVec2NEAR, expected, actual, abs_error)
#define EXPECT_VEC3_NEAR(expected, actual, abs_error) EXPECT_PRED_FORMAT3(::Base::Private::CmpHelperVec3NEAR, expected, actual, abs_error)
#define ASSERT_VEC3_NEAR(expected, actual, abs_error) ASSERT_PRED_FORMAT3(::Base::Private::CmpHelperVec3NEAR, expected, actual, abs_error)
#define EXPECT_VEC4_NEAR(expected, actual, abs_error) EXPECT_PRED_FORMAT3(::Base::Private::CmpHelperVec4NEAR, expected, actual, abs_error)
#define ASSERT_VEC4_NEAR(expected, actual, abs_error) ASSERT_PRED_FORMAT3(::Base::Private::CmpHelperVec4NEAR, expected, actual, abs_error)


// ==== QT ====

inline int mixQHash(int a, int b)
{
  return a ^ b;
}

// Workaround for https://bugreports.qt-project.org/browse/QTBUG-43197
class QSvgGenerator_Workaround : public QSvgGenerator
{
public:
  int metric(PaintDeviceMetric metric) const override
  {
    if(metric == PdmDevicePixelRatio)
      return 1;
    return QSvgGenerator::metric(metric);
  }
};

namespace std {


template<typename T>
int qHash(const std::shared_ptr<T>& ptr, uint seed=0)
{
  return QT_PREPEND_NAMESPACE(qHash)(ptr.get(), seed);
}


template<typename T>
int qHash(const std::weak_ptr<T>& ptr, uint seed=0)
{
  return QT_PREPEND_NAMESPACE(qHash)(ptr.lock().get(), seed);
}


inline int qHash(const std::string& key, uint seed=0)
{
  return qHash(QString::fromStdString(key), seed);
}


} // namespace std

namespace Base
{


  template<typename TKey, typename TValue>
  TValue hashValue(const QHash<TKey, TValue>& hash, const TKey& key, const TValue& fallbackValue)
  {
    typename QHash<TKey, TValue>::const_iterator i = hash.find(key);

    if(i != hash.end())
      return i.value();
    else
      return fallbackValue;
  }


  template<typename TKey, typename TValue>
  const TValue& hashValueCRef(const QHash<TKey, TValue>& hash, const TKey& key, const TValue& fallbackValue)
  {
    typename QHash<TKey, TValue>::const_iterator i = hash.find(key);

    if(i != hash.end())
      return i.value();
    else
      return fallbackValue;
  }


  template<typename TKey, typename TValue>
  bool hashHasKey(const QHash<TKey, TValue>& hash, const TKey& key)
  {
    return hash.find(key) != hash.end();
  }


}


// ==== Boost ====
#if BOOST_VERSION < 105000

#define BOOST_FILESYSTEM_NAMESPACE_NAME filesystem3

#else

#define BOOST_FILESYSTEM_NAMESPACE_NAME filesystem

#endif

namespace Base {
namespace IO {

typedef boost::BOOST_FILESYSTEM_NAMESPACE_NAME::path Path;

}
}

namespace boost {
namespace BOOST_FILESYSTEM_NAMESPACE_NAME {


inline void PrintTo(const path& p, std::ostream* o)
{
  *o << p;
}


}
}

#undef BOOST_FILESYSTEM_NAMESPACE_NAME



// ==== Standart C++ Library ====
namespace Base {


using std::placeholders::_1;
using std::placeholders::_2;
using std::placeholders::_3;
using std::placeholders::_4;
using std::placeholders::_5;
using std::placeholders::_6;
using std::placeholders::_7;
using std::placeholders::_8;
using std::placeholders::_9;

using std::swap;

using std::abs;
using std::min;
using std::max;

using std::shared_ptr;
using std::weak_ptr;
using std::unique_ptr;

typedef std::uint8_t uint8;
typedef std::int8_t int8;
typedef std::uint16_t uint16;
typedef std::int16_t int16;
typedef std::uint32_t uint32;
typedef std::int32_t int32;
typedef std::uint64_t uint64;
typedef std::int64_t int64;


}



// ==== decide, whether we're on a Unix System ====
#if OGRE_PLATFORM==OGRE_PLATFORM_ANDROID || \
    OGRE_PLATFORM==OGRE_PLATFORM_APPLE || \
    OGRE_PLATFORM==OGRE_PLATFORM_APPLE_IOS || \
    OGRE_PLATFORM==OGRE_PLATFORM_LINUX
#define PLATFORM_UNIX 1
#else
#define PLATFORM_UNIX 0
#endif

#define COMMA ,



// ==== general helper ====
namespace Base {


const real DEFAULT_ANTIALIASING_WIDTH = 1.0f;


class noncopyable
{
public:
  noncopyable(const noncopyable&) = delete;
  noncopyable& operator=(const noncopyable&) = delete;

public:
  noncopyable()
  {
  }

  ~noncopyable()
  {
  }
};

class nonconstructable
{
public:
  nonconstructable() = delete;
  nonconstructable(const nonconstructable&) = delete;
  nonconstructable& operator=(const nonconstructable&) = delete;

protected:
  ~nonconstructable() = delete;
};



template<typename T>
int compare(const T& a, const T& b)
{
  if(a<b)
    return -1;
  if(a>b)
    return 1;
  return 0;
}

template<typename T, typename... T_Args>
int compare(const T& a, const T& b, const T_Args&... other)
{
  if(a<b)
    return -1;
  if(a>b)
    return 1;
  return compare(other...);
}



}


#include <base/math.h>
#include <base/range-iterator.h>


#endif
