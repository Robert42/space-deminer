#include <dependencies.h>
#include <base/io/special-directories.h>
#include <base/io/regular-file.h>

#include <Shlobj.h>

namespace Base {
namespace IO {
using namespace boost::filesystem;
using boost::filesystem::path;


path getAppDataDirectory()
{
  // see also:
  // http://msdn.microsoft.com/en-us/library/bb762494%28VS.85%29.aspx
  // http://msdn.microsoft.com/en-us/library/bb762181%28v=vs.85%29.aspx

  std::vector<TCHAR> APP_DATA;
  APP_DATA.resize(MAX_PATH+2);
  HRESULT success = SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, APP_DATA.data());

  if(!SUCCEEDED(success))
    throw Ogre::FileNotFoundException(0,
                                      "Couldn't find the appdata directory.",
                                      "getAppDataDirectory()",
                                      __FILE__,
                                      __LINE__);

  path appDataPath = path(APP_DATA.data());

  appDataPath = absolute(appDataPath);

  if(!exists(appDataPath))
    throw Ogre::FileNotFoundException(1,
                                      "Couldn't find the appdata directory.",
                                      "getAppDataDirectory()",
                                      __FILE__,
                                      __LINE__);
  if(!is_directory(appDataPath))
    throw Ogre::FileNotFoundException(2,
                                      "<"+appDataPath.string()+"> is not a directory!",
                                      "getAppDataDirectory()",
                                      __FILE__,
                                      __LINE__);

  return appDataPath;
}


Directory SpecialDirectories::getGlobalConfigDirectory()
{
  return getAppDataDirectory();
}


RegularFile SpecialDirectories::getRunningBinary()
{
  // see http://msdn.microsoft.com/en-us/library/windows/desktop/ms683197.aspx

  std::vector<TCHAR> proc_self_exe;
  DWORD bufferLength = MAX_PATH+2;
  proc_self_exe.resize(bufferLength+2);

  DWORD pathLength = GetModuleFileName(NULL, proc_self_exe.data(), bufferLength-1);

  if(pathLength == 0)
    throw Ogre::FileNotFoundException(0,
                                      "Couldn't find the path to the running binary.",
                                      "getPathOfRunningBinary()",
                                      __FILE__,
                                      __LINE__);

  if(pathLength+2 > bufferLength)
  {
    bufferLength = MAX_PATH*16 +2;

    if(bufferLength < 4096)
      bufferLength = 5000;

    proc_self_exe.resize(bufferLength);
    pathLength = GetModuleFileName(NULL, proc_self_exe.data(), bufferLength-1);

    if(pathLength+2 > bufferLength)
      throw Ogre::FileNotFoundException(0,
                                        "Couldn't find the path to the running binary because of too long Path lengths.\nPlease choose another Directory with less charaters than 4000.",
                                        "getPathOfRunningBinary()",
                                        __FILE__,
                                        __LINE__);
  }

  proc_self_exe[pathLength] = 0;

  path binaryPath = absolute(path(proc_self_exe.data()));

  if(!exists(binaryPath))
    throw Ogre::FileNotFoundException(1,
                                      "Couldn't find the path to the running binary.",
                                      "getPathOfRunningBinary()",
                                      __FILE__,
                                      __LINE__);

  return binaryPath;
}


} // namespace IO
} // namespace Base
