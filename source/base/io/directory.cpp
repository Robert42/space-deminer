#include <dependencies.h>
#include <base/io/directory.h>
#include <base/io/regular-file.h>
#include <base/threads.h>


namespace Base {
namespace IO {

Directory::CouldntCreateDirectoryException::CouldntCreateDirectoryException(const IO::Path& path,
                                                                            const Ogre::String& source,
                                                                            const char* file,
                                                                            int line)
  : Ogre::Exception(0,
                    String::compose("Couldn't create the Directory <%0>", path).toOgreString(),
                    source,
                    "CouldntCreateDirectoryException",
                    file,
                    line)
{
}


Directory::Directory()
{
}


Directory::Directory(const Path& path) : File(path)
{
}


bool Directory::exists() const
{
  return File::exists() && boost::filesystem::is_directory(this->path());
}


void Directory::forallFiles(const std::function<bool(const File&)>& doAction) const
{
  iterator i = this->begin();
  iterator end;

  while(i != end)
  {
    if(!doAction(i->path()))
      return;

    ++i;
  }
}

void Directory::forallSubDirectories(const std::function<bool(const Directory&)>& doAction) const
{
  auto wrapper = [&doAction](const File& f){
    const Directory& d = static_cast<const Directory&>(f);

    if(d.exists())
      return doAction(d);
    else
      return true;
  };

  forallFiles(wrapper);
}

void Directory::forallRegularFiles(const std::function<bool(const RegularFile&)>& doAction) const
{
  auto wrapper = [&doAction](const File& f){
    const RegularFile& r = static_cast<const RegularFile&>(f);

    if(r.exists())
      return doAction(r);
    else
      return true;
  };

  forallFiles(wrapper);
}


Directory::iterator Directory::begin() const
{
  return iterator(this->path());
}


Directory::iterator Directory::end() const
{
  return iterator();
}


bool Directory::existsFileWith(const std::function<bool(const File&)>& predicate) const
{
  bool foundAFile = false;

  forallFiles([&foundAFile,&predicate](const File &filename)
  {
    if(predicate(filename))
    {
      foundAFile = true;
      return false;
    }

    return true;
  });

  return foundAFile;
}


Directory Directory::current()
{
  return boost::filesystem::current_path();
}


void Directory::setCurrent(const Directory& directory)
{
  boost::filesystem::current_path(directory.path());
}


QList<File> Directory::allFiles() const
{
  QList<File> files;

  auto collector = [&files](const File &f){
    files.append(f);
    return true;
  };

  forallFiles(collector);

  return files;
}


QList<Directory> Directory::allSubDirectories() const
{
  QList<Directory> directories;

  auto collector = [&directories](const Directory &d){
    directories.append(d);
    return true;
  };

  forallSubDirectories(collector);

  return directories;
}


QList<RegularFile> Directory::allRegularFiles() const
{
  QList<RegularFile> regularFiles;

  auto collector = [&regularFiles](const RegularFile &r){
    regularFiles.append(r);
    return true;
  };

  forallRegularFiles(collector);

  return regularFiles;
}


File Directory::operator * (const Path& path) const
{
  return File(this->path() / path);
}


Directory Directory::operator / (const Path& path) const
{
  return Directory(this->path() / path);
}


RegularFile Directory::operator | (const Path& path) const
{
  return RegularFile(this->path() / path);
}


void Directory::createIfNotExisting(bool waitUntilCreated)
{
  if(!this->exists())
  {
    create_directory(this->path());

    if(waitUntilCreated)
    {
      sleepWhile([this]()
      {
        return !this->exists();
      });

      if(!this->exists())
        throw CouldntCreateDirectoryException(this->path(), "Directory::createIfNotExisting", __FILE__, __LINE__);
    }
  }
}


CurrentDirectoryChanger::CurrentDirectoryChanger(const Directory& newCurrentDirectory)
  : oldDirectory(Directory::current())
{
  Directory::setCurrent(newCurrentDirectory);
}


CurrentDirectoryChanger::~CurrentDirectoryChanger()
{
  Directory::setCurrent(oldDirectory);
}


}
}
