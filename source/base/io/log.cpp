#include <dependencies.h>
#include <base/io/log.h>
#include <base/io/directory.h>
#include <base/io/special-directories.h>
#include <base/strings/string.h>

namespace Base {
namespace IO {


Log::Log()
{
  defaultLog = nullptr;
}


void Log::init()
{
  setLogFile(SpecialDirectories::directoryOfRunningBinary());
}


Log::~Log()
{
  if(this->defaultLog != nullptr)
    Ogre::LogManager::getSingleton().destroyLog(defaultLog);
}


void Log::createLog()
{
  if(this->defaultLog != nullptr)
    return;

  Ogre::LogManager* logManager = new Ogre::LogManager();

  logManager->setLogDetail(Ogre::LL_BOREME);

  this->defaultLog = logManager->createLog(logFile.pathAsOgreString(),
                                           true,
                                           false);
}


void Log::init(const Directory& logFileDirectory)
{
  singleton().setLogFile(logFileDirectory);

  singleton().createLog();
}


void Log::plainLog(const Ogre::String& message, bool writeToCErrr, Ogre::LogMessageLevel logMessageLevel, bool maskDebug)
{
  singleton().createLog();

#if OGRE_PLATFORM != OGRE_PLATFORM_WIN32
  if(writeToCErrr)
    std::cerr << message << std::endl;
#endif

  singleton().defaultLog->logMessage(message, logMessageLevel, maskDebug);
}


void Log::logUtf8String(const std::string& message)
{
  plainLog(String::fromUtf8(message).toOgreString());
}


void Log::logErrorUtf8String(const std::string& message)
{
  logError(String::fromUtf8(message));
}


void Log::logWarningUtf8String(const std::string& message)
{
  logWarning(String::fromUtf8(message));
}


void Log::setLogFile(const Directory& logFileDirectory)
{
  singleton().logFile = logFileDirectory | Path("space-deminer.log");
}


Log& Log::singleton()
{
  static unique_ptr<Log> singleton;

  if(!singleton)
  {
    singleton = unique_ptr<Log>(new Log());
    singleton->init();
  }

  return *singleton;
}


} // namespace IO
} // namespace Base
