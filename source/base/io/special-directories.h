#ifndef _BASE_IO_SPECIAL_DIRECTORIES_H_
#define _BASE_IO_SPECIAL_DIRECTORIES_H_

#include "./directory.h"

namespace Base {
namespace IO {

/** A helper class providing static functions with paths to special directories.
 *
 * It is not possible nor intended to create an instance of this class.
 */
class SpecialDirectories
{
private:
  SpecialDirectories();

public:
  /** Returns the directory which should be used to save configuration files in there.
   *
   * @return the path to the config directory.
   *
   * @note It's guarented, that the returned directory exists.
   */
  static Directory globalConfigDirectory();

  /** Returns the path of the executable currently running.
   *
   * @return the path to the current executable.
   *
   * @note It's guarented, that the returned directory exists.
   */
  static RegularFile runningBinary();

  /** Returns the directory of the executable currently running.
   *
   * @return the path to directory of the current executable.
   * @note It's guarented, that the returned directory exists.
   */
  static Directory directoryOfRunningBinary();

  /** Returns the directory containing the basic /bin /lib /assets structure
   *
   * @note It's guarented, that the returned directory exists.
   */
  static Directory binaryPrefix();

  /** Logs all paths for debugging reasons.
   *
   * @param applicationName the name of the application you want to use as folder name
   *        for configDirectory(). and logDirectory().
   */
  static void logAllPaths();
};

} // namespace IO
} // namespace Base

#endif
