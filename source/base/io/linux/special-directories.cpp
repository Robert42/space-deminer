#include <base/io/special-directories.h>
#include <base/io/regular-file.h>
#include <base/strings/string.h>

#include <unistd.h>

namespace Base {
namespace IO {


using namespace boost::filesystem;
using boost::filesystem::path;


path getHomeDirectory();


Directory SpecialDirectories::globalConfigDirectory()
{
  return getHomeDirectory();
}


RegularFile SpecialDirectories::runningBinary()
{
  char proc_self_exe[PATH_MAX+2];

  ssize_t pathLength = readlink("/proc/self/exe", proc_self_exe, PATH_MAX+1);

  proc_self_exe[pathLength] = 0;

  path binaryPath = absolute(path(proc_self_exe));

  if(!exists(binaryPath))
    throw Ogre::FileNotFoundException(0,
                                      "Couldn't find the path to the running binary.",
                                      "getPathOfRunningBinary()",
                                      __FILE__,
                                      __LINE__);

  return binaryPath;
}

} // namespace IO
} // namespace Base
