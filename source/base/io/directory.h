#ifndef _BASE_IO_DIRECTORY_H_
#define _BASE_IO_DIRECTORY_H_

#include "./file.h"

namespace Base {
namespace IO {


class RegularFile;

/** An instance of this class represents a directory - no matter whether the directory exist's or not.
     *
     * Internally it simply stores the path of the directory.
     */
class Directory : public File
{
public:
  class CouldntCreateDirectoryException : Ogre::Exception
  {
  public:
    CouldntCreateDirectoryException(const IO::Path& path, const Ogre::String& source, const char* file, int line);
  };

public:
  typedef boost::filesystem::directory_iterator iterator;

public:
  Directory();
  Directory(const Path& path);

public:
  bool exists() const;

public:
  iterator begin() const;
  iterator end() const;

  /** Executes an action for every file within a directory.
       *
       * @note Please note that this method is not recursive.
       *
       * @param doAction the function done for every child. It expects a path of the file
       *           and returns a boolean value telling, whether to continue the search. If
       *           doAction returns false, the search of the subfiles is aborted.
       */
  void forallFiles(const std::function<bool(const File&)>& doAction) const;


  /** Executes an action for every subdirectory within a directory.
       *
       * @note Please note that this method is not recursive.
       *
       * @param doAction the function done for every child. It expects a path of the file
       *           and returns a boolean value telling, whether to continue the search. If
       *           doAction returns false, the search of the subfiles is aborted.
       */
  void forallSubDirectories(const std::function<bool(const Directory&)>& doAction) const;


  /** Executes an action for every regular file within a directory.
       *
       * @note Please note that this method is not recursive.
       *
       * @param doAction the function done for every child. It expects a path of the file
       *           and returns a boolean value telling, whether to continue the search. If
       *           doAction returns false, the search of the subfiles is aborted.
       */
  void forallRegularFiles(const std::function<bool(const RegularFile&)>& doAction) const;


  /** Checks for all file within a directory, whether a certain attribute is true for them.
       *
       * @note Please note that this method is not recursive.
       *
       * @param predicate expecting a path and returning a boolean value. For the first file,
       *        for who the predicate returns true the internal loop gets aborted and existsFileInWith return true.
       *
       * @return true, if there's a file in directory for which predicate returns true.
       */
  bool existsFileWith(const std::function<bool(const File&)>& predicate) const;

  static Directory current();
  static void setCurrent(const Directory& directory);

  /** Returns a list of all files within a directory.
       *
       * @note Please note that this method is not recursive.
       *
       * @return All files within athe directory
       */
  QList<File> allFiles() const;

  /** Returns a list of all subdirectories within a directory.
       *
       * @note Please note that this method is not recursive.
       *
       * @return All const within athe directory
       */
  QList<Directory> allSubDirectories() const;

  /** Returns a list of all regular files within a directory.
       *
       * @note Please note that this method is not recursive.
       *
       * @return All regular files within athe directory
       */
  QList<RegularFile> allRegularFiles() const;

public:
  File operator * (const Path& path) const;
  Directory operator / (const Path& path) const;
  RegularFile operator | (const Path& path) const;

public:
  void createIfNotExisting(bool waitUntilCreated = true);
};



template<typename T>
std::basic_ostream<T>& operator<<(std::basic_ostream<T>& o, const Directory& directory)
{
  return o << "Directory(" << directory.path() << ")";
}


/**
     * A small helper class to temporary change the current directory.
     *
     * The constructor changes the current directory and the destructor resets it again.
     */
class CurrentDirectoryChanger
{
  const Directory oldDirectory;
public:
  CurrentDirectoryChanger(const Directory& newCurrentDirectory);
  ~CurrentDirectoryChanger();
};


} // namespace IO
} // namespace Base

#endif
