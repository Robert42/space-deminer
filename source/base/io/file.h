#ifndef _BASE_IO_FILE_H_
#define _BASE_IO_FILE_H_

#include <dependencies.h>

#include <base/strings/string.h>

namespace Base {
namespace IO {


class Directory;
class File
{
private:
  Path _path;

public:
  File();
  File(const Path& path);

public:
  const Path& path() const;
  String pathAsString() const;
  Ogre::String pathAsOgreString() const;
  CEGUI::String pathAsCeguiString() const;
  QString pathAsQString() const;

  Directory parentDirectory() const;

  String filename() const;
  String extension() const;
  String filenameWithoutExtension() const;

  bool exists() const;
};


template<typename T>
std::basic_ostream<T>& operator<<(std::basic_ostream<T>& o, const File& directory)
{
  return o << "File(" << directory.path() << ")";
}


} // namespace IO
} // namespace Base

#endif
