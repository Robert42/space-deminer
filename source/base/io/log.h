#ifndef _BASE_IO_OGRELOG_H_
#define _BASE_IO_OGRELOG_H_

#include "./regular-file.h"

namespace Base {
namespace IO {

/** Initializes and deinitializes the log of Ogre.
 *
 * @note There shouldn't be any reason for you to use this class.
 */
class Log : noncopyable
{
private:
  Ogre::Log* defaultLog;
  RegularFile logFile;

  Log();

  void createLog();

  static Log& singleton();
  void init();

public:
  ~Log();

public:
  static void init(const Directory& logFileDirectory);

  /** @brief plainLog
   *
   * Don't use this, use `Log::log` or `Log::logWarning` instead.
   *
   * @param message
   * @param logMessageLevel
   * @param maskDebug
   */
  static void plainLog(const Ogre::String& message,
                       bool writeToCErrr = false,
                       Ogre::LogMessageLevel logMessageLevel = Ogre::LML_NORMAL,
                       bool maskDebug = false);

  template<typename... argument_types>
  static void log(const String& message, const argument_types& ... arguments)
  {
    plainLog(String::compose(message, arguments...).toOgreString());
  }

  template<typename... argument_types>
  static void logWarning(const String& message, const argument_types& ... arguments)
  {
    plainLog(String::compose("**WARNING** " + message, arguments...).toOgreString(),
             true);
  }

  template<typename... argument_types>
  static void logError(const String& message, const argument_types& ... arguments)
  {
    plainLog(String::compose("**ERROR** " + message, arguments...).toOgreString(),
             true,
             Ogre::LML_CRITICAL);
  }

  static void logUtf8String(const std::string& message);
  static void logErrorUtf8String(const std::string& message);
  static void logWarningUtf8String(const std::string& message);

private:
  void setLogFile(const Directory& logFileDirectory);
};


} // namespace IO
} // namespace Base

#endif
