#include <base/io/special-directories.h>
#include <base/io/regular-file.h>

#include <mach-o/dyld.h>

namespace Base {
namespace IO {


using namespace boost::filesystem;
using boost::filesystem::path;


bool tryGetExecutable(std::string* executablePath, uint32_t* bufferSize);
std::string getExecutablePath();
std::string makeRealPath(const std::string& path);


path getHomeDirectory();


Directory SpecialDirectories::getGlobalConfigDirectory()
{
  return getHomeDirectory();
}


RegularFile SpecialDirectories::getRunningBinary()
{
  return path(makeRealPath(getExecutablePath()));
}


std::string getExecutablePath()
{
  uint32_t pathBufferSize = 1024;
  std::string executablePath;

  if(tryGetExecutable(&executablePath, &pathBufferSize))
    return executablePath;
  if(tryGetExecutable(&executablePath, &pathBufferSize))
    return executablePath;

  throw Ogre::FileNotFoundException(3,
                                    "Couldn't find the executable.\n_NSGetExecutablePath returned -1 twice.",
                                    "getExecutablePath()",
                                    __FILE__,
                                    __LINE__);
}

bool tryGetExecutable(std::string* executablePath, uint32_t* bufferSize)
{
  std::vector<char> pathBuffer;
  pathBuffer.resize(*bufferSize);

  int returnCode = _NSGetExecutablePath(pathBuffer.data(),
                                        bufferSize);

  if(returnCode == 0)
  {
    *executablePath = std::string(pathBuffer.data());
    return true;
  }

  if(returnCode == -1) // buffer was too small
  {
    *bufferSize += 8;
    return false;
  }

  throw Ogre::FileNotFoundException(4,
                                    "Couldn't find the executable.\n"
                                    "Calling _NSGetExecutablePath resulted in the "
                                    "unexpected error code." + std::to_string(returnCode),
                                    "tryGetExecutable()",
                                    __FILE__,
                                    __LINE__);
}

std::string makeRealPath(const std::string& path)
{
  char* resultPathBuffer = realpath(path.data(), NULL);

  std::string resultBuffer = resultPathBuffer;

  free(resultPathBuffer);

  return resultBuffer;
}



} // namespace IO
} // namespace Base
