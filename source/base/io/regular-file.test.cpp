#include <dependencies.h>
#include <base/io/regular-file.h>


namespace Base {
namespace IO {


TEST(base_io_RegularFile, gtest_printing)
{
  EXPECT_EQ("RegularFile(\"/a/b/c\")", ::testing::PrintToString(RegularFile("/a/b/c")));
}


} // namespace IO
} // namespace Base
