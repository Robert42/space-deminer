#ifndef _BASE_IO_REGULAR_FILE_H_
#define _BASE_IO_REGULAR_FILE_H_


#include "./file.h"


namespace Base {
namespace IO {


class RegularFile : public File
{
public:
  RegularFile();
  RegularFile(const Path& path);

public:
  bool exists() const;
};



template<typename T>
std::basic_ostream<T>& operator<<(std::basic_ostream<T>& o, const RegularFile& file)
{
  return o << "RegularFile(" << file.path() << ")";
}


} // namespace IO
} // namespace Base


#endif
