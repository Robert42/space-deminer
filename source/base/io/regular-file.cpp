#include <dependencies.h>
#include <base/io/regular-file.h>


namespace Base {
namespace IO {

RegularFile::RegularFile()
{
}


RegularFile::RegularFile(const Path& path) : File(path)
{
}


bool RegularFile::exists() const
{
  return File::exists() && boost::filesystem::is_regular(this->path());
}


} // namespace IO
} // namespace Base
