#include <dependencies.h>
#include <base/io/directory.h>


namespace Base {
namespace IO {


File::File()
{
}


File::File(const Path& path) : _path(path)
{
}


const Path& File::path() const
{
  return this->_path;
}


String File::pathAsString() const
{
  return String::fromPath(this->path());
}


Ogre::String File::pathAsOgreString() const
{
  return pathAsString().toOgreString();
}


CEGUI::String File::pathAsCeguiString() const
{
  return pathAsString().toCeguiString();
}


QString File::pathAsQString() const
{
  return pathAsString().toQString();
}


Directory File::parentDirectory() const
{
  return Directory(this->path().parent_path());
}


bool File::exists() const
{
  return boost::filesystem::exists(this->path());
}


String File::filename() const
{
  return String::fromPath(this->path().filename());
}


String File::extension() const
{
  return String::fromPath(this->path().extension());
}


String File::filenameWithoutExtension() const
{
  return filename().trimFromRight(extension().length());
}


} // namespace IO
} // namespace Base
