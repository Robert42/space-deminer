#ifndef _BASE_IO_FATALERRORLOG_H_
#define _BASE_IO_FATALERRORLOG_H_

#include <dependencies.h>

namespace Base {
namespace IO {

/** Writes a log output in case of a fatal error into a seperate file and the error stream.
 *
 * @note There shouldn't be any reason for you to use this method.
 *
 * @param logFilename this is not allowed to be a path. No slashes (forward or backslash) allowed here.
 * @param detailedDesription
 */
void writeFatalErrorLog(const std::string& detailedDesription, bool writeToLog = true);

} // namespace IO
} // namespace Base

#endif
