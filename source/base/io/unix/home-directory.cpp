#include <base/io/special-directories.h>
#include <base/io/regular-file.h>
#include <base/strings/string.h>

#include <pwd.h>
#include <unistd.h>

namespace Base {
namespace IO {


using namespace boost::filesystem;
using boost::filesystem::path;

inline path getHomeDirectoryFromEnvironment()
{
  char* HOME = getenv("HOME");

  if(HOME == nullptr)
    throw Ogre::FileNotFoundException(0,
                                      "The environment variable 'HOME' is not set",
                                      "getHomeDirectory()",
                                      __FILE__,
                                      __LINE__);

  return absolute(path(HOME));
}

inline path getHomeDirectoryFromUniStdLibrary()
{
  struct passwd* userinfo = getpwuid(getuid());

  if(userinfo)
    return absolute(path(userinfo->pw_dir));
  else
    return path();
}

inline bool isValidDirectory(const path& homeDirectory)
{
  return exists(homeDirectory) && is_directory(homeDirectory);
}


path getHomeDirectory()
{
  path homeDirectory = getHomeDirectoryFromEnvironment();

  if(!isValidDirectory(homeDirectory))
  {
    homeDirectory = getHomeDirectoryFromUniStdLibrary();

    if(!isValidDirectory(homeDirectory))
    {
      throw Ogre::FileNotFoundException(1,
                                        "Couldn't find the home directory.",
                                        "getHomeDirectory()",
                                        __FILE__,
                                        __LINE__);
    }
  }

  return homeDirectory;
}


} // namespace IO
} // namespace Base
