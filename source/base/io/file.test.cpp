#include <dependencies.h>
#include <base/io/directory.h>


namespace Base {
namespace IO {


TEST(base_io_File, gtest_printing)
{
  EXPECT_EQ("File(\"/a/b/c\")", ::testing::PrintToString(File("/a/b/c")));
}


} // namespace IO
} // namespace Base
