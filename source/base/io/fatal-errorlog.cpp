#include <dependencies.h>
#include <base/io/log.h>
#include <base/strings/string.h>

namespace Base {
namespace IO {


void writeFatalErrorLog(const std::string& detailedDesription, bool writeToLog)
{
  std::cerr << detailedDesription << std::endl;

  if(writeToLog)
  {
    IO::Log::plainLog(detailedDesription,
                      Ogre::LML_CRITICAL);
  }
}


} // namespace IO
} // namespace Base
