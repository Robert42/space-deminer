#include <dependencies.h>
#include <base/io/special-directories.h>
#include <base/io/log.h>
#include <base/strings/string.h>

namespace Base {
namespace IO {


SpecialDirectories::SpecialDirectories()
{
}


Directory SpecialDirectories::directoryOfRunningBinary()
{
  return runningBinary().parentDirectory();
}


Directory SpecialDirectories::binaryPrefix()
{
  Directory binDirectory = directoryOfRunningBinary();

  if(binDirectory.path().filename() == "MacOS" && binDirectory.parentDirectory().path().filename() == "Contents")
    return binDirectory.parentDirectory() / "Resources";

  if(binDirectory.path().filename() != "bin")
  {
    binDirectory = binDirectory.parentDirectory();

    if(binDirectory.path().filename() != "bin")
      throw Ogre::FileNotFoundException(0,
                                        "Couldn't find the binary prefix",
                                        "getBinaryPrefix()",
                                        __FILE__,
                                        __LINE__);
  }

  return binDirectory.parentDirectory();
}


void SpecialDirectories::logAllPaths()
{
  Log::log("\n==== Found Paths ====\n"
           "getGlobalConfigDirectory(): %0\n"
           "getPathOfRunningBinary(): %1\n"
           "getDirectoryOfRunningBinary(): %2\n"
           "getBinaryPrefix(): %3\n"
           "====",
           globalConfigDirectory(),
           runningBinary(),
           directoryOfRunningBinary(),
           binaryPrefix());
}

} // namespace IO
} // namespace Base
