#include <dependencies.h>
#include <base/io/directory.h>
#include <base/io/regular-file.h>
#include <base/threads.h>


namespace Base {
namespace IO {


TEST(base_io_Directoy, gtest_printing)
{
  EXPECT_EQ("Directory(\"/a/b/c\")", ::testing::PrintToString(Directory("/a/b/c")));
}


} // namespace IO
} // namespace Base
