#ifndef BASE_INCREMENTINLIFETIME_H
#define BASE_INCREMENTINLIFETIME_H

#include <dependencies.h>

namespace Base {


template<typename T_Int, T_Int max_value=std::numeric_limits<T_Int>::max()>
class IncrementInLifeTime
{
  T_Int* number;
public:
  IncrementInLifeTime(T_Int& n)
  {
    set(&n);
  }

  IncrementInLifeTime(const IncrementInLifeTime& other)
  {
    set(other.number);
  }

  ~IncrementInLifeTime()
  {
    unset();
  }

  IncrementInLifeTime& operator=(const IncrementInLifeTime& other) = delete;

private:
  void set(T_Int* n)
  {
    this->number = n;
    assert(*this->number<max_value);
    (*this->number)++;
  }
  void unset()
  {
    (*this->number)--;
  }
};


} // namespace Base

#endif // BASE_INCREMENTINLIFETIME_H
