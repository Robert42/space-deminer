#ifndef BASE_COLORTOOLS_H
#define BASE_COLORTOOLS_H

#include <dependencies.h>

namespace Base {

vec4 darker(const vec4& color, real factor);
vec4 lighter(const vec4& color, real factor);

real brightness(const vec4& color);

vec4 colorFromHsl(real h, real s, real l, real a=1.f);

QColor toQColor(const vec4& v);
vec4 fromQColor(const QColor& c);

Ogre::ColourValue premultiply(const Ogre::ColourValue& color);
Ogre::ColourValue unpremultiply(const Ogre::ColourValue& color);

ivec4 colorAsIVec(const vec4& color, int white=255);
ivec3 colorAsIVec(const vec3& color, int white=255);

uint32 colorAsRgbaInteger(const vec4& color);
uint32 colorAsRgbInteger(const vec3& color);

} // namespace Base

namespace glm {
namespace detail {

vec4 premultiply(const vec4& color);
vec4 unpremultiply(const vec4& color);

} // namespace detail
} // namespace glm

namespace Ogre {

Ogre::ColourValue premultiply(const Ogre::ColourValue& color);
Ogre::ColourValue unpremultiply(const Ogre::ColourValue& color);

} // namespace Ogre

#endif // BASE_COLORTOOLS_H
