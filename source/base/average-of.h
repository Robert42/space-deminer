#ifndef _BASE_AVERAGE_OF_H_
#define _BASE_AVERAGE_OF_H_

#include <dependencies.h>

namespace Base {


template<typename T = real, typename T_scalar = T>
class AverageOf
{
private:
  std::vector<T> values;
  size_t valuesStored;
  size_t writePointer;

public:
  AverageOf(size_t numberValuesToStore = 10);

  void addValue(const T& value);
  T calcAverage(const T& defaultValue = T()) const;
  T calcAverageWithFalloff(T_scalar factor = 0.8, const T& defaultValue = T()) const;

  void setNumberValuesToStore(size_t numberValuesToStore);

  size_t numberValuesStored() const;
  size_t numberValuesToStore() const;

  void reset();
};


template<typename T, typename T_scalar>
AverageOf<T, T_scalar>::AverageOf(size_t numberValuesToStore)
{
  valuesStored = 0;
  writePointer = 0;

  setNumberValuesToStore(numberValuesToStore);
}

template<typename T, typename T_scalar>
void AverageOf<T, T_scalar>::addValue(const T& value)
{
  values[writePointer] = value;

  valuesStored = min(valuesStored+1, numberValuesToStore());
  writePointer = (writePointer+1) % numberValuesToStore();
}

template<typename T, typename T_scalar>
T AverageOf<T, T_scalar>::calcAverage(const T& defaultValue) const
{
  if(numberValuesStored() < 1)
    return defaultValue;

  T average = T_scalar(0);

  for(size_t i=0; i<numberValuesStored(); ++i)
    average += values[i];

  return average / T_scalar(numberValuesStored());
}

template<typename T, typename T_scalar>
T AverageOf<T, T_scalar>::calcAverageWithFalloff(T_scalar factor, const T& defaultValue) const
{
  if(numberValuesStored() < 1)
    return defaultValue;

  T average = T_scalar(0);
  T_scalar weight = T_scalar(1.0);
  T_scalar totalWeight = T_scalar(0);

  size_t newestValue = (writePointer-1+numberValuesToStore()) % numberValuesToStore();

  for(size_t i=0; i<numberValuesStored(); ++i)
  {
    average += values[(newestValue-i+numberValuesToStore()) % numberValuesToStore()] * weight;
    totalWeight += weight;

    weight *= factor;
  }

  return average / totalWeight;
}

template<typename T, typename T_scalar>
void AverageOf<T, T_scalar>::setNumberValuesToStore(size_t numberValuesToStore)
{
  numberValuesToStore = clamp<size_t>(numberValuesToStore, 1, 32767);

  values.resize(numberValuesToStore);

  if(writePointer > numberValuesToStore)
    writePointer = 0;

  valuesStored = min(valuesStored, numberValuesToStore);
}

template<typename T, typename T_scalar>
size_t AverageOf<T, T_scalar>::numberValuesStored() const
{
  return valuesStored;
}

template<typename T, typename T_scalar>
size_t AverageOf<T, T_scalar>::numberValuesToStore() const
{
  return values.size();
}

template<typename T, typename T_scalar>
void AverageOf<T, T_scalar>::reset()
{
  valuesStored = 0;
  writePointer = 0;
}


} // namespace Base

#endif
