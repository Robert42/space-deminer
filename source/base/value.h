#ifndef BASE_VALUE_H
#define BASE_VALUE_H

#include <base/types/type.h>

namespace Base {

class Value final
{
public:
  Type _type;
  QVector<uint8> _bytes;

public:
  Value();
  ~Value();

  explicit Value(float value);
  explicit Value(int value);
  explicit Value(bool value);

  const Type& type() const;
  const QVector<uint8>& bytes() const;

  Value(const Value& other);
  Value& operator=(const Value& other);

  bool isEqualityCheckSupported() const;

  bool operator == (const Value& other) const;
  bool operator != (const Value& other) const;

  void initFromBytes(const Type& type, const void* bytes);

private:
  void _initWithFallback();
  void _deinit();
};

} // namespace Base

#endif // BASE_VALUE_H
