#include "job-distributor.h"

namespace Base {


TEST(base_JobDistributor, simple_16)
{
  JobDistributor jd(16, 8);

  EXPECT_EQ(2, jd.pickNextJob());
  EXPECT_EQ(2, jd.pickNextJob());
  EXPECT_EQ(2, jd.pickNextJob());
  EXPECT_EQ(2, jd.pickNextJob());
  EXPECT_EQ(2, jd.pickNextJob());
  EXPECT_EQ(2, jd.pickNextJob());
  EXPECT_EQ(2, jd.pickNextJob());
  EXPECT_EQ(2, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
}


TEST(base_JobDistributor, simple_21)
{
  JobDistributor jd(21, 8);

  EXPECT_EQ(3, jd.pickNextJob());
  EXPECT_EQ(3, jd.pickNextJob());
  EXPECT_EQ(3, jd.pickNextJob());
  EXPECT_EQ(3, jd.pickNextJob());
  EXPECT_EQ(3, jd.pickNextJob());
  EXPECT_EQ(2, jd.pickNextJob());
  EXPECT_EQ(2, jd.pickNextJob());
  EXPECT_EQ(2, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
}


TEST(base_JobDistributor, simple_8)
{
  JobDistributor jd(8, 8);

  EXPECT_EQ(1, jd.pickNextJob());
  EXPECT_EQ(1, jd.pickNextJob());
  EXPECT_EQ(1, jd.pickNextJob());
  EXPECT_EQ(1, jd.pickNextJob());
  EXPECT_EQ(1, jd.pickNextJob());
  EXPECT_EQ(1, jd.pickNextJob());
  EXPECT_EQ(1, jd.pickNextJob());
  EXPECT_EQ(1, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
}


TEST(base_JobDistributor, simple_3)
{
  JobDistributor jd(3, 8);

  EXPECT_EQ(1, jd.pickNextJob());
  EXPECT_EQ(1, jd.pickNextJob());
  EXPECT_EQ(1, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
}


TEST(base_JobDistributor, simple_0)
{
  JobDistributor jd(0, 8);

  EXPECT_EQ(0, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
  EXPECT_EQ(0, jd.pickNextJob());
}


} // namespace Base
