#ifndef BASE_PERMUTATIONS_INL
#define BASE_PERMUTATIONS_INL

#include "permutations.h"

namespace Base {
namespace Private {

template<typename T>
QList< QList<T> >  addPermutation(const QList< QList<T> >& oldPermutations, const T& value)
{
  QList< QList<T> > mergedPermutations;

  if(oldPermutations.empty())
  {
    mergedPermutations.append(QList<T>({value}));
  }else
  {
    for(const QList<T>& oldPermutation : oldPermutations)
    {
      for(int i=0; i<=oldPermutation.size(); ++i)
      {
        QList<T> p = oldPermutation;

        p.insert(i, value);
        mergedPermutations.append(p);
      }
    }
  }

  return mergedPermutations;
}

}

template<typename T>
QList< QList<T> >  permutate(const QList<T>& values)
{
  QList< QList<T> > permutations;

  for(int i=values.size()-1; i>=0; --i)
    permutations = Private::addPermutation<T>(permutations, values[i]);

  return permutations;
}

} // namespace Base

#endif // BASE_PERMUTATION_INL
