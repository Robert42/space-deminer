#include <base/base-application.h>
#include <base/io/log.h>
#include <base/io/special-directories.h>
#include <base/strings/names.h>
#include <base/strings/string-id-manager.h>
#include <base/scripting/engine.h>
#include <base/code-generator/code-module-manager.h>
#include <base/types/type-manager.h>
#include <base/private/ogre-core.h>
#include <base/types/types.h>

namespace Base {

using IO::Log;

BaseApplication::BaseApplication(const String& applicationName)
  : _name(applicationName)
{
  Ogre::StringConverter::setUseLocale(true);

  if(!isValidApplicationName(applicationName))
    throw Ogre::InvalidParametersException(0,
                                           ("applicationName must only consist of the characters [-_.a-zA-Z0-9], but actually it is '"+applicationName+"'").toOgreString(),
                                           "ConfigDirectory::ConfigDirectory",
                                           __FILE__,
                                           __LINE__);

  if(PLATFORM_UNIX)
    this->_configDirectory = IO::SpecialDirectories::globalConfigDirectory() / ("." + applicationName);
  else
    this->_configDirectory = IO::SpecialDirectories::globalConfigDirectory() / applicationName;

  this->_configDirectory.createIfNotExisting();

  Log::init(configDirectory());

  IO::SpecialDirectories::logAllPaths();

  Log::log("Found config-directory: %0", this->configDirectory());
}

BaseApplication::~BaseApplication()
{
  this->trackable.clear();

  this->codeGeneratorModuleManager.reset();
  this->scriptingEngine.reset();

  this->ogreCore.reset();
}


void BaseApplication::startup()
{
  this->ogreCore = shared_ptr<Private::OgreCore>(new Private::OgreCore(configDirectory()));

  initTypes();

  this->scriptingEngine = shared_ptr<Base::Scripting::Engine>(new Scripting::Engine());

  this->codeGeneratorModuleManager = shared_ptr<CodeGenerator::ModuleManager>(new CodeGenerator::ModuleManager);
}


const IO::Directory& BaseApplication::configDirectory()
{
  return singleton()._configDirectory;
}


const String& BaseApplication::name()
{
  return singleton()._name;
}


const I18n::Language& BaseApplication::language()
{
  return singleton()._language;
}

void BaseApplication::setLanguage(const I18n::Language& language)
{
  singleton()._language = language;
  singleton()._signalLanguageChanged();
}

Signals::Signal<void()>& BaseApplication::signalLanguageChanged()
{
  return singleton()._signalLanguageChanged;
}


} // namespace Base
