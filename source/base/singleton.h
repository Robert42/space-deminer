#ifndef BASE_SINGLETON_H
#define BASE_SINGLETON_H

#include <dependencies.h>

namespace Base {


/** A base class for singletons
 *
 * This class allows only one instance of T at once. As soon as an instance of T is created,
 * the static method singletonPtr returns a pointer and singleton returns a reference to it.
 *
 * You have to create and destroy the instance of T by yourself, as itf it weren't a singleton.
 * Singleton<T> just provides an easy access to the instance.
 *
 * @note Note that the destructor is not virtual
 *
 * @note Note that the destructor will set the pointer to the previous singleton (or nullptr if
 * there wasn't one).
 * This mechanism is here to allow creating temporary Singletons for testing.
 * But you have to take care to destroy the singleton objects in the opposite order like
 * you have created it. It's a good idea to destroy singletons by using the system stack to make it
 * exception safe.
 */
template<class T>
class Singleton : public noncopyable
{
private:
  T* _previousSingletonPointer;

public:
  Singleton();
  ~Singleton();

protected:
  static T* singletonPtr();
  static T& singleton();

  bool isTheOnlyInstance();
  static int numberInstances();

  T* previousSingletonPointer();

private:
  static T*& writeableSingletonPointer();
};


/** A base class for singletons
 *
 * This class behaves like Singleton<T>. The only difference is, that the singleton getters
 * are public.
 */
template<class T>
class PublicSingleton : public Singleton<T>
{
public:
  static T* singletonPtr();
  static T& singleton();
};


/** A base class for singletons
 *
 * This class has a static method singleton, which returns a shared_ptr to the singleton instance.
 * If there's no instance yet, a new one is created. Note, that internal only a weak pointer is stored,
 * So the singelton instance gets destroayed as soon as you remove any reference to it.
 *
 * @note Note that T must have a constructor callable by SharedSingleton<T> with no arguments.
 * @note Note that the destructor is not virtual
 */
template<class T>
class SmartSingleton : noncopyable
{
public:
  SmartSingleton();
  ~SmartSingleton();

protected:
  static std::shared_ptr<T> singleton();
};


/** A base class for singletons
 *
 * This class has a static method singleton, which returns a shared_ptr to the singleton instance.
 * You have to set the instance manually using setSingleton. Note, that internal only a weak pointer is stored,
 * So the singelton instance gets destroayed as soon as you remove any reference to it.
 *
 * The difference between SmartSingleton and SharedSingleton is, that SmartSingleton creates autmatically an instance
 * if there isn't one, while for SharedSingleton, you have to set it manually using setSingleton
 *
 * @note Note that the destructor is not virtual
 */
template<class T>
class SharedSingleton : noncopyable
{
public:
  SharedSingleton();
  ~SharedSingleton();

protected:
  static std::shared_ptr<T> singleton();
  static std::shared_ptr<T> setSingleton(const std::shared_ptr<T>& instance);

private:
  static std::weak_ptr<T>& writeableWeakPointer();
};


/** A base class for singletons
 *
 * This class is used to have a global instance of the T without having even to think about
 * creating and destroying the object. The only thing to keep in mind to make sure, that your
 * implementation doesn't depend on BaseApplication, as it may be created before BaseApplication gets
 * creater or destroyed, after BaseAppliction gets destroyed.
*/
template<class T>
class BlackboxSingleton : noncopyable
{
protected:
  BlackboxSingleton();

  static T& singleton();
};


} // namespace Base

#include "singleton.inl"

#endif // BASE_SINGLETON_H
