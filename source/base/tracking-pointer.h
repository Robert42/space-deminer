#ifndef BASE_TRACKINGPOINTER_H
#define BASE_TRACKINGPOINTER_H

#include "signals/trackable.h"
#include "optional.h"

namespace Base {

template<typename T>
class TrackingPtr final : public Signals::Trackable::Listener
{
public:
  typedef Signals::Trackable::Listener ParentClass;

private:
  Optional<Signals::Trackable*> _trackable;
  Optional<T> _value;

public:
  explicit TrackingPtr(const T& value, Signals::Trackable& trackable);
  TrackingPtr();
  ~TrackingPtr();

  const Optional<T>& value() const;
  Optional<T> value();

  void reset();

private:
  void trackableGotDestroyed(Signals::Trackable* trackable) override;
};

} // namespace Base


#include "tracking-pointer.inl"

#endif // BASE_TRACKINGPOINTER_H
