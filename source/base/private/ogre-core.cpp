#include <dependencies.h>
#include <base/private/ogre-core.h>
#include <base/strings/string.h>


namespace Base {
namespace Private {


OgreCore::OgreCore(const IO::Directory& configDirectory)
{

  IO::Directory configFilePath = configDirectory / "engine-settings.cfg";

  this->root = new Ogre::Root("",
                              configFilePath.pathAsOgreString(),
                              "");
}


OgreCore::~OgreCore()
{
  delete this->root;
}


} // namespace Private
} // namespace Base
