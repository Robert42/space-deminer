#ifndef _FRAMEWORK_PRIVATE_OGRECORE_H_
#define _FRAMEWORK_PRIVATE_OGRECORE_H_

#include <base/io/directory.h>

namespace Base {
namespace Private {

/** This class holds the Ogre::Root object.
 *
 * @note there shouldn't be any reason to use this class directly.
 */
class OgreCore
{
private:
  Ogre::Root* root;

public:
  OgreCore(const IO::Directory& configDirectory);
  ~OgreCore();
};


} // namespace Private
} // namespace Base


#endif
