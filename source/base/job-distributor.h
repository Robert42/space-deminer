#ifndef BASE_JOBDISTRIBUTOR_H
#define BASE_JOBDISTRIBUTOR_H

#include <dependencies.h>

namespace Base {

class JobDistributor
{
private:
  int _jobLeft;
  int _workersLeft;

public:
  JobDistributor(int job, int workers);

  int pickNextJob();

  int jobLeft() const;
  int workersLeft() const;
};

} // namespace Base

#endif // BASE_JOBDISTRIBUTOR_H
