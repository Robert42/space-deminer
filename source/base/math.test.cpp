#include "math.h"

namespace Base {

template<typename T>
void testFloorCeilDivision()
{
  EXPECT_EQ(2, floorDivision(T(10), T(5)));
  EXPECT_EQ(2, floorDivision(T(11), T(5)));
  EXPECT_EQ(2, floorDivision(T(14), T(5)));
  EXPECT_EQ(3, floorDivision(T(15), T(5)));

  EXPECT_EQ(2, floorDivision(T(-14), T(-5)));
  EXPECT_EQ(-3, floorDivision(T(-14), T(5)));
  EXPECT_EQ(-3, floorDivision(T(14), T(-5)));

  EXPECT_EQ(2, ceilDivision(T(10), T(5)));
  EXPECT_EQ(3, ceilDivision(T(11), T(5)));
  EXPECT_EQ(3, ceilDivision(T(14), T(5)));
  EXPECT_EQ(3, ceilDivision(T(15), T(5)));

  EXPECT_EQ(3, ceilDivision(T(-14), T(-5)));
  EXPECT_EQ(-2, ceilDivision(T(-14), T(5)));
  EXPECT_EQ(-2, ceilDivision(T(14), T(-5)));
}

TEST(base_floorDivision, floorDivision)
{
  testFloorCeilDivision<int16>();
  testFloorCeilDivision<int>();
  testFloorCeilDivision<int32>();
  testFloorCeilDivision<int64>();
  testFloorCeilDivision<float>();
  testFloorCeilDivision<double>();
}



TEST(base_mod, mod)
{
  EXPECT_EQ(98, mod(-2, 100));
}



TEST(base_roundPositiveUpToMultipleOfPowerOfTwo, roundPositiveUpToMultipleOfPowerOfTwo)
{
  EXPECT_EQ(8, roundPositiveUpToMultipleOfPowerOfTwo(1,3));
  EXPECT_EQ(4, roundPositiveUpToMultipleOfPowerOfTwo(1,2));
  EXPECT_EQ(2, roundPositiveUpToMultipleOfPowerOfTwo(1,1));

  EXPECT_EQ(0, roundPositiveUpToMultipleOfPowerOfTwo(0,2));
  EXPECT_EQ(4, roundPositiveUpToMultipleOfPowerOfTwo(1,2));
  EXPECT_EQ(4, roundPositiveUpToMultipleOfPowerOfTwo(2,2));
  EXPECT_EQ(4, roundPositiveUpToMultipleOfPowerOfTwo(3,2));
  EXPECT_EQ(4, roundPositiveUpToMultipleOfPowerOfTwo(4,2));
  EXPECT_EQ(8, roundPositiveUpToMultipleOfPowerOfTwo(5,2));
}


TEST(base_bin_log_floor, bin_log_floor)
{
  EXPECT_EQ(0, bin_log_floor(uint32(0)));
  EXPECT_EQ(0, bin_log_floor(uint32(1)));
  EXPECT_EQ(1, bin_log_floor(uint32(2)));
  EXPECT_EQ(1, bin_log_floor(uint32(3)));
  EXPECT_EQ(2, bin_log_floor(uint32(4)));
  EXPECT_EQ(2, bin_log_floor(uint32(5)));
  EXPECT_EQ(2, bin_log_floor(uint32(6)));
  EXPECT_EQ(2, bin_log_floor(uint32(7)));
  EXPECT_EQ(3, bin_log_floor(uint32(8)));
  EXPECT_EQ(3, bin_log_floor(uint32(9)));

  for(uint32 x=1; x<=0x000fffff; ++x)
  {
    uint32 log = bin_log_floor(x);

    ASSERT_LE(1<<log, x) << "x: " << x << "\nlog: " << log;
    ASSERT_GT(1<<(log+1), x) << "x: " << x << "\nlog: " << log;
  }
}

TEST(bin_log_ceil, bin_log_ceil)
{
  EXPECT_EQ(0, bin_log_ceil(uint32(0)));
  EXPECT_EQ(0, bin_log_ceil(uint32(1)));
  EXPECT_EQ(1, bin_log_ceil(uint32(2)));
  EXPECT_EQ(2, bin_log_ceil(uint32(3)));
  EXPECT_EQ(2, bin_log_ceil(uint32(4)));
  EXPECT_EQ(3, bin_log_ceil(uint32(5)));
  EXPECT_EQ(3, bin_log_ceil(uint32(6)));
  EXPECT_EQ(3, bin_log_ceil(uint32(7)));
  EXPECT_EQ(3, bin_log_ceil(uint32(8)));
  EXPECT_EQ(4, bin_log_ceil(uint32(9)));

  for(uint32 x=2; x<=0x000fffff; ++x)
  {
    uint32 log = bin_log_ceil(x);

    ASSERT_GE(1<<log, x) << "x: " << x << "\nlog: " << log;
    ASSERT_LT(1<<(log-1), x) << "x: " << x << "\nlog: " << log;
  }
}

TEST(numberOfSetBits, numberOfSetBits)
{
  EXPECT_EQ(0, numberOfSetBits<int>(0));
  EXPECT_EQ(1, numberOfSetBits<int>(1));
  EXPECT_EQ(1, numberOfSetBits<int>(2));
  EXPECT_EQ(2, numberOfSetBits<int>(3));
  EXPECT_EQ(1, numberOfSetBits<int>(4));
  EXPECT_EQ(2, numberOfSetBits<int>(5));
  EXPECT_EQ(2, numberOfSetBits<int>(6));
  EXPECT_EQ(3, numberOfSetBits<int>(7));

  EXPECT_EQ(8, numberOfSetBits<int>(0xff0000));
  EXPECT_EQ(9, numberOfSetBits<int>(0xff0008));
  EXPECT_EQ(32, numberOfSetBits<int32>(-1));
}


} // namespace Base
