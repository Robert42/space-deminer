#include "layers.h"

namespace Base {
namespace Containers {

#define PREFIX "Base::Containers::Layers::"


BaseLayers::Layer::Layer(bool maskLayersBelow)
  : maskLayersBelow(maskLayersBelow),
    _allLayers(nullptr),
    globalVisibility(false),
    locallyVisible(false)
{
}


BaseLayers::Layer::~Layer()
{
  if(_allLayers != nullptr)
    _allLayers->removeLayer(this);
}


BaseLayers* BaseLayers::Layer::allLayers()
{
  return _allLayers;
}


void BaseLayers::Layer::setGlobalVisibility(bool globalVisibility)
{
  if(this->globalVisibility != globalVisibility)
  {
    this->globalVisibility = globalVisibility;

    if(this->globalVisibility)
      _signalGloballyShown();
    else
      _signalGloballyHidden();
  }
}


void BaseLayers::Layer::setVisible(bool visible)
{
  if(this->locallyVisible != visible)
  {
    this->locallyVisible = visible;

    if(this->locallyVisible)
      _signalShown();
    else
      _signalHidden();

    if(_allLayers)
      _allLayers->recalcGlobalVisibility();
  }
}


void BaseLayers::Layer::setMaskLayersBelow(bool maskLayersBelow)
{
  if(this->maskLayersBelow != maskLayersBelow)
  {
    this->maskLayersBelow = maskLayersBelow;

    if(_allLayers && isGloballyVisible())
      _allLayers->recalcGlobalVisibility();
  }
}


bool BaseLayers::Layer::isMaskingLayersBelow() const
{
  return this->maskLayersBelow;
}


bool BaseLayers::Layer::isGloballyVisible() const
{
  return this->globalVisibility;
}


bool BaseLayers::Layer::isLocallyVisible() const
{
  return this->locallyVisible;
}


void BaseLayers::Layer::show()
{
  setVisible(true);
}


void BaseLayers::Layer::hide()
{
  setVisible(false);
}


Signals::Signal<void()>& BaseLayers::Layer::signalHidden()
{
  return _signalHidden;
}


Signals::Signal<void()>& BaseLayers::Layer::signalShown()
{
  return _signalShown;
}


Signals::Signal<void()>& BaseLayers::Layer::signalGloballyHidden()
{
  return _signalGloballyHidden;
}


Signals::Signal<void()>& BaseLayers::Layer::signalGloballyShown()
{
  return _signalGloballyShown;
}



// ==== Layers ====

BaseLayers::BaseLayers()
  : signalVisibleLayersChanged(_signalVisibleLayersChanged)
{
}


BaseLayers::~BaseLayers()
{
  for(const Layer::Ptr& layer : _allLayers)
  {
    layer->setGlobalVisibility(false);
    layer->_allLayers = nullptr;
  }
}


const std::list<BaseLayers::Layer::Ptr>& BaseLayers::allLayers()
{
  return _allLayers;
}


const std::list<BaseLayers::Layer::Ptr>& BaseLayers::visibleLayers()
{
  return _globallyVisibleLayers;
}


void BaseLayers::pushTopLayer(const BaseLayers::Layer::Ptr& layer)
{
  if(layer->_allLayers == this)
    return;
  if(layer->_allLayers != nullptr)
    throw std::logic_error(PREFIX"pushTopLayer(): Trying to add a layer already assignled to another group of layers.");
  layer->_allLayers = this;
  layer->setGlobalVisibility(false);

  _allLayers.push_back(layer);

  recalcGlobalVisibility();
}


void BaseLayers::popTopLayer()
{
  if(_allLayers.empty())
    throw std::logic_error(PREFIX"popTopLayer(): Trying to pop the top layer, although no layer is added.");

  topLayer()->setGlobalVisibility(false);
  topLayer()->_allLayers = nullptr;

  _allLayers.pop_back();

  recalcGlobalVisibility();
}


BaseLayers::Layer::ConstPtr BaseLayers::topLayer() const
{
  if(_allLayers.empty())
    throw std::logic_error(PREFIX"getTopLayer(): Trying to get the top layer, although no layer is added.");
  return *_allLayers.rbegin();
}


const BaseLayers::Layer::Ptr& BaseLayers::topLayer()
{
  if(_allLayers.empty())
    throw std::logic_error(PREFIX"getTopLayer(): Trying to get the top layer, although no layer is added.");
  return *_allLayers.rbegin();
}


void BaseLayers::pushBottomLayer(const BaseLayers::Layer::Ptr& layer)
{
  if(layer->_allLayers == this)
    return;
  if(layer->_allLayers != nullptr)
    throw std::logic_error(PREFIX"pushBottomLayer(): Trying to add a layer already assignled to another group of layers.");
  layer->_allLayers = this;
  layer->setGlobalVisibility(false);

  _allLayers.push_front(layer);

  recalcGlobalVisibility();
}


void BaseLayers::popBottomLayer()
{
  if(_allLayers.empty())
    throw std::logic_error(PREFIX"popBottomLayer(): Trying to pop the bottom layer, although no layer is added.");

  bottomLayer()->setGlobalVisibility(false);
  bottomLayer()->_allLayers = nullptr;

  _allLayers.pop_front();

  recalcGlobalVisibility();
}


BaseLayers::Layer::ConstPtr BaseLayers::bottomLayer() const
{
  if(_allLayers.empty())
    throw std::logic_error(PREFIX"getBottomLayer(): Trying to get the bottom layer, although no layer is added.");
  return *_allLayers.begin();
}


const BaseLayers::Layer::Ptr& BaseLayers::bottomLayer()
{
  if(_allLayers.empty())
    throw std::logic_error(PREFIX"getBottomLayer(): Trying to get the bottom layer, although no layer is added.");
  return *_allLayers.begin();
}


void BaseLayers::removeLayer(Layer* layer)
{
  if(layer->_allLayers != this)
    throw std::logic_error(PREFIX"removeLayer(): Trying to remove a layer, although it wasn't added.");

  layer->setGlobalVisibility(false);
  layer->_allLayers = nullptr;

  _allLayers.remove_if([layer](const Layer::Ptr &l){return l.get()==layer;});

  recalcGlobalVisibility();
}


void BaseLayers::removeLayer(const Layer::Ptr& layer)
{
  removeLayer(layer.get());
}


BaseLayers::Layer::ConstPtr BaseLayers::topVisibleLayer() const
{
  if(!areAnyVisibleLayers())
    throw std::logic_error(PREFIX"getTopVisibleLayer(): Trying to get the visible top layer, although no layer is visible.");

  return *_globallyVisibleLayers.rbegin();
}


const BaseLayers::Layer::Ptr& BaseLayers::topVisibleLayer()
{
  if(!areAnyVisibleLayers())
    throw std::logic_error(PREFIX"getTopVisibleLayer(): Trying to get the visible top layer, although no layer is visible.");

  return *_globallyVisibleLayers.rbegin();
}


BaseLayers::Layer::ConstPtr BaseLayers::bottomVisibleLayer() const
{
  if(!areAnyVisibleLayers())
    throw std::logic_error(PREFIX"getTopVisibleLayer(): Trying to get the visible bottom layer, although no layer is visible.");

  return *_globallyVisibleLayers.begin();
}


const BaseLayers::Layer::Ptr& BaseLayers::bottomVisibleLayer()
{
  if(!areAnyVisibleLayers())
    throw std::logic_error(PREFIX"getTopVisibleLayer(): Trying to get the visible bottom layer, although no layer is visible.");

  return *_globallyVisibleLayers.begin();
}


bool BaseLayers::areAnyVisibleLayers() const
{
  return !_globallyVisibleLayers.empty();
}


void BaseLayers::recalcGlobalVisibility()
{
  std::list<Layer::Ptr> previousVisibility;

  _globallyVisibleLayers.swap(previousVisibility);

  std::list<Layer::Ptr>::reverse_iterator i=_allLayers.rbegin();

  for(; i!=_allLayers.rend(); ++i)
  {
    const Layer::Ptr& l = *i;

    if(!l->isLocallyVisible())
    {
      l->setGlobalVisibility(false);
      continue;
    }

    _globallyVisibleLayers.push_front(l);
    l->setGlobalVisibility(true);

    if(l->isMaskingLayersBelow())
    {
      ++i;

      for(; i!=_allLayers.rend(); ++i)
      {
        const Layer::Ptr& l = *i;

        l->setGlobalVisibility(false);
      }
      break;
    }
  }

  if(previousVisibility != _globallyVisibleLayers)
    _signalVisibleLayersChanged();
}



} // namespace Containers
} // namespace Base
