#include <base/containers/std-container-accessor.h>


namespace Base {
namespace Containers {


class ValueType
{
public:
  int value;

  ValueType(int v)
  {
    value = v;
  }

  bool isConst()
  {
    return false;
  }

  bool isConst() const
  {
    return true;
  }
};

template<typename StdContainerAccessor>
inline void testSmartPointerStdContainerAccessor(StdContainerAccessor& stdContainerAccessor)
{
  int count = 0;

  for(typename StdContainerAccessor::iterator i = stdContainerAccessor.begin(); i!=stdContainerAccessor.end(); ++i)
  {
    EXPECT_EQ(count, (*i).get()->value);
    EXPECT_EQ(count++, i->get()->value);
  }

  count = 9;
  for(typename StdContainerAccessor::reverse_iterator i = stdContainerAccessor.rbegin(); i!=stdContainerAccessor.rend(); ++i)
  {
    EXPECT_EQ(count, (*i).get()->value);
    EXPECT_EQ(count--, i->get()->value);
  }

  count = 0;

  for(typename StdContainerAccessor::const_iterator i = stdContainerAccessor.cbegin(); i!=stdContainerAccessor.cend(); ++i)
  {
    EXPECT_EQ(count, (*i).get()->value);
    EXPECT_EQ(count++, i->get()->value);
  }

  count = 9;
  for(typename StdContainerAccessor::const_reverse_iterator i = stdContainerAccessor.crbegin(); i!=stdContainerAccessor.crend(); ++i)
  {
    const shared_ptr<const ValueType>& ptr = *i;

    shared_ptr<const ValueType> ptr2 = ptr;

    const ValueType* vt = ptr2.get();

    EXPECT_EQ(count, vt->value);
    EXPECT_EQ(count, (*i).get()->value);
    EXPECT_EQ(count--, i->get()->value);
  }
}

template<typename StdContainerAccessor>
inline void testStdContainerAccessor(StdContainerAccessor& stdContainerAccessor)
{
  int count = 0;

  for(typename StdContainerAccessor::iterator i = stdContainerAccessor.begin(); i!=stdContainerAccessor.end(); ++i)
  {
    EXPECT_EQ(count, (*i).value);
    EXPECT_EQ(count++, i->value);
  }

  count = 9;
  for(typename StdContainerAccessor::reverse_iterator i = stdContainerAccessor.rbegin(); i!=stdContainerAccessor.rend(); ++i)
  {
    EXPECT_EQ(count, (*i).value);
    EXPECT_EQ(count--, i->value);
  }

  count = 0;

  for(typename StdContainerAccessor::const_iterator i = stdContainerAccessor.cbegin(); i!=stdContainerAccessor.cend(); ++i)
  {
    EXPECT_EQ(count, (*i).value);
    EXPECT_EQ(count++, i->value);
  }

  count = 9;
  for(typename StdContainerAccessor::const_reverse_iterator i = stdContainerAccessor.crbegin(); i!=stdContainerAccessor.crend(); ++i)
  {
    EXPECT_EQ(count, (*i).value);
    EXPECT_EQ(count--, i->value);
  }
}

TEST(base_contaienrs_StdContainerAccessor_shared_ptr, foreach_loop)
{
  std::list<shared_ptr<ValueType>> values;
  for(int i=0; i<10; ++i)
    values.push_back(shared_ptr<ValueType>(new ValueType(i)));

  {
    auto i = values.crbegin();
    const shared_ptr<const ValueType>& ptr = *i;
    shared_ptr<const ValueType> ptr2 = ptr;
    const ValueType* vt = ptr2.get();
    EXPECT_EQ(9, vt->value);
  }

  StdContainerAccessor<std::list<shared_ptr<ValueType>>> accessor(values);

  {
    auto i = accessor.crbegin();
    const shared_ptr<const ValueType>& ptr = *i;
    shared_ptr<const ValueType> ptr2 = ptr;
    const ValueType* vt = ptr2.get();
    EXPECT_EQ(9, vt->value);
  }

  testSmartPointerStdContainerAccessor(accessor);
}

TEST(base_contaienrs_StdContainerAccessor_simple, foreach_loop)
{
  std::list<ValueType> values;
  for(int i=0; i<10; ++i)
    values.push_back(i);

  StdContainerAccessor<std::list<ValueType>> accessor(values);

  testStdContainerAccessor(accessor);
}

TEST(base_contaienrs_StdContainerAccessor_shared_ptr, simple_test)
{
  std::list<shared_ptr<int>> container;
  StdContainerAccessor<std::list<shared_ptr<int>>> a(container);
}

TEST(base_contaienrs_StdContainerAccessor_shared_ptr, constness)
{
  std::list<shared_ptr<const int>> container;
  StdContainerAccessor<std::list<shared_ptr<const int>>> a(container);

  Private::StdSmartPointerContainerAccessor<std::list<shared_ptr<const int>>, shared_ptr<const int>, shared_ptr<const int>>& right_base_class = a;

  (void)right_base_class;
}


} // namespace Containers
} // namespace Base
