#ifndef BASE_CONTAINERS_LAYERS_INL
#define BASE_CONTAINERS_LAYERS_INL

#include "layers.h"

namespace Base {
namespace Containers {

template<class T>
Layers<T>::Layers()
{
  dirty = false;
  dirtyVisibility = false;
}

template<class T>
Layers<T>::~Layers()
{
}


template<class T>
const std::list<typename Layers<T>::LayerPtr>& Layers<T>::allLayers()
{
  update();
  return _allLayers;
}


template<class T>
const std::list<typename Layers<T>::LayerPtr>& Layers<T>::visibleLayers()
{
  update();
  return _globallyVisibleLayers;
}


template<class T>
void Layers<T>::pushTopLayer(const LayerPtr& layer)
{
  ParentClass::pushTopLayer(layer);
  this->dirty = true;
}


template<class T>
void Layers<T>::popTopLayer()
{
  ParentClass::popTopLayer();
  this->dirty = true;
}


template<class T>
typename Layers<T>::LayerConstPtr Layers<T>::topLayer() const
{
  return std::static_pointer_cast<T>(ParentClass::topLayer());
}


template<class T>
void Layers<T>::pushBottomLayer(const LayerPtr& layer)
{
  ParentClass::pushBottomLayer(layer);
  this->dirty = true;
}


template<class T>
void Layers<T>::popBottomLayer()
{
  ParentClass::popBottomLayer();
  this->dirty = true;
}


template<class T>
typename Layers<T>::LayerConstPtr Layers<T>::bottomLayer() const
{
  return std::static_pointer_cast<T>(ParentClass::bottomLayer());
}


template<class T>
typename Layers<T>::LayerPtr Layers<T>::topVisibleLayer()
{
  return std::static_pointer_cast<T>(ParentClass::topVisibleLayer());
}


template<class T>
typename Layers<T>::LayerConstPtr Layers<T>::topVisibleLayer() const
{
  return std::static_pointer_cast<const T>(ParentClass::topVisibleLayer());
}


template<class T>
typename Layers<T>::LayerPtr Layers<T>::bottomVisibleLayer()
{
  return std::static_pointer_cast<T>(ParentClass::bottomVisibleLayer());
}


template<class T>
typename Layers<T>::LayerConstPtr Layers<T>::bottomVisibleLayer() const
{
  return std::static_pointer_cast<const T>(ParentClass::bottomVisibleLayer());
}


template<class T>
bool Layers<T>::areAnyVisibleLayers() const
{
  return ParentClass::areAnyVisibleLayers();
}


template<class T>
void Layers<T>::removeLayer(Layer* layer)
{
  ParentClass::removeLayer(layer);
  this->dirty = true;
}


template<class T>
void Layers<T>::removeLayer(const LayerPtr& layer)
{
  ParentClass::removeLayer(layer);
  this->dirty = true;
}


template<class T>
void Layers<T>::recalcGlobalVisibility()
{
  ParentClass::recalcGlobalVisibility();

  this->dirtyVisibility = true;
}


template<class T>
void Layers<T>::update()
{
  if(dirty)
  {
    _allLayers.clear();

    for(const BaseLayers::Layer::Ptr& l : BaseLayers::allLayers())
      _allLayers.push_back(std::static_pointer_cast<T>(l));

    dirtyVisibility = true;
    dirty = false;
  }

  if(dirtyVisibility)
  {
    _globallyVisibleLayers.clear();

    for(const ParentClass::Layer::Ptr& l : ParentClass::visibleLayers())
      _globallyVisibleLayers.push_back(std::static_pointer_cast<T>(l));

    dirtyVisibility = false;
  }
}



} // namespace Containers
} // namespace Base


#endif // BASE_CONTAINERS_LAYERS_INL
