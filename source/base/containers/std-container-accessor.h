#ifndef _BASE_STD_CONTAINER_ACCESSOR_H_
#define _BASE_STD_CONTAINER_ACCESSOR_H_

#include <dependencies.h>

namespace Base {
namespace Containers {
namespace Private {
template<typename T_container_type, typename T_value_type>
class StdContainerAccessorBase
{
private:
  T_container_type& std_container;

public:
  typedef typename T_container_type::iterator iterator;
  typedef typename T_container_type::reverse_iterator reverse_iterator;
  typedef typename T_container_type::const_iterator const_iterator;
  typedef typename T_container_type::const_reverse_iterator const_reverse_iterator;

  typedef T_value_type value_type;

public:
  StdContainerAccessorBase(T_container_type& std_container)
    : std_container(std_container)
  {
  }

public:
  iterator begin()
  {
    return std_container.begin();
  }

  iterator end()
  {
    return std_container.end();
  }

  const_iterator begin() const
  {
    return std_container.begin();
  }

  const_iterator end() const
  {
    return std_container.end();
  }

  const_iterator cbegin() const
  {
    return std_container.begin();
  }

  const_iterator cend() const
  {
    return std_container.end();
  }

  reverse_iterator rbegin()
  {
    return std_container.rbegin();
  }

  reverse_iterator rend()
  {
    return std_container.rend();
  }

  const_reverse_iterator rbegin() const
  {
    return std_container.rbegin();
  }

  const_reverse_iterator rend() const
  {
    return std_container.rend();
  }

  const_reverse_iterator crbegin() const
  {
    return std_container.crbegin();
  }

  const_reverse_iterator crend() const
  {
    return std_container.crend();
  }
};

template<typename inner_iterator_type, typename ptr, typename const_ptr>
class SmartPtrConstIterator
{
public:
  typedef SmartPtrConstIterator<inner_iterator_type, ptr, const_ptr> this_type;
  typedef const_ptr value_type;

private:
  inner_iterator_type inner_iterator;

public:
  SmartPtrConstIterator(const inner_iterator_type& inner_iterator)
    : inner_iterator(inner_iterator)
  {
  }

public:
  bool operator==(const this_type& other) const
  {
    return this->inner_iterator == other.inner_iterator;
  }

  bool operator!=(const this_type& other) const
  {
    return this->inner_iterator != other.inner_iterator;
  }

  const const_ptr& operator*() const
  {
    return *operator->();
  }

  const const_ptr* operator->() const
  {
    return reinterpret_cast<const const_ptr*>(&*inner_iterator);
  }

  this_type& operator++()
  {
    ++inner_iterator;
    return *this;
  }

  this_type operator++(int)
  {
    this_type prev_value(inner_iterator);
    ++inner_iterator;
    return prev_value;
  }

  this_type& operator--()
  {
    --inner_iterator;
    return *this;
  }

  this_type operator--(int)
  {
    this_type prev_value(inner_iterator);
    --inner_iterator;
    return prev_value;
  }
};

template<typename T_container_type, typename ptr, typename const_ptr>
class StdSmartPointerContainerAccessor
{
private:
  T_container_type& std_container;

public:
  typedef ptr value_type;
  typedef typename T_container_type::iterator iterator;
  typedef typename T_container_type::reverse_iterator reverse_iterator;
  typedef SmartPtrConstIterator<typename T_container_type::const_iterator, ptr, const_ptr> const_iterator;
  typedef typename std::reverse_iterator<const_iterator> const_reverse_iterator;

protected:
  StdSmartPointerContainerAccessor(T_container_type& std_container)
    : std_container(std_container)
  {
  }

public:
  iterator begin()
  {
    return std_container.begin();
  }

  iterator end()
  {
    return std_container.end();
  }

  reverse_iterator rbegin()
  {
    return std_container.rbegin();
  }

  reverse_iterator rend()
  {
    return std_container.rend();
  }

  const_iterator begin() const
  {
    return std_container.begin();
  }

  const_iterator end() const
  {
    return std_container.end();
  }

  const_reverse_iterator rbegin() const
  {
    return std_container.rbegin();
  }

  const_reverse_iterator rend() const
  {
    return std_container.rend();
  }

  const_iterator cbegin() const
  {
    return std_container.cbegin();
  }

  const_iterator cend() const
  {
    return std_container.cend();
  }

  const_reverse_iterator crbegin() const
  {
    return std_container.crbegin();
  }

  const_reverse_iterator crend() const
  {
    return std_container.crend();
  }
};

template<typename T_container_type, typename T_value_type>
class StdContainerAccessorBase<T_container_type, shared_ptr<T_value_type>> : public StdSmartPointerContainerAccessor<T_container_type, shared_ptr<T_value_type>, shared_ptr<const T_value_type>>
{
public:
  StdContainerAccessorBase(T_container_type& std_container)
    : StdSmartPointerContainerAccessor<T_container_type, shared_ptr<T_value_type>, shared_ptr<const T_value_type>>(std_container)
  {
  }
};

template<typename T_container_type, typename T_value_type>
class StdContainerAccessorBase<T_container_type, weak_ptr<T_value_type>> : public StdSmartPointerContainerAccessor<T_container_type, weak_ptr<T_value_type>, weak_ptr<const T_value_type>>
{
public:
  StdContainerAccessorBase(T_container_type& std_container)
    : StdSmartPointerContainerAccessor<T_container_type, shared_ptr<T_value_type>, shared_ptr<const T_value_type>>(std_container)
  {
  }
};

template<typename T_container_type, typename T_value_type>
class StdContainerAccessorBase<T_container_type, unique_ptr<T_value_type>> : nonconstructable
{
};


} // namespace Private


template<typename container_type>
class StdContainerAccessor : public Private::StdContainerAccessorBase<container_type, typename container_type::value_type>, noncopyable
{
public:
  typedef Private::StdContainerAccessorBase<container_type, typename container_type::value_type> parent_class;

  typedef typename parent_class::value_type value_type;
  typedef typename parent_class::iterator iterator;
  typedef typename parent_class::const_iterator const_iterator;
  typedef typename parent_class::reverse_iterator reverse_iterator;
  typedef typename parent_class::const_reverse_iterator const_reverse_iterator;

  StdContainerAccessor(container_type& container)
    : parent_class(container)
  {
  }

  template<typename UnaryFunction>
  void for_each(UnaryFunction f)
  {
    for(value_type& v : *this)
      f(v);
  }

  template<typename UnaryFunction>
  void for_each(UnaryFunction f) const
  {
    for(const value_type& v : *this)
      f(v);
  }

  template<typename UnaryPredicate>
  bool all_of(UnaryPredicate p) const
  {
    return std::all_of(this->begin(), this->end(), p);
  }

  template<typename UnaryPredicate>
  bool any_of(UnaryPredicate p) const
  {
    return std::any_of(this->begin(), this->end(), p);
  }

  template<typename UnaryPredicate>
  bool none_of(UnaryPredicate p) const
  {
    return std::none_of(this->begin(), this->end(), p);
  }

  template<typename T>
  typename std::iterator_traits<const_iterator>::difference_type count(const T& value) const
  {
    return std::count(this->begin(), this->end(), value);
  }

  template<typename UnaryPredicate>
  typename std::iterator_traits<const_iterator>::difference_type count_if(UnaryPredicate p) const
  {
    return std::count_if(this->begin(), this->end(), p);
  }

  template<typename T>
  iterator find(const T& value)
  {
    return std::find(this->begin(), this->end(), value);
  }

  template<typename T>
  const_iterator find(const T& value) const
  {
    return std::find(this->begin(), this->end(), value);
  }

  template<typename UnaryPredicate>
  iterator find_if(UnaryPredicate p)
  {
    return std::find_if(this->begin(), this->end(), p);
  }

  template<typename UnaryPredicate>
  const_iterator find_if(UnaryPredicate p) const
  {
    return std::find_if(this->begin(), this->end(), p);
  }

  template<typename UnaryPredicate>
  iterator find_if_not(UnaryPredicate p)
  {
    return std::find_if_not(this->begin(), this->end(), p);
  }

  template<typename UnaryPredicate>
  const_iterator find_if_not(UnaryPredicate p) const
  {
    return std::find_if_not(this->begin(), this->end(), p);
  }
};


} // namespace Containers
} // namespace Base

namespace std {
template<typename inner_iterator_type, typename ptr, typename const_ptr>
class iterator_traits<Base::Containers::Private::SmartPtrConstIterator<inner_iterator_type, ptr, const_ptr>>
{
public:
  typedef std::ptrdiff_t difference_type;
  typedef const const_ptr value_type;
  typedef const const_ptr* pointer;
  typedef const const_ptr& reference;
  typedef bidirectional_iterator_tag iterator_category;
};
} // namespace std


#endif
