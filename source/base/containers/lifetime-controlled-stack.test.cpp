#include <dependencies.h>

#include "lifetime-controlled-stack.h"
#include <base/strings/string.h>

namespace Base {
namespace Containers {

inline void connectTestSignals(String& result, LifetimeControlledStack<String>& stack)
{
  Signals::Connection c = stack.signalTopValueChanged().connect([&](){
    if(stack.empty())
      result += "empty\n";
    else
      result += String::compose("'%0'\n", stack.topValue());
  });

  c.addTrackingType(Signals::Connection::TrackingType::SUPPRESS_WARNING);
}

TEST(base_containers_LifetimeControlledStack, empty_signals)
{
  String expectedResult = "'a'\nempty\n";
  String result;

  LifetimeControlledStack<String> stack;

  EXPECT_TRUE(stack.empty());

  connectTestSignals(result, stack);

  {
    auto p = stack.push("a");

    EXPECT_FALSE(stack.empty());
    EXPECT_EQ("a", stack.topValue());

    p.reset();
  }

  EXPECT_TRUE(stack.empty());
  EXPECT_EQ(expectedResult, result);
}

TEST(base_containers_LifetimeControlledStack, push_pop_signals)
{
  String expectedResult = "'a'\n'b'\n'a'\n'c'\n'a'\nempty\n'd'\nempty\n";
  String result;

  LifetimeControlledStack<String> stack;

  EXPECT_TRUE(stack.empty());

  connectTestSignals(result, stack);

  {
    auto a = stack.push("a");

    EXPECT_FALSE(stack.empty());
    EXPECT_EQ("a", stack.topValue());

    {
      auto b = stack.push("b");

      EXPECT_FALSE(stack.empty());
      EXPECT_EQ("b", stack.topValue());

      b.reset();
    }

    {
      auto c = stack.push("c");

      EXPECT_FALSE(stack.empty());
      EXPECT_EQ("c", stack.topValue());

      c.reset();
    }

    a.reset();
  }

  {
    auto d = stack.push("d");

    EXPECT_FALSE(stack.empty());
    EXPECT_EQ("d", stack.topValue());

    d.reset();
  }

  EXPECT_TRUE(stack.empty());
  EXPECT_EQ(expectedResult, result);
}


} // namespace Containers
} // namespace Base
