#include "layers.h"

namespace Base {
namespace Containers {

inline BaseLayers::Layer::Ptr createLayer(bool masking=false)
{
  return BaseLayers::Layer::Ptr(new BaseLayers::Layer(masking));
}



TEST(base_containers_Layers, push_layer_already_added)
{
  BaseLayers layers;

  BaseLayers::Layer::Ptr layer = createLayer();

  layers.pushTopLayer(layer);
  layers.pushTopLayer(layer);
  layers.pushBottomLayer(layer);

  EXPECT_EQ(1, layers.allLayers().size());
}


TEST(base_containers_Layers, push_layer_already_belonging_to_other_layers)
{
  BaseLayers layersA;
  BaseLayers layersB;

  BaseLayers::Layer::Ptr layer = createLayer();

  layersA.pushTopLayer(layer);

  EXPECT_ANY_THROW(layersB.pushTopLayer(layer));
}


TEST(base_containers_Layers, push_top)
{
  BaseLayers layers;

  std::list<BaseLayers::Layer::Ptr> expectedResult;

  BaseLayers::Layer::Ptr a = createLayer();
  BaseLayers::Layer::Ptr b = createLayer();
  BaseLayers::Layer::Ptr c = createLayer();

  expectedResult = {};
  EXPECT_EQ(expectedResult, layers.allLayers());
  EXPECT_EQ(nullptr, a->allLayers());

  layers.pushTopLayer(a);

  expectedResult = {a};
  EXPECT_EQ(expectedResult, layers.allLayers());
  EXPECT_EQ(&layers, a->allLayers());
  EXPECT_EQ(nullptr, b->allLayers());

  layers.pushTopLayer(b);

  expectedResult = {a, b};
  EXPECT_EQ(expectedResult, layers.allLayers());
  EXPECT_EQ(&layers, b->allLayers());
  EXPECT_EQ(nullptr, c->allLayers());

  layers.pushTopLayer(c);

  expectedResult = {a, b, c};
  EXPECT_EQ(expectedResult, layers.allLayers());
  EXPECT_EQ(&layers, c->allLayers());
}


TEST(base_containers_Layers, push_bottom)
{
  BaseLayers layers;

  std::list<BaseLayers::Layer::Ptr> expectedResult;

  BaseLayers::Layer::Ptr a = createLayer();
  BaseLayers::Layer::Ptr b = createLayer();
  BaseLayers::Layer::Ptr c = createLayer();

  expectedResult = {};
  EXPECT_EQ(expectedResult, layers.allLayers());
  EXPECT_EQ(nullptr, a->allLayers());

  layers.pushBottomLayer(a);

  expectedResult = {a};
  EXPECT_EQ(expectedResult, layers.allLayers());
  EXPECT_EQ(&layers, a->allLayers());
  EXPECT_EQ(nullptr, b->allLayers());

  layers.pushBottomLayer(b);

  expectedResult = {b, a};
  EXPECT_EQ(expectedResult, layers.allLayers());
  EXPECT_EQ(&layers, b->allLayers());
  EXPECT_EQ(nullptr, c->allLayers());

  layers.pushBottomLayer(c);

  expectedResult = {c, b, a};
  EXPECT_EQ(expectedResult, layers.allLayers());
  EXPECT_EQ(&layers, c->allLayers());
}


TEST(base_containers_Layers, push_visible_masking_top)
{
  int aHidden = 0;
  int aShown = 0;
  int bHidden = 0;
  int bShown = 0;

  BaseLayers::Layer::Ptr a = createLayer();
  BaseLayers::Layer::Ptr b = createLayer(true);

  a->setVisible(true);
  b->setVisible(true);

  a->signalGloballyShown().connect([&aShown](){++aShown;}).trackManually();
  a->signalGloballyHidden().connect([&aHidden](){++aHidden;}).trackManually();
  b->signalGloballyShown().connect([&bShown](){++bShown;}).trackManually();
  b->signalGloballyHidden().connect([&bHidden](){++bHidden;}).trackManually();

  BaseLayers layers;

  layers.pushTopLayer(a);

  EXPECT_EQ(0, aHidden);
  EXPECT_EQ(1, aShown);
  EXPECT_EQ(0, bHidden);
  EXPECT_EQ(0, bShown);

  layers.pushTopLayer(b);

  EXPECT_EQ(1, aHidden);
  EXPECT_EQ(1, aShown);
  EXPECT_EQ(0, bHidden);
  EXPECT_EQ(1, bShown);
}


TEST(base_containers_Layers, push_invisible_masking_top)
{
  int aHidden = 0;
  int aShown = 0;
  int bHidden = 0;
  int bShown = 0;

  BaseLayers::Layer::Ptr a = createLayer();
  BaseLayers::Layer::Ptr b = createLayer(true);

  a->setVisible(true);
  b->setVisible(false);

  a->signalGloballyShown().connect([&aShown](){++aShown;}).trackManually();
  a->signalGloballyHidden().connect([&aHidden](){++aHidden;}).trackManually();
  b->signalGloballyShown().connect([&bShown](){++bShown;}).trackManually();
  b->signalGloballyHidden().connect([&bHidden](){++bHidden;}).trackManually();

  BaseLayers layers;

  layers.pushTopLayer(a);

  EXPECT_EQ(0, aHidden);
  EXPECT_EQ(1, aShown);
  EXPECT_EQ(0, bHidden);
  EXPECT_EQ(0, bShown);

  layers.pushTopLayer(b);

  EXPECT_EQ(0, aHidden);
  EXPECT_EQ(1, aShown);
  EXPECT_EQ(0, bHidden);
  EXPECT_EQ(0, bShown);
}


TEST(base_containers_Layers, push_visible_masking_bottom)
{
  int aHidden = 0;
  int aShown = 0;
  int bHidden = 0;
  int bShown = 0;

  BaseLayers::Layer::Ptr a = createLayer();
  BaseLayers::Layer::Ptr b = createLayer(true);

  a->setVisible(true);
  b->setVisible(true);

  a->signalGloballyShown().connect([&aShown](){++aShown;}).trackManually();
  a->signalGloballyHidden().connect([&aHidden](){++aHidden;}).trackManually();
  b->signalGloballyShown().connect([&bShown](){++bShown;}).trackManually();
  b->signalGloballyHidden().connect([&bHidden](){++bHidden;}).trackManually();

  BaseLayers layers;

  layers.pushTopLayer(a);

  EXPECT_EQ(0, aHidden);
  EXPECT_EQ(1, aShown);
  EXPECT_EQ(0, bHidden);
  EXPECT_EQ(0, bShown);

  layers.pushBottomLayer(b);

  EXPECT_EQ(0, aHidden);
  EXPECT_EQ(1, aShown);
  EXPECT_EQ(0, bHidden);
  EXPECT_EQ(1, bShown);
}


TEST(base_containers_Layers, push_invisible_masking_bottom)
{
  int aHidden = 0;
  int aShown = 0;
  int bHidden = 0;
  int bShown = 0;

  BaseLayers::Layer::Ptr a = createLayer();
  BaseLayers::Layer::Ptr b = createLayer(true);

  a->setVisible(true);
  b->setVisible(false);

  a->signalGloballyShown().connect([&aShown](){++aShown;}).trackManually();
  a->signalGloballyHidden().connect([&aHidden](){++aHidden;}).trackManually();
  b->signalGloballyShown().connect([&bShown](){++bShown;}).trackManually();
  b->signalGloballyHidden().connect([&bHidden](){++bHidden;}).trackManually();

  BaseLayers layers;

  layers.pushTopLayer(a);

  EXPECT_EQ(0, aHidden);
  EXPECT_EQ(1, aShown);
  EXPECT_EQ(0, bHidden);
  EXPECT_EQ(0, bShown);

  layers.pushBottomLayer(b);

  EXPECT_EQ(0, aHidden);
  EXPECT_EQ(1, aShown);
  EXPECT_EQ(0, bHidden);
  EXPECT_EQ(0, bShown);
}


TEST(base_containers_Layers, push_visible_masking_blocked_bottom)
{
  int aHidden = 0;
  int aShown = 0;
  int bHidden = 0;
  int bShown = 0;

  BaseLayers::Layer::Ptr a = createLayer(true);
  BaseLayers::Layer::Ptr b = createLayer(true);

  a->setVisible(true);
  b->setVisible(true);

  a->signalGloballyShown().connect([&aShown](){++aShown;}).trackManually();
  a->signalGloballyHidden().connect([&aHidden](){++aHidden;}).trackManually();
  b->signalGloballyShown().connect([&bShown](){++bShown;}).trackManually();
  b->signalGloballyHidden().connect([&bHidden](){++bHidden;}).trackManually();

  BaseLayers layers;

  layers.pushTopLayer(a);

  EXPECT_EQ(0, aHidden);
  EXPECT_EQ(1, aShown);
  EXPECT_EQ(0, bHidden);
  EXPECT_EQ(0, bShown);

  layers.pushBottomLayer(b);

  EXPECT_EQ(0, aHidden);
  EXPECT_EQ(1, aShown);
  EXPECT_EQ(0, bHidden);
  EXPECT_EQ(0, bShown);
}


TEST(base_containers_Layers, push_invisible_masking_blocked_bottom)
{
  int aHidden = 0;
  int aShown = 0;
  int bHidden = 0;
  int bShown = 0;

  BaseLayers::Layer::Ptr a = createLayer(true);
  BaseLayers::Layer::Ptr b = createLayer(true);

  a->setVisible(true);
  b->setVisible(false);

  a->signalGloballyShown().connect([&aShown](){++aShown;}).trackManually();
  a->signalGloballyHidden().connect([&aHidden](){++aHidden;}).trackManually();
  b->signalGloballyShown().connect([&bShown](){++bShown;}).trackManually();
  b->signalGloballyHidden().connect([&bHidden](){++bHidden;}).trackManually();

  BaseLayers layers;

  layers.pushTopLayer(a);

  EXPECT_EQ(0, aHidden);
  EXPECT_EQ(1, aShown);
  EXPECT_EQ(0, bHidden);
  EXPECT_EQ(0, bShown);

  layers.pushBottomLayer(b);

  EXPECT_EQ(0, aHidden);
  EXPECT_EQ(1, aShown);
  EXPECT_EQ(0, bHidden);
  EXPECT_EQ(0, bShown);
}


TEST(base_containers_Layers, remove_masking_layer)
{
  int aHidden = 0;
  int aShown = 0;
  int bHidden = 0;
  int bShown = 0;

  BaseLayers::Layer::Ptr a = createLayer();
  BaseLayers::Layer::Ptr b = createLayer(true);

  a->setVisible(true);
  b->setVisible(true);

  a->signalGloballyShown().connect([&aShown](){++aShown;}).trackManually();
  a->signalGloballyHidden().connect([&aHidden](){++aHidden;}).trackManually();
  b->signalGloballyShown().connect([&bShown](){++bShown;}).trackManually();
  b->signalGloballyHidden().connect([&bHidden](){++bHidden;}).trackManually();

  BaseLayers layers;

  layers.pushTopLayer(a);

  EXPECT_EQ(0, aHidden);
  EXPECT_EQ(1, aShown);
  EXPECT_EQ(0, bHidden);
  EXPECT_EQ(0, bShown);

  layers.pushTopLayer(b);

  EXPECT_EQ(1, aHidden);
  EXPECT_EQ(1, aShown);
  EXPECT_EQ(0, bHidden);
  EXPECT_EQ(1, bShown);

  layers.removeLayer(b);

  EXPECT_EQ(1, aHidden);
  EXPECT_EQ(2, aShown);
  EXPECT_EQ(1, bHidden);
  EXPECT_EQ(1, bShown);
}


TEST(base_containers_Layers, show_and_hide_masking_layer)
{
  int aHidden = 0;
  int aShown = 0;
  int bHidden = 0;
  int bShown = 0;

  BaseLayers::Layer::Ptr a = createLayer();
  BaseLayers::Layer::Ptr b = createLayer(true);

  a->setVisible(true);
  b->setVisible(false);

  a->signalGloballyShown().connect([&aShown](){++aShown;}).trackManually();
  a->signalGloballyHidden().connect([&aHidden](){++aHidden;}).trackManually();
  b->signalGloballyShown().connect([&bShown](){++bShown;}).trackManually();
  b->signalGloballyHidden().connect([&bHidden](){++bHidden;}).trackManually();

  BaseLayers layers;

  layers.pushTopLayer(a);

  EXPECT_EQ(0, aHidden);
  EXPECT_EQ(1, aShown);
  EXPECT_EQ(0, bHidden);
  EXPECT_EQ(0, bShown);

  layers.pushTopLayer(b);

  EXPECT_EQ(0, aHidden);
  EXPECT_EQ(1, aShown);
  EXPECT_EQ(0, bHidden);
  EXPECT_EQ(0, bShown);

  b->setVisible(true);

  EXPECT_EQ(1, aHidden);
  EXPECT_EQ(1, aShown);
  EXPECT_EQ(0, bHidden);
  EXPECT_EQ(1, bShown);

  b->setVisible(false);

  EXPECT_EQ(1, aHidden);
  EXPECT_EQ(2, aShown);
  EXPECT_EQ(1, bHidden);
  EXPECT_EQ(1, bShown);
}


TEST(base_containers_Layers, destructor)
{
  int nHidden = 0;
  int nShown = 0;

  BaseLayers::Layer::Ptr a = createLayer();

  {
    BaseLayers layers;

    a->signalGloballyShown().connect([&nShown](){++nShown;}).trackManually();
    a->signalGloballyHidden().connect([&nHidden](){++nHidden;}).trackManually();

    EXPECT_EQ(0, nHidden);
    EXPECT_EQ(0, nShown);

    a->setVisible(true);

    EXPECT_EQ(0, nHidden);
    EXPECT_EQ(0, nShown);

    layers.pushTopLayer(a);

    EXPECT_EQ(0, nHidden);
    EXPECT_EQ(1, nShown);
  }

  EXPECT_EQ(1, nHidden);
  EXPECT_EQ(1, nShown);
}


TEST(base_containers_Layers, changing_global_visibility_when_getting_removed_from)
{
  BaseLayers layers;

  int nHidden = 0;
  int nShown = 0;

  BaseLayers::Layer::Ptr a = createLayer();

  a->signalGloballyShown().connect([&nShown](){++nShown;}).trackManually();
  a->signalGloballyHidden().connect([&nHidden](){++nHidden;}).trackManually();

  EXPECT_EQ(0, nHidden);
  EXPECT_EQ(0, nShown);

  a->setVisible(true);

  EXPECT_EQ(0, nHidden);
  EXPECT_EQ(0, nShown);

  layers.pushTopLayer(a);

  EXPECT_EQ(0, nHidden);
  EXPECT_EQ(1, nShown);

  layers.popBottomLayer();

  EXPECT_EQ(1, nHidden);
  EXPECT_EQ(1, nShown);

  layers.pushBottomLayer(a);

  EXPECT_EQ(1, nHidden);
  EXPECT_EQ(2, nShown);

  layers.popTopLayer();

  EXPECT_EQ(2, nHidden);
  EXPECT_EQ(2, nShown);

  layers.pushBottomLayer(a);

  EXPECT_EQ(2, nHidden);
  EXPECT_EQ(3, nShown);

  layers.removeLayer(a);

  EXPECT_EQ(3, nHidden);
  EXPECT_EQ(3, nShown);
}


} // namespace Containers
} // namespace Base
