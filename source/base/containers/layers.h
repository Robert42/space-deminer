#ifndef BASE_CONTAINERS_LAYERS_H
#define BASE_CONTAINERS_LAYERS_H

#include <base/signals.h>

namespace Base {
namespace Containers {

/**
     * Each Layer can be visible or not. If a layer is visible and has the `maskLayersBelow` flag set,
     * all layers below are globally invisible, even if their visible flag is set to `true`;
     */
class BaseLayers : noncopyable
{
public:

  class Layer : noncopyable
  {
    friend class BaseLayers;
  public:
    typedef std::shared_ptr<Layer> Ptr;
    typedef std::shared_ptr<const Layer> ConstPtr;

  private:
    Signals::CallableSignal<void()> _signalHidden, _signalShown;
    Signals::CallableSignal<void()> _signalGloballyHidden, _signalGloballyShown;

  private:
    bool maskLayersBelow;
    BaseLayers* _allLayers;
    bool globalVisibility;
    bool locallyVisible;

  public:
    Layer(bool maskLayersBelow=false);
    virtual ~Layer();

    bool isMaskingLayersBelow() const;
    bool isGloballyVisible() const;
    bool isLocallyVisible() const;
    void show();
    void hide();
    void setVisible(bool visible);
    void setMaskLayersBelow(bool maskLayersBelow);

    BaseLayers* allLayers();

    Signals::Signal<void()>& signalHidden();
    Signals::Signal<void()>& signalShown();
    Signals::Signal<void()>& signalGloballyHidden();
    Signals::Signal<void()>& signalGloballyShown();

  protected:
    void setGlobalVisibility(bool gobalVisibility);
  };

private:
  std::list<Layer::Ptr> _allLayers;
  std::list<Layer::Ptr> _globallyVisibleLayers;

  Signals::CallableSignal<void()> _signalVisibleLayersChanged;

public:
  Signals::Signal<void()>& signalVisibleLayersChanged;

public:
  BaseLayers();
  virtual ~BaseLayers();

  /** Returns a list containing all Layers.
       *
       * The returned list begins with the bottom most alyer and ends with the topmost.
       */
  const std::list<Layer::Ptr>& allLayers();

  /** Returns a list containing all globally visible Layers.
       *
       * The returned list begins with the bottom most alyer and ends with the topmost.
       */
  const std::list<Layer::Ptr>& visibleLayers();

  void pushTopLayer(const Layer::Ptr& layer);
  void popTopLayer();
  Layer::ConstPtr topLayer() const;
  const Layer::Ptr& topLayer();

  void pushBottomLayer(const Layer::Ptr& layer);
  void popBottomLayer();
  Layer::ConstPtr bottomLayer() const;
  const Layer::Ptr& bottomLayer();

  Layer::ConstPtr topVisibleLayer() const;
  const Layer::Ptr& topVisibleLayer();
  Layer::ConstPtr bottomVisibleLayer() const;
  const Layer::Ptr& bottomVisibleLayer();

  bool areAnyVisibleLayers() const;

  void removeLayer(Layer* layer);
  void removeLayer(const Layer::Ptr& layer);

protected:
  virtual void recalcGlobalVisibility();
};


template<class T>
class Layers : public BaseLayers
{
public:
  static_assert(std::is_base_of<BaseLayers::Layer, T>::value, "T must inherit from Base::Containers::BaseLayers::Layer");

  typedef T Layer;
  typedef std::shared_ptr<T> LayerPtr;
  typedef std::shared_ptr<const T> LayerConstPtr;
  typedef BaseLayers ParentClass;

private:
  std::list<LayerPtr> _allLayers;
  std::list<LayerPtr> _globallyVisibleLayers;
  bool dirty, dirtyVisibility;

public:
  Layers();
  ~Layers();

  /** Returns a list containing all Layers.
       *
       * The returned list begins with the bottom most alyer and ends with the topmost.
       */
  const std::list<LayerPtr>& allLayers();

  /** Returns a list containing all globally visible Layers.
       *
       * The returned list begins with the bottom most alyer and ends with the topmost.
       */
  const std::list<LayerPtr>& visibleLayers();

  void pushTopLayer(const LayerPtr& layer);
  void popTopLayer();
  LayerConstPtr topLayer() const;

  void pushBottomLayer(const LayerPtr& layer);
  void popBottomLayer();
  LayerConstPtr bottomLayer() const;

  LayerPtr topVisibleLayer();
  LayerConstPtr topVisibleLayer() const;
  LayerPtr bottomVisibleLayer();
  LayerConstPtr bottomVisibleLayer() const;

  bool areAnyVisibleLayers() const;

  void removeLayer(Layer* layer);
  void removeLayer(const LayerPtr& layer);

private:
  void recalcGlobalVisibility() final override;
  void update();
};



} // namespace Containers
} // namespace Base


#include "layers.inl"


#endif // BASE_CONTAINERS_LAYERS_H
