#ifndef _BASE_STATEMACHINES_LIFETIMECONTROLLEDSTACK_H_
#define _BASE_STATEMACHINES_LIFETIMECONTROLLEDSTACK_H_

#include <dependencies.h>

#include <base/optional.h>
#include <base/signals/signal.h>

namespace Base {
namespace Containers {

template<typename value_type>
class LifetimeControlledStack
{
public:
  class StackElement : noncopyable
  {
    friend class LifetimeControlledStack;
  public:
    typedef shared_ptr<StackElement> Ptr;
    typedef weak_ptr<StackElement> WeakPtr;

  private:
    LifetimeControlledStack& stack;
    const Ptr previous;

  public:
    value_type value;

    ~StackElement()
    {
      stack.setTopElement(previous);
    }

  private:
    StackElement(const value_type& value,
                 LifetimeControlledStack& stack,
                 const Ptr& previous)
      : stack(stack),
        previous(previous),
        value(value)
    {
    }
  };

private:
  typename StackElement::WeakPtr topElement;

  Signals::CallableSignal<void()> _signalTopValueChanged;

public:
  LifetimeControlledStack()
  {
  }

  ~LifetimeControlledStack()
  {
    assert(empty());
  }

  typename StackElement::Ptr push(const value_type& value)
  {
    typename StackElement::Ptr newTop(new StackElement(value,
                                                       *this,
                                                       topElement.lock()));

    setTopElement(newTop);

    return newTop;
  }

  Optional<value_type> top() const
  {
    if(empty())
      return nothing;

    typename StackElement::Ptr element = topElement.lock();

    return element->value;
  }

  const value_type& topValue() const
  {
    typename StackElement::Ptr element = topElement.lock();

    if(empty())
      throw std::logic_error("Called LifetimeControlledStack::topValue for an empty stack");

    return element->value;
  }

  bool empty() const
  {
    return topElement.expired();
  }

  Signals::Signal<void()>& signalTopValueChanged()
  {
    return _signalTopValueChanged;
  }

private:
  void setTopElement(const typename StackElement::Ptr& newTop)
  {
    topElement = newTop;

    _signalTopValueChanged();
  }
};


} // namespace Containers
} // namespace Base

#endif
