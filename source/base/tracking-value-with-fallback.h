#ifndef BASE_TRACKINGVALUEWITHFALLBACK_H
#define BASE_TRACKINGVALUEWITHFALLBACK_H

#include "signals/trackable.h"
#include "optional.h"

namespace Base {

template<typename T>
class FallbackValueForType
{
public:
  static T value()
  {
    return T();
  }
};

template<typename T>
class DefaultFallbackValueForType
{
public:
  static T value()
  {
    return FallbackValueForType<T>::value();
  }
};

template<typename T>
class DefaultFallbackValueForType< std::shared_ptr<T> >
{
public:
  static std::shared_ptr<T> value()
  {
    return T::createFallback();
  }
};


template<typename T, class Fallback=DefaultFallbackValueForType<T> >
class TrackingValueWithFallback final : public Signals::Trackable::Listener
{
public:
  T value;

public:
  TrackingValueWithFallback()
    : value(Fallback::value())
  {
  }

  TrackingValueWithFallback(const T& value)
    : value(value)
  {
  }

  void track(Signals::Trackable& trackable)
  {
    this->startListeningToTrackable(&trackable);
  }

  void reset()
  {
    value = Fallback::value();
  }

private:
  void trackableGotDestroyed(Signals::Trackable*) override
  {
    reset();
  }
};

} // namespace Base

#endif // BASE_TRACKINGVALUEWITHFALLBACK_H
