#ifndef _BASE_THREADS_H_
#define _BASE_THREADS_H_

#include <dependencies.h>

#define SPACEDEMINER_SUPPRESS_MULTITHREADING 1

#if SPACEDEMINER_SUPPRESS_MULTITHREADING
#ifdef SPACEDEMINER_MULTITHREADING
#undef SPACEDEMINER_MULTITHREADING
#endif
#define SPACEDEMINER_MULTITHREADING 0
#endif

namespace Base {


/** Stops the current thread until the predicate returns false.
 *
 * @param predicate as soon as this method returns false, the method returns
 * @param maxWaitTime if waiting takes more time than this value in seconds, an exception gets thrown
 * @param updateRate the time between two checks using the predicate
 */
void sleepWhile(const std::function<bool()>& predicate,
                const std::chrono::milliseconds& maxWaitTime = std::chrono::milliseconds(1000),
                const std::chrono::microseconds& updateRate = std::chrono::microseconds(50000));


/** Stops the current thread for the given duraction
 *
 * @param sleepTime
 */
void sleepFor(const std::chrono::microseconds& sleepTime);

#if SPACEDEMINER_MULTITHREADING
/**
 * @note: Never run this method from one of the threads, as the array of threads is static
 */
template<typename JobType, typename Functor>
void executeMultithreaded(JobType job, int count, const Functor& function)
{
#ifndef NDEBUG
  static bool isUsed = false;
  static std::mutex mutex;

  mutex.lock();
  assert(!isUsed); // In debug mode, ensure this method never is going to be executeed from one of its threads
  isUsed = true;
  mutex.unlock();
#endif

  static std::vector<std::thread> threads;

  int nThreads = threads.size();

  if(threads.size() == 0)
  {
    nThreads = max<int>(1, std::thread::hardware_concurrency());
    assert(nThreads > 0);

    threads.resize(nThreads);
  }

  int jobBulk = (count + nThreads - 1) / nThreads;

  for(std::thread& thread : threads)
  {
    int currentCount = min(jobBulk, count);
    assert(currentCount >= 0);

    std::thread currentThread(std::bind(function, job, currentCount));
    thread.swap(currentThread);

    job += currentCount;
    count -= currentCount;
  }

  assert(count == 0);

  for(std::thread& thread : threads)
    thread.join();

#ifndef NDEBUG
  mutex.lock();
  isUsed = false;
  mutex.unlock();
#endif
}
#else
template<typename JobType, typename Functor>
void executeMultithreaded(JobType job, int count, const Functor& function)
{
  function(job, count);
}
#endif


} // namespace Base

#endif
