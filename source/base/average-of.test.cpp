#include <dependencies.h>

#include <base/average-of.h>

namespace Base {


TEST(Base_AverageOf, simple)
{
  AverageOf<real> average(3);

  EXPECT_EQ(42.f, average.calcAverage(42));

  average.addValue(0);
  EXPECT_EQ(0.f, average.calcAverage(42));

  average.addValue(4);
  EXPECT_EQ(2.f, average.calcAverage(42));

  average.addValue(5);
  EXPECT_EQ(3.f, average.calcAverage(42));

  average.addValue(3);
  EXPECT_EQ(4.f, average.calcAverage(42));

  average.addValue(1);
  EXPECT_EQ(3.f, average.calcAverage(42));
}


} // namespace Base

