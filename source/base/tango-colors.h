#ifndef _BASE_TANGO_COLORS_H_
#define _BASE_TANGO_COLORS_H_

#include <dependencies.h>

namespace Base {
namespace Tango {


extern const vec4 Butter_Dark;
extern const vec4 Butter_Middle;
extern const vec4 Butter_Light;
extern const vec4 Orange_Dark;
extern const vec4 Orange_Middle;
extern const vec4 Orange_Light;
extern const vec4 Chocolate_Dark;
extern const vec4 Chocolate_Middle;
extern const vec4 Chocolate_Light;
extern const vec4 Chameleon_Dark;
extern const vec4 Chameleon_Middle;
extern const vec4 Chameleon_Light;
extern const vec4 SkyBlue_Dark;
extern const vec4 SkyBlue_Middle;
extern const vec4 SkyBlue_Light;
extern const vec4 Plum_Dark;
extern const vec4 Plum_Middle;
extern const vec4 Plum_Light;
extern const vec4 ScarletRed_Dark;
extern const vec4 ScarletRed_Middle;
extern const vec4 ScarletRed_Light;
extern const vec4 AluminiumLight_Dark;
extern const vec4 AluminiumLight_Middle;
extern const vec4 AluminiumLight_Light;
extern const vec4 AluminiumDark_Dark;
extern const vec4 AluminiumDark_Middle;
extern const vec4 AluminiumDark_Light;
extern const vec4 GreyLight_Dark;
extern const vec4 GreyLight_Middle;
extern const vec4 GreyLight_Light;
extern const vec4 GreyDark_Dark;
extern const vec4 GreyDark_Middle;
extern const vec4 GreyDark_Light;

extern const vec4 Turquoise_Dark;
extern const vec4 Turquoise_Middle;
extern const vec4 Turquoise_Light;


} // namespace Tango
} // namespace Base

#endif
