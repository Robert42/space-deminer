#ifndef BASE_STATEMACHINES_FUNCTIONPOINTERBASED_FINALSTATEMACHINE_H
#define BASE_STATEMACHINES_FUNCTIONPOINTERBASED_FINALSTATEMACHINE_H

#include <base/signals/trackable.h>

namespace Base {
namespace StateMachines {
namespace FunctionPointerBased {

/**
 * This class is mostly taken the book Game Programming Gems 3
 * Section 3: Article: "Function Pointer-Based, Embedded Finite-State Machines"
 * From Charles Farris, VR1 Entertainment, Inc.
 *
 * It has been adapted to our conventions.
 */

class State
{
public:
  virtual ~State(){}
  virtual void executeBeginState() = 0;
  virtual void executeState() = 0;
  virtual void executeEndState() = 0;
};


/**
 * We've decided to allow te expect specific inherits of the State class, to make it more easy to add states with own overriden methods.
 */
template<class T_state = State>
class FinalStateMachine
{
public:
  typedef T_state state_type;
  typedef FinalStateMachine<state_type> this_type;

  static_assert(std::is_base_of<State, state_type>::value, "T_state must inherit State");

  template<class T>
  class StateTemplate : public state_type
  {
  protected:
    typedef void (T::*PFNSTATE)();
    T* instance;
    PFNSTATE beginState;
    PFNSTATE state;
    PFNSTATE endState;
  public:
    StateTemplate()
      : instance(nullptr),
        beginState(nullptr),
        endState(nullptr)
    {
    }

    void set(T* instance, PFNSTATE beginState, PFNSTATE state, PFNSTATE endState)
    {
      this->instance = instance;
      this->beginState = beginState;
      this->state = state;
      this->endState = endState;
    }

    void executeBeginState() override
    {
      assert(this->beginState != nullptr);

      (instance->*beginState)();
    }

    void executeState() override
    {
      assert(this->state != nullptr);

      (instance->*state)();
    }

    void executeEndState() override
    {
      assert(this->endState != nullptr);

      (instance->*endState)();
    }
  };

protected:
  state_type* _currentState;
  state_type* _newState;

public:
  FinalStateMachine(state_type* stateInitial)
  {
    _currentState = stateInitial;
    _newState = nullptr;
  }

  virtual ~FinalStateMachine()
  {
  }

  /** The update method updates the state and calls the executeState method.
   * Call this function every frame.
   */
  virtual void update()
  {
    updateState();

    currentState().executeState();
  }

  bool isState(state_type& state) const
  {
    return this->_currentState == &state;
  }

  bool gotoState(state_type& newState)
  {
    this->_newState = &newState;
    return true;
  }

  state_type& currentState()
  {
    return *this->_currentState;
  }

  const state_type& currentState() const
  {
    return *this->_currentState;
  }

  void updateState()
  {
    if(_newState && _newState!=_currentState)
    {
      _currentState->executeEndState();
      _currentState = _newState;
      _newState = nullptr;
      _currentState->executeBeginState();
    }
  }
};

} // namespace FunctionPointerBased
} // namespace StateMachines
} // namespace Base

#endif // BASE_STATEMACHINES_FUNCTIONPOINTERBASED_FINALSTATEMACHINE_H
