#include "code-language.h"
#include "source-code.h"

#include <base/io/log.h>
#include <base/types/types.h>

namespace Base {
namespace CodeGenerator {

inline std::string formatSourceCode(Language language,
                                    const std::function<void(SourceCode&, const LanguageSourceCodeHelper::Ptr&)>& sourceCodeGeneration)
{
  SourceCode sourceCode;
  LanguageSourceCodeHelper::Ptr sourceCodeHelper = LanguageSourceCodeHelper::create(language, &sourceCode);

  sourceCodeGeneration(sourceCode, sourceCodeHelper);

  return sourceCode.fullSourceCode;
}

inline std::string formatFunctionDeclaration(Language language, const FunctionDeclaration& functionDeclaration)
{
  return formatSourceCode(language,
                          [&functionDeclaration](SourceCode&, const LanguageSourceCodeHelper::Ptr& helper){
    helper->declareFunction(functionDeclaration);
  });
}

TEST(base_codegenerator_LanguageSourceCodeHelper, cpp_function_signature_declaration)
{
  typedef FunctionType::ArgumentType ArgType;

  FunctionDeclaration noReturnNoArgument(FunctionType(), "test", {});

  EXPECT_EQ("void test();", formatFunctionDeclaration(Language::CPP, noReturnNoArgument));
  EXPECT_EQ("void test();", formatFunctionDeclaration(Language::CPP_WITH_FRAMEWORK, noReturnNoArgument));
  EXPECT_EQ("void test();", formatFunctionDeclaration(Language::ANGLESCRIPT, noReturnNoArgument));
  EXPECT_EQ("#version 120 core  // OpenGL Version 2.1\n"
            "void test();", formatFunctionDeclaration(Language::GLSL_120, noReturnNoArgument));
  EXPECT_EQ("#version 430 core  // OpenGL Version 4.3\n"
            "void test();", formatFunctionDeclaration(Language::GLSL_430, noReturnNoArgument));

  FunctionDeclaration returnVec2MultipleArgumentReferences(FunctionType(Types::vec2, {ArgType(Types::_int, ArgType::ReferenceType::IN),
                                                                                      ArgType(Types::_bool, ArgType::ReferenceType::OUT),
                                                                                      ArgType(Types::_float, ArgType::ReferenceType::INOUT),
                                                                                      ArgType(Types::vec4)}),
                                                           "test", {"a", "b", "c", "d"});

  EXPECT_EQ("vec2 test(const int& a, bool& b, float& c, vec4 d);", formatFunctionDeclaration(Language::CPP, returnVec2MultipleArgumentReferences));
  EXPECT_EQ("vec2 test(const int& a, const Output< bool >& b, const InOutput< float >& c, vec4 d);", formatFunctionDeclaration(Language::CPP_WITH_FRAMEWORK, returnVec2MultipleArgumentReferences));
  EXPECT_EQ("vec2 test(const int &in a, bool &out b, float &inout c, vec4 d);", formatFunctionDeclaration(Language::ANGLESCRIPT, returnVec2MultipleArgumentReferences));
  EXPECT_EQ("#version 120 core  // OpenGL Version 2.1\n"
            "vec2 test(in int a, out bool b, inout float c, vec4 d);", formatFunctionDeclaration(Language::GLSL_120, returnVec2MultipleArgumentReferences));
  EXPECT_EQ("#version 430 core  // OpenGL Version 4.3\n"
            "vec2 test(in int a, out bool b, inout float c, vec4 d);", formatFunctionDeclaration(Language::GLSL_430, returnVec2MultipleArgumentReferences));
}


} // namespace CodeGenerator
} // namespace Base
