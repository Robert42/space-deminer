#ifndef BASE_CODEGENERATOR_CODEFUNCTIONSIGNATURE_H
#define BASE_CODEGENERATOR_CODEFUNCTIONSIGNATURE_H

#include <base/types/function-type.h>

namespace Base {
namespace CodeGenerator {

class FunctionSignature
{
protected:
  FunctionType _functionType;
  std::string _name;

public:
  FunctionSignature(const FunctionType& functionType, const std::string& name);

  const FunctionType& functionType() const;
  const std::string& name() const;
};

class FunctionDeclaration : public FunctionSignature
{
private:
  QVector<std::string> _argumentNames;

public:
  FunctionDeclaration(const FunctionType& functionType, const std::string& name, const QVector<std::string>& argumentNames);

  const QVector<std::string>& argumentNames() const;
};

} // namespace CodeGenerator
} // namespace Base

#endif // BASE_CODEGENERATOR_CODEFUNCTIONSIGNATURE_H
