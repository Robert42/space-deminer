#ifndef BASE_CODEGENERATOR_CODELANGUAGE_H
#define BASE_CODEGENERATOR_CODELANGUAGE_H

#include <dependencies.h>

#include <base/strings/string.h>
#include <base/enum-macros.h>

#include "code-function-signature.h"

namespace Base {
namespace CodeGenerator {

BEGIN_ENUMERATION(Language, :uint32)
  NONE = 0,
  CPP = 1,
  CPP_WITH_FRAMEWORK = CPP<<1,
  ANGLESCRIPT = CPP_WITH_FRAMEWORK<<1,
  GLSL_120 = ANGLESCRIPT<<1, // OpenGL 2.1 -- see http://de.wikipedia.org/wiki/GLSL
  GLSL_130 = GLSL_120<<1, // OpenGL 3.0
  GLSL_140 = GLSL_130<<1, // OpenGL 3.1
  GLSL_150 = GLSL_140<<1, // OpenGL 3.2
  GLSL_330 = GLSL_150<<1,
  GLSL_400 = GLSL_330<<1,
  GLSL_410 = GLSL_400<<1,
  GLSL_420 = GLSL_410<<1,
  GLSL_430 = GLSL_420<<1,
  GLSL_440 = GLSL_430<<1,

  GLSL_3xx = GLSL_130 | GLSL_140 | GLSL_150 | GLSL_330,
  GLSL_4xx = GLSL_400 | GLSL_410 | GLSL_420 | GLSL_430 | GLSL_440,
  GLSL_WITHOUT_120 = GLSL_3xx | GLSL_4xx,
  GLSL = GLSL_120 | GLSL_WITHOUT_120,
  ALL = CPP | ANGLESCRIPT | GLSL,
ENUMERATION_BASIC_OPERATORS(Language)
ENUMERATION_FLAG_OPERATORS(Language)
ENUM_TO_STRING_DECLARATION(Language)
  static std::string to_string_throwingException(Language language);

  static Language parseStringThrowingException(const std::string& string);
  static Language parseStringThrowingException(const String& string);

  static Language parseString(const std::string& string);
  static Language parseString(const String& string);
END_ENUMERATION;



class SourceCode;
class LanguageSourceCodeHelper
{
public:
  typedef std::shared_ptr<LanguageSourceCodeHelper> Ptr;

protected:
  SourceCode& sourceCode;

public:
  LanguageSourceCodeHelper(SourceCode* sourceCode);

  static Ptr create(Language language, SourceCode* sourceCode);

  virtual void beginSourceCode(){}
  virtual void endSourceCode(){}

  void declareFunction(const FunctionDeclaration& functionDeclaration, bool suppressSemicolon=false);
  void beginFunction(const FunctionDeclaration& functionDeclaration);
  void endFunction();

protected:
  virtual void _writeArgumentType(const FunctionType::ArgumentType& argumentType) = 0;

private:
  void _declareFunctionWithCustomArgumentNames(const FunctionSignature& functionSignature,
                                               const std::function<void(int, SourceCode&)>& customArgumentName);
};

} // namespace CodeGenerator
} // namespace Base

#endif // BASE_CODEGENERATOR_CODELANGUAGE_H
