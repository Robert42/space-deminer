#include "code-module.h"
#include "code-module-manager.h"
#include <base/io/log.h>

#include "code-source-file-loader.h"
#include "source-code.h"

namespace Base {
namespace CodeGenerator {

class IncludedModuleSection : public Module::Section
{
public:
  typedef std::shared_ptr<IncludedModuleSection> Ptr;

private:
  Module::Ptr module;

private:
  IncludedModuleSection(const Module::Ptr& module, Language language)
    : Section(language),
      module(module)
  {
  }

public:
  static Ptr create(const Module::Ptr& module, Language language)
  {
    return Ptr(new IncludedModuleSection(module, language));
  }

public:
  bool includesModule(Module* module) override
  {
    return (this->module.getPointer()==module) || this->module->includesModule(module);
  }

  Ogre::String label() const override
  {
    return module->getName();
  }

  void loadImpl() override
  {
    module->load();
  }

  void unloadImpl() override
  {
    module->unload();
  }

  size_t size() const override
  {
    return sizeof(*this);
  }

  bool alreadyIncluded(SourceCode& sourceCode)
  {
    Module* m = module.getPointer();

    if(sourceCode.alreadyIncludedModules.find(m) != sourceCode.alreadyIncludedModules.end())
      return true;

    sourceCode.alreadyIncludedModules.insert(m);

    return false;
  }

  void generateSourceCodeOverview(const InOutput<SourceCode>& sourceCodeOut) override
  {
    SourceCode& sourceCode = sourceCodeOut.value;

    if(alreadyIncluded(sourceCode) || !sourceCode.supportsLanguage(module->supportedLanguages()))
      return;

    module->generateSourceCodeOverview(sourceCodeOut);
  }

  void generateSourceCode(const InOutput<SourceCode>& sourceCodeOut) override
  {
    SourceCode& sourceCode = sourceCodeOut.value;

    if(alreadyIncluded(sourceCode))
      return;

    module->generateSourceCode(sourceCodeOut);
  }
};

class IncludedSourceFileSection : public Module::Section
{
public:
  typedef std::shared_ptr<IncludedSourceFileSection> Ptr;

private:
  Ogre::String fileName;
  Ogre::String sourceCode;

private:
  IncludedSourceFileSection(const Ogre::String& fileName, Language language)
    : Section(language),
      fileName(fileName)
  {
  }

public:
  static Ptr create(const Ogre::String& fileName, Language language)
  {
    return Ptr(new IncludedSourceFileSection(fileName, language));
  }

  Ogre::String label() const override
  {
    return fileName;
  }

  void loadImpl() override
  {
    sourceCode = SourceFileLoader::loadFile(fileName);
  }

  void unloadImpl() override
  {
    sourceCode.clear();
  }

  size_t size() const override
  {
    return sizeof(*this) + fileName.length() + sourceCode.length();
  }

  void generateSourceCodeOverview(const InOutput<SourceCode>& sourceCode) override
  {
    sourceCode.value.beginOverview("File: "+fileName);
    sourceCode.value.endOverview();
  }

  void generateSourceCode(const InOutput<SourceCode>& sourceCode) override
  {
    sourceCode.value.beginSection("File "+fileName, "---- SourceFile <"+fileName+"> --------");
    sourceCode.value.write(this->sourceCode);
    sourceCode.value.write("// -- End Source File <"+fileName+">\n");
  }
};

class IncludedSourceCodeSection : public Module::Section
{
public:
  typedef std::shared_ptr<IncludedSourceCodeSection> Ptr;

private:
  Ogre::String _label;
  Ogre::String sourceCode;

private:
  IncludedSourceCodeSection(const Ogre::String& label, const Ogre::String& sourceCode, Language language)
    : Section(language),
      _label(label),
      sourceCode(sourceCode)
  {
  }

public:
  static Ptr create(const Ogre::String& label, const Ogre::String& sourceCode, Language language)
  {
    return Ptr(new IncludedSourceCodeSection(label, sourceCode, language));
  }

  Ogre::String label() const override
  {
    return _label;
  }

  void loadImpl() override
  {
  }

  void unloadImpl() override
  {
  }

  size_t size() const override
  {
    return sizeof(*this) + label().length() + sourceCode.length();
  }

  void generateSourceCodeOverview(const InOutput<SourceCode>& sourceCode) override
  {
    sourceCode.value.beginOverview("Inline Code: "+label());
    sourceCode.value.endOverview();
  }

  void generateSourceCode(const InOutput<SourceCode>& sourceCode) override
  {
    sourceCode.value.beginSection("Inline SourceCode "+label(), "---- Inline SourceCode "+label()+"> ----");
    sourceCode.value.write(this->sourceCode);
    sourceCode.value.write("// -- End Source Code "+label()+"\n");
  }
};

class IncludedTemplateSourceFileSection : public Module::Section
{
public:
  typedef std::shared_ptr<IncludedTemplateSourceFileSection> Ptr;

private:
  Ogre::String fileName;
  Ogre::String templateText;
  Ogre::StringVectorPtr templateReplacements;
  Ogre::String sourceCode;

  IncludedTemplateSourceFileSection(const Ogre::String& filename,
                                    const Ogre::String& templateText,
                                    const Ogre::StringVectorPtr& replacements,
                                    Language supportedLanguages)
    : Section(supportedLanguages),
      fileName(filename),
      templateText(templateText),
      templateReplacements(replacements)
  {
  }

public:
  static Ptr create(const Ogre::String& filename, const Ogre::String& templateText, const Ogre::StringVectorPtr& replacements, Language supportedLanguages)
  {
    return Ptr(new IncludedTemplateSourceFileSection(filename, templateText, replacements, supportedLanguages));
  }

  Ogre::String label() const override
  {
    return fileName;
  }

  void loadImpl() override
  {
    Ogre::String templateCode = SourceFileLoader::loadFile(fileName);

    for(const Ogre::String& replacement : *templateReplacements)
    {
      Ogre::String t = templateCode;

      boost::replace_all(t, templateText, replacement.c_str());

      sourceCode += "// Replacing `"+templateText+"` with `"+replacement+"`\n" + t + "\n";
    }
  }

  void unloadImpl() override
  {
    sourceCode.clear();
  }

  size_t size() const override
  {
    return sizeof(*this) + fileName.length() + sourceCode.length();
  }

  void generateSourceCodeOverview(const InOutput<SourceCode>& sourceCode) override
  {
    sourceCode.value.beginOverview("Template-File: "+fileName);
    sourceCode.value.endOverview();
  }

  void generateSourceCode(const InOutput<SourceCode>& sourceCode) override
  {
    sourceCode.value.beginSection("File "+fileName, "---- Template-SourceFile <"+fileName+"> --------");
    sourceCode.value.write(this->sourceCode);
    sourceCode.value.write("// -- End Template-Source File <"+fileName+">\n");
  }
};

// ====

Module::Module(Ogre::ResourceManager* creator,
               const Ogre::String& name,
               Ogre::ResourceHandle handle,
               const Ogre::String& group,
               bool isManual,
               Ogre::ManualResourceLoader* loader)
  : Ogre::Resource(creator, name, handle, group, isManual, loader)
{
  initialized = false;
  _supportedLanguages = Language::ALL;
  createParamDictionary("Base::CodeGenerator::Module");
}

Module::~Module()
{
  unload();
}

void Module::loadImpl()
{
  mSize = sizeof(*this);

  for(const SectionPtr& section : sections)
  {
    section->loadImpl();

    mSize += section->size();
  }
}

void Module::unloadImpl()
{
  for(const SectionPtr& section : sections)
    section->unloadImpl();
}

void Module::includeModule(const Ogre::String& moduleName, Language supportedLanguages)
{
  initialized = true;

  Module::Ptr module = ModuleManager::createModule(moduleName);

  if(module.getPointer() == this)
  {
    IO::Log::logError("Base::CodeGenerator::Module::includeModule(): A module can't include itself");
    return;
  }

  if(module->includesModule(this))
  {
    IO::Log::logError("Base::CodeGenerator::Module::includeModule(): Curcular include references are not allowed");
    return;
  }

  sections.push_back(IncludedModuleSection::create(module, supportedLanguages));
}

void Module::addSourceFile(const Ogre::String& filename, Language supportedLanguages)
{
  initialized = true;

  sections.push_back(IncludedSourceFileSection::create(filename, supportedLanguages));
}

void Module::addSourceCode(const Ogre::String& label, const Ogre::String& code, Language supportedLanguages)
{
  initialized = true;

  sections.push_back(IncludedSourceCodeSection::create(label, code, supportedLanguages));
}

void Module::addTemplateSourceFile(const Ogre::String& filename, const Ogre::String& templateText, const Ogre::StringVectorPtr& replacements, Language supportedLanguages)
{
  initialized = true;

  sections.push_back(IncludedTemplateSourceFileSection::create(filename, templateText, replacements, supportedLanguages));
}

void Module::setSupportedLanguages(Language supportedLanguages)
{
  initialized = true;

  this->_supportedLanguages = supportedLanguages;
}

Language Module::supportedLanguages()
{
  return this->_supportedLanguages;
}

bool Module::includesModule(Module* otherModule)
{
  for(const SectionPtr& section : sections)
  {
    if(section->includesModule(otherModule))
      return true;
  }

  return false;
}

void Module::generateSourceCodeOverview(const InOutput<SourceCode>& sourceCode)
{
  sourceCode.value.beginOverview("Module: "+getName());
  for(const SectionPtr& section : sections)
    section->generateSourceCodeOverview(sourceCode);
  sourceCode.value.endOverview();
}

void Module::generateSourceCode(const InOutput<SourceCode>& sourceCodeOut)
{
  SourceCode& sourceCode = sourceCodeOut.value;

  if(!sourceCode.supportsLanguage(this->_supportedLanguages))
  {
    IO::Log::logError("The module '%0' is not supporting the language %1.", getName(), toString(sourceCode.language()));
    return;
  }

  load();

  sourceCode.beginSection("Module "+getName(), "==== Module '"+getName()+"' ========");
  for(const SectionPtr& section : sections)
  {
    if(sourceCode.supportsLanguage(section->language))
      section->generateSourceCode(sourceCodeOut);
  }
  sourceCode.write("// == End Module \""+getName()+"\"\n");
}


// ====


Module::Section::Section(Language language)
  : language(language)
{
}


bool Module::Section::includesModule(Module* module)
{
  (void)module;
  return false;
}


} // namespace CodeGenerator
} // namespace Base
