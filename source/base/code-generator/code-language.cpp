#include "code-language.h"
#include "source-code.h"

#include <base/io/log.h>

namespace Base {
namespace CodeGenerator {


std::string Language::to_string_throwingException(Language language)
{
  switch(language.value)
  {
  case Language::CPP:
    return "c++";
  case Language::ANGLESCRIPT:
    return "anglescript";
  case Language::GLSL_120:
    return "glsl-120";
  case Language::GLSL_130:
    return "glsl-130";
  case Language::GLSL_140:
    return "glsl-140";
  case Language::GLSL_150:
    return "glsl-150";
  case Language::GLSL_330:
    return "glsl-330";
  case Language::GLSL_400:
    return "glsl-400";
  case Language::GLSL_410:
    return "glsl-410";
  case Language::GLSL_420:
    return "glsl-420";
  case Language::GLSL_430:
    return "glsl-430";
  case Language::GLSL_440:
    return "glsl-440";
  case Language::GLSL_3xx:
    return "glsl-3xx";
  case Language::GLSL_4xx:
    return "glsl-4xx";
  case Language::GLSL_WITHOUT_120:
    return "glsl-without-120";
  case Language::GLSL:
    return "glsl";
  case Language::ALL:
    return "*";
  default:
    throw std::invalid_argument("Unknown language " + std::to_string(static_cast<uint32>(language.value)));
  }
}


Language Language::parseStringThrowingException(const std::string& string)
{
  if(string == "")
    return Language::NONE;

#define HANDLE(l) if(Language::toStdString(l) == string)return l;
  HANDLE(Language::CPP);
  HANDLE(Language::ANGLESCRIPT);
  HANDLE(Language::GLSL_120);
  HANDLE(Language::GLSL_130);
  HANDLE(Language::GLSL_140);
  HANDLE(Language::GLSL_150);
  HANDLE(Language::GLSL_330);
  HANDLE(Language::GLSL_400);
  HANDLE(Language::GLSL_410);
  HANDLE(Language::GLSL_420);
  HANDLE(Language::GLSL_430);
  HANDLE(Language::GLSL_440);
  HANDLE(Language::GLSL_3xx);
  HANDLE(Language::GLSL_4xx);
  HANDLE(Language::GLSL_WITHOUT_120);
  HANDLE(Language::GLSL);
  HANDLE(Language::ALL);
#undef HANDLE

  throw std::invalid_argument("Unknown language " + string);
}


Language Language::parseStringThrowingException(const String& string)
{
  return parseStringThrowingException(string.toUtf8String());
}


std::string Language::toStdString(const Language& language)
{
  try
  {
    return to_string_throwingException(language);
  }catch(const std::exception& e)
  {
    IO::Log::logError(e.what());
  }

  return "";
}


Language Language::parseString(const std::string& string)
{
  try
  {
    return parseStringThrowingException(string);
  }catch(const std::exception& e)
  {
    IO::Log::logError(e.what());
  }

  return Language::NONE;
}

Language Language::parseString(const String& string)
{
  return parseString(string.toUtf8String());
}

class LanguageGLSLSHaderCodeHelper : public LanguageSourceCodeHelper
{
public:
  int version = 0;
  Ogre::String openGLVersion;

  LanguageGLSLSHaderCodeHelper(Language language, SourceCode* sourceCode)
    : LanguageSourceCodeHelper(sourceCode)
  {
    switch(language.value)
    {
    case Language::GLSL_120:
      version = 120;
      openGLVersion = "2.1";
      break;
    case Language::GLSL_130:
      version = 130;
      openGLVersion = "3.0";
      break;
    case Language::GLSL_140:
      version = 140;
      openGLVersion = "3.1";
      break;
    case Language::GLSL_150:
      version = 150;
      openGLVersion = "3.2";
      break;
    case Language::GLSL_330:
      version = 330;
      openGLVersion = "3.3";
      break;
    case Language::GLSL_400:
      version = 400;
      openGLVersion = "4.0";
      break;
    case Language::GLSL_410:
      version = 410;
      openGLVersion = "4.1";
      break;
    case Language::GLSL_420:
      version = 420;
      openGLVersion = "4.2";
      break;
    case Language::GLSL_430:
      version = 430;
      openGLVersion = "4.3";
      break;
    case Language::GLSL_440:
      version = 440;
      openGLVersion = "4.4";
      break;
    default:
      throw std::logic_error("beginSourceCode: Unknown GLSL version");
    }

    sourceCode->beginSection("The OpenGL Version preprocessor declaration", "");
    sourceCode->write(String::compose("#version %0 core  // OpenGL Version %1\n", version, openGLVersion));
  }

  void _writeArgumentType(const FunctionType::ArgumentType& argumentType) override
  {
    switch(argumentType.reference.value)
    {
    case FunctionType::ArgumentType::ReferenceType::IN:
      sourceCode.write("in ");
      break;
    case FunctionType::ArgumentType::ReferenceType::INOUT:
      sourceCode.write("inout ");
      break;
    case FunctionType::ArgumentType::ReferenceType::OUT:
      sourceCode.write("out ");
      break;
    case FunctionType::ArgumentType::ReferenceType::NO_ANNOTATION:
    default:
      break;
    }

    sourceCode.write(argumentType.type.name());
  }
};

class LanguageAngelscriptCodeHelper : public LanguageSourceCodeHelper
{
public:
  void _writeArgumentType(const FunctionType::ArgumentType& argumentType) override
  {
    if(argumentType.reference == FunctionType::ArgumentType::ReferenceType::IN)
      sourceCode.write("const ");

    sourceCode.write(argumentType.type.name());

    switch(argumentType.reference.value)
    {
    case FunctionType::ArgumentType::ReferenceType::IN:
      sourceCode.write(" &in");
      break;
    case FunctionType::ArgumentType::ReferenceType::INOUT:
      sourceCode.write(" &inout");
      break;
    case FunctionType::ArgumentType::ReferenceType::OUT:
      sourceCode.write(" &out");
      break;
    case FunctionType::ArgumentType::ReferenceType::NO_ANNOTATION:
    default:
      break;
    }
  }

  LanguageAngelscriptCodeHelper(Language language, SourceCode* sourceCode)
    : LanguageSourceCodeHelper(sourceCode)
  {
    (void)language;
  }
};

class LanguageCppCodeHelper : public LanguageSourceCodeHelper
{
public:
  void _writeArgumentType(const FunctionType::ArgumentType& argumentType) override
  {
    if(argumentType.reference == FunctionType::ArgumentType::ReferenceType::IN)
      sourceCode.write("const ");

    sourceCode.write(argumentType.type.name());

    switch(argumentType.reference.value)
    {
    case FunctionType::ArgumentType::ReferenceType::IN:
    case FunctionType::ArgumentType::ReferenceType::INOUT:
    case FunctionType::ArgumentType::ReferenceType::OUT:
      sourceCode.write("&");
      break;
    case FunctionType::ArgumentType::ReferenceType::NO_ANNOTATION:
    default:
      break;
    }
  }

  LanguageCppCodeHelper(Language language, SourceCode* sourceCode)
    : LanguageSourceCodeHelper(sourceCode)
  {
    (void)language;
  }
};

class LanguageCppWithFrameworkCodeHelper : public LanguageCppCodeHelper
{
public:
  void _writeArgumentType(const FunctionType::ArgumentType& argumentType) override
  {
    switch(argumentType.reference.value)
    {
    case FunctionType::ArgumentType::ReferenceType::IN:
      sourceCode.write("const ");
      break;
    case FunctionType::ArgumentType::ReferenceType::INOUT:
      sourceCode.write("const InOutput< ");
      break;
    case FunctionType::ArgumentType::ReferenceType::OUT:
      sourceCode.write("const Output< ");
      break;
    case FunctionType::ArgumentType::ReferenceType::NO_ANNOTATION:
    default:
      break;
    }

    sourceCode.write(argumentType.type.name());

    switch(argumentType.reference.value)
    {
    case FunctionType::ArgumentType::ReferenceType::IN:
      sourceCode.write("&");
      break;
    case FunctionType::ArgumentType::ReferenceType::INOUT:
    case FunctionType::ArgumentType::ReferenceType::OUT:
      sourceCode.write(" >&");
      break;
    case FunctionType::ArgumentType::ReferenceType::NO_ANNOTATION:
    default:
      break;
    }
  }

  LanguageCppWithFrameworkCodeHelper(Language language, SourceCode* sourceCode)
    : LanguageCppCodeHelper(language, sourceCode)
  {
  }
};


LanguageSourceCodeHelper::LanguageSourceCodeHelper(SourceCode* sourceCode)
  : sourceCode(*sourceCode)
{
}

LanguageSourceCodeHelper::Ptr LanguageSourceCodeHelper::create(Language language, SourceCode* sourceCode)
{
  if((language & Language::GLSL) != Language::NONE)
    return Ptr(new LanguageGLSLSHaderCodeHelper(language, sourceCode));
  else if((language & Language::ANGLESCRIPT) != Language::NONE)
    return Ptr(new LanguageAngelscriptCodeHelper(language, sourceCode));
  else if((language & Language::CPP_WITH_FRAMEWORK) != Language::NONE)
    return Ptr(new LanguageCppWithFrameworkCodeHelper(language, sourceCode));
  else if((language & Language::CPP) != Language::NONE)
    return Ptr(new LanguageCppCodeHelper(language, sourceCode));

  assert("Unknown language" && false);
  return Ptr(new LanguageAngelscriptCodeHelper(language, sourceCode));
}


void LanguageSourceCodeHelper::declareFunction(const FunctionDeclaration& functionDeclaration, bool suppressSemicolon)
{
  _declareFunctionWithCustomArgumentNames(functionDeclaration,
                                          [&functionDeclaration](int i, SourceCode& sourceCode){
    sourceCode.write(" ");
    sourceCode.write(functionDeclaration.argumentNames()[i]);
  });

  if(!suppressSemicolon)
    sourceCode.write(";");
}


void LanguageSourceCodeHelper::beginFunction(const FunctionDeclaration& functionDeclaration)
{
  declareFunction(functionDeclaration, true);
  sourceCode.write("\n{\n");
}


void LanguageSourceCodeHelper::endFunction()
{
  sourceCode.write("}\n");
}


void LanguageSourceCodeHelper::_declareFunctionWithCustomArgumentNames(const FunctionSignature& functionSignature,
                                                                       const std::function<void(int, SourceCode&)>& customArgumentName)
{
  typedef FunctionType::ArgumentType ArgumentType;

  const Optional<Type>& returnType = functionSignature.functionType().returnType();
  const QVector<FunctionType::ArgumentType>& argumentTypes = functionSignature.functionType().argumentTypes();

  if(returnType)
  {
    sourceCode.write(returnType->name());
    sourceCode.write(" ");
  }else
  {
    sourceCode.write("void ");
  }
  sourceCode.write(functionSignature.name());
  sourceCode.write("(");
  int i = 0;
  for(const ArgumentType& argumentType : argumentTypes)
  {
    if(i!=0)
      sourceCode.write(", ");

    _writeArgumentType(argumentType);

    customArgumentName(i, this->sourceCode);

    ++i;
  }
  sourceCode.write(")");
}


} // namespace CodeGenerator
} // namespace Base
