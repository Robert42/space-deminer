#ifndef BASE_CODEGENERATOR_CODESOURCEFILELOADER_H
#define BASE_CODEGENERATOR_CODESOURCEFILELOADER_H

#include <dependencies.h>

namespace Base {
namespace CodeGenerator {

class SourceFileLoader : nonconstructable
{
public:
  static const Ogre::String codeResourceGroup;

  static bool existsFile(const Ogre::String& file);
  static Ogre::String loadFile(const Ogre::String& file);
};

} // namespace CodeGenerator
} // namespace Base

#endif // BASE_CODEGENERATOR_CODESOURCEFILELOADER_H
