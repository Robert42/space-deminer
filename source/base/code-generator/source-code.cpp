#include "source-code.h"
#include "code-module-manager.h"

#include <base/io/log.h>

namespace Base {
namespace CodeGenerator {

SourceCode::SourceCode()
{
  clear();
}


void SourceCode::clear()
{
  overviewIntendation = 0;
  _language = Language::NONE;

  fullSourceCode.clear();
  sourceCodeHelper.reset();
  alreadyIncludedModules.clear();
}


void SourceCode::load(const Ogre::String& moduleName, Language language)
{
  clear();

  this->_language = language;

  Optional<Module::Ptr> m = ModuleManager::moduleForName(moduleName);
  if(!m)
  {
    IO::Log::logError("SourceCode::load(): Couldn't find the module `%0`", moduleName);
    return;
  }
  Module::Ptr module = *m;

  sourceCodeHelper = LanguageSourceCodeHelper::create(language, this);

  sourceCodeHelper->beginSourceCode();

  write("\n// ==== Overview ========\n");
  module->generateSourceCodeOverview(inout(*this));
  write("//\n//\n\n");

  alreadyIncludedModules.clear();

  module->generateSourceCode(inout(*this));

  sourceCodeHelper->endSourceCode();
  sourceCodeHelper.reset();
}

void SourceCode::beginSection(const Ogre::String& sectionLabel, const Ogre::String& comment)
{
  (void)sectionLabel;

  if(!comment.empty())
    fullSourceCode += "\n// " + comment + "\n";
}

void SourceCode::write(const Ogre::String& code)
{
  fullSourceCode += code;
}

void SourceCode::write(const char* code)
{
  fullSourceCode += code;
}

void SourceCode::write(const String& code)
{
  write(code.toAnsiString());
}

void SourceCode::beginOverview(const Ogre::String& label)
{
  write("// ");
  for(int i=0; i<overviewIntendation; ++i)
    write("    ");
  write(label);
  write("\n");

  overviewIntendation++;
}

void SourceCode::endOverview()
{
  overviewIntendation--;
}

bool SourceCode::supportsLanguage(Language language) const
{
  return (this->language()&language) != Language::NONE;
}

Language SourceCode::language() const
{
  return this->_language;
}


} // namespace CodeGenerator
} // namespace Base
