#ifndef BASE_CODEGENERATOR_CODEMODULESCRIPTTRANSLATOR_H
#define BASE_CODEGENERATOR_CODEMODULESCRIPTTRANSLATOR_H

#include <dependencies.h>
#include "code-module.h"

namespace Base {
namespace CodeGenerator {


class ModuleScriptTranslator : public Ogre::ScriptTranslator
{
public:
  void translate(Ogre::ScriptCompiler* compiler, const Ogre::AbstractNodePtr& node) override;
  bool translateSource(const Module::Ptr& module, Ogre::ScriptCompiler* compiler, const Ogre::AbstractNodePtr& node);
  bool translateDependency(const Module::Ptr& module, Ogre::ScriptCompiler* compiler, const Ogre::AbstractNodePtr& node);
  bool translateSupport(const Module::Ptr& module, Ogre::ScriptCompiler* compiler, const Ogre::AbstractNodePtr& node);
  bool translateTemplate(const Module::Ptr& module, Ogre::ScriptCompiler* compiler, const Ogre::AbstractNodePtr& node);
};


class ScriptTranslatorManager : public Ogre::ScriptTranslatorManager
{
  ModuleScriptTranslator module;

public:
  ScriptTranslatorManager();

private:
  size_t getNumTranslators() const override;
  Ogre::ScriptTranslator* getTranslator(const Ogre::AbstractNodePtr& node) override;
};

} // namespace CodeGenerator
} // namespace Base

#endif // BASE_CODEGENERATOR_CODEMODULESCRIPTTRANSLATOR_H
