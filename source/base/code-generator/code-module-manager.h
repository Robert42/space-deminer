#ifndef BASE_CODEGENERATOR_CODEMODULEMANAGER_H
#define BASE_CODEGENERATOR_CODEMODULEMANAGER_H

#include "code-module.h"

#include <base/singleton.h>
#include <base/optional.h>

namespace Base {
namespace CodeGenerator {


class ScriptTranslatorManager;

/**
     * @note this implementation is based on http://www.ogre3d.org/tikiwiki/tiki-index.php?page=Resources+and+ResourceManagers
     */
class ModuleManager : public Ogre::ResourceManager, public Singleton<ModuleManager>
{
private:
  ScriptTranslatorManager* scriptTranslatorManager;

public:
  ModuleManager();
  virtual ~ModuleManager();

protected:
  Ogre::Resource* createImpl(const Ogre::String& name,
                             Ogre::ResourceHandle handle,
                             const Ogre::String& group,
                             bool isManual,
                             Ogre::ManualResourceLoader* loader,
                             const Ogre::NameValuePairList* createParams) override;

  void parseScript(Ogre::DataStreamPtr& stream, const Ogre::String& groupName) override;

public:
  static Optional<Module::Ptr> moduleForName(const Ogre::String& name, const Ogre::String& group = Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME);
  static Module::Ptr loadModule(const Ogre::String& name, const Ogre::String& group = Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME);
  static Module::Ptr createModule(const Ogre::String& name, const Ogre::String& group = Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME);

  static void removeModule(Module::Ptr& module);
};

} // namespace CodeGenerator
} // namespace Base

#endif // BASE_CODEGENERATOR_CODEMODULEMANAGER_H
