#ifndef BASE_CODEGENERATOR_SOURCECODE_H
#define BASE_CODEGENERATOR_SOURCECODE_H

#include "code-language.h"

#include <base/strings/string.h>

namespace Base {
namespace CodeGenerator {

class Module;
class SourceCode
{
private:
  LanguageSourceCodeHelper::Ptr sourceCodeHelper;
  int overviewIntendation;
  Language _language;

public:
  std::set<Module*> alreadyIncludedModules;
  Ogre::String fullSourceCode;

public:
  SourceCode();

  void clear();
  void load(const Ogre::String& moduleName, Language language);

  void beginSection(const Ogre::String& sectionLabel, const Ogre::String& comment);
  void write(const Ogre::String& code);
  void write(const String& code);
  void write(const char* code);

  void beginOverview(const Ogre::String& label);
  void endOverview();

  bool supportsLanguage(Language language) const;
  Language language() const;
};

} // namespace CodeGenerator
} // namespace Base

#endif // BASE_CODEGENERATOR_SOURCECODE_H
