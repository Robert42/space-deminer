#include "code-function-signature.h"
#include <base/strings/names.h>
#include <base/io/log.h>

namespace Base {
namespace CodeGenerator {


FunctionSignature::FunctionSignature(const FunctionType &functionType, const std::string& name)
  : _functionType(functionType),
    _name(name)
{
  // Empty function names are not allowed.
  assert(isValidCVariableName(String::fromAnsi(name)));
}

const FunctionType& FunctionSignature::functionType() const
{
  return this->_functionType;
}

const std::string& FunctionSignature::name() const
{
  return this->_name;
}


FunctionDeclaration::FunctionDeclaration(const FunctionType& functionType,
                                         const std::string& name,
                                         const QVector<std::string>& argumentNames)
  : FunctionSignature(functionType, name),
    _argumentNames(argumentNames)
{
  for(const std::string& name : argumentNames)
  {
    assert(isValidCVariableName(String::fromAnsi(name)));
  }

  if(this->_functionType.argumentTypes().size() != this->_argumentNames.size())
  {
    IO::Log::logError("FunctionSignature::FunctionSignature(): Number of ArgumentTypes differs from the number of Argument names");

    QVector<FunctionType::ArgumentType> argumentTypes = this->_functionType.argumentTypes();

    while(argumentTypes.size() > this->_argumentNames.size())
      argumentTypes.pop_back();
    while(argumentTypes.size() < this->_argumentNames.size())
      this->_argumentNames.pop_back();

    _functionType = FunctionType(_functionType.returnType(), argumentTypes);
  }
}

const QVector<std::string>& FunctionDeclaration::argumentNames() const
{
  return this->_argumentNames;
}


} // namespace CodeGenerator
} // namespace Base
