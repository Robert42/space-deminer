#include "code-module-manager.h"
#include "code-module-script-translator.h"

#include "source-code.h"

typedef Base::CodeGenerator::ModuleManager ModuleManager;

namespace Base {
namespace CodeGenerator {

ModuleManager::ModuleManager()
{
  this->mResourceType = "Base::CodeGenerator::Module";
  this->mScriptPatterns.push_back("*.script-module");
  this->mScriptPatterns.push_back("*.shader-module");
  this->mLoadOrder = 90.f;

  Ogre::ResourceGroupManager::getSingleton()._registerResourceManager(this->mResourceType, this);
  Ogre::ResourceGroupManager::getSingleton()._registerScriptLoader(this);

  scriptTranslatorManager = new ScriptTranslatorManager;

  Ogre::ScriptCompilerManager::getSingleton().addTranslatorManager(scriptTranslatorManager);
}


ModuleManager::~ModuleManager()
{
  Ogre::ScriptCompilerManager::getSingleton().removeTranslatorManager(scriptTranslatorManager);

  Ogre::ResourceGroupManager::getSingleton()._unregisterResourceManager(this->mResourceType);
  Ogre::ResourceGroupManager::getSingleton()._unregisterScriptLoader(this);
}


Ogre::Resource* ModuleManager::createImpl(const Ogre::String& name,
                                          Ogre::ResourceHandle handle,
                                          const Ogre::String& group,
                                          bool isManual,
                                          Ogre::ManualResourceLoader* loader,
                                          const Ogre::NameValuePairList* createParams)
{
  (void)createParams;
  return new Module(this, name, handle, group, isManual, loader);
}


Optional<Module::Ptr> ModuleManager::moduleForName(const Ogre::String& name, const Ogre::String& group)
{
  Module::Ptr module = singleton().getResourceByName(name, group).staticCast<Module>();

  if(module.isNull())
    return nothing;
  else
    return module;
}


Module::Ptr ModuleManager::loadModule(const Ogre::String& name, const Ogre::String& group)
{
  Module::Ptr module = createModule(name, group);

  module->load();
  return module;
}


Module::Ptr ModuleManager::createModule(const Ogre::String& name, const Ogre::String& group)
{
  Optional<Module::Ptr> module = moduleForName(name, group);

  if(module)
    return *module;

  return singleton().createResource(name, group).staticCast<Module>();
}


void ModuleManager::removeModule(Module::Ptr& module)
{
  Ogre::ResourcePtr r = module;
  module.setNull();

  singleton().remove(r);
}


void ModuleManager::parseScript(Ogre::DataStreamPtr& stream, const Ogre::String& groupName)
{
  Ogre::ScriptCompilerManager::getSingleton().parseScript(stream, groupName);

  SourceCode sourceCode;
  sourceCode.load("math-transformation", Language::GLSL_430);
  std::cout << sourceCode.fullSourceCode<< std::endl;
}


} // namespace CodeGenerator
} // namespace Base
