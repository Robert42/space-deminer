#include "code-source-file-loader.h"

#include <base/io/log.h>

namespace Base {
namespace CodeGenerator {

const Ogre::String SourceFileLoader::codeResourceGroup = "Script/Shader-Code";

bool SourceFileLoader::existsFile(const Ogre::String& file)
{
  if(!Ogre::ResourceGroupManager::getSingleton().resourceGroupExists(codeResourceGroup))
  {
    IO::Log::logError("Couldn't find the resource group `%0`", codeResourceGroup);
    return false;
  }

  const Ogre::ResourceGroupManager::LocationList& locationList = Ogre::ResourceGroupManager::getSingleton().getResourceLocationList(codeResourceGroup);
  for(const Ogre::ResourceGroupManager::ResourceLocation* location : locationList)
  {
    if(location->archive->exists(file))
      return true;
  }

  return false;
}

Ogre::String SourceFileLoader::loadFile(const Ogre::String& file)
{
  if(!Ogre::ResourceGroupManager::getSingleton().resourceGroupExists(codeResourceGroup))
  {
    IO::Log::logError("Couldn't find the resource group `%0`", codeResourceGroup);
    return "";
  }

  const Ogre::ResourceGroupManager::LocationList& locationList = Ogre::ResourceGroupManager::getSingleton().getResourceLocationList(codeResourceGroup);
  for(const Ogre::ResourceGroupManager::ResourceLocation* location : locationList)
  {
    if(location->archive->exists(file))
      return location->archive->open(file)->getAsString();
  }

  IO::Log::logError("Couldn't load the script/shader file `%0`", file);
  return "";
}

} // namespace CodeGenerator
} // namespace Base
