#ifndef BASE_CODEGENERATOR_CODEMODULE_H
#define BASE_CODEGENERATOR_CODEMODULE_H

#include <dependencies.h>

#include "code-language.h"

#include <base/containers/std-container-accessor.h>
#include <base/signals/trackable.h>

namespace Base {
namespace CodeGenerator {

/** A Single shader module or script module.
     *
     * A module consists of one or multiple sections. Each section
     * returns source code which will be concatenated.
     *
     * Possible sections are
     * - Dependencies which will include the source code from other modules
     * ensuring, that each dependency is included only once.
     * - SourceFile loads the content of a source file and uses it
     *
     * The script format is `*.shader-module`, `*.script-module` or `*.code-module`.
     * You can freely choose beween theese filetypes.
     * ```
     * // defines a code module called 'constants'
     * // Instead of code-module, you can also say
     * // `script-module` or `shader-module`
     * code-module geometry : math
     * {
     *   include-module
     * }
     * ```
     */
class Module : public Ogre::Resource
{
public:
  class Section;

  typedef Ogre::SharedPtr<Module> Ptr;
  typedef std::shared_ptr<Section> SectionPtr;

private:
  bool initialized;
  Language _supportedLanguages;
  QList<SectionPtr> sections;

public:
  Module(Ogre::ResourceManager* creator,
         const Ogre::String& name,
         Ogre::ResourceHandle handle,
         const Ogre::String& group,
         bool isManual = false,
         Ogre::ManualResourceLoader* loader = nullptr);
  virtual ~Module();

public:
  void loadImpl() override;
  void unloadImpl() override;

  void includeModule(const Ogre::String& moduleName, Language supportedLanguages = Language::ALL);
  void addSourceFile(const Ogre::String& filename, Language supportedLanguages = Language::ALL);
  void addSourceCode(const Ogre::String& label, const Ogre::String& code, Language supportedLanguages = Language::ALL);
  void addTemplateSourceFile(const Ogre::String& filename, const Ogre::String& templateText, const Ogre::StringVectorPtr& replacements, Language supportedLanguages = Language::ALL);
  void setSupportedLanguages(Language supportedLanguages);
  Language supportedLanguages();

  bool includesModule(Module* otherModule);
  void generateSourceCodeOverview(const InOutput<SourceCode>& sourceCode);
  void generateSourceCode(const InOutput<SourceCode>& sourceCodeOut);
};


class Module::Section
{
public:
  Language language;

public:
  Section(Language language = Language::ALL);

public:
  virtual bool includesModule(Module* module);

  virtual Ogre::String label() const = 0;

  virtual void loadImpl() = 0;
  virtual void unloadImpl() = 0;

  virtual size_t size() const = 0;

  virtual void generateSourceCodeOverview(const InOutput<SourceCode>& sourceCode) = 0;
  virtual void generateSourceCode(const InOutput<SourceCode>& sourceCode) = 0;
};


} // namespace CodeGenerator
} // namespace Base

#endif // BASE_CODEGENERATOR_CODEMODULE_H
