#include "code-module-script-translator.h"
#include "code-module-manager.h"
#include "code-language.h"

#include <base/io/log.h>

namespace Ogre {
String toString(Ogre::AbstractNodeType type)
{
  switch(type)
  {
  case ANT_ATOM:
    return "ANT_ATOM";
  case ANT_OBJECT:
    return "ANT_OBJECT";
  case ANT_PROPERTY:
    return "ANT_PROPERTY";
  case ANT_IMPORT:
    return "ANT_IMPORT";
  case ANT_VARIABLE_SET:
    return "ANT_VARIABLE_SET";
  case ANT_VARIABLE_ACCESS:
    return "ANT_VARIABLE_ACCESS";
  default:
    return "ANT_UNKNOWN";
  }
}
} // namespace Ogre

namespace Base {
namespace CodeGenerator {


uint32 ID_SHADER_MODULE = std::numeric_limits<uint32>::max();
uint32 ID_SCRIPT_MODULE = std::numeric_limits<uint32>::max();
uint32 ID_CODE_MODULE = std::numeric_limits<uint32>::max();
uint32 ID_SOURCE = std::numeric_limits<uint32>::max();
uint32 ID_INCLUDE = std::numeric_limits<uint32>::max();
uint32 ID_SUPPORT = std::numeric_limits<uint32>::max();
uint32 ID_TEMPLATE = std::numeric_limits<uint32>::max();

inline void printError(Ogre::ScriptCompiler* compiler, uint32 errorCode, const Ogre::AbstractNode& node, const String& msg)
{
  String fullText = String::compose("CodeModuleScript-Error in %0 (%1): %2",
                                    node.file,
                                    node.line,
                                    msg);

  IO::Log::logError(fullText);

  compiler->addError(errorCode, node.file, node.line);
}

#define PRINT_ERROR_OBJECTNAMEEXPECTED(n) printError(compiler, Ogre::ScriptCompiler::CE_OBJECTNAMEEXPECTED, n, String::compose("Expected an objectname for an object of class '%0'", static_cast<Ogre::ObjectAbstractNode&>(n).cls))
#define PRINT_ERROR_UNEXPECTEDNODE(n) printError(compiler, Ogre::ScriptCompiler::CE_UNEXPECTEDTOKEN, (n), String::compose("Unexpected node '%0' of Type '%1'", (n).getValue(), (n).type))
#define PRINT_ERROR_ONE_VALUE_EXPECTED(n) printError(compiler, Ogre::ScriptCompiler::CE_UNEXPECTEDTOKEN, (n), String::compose(static_cast<Ogre::PropertyAbstractNode&>(n).values.size()>1?"Only expected one value for the property '%0', but actually %1 arguments are given":"The property '%0' needs a value.", (n).getValue(), static_cast<Ogre::PropertyAbstractNode&>(n).values.size()))
#define PRINT_ERROR_VALUES_EXPECTED(n) printError(compiler, Ogre::ScriptCompiler::CE_UNEXPECTEDTOKEN, (n), String::compose("The property '%0' needs at least one value", (n).getValue()))
#define PRINT_ERROR_INHERITANCE_NOT_SUPPORTED(n) printError(compiler, Ogre::ScriptCompiler::CE_UNEXPECTEDTOKEN, (n), "Inharitaning Modules using `:` is not supported and will result in undefined behavior. Please consider using `include` instead")

void printNode(const Ogre::AbstractNodePtr& node);
inline void printNode(Ogre::AbstractNode& node)
{
  static int intendation = 0;
  static bool newLine = false;

  if(newLine)
    for(int i=0; i<intendation; ++i)
      std::cout << "  ";

  switch(node.type)
  {
  case Ogre::ANT_UNKNOWN:
    std::cout << "UNKOWN-TYPE";
    break;
  case Ogre::ANT_ATOM:
    std::cout << "Atom("<<static_cast<Ogre::AtomAbstractNode&>(node).value<<")";
    break;
  case Ogre::ANT_OBJECT:
  {
    Ogre::ObjectAbstractNode& o = static_cast<Ogre::ObjectAbstractNode&>(node);
    std::cout << o.name << "\n{\n";
    intendation++;
    for(const Ogre::AbstractNodePtr& child : o.children)
    {
      newLine = true;
      printNode(child);
      std::cout << "\n";
    }
    intendation--;
    std::cout << "}\n";
    newLine = true;
    break;
  }
  case Ogre::ANT_PROPERTY:
  {
    Ogre::PropertyAbstractNode& p = static_cast<Ogre::PropertyAbstractNode&>(node);
    std::cout << "Property(" << p.name;
    for(const Ogre::AbstractNodePtr& v : p.values)
    {
      std::cout << " ";
      printNode(v);
    }
    std::cout << ")";
    break;
  }
  case Ogre::ANT_IMPORT:
    std::cout << "IMPORT";
    break;
  case Ogre::ANT_VARIABLE_SET:
    std::cout << "ANT_VARIABLE_SET";
    break;
  case Ogre::ANT_VARIABLE_ACCESS:
    std::cout << "ANT_VARIABLE_ACCESS";
    break;
  }
}

inline void printNode(const Ogre::AbstractNodePtr& node)
{
  printNode(*node);
}

inline Language parseLanguages(const Ogre::AbstractNodeList::const_iterator& begin, const Ogre::AbstractNodeList::const_iterator& end, Language fallback)
{
  if(begin == end)
    return fallback;

  Language l = Language::NONE;

  for(auto i=begin; i!=end; ++i)
    l |= Language::parseString((*i)->getValue());

  return l;
}

inline Language parseLanguages(const Ogre::AbstractNodeList& nodes, Language fallback)
{
  return parseLanguages(nodes.begin(), nodes.end(), fallback);
}

inline Language parseLanguagesExceptFirst(const Ogre::AbstractNodeList& nodes, Language fallback)
{
  auto i = nodes.begin();
  ++i;
  return parseLanguages(i, nodes.end(), fallback);
}

void ModuleScriptTranslator::translate(Ogre::ScriptCompiler* compiler, const Ogre::AbstractNodePtr& node)
{
  Ogre::ObjectAbstractNode& objectNode = static_cast<Ogre::ObjectAbstractNode&>(*node);

  if(objectNode.name.empty())
    PRINT_ERROR_OBJECTNAMEEXPECTED(objectNode);

  Module::Ptr module = ModuleManager::createModule(objectNode.name, compiler->getResourceGroup());

  if(!objectNode.bases.empty())
    PRINT_ERROR_INHERITANCE_NOT_SUPPORTED(objectNode);

  for(const Ogre::AbstractNodePtr& child : objectNode.children)
  {
    if(!translateSource(module, compiler, child) &&
       !translateDependency(module, compiler, child) &&
       !translateSupport(module, compiler, child) &&
       !translateTemplate(module, compiler, child))
    {
      PRINT_ERROR_UNEXPECTEDNODE(*child);
    }
  }
}

bool ModuleScriptTranslator::translateSource(const Module::Ptr& module, Ogre::ScriptCompiler* compiler, const Ogre::AbstractNodePtr& node)
{
  if(node->type != Ogre::ANT_PROPERTY)
    return false;

  Ogre::PropertyAbstractNode& propertyNode = static_cast<Ogre::PropertyAbstractNode&>(*node);

  if(propertyNode.id != ID_SOURCE)
    return false;

  if(propertyNode.values.size()==0)
    PRINT_ERROR_VALUES_EXPECTED(propertyNode);

  Language supportedLanguages = parseLanguagesExceptFirst(propertyNode.values, Language::ALL);

  Ogre::String file = (*propertyNode.values.begin())->getValue();

  module->addSourceFile(file, supportedLanguages);

  return true;
}

bool ModuleScriptTranslator::translateDependency(const Module::Ptr& module, Ogre::ScriptCompiler* compiler, const Ogre::AbstractNodePtr& node)
{
  if(node->type != Ogre::ANT_PROPERTY)
    return false;

  Ogre::PropertyAbstractNode& propertyNode = static_cast<Ogre::PropertyAbstractNode&>(*node);

  if(propertyNode.id != ID_INCLUDE)
    return false;

  if(propertyNode.values.size()==0)
    PRINT_ERROR_VALUES_EXPECTED(propertyNode);

  Language supportedLanguages = parseLanguagesExceptFirst(propertyNode.values, Language::ALL);

  Ogre::String dependency = (*propertyNode.values.begin())->getValue();

  module->includeModule(dependency, supportedLanguages);

  return true;
}

bool ModuleScriptTranslator::translateSupport(const Module::Ptr& module, Ogre::ScriptCompiler* compiler, const Ogre::AbstractNodePtr& node)
{
  if(node->type != Ogre::ANT_PROPERTY)
    return false;

  Ogre::PropertyAbstractNode& propertyNode = static_cast<Ogre::PropertyAbstractNode&>(*node);

  if(propertyNode.id != ID_SUPPORT)
    return false;

  if(propertyNode.values.size()==0)
    PRINT_ERROR_VALUES_EXPECTED(propertyNode);

  Language supportedLanguages = parseLanguages(propertyNode.values, Language::ALL);

  module->setSupportedLanguages(supportedLanguages);

  return true;
}

bool ModuleScriptTranslator::translateTemplate(const Module::Ptr& module, Ogre::ScriptCompiler* compiler, const Ogre::AbstractNodePtr& node)
{
  if(node->type != Ogre::ANT_PROPERTY)
    return false;

  Ogre::PropertyAbstractNode& propertyNode = static_cast<Ogre::PropertyAbstractNode&>(*node);

  if(propertyNode.id != ID_TEMPLATE)
    return false;

  if(propertyNode.values.size()==0)
    PRINT_ERROR_VALUES_EXPECTED(propertyNode);

  Ogre::String file = (*propertyNode.values.begin())->getValue();

  Ogre::String templateText;
  Ogre::StringVectorPtr templateReplacements(new Ogre::StringVector);

  Language l = Language::NONE;
  bool first = true;
  for(const Ogre::AbstractNodePtr& node : propertyNode.values)
  {
    if(first)
    {
      first = false;
      continue;
    }

    try
    {
      l |= Language::parseStringThrowingException(node->getValue());
    }catch(...)
    {
      if(templateText.empty())
        templateText = node->getValue();
      else
        templateReplacements->push_back(node->getValue());
    }
  }

  if(l == Language::NONE)
    l = Language::ALL;

  module->addTemplateSourceFile(file, templateText, templateReplacements, l);

  return true;
}

// ====

ScriptTranslatorManager::ScriptTranslatorManager()
{
  ID_SHADER_MODULE = Ogre::ScriptCompilerManager::getSingleton().registerCustomWordId("shader-module");
  ID_SCRIPT_MODULE = Ogre::ScriptCompilerManager::getSingleton().registerCustomWordId("script-module");
  ID_CODE_MODULE = Ogre::ScriptCompilerManager::getSingleton().registerCustomWordId("code-module");
  ID_SOURCE = Ogre::ScriptCompilerManager::getSingleton().registerCustomWordId("source");
  ID_INCLUDE = Ogre::ScriptCompilerManager::getSingleton().registerCustomWordId("include");
  ID_SUPPORT = Ogre::ScriptCompilerManager::getSingleton().registerCustomWordId("support");
  ID_TEMPLATE = Ogre::ScriptCompilerManager::getSingleton().registerCustomWordId("template");
}

size_t ScriptTranslatorManager::getNumTranslators() const
{
  return 1;
}

Ogre::ScriptTranslator* ScriptTranslatorManager::getTranslator(const Ogre::AbstractNodePtr& node)
{
  if(node->type == Ogre::ANT_OBJECT)
  {
    Ogre::ObjectAbstractNode& objectNode = static_cast<Ogre::ObjectAbstractNode&>(*node);

    if(objectNode.parent==nullptr)
    {
      uint32 id = objectNode.id;

      if(id==ID_SHADER_MODULE || id==ID_SCRIPT_MODULE || id==ID_CODE_MODULE)
        return &module;
    }
  }

  return nullptr;
}

} // namespace CodeGenerator
} // namespace Base
