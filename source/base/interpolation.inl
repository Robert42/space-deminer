#ifndef BASE_INTERPOLATION_INL
#define BASE_INTERPOLATION_INL

#include "interpolation.h"
#include "output.h"

namespace Base {

namespace Private
{

template<typename T>
T _interpolateWithFallback(const T& fallbackValue, const InOutput<real>& totalWeightSoFar)
{
  real fallbackWeight = max(1.f-totalWeightSoFar.value, 0.f);

  totalWeightSoFar.value += fallbackWeight;

  return fallbackValue*fallbackWeight;
}

template<typename T, typename... T_otherValues>
T _interpolateWithFallback(const T& fallbackValue, const InOutput<real>& totalWeightSoFar, real weight, const T& value, const T_otherValues&... otherValues)
{
  totalWeightSoFar.value += weight;

  return value*weight + _interpolateWithFallback(fallbackValue, totalWeightSoFar, otherValues...);
}

} // namespace Private



template<typename Factor, typename Value>
Factor interpolationWeightOf(const Factor& value, const Value& lowerLimit, const Value& upperLimit)
{
  return (value-lowerLimit) / (upperLimit-lowerLimit);
}


template<typename Factor, typename Value>
Value interpolate(const Factor& weight, const Value& lowerLimit, const Value& upperLimit)
{
  return lowerLimit * (1.f-weight) + upperLimit * weight;
}


template<typename T, typename... T_otherValues>
T interpolateWithFallback(const T& fallbackValue, const T_otherValues&... otherValues)
{
  real totalWeight = 0.f;
  // The last call of _interpolateWithFallback, where the fallback Weight is set to max(1.f-totalWeightSoFar, 0.f)
  // guarantees, that total Weight will be >= 1, so dividing by it is always fine.
  T tempResult = Private::_interpolateWithFallback(fallbackValue, inout(totalWeight), otherValues...);

  // Dividing by the totalWeight must take place after the Private::_interpolateWithFallback function has been called.
  return tempResult / totalWeight;
}


inline float smoothInterpolationFactor(float x)
{
  x = clamp(x, 0.f, 1.f);
  float sqX = sq(x);
  return -2*sqX * x + 3*sqX;
}


} // namespace Base


#endif // BASE_INTERPOLATION_INL
