#include <base/scripting/engine.h>
#include <base/scripting/tokenized-script.h>
#include <base/strings/string.h>

namespace Base {
namespace Scripting {


inline bool isAlpha(char c)
{
  return (c>='a' && c<='z') || (c>='A' && c<='Z');
}


inline bool isDigit(char c)
{
  return (c>='0' && c<='9');
}


inline bool isIdentifier(const char* begin, const char* end)
{
  if(begin==end)
    return false;

  if(*begin!='_' && !isAlpha(*begin))
    return false;
  ++begin;

  for(const char* c=begin; c!=end; ++c)
  {
    if(*c!='_' && !isAlpha(*c) && !isDigit(*c))
      return false;
  }

  return true;
}


TokenizedScript::Token::Token(AngelScript::asETokenClass asToken,
                              size_t indexBegin,
                              const char* begin,
                              size_t length,
                              size_t thisIndex,
                              std::vector<Token>& tokens,
                              std::stack<size_t>* bracketTokenStack,
                              const Optional< std::shared_ptr<String> >& originalString)
  : asToken(asToken),
    tokenIndex(thisIndex),
    indexBegin(indexBegin),
    begin(begin),
    end(begin+length),
    matchingBracketToken(std::numeric_limits<size_t>::max()),
    isInRangeOfUnmachedBrackets(false),
    originalString(originalString)
{
  switch(asToken)
  {
  case AngelScript::asTC_UNKNOWN:
    type = Type::UNKNOWN;
    break;
  case AngelScript::asTC_KEYWORD:
    if(*begin == '(' || *begin == '[')
    {
      type = Type::KEYWORD_OPENING_BRACKET;
      bracketTokenStack->push(thisIndex);
    }else if(*begin == ')' || *begin == ']')
    {
      type = Type::KEYWORD_CLOSING_BRACKET;
      if(!bracketTokenStack->empty())
      {
        matchingBracketToken = bracketTokenStack->top();
        tokens.at(matchingBracketToken).matchingBracketToken = thisIndex;
        bracketTokenStack->pop();
      }else
      {
        for(Token& token : tokens)
          token.isInRangeOfUnmachedBrackets = true;
      }
    }else if(*begin == ',')
    {
      type = Type::KEYWORD_COMMA;
    }else if(*begin == ';')
    {
      type = Type::KEYWORD_SEMICOLON;
    }else if(compare("::")==0)
    {
      type = Type::KEYWORD_NAMESPACE_SEPERATOR;
    }else if(compare(".")==0)
    {
      type = Type::KEYWORD_DOT;
    }else if(!isIdentifier(begin, end))
    {
      type = Type::KEYWORD_OTHER_OPERATOR;
    }else
    {
      type = Type::KEYWORD_WORD;
    }
    break;
  case AngelScript::asTC_VALUE:
    if(*begin == '\'' || *begin == '"')
      type = Type::VALUE_STRING;
    else if(compare("true")==0 || compare("false")==0)
      type = Type::VALUE_BOOLEAN;
    else
      type = Type::VALUE_OTHER;
    break;
  case AngelScript::asTC_IDENTIFIER:
    type = Type::IDENTIFIER;
    break;
  case AngelScript::asTC_COMMENT:
    type = Type::COMMENT;
    break;
  case AngelScript::asTC_WHITESPACE:
    type = Type::WHITESPACE;
    break;
  }
}


std::string TokenizedScript::Token::tokenAsAnsiString() const
{
  return std::string(begin, end);
}


String TokenizedScript::Token::tokenAsString() const
{
  if(!originalString)
    return String::fromAnsi(tokenAsAnsiString());

  const String& originalString = **this->originalString;

  return originalString.subString(this->indexBegin, end-begin);
}

size_t TokenizedScript::Token::length() const
{
  return end-begin;
}

bool TokenizedScript::Token::hasMatchingBracket() const
{
  return matchingBracketToken != std::numeric_limits<size_t>::max();
}

int TokenizedScript::Token::compare(const char* otherAnsiString) const
{
  return Token::compare(begin, end, otherAnsiString);
}

int TokenizedScript::Token::compare(const char* begin, const char* end, const char* otherAnsiString)
{
  for(const char* c=begin; c<end; ++c, ++otherAnsiString)
  {
    if(*c != *otherAnsiString)
    {
      if(*otherAnsiString == 0)
        return 1;
      if(*c == 0)
        return -1;

      if(*c < *otherAnsiString)
        return -1;
      else
        return 1;
    }else if(*c == 0)
      return 0;

  }

  if(*otherAnsiString)
    return -1;
  else
    return 0;
}


// ====


TokenizedScript::TokenizedScript(const String& script)
  : TokenizedScript(script.toAnsiString(31), script)
{
}


TokenizedScript::TokenizedScript(const std::string& ansiScript, const Optional<String>& originalScript)
  : ansiString(ansiScript.c_str(),
               ansiScript.c_str()+ansiScript.length())
{
  assert(ansiScript.length()<=size_t(std::numeric_limits<int>::max()));
  const char* restOfScript = ansiString.data();
  size_t currentIndex = 0;

  std::stack<size_t> brackets;

  AngelScript::asIScriptEngine* asEngine = Engine::angelScriptEngine();
  Optional< std::shared_ptr<String> > sharedOriginalScript;

  if(originalScript)
    sharedOriginalScript = std::shared_ptr<String>(new String(*originalScript));

  int scriptLength = ansiScript.length();
  while(scriptLength > 0)
  {
    int tokenLength;
    AngelScript::asETokenClass tokenClass;

    tokenClass = asEngine->ParseToken(restOfScript, scriptLength, &tokenLength);
    assert(tokenLength > 0);

    _tokens.push_back(Token(tokenClass,
                            currentIndex,
                            restOfScript,
                            tokenLength,
                            _tokens.size(),
                            _tokens,
                            &brackets,
                            sharedOriginalScript));

    scriptLength -= tokenLength;
    restOfScript += tokenLength;
    currentIndex += tokenLength;
  }

  if(!brackets.empty())
  {
    size_t i;
    for(i=0; i<_tokens.size(); ++i)
      if(_tokens[i].type == Token::Type::KEYWORD_OPENING_BRACKET)
        break;
    for(; i<_tokens.size(); ++i)
      _tokens[i].isInRangeOfUnmachedBrackets = true;
  }
}


const std::vector<TokenizedScript::Token>& TokenizedScript::tokens() const
{
  return _tokens;
}


const TokenizedScript::Token& TokenizedScript::token(size_t tokenId) const
{
  tokenId = min(_tokens.size()-1, tokenId);

  return _tokens[tokenId];
}


std::vector<TokenizedScript::Token>::const_iterator TokenizedScript::findIteratorOfTokenContainingCharacter(size_t index, const std::vector<Token>::const_iterator& fallback) const
{
  for(std::vector<Token>::const_iterator iter=_tokens.begin(); iter!=_tokens.end(); ++iter)
  {
    const Token& token = *iter;
    const size_t indexBegin = token.indexBegin;
    const size_t indexEnd = indexBegin + token.length();

    if(indexBegin <= index && indexEnd > index)
      return iter;
  }

  return fallback;
}


const TokenizedScript::Token& TokenizedScript::findTokenContainingCharacter(size_t i) const
{
  std::vector<TokenizedScript::Token>::const_iterator iterator = _tokens.end();
  --iterator;
  return *findIteratorOfTokenContainingCharacter(i, iterator);
}


size_t TokenizedScript::findTokenIndexContainingCharacter(size_t characterIndex, size_t fallbackValue) const
{
  std::vector<TokenizedScript::Token>::const_iterator iterator =
    findIteratorOfTokenContainingCharacter(characterIndex, _tokens.end());

  if(iterator==_tokens.end())
    return fallbackValue;

  return iterator->tokenIndex;
}


size_t TokenizedScript::findTokenIndexContainingCharacter(size_t characterIndex) const
{
  return findTokenIndexContainingCharacter(characterIndex, _tokens.size()-1);
}


} // namespace Scripting
} // namespace Base
