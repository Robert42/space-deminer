#include <base/scripting/script-autocompletion.h>

namespace Base {
namespace Scripting {


inline TokenizedScript createTestTokenizedScript()
{
  //  0 - Terminal [ 0,  8[
  //  1 - ::       [ 8, 10[
  //  2 - print    [10, 15[
  //  3 - (        [15, 16[
  //  4 - abc      [16, 19[
  //  5 - ::       [19, 21[
  //  6 - def      [21, 24[
  //  7 - .        [24, 25[
  //  8 - ghi      [25, 28[
  //  9 - (        [28, 29[
  // 10 - )        [29, 30[
  // 11 - ,        [30, 31[
  // 12 -          [31, 32[
  // 13 - hello    [32, 37[
  // 14 - .        [37, 38[
  // 15 - world    [38, 43[
  // 16 - (        [43, 44[
  // 17 - )        [44, 45[
  // 18 - ,        [45, 46[
  // 19 -          [46, 47[
  // 20 - a        [47, 48[
  // 21 - )        [48, 49[
  // 22 - ;        [49, 50[
  TokenizedScript tokenizedScript(std::string("Terminal::print(abc::def.ghi(), hello.world(), a);"));

  return tokenizedScript;
}


TEST(base_scripting, createTestTokenizedScript)
{
  TokenizedScript tokenizedScript = createTestTokenizedScript();

  ASSERT_EQ('T', *tokenizedScript.token(0).begin);
  ASSERT_EQ(':', *tokenizedScript.token(1).begin);
  ASSERT_EQ('p', *tokenizedScript.token(2).begin);
  ASSERT_EQ('(', *tokenizedScript.token(3).begin);
  ASSERT_EQ('a', *tokenizedScript.token(4).begin);
  ASSERT_EQ(':', *tokenizedScript.token(5).begin);
  ASSERT_EQ('d', *tokenizedScript.token(6).begin);
  ASSERT_EQ('.', *tokenizedScript.token(7).begin);
  ASSERT_EQ('g', *tokenizedScript.token(8).begin);
  ASSERT_EQ('(', *tokenizedScript.token(9).begin);
  ASSERT_EQ(')', *tokenizedScript.token(10).begin);
  ASSERT_EQ(',', *tokenizedScript.token(11).begin);
  ASSERT_EQ(' ', *tokenizedScript.token(12).begin);
  ASSERT_EQ('h', *tokenizedScript.token(13).begin);
  ASSERT_EQ('.', *tokenizedScript.token(14).begin);
  ASSERT_EQ('w', *tokenizedScript.token(15).begin);
  ASSERT_EQ('(', *tokenizedScript.token(16).begin);
  ASSERT_EQ(')', *tokenizedScript.token(17).begin);
  ASSERT_EQ(',', *tokenizedScript.token(18).begin);
  ASSERT_EQ(' ', *tokenizedScript.token(19).begin);
  ASSERT_EQ('a', *tokenizedScript.token(20).begin);
}


TEST(base_scripting_Autocompletion, calcIdentifierToBeCompleted)
{
  TokenizedScript tokenizedScript = createTestTokenizedScript();

  EXPECT_EQ("", Autocompletion::calcIdentifierToBeCompleted(tokenizedScript, 0));
  EXPECT_EQ("T", Autocompletion::calcIdentifierToBeCompleted(tokenizedScript, 1));
  EXPECT_EQ("Te", Autocompletion::calcIdentifierToBeCompleted(tokenizedScript, 2));
  EXPECT_EQ("Termina", Autocompletion::calcIdentifierToBeCompleted(tokenizedScript, 7));
  EXPECT_EQ("Terminal", Autocompletion::calcIdentifierToBeCompleted(tokenizedScript, 8));
  EXPECT_EQ("Terminal:", Autocompletion::calcIdentifierToBeCompleted(tokenizedScript, 9));
  EXPECT_EQ("Terminal::", Autocompletion::calcIdentifierToBeCompleted(tokenizedScript, 10));
  EXPECT_EQ("Terminal::p", Autocompletion::calcIdentifierToBeCompleted(tokenizedScript, 11));
  EXPECT_EQ("Terminal::print", Autocompletion::calcIdentifierToBeCompleted(tokenizedScript, 15));
}


TEST(base_scripting_Autocompletion, firstTokenOfIdentifier)
{
  TokenizedScript tokenizedScript = createTestTokenizedScript();

  size_t firstTokenOfName;
  bool isIdentifier;

  firstTokenOfName = Autocompletion::firstTokenOfIdentifier(tokenizedScript, 0, &isIdentifier);
  EXPECT_EQ(0, firstTokenOfName);
  EXPECT_FALSE(isIdentifier);

  firstTokenOfName = Autocompletion::firstTokenOfIdentifier(tokenizedScript, 1, &isIdentifier);
  EXPECT_EQ(0, firstTokenOfName);
  EXPECT_TRUE(isIdentifier);

  firstTokenOfName = Autocompletion::firstTokenOfIdentifier(tokenizedScript, 7, &isIdentifier);
  EXPECT_EQ(0, firstTokenOfName);
  EXPECT_TRUE(isIdentifier);

  firstTokenOfName = Autocompletion::firstTokenOfIdentifier(tokenizedScript, 8, &isIdentifier);
  EXPECT_EQ(0, firstTokenOfName);
  EXPECT_TRUE(isIdentifier);

  firstTokenOfName = Autocompletion::firstTokenOfIdentifier(tokenizedScript, 9, &isIdentifier);
  EXPECT_EQ(0, firstTokenOfName);
  EXPECT_TRUE(isIdentifier);

  firstTokenOfName = Autocompletion::firstTokenOfIdentifier(tokenizedScript, 10, &isIdentifier);
  EXPECT_EQ(0, firstTokenOfName);
  EXPECT_TRUE(isIdentifier);

  firstTokenOfName = Autocompletion::firstTokenOfIdentifier(tokenizedScript, 14, &isIdentifier);
  EXPECT_EQ(0, firstTokenOfName);
  EXPECT_TRUE(isIdentifier);

  firstTokenOfName = Autocompletion::firstTokenOfIdentifier(tokenizedScript, 15, &isIdentifier);
  EXPECT_EQ(0, firstTokenOfName);
  EXPECT_TRUE(isIdentifier);

  firstTokenOfName = Autocompletion::firstTokenOfIdentifier(tokenizedScript, 16, &isIdentifier);
  EXPECT_EQ(3, firstTokenOfName);
  EXPECT_FALSE(isIdentifier);

  firstTokenOfName = Autocompletion::firstTokenOfIdentifier(tokenizedScript, 24, &isIdentifier);
  EXPECT_EQ(4, firstTokenOfName);
  EXPECT_TRUE(isIdentifier);

  firstTokenOfName = Autocompletion::firstTokenOfIdentifier(tokenizedScript, 27, &isIdentifier);
  EXPECT_EQ(4, firstTokenOfName);
  EXPECT_TRUE(isIdentifier);

  firstTokenOfName = Autocompletion::firstTokenOfIdentifier(tokenizedScript, 28, &isIdentifier);
  EXPECT_EQ(4, firstTokenOfName);
  EXPECT_TRUE(isIdentifier);

  firstTokenOfName = Autocompletion::firstTokenOfIdentifier(tokenizedScript, 47, &isIdentifier);
  EXPECT_EQ(19, firstTokenOfName);
  EXPECT_FALSE(isIdentifier);

  firstTokenOfName = Autocompletion::firstTokenOfIdentifier(tokenizedScript, 48, &isIdentifier);
  EXPECT_EQ(20, firstTokenOfName);
  EXPECT_TRUE(isIdentifier);
}


TEST(base_scripting_Autocompletion, isTokenPartOfIdentifier)
{
  TokenizedScript tokenizedScript = createTestTokenizedScript();

  EXPECT_TRUE (Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token( 0)));
  EXPECT_TRUE (Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token( 1)));
  EXPECT_TRUE (Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token( 2)));
  EXPECT_FALSE(Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token( 3)));
  EXPECT_TRUE (Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token( 4)));
  EXPECT_TRUE (Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token( 5)));
  EXPECT_TRUE (Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token( 6)));
  EXPECT_TRUE (Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token( 7)));
  EXPECT_TRUE (Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token( 8)));
  EXPECT_FALSE(Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token( 9)));
  EXPECT_FALSE(Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token(10)));
  EXPECT_FALSE(Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token(11)));
  EXPECT_FALSE(Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token(12)));
  EXPECT_TRUE (Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token(13)));
  EXPECT_TRUE (Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token(14)));
  EXPECT_TRUE (Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token(15)));
  EXPECT_FALSE(Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token(16)));
  EXPECT_FALSE(Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token(17)));
  EXPECT_FALSE(Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token(18)));
  EXPECT_FALSE(Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token(19)));
  EXPECT_TRUE (Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token(20)));
  EXPECT_FALSE(Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token(21)));
  EXPECT_FALSE(Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token(22)));
  EXPECT_FALSE(Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token(23)));

  tokenizedScript = TokenizedScript(std::string("a:b"));

  EXPECT_TRUE(Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token(0)));
  EXPECT_TRUE(Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token(1)));
  EXPECT_TRUE(Autocompletion::isTokenPartOfIdentifier(tokenizedScript.token(2)));
}


TEST(base_scripting_Autocompletion, autocomplete)
{
  Autocompletion autocompletion(Index::createTestIndex());

  auto autocomplete = [&autocompletion](const std::string& text){
    return autocompletion.autoComplete(TokenizedScript(text), text.length());
  };
  auto set = [](const std::initializer_list<std::string>& set){
    return std::set<std::string>(set);
  };

  EXPECT_EQ(set({}), autocomplete("lo"));
  EXPECT_EQ(set({"globalFunctionTheFirst",
                 "globalFunctionTheSecond",
                 "globalFunctionTheThird"}),
            autocomplete("globalF"));
  EXPECT_EQ(set({"NamespaceFoo::",
                 "NamespaceBar::"}),
            autocomplete("Namespa"));
  EXPECT_EQ(set({"NamespaceFoo::"}),
            autocomplete("NamespaceFo"));
  EXPECT_EQ(set({"NamespaceFoo::localFunctionFooTheFirst",
                 "NamespaceFoo::localFunctionFooTheSecond",
                 "NamespaceFoo::localFunctionFooTheThird"}),
            autocomplete("NamespaceFoo"));
  EXPECT_EQ(set({"NamespaceFoo::localFunctionFooTheFirst",
                 "NamespaceFoo::localFunctionFooTheSecond",
                 "NamespaceFoo::localFunctionFooTheThird"}),
            autocomplete("NamespaceFoo:"));
  EXPECT_EQ(set({"NamespaceFoo::localFunctionFooTheFirst",
                 "NamespaceFoo::localFunctionFooTheSecond",
                 "NamespaceFoo::localFunctionFooTheThird"}),
            autocomplete("NamespaceFoo::"));
  EXPECT_EQ(set({"globalFunctionTheFirst",
                 "globalFunctionTheSecond",
                 "globalFunctionTheThird",
                 "NamespaceFoo::",
                 "NamespaceBar::"}),
            autocomplete(""));
  // ISSUE-134 test recognizing typedefs
  // ISSUE-134 test completing excluding a dot, for example "insta" -> "instance"
  // ISSUE-134 test completing up to a dot, for example "instance" -> "instance.a", "instance.b"
}


}
}
