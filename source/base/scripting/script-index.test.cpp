#include <base/scripting/script-index.h>

namespace Base {
namespace Scripting {

const char* Index_Test_Module_Script_Source =
R"scriptSource(
void globalFunctionTheFirst(){}
void globalFunctionTheSecond(){}
void globalFunctionTheThird(){}

namespace NamespaceFoo
{
void localFunctionFooTheFirst(){}
void localFunctionFooTheSecond(){}
void localFunctionFooTheThird(){}
}

namespace NamespaceBar
{
void localFunctionBarTheFirst(){}
void localFunctionBarTheSecond(){}
void localFunctionBarTheThird(){}
}
)scriptSource";


Index::Ptr Index::createTestIndex()
{
  Ptr index = create();

  AngelScript::asIScriptEngine *engine = Scripting::Engine::angelScriptEngine();

  AngelScript::asIScriptModule *scriptModule = engine->GetModule("test-module", AngelScript::asGM_CREATE_IF_NOT_EXISTS);

  index->insertModule(scriptModule);

  if(scriptModule->GetFunctionByName("globalFunctionTheFirst") == nullptr)
  {
    scriptModule->AddScriptSection("Index_Test_Module_Script_Source", Index_Test_Module_Script_Source);

    int r = scriptModule->Build();
    Scripting::AngelScriptCheck(r);
  }

  return index;
}


} // namespace Scripting
} // namespace Base
