#ifndef BASE_SCRIPTING_SCRIPTINDEX_H
#define BASE_SCRIPTING_SCRIPTINDEX_H

#include "engine.h"

namespace Base {
namespace Scripting {

std::string mergeName(const std::string& namespaceName, const std::string& name);

class Index
{
private:
  class Implementation;
  class EngineGlobalIndex;
  class ModuleIndex;
  class ImplementationList;

  shared_ptr<ImplementationList> implementations;

public:
  typedef std::shared_ptr<Index> Ptr;
  typedef std::shared_ptr<const Index> ConstPtr;

  class Visitor
  {
  public:
    virtual void visitFunction(const AngelScript::asIScriptFunction* function) = 0;
    virtual void visitEnumValue(const char* nameSpace, const char* enumName, const char* valueName, int value) = 0;
  };

  class NameVisitor : public Visitor
  {
  public:
    void visitFunction(const AngelScript::asIScriptFunction* function) final override;
    void visitEnumValue(const char* nameSpace, const char* enumName, const char* valueName, int value) final override;

    virtual void visitFunction(const std::string& name, const AngelScript::asIScriptFunction* function) = 0;
    virtual void visitEnumValue(const std::string& name, int value) = 0;

  private:
    static const char* truncSet(const char* str);
    static const char* truncGet(const char* str);
    static const char* truncGetSet(const char* str, char first);

  };


private:
  Index();

public:
  static Ptr create();
  static Ptr createTestIndex();

  void insertGlobal(AngelScript::asIScriptEngine* engine, uint32 accessMask);
  void insertModule(AngelScript::asIScriptModule* scriptModule);

  void visitAllWith(Visitor& visitor) const;
};

} // namespace Scripting
} // namespace Base

#endif // BASE_SCRIPTING_SCRIPTINDEX_H
