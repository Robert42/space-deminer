#include <base/scripting/script-autocompletion.h>

namespace Base {
namespace Scripting {

class AutocompletionVisitor : public Index::NameVisitor
{
public:
  const std::string identifierToBeCompleted;
  std::set<std::string> result;


public:
  AutocompletionVisitor(const std::string& identifierToBeCompleted)
    : identifierToBeCompleted(identifierToBeCompleted)
  {
  }


public:
  void visitFunction(const std::string& name, const AngelScript::asIScriptFunction*) override
  {
    addName(name);
  }

  void visitEnumValue(const std::string& name, int) override
  {
    addName(name);
  }

private:
  void addName(std::string name)
  {
    size_t nextNamespaceSeperator = name.find("::", identifierToBeCompleted.length()+1, 2);
    if(nextNamespaceSeperator < name.length())
      name = name.substr(0, nextNamespaceSeperator+2);

    bool beginsWithIdentifierToBeCompleted = name.length() >= identifierToBeCompleted.length() &&
                                             name.substr(0, identifierToBeCompleted.length()) == identifierToBeCompleted;

    if(!beginsWithIdentifierToBeCompleted)
      return;

    result.insert(name);
  }
};


Autocompletion::Autocompletion(const Index::ConstPtr& index) :
  index(index)
{
}


std::set<std::string> Autocompletion::autoComplete(const TokenizedScript& tokenizedScript,
                                                   size_t cursor,
                                                   const std::function<void(std::set<std::string>& result,const std::string& identifierToBeCompleted)>& modifyResult) const
{
  std::string identifierToBeCompleted = calcIdentifierToBeCompleted(tokenizedScript, cursor);

  AutocompletionVisitor visitor(identifierToBeCompleted);
  index->visitAllWith(visitor);

  modifyResult(visitor.result, identifierToBeCompleted);

  return visitor.result;
}


std::set<std::string> Autocompletion::autoComplete(const TokenizedScript& tokenizedScript,
                                                   size_t cursor) const
{
  return autoComplete(tokenizedScript,
                      cursor,
                      [](std::set<std::string>&,const std::string&){});
}


std::string Autocompletion::calcIdentifierToBeCompleted(const TokenizedScript& tokenizedScript,
                                                        size_t cursor)
{
  bool isIdentifier;
  size_t tokenId = firstTokenOfIdentifier(tokenizedScript, cursor, &isIdentifier);

  if(!isIdentifier)
    return std::string();

  const TokenizedScript::Token& firstToken = tokenizedScript.token(tokenId);
  const char* begin = firstToken.begin;
  const char* end = begin+cursor-firstToken.indexBegin;

  return std::string(begin, end);
}


size_t Autocompletion::firstTokenOfIdentifier(const TokenizedScript& tokenizedScript,
                                              size_t cursor,
                                              bool* isIdentifier)
{
  bool dummy;
  if(isIdentifier == nullptr)
    isIdentifier = &dummy;
  *isIdentifier = false;

  if(cursor == 0)
    return 0;

  --cursor;

  const size_t firstTokenId = tokenizedScript.findTokenIndexContainingCharacter(cursor);

  const TokenizedScript::Token& firstToken = tokenizedScript.token(firstTokenId);

  if(!isTokenPartOfIdentifier(firstToken))
    return firstTokenId;

  *isIdentifier = true;

  size_t tokenId = firstTokenId;
  while(tokenId > 0)
  {
    --tokenId;

    const TokenizedScript::Token& token = tokenizedScript.token(tokenId);

    if(!isTokenPartOfIdentifier(token))
      return tokenId+1;
  }

  return 0;
}


bool Autocompletion::isTokenPartOfIdentifier(const TokenizedScript::Token& token)
{
  if(*token.begin == ':' && token.length()==1)
    return true;

  switch(token.type.value)
  {
  case TokenizedScript::Token::Type::IDENTIFIER:
  case TokenizedScript::Token::Type::KEYWORD_DOT:
  case TokenizedScript::Token::Type::KEYWORD_NAMESPACE_SEPERATOR:
  case TokenizedScript::Token::Type::KEYWORD_WORD:
    return true;
  default:
    return false;
  }
}


} // namespace Scripting
} // namespace Base
