#ifndef _BASE_SCRIPTING_ENGINE_H_
#define _BASE_SCRIPTING_ENGINE_H_

#include <base/signals/signal.h>
#include <base/strings/string.h>
#include <base/io/regular-file.h>
#include <base/singleton.h>

namespace Base {
namespace Scripting {
namespace AccessMasks {
const uint32 GLOBAL = 1;
const uint32 MATH = 2;
const uint32 ALL = 0xffffffff;
}


class Engine : public Singleton<Engine>
{
public:
  class MessageInfo
  {
  public:
    typedef AngelScript::asEMsgType Type;

    MessageInfo(const AngelScript::asSMessageInfo& messageInfo);

    String section;
    int row;
    int column;
    Type type;
    String message;

    String format() const;
  };

private:
  AngelScript::asIScriptEngine* as_engine;
  Signals::CallableSignal<bool(const MessageInfo&)> _scriptMessageSignal;

public:
  Engine();
  ~Engine();

public:
  static Signals::Signal<bool(const MessageInfo&)>& scriptMessageSignal();

  static AngelScript::asIScriptEngine* angelScriptEngine();

private:
  void sendMessageSignal(const AngelScript::asSMessageInfo* messageInfo);

  static bool logMessage(const MessageInfo& messageInfo);
  static bool printMessage(const MessageInfo& messageInfo);

  void initMathLibrary();
  void initFormatLibrary();
  void initLoggingLibrary();

  void registerMessageHandlerLogger();
  void registerMessageHandlerPrinter();
};

void AngelScriptCheck(int returnCode);
void AngelScriptCheckThrowingException(int returnCode);

int executeWithTimeout(AngelScript::asIScriptContext* context, const std::chrono::microseconds& time);

void executeScriptThrowingException(const std::string& scriptSource, const std::chrono::microseconds& time, AngelScript::asDWORD accessMask, const char* mainMethod="main");
void executeScriptThrowingException(const IO::RegularFile& scriptFile, const std::chrono::microseconds& time, AngelScript::asDWORD accessMask, const char* mainMethod="main");
void executeScript(const IO::RegularFile& scriptFile, const std::chrono::microseconds& time, AngelScript::asDWORD accessMask, const char* mainMethod="main");


} // namespace Scripting
} // namespace Base

#endif
