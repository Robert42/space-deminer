#include "engine-library-format.h"
#include <base/io/log.h>

namespace Base {
namespace Scripting {


void logFromScript(const std::string& s)
{
  IO::Log::logUtf8String(s);
}


void logFromScript(const FormatedValue& v)
{
  IO::Log::logUtf8String(v.get());
}


void logFromScript(const std::string& s, const FormatedValue& v1)
{
  IO::Log::logUtf8String(format(s, v1));
}


void logFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2)
{
  IO::Log::logUtf8String(format(s, v1, v2));
}


void logFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3)
{
  IO::Log::logUtf8String(format(s, v1, v2, v3));
}


void logFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4)
{
  IO::Log::logUtf8String(format(s, v1, v2, v3, v4));
}


void logFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5)
{
  IO::Log::logUtf8String(format(s, v1, v2, v3, v4, v5));
}


void logFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6)
{
  IO::Log::logUtf8String(format(s, v1, v2, v3, v4, v5, v6));
}


void logFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6, const FormatedValue& v7)
{
  IO::Log::logUtf8String(format(s, v1, v2, v3, v4, v5, v6, v7));
}


void logFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6, const FormatedValue& v7, const FormatedValue& v8)
{
  IO::Log::logUtf8String(format(s, v1, v2, v3, v4, v5, v6, v7, v8));
}


void logFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6, const FormatedValue& v7, const FormatedValue& v8, const FormatedValue& v9)
{
  IO::Log::logUtf8String(format(s, v1, v2, v3, v4, v5, v6, v7, v8, v9));
}


void logFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6, const FormatedValue& v7, const FormatedValue& v8, const FormatedValue& v9, const FormatedValue& v10)
{
  IO::Log::logUtf8String(format(s, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10));
}


void logWarningFromScript(const std::string& s)
{
  IO::Log::logWarningUtf8String(s);
}


void logWarningFromScript(const FormatedValue& v)
{
  IO::Log::logWarningUtf8String(v.get());
}


void logWarningFromScript(const std::string& s, const FormatedValue& v1)
{
  IO::Log::logWarningUtf8String(format(s, v1));
}


void logWarningFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2)
{
  IO::Log::logWarningUtf8String(format(s, v1, v2));
}


void logWarningFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3)
{
  IO::Log::logWarningUtf8String(format(s, v1, v2, v3));
}


void logWarningFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4)
{
  IO::Log::logWarningUtf8String(format(s, v1, v2, v3, v4));
}


void logWarningFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5)
{
  IO::Log::logWarningUtf8String(format(s, v1, v2, v3, v4, v5));
}


void logWarningFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6)
{
  IO::Log::logWarningUtf8String(format(s, v1, v2, v3, v4, v5, v6));
}


void logWarningFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6, const FormatedValue& v7)
{
  IO::Log::logWarningUtf8String(format(s, v1, v2, v3, v4, v5, v6, v7));
}


void logWarningFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6, const FormatedValue& v7, const FormatedValue& v8)
{
  IO::Log::logWarningUtf8String(format(s, v1, v2, v3, v4, v5, v6, v7, v8));
}


void logWarningFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6, const FormatedValue& v7, const FormatedValue& v8, const FormatedValue& v9)
{
  IO::Log::logWarningUtf8String(format(s, v1, v2, v3, v4, v5, v6, v7, v8, v9));
}


void logWarningFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6, const FormatedValue& v7, const FormatedValue& v8, const FormatedValue& v9, const FormatedValue& v10)
{
  IO::Log::logWarningUtf8String(format(s, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10));
}


void logErrorFromScript(const std::string& s)
{
  IO::Log::logErrorUtf8String(s);
}


void logErrorFromScript(const FormatedValue& v)
{
  IO::Log::logErrorUtf8String(v.get());
}


void logErrorFromScript(const std::string& s, const FormatedValue& v1)
{
  IO::Log::logErrorUtf8String(format(s, v1));
}


void logErrorFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2)
{
  IO::Log::logErrorUtf8String(format(s, v1, v2));
}


void logErrorFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3)
{
  IO::Log::logErrorUtf8String(format(s, v1, v2, v3));
}


void logErrorFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4)
{
  IO::Log::logErrorUtf8String(format(s, v1, v2, v3, v4));
}


void logErrorFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5)
{
  IO::Log::logErrorUtf8String(format(s, v1, v2, v3, v4, v5));
}


void logErrorFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6)
{
  IO::Log::logErrorUtf8String(format(s, v1, v2, v3, v4, v5, v6));
}


void logErrorFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6, const FormatedValue& v7)
{
  IO::Log::logErrorUtf8String(format(s, v1, v2, v3, v4, v5, v6, v7));
}


void logErrorFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6, const FormatedValue& v7, const FormatedValue& v8)
{
  IO::Log::logErrorUtf8String(format(s, v1, v2, v3, v4, v5, v6, v7, v8));
}


void logErrorFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6, const FormatedValue& v7, const FormatedValue& v8, const FormatedValue& v9)
{
  IO::Log::logErrorUtf8String(format(s, v1, v2, v3, v4, v5, v6, v7, v8, v9));
}


void logErrorFromScript(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6, const FormatedValue& v7, const FormatedValue& v8, const FormatedValue& v9, const FormatedValue& v10)
{
  IO::Log::logErrorUtf8String(format(s, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10));
}



void Engine::initLoggingLibrary()
{
  int r=0;

  as_engine->SetDefaultAccessMask(AccessMasks::ALL);

  r = as_engine->RegisterGlobalFunction("void log(string &in str)", AngelScript::asFUNCTIONPR(logFromScript, (const std::string&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void log(FormatedValue &in v)", AngelScript::asFUNCTIONPR(logFromScript, (const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void log(const string &in str, FormatedValue &in v1)", AngelScript::asFUNCTIONPR(logFromScript, (const std::string&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void log(const string &in str, FormatedValue &in v1, FormatedValue &in v2)", AngelScript::asFUNCTIONPR(logFromScript, (const std::string&, const FormatedValue&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void log(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3)", AngelScript::asFUNCTIONPR(logFromScript, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void log(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4)", AngelScript::asFUNCTIONPR(logFromScript, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void log(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5)", AngelScript::asFUNCTIONPR(logFromScript, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void log(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, FormatedValue &in v6)", AngelScript::asFUNCTIONPR(logFromScript, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void log(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, FormatedValue &in v6, FormatedValue &in v7)", AngelScript::asFUNCTIONPR(logFromScript, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void log(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, FormatedValue &in v6, FormatedValue &in v7, FormatedValue &in v8)", AngelScript::asFUNCTIONPR(logFromScript, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void log(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, FormatedValue &in v6, FormatedValue &in v7, FormatedValue &in v8, FormatedValue &in v9)", AngelScript::asFUNCTIONPR(logFromScript, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void log(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, FormatedValue &in v6, FormatedValue &in v7, FormatedValue &in v8, FormatedValue &in v9, FormatedValue &in v10)", AngelScript::asFUNCTIONPR(logFromScript, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&),void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);

  r = as_engine->RegisterGlobalFunction("void logWarning(string &in str)", AngelScript::asFUNCTIONPR(logWarningFromScript, (const std::string&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void logWarning(FormatedValue &in v)", AngelScript::asFUNCTIONPR(logWarningFromScript, (const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void logWarning(const string &in str, FormatedValue &in v1)", AngelScript::asFUNCTIONPR(logWarningFromScript, (const std::string&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void logWarning(const string &in str, FormatedValue &in v1, FormatedValue &in v2)", AngelScript::asFUNCTIONPR(logWarningFromScript, (const std::string&, const FormatedValue&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void logWarning(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3)", AngelScript::asFUNCTIONPR(logWarningFromScript, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void logWarning(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4)", AngelScript::asFUNCTIONPR(logWarningFromScript, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void logWarning(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5)", AngelScript::asFUNCTIONPR(logWarningFromScript, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void logWarning(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, FormatedValue &in v6)", AngelScript::asFUNCTIONPR(logWarningFromScript, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void logWarning(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, FormatedValue &in v6, FormatedValue &in v7)", AngelScript::asFUNCTIONPR(logWarningFromScript, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void logWarning(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, FormatedValue &in v6, FormatedValue &in v7, FormatedValue &in v8)", AngelScript::asFUNCTIONPR(logWarningFromScript, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void logWarning(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, FormatedValue &in v6, FormatedValue &in v7, FormatedValue &in v8, FormatedValue &in v9)", AngelScript::asFUNCTIONPR(logWarningFromScript, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void logWarning(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, FormatedValue &in v6, FormatedValue &in v7, FormatedValue &in v8, FormatedValue &in v9, FormatedValue &in v10)", AngelScript::asFUNCTIONPR(logWarningFromScript, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&),void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);

  r = as_engine->RegisterGlobalFunction("void logError(string &in str)", AngelScript::asFUNCTIONPR(logErrorFromScript, (const std::string&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void logError(FormatedValue &in v)", AngelScript::asFUNCTIONPR(logErrorFromScript, (const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void logError(const string &in str, FormatedValue &in v1)", AngelScript::asFUNCTIONPR(logErrorFromScript, (const std::string&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void logError(const string &in str, FormatedValue &in v1, FormatedValue &in v2)", AngelScript::asFUNCTIONPR(logErrorFromScript, (const std::string&, const FormatedValue&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void logError(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3)", AngelScript::asFUNCTIONPR(logErrorFromScript, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void logError(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4)", AngelScript::asFUNCTIONPR(logErrorFromScript, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void logError(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5)", AngelScript::asFUNCTIONPR(logErrorFromScript, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void logError(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, FormatedValue &in v6)", AngelScript::asFUNCTIONPR(logErrorFromScript, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void logError(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, FormatedValue &in v6, FormatedValue &in v7)", AngelScript::asFUNCTIONPR(logErrorFromScript, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void logError(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, FormatedValue &in v6, FormatedValue &in v7, FormatedValue &in v8)", AngelScript::asFUNCTIONPR(logErrorFromScript, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void logError(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, FormatedValue &in v6, FormatedValue &in v7, FormatedValue &in v8, FormatedValue &in v9)", AngelScript::asFUNCTIONPR(logErrorFromScript, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void logError(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, FormatedValue &in v6, FormatedValue &in v7, FormatedValue &in v8, FormatedValue &in v9, FormatedValue &in v10)", AngelScript::asFUNCTIONPR(logErrorFromScript, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&),void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
}


} // namespace Scripting
} // namespace Base
