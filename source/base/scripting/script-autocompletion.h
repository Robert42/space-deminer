#ifndef BASE_SCRIPTING_SCRIPTAUTOCOMPLETION_H
#define BASE_SCRIPTING_SCRIPTAUTOCOMPLETION_H

#include "script-index.h"
#include "tokenized-script.h"

namespace Base {
namespace Scripting {


class Autocompletion
{
private:
  Index::ConstPtr index;

public:
  Autocompletion(const Index::ConstPtr& index);

  std::set<std::string> autoComplete(const TokenizedScript& tokenizedScript,
                                     size_t cursor,
                                     const std::function<void(std::set<std::string>& result,const std::string& identifierToBeCompleted)>& modifyResult) const;
  std::set<std::string> autoComplete(const TokenizedScript& tokenizedScript,
                                     size_t cursor) const;

private:
  FRIEND_TEST(base_scripting, Autocompletion_calcIdentifierToBeCompleted);
  FRIEND_TEST(base_scripting, Autocompletion_firstTokenOfIdentifier);
  FRIEND_TEST(base_scripting, Autocompletion_isTokenPartOfIdentifier);

  static std::string calcIdentifierToBeCompleted(const TokenizedScript& tokenizedScript, size_t cursor);

  static size_t firstTokenOfIdentifier(const TokenizedScript& tokenizedScript,
                                       size_t cursor,
                                       bool* isIdentifier = nullptr);
  static bool isTokenPartOfIdentifier(const TokenizedScript::Token& token);
};


} // namespace Scripting
} // namespace Base

#endif // BASE_SCRIPTING_AUTOCOMPLETION_H
