#include "engine-library-format.h"

namespace Base {
namespace Scripting {

std::ostream& operator <<(std::ostream& s, const FormatedValue& fv)
{
  return s << fv.get();
}


template<typename T>
FormatedValue FormatedValue_from(const T* v)
{
  return FormatedValue(*v);
}


std::string format(const std::string& s)
{
  return s;
}


std::string format(const FormatedValue& f)
{
  return f.get();
}


std::string format(const std::string& s, const FormatedValue& v1)
{
  return String::compose(String::fromUtf8(s), v1).toUtf8String();
}


std::string format(const std::string& s, const FormatedValue& v1, const FormatedValue& v2)
{
  return String::compose(String::fromUtf8(s), v1, v2).toUtf8String();
}


std::string format(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3)
{
  return String::compose(String::fromUtf8(s), v1, v2, v3).toUtf8String();
}


std::string format(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4)
{
  return String::compose(String::fromUtf8(s), v1, v2, v3, v4).toUtf8String();
}


std::string format(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5)
{
  return String::compose(String::fromUtf8(s), v1, v2, v3, v4, v5).toUtf8String();
}


std::string format(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6)
{
  return String::compose(String::fromUtf8(s), v1, v2, v3, v4, v5, v6).toUtf8String();
}


std::string format(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6, const FormatedValue& v7)
{
  return String::compose(String::fromUtf8(s), v1, v2, v3, v4, v5, v6, v7).toUtf8String();
}


std::string format(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6, const FormatedValue& v7, const FormatedValue& v8)
{
  return String::compose(String::fromUtf8(s), v1, v2, v3, v4, v5, v6, v7, v8).toUtf8String();
}


std::string format(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6, const FormatedValue& v7, const FormatedValue& v8, const FormatedValue& v9)
{
  return String::compose(String::fromUtf8(s), v1, v2, v3, v4, v5, v6, v7, v8, v9).toUtf8String();
}


std::string format(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6, const FormatedValue& v7, const FormatedValue& v8, const FormatedValue& v9, const FormatedValue& v10)
{
  return String::compose(String::fromUtf8(s), v1, v2, v3, v4, v5, v6, v7, v8, v9, v10).toUtf8String();
}



void Engine::initFormatLibrary()
{
  as_engine->SetDefaultAccessMask(AccessMasks::ALL);

  int r;

  // Register the vec2 class
  // asOBJ_VALUE because it's a value type
  // asOBJ_APP_CLASS because its a class
  // asOBJ_APP_CLASS_CONSTRUCTOR because it has a default contructor
  // asOBJ_APP_CLASS_ASSIGNMENT because it's able to assign vectors to other vectors
  // asOBJ_APP_CLASS_COPY_CONSTRUCTOR because it has a copy constructor
  r = as_engine->RegisterObjectType("FormatedValue",
                                    sizeof(FormatedValue),
                                    AngelScript::asOBJ_VALUE |
                                    AngelScript::asOBJ_APP_CLASS |
                                    AngelScript::asOBJ_APP_CLASS_CONSTRUCTOR |
                                    AngelScript::asOBJ_APP_CLASS_ASSIGNMENT |
                                    AngelScript::asOBJ_APP_CLASS_COPY_CONSTRUCTOR);
  AngelScriptCheck(r);

  r = as_engine->RegisterObjectBehaviour("vec2",   AngelScript::asBEHAVE_IMPLICIT_VALUE_CAST, "FormatedValue f()", AngelScript::asFUNCTIONPR(FormatedValue_from, (const vec2*), FormatedValue), AngelScript::asCALL_CDECL_OBJFIRST);
  AngelScriptCheck(r);
  r = as_engine->RegisterObjectBehaviour("vec3",   AngelScript::asBEHAVE_IMPLICIT_VALUE_CAST, "FormatedValue f()", AngelScript::asFUNCTIONPR(FormatedValue_from, (const vec3*), FormatedValue), AngelScript::asCALL_CDECL_OBJFIRST);
  AngelScriptCheck(r);
  r = as_engine->RegisterObjectBehaviour("vec4",   AngelScript::asBEHAVE_IMPLICIT_VALUE_CAST, "FormatedValue f()", AngelScript::asFUNCTIONPR(FormatedValue_from, (const vec4*), FormatedValue), AngelScript::asCALL_CDECL_OBJFIRST);
  AngelScriptCheck(r);
  r = as_engine->RegisterObjectBehaviour("dvec2",   AngelScript::asBEHAVE_IMPLICIT_VALUE_CAST, "FormatedValue f()", AngelScript::asFUNCTIONPR(FormatedValue_from, (const dvec2*), FormatedValue), AngelScript::asCALL_CDECL_OBJFIRST);
  AngelScriptCheck(r);
  r = as_engine->RegisterObjectBehaviour("dvec3",   AngelScript::asBEHAVE_IMPLICIT_VALUE_CAST, "FormatedValue f()", AngelScript::asFUNCTIONPR(FormatedValue_from, (const dvec3*), FormatedValue), AngelScript::asCALL_CDECL_OBJFIRST);
  AngelScriptCheck(r);
  r = as_engine->RegisterObjectBehaviour("dvec4",   AngelScript::asBEHAVE_IMPLICIT_VALUE_CAST, "FormatedValue f()", AngelScript::asFUNCTIONPR(FormatedValue_from, (const dvec4*), FormatedValue), AngelScript::asCALL_CDECL_OBJFIRST);
  AngelScriptCheck(r);
  r = as_engine->RegisterObjectBehaviour("bvec2",   AngelScript::asBEHAVE_IMPLICIT_VALUE_CAST, "FormatedValue f()", AngelScript::asFUNCTIONPR(FormatedValue_from, (const bvec2*), FormatedValue), AngelScript::asCALL_CDECL_OBJFIRST);
  AngelScriptCheck(r);
  r = as_engine->RegisterObjectBehaviour("bvec3",   AngelScript::asBEHAVE_IMPLICIT_VALUE_CAST, "FormatedValue f()", AngelScript::asFUNCTIONPR(FormatedValue_from, (const bvec3*), FormatedValue), AngelScript::asCALL_CDECL_OBJFIRST);
  AngelScriptCheck(r);
  r = as_engine->RegisterObjectBehaviour("bvec4",   AngelScript::asBEHAVE_IMPLICIT_VALUE_CAST, "FormatedValue f()", AngelScript::asFUNCTIONPR(FormatedValue_from, (const bvec4*), FormatedValue), AngelScript::asCALL_CDECL_OBJFIRST);
  AngelScriptCheck(r);
  r = as_engine->RegisterObjectBehaviour("ivec2",   AngelScript::asBEHAVE_IMPLICIT_VALUE_CAST, "FormatedValue f()", AngelScript::asFUNCTIONPR(FormatedValue_from, (const ivec2*), FormatedValue), AngelScript::asCALL_CDECL_OBJFIRST);
  AngelScriptCheck(r);
  r = as_engine->RegisterObjectBehaviour("ivec3",   AngelScript::asBEHAVE_IMPLICIT_VALUE_CAST, "FormatedValue f()", AngelScript::asFUNCTIONPR(FormatedValue_from, (const ivec3*), FormatedValue), AngelScript::asCALL_CDECL_OBJFIRST);
  AngelScriptCheck(r);
  r = as_engine->RegisterObjectBehaviour("ivec4",   AngelScript::asBEHAVE_IMPLICIT_VALUE_CAST, "FormatedValue f()", AngelScript::asFUNCTIONPR(FormatedValue_from, (const ivec4*), FormatedValue), AngelScript::asCALL_CDECL_OBJFIRST);
  AngelScriptCheck(r);
  r = as_engine->RegisterObjectBehaviour("uvec2",   AngelScript::asBEHAVE_IMPLICIT_VALUE_CAST, "FormatedValue f()", AngelScript::asFUNCTIONPR(FormatedValue_from, (const uvec2*), FormatedValue), AngelScript::asCALL_CDECL_OBJFIRST);
  AngelScriptCheck(r);
  r = as_engine->RegisterObjectBehaviour("uvec3",   AngelScript::asBEHAVE_IMPLICIT_VALUE_CAST, "FormatedValue f()", AngelScript::asFUNCTIONPR(FormatedValue_from, (const uvec3*), FormatedValue), AngelScript::asCALL_CDECL_OBJFIRST);
  AngelScriptCheck(r);
  r = as_engine->RegisterObjectBehaviour("uvec4",   AngelScript::asBEHAVE_IMPLICIT_VALUE_CAST, "FormatedValue f()", AngelScript::asFUNCTIONPR(FormatedValue_from, (const uvec4*), FormatedValue), AngelScript::asCALL_CDECL_OBJFIRST);
  AngelScriptCheck(r);
  r = as_engine->RegisterObjectBehaviour("string", AngelScript::asBEHAVE_IMPLICIT_VALUE_CAST, "FormatedValue f()", AngelScript::asFUNCTIONPR(FormatedValue_from, (const std::string*), FormatedValue), AngelScript::asCALL_CDECL_OBJFIRST);
  AngelScriptCheck(r);
  r = as_engine->RegisterObjectBehaviour("FormatedValue",    AngelScript::asBEHAVE_CONSTRUCT, "void f(int &in x)", AngelScript::asMETHODPR(FormatedValue, init, (const int&), void), AngelScript::asCALL_THISCALL);
  AngelScriptCheck(r);
  r = as_engine->RegisterObjectBehaviour("FormatedValue",  AngelScript::asBEHAVE_CONSTRUCT, "void f(float &in x)", AngelScript::asMETHODPR(FormatedValue, init, (const float&), void), AngelScript::asCALL_THISCALL);
  AngelScriptCheck(r);
  r = as_engine->RegisterObjectBehaviour("FormatedValue",  AngelScript::asBEHAVE_CONSTRUCT, "void f(bool &in x)", AngelScript::asMETHODPR(FormatedValue, init, (const bool&), void), AngelScript::asCALL_THISCALL);
  AngelScriptCheck(r);
  r = as_engine->RegisterObjectBehaviour("FormatedValue", AngelScript::asBEHAVE_CONSTRUCT, "void f(FormatedValue &in fv)", AngelScript::asMETHODPR(FormatedValue, init, (const FormatedValue&), void), AngelScript::asCALL_THISCALL);
  AngelScriptCheck(r);
  r = as_engine->RegisterObjectBehaviour("FormatedValue", AngelScript::asBEHAVE_CONSTRUCT, "void f()", AngelScript::asMETHODPR(FormatedValue, init, (), void), AngelScript::asCALL_THISCALL);
  AngelScriptCheck(r);
  r = as_engine->RegisterObjectBehaviour("FormatedValue", AngelScript::asBEHAVE_DESTRUCT,  "void f()", AngelScript::asMETHOD(FormatedValue, deinit), AngelScript::asCALL_THISCALL);
  AngelScriptCheck(r);
  r = as_engine->RegisterObjectMethod("FormatedValue", "void opAssign(FormatedValue &in fv)", AngelScript::asMETHODPR(FormatedValue, set, (const FormatedValue&), void), AngelScript::asCALL_THISCALL);
  AngelScriptCheck(r);
  r = as_engine->RegisterObjectMethod("FormatedValue", "string get_str()", AngelScript::asMETHOD(FormatedValue, copy), AngelScript::asCALL_THISCALL);
  AngelScriptCheck(r);

  r = as_engine->RegisterGlobalFunction("string format(string &in str)", AngelScript::asFUNCTIONPR(format, (const std::string&), std::string), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("string format(FormatedValue &in v)", AngelScript::asFUNCTIONPR(format, (const FormatedValue&), std::string), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("string format(const string &in str, FormatedValue &in v1)", AngelScript::asFUNCTIONPR(format, (const std::string&, const FormatedValue&), std::string), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("string format(const string &in str, FormatedValue &in v1, FormatedValue &in v2)", AngelScript::asFUNCTIONPR(format, (const std::string&, const FormatedValue&, const FormatedValue&), std::string), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("string format(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3)", AngelScript::asFUNCTIONPR(format, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&), std::string), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("string format(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4)", AngelScript::asFUNCTIONPR(format, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&), std::string), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("string format(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5)", AngelScript::asFUNCTIONPR(format, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&), std::string), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("string format(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, FormatedValue &in v6)", AngelScript::asFUNCTIONPR(format, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&), std::string), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("string format(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, FormatedValue &in v6, FormatedValue &in v7)", AngelScript::asFUNCTIONPR(format, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&), std::string), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("string format(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, FormatedValue &in v6, FormatedValue &in v7, FormatedValue &in v8)", AngelScript::asFUNCTIONPR(format, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&), std::string), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("string format(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, FormatedValue &in v6, FormatedValue &in v7, FormatedValue &in v8, FormatedValue &in v9)", AngelScript::asFUNCTIONPR(format, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&), std::string), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("string format(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, FormatedValue &in v6, FormatedValue &in v7, FormatedValue &in v8, FormatedValue &in v9, FormatedValue &in v10)", AngelScript::asFUNCTIONPR(format, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&), std::string), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
}


} // namespace Scripting
} // namespace Base
