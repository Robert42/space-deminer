#include <base/scripting/engine.h>
#include <base/io/log.h>
#include <base/threads.h>

namespace Base {
namespace Scripting {

void initVectorLibrary_classes(AngelScript::asIScriptEngine* as_engine);
void initVectorLibrary_swizzle_operators_vec_xyzw(AngelScript::asIScriptEngine* as_engine);
void initVectorLibrary_swizzle_operators_vec_rgba(AngelScript::asIScriptEngine* as_engine);
void initVectorLibrary_swizzle_operators_vec_stpq(AngelScript::asIScriptEngine* as_engine);
void initVectorLibrary_swizzle_operators_dvec_xyzw(AngelScript::asIScriptEngine* as_engine);
void initVectorLibrary_swizzle_operators_dvec_rgba(AngelScript::asIScriptEngine* as_engine);
void initVectorLibrary_swizzle_operators_dvec_stpq(AngelScript::asIScriptEngine* as_engine);
void initVectorLibrary_swizzle_operators_ivec_xyzw(AngelScript::asIScriptEngine* as_engine);
void initVectorLibrary_swizzle_operators_ivec_rgba(AngelScript::asIScriptEngine* as_engine);
void initVectorLibrary_swizzle_operators_ivec_stpq(AngelScript::asIScriptEngine* as_engine);
void initVectorLibrary_swizzle_operators_uvec_xyzw(AngelScript::asIScriptEngine* as_engine);
void initVectorLibrary_swizzle_operators_uvec_rgba(AngelScript::asIScriptEngine* as_engine);
void initVectorLibrary_swizzle_operators_uvec_stpq(AngelScript::asIScriptEngine* as_engine);
void initVectorLibrary_swizzle_operators_bvec_xyzw(AngelScript::asIScriptEngine* as_engine);
void initVectorLibrary_swizzle_operators_bvec_rgba(AngelScript::asIScriptEngine* as_engine);
void initVectorLibrary_swizzle_operators_bvec_stpq(AngelScript::asIScriptEngine* as_engine);

Engine::Engine()
{
  int r;

  r = AngelScript::asPrepareMultithread();
  AngelScriptCheck(r);

  as_engine = AngelScript::asCreateScriptEngine(ANGELSCRIPT_VERSION);
  assert(as_engine != nullptr);

  r = as_engine->SetMessageCallback(AngelScript::asMETHOD(Engine, sendMessageSignal),
                                    this,
                                    AngelScript::asCALL_THISCALL);
  AngelScriptCheck(r);

  r = as_engine->SetEngineProperty(AngelScript::asEP_REQUIRE_ENUM_SCOPE, 1);
  AngelScriptCheck(r);

  uint32 oldAccessMask = as_engine->SetDefaultAccessMask(AccessMasks::ALL);


  AngelScript::RegisterStdString(as_engine);

  initMathLibrary();
  initFormatLibrary();
  initLoggingLibrary();

  as_engine->SetDefaultAccessMask(oldAccessMask);

  registerMessageHandlerLogger();
  registerMessageHandlerPrinter();
}


Engine::~Engine()
{
  int r;

  r = as_engine->Release();
  AngelScriptCheck(r);

  AngelScript::asUnprepareMultithread();
}


void Engine::initMathLibrary()
{
  as_engine->SetDefaultAccessMask(AccessMasks::MATH);
  initVectorLibrary_classes(as_engine);
  initVectorLibrary_swizzle_operators_vec_xyzw(as_engine);
  initVectorLibrary_swizzle_operators_vec_rgba(as_engine);
  initVectorLibrary_swizzle_operators_vec_stpq(as_engine);
  initVectorLibrary_swizzle_operators_dvec_xyzw(as_engine);
  initVectorLibrary_swizzle_operators_dvec_rgba(as_engine);
  initVectorLibrary_swizzle_operators_dvec_stpq(as_engine);
  initVectorLibrary_swizzle_operators_ivec_xyzw(as_engine);
  initVectorLibrary_swizzle_operators_ivec_rgba(as_engine);
  initVectorLibrary_swizzle_operators_ivec_stpq(as_engine);
  initVectorLibrary_swizzle_operators_uvec_xyzw(as_engine);
  initVectorLibrary_swizzle_operators_uvec_rgba(as_engine);
  initVectorLibrary_swizzle_operators_uvec_stpq(as_engine);
  initVectorLibrary_swizzle_operators_bvec_xyzw(as_engine);
  initVectorLibrary_swizzle_operators_bvec_rgba(as_engine);
  initVectorLibrary_swizzle_operators_bvec_stpq(as_engine);
}


void Engine::registerMessageHandlerLogger()
{
  scriptMessageSignal().connect(logMessage).trackManually();
}


void Engine::registerMessageHandlerPrinter()
{
#if OGRE_PLATFORM != OGRE_PLATFORM_WIN32
  scriptMessageSignal().connect(printMessage).trackManually();
#endif
}


Signals::Signal<bool(const Engine::MessageInfo&)>& Engine::scriptMessageSignal()
{
  return singleton()._scriptMessageSignal;
}


void Engine::sendMessageSignal(const AngelScript::asSMessageInfo* messageInfo)
{
  _scriptMessageSignal(*messageInfo);
}


AngelScript::asIScriptEngine* Engine::angelScriptEngine()
{
  return singleton().as_engine;
}

bool Engine::logMessage(const MessageInfo& messageInfo)
{
  IO::Log::log(messageInfo.format()+"\n");
  return false;
}

bool Engine::printMessage(const MessageInfo& messageInfo)
{
  std::cerr << messageInfo.format() << std::endl;
  return false;
}


Engine::MessageInfo::MessageInfo(const AngelScript::asSMessageInfo& messageInfo)
{
  this->section = String::fromUtf8(messageInfo.section);
  this->row = messageInfo.row;
  this->column = messageInfo.col;
  this->type = messageInfo.type;
  this->message = String::fromUtf8(messageInfo.message);
}

String Engine::MessageInfo::format() const
{
  String type;
  switch(this->type)
  {
  case AngelScript::asMSGTYPE_ERROR:
    type = "Error";
    break;
  case AngelScript::asMSGTYPE_WARNING:
    type = "Warning";
    break;
  case AngelScript::asMSGTYPE_INFORMATION:
  default:
    type = "Information";
  }

  return String::compose("Angelscript-%0: (section: %1 line: %2 column: %3): %4",
                         type,
                         section,
                         row,
                         column,
                         message);
}

std::string AngelScriptReturnCodeAsString(AngelScript::asERetCodes returnCode)
{
#define CASE(x) case AngelScript::x:return #x;
  switch(returnCode)
  {
    CASE(asSUCCESS)
    CASE(asERROR)
    CASE(asCONTEXT_ACTIVE)
    CASE(asCONTEXT_NOT_FINISHED)
    CASE(asCONTEXT_NOT_PREPARED)
    CASE(asINVALID_ARG)
    CASE(asNO_FUNCTION)
    CASE(asNOT_SUPPORTED)
    CASE(asINVALID_NAME)
    CASE(asNAME_TAKEN)
    CASE(asINVALID_DECLARATION)
    CASE(asINVALID_OBJECT)
    CASE(asINVALID_TYPE)
    CASE(asALREADY_REGISTERED)
    CASE(asMULTIPLE_FUNCTIONS)
    CASE(asNO_MODULE)
    CASE(asNO_GLOBAL_VAR)
    CASE(asINVALID_CONFIGURATION)
    CASE(asINVALID_INTERFACE)
    CASE(asCANT_BIND_ALL_FUNCTIONS)
    CASE(asLOWER_ARRAY_DIMENSION_NOT_REGISTERED)
    CASE(asWRONG_CONFIG_GROUP)
    CASE(asCONFIG_GROUP_IS_IN_USE)
    CASE(asILLEGAL_BEHAVIOUR_FOR_TYPE)
    CASE(asWRONG_CALLING_CONV)
    CASE(asBUILD_IN_PROGRESS)
    CASE(asINIT_GLOBAL_VARS_FAILED)
    CASE(asOUT_OF_MEMORY)
  default:
    return "Unknown AngelScript ReturnCode";
  }
#undef CASE
}


void AngelScriptCheck(int r)
{
  AngelScript::asERetCodes returnCode = static_cast<AngelScript::asERetCodes>(r);

  if(returnCode >= 0)
    return;

  std::string strReturnCode = AngelScriptReturnCodeAsString(returnCode);

  IO::Log::logError("AngelScriptCheck(): Error Code %0 detected!", strReturnCode);

  assert(returnCode >= 0);
}


void AngelScriptCheckThrowingException(int r)
{
  AngelScript::asERetCodes returnCode = static_cast<AngelScript::asERetCodes>(r);

  if(returnCode >= 0)
    return;

  throw std::runtime_error("AngelScriptCheck(): Error Code " + AngelScriptReturnCodeAsString(returnCode) + " detected!");
}


std::mutex suspendMutex;
void suspendContextAfter(AngelScript::asIScriptContext* context, const std::chrono::microseconds& time, bool** isStillValid_)
{
  bool isStillValid = true;

  {
    std::lock_guard<std::mutex> guard(suspendMutex);
    *isStillValid_ = &isStillValid;
    (void)guard;
  }

  sleepFor(time);

  {
    std::lock_guard<std::mutex> guard(suspendMutex);
    if(isStillValid)
      context->Suspend();
    (void)guard;
  }
}


int executeWithTimeout(AngelScript::asIScriptContext* context, const std::chrono::microseconds& time)
{
  bool* isStillValid = nullptr;

  std::thread thread(suspendContextAfter, context, time, &isStillValid);

  int status = context->Execute();

  {
    while(isStillValid == nullptr)
      sleepFor(std::chrono::microseconds(1));
    std::lock_guard<std::mutex> guard(suspendMutex);
    *isStillValid = false;
    (void)guard;
  }

  thread.detach();

  return status;
}

void executeScriptThrowingException(const std::string& scriptSource, const std::chrono::microseconds& time, AngelScript::asDWORD accessMask, const char* mainMethod)
{
  int r=0;

  AngelScript::asIScriptEngine* engine = Scripting::Engine::angelScriptEngine();

  AngelScript::asIScriptModule* scriptModule = engine->GetModule("executeScriptThrowingException", AngelScript::asGM_ALWAYS_CREATE);

  try
  {
    scriptModule->SetAccessMask(accessMask);

    r = scriptModule->AddScriptSection("source", scriptSource.data()); AngelScriptCheckThrowingException(r);
    r = scriptModule->Build();
    if(r == AngelScript::asERROR)
      throw std::runtime_error("Compilation Error");
    Scripting::AngelScriptCheckThrowingException(r);

    AngelScript::asIScriptFunction* function = scriptModule->GetFunctionByName(mainMethod);
    if(function == nullptr)
      throw std::runtime_error("Couldn't find method "+std::string(mainMethod));

    AngelScript::asIScriptContext* scriptContext = engine->CreateContext();

    try
    {
      r = scriptContext->Prepare(function); Scripting::AngelScriptCheckThrowingException(r);

      r = executeWithTimeout(scriptContext, time); Scripting::AngelScriptCheckThrowingException(r);
    }catch(...)
    {
      scriptContext->Release();
      throw;
    }
    scriptContext->Release();

  }catch(...)
  {
    engine->DiscardModule(scriptModule->GetName());
    throw;
  }
  engine->DiscardModule(scriptModule->GetName());
}

void executeScriptThrowingException(const IO::RegularFile& scriptFile, const std::chrono::microseconds& time, AngelScript::asDWORD accessMask, const char* mainMethod)
{
  if(!scriptFile.exists())
    throw std::runtime_error("The File <"+scriptFile.pathAsString().toUtf8String() + "> doesn't exist.");

  std::ifstream s;

  s.open(scriptFile.pathAsOgreString());

  Ogre::FileStreamDataStream stream(scriptFile.pathAsOgreString(), &s, false);

  executeScriptThrowingException(stream.getAsString(), time, accessMask, mainMethod);
}

void executeScript(const IO::RegularFile& scriptFile, const std::chrono::microseconds& time, AngelScript::asDWORD accessMask, const char* mainMethod)
{
  try
  {
    executeScriptThrowingException(scriptFile, time, accessMask, mainMethod);
  }catch(const std::exception& e)
  {
    IO::Log::logError("Couldn't execute the script <%0>:\n%1", scriptFile.pathAsString(), String::fromUtf8(e.what()));
  }
}


} // namespace Scripting
} // namespace Base
