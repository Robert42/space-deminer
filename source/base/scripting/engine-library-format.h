#include <base/scripting/engine.h>

namespace Base {
namespace Scripting {

class FormatedValue
{
private:
  std::string* _str;

public:

  template<typename T>
  FormatedValue(const T& value)
  {
    init<T>(value);
  }

  FormatedValue()
  {
    init();
  }

  ~FormatedValue()
  {
    deinit();
  }

  template<typename T>
  void init(const T& value)
  {
    init(toString<T>(value));
  }

  void init(const String& str)
  {
    init(str.toUtf8String());
  }

  void init(const std::string& str)
  {
    _str = new std::string(str);
  }

  void init()
  {
    init(std::string());
  }

  void init(const FormatedValue& f)
  {
    init(f.get());
  }

  void deinit()
  {
    delete _str;
    _str = nullptr;
  }

  void set(const String& str)
  {
    set(str.toUtf8String());
  }

  void set(const FormatedValue& f)
  {
    set(f.get());
  }

  void set(const std::string& str)
  {
    *this->_str = str;
  }

  std::string get() const
  {
    return *this->_str;
  }

  std::string copy() const
  {
    return *this->_str;
  }
};

std::ostream& operator <<(std::ostream& s, const FormatedValue& fv);


std::string format(const std::string& s);
std::string format(const FormatedValue& f);
std::string format(const std::string& s, const FormatedValue& v1);
std::string format(const std::string& s, const FormatedValue& v1, const FormatedValue& v2);
std::string format(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3);
std::string format(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4);
std::string format(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5);
std::string format(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6);
std::string format(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6, const FormatedValue& v7);
std::string format(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6, const FormatedValue& v7, const FormatedValue& v8);
std::string format(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6, const FormatedValue& v7, const FormatedValue& v8, const FormatedValue& v9);
std::string format(const std::string& s, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6, const FormatedValue& v7, const FormatedValue& v8, const FormatedValue& v9, const FormatedValue& v10);


} // namespace Scripting
} // namespace Base
