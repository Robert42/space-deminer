#ifndef _BASE_SCRIPTING_TOKENIZEDSCRIPT_H_
#define _BASE_SCRIPTING_TOKENIZEDSCRIPT_H_

#include <base/strings/string.h>
#include <base/optional.h>
#include <base/enum-macros.h>

namespace Base {
namespace Scripting {


class TokenizedScript
{
public:
  class Token
  {
  public:
    BEGIN_ENUMERATION(Type,)
      KEYWORD_OPENING_BRACKET,
      KEYWORD_CLOSING_BRACKET,
      KEYWORD_COMMA,
      KEYWORD_SEMICOLON,
      KEYWORD_NAMESPACE_SEPERATOR,
      KEYWORD_DOT,
      KEYWORD_OTHER_OPERATOR,
      KEYWORD_WORD,
      VALUE_STRING,
      VALUE_BOOLEAN,
      VALUE_OTHER,
      IDENTIFIER,
      COMMENT,
      WHITESPACE,
      UNKNOWN
    ENUMERATION_BASIC_OPERATORS(Type)
    END_ENUMERATION;

    Type type;
    const AngelScript::asETokenClass asToken;
    const size_t tokenIndex;
    const size_t indexBegin;
    const char* const begin, * const end;
    size_t matchingBracketToken;
    bool isInRangeOfUnmachedBrackets;
    Optional< std::shared_ptr<String> > originalString;

    Token(AngelScript::asETokenClass asToken,
          size_t indexBegin,
          const char* begin,
          size_t length,
          size_t thisIndex,
          std::vector<Token>& tokens,
          std::stack<size_t>* bracketTokenStack,
          const Optional< std::shared_ptr<String> >& originalString=nothing);

    std::string tokenAsAnsiString() const;
    String tokenAsString() const;

    size_t length() const;

    bool hasMatchingBracket() const;

    int compare(const char* otherAnsiString) const;
    static int compare(const char* begin, const char* end, const char* otherAnsiString);

  private:
  };

private:
  std::vector<Token> _tokens;
  std::vector<char> ansiString;

public:
  TokenizedScript(const String& script);
  TokenizedScript(const std::string& ansiScript,
                  const Optional<String>& originalScript=nothing);

  const std::vector<Token>& tokens() const;
  const Token& token(size_t tokenId) const;

  std::vector<Token>::const_iterator findIteratorOfTokenContainingCharacter(size_t i, const std::vector<Token>::const_iterator& fallback) const;
  const Token& findTokenContainingCharacter(size_t i) const;
  size_t findTokenIndexContainingCharacter(size_t characterIndex, size_t fallbackValue) const;
  size_t findTokenIndexContainingCharacter(size_t characterIndex) const;
};


} // namespace Scripting
} // namespace Base

#endif
