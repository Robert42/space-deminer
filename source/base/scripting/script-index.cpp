#include <base/scripting/script-index.h>

namespace Base {
namespace Scripting {

class Index::Implementation
{
public:
  typedef shared_ptr<Implementation> Ptr;

public:
  virtual ~Implementation()
  {
  }

  virtual void visitAllEnumerationsWith(Visitor& visitor) const = 0;
  virtual void visitAllFunctionsWith(Visitor& visitor) const = 0;
};

class Index::EngineGlobalIndex : public Implementation
{
private:
  AngelScript::asIScriptEngine* engine;
  uint32 accessMask;

public:
  EngineGlobalIndex(AngelScript::asIScriptEngine* engine, uint32 accessMask)
    : engine(engine),
      accessMask(accessMask)
  {
    engine->AddRef();
  }

  ~EngineGlobalIndex()
  {
    engine->Release();
  }

  void visitAllEnumerationsWith(Visitor& visitor) const
  {
    size_t enumCount = engine->GetEnumCount();
    for(size_t enumIndex=0; enumIndex<enumCount; ++enumIndex)
    {
      int enumTypeId;
      const char* nameSpace;
      const char* enumName = engine->GetEnumByIndex(enumIndex, &enumTypeId, &nameSpace);

      size_t valueCount = engine->GetEnumValueCount(enumTypeId);
      for(size_t valueIndex=0; valueIndex<valueCount; ++valueIndex)
      {
        int value;
        const char* valueName = engine->GetEnumValueByIndex(enumTypeId, valueIndex, &value);

        visitor.visitEnumValue(nameSpace, enumName, valueName, value);
      }
    }
  }

  void visitAllFunctionsWith(Visitor& visitor) const
  {
    size_t count = engine->GetGlobalFunctionCount();

    for(size_t i=0; i<count; ++i)
    {
      AngelScript::asIScriptFunction* f = engine->GetGlobalFunctionByIndex(i);

      if(f==nullptr)
        continue;

      if(f->GetAccessMask() & accessMask)
        visitor.visitFunction(f);
    }
  }
};

class Index::ModuleIndex : public Implementation
{
private:
  AngelScript::asIScriptModule* module;

public:
  ModuleIndex(AngelScript::asIScriptModule* module)
    : module(module)
  {
  }

  void visitAllEnumerationsWith(Visitor& visitor) const
  {
    size_t enumCount = module->GetEnumCount();
    for(size_t enumIndex=0; enumIndex<enumCount; ++enumIndex)
    {
      int enumTypeId;
      const char* nameSpace;
      const char* enumName = module->GetEnumByIndex(enumIndex, &enumTypeId, &nameSpace);

      size_t valueCount = module->GetEnumValueCount(enumTypeId);
      for(size_t valueIndex=0; valueIndex<valueCount; ++valueIndex)
      {
        int value;
        const char* valueName = module->GetEnumValueByIndex(enumTypeId, valueIndex, &value);

        visitor.visitEnumValue(nameSpace, enumName, valueName, value);
      }
    }
  }

  void visitAllFunctionsWith(Visitor& visitor) const
  {
    size_t count = module->GetFunctionCount();
    for(size_t i=0; i<count; ++i)
      visitor.visitFunction(module->GetFunctionByIndex(i));
  }
};

class Index::ImplementationList : public Implementation
{
public:
  std::list<Implementation::Ptr> implementations;

public:
  void insert(const Implementation::Ptr& implementation)
  {
    implementations.push_back(implementation);
  }

  void visitAllEnumerationsWith(Visitor& visitor) const
  {
    for(const Implementation::Ptr& implementation : implementations)
      implementation->visitAllEnumerationsWith(visitor);
  }

  void visitAllFunctionsWith(Visitor& visitor) const
  {
    for(const Implementation::Ptr& implementation : implementations)
      implementation->visitAllFunctionsWith(visitor);
  }
};

Index::Ptr Index::create()
{
  return Ptr(new Index);
}

Index::Index()
{
  implementations = shared_ptr<ImplementationList>(new ImplementationList);
}

void Index::insertGlobal(AngelScript::asIScriptEngine* engine, uint32 accessMask)
{
  implementations->insert(Implementation::Ptr(new EngineGlobalIndex(engine, accessMask)));
}

void Index::insertModule(AngelScript::asIScriptModule* scriptModule)
{
  implementations->insert(Implementation::Ptr(new ModuleIndex(scriptModule)));
}

void Index::visitAllWith(Visitor& visitor) const
{
  implementations->visitAllEnumerationsWith(visitor);
  implementations->visitAllFunctionsWith(visitor);
}

std::string mergeName(const std::string& namespaceName, const std::string& name)
{
  if(namespaceName.empty())
    return name;

  if(namespaceName.size()>=2 && namespaceName[namespaceName.size()-1]==':')
    return namespaceName + name;

  return namespaceName + "::" + name;
}

// ====

void Index::NameVisitor::visitFunction(const AngelScript::asIScriptFunction* function)
{
  std::string name;

  if(function->GetParamCount() == 1)
    name = mergeName(function->GetNamespace(), truncSet(function->GetName()));
  else if(function->GetParamCount() == 0)
    name = mergeName(function->GetNamespace(), truncGet(function->GetName()));
  else
    name = mergeName(function->GetNamespace(), function->GetName());

  visitFunction(name, function);
}

void Index::NameVisitor::visitEnumValue(const char* nameSpace, const char* enumName, const char* valueName, int value)
{
  visitEnumValue(mergeName(nameSpace, enumName)+"::"+valueName, value);
}

const char* Index::NameVisitor::truncSet(const char* str)
{
  return truncGetSet(str, 's');
}

const char* Index::NameVisitor::truncGet(const char* str)
{
  return truncGetSet(str, 'g');
}

const char* Index::NameVisitor::truncGetSet(const char* str, char first)
{
  const char* truncated = str;

  if(*truncated!=first)
    return str;
  ++truncated;
  if(*truncated!='e')
    return str;
  ++truncated;
  if(*truncated!='t')
    return str;
  ++truncated;
  if(*truncated!='_')
    return str;
  ++truncated;
  return truncated;
}


} // namespace Scripting
} // namespace Base
