#include <base/scripting/tokenized-script.h>

namespace Base {
namespace Scripting {


TEST(base_scripting_TokenizedScript, compare_sameContent)
{
  const char* strA = "abcdefg";
  const char* strB = "abcdef";

  const char* begin = strA;
  const char* end = strA+7;

  EXPECT_EQ(1, TokenizedScript::Token::compare(begin, end, strB));
  --end;
  EXPECT_EQ(0, TokenizedScript::Token::compare(begin, end, strB));
  --end;
  EXPECT_EQ(-1, TokenizedScript::Token::compare(begin, end, strB));
}


TEST(base_scripting_TokenizedScript, compare_differentContent)
{
  const char* strA = "abceef";
  const char* strB = "abcdef";

  const char* begin = strA;
  const char* end = strA+6;

  EXPECT_EQ(1, TokenizedScript::Token::compare(begin, end, strB));

  begin = strB;
  end = strB+6;

  EXPECT_EQ(-1, TokenizedScript::Token::compare(begin, end, strA));
}


TEST(base_scripting_TokenizedScript, compare_ZeroWithin1)
{
  const char* strA = "abc";
  const char* strB = "abc";

  const char* begin = strA;
  const char* end = strA+6;

  EXPECT_EQ(0, TokenizedScript::Token::compare(begin, end, strB));
}


TEST(base_scripting_TokenizedScript, compare_ZeroWithin2)
{
  const char* strA = "abc";
  const char* strB = "abcdefghij";

  const char* begin = strA;
  const char* end = strA+6;

  EXPECT_EQ(-1, TokenizedScript::Token::compare(begin, end, strB));
}


TEST(base_scripting_TokenizedScript, compare_ZeroWithin3)
{
  const char* strA = "abcdefghij";
  const char* strB = "abc";

  const char* begin = strA;
  const char* end = strA+6;

  EXPECT_EQ(1, TokenizedScript::Token::compare(begin, end, strB));
}


} // namespace Scripting
} // namespace Base
