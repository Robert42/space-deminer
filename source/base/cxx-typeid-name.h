#ifndef BASE_CXXTYPEIDNAME_H
#define BASE_CXXTYPEIDNAME_H

#include <dependencies.h>

namespace Base {

#ifdef CXX_TYPEID_NAME_SUPPORTED

template<typename T>
std::string demangeledTypeName();

#endif

template<typename T>
void assertSameNameAsType(const std::string& name);

} // namespace Base

#include "cxx-typeid-name.inl"

#endif // BASE_CXXTYPEIDNAME_H
