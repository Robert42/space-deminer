#include "interpolation.h"

#include <base/interpolation.h>

namespace Base {


TEST(base_interpolation, interpolate)
{
  EXPECT_EQ(0.5, interpolate( 0.5, 0., 1.));
  EXPECT_EQ(15., interpolate( 0.5, 10., 20.));
  EXPECT_EQ(15., interpolate( 0.5, 20., 10.));
  EXPECT_EQ(-5., interpolate(-0.5, 0., 10.));
  EXPECT_EQ(15., interpolate( 1.5, 0., 10.));

  EXPECT_EQ(vec2(21.f, 50.f), interpolate( 0.5f, vec2(0.f, 25.f), vec2(42.f, 75.f)));
}

TEST(base_interpolationWeightOf, interpolationWeightOf)
{
  EXPECT_EQ(0.5, interpolationWeightOf( 0.5, 0., 1.));
  EXPECT_EQ(0.5, interpolationWeightOf( 15., 10., 20.));
  EXPECT_EQ(0.5, interpolationWeightOf( 15., 20., 10.));
  EXPECT_EQ(-0.5, interpolationWeightOf(-5., 0., 10.));
  EXPECT_EQ(1.5, interpolationWeightOf( 15., 0., 10.));
}


TEST(base_interpolateWithFallback, interpolateWithFallback)
{
  EXPECT_EQ(42.f, interpolateWithFallback(42.f));
  EXPECT_EQ(15.f, interpolateWithFallback(10.f, 0.5f, 20.f));
  EXPECT_EQ(10.f, interpolateWithFallback(10.f, 0.0f, 20.f));
  EXPECT_EQ(10.f, interpolateWithFallback(10.f, 0.0f, 20.f, 0.f, 30.f));
  EXPECT_EQ(20.f, interpolateWithFallback(10.f, 1.0f, 20.f));
  EXPECT_EQ(24.f, interpolateWithFallback(10.f, 3.0f, 20.f, 2.0f, 30.f));
  EXPECT_EQ(15.f, interpolateWithFallback(10.f, 0.1f, 20.f, 0.2f, 30.f));
}


} // namespace Base
