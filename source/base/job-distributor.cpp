#include "job-distributor.h"

namespace Base {

JobDistributor::JobDistributor(int job, int workers)
  : _jobLeft(max(0, job)),
    _workersLeft(max(0, workers))
{
}


int JobDistributor::pickNextJob()
{
  if(jobLeft() > 0)
  {
    assert(workersLeft());

    if(!workersLeft())
      _workersLeft = 1;

    int nextJob = (jobLeft() + workersLeft()-1) / workersLeft();

    _jobLeft -= nextJob;
    _workersLeft--;

    return nextJob;
  }else
  {
    return 0;
  }
}

int JobDistributor::jobLeft() const
{
  return _jobLeft;
}

int JobDistributor::workersLeft() const
{
  return _workersLeft;
}

} // namespace Base
