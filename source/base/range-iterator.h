#ifndef BASE_RANGEITERATOR_H
#define BASE_RANGEITERATOR_H

#include <dependencies.h>

namespace Base {
namespace Private {


template <typename T>
class IterableRange
{
public:
  class Iterator
  {
  public:
    int n;
    T value;
    const T stepSize;

    Iterator(int n, T value, T stepSize)
      : n(n),
        value(value),
        stepSize(stepSize)
    {
    }

    T operator*() const
    {
      return value;
    }

    void operator++()
    {
      value += stepSize;
      --n;
    }

    bool operator!=(const Iterator& other) const
    {
      return this->n != other.n;
    }
  };

private:
  const T _first;
  const T _last;
  const T stepSize;
  const int nSteps;

public:
  IterableRange(T first, T last, T stepSize, bool lastValueGreaterEqualEnd)
    : _first(first),
      _last(last),
      stepSize(first<last ? abs(stepSize) : -abs(stepSize)),
      nSteps(_calcNumSteps(first, last, this->stepSize, lastValueGreaterEqualEnd))
  {
  }

  Iterator begin() const
  {
    return Iterator(nSteps, _first, stepSize);
  }

  Iterator end() const
  {
    return Iterator(0, _last, stepSize);
  }

public:
  static int _calcNumSteps(T first, T last, T stepSize, bool lastValueGreaterEqualEnd)
  {
    if(stepSize==0)
      return 1;

    int n;
    if(lastValueGreaterEqualEnd)
      n = ceilDivision(last - first, stepSize);
    else
      n = floorDivision(last - first, stepSize);

    return max(0, n) + 1;
  }
};


} // namespace Private


/** Helper function simplifying to iterate over a range of numbers.
 *
 * Simple usage example:
 * ```
 * // Prints 1 2 3 4 5
 * for(int i : range(1, 5))
 *   std::cout << " " << i;
 *
 * // Prints 5 4 3 2 1
 * for(int i : range(5, 1, -1))
 *   std::cout << " " << i;
 *
 * // Prints 0 0.3 0.6 0.9
 * for(int i : range(0., 1., 0.3))
 *   std::cout << " " << i;
 *
 * // Prints 0 0.3 0.6 0.9 1.2
 * for(int i : range(0., 1., 0.3, true))
 *   std::cout << " " << i;
 * ```
 *
 * @param begin the first value to be called within the range. If the input is valid, this value
 * will be always the first element.
 * @param the end of the range.
 * @param stepSize the step size of the range iteration. Can be negative. If zero, only begin will
 * be visited.
 * @param forceEndToBeInclude If true, it will be made sure, that the last value will be greater or
 *  equal `end` - otherwise the last value will be smaller or equal `end`. Whether end itself will
 *  be used as last value, depends, whether `end-start` is a multiple of stepSize or not.
 */
template<typename T>
Private::IterableRange<T> range(T first, T last, T stepSize=1, bool lastValueGreaterEqualEnd=false)
{
  return Private::IterableRange<T>(first, last, stepSize, lastValueGreaterEqualEnd);
}


} // namespace Base

#endif // BASE_RANGEITERATOR_H
