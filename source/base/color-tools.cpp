#include "color-tools.h"
#include "interpolation.h"

namespace Base {

vec4 darker(const vec4& color, real factor)
{
  return interpolate(factor, color, vec4(vec3(0), color.a));
}

vec4 lighter(const vec4& color, real factor)
{
  return interpolate(factor, color, vec4(vec3(1), color.a));
}


real brightness(const vec4& color)
{
  return (color.r + color.g + color.b) / 3.f;
}

vec4 colorFromHsl(real h, real s, real l, real a)
{
  return fromQColor(QColor::fromHslF(h, s, l, a));
}


vec4 fromQColor(const QColor& c)
{
  return vec4(c.redF(),
              c.greenF(),
              c.blueF(),
              c.alphaF());
}

QColor toQColor(const vec4& v)
{
  return QColor::fromRgbF(v.r, v.g, v.b, v.a);
}

ivec4 colorAsIVec(const vec4& color, int white)
{
  return clamp(ivec4(round(color * float(white))),
               ivec4(0),
               ivec4(white));
}

ivec3 colorAsIVec(const vec3& color, int white)
{
  return clamp(ivec3(round(color * float(white))),
               ivec3(0),
               ivec3(255));
}

uint32 colorAsRgbaInteger(const vec4& color)
{
  ivec4 roundedColor = colorAsIVec(color);

  return (roundedColor.r<<24) | (roundedColor.g<<16) | (roundedColor.b<<8) | roundedColor.a;
}

uint32 colorAsRgbInteger(const vec3& color)
{
  ivec3 roundedColor = colorAsIVec(color);

  return (roundedColor.r<<16) | (roundedColor.g<<8) | roundedColor.b;
}


} // namespace Base

namespace glm {
namespace detail {


vec4 premultiply(const vec4& color)
{
  return vec4(color.rgb()*color.a, color.a);
}

vec4 unpremultiply(const vec4& color)
{
  if(color.a==0)
    return vec4(0);
  else
    return vec4(color.rgb()/color.a, color.a);
}


} // namespace detail
} // namespace glm


namespace Ogre {


ColourValue premultiply(const ColourValue& color)
{
  return Ogre::ColourValue(color.r*color.a,
                           color.g*color.a,
                           color.b*color.a,
                           color.a);
}

ColourValue unpremultiply(const ColourValue& color)
{
  if(color.a==0)
    return ColourValue(0, 0, 0, 0);
  else
    return ColourValue(color.r/color.a,
                       color.g/color.a,
                       color.b/color.a,
                       color.a);
}


} // namespace Ogre
