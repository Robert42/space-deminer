#include <dependencies.h>

#include <base/threads.h>

namespace Base {
inline bool alwaysTrue(){return true;}
inline bool alwaysFalse(){return false;}


TEST(base_sleepWhile, handle_endless_loops1)
{
  ASSERT_THROW(sleepWhile(alwaysTrue, std::chrono::milliseconds(10), std::chrono::microseconds(100)),
               Ogre::Exception);
}

TEST(base_sleepWhile, handle_endless_loops2)
{
  ASSERT_THROW(sleepWhile(alwaysTrue, std::chrono::milliseconds(10), std::chrono::milliseconds(1)),
               Ogre::Exception);
}

TEST(base_sleepWhile, leave_immediatly)
{
  sleepWhile(alwaysFalse);
  SUCCEED();
}


} // namespace Base
