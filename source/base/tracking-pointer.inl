#ifndef BASE_TRACKINGPOINTER_INL
#define BASE_TRACKINGPOINTER_INL


#include "tracking-pointer.h"


namespace Base {


template<typename T>
TrackingPtr<T>::TrackingPtr(const T& value, Signals::Trackable& trackable)
  : _trackable(&trackable),
    _value(value)
{
  startListeningToTrackable(&trackable);
}


template<typename T>
TrackingPtr<T>::TrackingPtr()
{
}


template<typename T>
TrackingPtr<T>::~TrackingPtr()
{
}


template<typename T>
const Optional<T>& TrackingPtr<T>::value() const
{
  return _value;
}


template<typename T>
Optional<T> TrackingPtr<T>::value()
{
  return _value;
}


template<typename T>
void TrackingPtr<T>::reset()
{
  if(_trackable)
    stopListeningToTrackable(*_trackable);
  _value.reset();
  _trackable.reset();
}


template<typename T>
void TrackingPtr<T>::trackableGotDestroyed(Signals::Trackable* trackable)
{
  assert(_trackable && *_trackable == trackable);

  if(_trackable && *_trackable == trackable)
  {
    _value.reset();
    _trackable.reset();
  }
}


} // namespace Base


#endif // BASE_TRACKINGPOINTER_HINL
