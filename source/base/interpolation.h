#ifndef BASE_INTERPOLATION_H
#define BASE_INTERPOLATION_H

#include <dependencies.h>

namespace Base {


template<typename Factor, typename Value>
Factor interpolationWeightOf(const Factor& value, const Value& lowerLimit, const Value& upperLimit);


template<typename Factor, typename Value>
Value interpolate(const Factor& weight, const Value& lowerLimit, const Value& upperLimit);


/** A helper function allowing to easily interpolate betwen a large (but fixed) number of values.
 *
 * @param fallbackValue The fallback value which wil be used of the sum of the given weights is
 *        smaller than one. The weight of the fallback value is automatically calculated.
 * @param otherValuest the other weights and values
 *
 * Usage:
 * ```
 * result = interpolateWithFallback(x_fallback, a_1, x_1, a_2, x_2, ..., a_n, x_n);
 * ```
 * The weight of the fallback value is automatically calculated.
 *
 * @return the interpolated values. Equal to
 *         \f$ a_f\cdot x_f + \sum_{i=1}^n a_i\cdot x_i \f$
 *         with \f$x_f\f$ being the fallback value and
 *         \f$a_f=max\{1-(a_1+a_2+...+a_n), 0\}\f$
 */
template<typename T, typename... T_otherValues>
T interpolateWithFallback(const T& fallbackValue, const T_otherValues&... otherValues);

float smoothInterpolationFactor(float x);

} // namespace Base

#include "interpolation.inl"

#endif // BASE_INTERPOLATION_H
