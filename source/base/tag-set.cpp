#include "tag-set.h"

namespace Base {


Tag TagIds::tagFromString(StringId stringId)
{
  return static_cast<Tag>(stringId);
}

Tag TagIds::tagFromString(const QString& string)
{
  return tagFromString(StringIdManager::idFromString(string));
}

Tag TagIds::tagFromString(const String& string)
{
  return tagFromString(StringIdManager::idFromString(string));
}

Tag TagIds::tagFromString(const char* string)
{
  return tagFromString(StringIdManager::idFromString(string));
}


} // namespace Base
