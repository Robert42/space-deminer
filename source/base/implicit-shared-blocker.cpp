#include "implicit-shared-blocker.h"

namespace Base {


class Blocker::BlockerImpl
{
public:
  int& blockCounter;
  const std::function<bool()> validator;

  BlockerImpl(int* blockCounter, const std::function<bool()>& validator)
    : blockCounter(*blockCounter),
      validator(validator)
  {
    this->blockCounter++;
  }

  ~BlockerImpl()
  {
    assert(validator());
    assert(blockCounter > 0);

    blockCounter--;
  }
};

Blocker::Blocker(int *blockCounter, const std::function<bool()>& validator)
{
  impl = std::shared_ptr<BlockerImpl>(new BlockerImpl(blockCounter, validator));
}

Blocker::Blocker()
{
}

void Blocker::reset()
{
  impl.reset();
}


} // namespace Base
