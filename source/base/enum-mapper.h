#ifndef _BASE_ENUM_MAPPER_H_
#define _BASE_ENUM_MAPPER_H_

#include <dependencies.h>

namespace Base {


template<typename SourceEnum, typename TargetEnum>
class EnumMapper : noncopyable
{
public:
  class Range
  {
  public:
    SourceEnum first, last;

  public:
    Range(SourceEnum first = SourceEnum(0), SourceEnum last = SourceEnum(255))
      : first(first), last(last)
    {
      if(first > last)
        throw std::invalid_argument("**EnumMapper<>::Range::Range** A Range must include at least one element.");
    }

    bool contains(SourceEnum e) const
    {
      return e>=first && e<=last;
    }

    size_t indexOf(SourceEnum e) const
    {
      return static_cast<size_t>(e) - static_cast<size_t>(first);
    }

    size_t rangeLength() const
    {
      return indexOf(last) + 1;
    }
  };

private:
  typedef EnumMapper<SourceEnum, TargetEnum> ThisEnumMapperType;

  QVector<TargetEnum> directMapping;
  const Range directMappedRange;
  QHash<SourceEnum, TargetEnum> slowMapping;

public:
  EnumMapper(const Range& directMappedRange = Range())
    : directMappedRange(directMappedRange)
  {
    directMapping.resize(directMappedRange.rangeLength());
  }

  ~EnumMapper()
  {
  }

  bool contains(SourceEnum src) const
  {
    return directMappedRange.contains(src) || slowMapping.contains(src);
  }

  void addMapping(SourceEnum src, TargetEnum target)
  {
    if(directMappedRange.contains(src))
      directMapping[directMappedRange.indexOf(src)] = target;
    else
      slowMapping[src] = target;
  }

  TargetEnum& operator[](SourceEnum src)
  {
    if(directMappedRange.contains(src))
      return directMapping[directMappedRange.indexOf(src)];
    else
      return slowMapping[src];
  }

  TargetEnum operator[](SourceEnum src) const
  {
    ThisEnumMapperType& mapper = *const_cast<ThisEnumMapperType*>(this);

    return mapper[src];
  }

};


} // namespace Base


#endif
