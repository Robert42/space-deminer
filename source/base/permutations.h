#ifndef BASE_PERMUTATIONS_H
#define BASE_PERMUTATIONS_H

#include <dependencies.h>

namespace Base {

template<typename T>
QList< QList<T> >  permutate(const QList<T>& l);

} // namespace Base

#include "permutations.inl"

#endif // BASE_PERMUTATION_H
