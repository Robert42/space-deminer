#include "math.h"

namespace Base {


template<typename T>
T ceilDivisionInteger(T a, T b);

template<typename T>
T unsignedCeilDivisionInteger(T a, T b)
{
  return (a+b-1) / b;
}

template<typename T>
T floorDivisionInteger(T a, T b)
{
  if(a < 0)
    return floorDivisionInteger<T>(-a, -b);

  if(b < 0)
    return -ceilDivisionInteger<T>(a, -b);

  return a / b;
}

template<typename T>
T ceilDivisionInteger(T a, T b)
{
  if(a < 0)
    return ceilDivisionInteger<T>(-a, -b);

  if(b < 0)
    return -floorDivisionInteger<T>(a, -b);

  return unsignedCeilDivisionInteger(a, b);
}

int floorDivision(int a, int b)
{
  return floorDivisionInteger(a, b);
}

int ceilDivision(int a, int b)
{
  return ceilDivisionInteger(a, b);
}

uint64 floorDivision(uint64 a, uint64 b)
{
  return a / b;
}

uint64 ceilDivision(uint64 a, uint64 b)
{
  return unsignedCeilDivisionInteger(a, b);
}

uint32 floorDivision(uint32 a, uint32 b)
{
  return a / b;
}

uint32 ceilDivision(uint32 a, uint32 b)
{
  return unsignedCeilDivisionInteger(a, b);
}

uint16 floorDivision(uint16 a, uint16 b)
{
  return a / b;
}

uint16 ceilDivision(uint16 a, uint16 b)
{
  return unsignedCeilDivisionInteger(a, b);
}

uint8 floorDivision(uint8 a, uint8 b)
{
  return a / b;
}

uint8 ceilDivision(uint8 a, uint8 b)
{
  return unsignedCeilDivisionInteger(a, b);
}

int64 floorDivision(int64 a, int64 b)
{
  return floorDivisionInteger(a, b);
}

int64 ceilDivision(int64 a, int64 b)
{
  return ceilDivisionInteger(a, b);
}

int16 floorDivision(int16 a, int16 b)
{
  return floorDivisionInteger(a, b);
}

int16 ceilDivision(int16 a, int16 b)
{
  return ceilDivisionInteger(a, b);
}

int8 floorDivision(int8 a, int8 b)
{
  return floorDivisionInteger(a, b);
}

int8 ceilDivision(int8 a, int8 b)
{
  return ceilDivisionInteger(a, b);
}

real floorDivision(real a, real b)
{
  return floor(a / b);
}

real ceilDivision(real a, real b)
{
  return ceil(a / b);
}

double floorDivision(double a, double b)
{
  return floor(a / b);
}

double ceilDivision(double a, double b)
{
  return ceil(a / b);
}



int mod(int x, int y)
{
  return x - y * int(floor(double(x)/double(y)));
}


ivec2 mod(ivec2 x, int y)
{
  return x - y * ivec2(floor(dvec2(x)/double(y)));
}


ivec3 mod(ivec3 x, int y)
{
  return x - y * ivec3(floor(dvec3(x)/double(y)));
}


ivec4 mod(ivec4 x, int y)
{
  return x - y * ivec4(floor(dvec4(x)/double(y)));
}


ivec2 mod(ivec2 x, ivec2 y)
{
  return x - y * ivec2(floor(dvec2(x)/dvec2(y)));
}


ivec3 mod(ivec3 x, ivec3 y)
{
  return x - y * ivec3(floor(dvec3(x)/dvec3(y)));
}


ivec4 mod(ivec4 x, ivec4 y)
{
  return x - y * ivec4(floor(dvec4(x)/dvec4(y)));
}


uint8 bin_log_floor(uint8 x)
{
  int l = 0;
  if(x & 0xf0)
  {
    x &= 0xf0;
    l += 4;
  }
  if(x & 0xcc)
  {
    x &= 0xcc;
    l += 2;
  }
  if(x & 0xaa)
  {
    x &= 0xaa;
    l += 1;
  }
  return l;
}

uint16 bin_log_floor(uint16 x)
{
  int l = 0;
  if(x & 0xff00)
  {
    x &= 0xff00;
    l += 8;
  }
  if(x & 0xf0f0)
  {
    x &= 0xf0f0;
    l += 4;
  }
  if(x & 0xcccc)
  {
    x &= 0xcccc;
    l += 2;
  }
  if(x & 0xaaaa)
  {
    x &= 0xaaaa;
    l += 1;
  }
  return l;
}

uint32 bin_log_floor(uint32 x)
{
  int l = 0;
  if(x & 0xffff0000)
  {
    x &= 0xffff0000;
    l += 16;
  }
  if(x & 0xff00ff00)
  {
    x &= 0xff00ff00;
    l += 8;
  }
  if(x & 0xf0f0f0f0)
  {
    x &= 0xf0f0f0f0;
    l += 4;
  }
  if(x & 0xcccccccc)
  {
    x &= 0xcccccccc;
    l += 2;
  }
  if(x & 0xaaaaaaaa)
  {
    x &= 0xaaaaaaaa;
    l += 1;
  }

  return l;
}

uint64 bin_log_floor(uint64 x)
{
  int l = 0;
  if(x & 0xffffffff00000000)
  {
    x &= 0xffffffff00000000;
    l += 32;
  }
  if(x & 0xffff0000ffff0000)
  {
    x &= 0xffff0000ffff0000;
    l += 16;
  }
  if(x & 0xff00ff00ff00ff00)
  {
    x &= 0xff00ff00ff00ff00;
    l += 8;
  }
  if(x & 0xf0f0f0f0f0f0f0f0)
  {
    x &= 0xf0f0f0f0f0f0f0f0;
    l += 4;
  }
  if(x & 0xcccccccccccccccc)
  {
    x &= 0xcccccccccccccccc;
    l += 2;
  }
  if(x & 0xaaaaaaaaaaaaaaaa)
  {
    x &= 0xaaaaaaaaaaaaaaaa;
    l += 1;
  }

  return l;
}

int8 bin_log_floor(int8 x)
{
  if(x<=0)
    return 0;

  return bin_log_floor(uint32(x));
}

int16 bin_log_floor(int16 x)
{
  if(x<=0)
    return 0;

  return bin_log_floor(uint32(x));
}

int32 bin_log_floor(int32 x)
{
  if(x<=0)
    return 0;

  return bin_log_floor(uint32(x));
}

int64 bin_log_floor(int64 x)
{
  if(x<=0)
    return 0;

  return bin_log_floor(uint64(x));
}


} // namespace Base
