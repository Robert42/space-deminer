#ifndef BASE_FUNCTIONTYPE_H
#define BASE_FUNCTIONTYPE_H

#include "type.h"
#include <base/enum-macros.h>

namespace Base {

class FunctionType : public Type
{
public:
  class ArgumentType
  {
  public:
    BEGIN_ENUMERATION(ReferenceType,)
      NO_ANNOTATION,
      IN,
      OUT,
      INOUT
    ENUMERATION_BASIC_OPERATORS(ReferenceType)
    END_ENUMERATION;

  public:
    Type type;
    ReferenceType reference;

  public:
    ArgumentType();
    explicit ArgumentType(const Type& type, ReferenceType reference=ReferenceType::NO_ANNOTATION);
  };

  class FunctionTypeDescription final : public Description
  {
  public:
    typedef std::shared_ptr<FunctionTypeDescription> Ptr;

  public:
    const Optional<Type> returnType;
    const QVector<ArgumentType> argumentTypes;

  private:
    FunctionTypeDescription(const std::string& name,
                            Optional<Type> returnType,
                            const QVector<ArgumentType>& argumentTypes);

  public:
    static Ptr create(Optional<Type> returnType=nothing);
    static Ptr create(Optional<Type> returnType,
                      const QVector<Type>& argumentTypes);
    static Ptr create(Optional<Type> returnType,
                      const QVector<ArgumentType>& argumentTypes);

  private:
    static std::string calcName(Optional<Type> returnType,
                                const QVector<ArgumentType>& argumentTypes);
    static Optional<Ptr> functionTypeDescriptionForId(Type::Id typeId);
  };

private:
  explicit FunctionType(const FunctionTypeDescription::Ptr& functionDescription);

public:
  FunctionType();
  FunctionType(Optional<Type> returnType);
  FunctionType(Optional<Type> returnType,
               const QVector<Type>& argumentTypes);
  FunctionType(Optional<Type> returnType,
               const QVector<ArgumentType>& argumentTypes);

  static bool isFunctionType(const Type& type);
  static Optional<FunctionType> fromType(const Type& type);

  FunctionTypeDescription::Ptr description() const;

  const Optional<Type>& returnType() const;
  const QVector<ArgumentType>& argumentTypes() const;
};

void PrintTo(const FunctionType::ArgumentType& type, std::ostream* o);

std::ostream& operator<<(std::ostream& o, const FunctionType::ArgumentType& argumentType);

} // namespace Base

#endif // BASE_FUNCTIONTYPE_H
