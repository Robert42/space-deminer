#include "type.h"
#include "type-manager.h"

namespace Base {


Type::Type()
  : Type(fallbackDescription())
{
}

Type::Type(const Description::Ptr& description)
  : _description(description)
{
  assert(_description->isFallbackDescription() || TypeManager::isRegistered(description));
}

Type::Type(Type::Id typeId)
{
  Optional<Description::Ptr> optionalDescription = TypeManager::typeDescriptionForId(typeId);
  if(optionalDescription)
    _description = *optionalDescription;
  else
    _description = fallbackDescription();
}

Type::~Type()
{
}


Type Type::_registerTypeDescription(const Description::Ptr& description)
{
  return TypeManager::registerTypeDescription(description);
}

Type Type::_tryRegisterTypeDescription(const Description::Ptr& description)
{
  return TypeManager::tryRegisterTypeDescription(description);
}


Type::Id Type::typeIdFromTypeName(StringId name)
{
  return static_cast<Type::Id>(name);
}

Type::Id Type::typeIdFromTypeName(const std::string& name)
{
  return typeIdFromTypeName(StringIdManager::idFromString(name));
}

Type::Id Type::typeIdFromTypeName(const char* name)
{
  return typeIdFromTypeName(StringIdManager::idFromString(name));
}

Type::Id Type::id() const
{
  return _description->id;
}

const std::string& Type::name() const
{
  return _description->name;
}

StringId Type::nameId() const
{
  return static_cast<StringId>(id());
}

Tag Type::tag() const
{
  return static_cast<Tag>(id());
}

const Type::Description::Ptr& Type::description() const
{
  return _description;
}

Type::operator Type::Id() const
{
  return this->id();
}

bool Type::operator==(const Type& type) const
{
  return this->_description == type._description;
}

bool Type::operator!=(const Type& type) const
{
  return this->_description != type._description;
}

bool Type::operator<(const Type& type) const
{
  return this->id() < type.id();
}

bool Type::operator<=(const Type& type) const
{
  return this->id() <= type.id();
}

bool Type::operator>(const Type& type) const
{
  return this->id() > type.id();
}

bool Type::operator>=(const Type& type) const
{
  return this->id() >= type.id();
}

Type::Description::Ptr Type::fallbackDescription()
{
  class FallbackDescription : public PrimitiveDescription
  {
  public:
    FallbackDescription()
      : PrimitiveDescription("fallbackPrimitiveType", sizeof(int), false, true, false)
    {
      _isFallbackDescription = true;
    }
  };

  static Description::Ptr description(new FallbackDescription);

  return description;
}


// ====


Type::Description::Description(const std::string& name)
  : _isClassDescription(false),
    _isPrimitiveDescription(false),
    _isFunctionTypeDescription(false),
    _isFallbackDescription(false),
    id(Type::typeIdFromTypeName(name)),
    name(name)
{
}

bool Type::Description::isClassDescription() const
{
  return _isClassDescription;
}

bool Type::Description::isPrimitiveDescription() const
{
  return _isPrimitiveDescription;
}

bool Type::Description::isFunctionDescription() const
{
  return _isFunctionTypeDescription;
}

bool Type::Description::isFallbackDescription() const
{
  return _isFallbackDescription;
}


// ====


Type::PrimitiveDescription::PrimitiveDescription(const std::string& name, size_t size, bool floatType, bool intType, bool boolType)
  : Description(name),
    size(size),
    floatType(floatType),
    intType(intType),
    boolType(boolType)
{
  assert(!name.empty());
  assert(floatType + intType + boolType <= 1); // a primitive can't be a float, an int and a bool at the same time

  _isPrimitiveDescription = true;
}

Type::PrimitiveDescription::Ptr Type::PrimitiveDescription::create(const std::string& name, size_t size, bool floatType, bool intType, bool boolType)
{
  return Ptr(new PrimitiveDescription(name, size, floatType, intType, boolType));
}


// ====


int qHash(Type::Id typeId, uint seed)
{
  return QT_PREPEND_NAMESPACE(qHash)(static_cast<std::underlying_type<Type::Id>::type>(typeId), seed);
}

int qHash(const Type& type, uint seed)
{
  return qHash(type.id(), seed);
}


void PrintTo(const Type& type, std::ostream* o)
{
  (*o) << type;
}


} // namespace Base
