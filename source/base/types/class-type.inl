#ifndef BASE_CLASSTYPE_INL
#define BASE_CLASSTYPE_INL

#include "class-type.h"
#include <base/io/log.h>

namespace Base {


template<typename T_result>
Optional<T_result> ClassType::tryCalcValueForWholeHierarchy(const ClassType& type,
                                                            const std::function<Optional<T_result>(const ClassType& type)>& function)
{
  Optional<ClassType> currentType = type;

  while(currentType)
  {
    Optional<T_result> r = function(*currentType);

    if(r)
      return *r;

    currentType = currentType->baseClass();
  }

  return nothing;
}

template<typename T_result>
T_result ClassType::calcValueForWholeHierarchy(const ClassType& type,
                                               const T_result& fallbackValue,
                                               const std::function<Optional<T_result>(const ClassType& type)>& function)
{
  Optional<T_result> result = tryCalcValueForWholeHierarchy<T_result>(type,
                                                                      function);
  if(result)
    return *result;
  else
    return fallbackValue;
}


template<typename T_result>
Optional<T_result> ClassType::tryCalcValueForTwoHierarchies(const ClassType& typeA,
                                                            const ClassType& typeB,
                                                            const std::function<Optional<T_result>(const ClassType& typeA, const ClassType& typeB)>& function)
{
  return tryCalcValueForWholeHierarchy<T_result>(typeA,
                                                 [&typeB, &function](const ClassType& type) {
    return tryCalcValueForWholeHierarchy<T_result>(typeB, std::bind(function, type, _1));
  });
}

template<typename T_result>
T_result ClassType::calcValueForTwoHierarchies(const ClassType& typeA,
                                               const ClassType& typeB,
                                               const T_result& fallbackValue,
                                               const std::function<Optional<T_result>(const ClassType& typeA, const ClassType& typeB)>& function)
{
  Optional<T_result> result = tryCalcValueForTwoHierarchies<T_result>(typeA,
                                                                      typeB,
                                                                      function);
  if(result)
    return *result;
  else
    return fallbackValue;
}


template<typename T>
void ClassType::assertionsForNewClassType(const std::string& name)
{
  // make sure the class is declared
  typeOf<T>();

  assertSameNameAsType<T>(name);
}


template<typename T>
ClassType ClassType::registerClassType(const std::string& name)
{
  assertionsForNewClassType<T>(name);

  ClassDescription::Ptr classDescription = ClassDescription::create(name);
  _registerTypeDescription(classDescription);
  return ClassType(classDescription);
}

template<typename T>
ClassType ClassType::tryRegisterClassType(const std::string& name)
{
  assertionsForNewClassType<T>(name);

  ClassDescription::Ptr classDescription = ClassDescription::create(name);
  Optional<ClassType> registeredType = ClassType::fromType(_tryRegisterTypeDescription(classDescription));

  assert("ClassType::tryRegisterClassType(): trying to register a type with a nave, which is already give for a nonclasstype" && registeredType); // ISSUE-183 here an exception should be thrown (and handeled)

  return *registeredType;
}

template<typename T, typename T_base>
ClassType ClassType::registerClassTypeWithBase(const std::string& name)
{
  assertionsForNewClassType<T>(name);

  Optional<ClassType> baseClassType = ClassType::fromType(typeOf<T_base>());

  // When calling registerClassTypeWithBase, the caller has to guarant, T_base is a registered class
  assert(baseClassType);

  ClassDescription::Ptr classDescription = ClassDescription::create(name, *baseClassType);
  _registerTypeDescription(classDescription);
  return ClassType(classDescription);
}

template<typename T, typename T_base>
ClassType ClassType::tryRegisterClassTypeWithBase(const std::string& name)
{
  assertionsForNewClassType<T>(name);

  Optional<ClassType> baseClassType = ClassType::fromType(typeOf<T_base>());

  // When calling registerClassTypeWithBase, the caller has to guarant, T_base is a registered class
  assert(baseClassType);

  ClassDescription::Ptr classDescription = ClassDescription::create(name, *baseClassType);
  Optional<ClassType> registeredType = ClassType::fromType(_tryRegisterTypeDescription(classDescription));

  assert("ClassType::tryRegisterClassTypeWithBase(): trying to register a type with a nave, which is already give for a nonclasstype" && registeredType); // ISSUE-183 here an exception should be thrown (and handeled)

  return *registeredType;
}



} // namespace Base

#endif // BASE_CLASSTYPE_INL
