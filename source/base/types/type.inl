#ifndef BASE_TYPE_INL
#define BASE_TYPE_INL

#include "type.h"

namespace Base {


template<typename T>
Type Type::registerPrimitiveType(const std::string& name, bool floatType, bool intType, bool boolType)
{
  return _registerTypeDescription(PrimitiveDescription::create(name, sizeof(T), floatType, intType, boolType));
}


template<typename T>
Type Type::tryRegisterPrimitiveType(const std::string& name, bool floatType, bool intType, bool boolType)
{
  return _tryRegisterTypeDescription(PrimitiveDescription::create(name, sizeof(T), floatType, intType, boolType));
}

template<typename T>
std::basic_ostream<T>& operator<<(std::basic_ostream<T>& o, const Type& type)
{
  return o << type.name();
}

} // namespace Base


#endif // BASE_TYPE_INL
