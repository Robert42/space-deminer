#include "class-type.h"
#include "type-manager.h"

namespace Base {



ClassType::ClassType()
  : ClassType(fallbackClassDescription())
{
}

ClassType::ClassType(const ClassDescription::Ptr &classDescription)
  : Type(classDescription)
{
}


bool ClassType::isClassType(const Type& type)
{
  return type.description()->isClassDescription();
}

Optional<ClassType> ClassType::fromType(const Type& type)
{
  const Description::Ptr& description = type.description();

  if(description->isClassDescription())
    return ClassType(std::static_pointer_cast<ClassDescription>(description));
  else
    return nothing;
}


ClassType::ClassDescription::Ptr ClassType::description() const
{
  assert(Type::description()->isClassDescription());
  return std::static_pointer_cast<ClassDescription>(Type::description());
}


Optional<ClassType> ClassType::baseClass() const
{
  const ClassDescription::Ptr& classDescription = this->description();

  Optional<Type::Id> baseClassTypeId = classDescription->parentId;

  if(baseClassTypeId)
    return ClassType::fromType(*baseClassTypeId);
  else
    return nothing;
}


bool ClassType::isInheritingOrSame(const Type& baseClass) const
{
  if(*this == baseClass)
    return true;

  Optional<ClassType> realBaseClass = this->baseClass();

  if(!realBaseClass)
    return false;

  return realBaseClass->isInheritingOrSame(baseClass);
}

ClassType::ClassDescription::Ptr ClassType::fallbackClassDescription()
{
  class FallbackDescription : public ClassDescription
  {
  public:
    FallbackDescription()
      : ClassDescription("fallbackPrimitiveType")
    {
      _isFallbackDescription = true;
    }
  };

  static ClassDescription::Ptr description(new FallbackDescription);
  return description;
}


// ====


ClassType::ClassDescription::ClassDescription(const std::string& name,
                                              const Optional<ClassType>& parent)
  : Description(name),
    parentId(parent ? Optional<Type::Id>(parent->id()) : Optional<Type::Id>(nothing))
{
  _isClassDescription = true;

  // No check needed, whether parent is a class, as if it's present as a ClassType, it's already
  // guaranteed, that it's a class
}

ClassType::ClassDescription::ClassDescription(const std::string& name)
  : Description(name),
    parentId(nothing)
{
  _isClassDescription = true;
}

ClassType::ClassDescription::Ptr ClassType::ClassDescription::create(const std::string& name,
                                                                     const Optional<ClassType>& parent)
{
  return Ptr(new ClassDescription(name, parent));
}

ClassType::ClassDescription::Ptr ClassType::ClassDescription::create(const std::string& name)
{
  return Ptr(new ClassDescription(name));
}


// ====

RttiClassMember::RttiClassMember()
  : _id(static_cast<Type::Id>(0))
   // StringIdManager guarants, that 0 is the empty string, which will never be the name of a type
{
}

ClassType RttiClassMember::type() const
{
  Optional<ClassType> classType = ClassType::fromType(typeId());

  if(classType)
    return *classType;
  else
    return ClassType();
}

void RttiClassMember::setTypeId(Type::Id id)
{
  _id = id;
  tags.insert(static_cast<Tag>(_id));
}

Type::Id RttiClassMember::typeId() const
{
  // If this assertion gets triggered, you forgot to use the RTTI_CLASS_SET_TYPE
  assert(_id != 0);

  return _id;
}


} // namespace Base
