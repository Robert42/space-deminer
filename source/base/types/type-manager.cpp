#include "type-manager.h"
#include "types.h"
#include "class-type.h"

namespace Base {


TypeManager::TypeManager()
{
}

std::shared_ptr<TypeManager> TypeManager::create()
{
  return std::shared_ptr<TypeManager>(new TypeManager);
}

Type::Id TypeManager::registerTypeDescription(const Type::Description::Ptr& description)
{
  Hashmap& typeDescriptions = singleton()._typeDescriptions;

  assert(typeDescriptions.find(description->id) == typeDescriptions.end());

  typeDescriptions.insert(description->id, description);
  return description->id;
}


Type::Id TypeManager::tryRegisterTypeDescription(const Type::Description::Ptr& description)
{
  Hashmap& typeDescriptions = singleton()._typeDescriptions;

  if(!isRegistered(description->id))
    typeDescriptions.insert(description->id, description);

  return description->id;
}


bool TypeManager::isRegistered(const Type::Description::Ptr& description)
{
  Hashmap& typeDescriptions = singleton()._typeDescriptions;
  return typeDescriptions.find(description->id) != typeDescriptions.end();
}

bool TypeManager::isRegistered(Type::Id id)
{
  Hashmap& typeDescriptions = singleton()._typeDescriptions;
  return typeDescriptions.find(id) != typeDescriptions.end();
}

Optional<Type::Description::Ptr> TypeManager::typeDescriptionForId(Type::Id id)
{
  Hashmap& typeDescriptions = singleton()._typeDescriptions;

  return optionalFromHashMap(typeDescriptions, id);
}


} // namespace Base
