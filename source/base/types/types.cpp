#include "types.h"

#include <base/geometry/ray.h>

namespace Base {
namespace Types {

Type _int;
Type _float;
Type _bool;

Type vec2;
Type vec3;
Type vec4;
Type ivec2;
Type ivec3;
Type ivec4;
Type bvec2;
Type bvec3;
Type bvec4;

Type Ray;

} // namespace Types


void initTypes()
{
  Types::_int = RTTI_PRIMITIVE_REGISTER_TYPE(int);
  Types::_float = RTTI_PRIMITIVE_REGISTER_TYPE(float);
  Types::_bool = RTTI_PRIMITIVE_BOOL_REGISTER_TYPE(bool);

  Types::vec2 = RTTI_PRIMITIVE_FLOAT_REGISTER_TYPE(vec2);
  Types::vec3 = RTTI_PRIMITIVE_FLOAT_REGISTER_TYPE(vec3);
  Types::vec4 = RTTI_PRIMITIVE_FLOAT_REGISTER_TYPE(vec4);
  Types::ivec2 = RTTI_PRIMITIVE_INT_REGISTER_TYPE(ivec2);
  Types::ivec3 = RTTI_PRIMITIVE_INT_REGISTER_TYPE(ivec3);
  Types::ivec4 = RTTI_PRIMITIVE_INT_REGISTER_TYPE(ivec4);
  Types::bvec2 = RTTI_PRIMITIVE_BOOL_REGISTER_TYPE(bvec2);
  Types::bvec3 = RTTI_PRIMITIVE_BOOL_REGISTER_TYPE(bvec3);
  Types::bvec4 = RTTI_PRIMITIVE_BOOL_REGISTER_TYPE(bvec4);

  Types::Ray = RTTI_PRIMITIVE_FLOAT_REGISTER_TYPE(Base::Geometry::Ray);
}


} // namespace Base
