#include "class-type.h"
#include "type-manager.h"

namespace Base {
namespace Private_ClassTypeTest
{

class Foo
{
public:
  RTTI_CLASS_MEMBER

  Foo()
  {
    RTTI_CLASS_SET_TYPE(Base::Private_ClassTypeTest::Foo)
  }
};

RTTI_CLASS_DECLARE(Base::Private_ClassTypeTest::Foo)

} // namespace Private_ClassTypeTest

using namespace Private_ClassTypeTest;


inline void testRegisteringFooClass()
{
  ClassType type_Foo = RTTI_CLASS_TRY_REGISTER_TYPE(Base::Private_ClassTypeTest::Foo);

  EXPECT_EQ(typeOf<Base::Private_ClassTypeTest::Foo>(), type_Foo.id());

  Foo foo;

  EXPECT_EQ(type_Foo, foo.rtti.type());
}


TEST(base, ClassType)
{
  testRegisteringFooClass();
  testRegisteringFooClass();
}


} // namespace Base
