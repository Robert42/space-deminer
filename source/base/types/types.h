#ifndef BASE_TYPES_H
#define BASE_TYPES_H

#include "class-type.h"

namespace Base {
namespace Types {

extern Type _int;
extern Type _float;
extern Type _bool;

extern Type vec2;
extern Type vec3;
extern Type vec4;
extern Type ivec2;
extern Type ivec3;
extern Type ivec4;
extern Type bvec2;
extern Type bvec3;
extern Type bvec4;
extern Type Ray;

} // namespace Types

void initTypes();

} // namespace Base

#endif // BASE_TYPES_H
