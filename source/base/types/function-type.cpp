#include "function-type.h"
#include "type-manager.h"

namespace Base {

FunctionType::FunctionType()
  : FunctionType(FunctionTypeDescription::create())
{
}

FunctionType::FunctionType(const FunctionTypeDescription::Ptr& functionDescription)
  : Type(functionDescription)
{
}

FunctionType::FunctionType(Optional<Type> returnType)
  : FunctionType(FunctionTypeDescription::create(returnType))
{
}

FunctionType::FunctionType(Optional<Type> returnType,
                           const QVector<Type>& argumentTypes)
  : FunctionType(FunctionTypeDescription::create(returnType,
                                                 argumentTypes))
{
}

FunctionType::FunctionType(Optional<Type> returnType,
                           const QVector<ArgumentType>& argumentTypes)
  : FunctionType(FunctionTypeDescription::create(returnType,
                                                 argumentTypes))
{
}


bool FunctionType::isFunctionType(const Type& type)
{
  return type.description()->isFunctionDescription();
}

Optional<FunctionType> FunctionType::fromType(const Type& type)
{
  const Description::Ptr& description = type.description();

  if(description->isFunctionDescription())
    return FunctionType(std::static_pointer_cast<FunctionTypeDescription>(description));
  else
    return nothing;
}


FunctionType::FunctionTypeDescription::Ptr FunctionType::description() const
{
  assert(Type::description()->isFunctionDescription());
  return std::static_pointer_cast<FunctionTypeDescription>(Type::description());
}


const Optional<Type>& FunctionType::returnType() const
{
  return description()->returnType;
}

const QVector<FunctionType::ArgumentType>& FunctionType::argumentTypes() const
{
  return description()->argumentTypes;
}


// ====


FunctionType::ArgumentType::ArgumentType()
  : ArgumentType(Type())
{
}

FunctionType::ArgumentType::ArgumentType(const Type& type, ReferenceType reference)
  : type(type),
    reference(reference)
{
}

void PrintTo(const FunctionType::ArgumentType& type, std::ostream* o)
{
  (*o) << type;
}

std::ostream& operator<<(std::ostream& o, const FunctionType::ArgumentType& argumentType)
{
  switch(argumentType.reference.value)
  {
  case FunctionType::ArgumentType::ReferenceType::IN:
    o << "in ";
    break;
  case FunctionType::ArgumentType::ReferenceType::OUT:
    o << "out ";
    break;
  case FunctionType::ArgumentType::ReferenceType::INOUT:
    o << "inout ";
    break;
  case FunctionType::ArgumentType::ReferenceType::NO_ANNOTATION:
  default:
    break;
  }

  return o << argumentType.type.name();
}


// ====


FunctionType::FunctionTypeDescription::FunctionTypeDescription(const std::string& name,
                                                               Optional<Type> returnType,
                                                               const QVector<ArgumentType>& argumentTypes)
  : Type::Description(name),
    returnType(returnType),
    argumentTypes(argumentTypes)
{
  _isFunctionTypeDescription = true;
}

FunctionType::FunctionTypeDescription::Ptr FunctionType::FunctionTypeDescription::create(Optional<Type> returnType)
{
  return create(returnType, QVector<ArgumentType>());
}

FunctionType::FunctionTypeDescription::Ptr FunctionType::FunctionTypeDescription::create(Optional<Type> returnType,
                                                                                         const QVector<Type>& argumentTypes)
{
  QVector<ArgumentType> convertedArgumentTypes;

  convertedArgumentTypes.reserve(argumentTypes.size());

  for(const Type& type : argumentTypes)
    convertedArgumentTypes.append(ArgumentType(type));

  return create(returnType, convertedArgumentTypes);
}


FunctionType::FunctionTypeDescription::Ptr FunctionType::FunctionTypeDescription::create(Optional<Type> returnType,
                                                                                         const QVector<ArgumentType>& argumentTypes)
{
  std::string name = calcName(returnType, argumentTypes);

  Type::Id id = Type::typeIdFromTypeName(name);

  Optional<Ptr> optionalDescription = functionTypeDescriptionForId(id);

  if(optionalDescription)
    return *optionalDescription;

  Ptr description = Ptr(new FunctionTypeDescription(name, returnType, argumentTypes));

  TypeManager::registerTypeDescription(description);

  return description;
}

std::string FunctionType::FunctionTypeDescription::calcName(Optional<Type> returnType,
                                                            const QVector<ArgumentType>& argumentTypes)
{
  std::ostringstream s;

  if(returnType)
    s << returnType->name();
  else
    s << "void";

  s << '(';

  bool first = true;
  for(const ArgumentType& argumentType : argumentTypes)
  {
    if(!first)
    {
      s << ',';
    }

    s << argumentType;

    first = false;
  }

  s << ')';

  return s.str();
}

Optional<FunctionType::FunctionTypeDescription::Ptr> FunctionType::FunctionTypeDescription::functionTypeDescriptionForId(Type::Id typeId)
{
  Optional<Description::Ptr> optionalDescription = TypeManager::typeDescriptionForId(typeId);

  if(!optionalDescription)
    return nothing;

  Description::Ptr description = *optionalDescription;

  if(!description->isFunctionDescription())
    return nothing;

  return std::static_pointer_cast<FunctionTypeDescription>(description);
}

// ====


} // namespace Base
