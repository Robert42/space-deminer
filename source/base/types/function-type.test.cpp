#include "function-type.h"
#include "types.h"

namespace Base {

TEST(base, FunctionType)
{
  EXPECT_EQ("vec2", Types::vec2.name());

  FunctionType a(Types::_bool, {Types::vec2, Types::_float});

  EXPECT_EQ("bool(vec2,float)", a.name());

  FunctionType b(Types::_bool, {FunctionType::ArgumentType(Types::vec2, FunctionType::ArgumentType::ReferenceType::OUT), FunctionType::ArgumentType(Types::_float)});

  EXPECT_EQ("bool(out vec2,float)", b.name());
}


} // namespace Base
