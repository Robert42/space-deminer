#ifndef BASE_TYPE_H
#define BASE_TYPE_H

#include <base/optional.h>
#include <base/strings/string-id-manager.h>
#include <base/tag-set.h>
#include <base/cxx-typeid-name.h>

namespace Base {

class Type
{
public:
  enum Id : std::underlying_type<StringId>::type
  {
  };

public:
  class PrimitiveDescription;
  class Description
  {
  public:
    typedef std::shared_ptr<Description> Ptr;

  protected:
    bool _isClassDescription : 1;
    bool _isPrimitiveDescription : 1;
    bool _isFunctionTypeDescription : 1;
    bool _isFallbackDescription : 1;

  public:
    const Type::Id id;
    const std::string name;

  protected:
    Description(const std::string& name);

  public:
    bool isClassDescription() const;
    bool isPrimitiveDescription() const;
    bool isFunctionDescription() const;
    bool isFallbackDescription() const;
  };

  class PrimitiveDescription : public Description
  {
  public:
    typedef std::shared_ptr<PrimitiveDescription> Ptr;

  public:
    const size_t size;

    const bool floatType : 1;
    const bool intType : 1;
    const bool boolType : 1;

  protected:
    PrimitiveDescription(const std::string& name, size_t size, bool floatType, bool intType, bool boolType);

  public:
    static Ptr create(const std::string& name, size_t size, bool floatType, bool intType, bool boolType);
  };

private:
  Description::Ptr _description;

public:
  Type();
  explicit Type(const Description::Ptr& description);
  Type(Type::Id typeId);
  virtual ~Type();

  template<typename T>
  static Type registerPrimitiveType(const std::string& name, bool floatType, bool intType, bool boolType);
  template<typename T>
  static Type tryRegisterPrimitiveType(const std::string& name, bool floatType, bool intType, bool boolType);

  static Id typeIdFromTypeName(StringId name);
  static Id typeIdFromTypeName(const std::string& name);
  static Id typeIdFromTypeName(const char* name);

  Id id() const;
  const std::string& name() const;
  StringId nameId() const;
  Tag tag() const;

  const Description::Ptr& description() const;

  operator Id() const;

  bool operator==(const Type& type) const;
  bool operator!=(const Type& type) const;
  bool operator<(const Type& type) const;
  bool operator<=(const Type& type) const;
  bool operator>(const Type& type) const;
  bool operator>=(const Type& type) const;

private:
  static Description::Ptr fallbackDescription();

protected:
  static Type _registerTypeDescription(const Description::Ptr& description);
  static Type _tryRegisterTypeDescription(const Description::Ptr& description);
};

template<typename T>
std::basic_ostream<T>& operator<<(std::basic_ostream<T>& o, const Type& type);

void PrintTo(const Type& type, std::ostream* o);

int qHash(Type::Id typeId, uint seed=0);
int qHash(const Type& type, uint seed=0);


template<typename T>
Type::Id typeOf()
{
  _typeOf_static_assert_same_type(static_cast<T*>(nullptr), static_cast<T*>(nullptr));
  return _typeOf(static_cast<T*>(nullptr));
}

#define _RTTI_BASIC_DECLARE(name, cpptype) \
  inline ::Base::Type::Id _typeOf(cpptype*) \
  { \
    ::Base::assertSameNameAsType<cpptype>(name); \
    static ::Base::Type::Id typeId = ::Base::Type::typeIdFromTypeName(name); \
    return typeId; \
  } \
  template<typename T> \
  void _typeOf_static_assert_same_type(cpptype*, T*) \
  { \
    static_assert(std::is_same<T, cpptype>::value, "No _typeOf function defined for the type T. Did you forget to use the declare macro?"); \
  }

#define _RTTI_PRIMITIVE_DECLARE(name, cpptype) \
  _RTTI_BASIC_DECLARE(name, cpptype)
#define _RTTI_PRIMITIVE_TRY_REGISTER_TYPE(name, cpptype) Type::tryRegisterPrimitiveType<cpptype>(name)
#define _RTTI_PRIMITIVE_REGISTER_TYPE(name, cpptype, floatType, intType, boolType) Type::registerPrimitiveType<cpptype>(name, floatType, intType, boolType)

#define RTTI_PRIMITIVE_DECLARE(name) _RTTI_PRIMITIVE_DECLARE(#name, name)
#define RTTI_PRIMITIVE_TRY_REGISTER_TYPE(name) _RTTI_PRIMITIVE_TRY_REGISTER_TYPE(#name, name)
#define RTTI_PRIMITIVE_REGISTER_TYPE(name) _RTTI_PRIMITIVE_REGISTER_TYPE(#name, name, false, false, false)

#define RTTI_PRIMITIVE_FLOAT_REGISTER_TYPE(name) _RTTI_PRIMITIVE_REGISTER_TYPE(#name, name, true, false, false)
#define RTTI_PRIMITIVE_INT_REGISTER_TYPE(name) _RTTI_PRIMITIVE_REGISTER_TYPE(#name, name, false, true, false)
#define RTTI_PRIMITIVE_BOOL_REGISTER_TYPE(name) _RTTI_PRIMITIVE_REGISTER_TYPE(#name, name, false, false, true)


RTTI_PRIMITIVE_DECLARE(vec2)
RTTI_PRIMITIVE_DECLARE(vec3)
RTTI_PRIMITIVE_DECLARE(vec4)
RTTI_PRIMITIVE_DECLARE(ivec2)
RTTI_PRIMITIVE_DECLARE(ivec3)
RTTI_PRIMITIVE_DECLARE(ivec4)
RTTI_PRIMITIVE_DECLARE(bvec2)
RTTI_PRIMITIVE_DECLARE(bvec3)
RTTI_PRIMITIVE_DECLARE(bvec4)

} // namespace Base

RTTI_PRIMITIVE_DECLARE(int)
RTTI_PRIMITIVE_DECLARE(float)
RTTI_PRIMITIVE_DECLARE(bool)

#include "type.inl"

#endif // BASE_TYPE_H
