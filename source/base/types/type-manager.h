#ifndef BASE_TYPEMANAGER_H
#define BASE_TYPEMANAGER_H

#include "type.h"

namespace Base {

class TypeManager : public BlackboxSingleton<TypeManager>
{
  friend class Type;

private:
  typedef QHash<Type::Id, Type::Description::Ptr> Hashmap;
  Hashmap _typeDescriptions;

private:
  friend class BlackboxSingleton<TypeManager>;
  TypeManager();

public:
  static std::shared_ptr<TypeManager> create();

public:
  static Type::Id registerTypeDescription(const Type::Description::Ptr& description);
  static Type::Id tryRegisterTypeDescription(const Type::Description::Ptr& description);
  static bool isRegistered(const Type::Description::Ptr& description);
  static bool isRegistered(Type::Id id);
  static Optional<Type::Description::Ptr> typeDescriptionForId(Type::Id id);
};

} // namespace Base

#endif // BASE_TYPEMANAGER_H
