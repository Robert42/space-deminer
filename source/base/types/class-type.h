#ifndef BASE_CLASSTYPE_H
#define BASE_CLASSTYPE_H

#include "./type.h"

namespace Base {

/** An instance of ClassType holds [rtti](http://en.wikipedia.org/wiki/Run-time_type_information)
 * information about a class.
 *
 * You should use the rtti system as seldon as possible. There are rare cases, where you need this.
 *
 * To add RTTI support, you need to use the following macros
 * - `RTTI_CLASS_MEMBER` Use this class in the root class of your hierarchy, so the class will know
 *    its type id
 * - `RTTI_CLASS_SET_TYPE` Sets theright id into the `rtti` member object
 * - `RTTI_CLASS_DECLARE` Declares the right `_typeOf` helper function, so `typeOf<>()` will return
 *   now the valid type-id for the new class.
 * - `RTTI_CLASS_REGISTER_TYPE`/`RTTI_CLASS_TRY_REGISTER_TYPE`/`RTTI_CLASS_REGISTER_TYPE_WITH_BASE`/
 *   `RTTI_CLASS_TRY_REGISTER_TYPE_WITH_BASE` register the class-type.
 *
 * Whenever you are using on of the RTTI macros, please always use the full type name (including
 * namespaces). I've added some assertions on g++ 4.8 or higher to prevent you from doing otherwise.
 *
 * Example:
 * ```
 * namespace A
 * {
 *
 * class Foo
 * {
 *   // Foo is the root of our hierarchy, so it will have the rtti member
 *   RTTI_CLASS_MEMBER
 *
 * public:
 *   Foo();
 * };
 *
 * RTTI_CLASS_DECLARE(A::Foo)
 *
 * Foo::Foo()
 * {
 *   RTTI_CLASS_SET_TYPE(A::Foo);
 * }
 *
 * }
 *
 * namespace B
 * {
 *
 * class Bar : public A::Foo
 * {
 * };
 *
 * RTTI_CLASS_DECLARE(B::Bar)
 *
 * Foo::Foo()
 * {
 *   RTTI_CLASS_SET_TYPE(B::Bar);
 * }
 *
 * }
 *
 * using namespace A;
 * using namespace B;
 *
 * namespace Types
 * {
 *   ClassType Foo, Bar;
 * }
 *
 * void main()
 * {
 *   Types::Foo = RTTI_CLASS_REGISTER_TYPE(A::Foo)
 *   Types::Bar = RTTI_CLASS_REGISTER_TYPE_WITH_BASE(B::Bar, A::Foo)
 *
 *   Bar bar;
 *   Foo& foo = bar;
 *
 *   std::cout << foo.name() << std::endl; // Prints "B::Bar"
 *   std::cout << foo.type() == Types::Bar << std::endl; // Prints 1
 * }
 * ```
 */
class ClassType : public Type
{
public:
  class ClassDescription : public Description
  {
  public:
    typedef std::shared_ptr<ClassDescription> Ptr;

  public:
    const Optional<Type::Id> parentId;

  protected:
    // This two version of the constructor do exist, as while creating the fallback classDescription,
    // nothing would be given as parent, resulting in an stack overflow:
    // The instance of Optional<ClassType> would create a new instance of ClassType wanting to create
    // a fallback ClassDescription instance, which would give nothing as parent.
    ClassDescription(const std::string& name, const Optional<ClassType>& parent);
    ClassDescription(const std::string& name);

  public:
    static Ptr create(const std::string& name, const Optional<ClassType>& parent);
    static Ptr create(const std::string& name);
  };

private:
  explicit ClassType(const ClassDescription::Ptr& classDescription);

public:
  ClassType();

  static bool isClassType(const Type& type);
  static Optional<ClassType> fromType(const Type& type);

  ClassDescription::Ptr description() const;

  Optional<ClassType> baseClass() const;
  bool isInheritingOrSame(const Type& baseClass) const;

public:
  template<typename T_result>
  static Optional<T_result> tryCalcValueForWholeHierarchy(const ClassType& type,
                                                          const std::function<Optional<T_result>(const ClassType& type)>& function);
  template<typename T_result>
  static T_result calcValueForWholeHierarchy(const ClassType& type,
                                             const T_result& fallbackValue,
                                             const std::function<Optional<T_result>(const ClassType& type)>& function);

  template<typename T_result>
  static Optional<T_result> tryCalcValueForTwoHierarchies(const ClassType& typeA,
                                                          const ClassType& typeB,
                                                          const std::function<Optional<T_result>(const ClassType& typeA, const ClassType& typeB)>& function);
  template<typename T_result>
  static T_result calcValueForTwoHierarchies(const ClassType& typeA,
                                             const ClassType& typeB,
                                             const T_result& fallbackValue,
                                             const std::function<Optional<T_result>(const ClassType& typeA, const ClassType& typeB)>& function);

  template<typename T>
  static ClassType registerClassType(const std::string& name);
  template<typename T>
  static ClassType tryRegisterClassType(const std::string& name);
  template<typename T, typename T_base>
  static ClassType registerClassTypeWithBase(const std::string& name);
  template<typename T, typename T_base>
  static ClassType tryRegisterClassTypeWithBase(const std::string& name);

private:
  static ClassDescription::Ptr fallbackClassDescription();

  template<typename T>
  static void assertionsForNewClassType(const std::string& name);
};

#define _RTTI_DECLARE_CLASS(name, cpptype) \
  _RTTI_BASIC_DECLARE(name, cpptype)

#define _RTTI_CLASS_TRY_REGISTER_TYPE_WITH_BASE(name, cpptype, base) ClassType::tryRegisterClassTypeWithBase<cpptype, base>(name);
#define _RTTI_CLASS_REGISTER_TYPE_WITH_BASE(name, cpptype, base) ClassType::registerClassTypeWithBase<cpptype, base>(name);
#define _RTTI_CLASS_TRY_REGISTER_TYPE(name, cpptype) ClassType::tryRegisterClassType<cpptype>(name);
#define _RTTI_CLASS_REGISTER_TYPE(name, cpptype) ClassType::registerClassType<cpptype>(name);

class RttiClassMember
{
private:
  Type::Id _id;

public:
  TagSet tags;

public:
  RttiClassMember();

  ClassType type() const;

  void setTypeId(Type::Id id);
  Type::Id typeId() const;
};

/**
 * @note Note that you must only use this macro in the highest class of the class hierarchy with rtti
 * support. There should be only one class holding the rtti member in each class
 * hierarchy.
 * As a consequence, multiple inheritance also is not supported.
 */
#define RTTI_CLASS_MEMBER public: RttiClassMember rtti;
#define RTTI_CLASS_SET_TYPE(name) \
  this->rtti.setTypeId(typeOf<typename name>()); \
  ::Base::assertSameNameAsType<name>(#name);

#define RTTI_CLASS_DECLARE(name) _RTTI_DECLARE_CLASS(#name, name)
#define RTTI_CLASS_TRY_REGISTER_TYPE_WITH_BASE(name, base) _RTTI_CLASS_TRY_REGISTER_TYPE_WITH_BASE(#name, name, base)
#define RTTI_CLASS_REGISTER_TYPE_WITH_BASE(name, base) _RTTI_CLASS_REGISTER_TYPE_WITH_BASE(#name, name, base)
#define RTTI_CLASS_TRY_REGISTER_TYPE(name) _RTTI_CLASS_TRY_REGISTER_TYPE(#name, name)
#define RTTI_CLASS_REGISTER_TYPE(name) _RTTI_CLASS_REGISTER_TYPE(#name, name)

} // namespace Base

#include "class-type.inl"

#endif // BASE_CLASSTYPE_H
