#include <dependencies.h>
#include <base/enum-mapper.h>

namespace Base {


enum Foo
{
  FOO_MINUS_2 = -2,
  FOO_MINUS_1 = -1,
  FOO_0 = 0,
  FOO_1 = 1,
  FOO_2 = 2,
  FOO_3 = 3,
  FOO_100 = 100,
  FOO_200 = 200,
  FOO_255 = 255,
  FOO_256 = 256,
  FOO_257 = 257,
  FOO_300 = 300
};

enum Bar
{
  BAR_MINUS_2 = 1000-2,
  BAR_MINUS_1 = 1000-1,
  BAR_0 = 1000+0,
  BAR_1 = 1000+1,
  BAR_2 = 1000+2,
  BAR_3 = 1000+3,
  BAR_100 = 1000+100,
  BAR_200 = 1000+200,
  BAR_255 = 1000+255,
  BAR_256 = 1000+256,
  BAR_257 = 1000+257,
  BAR_300 = 1000+300
};


TEST(base_EnumMapper_Range, contains)
{
  EnumMapper<Foo, Bar>::Range range(FOO_0, FOO_255);

  EXPECT_FALSE(range.contains(FOO_MINUS_2));
  EXPECT_FALSE(range.contains(FOO_MINUS_1));
  EXPECT_TRUE(range.contains(FOO_0));
  EXPECT_TRUE(range.contains(FOO_1));
  EXPECT_TRUE(range.contains(FOO_255));
  EXPECT_FALSE(range.contains(FOO_256));
}


TEST(base_EnumMapper_Range, getIndex)
{
  EnumMapper<Foo, Bar>::Range range;

  EXPECT_EQ(static_cast<size_t>(-2), range.indexOf(FOO_MINUS_2));
  EXPECT_EQ(static_cast<size_t>(-1), range.indexOf(FOO_MINUS_1));
  EXPECT_EQ(static_cast<size_t>(0), range.indexOf(FOO_0));
  EXPECT_EQ(static_cast<size_t>(1), range.indexOf(FOO_1));
  EXPECT_EQ(static_cast<size_t>(255), range.indexOf(FOO_255));
  EXPECT_EQ(static_cast<size_t>(256), range.indexOf(FOO_256));

  EnumMapper<Bar, Foo>::Range rangeWithOffset(BAR_0, BAR_255);

  EXPECT_EQ(static_cast<size_t>(-2), rangeWithOffset.indexOf(BAR_MINUS_2));
  EXPECT_EQ(static_cast<size_t>(-1), rangeWithOffset.indexOf(BAR_MINUS_1));
  EXPECT_EQ(static_cast<size_t>(0), rangeWithOffset.indexOf(BAR_0));
  EXPECT_EQ(static_cast<size_t>(1), rangeWithOffset.indexOf(BAR_1));
  EXPECT_EQ(static_cast<size_t>(255), rangeWithOffset.indexOf(BAR_255));
  EXPECT_EQ(static_cast<size_t>(256), rangeWithOffset.indexOf(BAR_256));
}


TEST(base_EnumMapper_Range, rangeLength)
{
  EnumMapper<Foo, Bar>::Range range_0_255(FOO_0, FOO_255);
  EnumMapper<Foo, Bar>::Range range_0_256(FOO_0, FOO_256);
  EnumMapper<Foo, Bar>::Range range_1_256(FOO_1, FOO_256);

  EXPECT_EQ(static_cast<size_t>(256), range_0_255.rangeLength());
  EXPECT_EQ(static_cast<size_t>(257), range_0_256.rangeLength());
  EXPECT_EQ(static_cast<size_t>(256), range_1_256.rangeLength());
}


TEST(base_EnumMapper, values_smaller_than_the_direct_range)
{
  EnumMapper<Foo, Bar> mapper(EnumMapper<Foo, Bar>::Range(FOO_0, FOO_255));

  mapper.addMapping(FOO_MINUS_2, BAR_MINUS_2);
  mapper.addMapping(FOO_MINUS_1, BAR_MINUS_1);
  mapper.addMapping(FOO_0, BAR_0);

  EXPECT_EQ(BAR_MINUS_2, mapper[FOO_MINUS_2]);
  EXPECT_EQ(BAR_MINUS_1, mapper[FOO_MINUS_1]);
  EXPECT_EQ(BAR_0, mapper[FOO_0]);
}


TEST(base_EnumMapper, values_larger_than_the_direct_range)
{
  EnumMapper<Foo, Bar> mapper(EnumMapper<Foo, Bar>::Range(FOO_0, FOO_255));

  mapper.addMapping(FOO_255, BAR_255);
  mapper.addMapping(FOO_256, BAR_256);
  mapper.addMapping(FOO_257, BAR_257);

  EXPECT_EQ(BAR_255, mapper[FOO_255]);
  EXPECT_EQ(BAR_256, mapper[FOO_256]);
  EXPECT_EQ(BAR_257, mapper[FOO_257]);
}


TEST(base_EnumMapper, very_small_range)
{
  EnumMapper<Foo, Bar> mapper(EnumMapper<Foo, Bar>::Range(FOO_1, FOO_1));

  mapper.addMapping(FOO_0, BAR_0);
  mapper.addMapping(FOO_1, BAR_1);
  mapper.addMapping(FOO_2, BAR_2);

  EXPECT_EQ(BAR_0, mapper[FOO_0]);
  EXPECT_EQ(BAR_1, mapper[FOO_1]);
  EXPECT_EQ(BAR_2, mapper[FOO_2]);
}

inline void create_illegal_range()
{
  EnumMapper<Foo, Bar>::Range(FOO_1, FOO_0);
}

TEST(base_EnumMapper, illegal_range2)
{
  EXPECT_THROW(create_illegal_range(), std::invalid_argument);
}


} // namespace Base

