#ifndef BASE_MATH_H
#define BASE_MATH_H

#include <dependencies.h>

namespace Base {


int floorDivision(int a, int b);
int ceilDivision(int a, int b);
uint64 floorDivision(uint64 a, uint64 b);
uint64 ceilDivision(uint64 a, uint64 b);
uint32 floorDivision(uint32 a, uint32 b);
uint32 ceilDivision(uint32 a, uint32 b);
uint16 floorDivision(uint16 a, uint16 b);
uint16 ceilDivision(uint16 a, uint16 b);
uint8 floorDivision(uint16 a, uint8 b);
uint8 ceilDivision(uint16 a, uint8 b);
int64 floorDivision(int64 a, int64 b);
int64 ceilDivision(int64 a, int64 b);
int16 floorDivision(int16 a, int16 b);
int16 ceilDivision(int16 a, int16 b);
int8 floorDivision(int16 a, int8 b);
int8 ceilDivision(int16 a, int8 b);
real floorDivision(real a, real b);
real ceilDivision(real a, real b);
double floorDivision(double a, double b);
double ceilDivision(double a, double b);


int mod(int x, int y);
ivec2 mod(ivec2 x, int y);
ivec3 mod(ivec3 x, int y);
ivec4 mod(ivec4 x, int y);
ivec2 mod(ivec2 x, ivec2 y);
ivec3 mod(ivec3 x, ivec3 y);
ivec4 mod(ivec4 x, ivec4 y);


template<typename T>
T clamp(const T& x, const T& a, const T& b)
{
  return min<T>(max<T>(x, a), b);
}


template<typename T>
T sq(const T& x)
{
  return x*x;
}


template<typename T>
void exchange(T* a, T* b)
{
  T temp = *a;
  *a = *b;
  *b = temp;
}


template<typename A, glm::precision B>
inline glm::detail::tvec2<A, B> perpendicular90Degree(const glm::detail::tvec2<A, B>& v)
{
  return glm::detail::tvec2<A, B>(-v.y, v.x);
}

template<typename A, glm::precision B>
inline glm::detail::tvec2<A, B> perpendicular270Degree(const glm::detail::tvec2<A, B>& v)
{
  return glm::detail::tvec2<A, B>(v.y, -v.x);
}


/** Rounds a positive integer up to a given power of two.
 *
 * Examples:
 * ```
 * roundPositiveUpToMultipleOfPowerOfTwo(1,3); // Returns 8
 * roundPositiveUpToMultipleOfPowerOfTwo(1,2); // Returns 4
 * roundPositiveUpToMultipleOfPowerOfTwo(1,1); // Returns 2
 *
 * roundPositiveUpToMultipleOfPowerOfTwo(0,2); // Returns 0
 * roundPositiveUpToMultipleOfPowerOfTwo(1,2); // Returns 4
 * roundPositiveUpToMultipleOfPowerOfTwo(2,2); // Returns 4
 * roundPositiveUpToMultipleOfPowerOfTwo(3,2); // Returns 4
 * roundPositiveUpToMultipleOfPowerOfTwo(4,2); // Returns 4
 * roundPositiveUpToMultipleOfPowerOfTwo(5,2); // Returns 8
 * ```
 */
template<typename T>
inline T roundPositiveUpToMultipleOfPowerOfTwo(T value, T power)
{
  T mask = (1<<power)-1;

  return (value+mask) & (~mask);
}

uint8 bin_log_floor(uint8 x);
uint16 bin_log_floor(uint16 x);
uint32 bin_log_floor(uint32 x);
uint64 bin_log_floor(uint64 x);

int8 bin_log_floor(int8 x);
int16 bin_log_floor(int16 x);
int32 bin_log_floor(int32 x);
int64 bin_log_floor(int64 x);

template<typename A, glm::precision B>
inline glm::detail::tvec2<A, B> bin_log_floor(const glm::detail::tvec2<A, B>& v)
{
  static_assert(std::is_integral<A>::value, "bin_log_floor accepts only tvec2<A> with A being an integer type");
  return glm::detail::tvec2<A, B>(bin_log_floor(v.x),
                                  bin_log_floor(v.y));
}

template<typename A, glm::precision B>
inline glm::detail::tvec3<A, B> bin_log_floor(const glm::detail::tvec3<A, B>& v)
{
  static_assert(std::is_integral<A>::value, "bin_log_floor accepts only tvec3<A> with A being an integer type");
  return glm::detail::tvec3<A, B>(bin_log_floor(v.x),
                                  bin_log_floor(v.y),
                                  bin_log_floor(v.z));
}

template<typename A, glm::precision B>
inline glm::detail::tvec4<A, B> bin_log_floor(const glm::detail::tvec4<A, B>& v)
{
  static_assert(std::is_integral<A>::value, "bin_log_floor accepts only tvec3<A> with A being an integer type");
  return glm::detail::tvec4<A, B>(bin_log_floor(v.x),
                                  bin_log_floor(v.y),
                                  bin_log_floor(v.z),
                                  bin_log_floor(v.w));
}

template<typename T>
T bin_log_ceil(T x)
{
  static_assert(std::is_integral<T>::value, "bin_log_ceil accepts only an integer type");

  T l = bin_log_floor(x);

  if(1<<l < x)
    return l+1;
  return l;
}

template<typename A, glm::precision B>
inline glm::detail::tvec2<A, B> bin_log_ceil(const glm::detail::tvec2<A, B>& v)
{
  static_assert(std::is_integral<A>::value, "bin_log_ceil accepts only tvec2<A> with A being an integer type");
  return glm::detail::tvec2<A, B>(bin_log_ceil(v.x),
                                  bin_log_ceil(v.y));
}

template<typename A, glm::precision B>
inline glm::detail::tvec3<A, B> bin_log_ceil(const glm::detail::tvec3<A, B>& v)
{
  static_assert(std::is_integral<A>::value, "bin_log_ceil accepts only tvec3<A> with A being an integer type");
  return glm::detail::tvec3<A, B>(bin_log_ceil(v.x),
                                  bin_log_ceil(v.y),
                                  bin_log_ceil(v.z));
}

template<typename A, glm::precision B>
inline glm::detail::tvec4<A, B> bin_log_ceil(const glm::detail::tvec4<A, B>& v)
{
  static_assert(std::is_integral<A>::value, "bin_log_ceil accepts only tvec3<A> with A being an integer type");
  return glm::detail::tvec4<A, B>(bin_log_ceil(v.x),
                                  bin_log_ceil(v.y),
                                  bin_log_ceil(v.z),
                                  bin_log_ceil(v.w));
}


template<typename T>
T round_up_to_power_of_two(T x)
{
  static_assert(std::is_integral<T>::value, "round_up_to_power_of_two accepts only an integer type");

  return 1<<bin_log_ceil(x);
}

template<typename A, glm::precision B>
inline glm::detail::tvec2<A, B> round_up_to_power_of_two(const glm::detail::tvec2<A, B>& v)
{
  static_assert(std::is_integral<A>::value, "round_up_to_power_of_two accepts only tvec2<A> with A being an integer type");
  return glm::detail::tvec2<A, B>(round_up_to_power_of_two(v.x),
                                  round_up_to_power_of_two(v.y));
}

template<typename A, glm::precision B>
inline glm::detail::tvec3<A, B> round_up_to_power_of_two(const glm::detail::tvec3<A, B>& v)
{
  static_assert(std::is_integral<A>::value, "round_up_to_power_of_two accepts only tvec3<A> with A being an integer type");
  return glm::detail::tvec3<A, B>(round_up_to_power_of_two(v.x),
                                  round_up_to_power_of_two(v.y),
                                  round_up_to_power_of_two(v.z));
}

template<typename A, glm::precision B>
inline glm::detail::tvec4<A, B> round_up_to_power_of_two(const glm::detail::tvec4<A, B>& v)
{
  static_assert(std::is_integral<A>::value, "round_up_to_power_of_two accepts only tvec3<A> with A being an integer type");
  return glm::detail::tvec4<A, B>(round_up_to_power_of_two(v.x),
                                  round_up_to_power_of_two(v.y),
                                  round_up_to_power_of_two(v.z),
                                  round_up_to_power_of_two(v.w));
}

template<typename T>
T round_down_to_power_of_two(T x)
{
  static_assert(std::is_integral<T>::value, "round_down_to_power_of_two accepts only an integer type");

  return 1<<bin_log_floor(x);
}

template<typename A, glm::precision B>
inline glm::detail::tvec2<A, B> round_down_to_power_of_two(const glm::detail::tvec2<A, B>& v)
{
  static_assert(std::is_integral<A>::value, "round_down_to_power_of_two accepts only tvec2<A> with A being an integer type");
  return glm::detail::tvec2<A, B>(round_down_to_power_of_two(v.x),
                                  round_down_to_power_of_two(v.y));
}

template<typename A, glm::precision B>
inline glm::detail::tvec3<A, B> round_down_to_power_of_two(const glm::detail::tvec3<A, B>& v)
{
  static_assert(std::is_integral<A>::value, "round_down_to_power_of_two accepts only tvec3<A> with A being an integer type");
  return glm::detail::tvec3<A, B>(round_down_to_power_of_two(v.x),
                                  round_down_to_power_of_two(v.y),
                                  round_down_to_power_of_two(v.z));
}

template<typename A, glm::precision B>
inline glm::detail::tvec4<A, B> round_down_to_power_of_two(const glm::detail::tvec4<A, B>& v)
{
  static_assert(std::is_integral<A>::value, "round_down_to_power_of_two accepts only tvec3<A> with A being an integer type");
  return glm::detail::tvec4<A, B>(round_down_to_power_of_two(v.x),
                                  round_down_to_power_of_two(v.y),
                                  round_down_to_power_of_two(v.z),
                                  round_down_to_power_of_two(v.w));
}


template<typename T>
int numberOfSetBits(const T& x)
{
  int n = 0;
  int nBytes = sizeof(T);

  for(int i=0; i<nBytes; ++i)
  {
    uint8 y = reinterpret_cast<const uint8*>(&x)[i];
    while(y!=0)
    {
      if(T(1) & y)
        n++;

      y >>= 1;
    }
  }

  return n;
}


template<typename T>
bool isPowerOfTwo(const T& x)
{
  return round_down_to_power_of_two(x) == x;
}


} // namespace Base

#endif // BASE_MATH_H
