#ifndef BASE_BASEAPPLICATION_H
#define BASE_BASEAPPLICATION_H

#include <base/singleton.h>
#include <base/signals.h>
#include <base/io/directory.h>
#include <base/i18n/language.h>

namespace Base {

class StringIdManager;
class TypeManager;

namespace Private {
class OgreCore;
} // namespace Private

namespace Scripting {
class Engine;
} // namespace Scripting
namespace CodeGenerator {
class ModuleManager;
} // namespace CodeGenerator

class BaseApplication : public PublicSingleton<BaseApplication>
{
public:
  Signals::Trackable trackable;

private:
  std::shared_ptr<Private::OgreCore> ogreCore;
  std::shared_ptr<Base::Scripting::Engine> scriptingEngine;
  std::shared_ptr<CodeGenerator::ModuleManager> codeGeneratorModuleManager;

  IO::Directory _configDirectory;

  Signals::CallableSignal<void()> _signalLanguageChanged;
  I18n::Language _language;

  const String _name;

protected:
  BaseApplication(const String& applicationName);
  virtual ~BaseApplication();

protected:
  virtual void startup();

  /** Returns a path to the directory, where the local configuration the files are stored, like the user settings.
   *
   * @return a path to a existing directory
   */
  static const IO::Directory& configDirectory();

public:
  static const String& name();

  static const I18n::Language& language();
  static void setLanguage(const I18n::Language& language);
  static Signals::Signal<void()>& signalLanguageChanged();
};

} // namespace Base

#endif // BASE_BASEAPPLICATION_H
