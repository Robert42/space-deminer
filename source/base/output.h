#ifndef _BASE_OUTPUT_H_
#define _BASE_OUTPUT_H_


#include <dependencies.h>


namespace Base {


template<typename T>
class Output
{
public:
  T& value;

  explicit Output(T& reference) : value(reference)
  {
  }
};

template<typename T>
Output<T> out(T& var)
{
  return Output<T>(var);
}


template<typename T>
class InOutput
{
public:
  T& value;

  explicit InOutput(T& reference) : value(reference)
  {
  }
};

template<typename T>
InOutput<T> inout(T& var)
{
  return InOutput<T>(var);
}


} // namespace Base

#endif
