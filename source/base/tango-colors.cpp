#include <base/tango-colors.h>

namespace Base {
namespace Tango {


inline vec4 colorFromTangoHex(unsigned int hex)
{
  Ogre::ColourValue c;
  c.setAsARGB(0xff000000 | hex);

  return vec4_cast(c);
}

const vec4 Butter_Dark = colorFromTangoHex(0xc4a000);
const vec4 Butter_Middle = colorFromTangoHex(0xedd400);
const vec4 Butter_Light = colorFromTangoHex(0xfce94f);
const vec4 Orange_Dark = colorFromTangoHex(0xce5c00);
const vec4 Orange_Middle = colorFromTangoHex(0xf57900);
const vec4 Orange_Light = colorFromTangoHex(0xfcaf3e);
const vec4 Chocolate_Dark = colorFromTangoHex(0x8f5902);
const vec4 Chocolate_Middle = colorFromTangoHex(0xc17d11);
const vec4 Chocolate_Light = colorFromTangoHex(0xe9b96e);
const vec4 Chameleon_Dark = colorFromTangoHex(0x4e9a06);
const vec4 Chameleon_Middle = colorFromTangoHex(0x73d216);
const vec4 Chameleon_Light = colorFromTangoHex(0x8ae234);
const vec4 SkyBlue_Dark = colorFromTangoHex(0x204a87);
const vec4 SkyBlue_Middle = colorFromTangoHex(0x3465a4);
const vec4 SkyBlue_Light = colorFromTangoHex(0x729fcf);
const vec4 Plum_Dark = colorFromTangoHex(0x5c3566);
const vec4 Plum_Middle = colorFromTangoHex(0x75507b);
const vec4 Plum_Light = colorFromTangoHex(0xad7fa8);
const vec4 ScarletRed_Dark = colorFromTangoHex(0xa40000);
const vec4 ScarletRed_Middle = colorFromTangoHex(0xcc0000);
const vec4 ScarletRed_Light = colorFromTangoHex(0xef2929);
const vec4 AluminiumLight_Dark = colorFromTangoHex(0xbabdb6);
const vec4 AluminiumLight_Middle = colorFromTangoHex(0xd3d7cf);
const vec4 AluminiumLight_Light = colorFromTangoHex(0xeeeeec);
const vec4 AluminiumDark_Dark = colorFromTangoHex(0x2e3436);
const vec4 AluminiumDark_Middle = colorFromTangoHex(0x555753);
const vec4 AluminiumDark_Light = colorFromTangoHex(0x888a85);
const vec4 GreyLight_Dark = colorFromTangoHex(0xbdbdbd);
const vec4 GreyLight_Middle = colorFromTangoHex(0xd7d7d7);
const vec4 GreyLight_Light = colorFromTangoHex(0xeeeeee);
const vec4 GreyDark_Dark = colorFromTangoHex(0x363636);
const vec4 GreyDark_Middle = colorFromTangoHex(0x575757);
const vec4 GreyDark_Light = colorFromTangoHex(0x8a8a8a);


const vec4 Turquoise_Dark = colorFromTangoHex(0x336c51);
const vec4 Turquoise_Middle = colorFromTangoHex(0x3e7969);
const vec4 Turquoise_Light = colorFromTangoHex(0x7cbb8d);


} // namespace Tango
} // namespace Base
