#ifndef BASE_SETVALUEINLIFETIME_H
#define BASE_SETVALUEINLIFETIME_H

#include "output.h"

namespace Base {

template<typename T_value>
class SetValueInLifetime : public noncopyable
{
public:
  T_value* variable;
  T_value oldValue;

public:
  SetValueInLifetime(const InOutput<T_value>& variable, T_value temporaryValue)
    : variable(&variable.value),
      oldValue(variable)
  {
    *this->variable = temporaryValue;
  }

  ~SetValueInLifetime()
  {
    *this->variable = oldValue;
  }
};

} // namespace Base

#endif // BASE_SETVALUEINLIFETIME_H
