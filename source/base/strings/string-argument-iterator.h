#include <base/strings/string.h>

namespace Base {


class StringArgumentIterator
{
private:
  const String& str;
  String::const_iterator iter;

public:
  String::const_iterator beginOfContent;
  String::const_iterator endOfContent;

  StringArgumentIterator(const String& str)
    : str(str),
      iter(str.begin()),
      beginOfContent(iter),
      endOfContent(iter)
  {
  }

  String replaceAll(const std::function<bool()>& p, const std::function<String()>& f)
  {
    String result;
    String::const_iterator beginBlock = str.begin();

    while(hasNextPercent())
    {
      result += String(beginBlock, iter);
      beginBlock = endOfContent;

      if(p())
        result += f();
      else
        result += String(iter, endOfContent);
    }

    result += String(beginBlock, str.end());

    return result;
  }

  String value() const
  {
    return String(beginOfContent, endOfContent);
  }

  size_t valueAsNumber() const
  {
    if(!isNumber())
      return String::npos;

    if(*beginOfContent == '[')
    {
      String::const_iterator b = beginOfContent;
      ++b;
      String::const_iterator e = endOfContent;
      ++e;
      String content = String(b, e);

      assert(!content.empty());

      return parseString<size_t>(content);
    }else
      return static_cast<size_t>(*beginOfContent) - static_cast<size_t>('0');
  }

  bool isNumber() const
  {
    return isdigit(*beginOfContent) || *beginOfContent=='[';
  }

  bool isPercent() const
  {
    return *beginOfContent == '%';
  }

  bool hasNextPercent()
  {
    iter = endOfContent;

    while(iter != str.end() && *iter!='%')
      ++iter;

    if(iter == str.end())
      return false;


    beginOfContent = iter;
    ++beginOfContent;
    endOfContent = beginOfContent;

    if(endOfContent==str.end())
      return false;

    if(*endOfContent == '%' || isDigit(*endOfContent))
      ++endOfContent;

    if(*endOfContent == '[')
    {
      ++endOfContent;

      bool valid = false;
      bool hadAnyDigits = false;

      while(endOfContent!=str.end())
      {
        if(*endOfContent == ']')
        {
          valid = hadAnyDigits;
          ++endOfContent;
          break;
        }
        if(!isDigit(*endOfContent))
          break;
        else
          hadAnyDigits = true;

        ++endOfContent;
      }

      if(!valid)
        endOfContent = beginOfContent;

      return valid;
    }

    return true;
  }

private:
  static bool isDigit(String::value_type x)
  {
    return x>='0' && x<='9';
  }
};


} // namespace Base

