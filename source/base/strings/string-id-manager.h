#ifndef BASE_STRINGIDMANAGER_H
#define BASE_STRINGIDMANAGER_H

#include <base/singleton.h>
#include <base/strings/string.h>

namespace Base {


enum StringId : int
{
  STRING_EMPTY = 0
};


class StringIdManager final : public BlackboxSingleton<StringIdManager>
{
public:
  typedef std::shared_ptr<StringIdManager> Ptr;

private:
  QHash<QString, StringId> stringIds;

private:
  friend class BlackboxSingleton<StringIdManager>;
  StringIdManager();

public:
  static StringId idFromString(const QString& string);
  static StringId idFromString(const String& string);
  static StringId idFromString(const std::string& string);
  static StringId idFromString(const char* string);

private:
  static StringIdManager& singleton();
};


} // namespace Base

#endif // BASE_STRINGIDMANAGER_H
