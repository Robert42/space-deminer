#include "unique-name-generator.h"

namespace Base {


TEST(base_UniqueNameGenerator, markNameAsUsed)
{
  UniqueNameGenerator nameGenerator;

  EXPECT_FALSE(nameGenerator.isNameAlreadyUsed("a"));

  nameGenerator.markNameAsUsed("a");

  EXPECT_TRUE(nameGenerator.isNameAlreadyUsed("a"));
}


TEST(base_UniqueNameGenerator, getUniqeName)
{
  UniqueNameGenerator nameGenerator;

  EXPECT_EQ("a", nameGenerator.generateUniqeName("a"));
  EXPECT_EQ("a_1", nameGenerator.generateUniqeName("a"));
  EXPECT_EQ("a_2", nameGenerator.generateUniqeName("a"));
}


} // namespace Base
