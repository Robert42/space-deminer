#include "unique-name-generator.h"

namespace Base {

UniqueNameGenerator::UniqueNameGenerator(StringType::value_type seperator)
  : seperator(seperator)
{
}


bool UniqueNameGenerator::isNameAlreadyUsed(const StringType& name) const
{
  return this->alreadyUsedNames.find(name) != this->alreadyUsedNames.end();
}


void UniqueNameGenerator::markNameAsUsed(const StringType& name)
{
  this->alreadyUsedNames.insert(name);
}


UniqueNameGenerator::StringType UniqueNameGenerator::generateUniqeName(const StringType& name)
{
  StringType n = name;

  if(isNameAlreadyUsed(n))
  {
    int i=1;

    while(isNameAlreadyUsed(n = name+seperator+std::to_string(i)))
    {
      assert(i < std::numeric_limits<int>::max());
      ++i;
    }
  }

  markNameAsUsed(n);

  return n;
}


} // namespace Base
