#ifndef _BASE_STRINGS_NAMES_H_
#define _BASE_STRINGS_NAMES_H_

#include <dependencies.h>
#include <base/strings/string.h>

namespace Base {


/** Returns, whether the given text is a valid application name.
 *
 * @param text
 *
 * @return true, if text fits the regex `^[_a-zA-Z][-_.a-zA-Z0-9]*$`
 */
bool isValidApplicationName(const String& text);


/** Returns, whether the given text is a valid c-style variable name.
 *
 * @param text
 *
 * @return true, if text fits the regex `^[_a-zA-Z][_a-zA-Z0-9]*$` Also the following names are forbidden:
 * - `if`
 * - `for`
 * - `while`
 * - `class`
 * - `struct`
 * - `void`
 * - `bool`
 * - `int`
 * - `float`
 */
bool isValidCVariableName(const String& text);


}

#endif
