#include <dependencies.h>
#include <base/strings/string.h>
#include <base/strings/parse.h>
#include <base/runtime-assertion.h>
#include <base/io/log.h>

#include "string-argument-iterator.h"


namespace Base {

#ifdef USING_GTEST

#define validate_string(str, source, message, isValidString) \
  if(!String::isValidString(str)) \
    throw InvalidStringException(message, source, __FILE__, __LINE__);

#define validate_parameter_string(parameter, source) validate_string(parameter, source, "Invalid string given as parameter " #parameter " to the " source, isValidString)
#define validate_result_string(result, source) validate_string(result, source, "Invalid string generated from " #source " as result " #result, isValidString)
#define validate_utf8(str, source) validate_string(str, source, "Invalid utf8-string " #str " in " source, isValidUtf8)

#else

#define validate_string(str, source, message)
#define validate_parameter_string(parameter, source)
#define validate_result_string(result, source)
#define validate_utf8(str, message)

#endif

const String::size_type String::npos = std::numeric_limits<String::size_type>::max();

String::String()
{
}


String::String(const char* c_str_utf8)
{
  *this = fromUtf8(c_str_utf8);
}

String& String::operator=(const char* c_str_utf8)
{
  return *this = fromUtf8(c_str_utf8);
}


String::String(const String& str)
{
  *this = str;
}


String String::fromPath(const IO::Path& path)
{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
  return fromUtf16(path.wstring());
#else
  return fromUtf8(path.string());
#endif
}


String String::fromOgreString(const Ogre::String& str)
{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
  return fromAnsi(str);
#else
  return fromUtf8(str);
#endif
}


String String::fromCeguiString(const CEGUI::String& str)
{
  return String(str.ptr(), str.ptr()+str.length());
}


String String::fromAnsi(const std::basic_string<char>& str_ansi)
{
  validate_parameter_string(str_ansi, "String::fromAnsi");

  String result;
  result.raw.resize(str_ansi.length());

  size_t l = sf::Utf32::fromAnsi(str_ansi.begin(),
                                 str_ansi.end(),
                                 result.begin()) - result.begin();

  result.raw.resize(l);
  validate_result_string(result, "String::fromAnsi");
  return result;
}



String String::fromUtf32(const std::basic_string<char32_t>& str_utf32)
{
  validate_parameter_string(str_utf32, "String::fromUtf32");

  String str;
  str.raw = str_utf32;

  validate_result_string(str, "String::fromUtf32");
  return str;
}


String String::fromUtf8(const std::basic_string<char>& str_utf8)
{
  validate_utf8(str_utf8, "String::fromUtf8");

  String result;
  result.raw.resize(str_utf8.length());

  size_t l = sf::Utf8::toUtf32(str_utf8.begin(),
                               str_utf8.end(),
                               result.raw.begin()) - result.raw.begin();

  result.raw.resize(l);
  validate_result_string(result, "String::fromUtf8");
  return result;
}



String String::fromUtf16(const std::basic_string<wchar_t>& str_utf16)
{
  String result;
  result.raw.resize(str_utf16.length());

  size_t l = sf::Utf16::toUtf32(str_utf16.begin(),
                                str_utf16.end(),
                                result.raw.begin()) - result.raw.begin();

  result.raw.resize(l);

  validate_result_string(result, "String::fromUtf16");
  return result;
}



String& String::operator=(const String& str)
{
  this->raw = str.raw;

  return *this;
}


String::size_type String::length() const
{
  return this->raw.length();
}



void String::swap(String& other)
{
  this->raw.swap(other.raw);
}



void String::reserve(size_type size)
{
  this->raw.reserve(size);
}



Ogre::String String::toOgreString() const
{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
  return toAnsiString();
#else
  return toUtf8String();
#endif
}


CEGUI::String String::toCeguiString() const
{
  const CEGUI::utf32* data = reinterpret_cast<const CEGUI::utf32*>(this->raw.data());

  CEGUI::String::const_iterator begin(data);
  CEGUI::String::const_iterator end(data + this->length());

  return CEGUI::String(begin, end);
}


const std::basic_string<char32_t>& String::toUtf32String() const
{
  validate_result_string(raw, "String::toUtf32String");
  return raw;
}


std::basic_string<char> String::toUtf8String() const
{
  validate_parameter_string(*this, "String::toUtf8String");

  std::string utf8;
  utf8.resize(calcNumberUtf8Bytes(this->raw));

  size_t l = sf::Utf32::toUtf8(this->raw.begin(),
                               this->raw.end(),
                               utf8.begin()) - utf8.begin();

  utf8.resize(l);


  validate_utf8(utf8, "String::toUtf8String");

  return utf8;
}



std::basic_string<char> String::toAnsiString(char replacement) const
{
  validate_parameter_string(*this, "String::toAnsiString");

  std::basic_string<char> ansi;
  ansi.resize(length());

  size_t l = sf::Utf32::toAnsi(raw.begin(),
                               raw.end(),
                               ansi.begin(),
                               replacement) - ansi.begin();

  ansi.resize(l);
  return ansi;
}



std::basic_string<wchar_t> String::toUtf16String() const
{
  validate_parameter_string(*this, "String::toUtf16String");

  std::basic_string<wchar_t> utf16;
  utf16.resize(length()*2);

  size_t l = sf::Utf32::toUtf16(raw.begin(),
                                raw.end(),
                                utf16.begin()) - utf16.begin();

  utf16.resize(l);
  return utf16.data();
}



IO::Path String::toPath() const
{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
  return IO::Path(toUtf16String());
#else
  return IO::Path(toUtf8String());
#endif
}



String String::fromQString(const QString& qstring)
{
  QVector<uint> ucs4 = qstring.toUcs4();

  return String(ucs4.begin(), ucs4.end());
}



QString String::toQString() const
{
  return QString::fromUcs4(reinterpret_cast<const uint32*>(this->toUtf32String().data()),
                           this->length());
}



void String::toStream(const Output<std::basic_stringstream<char> >& stream) const
{
  validate_parameter_string(*this, "String::toStream");

  stream.value.str(this->toAnsiString());
}



String::operator IO::Path() const
{
  return this->toPath();
}


String::size_type String::calcNumberUtf8Bytes(const raw_type& string)
{
  size_type size = 0;

  for(value_type value : string)
  {
    size += calcNumberUtf8Bytes(value);
  }

  return size;
}


int String::calcNumberUtf8Bytes(char32_t value)
{
  runtimeAssert(value >= 0 && value <= 0x7fffffff, "String::calcNumberUtf8Bytes, value is not within the range of encodable utf8 values.");

  if(value <= 0x7f)
    return 1;
  else if(value>=0x80 && value<=0x7ff)
    return 2;
  else if(value>=0x800 && value<=0xffff)
    return 3;
  else if(value>=0x10000 && value<=0x1fffff)
    return 4;
  else if(value>=0x200000 && value<=0x3ffffff)
    return 5;
  else if(value>=0x4000000 && value<=0x7fffffff)
    return 6;

  return 0;
}


bool String::is_string_empty(const String& string)
{
  return string.empty();
}


bool String::isValidUtf8(const std::string& utf8, bool strict, value_type maxAllowedValue)
{
  std::string::const_iterator iter = utf8.begin();

  while(iter != utf8.end())
  {
    std::string::const_iterator prev_iter = iter;

    sf::Uint32 value;
    iter = sf::Utf8::decode(iter, utf8.end(), value);

    int nBytes = iter-prev_iter;

    if(strict)
    {
      if(value > static_cast<sf::Uint32>(maxAllowedValue))
        return false;

      if(nBytes != calcNumberUtf8Bytes(value))
        return false;

      char curr_char = *prev_iter;
      switch(nBytes)
      {
      case 1:
        if((curr_char & 0x80) != 0)
          return false;
        break;
      case 2:
        if((curr_char & 0xe0) != 0xc0)
          return false;
        break;
      case 3:
        if((curr_char & 0xf0) != 0xe0)
          return false;
        break;
      case 4:
        if((curr_char & 0xf8) != 0xf0)
          return false;
        break;
      case 5:
        if((curr_char & 0xfc) != 0xf8)
          return false;
        break;
      case 6:
        if((curr_char & 0xfe) != 0xfc)
          return false;
        break;
      default:
        return false;
      }
      ++prev_iter;

      while(prev_iter != iter)
      {
        curr_char = *prev_iter;

        if((0xc0 & curr_char) != 0x80)
          return false;

        ++prev_iter;
      }
    }

    if(!isValidCharacter(value))
    {
      if(value>0x10ffff && value <= static_cast<sf::Uint32>(maxAllowedValue))
      {
      }else
      {
        return false;
      }
    }
  }

  return iter==utf8.end();
}


String String::subString(size_type begin, size_type length) const
{
  if(begin >= this->length())
    return String();

  length = min(length, this->length()-begin);

  if(length == 0)
    return String();

  return fromUtf32(this->raw.substr(begin, length));
}


void String::convertToLowerCase()
{
  for(value_type& c : raw)
    c = std::tolower(c);
}


void String::convertToUpperCase()
{
  for(value_type& c : raw)
    c = std::toupper(c);
}



String String::lowerCase() const
{
  String result = *this;

  result.convertToLowerCase();

  return result;
}


String String::upperCase() const
{
  String result = *this;

  result.convertToUpperCase();

  return result;
}


String String::trim(size_type numberCharactersFromLeft, size_type numberCharactersFromRight) const
{
  size_type length = this->length();

  return this->subString(numberCharactersFromLeft, length - min(numberCharactersFromLeft + numberCharactersFromRight,
                                                                length));
}


String String::trimFromLeft(size_type numberCharactersToTrim) const
{
  return this->subString(numberCharactersToTrim);
}


String String::trimFromRight(size_type numberCharactersToTrim) const
{
  size_type length = this->length();

  return this->subString(0,
                         length - min(numberCharactersToTrim,
                                      length));
}


String String::trimAllFromLeftBut(size_type numberCharactersToLeave) const
{
  size_type length = this->length();

  return this->subString(length - min(numberCharactersToLeave,
                                      length));
}


String String::trimAllFromRightBut(size_type numberCharactersToLeave) const
{
  return this->subString(0, numberCharactersToLeave);
}


String String::trim(const std::function<bool(value_type)>& predicate) const
{
  const_iterator begin = this->begin();
  const_iterator end = this->end();

  while(end != begin)
  {
    const_iterator prev = end;
    --prev;

    if(!predicate(*prev))
      break;

    end = prev;
  }

  while(end!=begin && predicate(*begin))
  {
    ++begin;
  }

  return String(begin, end);
}

String String::trimFromRight(const std::function<bool(value_type)>& predicate) const
{
  const_iterator end = this->end();

  while(end != begin())
  {
    const_iterator prev = end;
    --prev;

    if(!predicate(*prev))
      break;

    end = prev;
  }

  return String(begin(), end);
}

String String::trimFromLeft(const std::function<bool(value_type)>& predicate) const
{
  const_iterator begin = this->begin();

  while(end()!=begin && predicate(*begin))
  {
    ++begin;
  }

  return String(begin, end());
}


String String::trimWhitespace() const
{
  return trim(isWhitespace);
}


String String::trimWhitespaceFromLeft() const
{
  return trimFromLeft(isWhitespace);
}


String String::trimWhitespaceFromRight() const
{
  return trimFromRight(isWhitespace);
}


std::list<String> String::split(value_type separator, bool ignoreEmptySubStrings) const
{
  std::list<String> substrings;

  String::const_iterator current_substring = this->begin();
  String::const_iterator iter = this->begin();

  while(iter != this->end())
  {
    if(*iter == separator)
    {
      substrings.push_back(String(current_substring, iter));
      ++iter;
      current_substring = iter;
    }else
    {
      ++iter;
    }
  }

  substrings.push_back(String(current_substring, this->end()));

  if(ignoreEmptySubStrings)
    substrings.remove_if(is_string_empty);

  return substrings;
}

String String::append(value_type separator, const String& attachment) const
{
  if(this->empty())
    return attachment;
  else
    return String::compose("%0%1%2", *this, String::fromUnivodeValue(separator), attachment);
}


bool String::isWhitespace(value_type character)
{
  return character==' ' || character=='\n' || character=='\t' || std::isspace(character);
}



bool String::empty() const
{
  return this->raw.empty();
}


void String::clear()
{
  this->raw.clear();
}


bool String::containsCharacter(value_type character) const
{
  return this->raw.find(character) != raw_type::npos;
}


bool String::containsSubString(const String& substring) const
{
  return this->find(substring) != raw_type::npos;
}


size_t String::find(const String& substring) const
{
  return this->raw.find(substring.raw);
}


size_t String::rfind(const String& substring) const
{
  return this->raw.rfind(substring.raw);
}


void String::replace(value_type replace, value_type with)
{
  return std::replace(this->raw.begin(), this->raw.end(), replace, with);
}


bool String::matchesRegex(const String& regexPattern, bool partial) const
{
  QRegularExpression regex(regexPattern.toQString());

  return matchesRegex(regex, partial);
}


bool String::matchesRegex(const QRegularExpression& regex, bool partial) const
{
  if(!regex.isValid())
    IO::Log::logWarning("Invalid regular expression: `%0`", regex.pattern());

  if(regex.match(this->toQString()).hasMatch())
    return true;

  if(partial)
    return regex.match(this->toQString()).hasPartialMatch();

  return false;
}


void String::push_back(char32_t character)
{
  this->raw.push_back(character);
}


String String::noMoreArguments() const
{
  StringArgumentIterator iter(*this);
  String percent = String::fromUnivodeValue('%');
  return iter.replaceAll([&iter](){return iter.isPercent();},
  [&percent](){return percent;});
}

String String::withArgument(const String& argument, size_type i) const
{
  size_type smallestIndex = i;

  if(smallestIndex == npos)
  {
    StringArgumentIterator iter(*this);

    while(iter.hasNextPercent())
    {
      if(iter.isNumber())
        smallestIndex = min(smallestIndex,
                            iter.valueAsNumber());
    }
  }

  {
    StringArgumentIterator iter(*this);
    return iter.replaceAll([&iter, smallestIndex](){return iter.isNumber() && iter.valueAsNumber() == smallestIndex;},
                           [&argument](){return argument;});
  }
}


String::iterator String::begin()
{
  return this->raw.begin();
}


String::const_iterator String::begin() const
{
  return this->raw.begin();
}


String::reverse_iterator String::rbegin()
{
  return this->raw.rbegin();
}


String::const_reverse_iterator String::rbegin() const
{
  return this->raw.rbegin();
}


String::iterator String::end()
{
  return this->raw.end();
}


String::const_iterator String::end() const
{
  return this->raw.end();
}


String::reverse_iterator String::rend()
{
  return this->raw.rend();
}


String::const_reverse_iterator String::rend() const
{
  return this->raw.rend();
}


bool operator==(const String& a, const String& b)
{
  return a.raw == b.raw;
}


bool operator!=(const String& a, const String& b)
{
  return a.raw != b.raw;
}


bool operator<(const String& a, const String& b)
{
  return a.raw < b.raw;
}


bool operator>(const String& a, const String& b)
{
  return a.raw > b.raw;
}


bool operator<=(const String& a, const String& b)
{
  return a.raw <= b.raw;
}


bool operator>=(const String& a, const String& b)
{
  return a.raw >= b.raw;
}



String operator+(const String& a, const String& b)
{
  return String::fromUtf32(a.raw + b.raw);
}


String& String::operator+=(const String& str)
{
  this->raw += str.raw;

  return *this;
}


String::value_type& String::operator[](size_type i)
{
  if(i >= this->length())
    throw std::logic_error("**String::operator[]** the given index is too large.");

  return this->raw[i];
}


String::value_type String::operator[](size_type i) const
{
  return this->raw[i];
}


void PrintTo(const String& string, std::ostream* o)
{
  *o << '"' << string << '"';
}


std::ostream& operator<<(std::ostream& o, const String& str)
{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
  return o << str.toAnsiString();
#else
  return o << str.toUtf8String();
#endif
}


std::wostream& operator<<(std::wostream& o, const String& str)
{
  return o << str.toUtf16String();
}


void swap(Base::String& a, Base::String& b)
{
  a.swap(b);
}


InvalidStringException::InvalidStringException(const std::string& message,
                                               const std::string& source,
                                               const char* file,
                                               int line)
  : Ogre::Exception(line, message, source, "InvalidStringException", file, line)
{
}

}
