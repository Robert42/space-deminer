#include <dependencies.h>
#include <base/strings/names.h>

namespace Base {


bool isValidApplicationName(const String& text)
{
  return text.matchesRegex("^[_a-zA-Z][-_.a-zA-Z0-9]*$");
}


bool isValidCVariableName(const String& text)
{
  QSet<String> invalidNames = {"if",
                               "for",
                               "while",
                               "class",
                               "struct",
                               "void",
                               "bool",
                               "int",
                               "float"};

  if(invalidNames.contains(text))
    return false;
  return text.matchesRegex("^[_a-zA-Z][_a-zA-Z0-9]*$");
}



}
