#include "string-id-manager.h"

namespace Base {


StringIdManager::StringIdManager()
{
  stringIds.insert(QString(), STRING_EMPTY);
}


StringId StringIdManager::idFromString(const QString& string)
{
  QHash<QString, StringId>& stringIds = singleton().stringIds;

  QHash<QString, StringId>::iterator i = stringIds.find(string);

  if(i != stringIds.end())
    return i.value();

  assert(stringIds.size() < std::numeric_limits<int>::max());

  StringId id = static_cast<StringId>(stringIds.size());
  stringIds.insert(string, id);

  return id;
}

StringId StringIdManager::idFromString(const String& string)
{
  return idFromString(string.toQString());
}

StringId StringIdManager::idFromString(const std::string& string)
{
  return idFromString(QString::fromStdString(string));
}

StringId StringIdManager::idFromString(const char* string)
{
  return idFromString(QString(string));
}

StringIdManager& StringIdManager::singleton()
{
  static StringIdManager _singleton;

  return _singleton;
}


} // namespace Base
