#ifndef BASE_UNIQUENAMEGENERATOR_H
#define BASE_UNIQUENAMEGENERATOR_H

#include <base/strings/string.h>

namespace Base {

class UniqueNameGenerator
{
public:
  typedef std::string StringType;

public:
  const StringType::value_type seperator;
  std::set<StringType> alreadyUsedNames;

public:
  UniqueNameGenerator(StringType::value_type seperator='_');

  bool isNameAlreadyUsed(const StringType& name) const;

  void markNameAsUsed(const StringType& name);
  StringType generateUniqeName(const StringType& name);
};

} // namespace Base

#endif // BASE_UNIQUENAMEGENERATOR_H
