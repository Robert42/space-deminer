#ifndef _BASE_STRINGS_STRING_INL_
#define _BASE_STRINGS_STRING_INL_

#include "./string.h"

namespace Base {

template<typename Iterator>
String::String(const Iterator& begin, const Iterator& end)
  : raw(begin, end)
{
}


template<typename T>
T& String::parse(const Output<T>& var) const
{
  std::basic_stringstream<char> stream;

  this->toStream(out(stream));

  stream >> var.value;

  return var.value;
}

template<typename T>
T String::parse() const
{
  T var;

  return parse<T>(out(var));
}


template<typename T_string>
void String::toCustomUtf32String(T_string& destString) const
{
  static_assert(sizeof(typename T_string::value_type) == 4, "The character size of an utf-32 string must be 4 byte");

  const size_t length = this->length();

  destString.resize(length);

  for(size_t i=0; i<length; ++i)
    destString[i] = raw[i];
}


template<typename TIterator>
bool String::isValidString(const TIterator& begin, const TIterator& end)
{
  for(TIterator iter = begin; iter!=end; ++iter)
    if(!isValidCharacter(*iter))
      return false;

  return true;
}


template<typename TString>
bool String::isValidString(const TString& string)
{
  return isValidString(string.begin(), string.end());
}


template<typename TCharacter>
bool String::isValidCharacter(TCharacter character)
{
  char32_t c = static_cast<char32_t>(character);

  if(c <= 0)
    return false;
  if(c > 0x10ffff)
    return false;
  if(c>=0xd800 && c<=0xdfff)
    return false;
  return true;
}


}


#endif
