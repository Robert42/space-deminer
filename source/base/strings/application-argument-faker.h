#ifndef _BASE_STRINGS_APPLICATION_ARGUMENT_FAKER_H_
#define _BASE_STRINGS_APPLICATION_ARGUMENT_FAKER_H_

#include <dependencies.h>

namespace Base {


/** A helper class allowing to create easily an argument list wjich can be used to give arguments to
 * for example the google testframework or the qt framework.
 *
 * \note the returned pointer are as long valid as long the instance is valid.
 */
class ApplicationArgumentFaker
{
private:
  std::vector<std::string> arguments;
  std::vector<char*> arguments_c_str;
  int _argc;

public:
  ApplicationArgumentFaker(const std::initializer_list<std::string>& arguments);

  int* argc();
  char** argv();
};


} // namespace Base

#endif
