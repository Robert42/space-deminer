#ifndef _BASE_STRINGS_PARSE_H_
#define _BASE_STRINGS_PARSE_H_

#include "./string.h"

namespace Base {

template<typename T>
T parseString(const String& str)
{
  std::stringstream stream;

  str.toStream(out(stream));

  T value;

  stream >> value;

  return value;
}

}

#endif
