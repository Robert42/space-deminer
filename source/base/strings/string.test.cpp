#include <dependencies.h>
#include <base/strings/string.h>
#include <base/strings/parse.h>
#include <base/runtime-assertion.h>
#include <base/io/log.h>

#include "string-argument-iterator.h"


namespace Base {

#ifdef USING_GTEST

#define validate_string(str, source, message, isValidString) \
  if(!String::isValidString(str)) \
    throw InvalidStringException(message, source, __FILE__, __LINE__);

#define validate_parameter_string(parameter, source) validate_string(parameter, source, "Invalid string given as parameter " #parameter " to the " source, isValidString)
#define validate_result_string(result, source) validate_string(result, source, "Invalid string generated from " #source " as result " #result, isValidString)
#define validate_utf8(str, source) validate_string(str, source, "Invalid utf8-string " #str " in " source, isValidUtf8)

#else

#define validate_string(str, source, message)
#define validate_parameter_string(parameter, source)
#define validate_result_string(result, source)
#define validate_utf8(str, message)

#endif

TEST(base_String, fromAnsi)
{
  std::string ansi = "abcdefg";
  ansi[3] = 0;

  EXPECT_THROW(String::fromAnsi(ansi), InvalidStringException);
}



TEST(base_String, setting_from_utf8_empty)
{
  std::basic_string<char> emptyUtf8;

  String str = String::fromUtf8(emptyUtf8);

  EXPECT_EQ(static_cast<String::size_type>(0),
            str.length());
  EXPECT_EQ("",
            str.toUtf8String());
}

TEST(base_String, setting_from_utf8_invalid)
{
  std::basic_string<char> invalidUtf8 = "abc\xff""def";

  EXPECT_THROW(String::fromUtf8(invalidUtf8), InvalidStringException);
}

TEST(base_String, setting_from_utf8_with_null_chracter)
{
  std::basic_string<char> invalidUtf8;
  invalidUtf8.resize(4);
  invalidUtf8[0] = 'a';
  invalidUtf8[1] = 'b';
  invalidUtf8[2] = 0;
  invalidUtf8[3] = 'c';

  EXPECT_THROW(String::fromUtf8(invalidUtf8), InvalidStringException);
}


TEST(base_String, setting_from_utf16_empty)
{
  std::basic_string<wchar_t> emptyUtf16;
  String string = String::fromUtf16(emptyUtf16);

  EXPECT_EQ(static_cast<String::size_type>(0),
            string.length());
  EXPECT_EQ("",
            string.toUtf8String());
}

TEST(base_String, setting_from_utf16_simple)
{
  std::basic_string<wchar_t> simpleUtf16 = L"\x03C0";
  String string = String::fromUtf16(simpleUtf16);

  EXPECT_EQ(static_cast<String::size_type>(1),
            string.length());
  EXPECT_EQ("\317\200",
            string.toUtf8String());
}

TEST(base_String, setting_from_utf16_bondary_values)
{
  std::basic_string<wchar_t> invalidUtf16;
  invalidUtf16.resize(4);
  invalidUtf16[0] = 'a';
  invalidUtf16[1] = 'b';
  invalidUtf16[2] = 0xffff;
  invalidUtf16[3] = 'c';

  String str = String::fromUtf16(invalidUtf16);

  EXPECT_EQ(static_cast<String::size_type>(4),
            str.length());
  EXPECT_EQ("ab\357\277\277c",
            str.toUtf8String());
}


TEST(base_String, setting_from_utf16_length_is_set_correctly_with_invalid_characters)
{
  std::basic_string<wchar_t> invalidUtf16;
  invalidUtf16.resize(4);
  invalidUtf16[0] = 'a';
  invalidUtf16[1] = 'b';
  invalidUtf16[2] = 0xff;
  invalidUtf16[3] = 'c';

  String str = String::fromUtf16(invalidUtf16);

  EXPECT_EQ(static_cast<String::size_type>(4),
            str.length());
  EXPECT_EQ("ab\303\277c",
            str.toUtf8String());
}

TEST(base_String, setting_from_utf16_length_is_set_correctly_with_null_characters)
{
  std::basic_string<wchar_t> invalidUtf16;
  invalidUtf16.resize(4);
  invalidUtf16[0] = 'a';
  invalidUtf16[1] = 'b';
  invalidUtf16[2] = 0;
  invalidUtf16[3] = 'c';

  EXPECT_THROW(String::fromUtf16(invalidUtf16), InvalidStringException);
}


TEST(base_String, swap_string)
{
  String a = "Abcdef";
  String b = "B";

  a.swap(b);

  EXPECT_EQ(static_cast<String::size_type>(6),
            b.length());
  EXPECT_EQ("Abcdef",
            b.toUtf8String());
  EXPECT_EQ(static_cast<String::size_type>(1),
            a.length());
  EXPECT_EQ("B",
            a.toUtf8String());
}


TEST(base_String, toUtf32String_crash_test)
{
  String("").toUtf32String();
}


TEST(base_String, toUtf8String_simple)
{
  String simple = "abc";

  EXPECT_EQ("abc", simple.toUtf8String());

  simple = "abcÄßµd";

  EXPECT_EQ("abcÄßµd", simple.toUtf8String());
}

TEST(base_String, toUtf8String_empty)
{
  String empty;

  EXPECT_EQ("", empty.toUtf8String());
}


TEST(base_String, parse)
{
  int theAnswer;
  float pi;

  theAnswer = String("42").parse<int>();
  pi = String("3.1415").parse<float>();

  EXPECT_EQ(42, theAnswer);
  EXPECT_NEAR(3.1415, pi, 0.00001);
}

TEST(base_String, parse_with_out)
{
  int theAnswer;
  float pi;

  String("42").parse(out(theAnswer));
  String("3.1415").parse(out(pi));

  EXPECT_EQ(42, theAnswer);
  EXPECT_NEAR(3.1415, pi, 0.00001);
}

inline std::string generateUtfCode(uint32 value, int nBytes)
{
  if(nBytes == 1)
    return std::string(1, static_cast<uint8>(0x7f&value));

  std::string result;
  uint8 byte;

  int nBytesLeft = nBytes;

  while(nBytesLeft > 1)
  {
    byte = 0x80;

    byte |= 0x3f & value;

    value >>= 6;

    result = static_cast<char>(byte) + result;
    --nBytesLeft;
  }

  uint8 firstBits = (0xfe << (7-nBytes));
  uint8 valueBits = (0xff << (7-nBytes));

  byte = (value & ~valueBits);
  assert(byte == value);
  byte = firstBits | static_cast<char>(byte);
  result = static_cast<char>(byte) + result;

  return result;
}


TEST(base_String_test, generateUtfCode)
{
  EXPECT_EQ("\x12",
            generateUtfCode(0x12, 1));
  EXPECT_EQ("\x7f",
            generateUtfCode(0x7f, 1));
  EXPECT_EQ("\x01",
            generateUtfCode(0x81, 1));
  EXPECT_EQ("\xc1\xbf",
            generateUtfCode(0x7f, 2));
}

TEST(base_String, isValidUtf8_null_character)
{
  std::string invalid_string;
  invalid_string.resize(7);
  invalid_string[0] = 'a';
  invalid_string[1] = 'b';
  invalid_string[2] = 'c';
  invalid_string[3] = 0;
  invalid_string[4] = 'd';
  invalid_string[5] = 'e';
  invalid_string[6] = 'f';

  EXPECT_FALSE(String::isValidUtf8(invalid_string, false));

  invalid_string.resize(4);
  invalid_string[0] = 'a';
  invalid_string[1] = 'b';
  invalid_string[2] = 'c';
  invalid_string[3] = 0;

  EXPECT_FALSE(String::isValidUtf8(invalid_string, false));

  invalid_string.resize(1);
  invalid_string[0] = 0;

  EXPECT_FALSE(String::isValidUtf8(invalid_string, false));
  invalid_string[0] = 'a';

  EXPECT_TRUE(String::isValidUtf8(invalid_string, false));

  EXPECT_FALSE(String::isValidUtf8("abc\xc0\x80""defg", false));

  EXPECT_TRUE(String::isValidUtf8("abc\0defg", false));
  EXPECT_TRUE(String::isValidUtf8("abc\0", false));
  EXPECT_TRUE(String::isValidUtf8("\0", false));
  EXPECT_TRUE(String::isValidUtf8("abcdefg", false));
  EXPECT_TRUE(String::isValidUtf8("abc", false));
  EXPECT_TRUE(String::isValidUtf8("", false));
}

TEST(base_String, isValidUtf8_strict_too_less_bytes)
{
  EXPECT_FALSE(String::isValidUtf8("abc\x80""defg", true));
  EXPECT_TRUE(String::isValidUtf8("abc\x80""defg", false));
}

TEST(base_String, isValidUtf8_strict_more_bytes_used_than_needed_for_small_value)
{
  EXPECT_TRUE(String::isValidUtf8(generateUtfCode(0x7f, 1)));
  EXPECT_FALSE(String::isValidUtf8(generateUtfCode(0x7f, 2)));
  EXPECT_TRUE(String::isValidUtf8(generateUtfCode(0x80, 2)));

  EXPECT_TRUE(String::isValidUtf8(generateUtfCode(0x7ff, 2)));
  EXPECT_FALSE(String::isValidUtf8(generateUtfCode(0x7ff, 3)));
  EXPECT_TRUE(String::isValidUtf8(generateUtfCode(0x800, 3)));

  EXPECT_TRUE(String::isValidUtf8(generateUtfCode(0xffff, 3)));
  EXPECT_FALSE(String::isValidUtf8(generateUtfCode(0xffff, 4)));
  EXPECT_TRUE(String::isValidUtf8(generateUtfCode(0x10000, 4)));

  EXPECT_TRUE(String::isValidUtf8(generateUtfCode(0x1fffff, 4), true, 0x7fffffff));
  EXPECT_FALSE(String::isValidUtf8(generateUtfCode(0x1fffff, 5), true, 0x7fffffff));
  EXPECT_TRUE(String::isValidUtf8(generateUtfCode(0x200000, 5), true, 0x7fffffff));

  EXPECT_TRUE(String::isValidUtf8(generateUtfCode(0x3ffffff, 5), true, 0x7fffffff));
  EXPECT_FALSE(String::isValidUtf8(generateUtfCode(0x3ffffff, 6), true, 0x7fffffff));
  EXPECT_TRUE(String::isValidUtf8(generateUtfCode(0x4000000, 6), true, 0x7fffffff));

  EXPECT_TRUE(String::isValidUtf8(generateUtfCode(0x7fffffff, 6), true, 0x7fffffff));
}

TEST(base_String, isValidUtf8_too_large)
{
  EXPECT_FALSE(String::isValidUtf8(generateUtfCode(0x80000000, 7)));
  EXPECT_FALSE(String::isValidUtf8("\xfe\xbf\xbf\xbf\xbf\xbf\xbf", true, 0x7fffffff));
  EXPECT_FALSE(String::isValidUtf8("\xff\xbf\xbf\xbf\xbf\xbf\xbf\xbf", true, 0x7fffffff));
}

TEST(base_String, isValidUtf8_incomplete_character)
{
  EXPECT_FALSE(String::isValidUtf8("ab\360\221\200", false));
  EXPECT_TRUE(String::isValidUtf8("ab\360\221\200\200", false));
}


TEST(base_String, subString_basic_usage)
{
  EXPECT_EQ("abcd", String("abcd").subString(0, 4));
  EXPECT_EQ("bcd", String("abcd").subString(1, 3));
  EXPECT_EQ("abc", String("abcd").subString(0, 3));
  EXPECT_EQ("", String("abcd").subString(2, 0));
}

TEST(base_String, subString_special_cases)
{
  EXPECT_EQ("", String("").subString(0, 4));
  EXPECT_EQ("", String("").subString(0, 0));
  EXPECT_EQ("a", String("a").subString(0, 4));
  EXPECT_EQ("", String("a").subString(8, 4));
  EXPECT_EQ("", String("a").subString(1, 4));
}

TEST(base_String, subString_border_vases_for_length)
{
  EXPECT_EQ("abc", String("abc").subString(0, 3));
  EXPECT_EQ("bc", String("abc").subString(1, 2));
  EXPECT_EQ("c", String("abc").subString(2, 1));

  EXPECT_EQ("abc", String("abc").subString(0, 4));
  EXPECT_EQ("bc", String("abc").subString(1, 3));
  EXPECT_EQ("c", String("abc").subString(2, 2));

  EXPECT_EQ("ab", String("abc").subString(0, 2));
  EXPECT_EQ("b", String("abc").subString(1, 1));
  EXPECT_EQ("", String("abc").subString(2, 0));
}


TEST(base_String, getLowerCase)
{
  EXPECT_EQ("a.b", String("a.B").lowerCase());
}


TEST(base_String, getUpperCase)
{
  EXPECT_EQ("A.B", String("a.B").upperCase());
}


TEST(base_String, trim)
{
  EXPECT_EQ("abc", String("abc").trim(0, 0));
  EXPECT_EQ("bc", String("abc").trim(1, 0));
  EXPECT_EQ("c", String("abc").trim(2, 0));
  EXPECT_EQ("", String("abc").trim(3, 0));

  EXPECT_EQ("ab", String("abc").trim(0, 1));
  EXPECT_EQ("b", String("abc").trim(1, 1));
  EXPECT_EQ("", String("abc").trim(2, 1));
  EXPECT_EQ("", String("abc").trim(3, 1));

  EXPECT_EQ("a", String("abc").trim(0, 2));
  EXPECT_EQ("", String("abc").trim(1, 2));
  EXPECT_EQ("", String("abc").trim(2, 2));
  EXPECT_EQ("", String("abc").trim(3, 2));

  EXPECT_EQ("", String("abc").trim(0, 3));
  EXPECT_EQ("", String("abc").trim(1, 3));
  EXPECT_EQ("", String("abc").trim(2, 3));
  EXPECT_EQ("", String("abc").trim(3, 3));
}

TEST(base_String, trim_invalid_parameters)
{
  EXPECT_EQ("", String("abc").trim(4, 0));
  EXPECT_EQ("", String("abc").trim(0, 4));
  EXPECT_EQ("", String("abc").trim(2, 2));
  EXPECT_EQ("", String("").trim(1, 1));
}


TEST(base_String, trimFromLeft)
{
  EXPECT_EQ("abc", String("abc").trimFromLeft(0));
  EXPECT_EQ("bc", String("abc").trimFromLeft(1));
  EXPECT_EQ("c", String("abc").trimFromLeft(2));
  EXPECT_EQ("", String("abc").trimFromLeft(3));
}

TEST(base_String, trimFromLeft_invalid_parameters)
{
  EXPECT_EQ("", String("abc").trimFromLeft(4));
  EXPECT_EQ("", String("").trimFromLeft(4));
}


TEST(base_String, trimFromRight)
{
  EXPECT_EQ("abc", String("abc").trimFromRight(0));
  EXPECT_EQ("ab", String("abc").trimFromRight(1));
  EXPECT_EQ("a", String("abc").trimFromRight(2));
  EXPECT_EQ("", String("abc").trimFromRight(3));
}

TEST(base_String, trimFromRight_invalid_parameters)
{
  EXPECT_EQ("", String("abc").trimFromRight(4));
  EXPECT_EQ("", String("").trimFromRight(4));
}


TEST(base_String, trimAllFromLeftBut)
{
  EXPECT_EQ("", String("abc").trimAllFromLeftBut(0));
  EXPECT_EQ("c", String("abc").trimAllFromLeftBut(1));
  EXPECT_EQ("bc", String("abc").trimAllFromLeftBut(2));
  EXPECT_EQ("abc", String("abc").trimAllFromLeftBut(3));
}

TEST(base_String, trimAllFromLeftBut_invalid_parameters)
{
  EXPECT_EQ("abc", String("abc").trimAllFromLeftBut(4));
  EXPECT_EQ("", String("").trimAllFromLeftBut(4));
}


TEST(base_String, trimAllFromRightBut)
{
  EXPECT_EQ("", String("abc").trimAllFromRightBut(0));
  EXPECT_EQ("a", String("abc").trimAllFromRightBut(1));
  EXPECT_EQ("ab", String("abc").trimAllFromRightBut(2));
  EXPECT_EQ("abc", String("abc").trimAllFromRightBut(3));
}

TEST(base_String, trimAllFromRightBut_invalid_parameters)
{
  EXPECT_EQ("abc", String("abc").trimAllFromRightBut(4));
  EXPECT_EQ("", String("").trimAllFromRightBut(4));
}


TEST(base_String, trimWhitespace)
{
  EXPECT_EQ("abc", String("abc").trimWhitespace());
  EXPECT_EQ("", String("").trimWhitespace());

  EXPECT_EQ("abc", String(" abc").trimWhitespace());
  EXPECT_EQ("abc", String("  abc").trimWhitespace());
  EXPECT_EQ("abc", String("abc  ").trimWhitespace());
  EXPECT_EQ("abc", String("abc ").trimWhitespace());
  EXPECT_EQ("a  b  c", String("  a  b  c  ").trimWhitespace());
  EXPECT_EQ("a  b  c", String(" a  b  c ").trimWhitespace());
  EXPECT_EQ("b", String("    b    ").trimWhitespace());
  EXPECT_EQ("", String(" ").trimWhitespace());
}


TEST(base_String, trimWhitespaceFromLeft)
{
  EXPECT_EQ("abc", String("abc").trimWhitespaceFromLeft());
  EXPECT_EQ("", String("").trimWhitespaceFromLeft());

  EXPECT_EQ("abc", String(" abc").trimWhitespaceFromLeft());
  EXPECT_EQ("abc", String("  abc").trimWhitespaceFromLeft());
  EXPECT_EQ("abc  ", String("abc  ").trimWhitespaceFromLeft());
  EXPECT_EQ("abc ", String("abc ").trimWhitespaceFromLeft());
  EXPECT_EQ("a  b  c  ", String("  a  b  c  ").trimWhitespaceFromLeft());
  EXPECT_EQ("a  b  c ", String(" a  b  c ").trimWhitespaceFromLeft());
  EXPECT_EQ("b    ", String("    b    ").trimWhitespaceFromLeft());
  EXPECT_EQ("", String(" ").trimWhitespaceFromLeft());
}


TEST(base_String, trimWhitespaceFromRight)
{
  EXPECT_EQ("abc", String("abc").trimWhitespaceFromRight());
  EXPECT_EQ("", String("").trimWhitespaceFromRight());

  EXPECT_EQ(" abc", String(" abc").trimWhitespaceFromRight());
  EXPECT_EQ("  abc", String("  abc").trimWhitespaceFromRight());
  EXPECT_EQ("abc", String("abc  ").trimWhitespaceFromRight());
  EXPECT_EQ("abc", String("abc ").trimWhitespaceFromRight());
  EXPECT_EQ("  a  b  c", String("  a  b  c  ").trimWhitespaceFromRight());
  EXPECT_EQ(" a  b  c", String(" a  b  c ").trimWhitespaceFromRight());
  EXPECT_EQ("    b", String("    b    ").trimWhitespaceFromRight());
  EXPECT_EQ("", String(" ").trimWhitespaceFromRight());
}


TEST(base_String, simple_split)
{
  std::list<String> expected;
  expected.push_back("a");
  expected.push_back("b");

  EXPECT_EQ(expected, String("a\nb").split('\n')) << ::testing::PrintToString(expected);
}

TEST(base_String, multiple_split)
{
  std::list<String> expected;
  expected.push_back("a");
  expected.push_back("b");
  expected.push_back("c");
  expected.push_back("d");

  EXPECT_EQ(expected, String("a\nb\nc\nd").split('\n')) << ::testing::PrintToString(expected);
}

TEST(base_String, split_hides_elements)
{
  std::list<String> expected;
  expected.push_back("a");
  expected.push_back("b");
  expected.push_back("c");
  expected.push_back("d");

  EXPECT_EQ(expected, String("\n\n\n\na\nb\n\n\n\nc\nd\n\n\n\n").split('\n')) << ::testing::PrintToString(expected);
}

TEST(base_String, split_empty)
{
  std::list<String> expected_not_empty;
  expected_not_empty.push_back("");

  std::list<String> expected_empty;

  EXPECT_EQ(expected_not_empty, String("").split('\n', false)) << ::testing::PrintToString(expected_not_empty);
  EXPECT_EQ(expected_empty, String("").split('\n')) << ::testing::PrintToString(expected_empty);
}

TEST(base_String, split_seperator_only)
{
  std::list<String> expected_not_empty;
  expected_not_empty.push_back("");
  expected_not_empty.push_back("");

  std::list<String> expected_empty;

  EXPECT_EQ(expected_not_empty, String("\n").split('\n', false)) << ::testing::PrintToString(expected_not_empty);
  EXPECT_EQ(expected_empty, String("\n").split('\n')) << ::testing::PrintToString(expected_empty);
}

TEST(base_String, split_multiple_seperator_only)
{
  std::list<String> expected_not_empty;
  expected_not_empty.push_back("");
  expected_not_empty.push_back("");
  expected_not_empty.push_back("");
  expected_not_empty.push_back("");

  std::list<String> expected_empty;

  EXPECT_EQ(expected_not_empty, String("\n\n\n").split('\n', false)) << ::testing::PrintToString(expected_not_empty);
  EXPECT_EQ(expected_empty, String("\n").split('\n')) << ::testing::PrintToString(expected_empty);
}


TEST(base_String, empty)
{
  EXPECT_TRUE(String().empty());
  EXPECT_FALSE(String("a").empty());
}


TEST(base_String, clear)
{
  String s("a");
  EXPECT_FALSE(s.empty());
  s.clear();
  EXPECT_TRUE(s.empty());
}


TEST(base_String, containsCharacter)
{
  String s("abcde");
  EXPECT_FALSE(s.containsCharacter(' '));
  EXPECT_FALSE(s.containsCharacter('\0'));
  EXPECT_FALSE(s.containsCharacter('A'));
  EXPECT_TRUE(s.containsCharacter('a'));
  EXPECT_TRUE(s.containsCharacter('b'));
  EXPECT_TRUE(s.containsCharacter('c'));
  EXPECT_TRUE(s.containsCharacter('d'));
}


TEST(base_String, containsSubString)
{
  String s("abcde");
  EXPECT_FALSE(s.containsSubString("acde"));
  EXPECT_FALSE(s.containsSubString("cdef"));
  EXPECT_FALSE(s.containsSubString(" abc"));
  EXPECT_TRUE(s.containsSubString("a"));
  EXPECT_TRUE(s.containsSubString("bcd"));
  EXPECT_TRUE(s.containsSubString(""));
  EXPECT_TRUE(s.containsSubString("abcde"));
}


TEST(base_String_compose, empty)
{
  EXPECT_EQ("", String::compose(""));
  EXPECT_EQ("", String::compose("", 42));
}

TEST(base_String_compose, basic)
{
  EXPECT_EQ("%", String::compose("%%"));
  EXPECT_EQ("%%", String::compose("%%%"));
  EXPECT_EQ("42", String::compose("%0", 42));
  EXPECT_EQ("42", String::compose("%1", 42));
  EXPECT_EQ("ab%cdefg", String::compose("ab%cdefg", 42));
  EXPECT_EQ("%0", String::compose("%%0", 42));
  EXPECT_EQ("%1", String::compose("%%1", 42));
  EXPECT_EQ("%%0", String::compose("%%%0"));
  EXPECT_EQ("%42", String::compose("%%%0", 42));
  EXPECT_EQ("%42", String::compose("%%%1", 42));
}

TEST(base_String_compose, string_as_argument)
{
  EXPECT_EQ("abc", String::compose("%0", String("abc")));
  EXPECT_EQ("abc", String::compose("%0", std::string("abc")));
}

TEST(base_String_compose, arguments)
{
  EXPECT_EQ("0, 1, 2, 3, 4, 5, 6, 7, 8, 9",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7, %8, %9", 0, 1, 2, 3, 4, 5, 6, 7, 8, 9));
  EXPECT_EQ("0, 1, 2, 3, 4, 5, 6, 7, 8",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7, %8", 0, 1, 2, 3, 4, 5, 6, 7, 8));
  EXPECT_EQ("0, 1, 2, 3, 4, 5, 6, 7",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7", 0, 1, 2, 3, 4, 5, 6, 7));
  EXPECT_EQ("0, 1, 2, 3, 4, 5, 6",
            String::compose("%0, %1, %2, %3, %4, %5, %6", 0, 1, 2, 3, 4, 5, 6));
  EXPECT_EQ("0, 1, 2, 3, 4, 5",
            String::compose("%0, %1, %2, %3, %4, %5", 0, 1, 2, 3, 4, 5));
  EXPECT_EQ("0, 1, 2, 3, 4",
            String::compose("%0, %1, %2, %3, %4", 0, 1, 2, 3, 4));
  EXPECT_EQ("0, 1, 2, 3",
            String::compose("%0, %1, %2, %3", 0, 1, 2, 3));
  EXPECT_EQ("0, 1, 2",
            String::compose("%0, %1, %2", 0, 1, 2));
  EXPECT_EQ("0, 1",
            String::compose("%0, %1", 0, 1));
  EXPECT_EQ("0",
            String::compose("%0", 0));
}

TEST(base_String_compose, argument_types)
{
  EXPECT_EQ("0, 1, 2, 3, 4, 5, 6, 7, 8, 9.5",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7, %8, %9", 0, 1, 2, 3, 4, 5, 6, 7, 8, 9.5));
  EXPECT_EQ("0, 1, 2, 3, 4, 5, 6, 7, 8",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7, %8", 0, 1, 2, 3, 4, 5, 6, 7, 8));
  EXPECT_EQ("0, 1, 2, 3, 4, 5, 6, 7",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7", 0, 1, 2, 3, 4, 5, 6, 7));
  EXPECT_EQ("0, 1, 2, 3, 4, 5, 6",
            String::compose("%0, %1, %2, %3, %4, %5, %6", 0, 1, 2, 3, 4, 5, 6));
  EXPECT_EQ("0, 1, 2, 3, 4, 5",
            String::compose("%0, %1, %2, %3, %4, %5", 0, 1, 2, 3, 4, 5));
  EXPECT_EQ("0, 1, 2, 3, 4",
            String::compose("%0, %1, %2, %3, %4", 0, 1, 2, 3, 4));
  EXPECT_EQ("0, 1, 2, 3",
            String::compose("%0, %1, %2, %3", 0, 1, 2, 3));
  EXPECT_EQ("0, 1, 2",
            String::compose("%0, %1, %2", 0, 1, 2));
  EXPECT_EQ("0, 1",
            String::compose("%0, %1", 0, 1));
  EXPECT_EQ("0",
            String::compose("%0", 0));

  EXPECT_EQ("0, 1, 2, 3, 4, 5, 6, 7, 8.5, 9",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7, %8, %9", 0, 1, 2, 3, 4, 5, 6, 7, 8.5, 9));
  EXPECT_EQ("0, 1, 2, 3, 4, 5, 6, 7, 8.5",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7, %8", 0, 1, 2, 3, 4, 5, 6, 7, 8.5));
  EXPECT_EQ("0, 1, 2, 3, 4, 5, 6, 7",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7", 0, 1, 2, 3, 4, 5, 6, 7));
  EXPECT_EQ("0, 1, 2, 3, 4, 5, 6",
            String::compose("%0, %1, %2, %3, %4, %5, %6", 0, 1, 2, 3, 4, 5, 6));
  EXPECT_EQ("0, 1, 2, 3, 4, 5",
            String::compose("%0, %1, %2, %3, %4, %5", 0, 1, 2, 3, 4, 5));
  EXPECT_EQ("0, 1, 2, 3, 4",
            String::compose("%0, %1, %2, %3, %4", 0, 1, 2, 3, 4));
  EXPECT_EQ("0, 1, 2, 3",
            String::compose("%0, %1, %2, %3", 0, 1, 2, 3));
  EXPECT_EQ("0, 1, 2",
            String::compose("%0, %1, %2", 0, 1, 2));
  EXPECT_EQ("0, 1",
            String::compose("%0, %1", 0, 1));
  EXPECT_EQ("0",
            String::compose("%0", 0));

  EXPECT_EQ("0, 1, 2, 3, 4, 5, 6, 7.5, 8, 9",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7, %8, %9", 0, 1, 2, 3, 4, 5, 6, 7.5, 8, 9));
  EXPECT_EQ("0, 1, 2, 3, 4, 5, 6, 7.5, 8",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7, %8", 0, 1, 2, 3, 4, 5, 6, 7.5, 8));
  EXPECT_EQ("0, 1, 2, 3, 4, 5, 6, 7.5",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7", 0, 1, 2, 3, 4, 5, 6, 7.5));
  EXPECT_EQ("0, 1, 2, 3, 4, 5, 6",
            String::compose("%0, %1, %2, %3, %4, %5, %6", 0, 1, 2, 3, 4, 5, 6));
  EXPECT_EQ("0, 1, 2, 3, 4, 5",
            String::compose("%0, %1, %2, %3, %4, %5", 0, 1, 2, 3, 4, 5));
  EXPECT_EQ("0, 1, 2, 3, 4",
            String::compose("%0, %1, %2, %3, %4", 0, 1, 2, 3, 4));
  EXPECT_EQ("0, 1, 2, 3",
            String::compose("%0, %1, %2, %3", 0, 1, 2, 3));
  EXPECT_EQ("0, 1, 2",
            String::compose("%0, %1, %2", 0, 1, 2));
  EXPECT_EQ("0, 1",
            String::compose("%0, %1", 0, 1));
  EXPECT_EQ("0",
            String::compose("%0", 0));

  EXPECT_EQ("0, 1, 2, 3, 4, 5, 6.5, 7, 8, 9",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7, %8, %9", 0, 1, 2, 3, 4, 5, 6.5, 7, 8, 9));
  EXPECT_EQ("0, 1, 2, 3, 4, 5, 6.5, 7, 8",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7, %8", 0, 1, 2, 3, 4, 5, 6.5, 7, 8));
  EXPECT_EQ("0, 1, 2, 3, 4, 5, 6.5, 7",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7", 0, 1, 2, 3, 4, 5, 6.5, 7));
  EXPECT_EQ("0, 1, 2, 3, 4, 5, 6.5",
            String::compose("%0, %1, %2, %3, %4, %5, %6", 0, 1, 2, 3, 4, 5, 6.5));
  EXPECT_EQ("0, 1, 2, 3, 4, 5",
            String::compose("%0, %1, %2, %3, %4, %5", 0, 1, 2, 3, 4, 5));
  EXPECT_EQ("0, 1, 2, 3, 4",
            String::compose("%0, %1, %2, %3, %4", 0, 1, 2, 3, 4));
  EXPECT_EQ("0, 1, 2, 3",
            String::compose("%0, %1, %2, %3", 0, 1, 2, 3));
  EXPECT_EQ("0, 1, 2",
            String::compose("%0, %1, %2", 0, 1, 2));
  EXPECT_EQ("0, 1",
            String::compose("%0, %1", 0, 1));
  EXPECT_EQ("0",
            String::compose("%0", 0));

  EXPECT_EQ("0, 1, 2, 3, 4, 5.5, 6, 7, 8, 9",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7, %8, %9", 0, 1, 2, 3, 4, 5.5, 6, 7, 8, 9));
  EXPECT_EQ("0, 1, 2, 3, 4, 5.5, 6, 7, 8",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7, %8", 0, 1, 2, 3, 4, 5.5, 6, 7, 8));
  EXPECT_EQ("0, 1, 2, 3, 4, 5.5, 6, 7",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7", 0, 1, 2, 3, 4, 5.5, 6, 7));
  EXPECT_EQ("0, 1, 2, 3, 4, 5.5, 6",
            String::compose("%0, %1, %2, %3, %4, %5, %6", 0, 1, 2, 3, 4, 5.5, 6));
  EXPECT_EQ("0, 1, 2, 3, 4, 5.5",
            String::compose("%0, %1, %2, %3, %4, %5", 0, 1, 2, 3, 4, 5.5));
  EXPECT_EQ("0, 1, 2, 3, 4",
            String::compose("%0, %1, %2, %3, %4", 0, 1, 2, 3, 4));
  EXPECT_EQ("0, 1, 2, 3",
            String::compose("%0, %1, %2, %3", 0, 1, 2, 3));
  EXPECT_EQ("0, 1, 2",
            String::compose("%0, %1, %2", 0, 1, 2));
  EXPECT_EQ("0, 1",
            String::compose("%0, %1", 0, 1));
  EXPECT_EQ("0",
            String::compose("%0", 0));

  EXPECT_EQ("0, 1, 2, 3, 4.5, 5, 6, 7, 8, 9",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7, %8, %9", 0, 1, 2, 3, 4.5, 5, 6, 7, 8, 9));
  EXPECT_EQ("0, 1, 2, 3, 4.5, 5, 6, 7, 8",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7, %8", 0, 1, 2, 3, 4.5, 5, 6, 7, 8));
  EXPECT_EQ("0, 1, 2, 3, 4.5, 5, 6, 7",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7", 0, 1, 2, 3, 4.5, 5, 6, 7));
  EXPECT_EQ("0, 1, 2, 3, 4.5, 5, 6",
            String::compose("%0, %1, %2, %3, %4, %5, %6", 0, 1, 2, 3, 4.5, 5, 6));
  EXPECT_EQ("0, 1, 2, 3, 4.5, 5",
            String::compose("%0, %1, %2, %3, %4, %5", 0, 1, 2, 3, 4.5, 5));
  EXPECT_EQ("0, 1, 2, 3, 4.5",
            String::compose("%0, %1, %2, %3, %4", 0, 1, 2, 3, 4.5));
  EXPECT_EQ("0, 1, 2, 3",
            String::compose("%0, %1, %2, %3", 0, 1, 2, 3));
  EXPECT_EQ("0, 1, 2",
            String::compose("%0, %1, %2", 0, 1, 2));
  EXPECT_EQ("0, 1",
            String::compose("%0, %1", 0, 1));
  EXPECT_EQ("0",
            String::compose("%0", 0));

  EXPECT_EQ("0, 1, 2, 3.5, 4, 5, 6, 7, 8, 9",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7, %8, %9", 0, 1, 2, 3.5, 4, 5, 6, 7, 8, 9));
  EXPECT_EQ("0, 1, 2, 3.5, 4, 5, 6, 7, 8",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7, %8", 0, 1, 2, 3.5, 4, 5, 6, 7, 8));
  EXPECT_EQ("0, 1, 2, 3.5, 4, 5, 6, 7",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7", 0, 1, 2, 3.5, 4, 5, 6, 7));
  EXPECT_EQ("0, 1, 2, 3.5, 4, 5, 6",
            String::compose("%0, %1, %2, %3, %4, %5, %6", 0, 1, 2, 3.5, 4, 5, 6));
  EXPECT_EQ("0, 1, 2, 3.5, 4, 5",
            String::compose("%0, %1, %2, %3, %4, %5", 0, 1, 2, 3.5, 4, 5));
  EXPECT_EQ("0, 1, 2, 3.5, 4",
            String::compose("%0, %1, %2, %3, %4", 0, 1, 2, 3.5, 4));
  EXPECT_EQ("0, 1, 2, 3.5",
            String::compose("%0, %1, %2, %3", 0, 1, 2, 3.5));
  EXPECT_EQ("0, 1, 2",
            String::compose("%0, %1, %2", 0, 1, 2));
  EXPECT_EQ("0, 1",
            String::compose("%0, %1", 0, 1));
  EXPECT_EQ("0",
            String::compose("%0", 0));

  EXPECT_EQ("0, 1, 2.5, 3, 4, 5, 6, 7, 8, 9",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7, %8, %9", 0, 1, 2.5, 3, 4, 5, 6, 7, 8, 9));
  EXPECT_EQ("0, 1, 2.5, 3, 4, 5, 6, 7, 8",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7, %8", 0, 1, 2.5, 3, 4, 5, 6, 7, 8));
  EXPECT_EQ("0, 1, 2.5, 3, 4, 5, 6, 7",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7", 0, 1, 2.5, 3, 4, 5, 6, 7));
  EXPECT_EQ("0, 1, 2.5, 3, 4, 5, 6",
            String::compose("%0, %1, %2, %3, %4, %5, %6", 0, 1, 2.5, 3, 4, 5, 6));
  EXPECT_EQ("0, 1, 2.5, 3, 4, 5",
            String::compose("%0, %1, %2, %3, %4, %5", 0, 1, 2.5, 3, 4, 5));
  EXPECT_EQ("0, 1, 2.5, 3, 4",
            String::compose("%0, %1, %2, %3, %4", 0, 1, 2.5, 3, 4));
  EXPECT_EQ("0, 1, 2.5, 3",
            String::compose("%0, %1, %2, %3", 0, 1, 2.5, 3));
  EXPECT_EQ("0, 1, 2.5",
            String::compose("%0, %1, %2", 0, 1, 2.5));
  EXPECT_EQ("0, 1",
            String::compose("%0, %1", 0, 1));
  EXPECT_EQ("0",
            String::compose("%0", 0));

  EXPECT_EQ("0, 1.5, 2, 3, 4, 5, 6, 7, 8, 9",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7, %8, %9", 0, 1.5, 2, 3, 4, 5, 6, 7, 8, 9));
  EXPECT_EQ("0, 1.5, 2, 3, 4, 5, 6, 7, 8",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7, %8", 0, 1.5, 2, 3, 4, 5, 6, 7, 8));
  EXPECT_EQ("0, 1.5, 2, 3, 4, 5, 6, 7",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7", 0, 1.5, 2, 3, 4, 5, 6, 7));
  EXPECT_EQ("0, 1.5, 2, 3, 4, 5, 6",
            String::compose("%0, %1, %2, %3, %4, %5, %6", 0, 1.5, 2, 3, 4, 5, 6));
  EXPECT_EQ("0, 1.5, 2, 3, 4, 5",
            String::compose("%0, %1, %2, %3, %4, %5", 0, 1.5, 2, 3, 4, 5));
  EXPECT_EQ("0, 1.5, 2, 3, 4",
            String::compose("%0, %1, %2, %3, %4", 0, 1.5, 2, 3, 4));
  EXPECT_EQ("0, 1.5, 2, 3",
            String::compose("%0, %1, %2, %3", 0, 1.5, 2, 3));
  EXPECT_EQ("0, 1.5, 2",
            String::compose("%0, %1, %2", 0, 1.5, 2));
  EXPECT_EQ("0, 1.5",
            String::compose("%0, %1", 0, 1.5));
  EXPECT_EQ("0",
            String::compose("%0", 0));

  EXPECT_EQ("0.5, 1, 2, 3, 4, 5, 6, 7, 8, 9",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7, %8, %9", 0.5, 1, 2, 3, 4, 5, 6, 7, 8, 9));
  EXPECT_EQ("0.5, 1, 2, 3, 4, 5, 6, 7, 8",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7, %8", 0.5, 1, 2, 3, 4, 5, 6, 7, 8));
  EXPECT_EQ("0.5, 1, 2, 3, 4, 5, 6, 7",
            String::compose("%0, %1, %2, %3, %4, %5, %6, %7", 0.5, 1, 2, 3, 4, 5, 6, 7));
  EXPECT_EQ("0.5, 1, 2, 3, 4, 5, 6",
            String::compose("%0, %1, %2, %3, %4, %5, %6", 0.5, 1, 2, 3, 4, 5, 6));
  EXPECT_EQ("0.5, 1, 2, 3, 4, 5",
            String::compose("%0, %1, %2, %3, %4, %5", 0.5, 1, 2, 3, 4, 5));
  EXPECT_EQ("0.5, 1, 2, 3, 4",
            String::compose("%0, %1, %2, %3, %4", 0.5, 1, 2, 3, 4));
  EXPECT_EQ("0.5, 1, 2, 3",
            String::compose("%0, %1, %2, %3", 0.5, 1, 2, 3));
  EXPECT_EQ("0.5, 1, 2",
            String::compose("%0, %1, %2", 0.5, 1, 2));
  EXPECT_EQ("0.5, 1",
            String::compose("%0, %1", 0.5, 1));
  EXPECT_EQ("0.5",
            String::compose("%0", 0.5));
}

class MyStringComposeTestType
{
};

std::wostream& operator<<(std::wostream& o, const MyStringComposeTestType&)
{
  return o << "operator<<(wostream, MyStringComposeTestType)";
}

std::ostream& operator<<(std::ostream& o, const MyStringComposeTestType&)
{
  return o << "operator<<(ostream, MyStringComposeTestType)";
}

TEST(base_String_compose, using_leftshift_operator)
{
  EXPECT_EQ("toString(MyStringComposeTestType)", String::compose("%0", MyStringComposeTestType()));
}

String toString(const MyStringComposeTestType&)
{
  return "toString(MyStringComposeTestType)";
}

TEST(base_String_compose, using_toString)
{
  EXPECT_EQ("toString(MyStringComposeTestType)", String::compose("%0", MyStringComposeTestType()));
}

class MyStringComposeTestType2
{
};

std::wostream& operator<<(std::wostream& o, const MyStringComposeTestType2&)
{
  return o << "operator<<(wostream, MyStringComposeTestType2)";
}

std::ostream& operator<<(std::ostream& o, const MyStringComposeTestType2&)
{
  return o << "operator<<(ostream, MyStringComposeTestType2)";
}

TEST(base_String_compose, using_leftshift_operator2)
{
  EXPECT_EQ("operator<<(ostream, MyStringComposeTestType2)", String::compose("%0", MyStringComposeTestType2()));
}


TEST(framework_StringArgumentIterator, hasNextPercent_bad_input_empty_input)
{
  String str = "";
  StringArgumentIterator iter(str);

  EXPECT_FALSE(iter.hasNextPercent());
}

TEST(framework_StringArgumentIterator, hasNextPercent_bad_input_no_percent)
{
  String str = "abcdefg";
  StringArgumentIterator iter(str);

  EXPECT_FALSE(iter.hasNextPercent());
}

TEST(framework_StringArgumentIterator, hasNextPercent_bad_input_ending_with_percent)
{
  String str = "abcdefg%";
  StringArgumentIterator iter(str);

  EXPECT_FALSE(iter.hasNextPercent());
}

TEST(framework_StringArgumentIterator, hasNextPercent_bad_input_only_one_percent)
{
  String str = "%";
  StringArgumentIterator iter(str);

  EXPECT_FALSE(iter.hasNextPercent());
}

TEST(framework_StringArgumentIterator, hasNextPercent_bad_input_not_a_number)
{
  String str = "abc%defg";
  StringArgumentIterator iter(str);

  EXPECT_TRUE(iter.hasNextPercent());
  EXPECT_FALSE(iter.isPercent());
  EXPECT_FALSE(iter.isNumber());
  EXPECT_EQ(static_cast<String::value_type>('d'), *iter.beginOfContent);
  EXPECT_EQ(static_cast<String::value_type>('d'), *iter.endOfContent);

  EXPECT_FALSE(iter.hasNextPercent());
}

TEST(framework_StringArgumentIterator, hasNextPercent_recognizing_double_percents_1)
{
  String str = "%%";
  StringArgumentIterator iter(str);

  EXPECT_TRUE(iter.hasNextPercent());
  EXPECT_TRUE(iter.isPercent());
  EXPECT_FALSE(iter.isNumber());
  EXPECT_EQ(static_cast<String::value_type>('%'), *iter.beginOfContent);
  EXPECT_EQ(static_cast<String::value_type>(0), *iter.endOfContent);

  EXPECT_FALSE(iter.hasNextPercent());
}

TEST(framework_StringArgumentIterator, hasNextPercent_recognizing_double_percents_2)
{
  String str = "ab%%cd";
  StringArgumentIterator iter(str);

  EXPECT_TRUE(iter.hasNextPercent());
  EXPECT_TRUE(iter.isPercent());
  EXPECT_FALSE(iter.isNumber());
  EXPECT_EQ(static_cast<String::value_type>('%'), *iter.beginOfContent);
  EXPECT_EQ(static_cast<String::value_type>('c'), *iter.endOfContent);

  EXPECT_FALSE(iter.hasNextPercent());
}

TEST(framework_StringArgumentIterator, hasNextPercent_recognizing_double_percents_3)
{
  String str = "%%012";
  StringArgumentIterator iter(str);

  EXPECT_TRUE(iter.hasNextPercent());
  EXPECT_TRUE(iter.isPercent());
  EXPECT_FALSE(iter.isNumber());
  EXPECT_EQ(static_cast<String::value_type>('%'), *iter.beginOfContent);
  EXPECT_EQ(static_cast<String::value_type>('0'), *iter.endOfContent);

  EXPECT_FALSE(iter.hasNextPercent());
}

TEST(framework_StringArgumentIterator, hasNextPercent_recognizing_double_percents_4)
{
  String str = "%%%012%7";
  StringArgumentIterator iter(str);

  EXPECT_TRUE(iter.hasNextPercent());
  EXPECT_TRUE(iter.isPercent());
  EXPECT_FALSE(iter.isNumber());
  EXPECT_EQ(static_cast<String::value_type>('%'), *iter.beginOfContent);
  EXPECT_EQ(static_cast<String::value_type>('%'), *iter.endOfContent);

  EXPECT_TRUE(iter.hasNextPercent());
  EXPECT_FALSE(iter.isPercent());
  EXPECT_TRUE(iter.isNumber());
  EXPECT_EQ(static_cast<String::value_type>('0'), *iter.beginOfContent);
  EXPECT_EQ(static_cast<String::value_type>('1'), *iter.endOfContent);

  EXPECT_TRUE(iter.hasNextPercent());
  EXPECT_FALSE(iter.isPercent());
  EXPECT_TRUE(iter.isNumber());
  EXPECT_EQ(static_cast<String::value_type>('7'), *iter.beginOfContent);

  EXPECT_FALSE(iter.hasNextPercent());
}

TEST(framework_StringArgumentIterator, hasNextPercent_recognizing_double_percents_5)
{
  String str = "%%%";
  StringArgumentIterator iter(str);

  EXPECT_TRUE(iter.hasNextPercent());
  EXPECT_TRUE(iter.isPercent());
  EXPECT_FALSE(iter.isNumber());
  EXPECT_EQ(static_cast<String::value_type>('%'), *iter.beginOfContent);
  EXPECT_EQ(static_cast<String::value_type>('%'), *iter.endOfContent);

  EXPECT_FALSE(iter.hasNextPercent());
}

TEST(framework_StringArgumentIterator, hasNextPercent_recognizing_double_percents_6)
{
  String str = "%%%%%012%7";
  StringArgumentIterator iter(str);

  EXPECT_TRUE(iter.hasNextPercent());
  EXPECT_TRUE(iter.isPercent());
  EXPECT_FALSE(iter.isNumber());
  EXPECT_EQ(static_cast<String::value_type>('%'), *iter.beginOfContent);
  EXPECT_EQ(static_cast<String::value_type>('%'), *iter.endOfContent);

  EXPECT_TRUE(iter.hasNextPercent());
  EXPECT_TRUE(iter.isPercent());
  EXPECT_FALSE(iter.isNumber());
  EXPECT_EQ(static_cast<String::value_type>('%'), *iter.beginOfContent);
  EXPECT_EQ(static_cast<String::value_type>('%'), *iter.endOfContent);

  EXPECT_TRUE(iter.hasNextPercent());
  EXPECT_FALSE(iter.isPercent());
  EXPECT_TRUE(iter.isNumber());
  EXPECT_EQ(static_cast<String::value_type>('0'), *iter.beginOfContent);
  EXPECT_EQ(static_cast<String::value_type>('1'), *iter.endOfContent);

  EXPECT_TRUE(iter.hasNextPercent());
  EXPECT_FALSE(iter.isPercent());
  EXPECT_TRUE(iter.isNumber());
  EXPECT_EQ(static_cast<String::value_type>('7'), *iter.beginOfContent);

  EXPECT_FALSE(iter.hasNextPercent());
}

TEST(framework_StringArgumentIterator, hasNextPercent_recognizing_double_percents_7)
{
  String str = "%%%%012%7";
  StringArgumentIterator iter(str);

  EXPECT_TRUE(iter.hasNextPercent());
  EXPECT_TRUE(iter.isPercent());
  EXPECT_FALSE(iter.isNumber());
  EXPECT_EQ(static_cast<String::value_type>('%'), *iter.beginOfContent);
  EXPECT_EQ(static_cast<String::value_type>('%'), *iter.endOfContent);

  EXPECT_TRUE(iter.hasNextPercent());
  EXPECT_TRUE(iter.isPercent());
  EXPECT_FALSE(iter.isNumber());
  EXPECT_EQ(static_cast<String::value_type>('%'), *iter.beginOfContent);
  EXPECT_EQ(static_cast<String::value_type>('0'), *iter.endOfContent);

  EXPECT_TRUE(iter.hasNextPercent());
  EXPECT_FALSE(iter.isPercent());
  EXPECT_TRUE(iter.isNumber());
  EXPECT_EQ(static_cast<String::value_type>('7'), *iter.beginOfContent);

  EXPECT_FALSE(iter.hasNextPercent());
}

TEST(framework_StringArgumentIterator, hasNextPercent_bad_input_multidigit_index)
{
  String str = "%[]";
  StringArgumentIterator iter(str);

  EXPECT_FALSE(iter.hasNextPercent());
}

TEST(framework_StringArgumentIterator, hasNextPercent_bad_input_multidigit_index_2)
{
  String str = "%[";
  StringArgumentIterator iter(str);

  EXPECT_FALSE(iter.hasNextPercent());
}

TEST(framework_StringArgumentIterator, hasNextPercent_multidigit_index)
{
  String str = "%[7]X";
  StringArgumentIterator iter(str);

  EXPECT_TRUE(iter.hasNextPercent());
  EXPECT_FALSE(iter.isPercent());
  EXPECT_TRUE(iter.isNumber());
  EXPECT_EQ(static_cast<String::size_type>(7), iter.valueAsNumber());
  EXPECT_EQ(static_cast<String::value_type>('['), *iter.beginOfContent);
  EXPECT_EQ(static_cast<String::value_type>('X'), *iter.endOfContent);

  EXPECT_FALSE(iter.hasNextPercent());
}

TEST(framework_StringArgumentIterator, hasNextPercent_multidigit_index_2)
{
  String str = "%[0000007]X";
  StringArgumentIterator iter(str);

  EXPECT_TRUE(iter.hasNextPercent());
  EXPECT_FALSE(iter.isPercent());
  EXPECT_TRUE(iter.isNumber());
  EXPECT_EQ(static_cast<String::size_type>(7), iter.valueAsNumber());
  EXPECT_EQ(static_cast<String::value_type>('['), *iter.beginOfContent);
  EXPECT_EQ(static_cast<String::value_type>('X'), *iter.endOfContent);

  EXPECT_FALSE(iter.hasNextPercent());
}


TEST(base_toString, basic_template_version)
{
  EXPECT_EQ("42", toString(42));
}


TEST(base_integerToString, integer)
{
  EXPECT_EQ("42", integerToString(42));
  EXPECT_EQ("ffff", integerToString(65535, true));
  EXPECT_EQ("0000ffff", integerToString(65535, true, 8));
  EXPECT_EQ("$$$$ffff", integerToString(65535, true, 8, '$'));
}

TEST(realToString, real)
{
  EXPECT_EQ("42", realToString(42.f));
  EXPECT_EQ("42.0000", realToString(42.f, 4, true));
}


}
