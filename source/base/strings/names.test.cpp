#include <dependencies.h>
#include <base/strings/names.h>

namespace Base {


TEST(base_isValidApplicationName, valid_names)
{
  EXPECT_TRUE(isValidApplicationName("a"));
  EXPECT_TRUE(isValidApplicationName("a."));
  EXPECT_TRUE(isValidApplicationName("a-b"));
  EXPECT_TRUE(isValidApplicationName("a_b"));
  EXPECT_TRUE(isValidApplicationName("A_0"));
  EXPECT_TRUE(isValidApplicationName("_0abc"));
}

TEST(base_isValidApplicationName, recognize_illegal_characters)
{
  EXPECT_FALSE(isValidApplicationName("+abc"));
  EXPECT_FALSE(isValidApplicationName("abc+"));
}

TEST(base_isValidApplicationName, boundary_conditions)
{
  EXPECT_FALSE(isValidApplicationName(""));
}

TEST(base_isValidApplicationName, not_beginning_with_letter)
{
  EXPECT_FALSE(isValidApplicationName(".abc"));
  EXPECT_FALSE(isValidApplicationName("0abc"));
  EXPECT_FALSE(isValidApplicationName("-abc"));
}


}
