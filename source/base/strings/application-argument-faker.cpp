#include <dependencies.h>
#include <base/strings/application-argument-faker.h>

namespace Base {


ApplicationArgumentFaker::ApplicationArgumentFaker(const std::initializer_list<std::string>& arguments)
  : arguments(arguments)
{
  this->arguments.push_back("");

  for(std::string& str : this->arguments)
    arguments_c_str.push_back(&str[0]);

  _argc = this->arguments.size()-1;
}

int* ApplicationArgumentFaker::argc()
{
  return &_argc;
}

char** ApplicationArgumentFaker::argv()
{
  return &arguments_c_str[0];
}


} // namespace Base

