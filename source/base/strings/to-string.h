#ifndef _BASE_STRINGS_TO_STRING_H_
#define _BASE_STRINGS_TO_STRING_H_

#include "./string.h"
#include "../color-tools.h"

namespace Base {


template<typename T>
class ToStringConverter
{
public:
  static String convert(const T& value)
  {
    std::ostringstream stream;

    stream << value;

    return String::fromUtf8(stream.str());
  }
};


template<typename T>
class ToStringConverter<const T>
{
  static String convert(const T& value)
  {
    return ToStringConverter<T>::convert(value);
  }
};


template<>
class ToStringConverter<String>
{
public:
  static const String& convert(const String& value)
  {
    return value;
  }
};


template<>
class ToStringConverter<QString>
{
public:
  static String convert(const QString& value)
  {
    return String::fromQString(value);
  }
};


template<typename T>
String toString(const T& value)
{
  return ToStringConverter<T>::convert(value);
}



template<typename TInteger>
String integerToString(TInteger value, bool hex=false, int minNumberDigits=1, char fill='0')
{
  static_assert(std::numeric_limits<TInteger>::is_integer,
                "integerToString expected one of the builtin integer types");

  std::ostringstream oss;

  if(hex)
    oss << std::setbase(16);

  if(minNumberDigits > 1)
    oss << std::setfill(fill) << std::setw(minNumberDigits);

  oss << value;

  return String::fromUtf8(oss.str());
}


template<typename TInteger>
String integerToStringAsHex(TInteger value, bool withPrefix=false, int minNumberDigits=sizeof(TInteger)*2)
{
  String s = integerToString(value, true, minNumberDigits);

  if(withPrefix)
    return "0x"+s;
  else
    return s;
}


inline String colorToHexCode(uint32 value, bool withPrefix=false, bool withAlpha=true)
{
  String s = integerToStringAsHex(value, false, withAlpha ? 8 : 6);

  if(withPrefix)
    return "#"+s;
  else
    return s;
}


inline String colorToHexCode(const vec4& value, bool withPrefix=false, bool withAlpha=true)
{
  return colorToHexCode(colorAsRgbaInteger(value), withPrefix, withAlpha);
}


template<typename TReal>
String realToString(TReal value, int precision=6, bool fixed=false)
{
  static_assert(std::is_floating_point<TReal>(),
                "realToString expeted one of the builtin real types");

  std::ostringstream oss;

  if(fixed)
    oss << std::fixed << std::setprecision(precision);

  oss <<  value;

  return String::fromUtf8(oss.str());
}


}


#endif
