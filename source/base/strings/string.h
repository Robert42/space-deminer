#ifndef _BASE_STRINGS_STRING_H_
#define _BASE_STRINGS_STRING_H_

#include <dependencies.h>

#include <base/output.h>

namespace Base {
class String;

class InvalidStringException : public Ogre::Exception
{
public:
  InvalidStringException(const std::string& message, const std::string& source, const char* file, int line);
};

template<typename T>
String toString(const T& value);

class String
{
public:
  typedef std::u32string raw_type;
  typedef std::u32string::value_type value_type;
  typedef std::u32string::size_type size_type;

  typedef raw_type::iterator iterator;
  typedef raw_type::const_iterator const_iterator;
  typedef raw_type::reverse_iterator reverse_iterator;
  typedef raw_type::const_reverse_iterator const_reverse_iterator;

  static const size_type npos;

private:
  raw_type raw;

public:
  String();

  String(const char* c_str_utf8);
  String(const String& str);


  template<typename Iterator>
  String(const Iterator& begin, const Iterator& end);

public:
  String& operator=(const char* c_str_utf8);
  String& operator=(const String& str);

  static String fromPath(const IO::Path& path);
  static String fromOgreString(const Ogre::String& str);
  static String fromCeguiString(const CEGUI::String& str);
  static String fromAnsi(const std::basic_string<char>& str_ansi);
  static String fromUtf8(const std::basic_string<char>& str_utf8);
  static String fromUtf16(const std::basic_string<wchar_t>& str_utf16);
  static String fromUtf32(const std::basic_string<char32_t>& str_utf32);
  static String fromQString(const QString& qstring);

  template<typename TUnicodevalue>
  static String fromUnivodeValue(TUnicodevalue v, size_t times = 1)
  {
    static_assert(std::numeric_limits<TUnicodevalue>::is_integer,
                  "String::fromUnivodeValue expected one of the builtin integer types");

    return String::fromUtf32(std::u32string(times, static_cast<value_type>(v)));
  }

  void swap(String& other);
  void reserve(size_type size);

public:
  size_type length() const;

  IO::Path toPath() const;
  Ogre::String toOgreString() const;
  CEGUI::String toCeguiString() const;
  const std::basic_string<char32_t>& toUtf32String() const;
  std::basic_string<char> toUtf8String() const;
  std::basic_string<char> toAnsiString(char replacement = '?') const;
  std::basic_string<wchar_t> toUtf16String() const;
  QString toQString() const;

  void toStream(const Output<std::basic_stringstream<char> >& stream) const;

  template<typename T>
  T& parse(const Output<T>& var) const;
  template<typename T>
  T parse() const;

  template<typename T_string>
  void toCustomUtf32String(T_string& destString) const;

  operator IO::Path() const;

public:
  static String compose(const String& format)
  {
    return format.noMoreArguments();
  }

  template<typename first_argument_type, typename... argument_types>
  static String compose(const String& format, const first_argument_type& firstArgument, const argument_types& ... otherArguments)
  {
    return compose(format.withArgument(firstArgument), otherArguments...);
  }

  String noMoreArguments() const;
  String withArgument(const String& argument, size_type i=npos) const;

  template<typename argument_type>
  String withArgument(const argument_type& argument, size_type i=npos) const
  {
    return withArgument(toString(argument), i);
  }

public:
  bool empty() const;
  void clear();

  bool containsCharacter(value_type character) const;
  bool containsSubString(const String& substring) const;
  String subString(size_type begin, size_type length = npos) const;

  void push_back(char32_t character);

  size_t find(const String& substring) const;
  size_t rfind(const String& substring) const;

  void replace(value_type replace, value_type with);

  bool matchesRegex(const String& regexPattern, bool partial=false) const;
  bool matchesRegex(const QRegularExpression& regex, bool partial=false) const;

public:
  void convertToLowerCase();
  void convertToUpperCase();
  String lowerCase() const;
  String upperCase() const;

public:
  String trim(size_type numberCharactersFromLeft, size_type numberCharactersFromRight) const;
  String trimFromLeft(size_type numberCharactersToTrim) const;
  String trimFromRight(size_type numberCharactersToTrim) const;
  String trimAllFromLeftBut(size_type numberCharactersToLeave) const;
  String trimAllFromRightBut(size_type numberCharactersToLeave) const;

  String trim(const std::function<bool(value_type)>& predicate) const;
  String trimFromLeft(const std::function<bool(value_type)>& predicate) const;
  String trimFromRight(const std::function<bool(value_type)>& predicate) const;

  String trimWhitespace() const;
  String trimWhitespaceFromLeft() const;
  String trimWhitespaceFromRight() const;

  std::list<String> split(value_type separator, bool removeEmptySubStrings = true) const;

  String append(value_type separator, const String& attachment) const;

public:
  iterator begin();
  const_iterator begin() const;
  reverse_iterator rbegin();
  const_reverse_iterator rbegin() const;
  iterator end();
  const_iterator end() const;
  reverse_iterator rend();
  const_reverse_iterator rend() const;

public:
  friend bool operator<(const String& a, const String& b);
  friend bool operator>(const String& a, const String& b);
  friend bool operator<=(const String& a, const String& b);
  friend bool operator>=(const String& a, const String& b);

  friend bool operator==(const String& a, const String& b);
  friend bool operator!=(const String& a, const String& b);

  friend String operator+(const String& a, const String& b);
  String& operator+=(const String& str);

  value_type& operator[](size_type i);
  value_type operator[](size_type i) const;

public:
  static bool isValidUtf8(const std::string& utf8, bool strict=true, value_type maxAllowedValue = 0x10ffff);

  template<typename TIterator>
  static bool isValidString(const TIterator& begin, const TIterator& end);
  template<typename TString>
  static bool isValidString(const TString& string);
  template<typename TCharacter>
  static bool isValidCharacter(TCharacter character);

  static bool isWhitespace(value_type character);

private:
  static size_type calcNumberUtf8Bytes(const raw_type& value);
  static int calcNumberUtf8Bytes(char32_t value);

  static bool is_string_empty(const String& string);
};

void PrintTo(const String& string, std::ostream* o);

std::ostream& operator<<(std::ostream& o, const String& str);
std::wostream& operator<<(std::wostream& o, const String& str);

void swap(Base::String& a, Base::String& b);


inline int qHash(const String& key, uint seed=0)
{
  return qHash(key.toQString(), seed);
}

}

#include "./string.inl"
#include "./to-string.h"

#endif
