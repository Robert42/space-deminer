#include "value.h"
#include <base/types/types.h>

namespace Base {


Value::Value()
  : Value(float(0.f))
{
}

Value::~Value()
{
  _deinit();
}


Value::Value(float value)
{
  initFromBytes(Types::_float, &value);
}

Value::Value(int value)
{
  initFromBytes(Types::_int, &value);
}

Value::Value(bool value)
{
  initFromBytes(Types::_bool, &value);
}


const Type& Value::type() const
{
  return this->_type;
}

const QVector<uint8>& Value::bytes() const
{
  return this->_bytes;
}


Value::Value(const Value& other)
{
  *this = other;
}

Value& Value::operator=(const Value& other)
{
  _deinit();

  if(other.type().description()->isPrimitiveDescription())
  {
    _type = other.type();
    _bytes = other.bytes();
  }else
  {
    _initWithFallback();
  }

  return *this;
}


bool Value::isEqualityCheckSupported() const
{
  if(type().description()->isPrimitiveDescription())
    return true;

  return false;
}

template<typename T>
bool arePrimitiveDataEqual(const Type::PrimitiveDescription& description,
                           const QVector<uint8>& thisBytes,
                           const QVector<uint8>& otherBytes)
{
  int nValues = description.size / sizeof(T);

  assert(description.size == nValues*sizeof(T));

  const T* thisValues = reinterpret_cast<const T*>(thisBytes.data());
  const T* otherValues = reinterpret_cast<const T*>(otherBytes.data());

  for(int i=0; i<nValues; ++i)
  {
    if(thisValues[i]!=otherValues[i])
      return false;
  }
  return true;
}

bool Value::operator == (const Value& other) const
{
  if(other.type() != this->type())
    return false;

  if(type().description()->isPrimitiveDescription())
  {
    Type::PrimitiveDescription::Ptr primitiveDescription = std::static_pointer_cast<Type::PrimitiveDescription>(type().description());

    assert(primitiveDescription->size == primitiveDescription->size);
    assert(primitiveDescription->size == primitiveDescription->size);

    if(primitiveDescription->floatType)
      return arePrimitiveDataEqual<float>(*primitiveDescription, this->bytes(), other.bytes());
    else if(primitiveDescription->intType)
      return arePrimitiveDataEqual<int>(*primitiveDescription, this->bytes(), other.bytes());
    else if(primitiveDescription->boolType)
      return arePrimitiveDataEqual<bool>(*primitiveDescription, this->bytes(), other.bytes());
    else
      return memcmp(this->bytes().data(), other.bytes().data(), primitiveDescription->size) == 0;
  }

  return false;
}

bool Value::operator != (const Value& other) const
{
  return !(*this == other);
}


void Value::initFromBytes(const Type& type, const void* bytes)
{
  if(type.description()->isPrimitiveDescription())
  {
    Type::PrimitiveDescription::Ptr primitiveDescription = std::static_pointer_cast<Type::PrimitiveDescription>(type.description());

    size_t size = primitiveDescription->size;

    _type = type;
    _bytes.resize(size);
    memcpy(_bytes.data(), bytes, size);
  }else
  {
    _initWithFallback();
  }
}


void Value::_initWithFallback()
{
  _deinit();

  _type = Types::_float;

  _bytes.resize(sizeof(float));
  for(int i=0; i<sizeof(float); ++i)
    _bytes[i] = 0;
}


void Value::_deinit()
{
}


} // namespace Base
