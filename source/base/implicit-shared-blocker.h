#ifndef BASE_IMPLICITSHAREDBLOCKER_H
#define BASE_IMPLICITSHAREDBLOCKER_H

namespace Base {


/**
 * @brief A simple blocker class used to block something over multiple frames.
 *
 * This class is implicitly shared. So you can copy it as often as you want with almost no costs.
 * As soon, as the last instance is destroyed, the blocker will decrease the blockCounter;
 */
class Blocker
{
  friend class InputDevice;
private:
  class BlockerImpl;
  std::shared_ptr<BlockerImpl> impl;

public:
  Blocker(int* blockCounter, const std::function<bool()>& validator);
  Blocker();

  void reset();
};


} // namespace Base

#endif // BASE_IMPLICITSHAREDBLOCKER_H
