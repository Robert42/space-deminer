#ifndef BASE_SINGLETON_INL
#define BASE_SINGLETON_INL

#include "singleton.h"

namespace Base {


// ISSUE-186


template<class T>
Singleton<T>::Singleton()
{
  static_assert(std::is_base_of<Singleton<T>, T>::value, "T must inherit Singleton<T>");

  _previousSingletonPointer = writeableSingletonPointer();

  writeableSingletonPointer() = static_cast<T*>(this);
}


template<class T>
Singleton<T>::~Singleton()
{
  if(writeableSingletonPointer() == this)
  {
    writeableSingletonPointer() = _previousSingletonPointer;
  }else
  {
    Singleton<T>* s = writeableSingletonPointer();

    while(s != nullptr)
    {
      if(s->_previousSingletonPointer == this)
      {
        s->_previousSingletonPointer = this->_previousSingletonPointer;
        break;
      }
      s = s->_previousSingletonPointer;
    }
  }
}


template<class T>
T* Singleton<T>::singletonPtr()
{
  return writeableSingletonPointer();
}


template<class T>
T& Singleton<T>::singleton()
{
  return *writeableSingletonPointer();
}


template<class T>
T*& Singleton<T>::writeableSingletonPointer()
{
  static T* singletonPointer = nullptr;
  return singletonPointer;
}


template<class T>
bool Singleton<T>::isTheOnlyInstance()
{
  return singletonPtr()==this
      && this->_previousSingletonPointer==nullptr;
}


template<class T>
int Singleton<T>::numberInstances()
{
  int n = 0;
  T* instance = singletonPtr();

  while(instance != nullptr)
  {
    instance = instance->_previousSingletonPointer;
    ++n;
  }

  return n;
}


template<class T>
T* Singleton<T>::previousSingletonPointer()
{
  return this->_previousSingletonPointer;
}


// ====


template<class T>
T* PublicSingleton<T>::singletonPtr()
{
  return Singleton<T>::singletonPtr();
}


template<class T>
T& PublicSingleton<T>::singleton()
{
  return Singleton<T>::singleton();
}


// ====


template<class T>
SmartSingleton<T>::SmartSingleton()
{
  static_assert(std::is_base_of<SmartSingleton<T>, T>::value, "T must inherit SmartSingleton<T>");
}


template<class T>
SmartSingleton<T>::~SmartSingleton()
{
}


template<class T>
std::shared_ptr<T> SmartSingleton<T>::singleton()
{
  static std::weak_ptr<T> singleton;

  std::shared_ptr<T> instance = singleton.lock();

  if(!instance)
  {
    instance = std::shared_ptr<T>(new T);
    singleton = instance;
  }

  return instance;
}


// ====


template<class T>
SharedSingleton<T>::SharedSingleton()
{
  static_assert(std::is_base_of<SharedSingleton<T>, T>::value, "T must inherit SharedSingleton<T>");
}


template<class T>
SharedSingleton<T>::~SharedSingleton()
{
}


template<class T>
std::shared_ptr<T> SharedSingleton<T>::singleton()
{
  return writeableWeakPointer().lock();
}


template<class T>
std::shared_ptr<T> SharedSingleton<T>::setSingleton(const std::shared_ptr<T>& instance)
{
  writeableWeakPointer() = instance;
  return instance;
}


template<class T>
std::weak_ptr<T>& SharedSingleton<T>::writeableWeakPointer()
{
  static std::weak_ptr<T> singleton;

  return singleton;
}


// ====

template<class T>
BlackboxSingleton<T>::BlackboxSingleton()
{
}

template<class T>
T& BlackboxSingleton<T>::singleton()
{
  static T _singleton;
  return _singleton;
}


} // namespace Base

#endif // BASE_SINGLETON_INL
