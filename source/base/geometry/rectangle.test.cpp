#include "rectangle.h"
#include "interval.h"

namespace Base {


TEST(base_rectangle, wraparound)
{
  EXPECT_EQ(ivec2(98, 2),
            Rectangle<ivec2>(ivec2(0), ivec2(100)).wraparound(ivec2(-2, 2)));
}


TEST(base_rectangle, corner)
{
  Rectangle<vec2> rect(vec2(0), vec2(1));

  EXPECT_VEC2_EQ(vec2(0), rect.corner(0));
  EXPECT_VEC2_EQ(vec2(0, 1), rect.corner(1));
  EXPECT_VEC2_EQ(vec2(1), rect.corner(2));
  EXPECT_VEC2_EQ(vec2(1, 0), rect.corner(3));
}


TEST(base_rectangle, distanceTo)
{
  Rectangle<vec2> rect(vec2(3, 2), vec2(8, 6));

  EXPECT_FLOAT_EQ(0, rect.distanceTo(vec2(5, 4)));

  EXPECT_FLOAT_EQ(sqrt_two, rect.distanceTo(vec2(2, 1)));
  EXPECT_FLOAT_EQ(sqrt_two, rect.distanceTo(vec2(2, 7)));
  EXPECT_FLOAT_EQ(sqrt_two, rect.distanceTo(vec2(9, 7)));
  EXPECT_FLOAT_EQ(sqrt_two, rect.distanceTo(vec2(9, 1)));

  EXPECT_FLOAT_EQ(1, rect.distanceTo(vec2(3, 1)));
  EXPECT_FLOAT_EQ(1, rect.distanceTo(vec2(2, 2)));
  EXPECT_FLOAT_EQ(1, rect.distanceTo(vec2(8, 1)));
  EXPECT_FLOAT_EQ(1, rect.distanceTo(vec2(9, 2)));
  EXPECT_FLOAT_EQ(1, rect.distanceTo(vec2(9, 6)));
  EXPECT_FLOAT_EQ(1, rect.distanceTo(vec2(8, 7)));
  EXPECT_FLOAT_EQ(1, rect.distanceTo(vec2(2, 6)));
  EXPECT_FLOAT_EQ(1, rect.distanceTo(vec2(3, 7)));

  EXPECT_FLOAT_EQ(2, rect.distanceTo(vec2(5, 0)));
  EXPECT_FLOAT_EQ(4, rect.distanceTo(vec2(5, 10)));
  EXPECT_FLOAT_EQ(3, rect.distanceTo(vec2(0, 4)));
  EXPECT_FLOAT_EQ(2, rect.distanceTo(vec2(10, 5)));
}


TEST(base_rectangle, createMappingMatrixTo)
{
  Rectangle<vec2> a, b;

  a = Rectangle<vec2>(vec2(1, 2), vec2(3, 4));
  b = Rectangle<vec2>(vec2(-10, -20), vec2(30, 40));

  mat3 m = a.mappingMatrixTo(b, false);

  EXPECT_EQ(vec2(-10, -20), m*vec2(1,2));
  EXPECT_EQ(vec2(30, 40), m*vec2(3,4));

  EXPECT_EQ(vec2(-10, 40), m*vec2(1,4));
  EXPECT_EQ(vec2(30, -20), m*vec2(3,2));

  m = a.mappingMatrixTo(b, true);

  EXPECT_EQ(vec2(-10, -20), m*vec2(1,4));
  EXPECT_EQ(vec2(30, 40), m*vec2(3,2));

  EXPECT_EQ(vec2(-10, 40), m*vec2(1,2));
  EXPECT_EQ(vec2(30, -20), m*vec2(3,4));
}

TEST(base_rectangle, absoluteSubRect)
{
  Rectangle<vec2> a, b;

  a = Rectangle<vec2>(vec2(-10, -20), vec2(30, 40));

  b = a.absoluteSubRect(vec2(1, -1), vec2(0), true);

  EXPECT_EQ(vec2(-9, 39), b.min());
}



}
