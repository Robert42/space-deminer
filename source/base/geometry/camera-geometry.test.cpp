#include "camera-geometry.h"
#include "matrix4-helper.h"

namespace Base {
namespace Geometry {


TEST(base_geometry_Camera, viewportMatrix)
{
  Camera c;

  EXPECT_EQ(vec3(0, 0, 0),
            transformVertex(c.viewportMatrix(), vec3(0, 0, 0)));
  EXPECT_EQ(vec3(1, 1, 0),
            transformVertex(c.viewportMatrix(), vec3(1, 1, 0)));
  EXPECT_EQ(vec3(-1, -1, 0),
            transformVertex(c.viewportMatrix(), vec3(-1, -1, 0)));

  c.setViewport(Rectangle<vec2>(vec2(0), vec2(1024, 768)));

  EXPECT_EQ(vec3(512, 384, 0),
            transformVertex(c.viewportMatrix(), vec3(0, 0, 0)));
  EXPECT_EQ(vec3(1024, 768, 0),
            transformVertex(c.viewportMatrix(), vec3(1, 1, 0)));
  EXPECT_EQ(vec3(0, 0, 0),
            transformVertex(c.viewportMatrix(), vec3(-1, -1, 0)));

  c.setFlipY(true);

  EXPECT_EQ(vec3(512, 384, 0),
            transformVertex(c.viewportMatrix(), vec3(0, 0, 0)));
  EXPECT_EQ(vec3(1024, 0, 0),
            transformVertex(c.viewportMatrix(), vec3(1, 1, 0)));
  EXPECT_EQ(vec3(0, 768, 0),
            transformVertex(c.viewportMatrix(), vec3(-1, -1, 0)));
}

TEST(base_geometry_Camera, projectionMatrix)
{
  Camera c;

  c.setFovX(radians(90.f));
}

TEST(base_geometry_Camera, matrixProduct_fovx)
{
  Camera c;

  c.setFovX(radians(90.f));
  c.setViewport(Rectangle<vec2>(vec2(0), vec2(1920, 1080)));
  c.setFlipY(true);

  mat4 view = c.viewportMatrix();
  mat4 proj = c.projectionMatrix();

  EXPECT_VEC2_NEAR(vec2(363, 178),
                   transformVertex(view * proj, vec3(-0.27194,
                                              0.16478,
                                              -0.43725)).xy(),
                   1);
}


TEST(base_geometry_Camera, matrixProduct_fovy)
{
  Camera c;

  c.setFovY(radians(90.f));
  c.setViewport(Rectangle<vec2>(vec2(0), vec2(1920, 1080)));
  c.setFlipY(true);

  mat4 view = c.viewportMatrix();
  mat4 proj = c.projectionMatrix();

  EXPECT_VEC2_NEAR(vec2(624, 336),
                   transformVertex(view * proj, vec3(-0.27194,
                                                     0.16478,
                                                     -0.43725)).xy(),
                   1);
}


TEST(base_geometry_Camera, normalizePointInViewport)
{
  Camera c;

  EXPECT_VEC2_EQ(vec2(1),
                 c.normalizePointInViewport(vec2(1)));
  EXPECT_VEC2_EQ(vec2(0),
                 c.normalizePointInViewport(vec2(0)));
  EXPECT_VEC2_EQ(vec2(-1),
                 c.normalizePointInViewport(vec2(-1)));

  c.setViewport(Rectangle<vec2>(vec2(0), vec2(1920, 1080)));

  EXPECT_VEC2_EQ(vec2(1),
                 c.normalizePointInViewport(vec2(1920, 1080)));
  EXPECT_VEC2_EQ(vec2(-1),
                 c.normalizePointInViewport(vec2(0)));

  c.setFlipY(true);

  EXPECT_VEC2_EQ(vec2(1),
                 c.normalizePointInViewport(vec2(1920, 0)));
  EXPECT_VEC2_EQ(vec2(-1),
                 c.normalizePointInViewport(vec2(0, 1080)));
}

TEST(base_geometry_Camera, rayForViewportPosition_grid)
{
  const vec3 cameraPosition = vec3(7.48113, -6.50764, 5.34367);
  const quaternion cameraOrientation = normalize(quaternion(0.782, 0.482, 0.213, 0.334));

  Camera camera;
  camera.setFlipY(true);
  camera.setFovY(radians(30.f));
  camera.setViewport(Rectangle<vec2>(vec2(0), vec2(1920, 1080)));
  camera.coordinate.position = cameraPosition;
  camera.coordinate.orientation = cameraOrientation;

  Camera cameraWithoutTransformation;
  cameraWithoutTransformation.setFlipY(true);
  cameraWithoutTransformation.setFovY(radians(30.f));
  cameraWithoutTransformation.setViewport(Rectangle<vec2>(vec2(0), vec2(1920, 1080)));

  const mat4 m = camera.viewportMatrix() * camera.projectionMatrix() * camera.viewMatrix();

  for(real x=0; x<=1920.f; x += 10.f)
  {
    for(real y=0; y<=1080.f; y += 10.f)
    {
      const vec2 viewportPosition = vec2(x, y);

      Ray ray = camera.rayForViewportPosition(viewportPosition);
      Ray rayWithoutCameraTransformation = cameraWithoutTransformation.rayForViewportPosition(viewportPosition);

      const vec3 spatialPosition = ray.pointAlongRay(10.f);

      const vec3 expectedViewportPosition = rayWithoutCameraTransformation.pointAlongRay(10.f);
      vec3 viewspacePosition;
      vec2 projectedScreenPosition;

      viewspacePosition = camera.coordinate.invertedCoordinate() * spatialPosition;
      EXPECT_VEC3_NEAR(expectedViewportPosition, viewspacePosition, 1.e-3f);

      viewspacePosition = transformVertex(camera.viewMatrix(), spatialPosition);
      EXPECT_VEC3_NEAR(expectedViewportPosition, viewspacePosition, 1.e-3f);

      projectedScreenPosition = transformVertex(cameraWithoutTransformation.viewportMatrix() * cameraWithoutTransformation.projectionMatrix(), viewspacePosition).xy();
      EXPECT_VEC2_NEAR(viewportPosition, projectedScreenPosition, 1.e-3f);

      projectedScreenPosition = transformVertex(m, spatialPosition).xy();
      EXPECT_VEC2_NEAR(viewportPosition, projectedScreenPosition, 1.e-3f);
    }
  }

}


TEST(base_geometry_Camera, rayForViewportPosition)
{
  const vec2 viewportPosition = vec2(322, 390);
  const vec3 spatialPosition = vec3(-1.637, -2.39835, 1.52539);
  const vec3 cameraPosition = vec3(7.48113, -6.50764, 5.34367);
  const quaternion cameraOrientation = normalize(quaternion(0.782, 0.482, 0.213, 0.334));

  Camera c;
  c.setFlipY(true);
  c.setFovY(radians(30.f));
  c.setViewport(Rectangle<vec2>(vec2(0), vec2(1920, 1080)));
  c.coordinate.position = cameraPosition;
  c.coordinate.orientation = cameraOrientation;

  Ray ray = c.rayForViewportPosition(viewportPosition);

  Ray expectedRay(cameraPosition,
                  normalize(spatialPosition-cameraPosition));

  EXPECT_VEC3_EQ(expectedRay.origin, ray.origin);
  EXPECT_VEC3_NEAR(expectedRay.direction, ray.direction, 1.e-3f);
}


} // namespace Geometry
} // namespace Base
