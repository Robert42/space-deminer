#include "region.h"

namespace Base {
namespace Geometry {

Region::Region()
{
}

Region::~Region()
{
}

bool Region::intersects(const Circle& circle) const
{
  return distanceTo(circle.centerPosition()) <= circle.radius();
}

bool Region::empty() const
{
  return false;
}

// ==== Empty Region ====

class EmptyRegion : public Region
{
public:
  EmptyRegion()
  {
  }

  bool contains(const vec2& point) const override
  {
    (void)point;
    return false;
  }

  real distanceTo(const vec2& point) const override
  {
    (void)point;
    return INFINITY;
  }

  vec2 nearestPointTo(const vec2& point) const override
  {
    return point;
  }

  bool intersects(const Rectangle<vec2>& rectangle) const override
  {
    (void)rectangle;
    return false;
  }

  bool empty() const override
  {
    return true;
  }
};

Region::Ptr Region::createEmptyRegion()
{
  return Ptr(new EmptyRegion);
}

// ==== Circle Region ====

class CirclarRegion : public Region
{
public:
  Circle circle;

  CirclarRegion(const Circle& circle)
    : circle(circle)
  {
  }

  bool contains(const vec2& point) const override
  {
    return circle.contains(point);
  }

  real distanceTo(const vec2& point) const override
  {
    return circle.distanceTo(point);
  }

  vec2 nearestPointTo(const vec2& point) const override
  {
    return circle.nearestPointTo(point);
  }

  bool intersects(const Rectangle<vec2>& rectangle) const override
  {
    return circle.intersects(rectangle);
  }
};

Region::Ptr Region::createCirclarRegion(const Circle& circle)
{
  return Ptr(new CirclarRegion(circle));
}

// ==== Rectangular Region ====

class RectangularRegion : public Region
{
public:
  Rectangle<vec2> rectangle;

  RectangularRegion(const Rectangle<vec2>& rectangle)
    : rectangle(rectangle)
  {
  }

  bool contains(const vec2& point) const override
  {
    return rectangle.contains(point);
  }

  real distanceTo(const vec2& point) const override
  {
    return rectangle.distanceTo(point);
  }

  vec2 nearestPointTo(const vec2& point) const override
  {
    return rectangle.nearestPointTo(point);
  }

  bool intersects(const Rectangle<vec2>& rectangle) const override
  {
    return rectangle.intersects(rectangle);
  }
};

Region::Ptr Region::createRectangularRegion(const Rectangle<vec2>& rectangle)
{
  return Ptr(new RectangularRegion(rectangle));
}

// ==== RoundedRectangel Region ====

class RoundedRectangleRegion final : public Region
{
public:
  RoundedRectangle roundedRectangle;

  RoundedRectangleRegion(const RoundedRectangle& roundedRectangle)
    : roundedRectangle(roundedRectangle)
  {
  }

  bool contains(const vec2& point) const override
  {
    return roundedRectangle.contains(point);
  }

  real distanceTo(const vec2& point) const override
  {
    return roundedRectangle.distanceTo(point);
  }

  vec2 nearestPointTo(const vec2& point) const override
  {
    return roundedRectangle.nearestPointTo(point);
  }

  bool intersects(const Rectangle<vec2>& rectangle) const override
  {
    return roundedRectangle.intersects(rectangle);
  }
};

Region::Ptr Region::createRoundedRectangelRegion(const RoundedRectangle& rectangle)
{
  return Ptr(new RoundedRectangleRegion(rectangle));
}

// ==== Region Mix ====

class RegionMix : public Region
{
public:
  const QVector<Region::Ptr> regions;

  RegionMix(const QVector<Ptr>& regions)
    : regions(removeEmptyRegions(regions))
  {
  }

  static QVector<Region::Ptr> removeEmptyRegions(const QVector<Ptr>& regions)
  {
    QVector<Region::Ptr> newVector;
    for(const Region::Ptr& region : regions)
    {
      if(!region->empty())
        newVector.append(region);
    }

    return newVector;
  }
};

// ==== United Region ====

class UnitedRegion : public RegionMix
{
public:
  UnitedRegion(const QVector<Ptr>& regions)
    : RegionMix(regions)
  {
  }

  bool contains(const vec2& point) const override
  {
    for(const Ptr& region : regions)
      if(region->contains(point))
        return true;
    return false;
  }

  real distanceTo(const vec2& point) const override
  {
    real distance = INFINITY;
    for(const Ptr& region : regions)
      distance = std::min(distance, region->distanceTo(point));
    return distance;
  }

  vec2 nearestPointTo(const vec2& point) const override
  {
    vec2 nearestPoint = point;
    real bestSquareDistance = INFINITY;

    for(const Ptr& region : regions)
    {
      vec2 p = region->nearestPointTo(point);
      real d = squareDistance(point, p);

      if(bestSquareDistance > d)
      {
        bestSquareDistance = d;
        nearestPoint = p;
      }
    }

    return nearestPoint;
  }

  bool intersects(const Rectangle<vec2>& rectangle) const override
  {
    for(const Ptr& region : regions)
      if(region->intersects(rectangle))
        return true;
    return false;
  }

  bool empty() const override
  {
    return regions.empty();
  }
};

Region::Ptr Region::createUnitedRegions(const QVector<Ptr>& regions)
{
  return Ptr(new UnitedRegion(regions));
}

// ==== Intersected Region ====

class IntersectedRegion : public RegionMix
{
public:
  IntersectedRegion(const QVector<Ptr>& regions)
    : RegionMix(regions)
  {
  }

  bool contains(const vec2& point) const override
  {
    for(const Ptr& region : regions)
      if(!region->contains(point))
        return false;
    return true;
  }

  real distanceTo(const vec2& point) const override
  {
    if(regions.empty())
      return INFINITY;

    real distance = -INFINITY;
    for(const Ptr& region : regions)
      distance = std::max(distance, region->distanceTo(point));

    return distance;
  }

  vec2 nearestPointTo(const vec2& point) const override
  {
    vec2 nearestPoint = point;
    real bestSquareDistance = -INFINITY;

    for(const Ptr& region : regions)
    {
      vec2 p = region->nearestPointTo(point);
      real d = squareDistance(p, point);

      if(bestSquareDistance < d)
      {
        bestSquareDistance = d;
        nearestPoint = p;
      }
    }

    return nearestPoint;
  }

  bool intersects(const Rectangle<vec2>& rectangle) const override
  {
    for(const Ptr& region : regions)
      if(!region->intersects(rectangle))
        return false;
    return true;
  }

  bool empty() const override
  {
    return regions.empty();
  }
};

Region::Ptr Region::createIntersectedRegions(const QVector<Ptr>& regions)
{
  return Ptr(new IntersectedRegion(regions));
}


} // namespace Geometry
} // namespace Base
