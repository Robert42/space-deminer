#include "plane.h"

namespace Base {
namespace Geometry {

Plane::Plane(const vec3& normal, real d)
  : normal(normal),
    d(d)
{
}

} // namespace Geometry
} // namespace Base
