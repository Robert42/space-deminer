#ifndef BASE_CIRCLE_H
#define BASE_CIRCLE_H

#include <dependencies.h>

namespace Base {

template<typename Tvec2>
class Rectangle;

class Circle
{
private:
  vec2 _centerPosition;
  real _radius;

public:
  Circle(const vec2& centerPosition, real _radius);

  const vec2& centerPosition() const;
  real radius() const;

  bool contains(const vec2& point) const;

  template<class Tvec2>
  bool contains(const Rectangle<Tvec2>& rectangle) const;
  template<class Tvec2>
  bool contains(const Rectangle<Tvec2>& rectangle, real epsilon) const;

  template<class Tvec2>
  bool intersects(const Rectangle<Tvec2>& rectangle) const;
  bool intersects(const Circle& other) const;

  real distanceTo(const vec2& point) const;

  real circumference() const;

  Rectangle<vec2> boundingRect() const;

  vec2 clamp(const vec2& point) const;
  vec2 wraparound(const vec2& point) const;

  vec2 nearestPointTo(const vec2& point) const;

  Circle expandedCircle(real space) const;

  vec2 constrainPointDistance(const vec2& point, const std::function<real(real)>& constraint) const;

  vec2 edgePointForAngle(real angle) const;
  vec2 edgePointForAngle(real angle, const vec2& scale) const;
  static vec2 directionForAngle(real angle);
};

} // namespace Base

#include "circle.inl"

#endif // BASE_CIRCLE_H
