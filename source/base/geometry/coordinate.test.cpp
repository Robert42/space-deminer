#include <base/geometry/coordinate.h>
#include <base/geometry/matrix4-helper.h>

namespace Base {

using namespace Geometry;


TEST(base_Coordinate, rotate_global_x)
{
  Coordinate coordinate;

  coordinate.rotateX(glm::radians(30.0f));

  EXPECT_FLOAT_EQ(glm::radians(30.f),
                  coordinate.orientationAngles().x);
  EXPECT_FLOAT_EQ(0.f,
                  coordinate.orientationAngles().y);
  EXPECT_FLOAT_EQ(0.f,
                  coordinate.orientationAngles().z);
}


TEST(base_Coordinate, rotate_global_y)
{
  Coordinate coordinate;

  coordinate.rotateY(glm::radians(30.0f));

  EXPECT_FLOAT_EQ(0.f,
                  coordinate.orientationAngles().x);
  EXPECT_FLOAT_EQ(glm::radians(30.f),
                  coordinate.orientationAngles().y);
  EXPECT_FLOAT_EQ(0.f,
                  coordinate.orientationAngles().z);
}


TEST(base_Coordinate, rotate_global_z)
{
  Coordinate coordinate;

  coordinate.rotateZ(glm::radians(30.0f));

  EXPECT_FLOAT_EQ(0.f,
                  coordinate.orientationAngles().x);
  EXPECT_FLOAT_EQ(0.f,
                  coordinate.orientationAngles().y);
  EXPECT_FLOAT_EQ(glm::radians(30.f),
                  coordinate.orientationAngles().z);
}


TEST(base_Coordinate, rotate_local_equiv_global_x)
{
  Coordinate coordinate;

  coordinate.rotateLocalX(glm::radians(30.0f));

  EXPECT_FLOAT_EQ(glm::radians(30.f),
                  coordinate.orientationAngles().x);
  EXPECT_FLOAT_EQ(0.f,
                  coordinate.orientationAngles().y);
  EXPECT_FLOAT_EQ(0.f,
                  coordinate.orientationAngles().z);
}


TEST(base_Coordinate, rotate_local_equiv_global_y)
{
  Coordinate coordinate;

  coordinate.rotateLocalY(glm::radians(30.0f));

  EXPECT_FLOAT_EQ(0.f,
                  coordinate.orientationAngles().x);
  EXPECT_FLOAT_EQ(glm::radians(30.f),
                  coordinate.orientationAngles().y);
  EXPECT_FLOAT_EQ(0.f,
                  coordinate.orientationAngles().z);
}


TEST(base_Coordinate, rotate_local_equiv_global_z)
{
  Coordinate coordinate;

  coordinate.rotateLocalZ(glm::radians(30.0f));

  EXPECT_FLOAT_EQ(0.f,
                  coordinate.orientationAngles().x);
  EXPECT_FLOAT_EQ(0.f,
                  coordinate.orientationAngles().y);
  EXPECT_FLOAT_EQ(glm::radians(30.f),
                  coordinate.orientationAngles().z);
}


TEST(base_Coordinate, rotate_local_x_after_global_z)
{
  Coordinate coordinate;

  coordinate.rotateZ(glm::radians(90.f));
  coordinate.rotateLocalX(glm::radians(30.0f));

  EXPECT_FLOAT_EQ(glm::radians(30.f),
                  coordinate.orientationAngles().x);
  EXPECT_FLOAT_EQ(0.f,
                  coordinate.orientationAngles().y);
  EXPECT_FLOAT_EQ(glm::radians(90.f),
                  coordinate.orientationAngles().z);
}


TEST(base_Coordinate, concatenate)
{
  Coordinate a;
  Coordinate b;

  a.position = vec3(10, 5, 0);
  b.position = vec3(1, 3, 0);
  a.orientation = glm::angleAxis(radians(90.f), vec3(0, 0, 1));
  b.orientation = glm::angleAxis(radians(10.f), vec3(0, 0, 1));

  Coordinate coordinate = a * b;

  EXPECT_FLOAT_EQ(glm::radians(0.f), coordinate.orientationAngles().x);
  EXPECT_FLOAT_EQ(glm::radians(0.f), coordinate.orientationAngles().y);
  EXPECT_FLOAT_EQ(glm::radians(100.f), coordinate.orientationAngles().z);
  EXPECT_FLOAT_EQ(7.f, coordinate.position.x);
  EXPECT_FLOAT_EQ(6.f, coordinate.position.y);
  EXPECT_FLOAT_EQ(0.f, coordinate.position.z);
}


TEST(base_Coordinate, toMatrix4x4)
{
  Coordinate a;

  a.position = vec3(10, 5, 0);
  a.orientation = glm::angleAxis(radians(90.f), vec3(0, 0, 1));

  mat4 m = a.toMatrix4x4();

  EXPECT_NEAR(0.f, m[0][0], 1.e-7f);
  EXPECT_FLOAT_EQ( 1.f, m[0][1]);
  EXPECT_FLOAT_EQ( 0.f, m[0][2]);
  EXPECT_FLOAT_EQ( 0.f, m[0][3]);

  EXPECT_FLOAT_EQ(-1.f, m[1][0]);
  EXPECT_NEAR(0.f, m[1][1], 1.e-7f);
  EXPECT_FLOAT_EQ( 0.f, m[1][2]);
  EXPECT_FLOAT_EQ( 0.f, m[1][3]);

  EXPECT_FLOAT_EQ( 0.f, m[2][0]);
  EXPECT_FLOAT_EQ( 0.f, m[2][1]);
  EXPECT_FLOAT_EQ( 1.f, m[2][2]);
  EXPECT_FLOAT_EQ( 0.f, m[2][3]);

  EXPECT_FLOAT_EQ(10.f, m[3][0]);
  EXPECT_FLOAT_EQ( 5.f, m[3][1]);
  EXPECT_FLOAT_EQ( 0.f, m[3][2]);
  EXPECT_FLOAT_EQ( 1.f, m[3][3]);
}


TEST(base_Coordinate, toMatrix4x3)
{
  Coordinate a;

  a.position = vec3(10, 5, 0);
  a.orientation = glm::angleAxis(radians(90.f), vec3(0, 0, 1));

  mat4x3 m = a.toMatrix4x3();

  EXPECT_NEAR(0.f, m[0][0], 1.e-7f);
  EXPECT_FLOAT_EQ( 1.f, m[0][1]);
  EXPECT_FLOAT_EQ( 0.f, m[0][2]);

  EXPECT_FLOAT_EQ(-1.f, m[1][0]);
  EXPECT_NEAR(0.f, m[1][1], 1.e-7f);
  EXPECT_FLOAT_EQ( 0.f, m[1][2]);

  EXPECT_FLOAT_EQ( 0.f, m[2][0]);
  EXPECT_FLOAT_EQ( 0.f, m[2][1]);
  EXPECT_FLOAT_EQ( 1.f, m[2][2]);

  EXPECT_FLOAT_EQ(10.f, m[3][0]);
  EXPECT_FLOAT_EQ( 5.f, m[3][1]);
  EXPECT_FLOAT_EQ( 0.f, m[3][2]);
}


TEST(base_Coordinate, invert)
{
  Coordinate a;

  a.position = vec3(10, 5, 0);
  a.orientation = glm::angleAxis(radians(30.f), vec3(0, 0, 1));

  Coordinate inv = a.invertedCoordinate();

  const vec3 v(1, 2, 3);

  EXPECT_VEC3_NEAR(v, a * (inv * v), 1.e-4f);
  EXPECT_VEC3_NEAR(v, inv * (a * v), 1.e-4f);

  EXPECT_VEC3_NEAR(v, (a * inv) * v, 1.e-4f);
  EXPECT_VEC3_NEAR(v, (inv * a) * v, 1.e-4f);

  EXPECT_VEC3_NEAR(v, transformVertex((a * inv).toMatrix4x4(), v), 1.e-4f);
  EXPECT_VEC3_NEAR(v, transformVertex((inv * a).toMatrix4x4(), v), 1.e-4f);

  EXPECT_VEC3_NEAR(v, transformVertex(a.toMatrix4x4() * inv.toMatrix4x4(), v), 1.e-4f);
  EXPECT_VEC3_NEAR(v, transformVertex(inv.toMatrix4x4() * a.toMatrix4x4(), v), 1.e-4f);

  EXPECT_VEC3_NEAR(v, transformVertex(a.toMatrix4x4(), transformVertex(inv.toMatrix4x4(), v)), 1.e-4f);
  EXPECT_VEC3_NEAR(v, transformVertex(inv.toMatrix4x4(), transformVertex(a.toMatrix4x4(), v)), 1.e-4f);
}


} // namespace Base

