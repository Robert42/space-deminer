#ifndef BASE_GEOMETRY_MAT3_TRANSFORMATIONS_H
#define BASE_GEOMETRY_MAT3_TRANSFORMATIONS_H

#include <dependencies.h>

namespace Base {
namespace Geometry {


mat3 translate3(const vec2& t);
mat3 scale3(const vec2& s);
mat3 rotate3(real angle);
mat3 rotateDiscrete3(int angle);

mat3x2 mat3x3ToMat3x2(const mat3& m);

vec2 transform(const mat3& m, const vec2& v);

} // namespace Geometry

vec2 operator*(const mat3& m, const vec2& v);

} // namespace Base


#endif // BASE_GEOMETRY_MAT3_TRANSFORMATIONS_H
