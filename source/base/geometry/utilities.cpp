#include "utilities.h"

namespace Base {


vec2 fitSizeKeepingAspectRatio(const vec2& aspectRatio, const vec2& targetSize, bool expand)
{
  bool alignToWidth = targetSize.x * aspectRatio.y < aspectRatio.x * targetSize.y;

  if(expand)
    alignToWidth = !alignToWidth;

  vec2 size = targetSize;

  if(alignToWidth)
  {
    size.y = size.x * aspectRatio.y / aspectRatio.x;
  }else
  {
    size.x = size.y * aspectRatio.x / aspectRatio.y;
  }

  return size;
}

QPointF toQPointF(const vec2& p)
{
  return QPointF(p.x, p.y);
}

QPoint toQPoint(const ivec2& p)
{
  return QPoint(p.x, p.y);
}

QSizeF toQSizeF(const vec2& s)
{
  return QSizeF(s.x, s.y);
}

QSize toQSize(const ivec2& s)
{
  return QSize(s.x, s.y);
}

QRect toQRect(const Rectangle<ivec2>& r)
{
  return QRect(toQPoint(r.min()),
               toQPoint(r.max()));
}

QRectF toQRectF(const Rectangle<vec2>& r)
{
  return QRectF(toQPointF(r.min()),
                toQPointF(r.max()));
}

QRect toQRect(const ivec2& a, const ivec2& b)
{
  return toQRect(Rectangle<ivec2>(a, b));
}

QRectF toQRectF(const vec2& a, const vec2& b)
{
  return toQRectF(Rectangle<vec2>(a, b));
}


vec2 fromQPointF(const QPointF& p)
{
  return vec2(p.x(), p.y());
}

ivec2 fromQPoint(const QPoint& p)
{
  return ivec2(p.x(), p.y());
}

vec2 fromQSizeF(const QSizeF& s)
{
  return vec2(s.width(), s.height());
}

ivec2 fromQSize(const QSize& s)
{
  return ivec2(s.width(), s.height());
}


} // namespace Base
