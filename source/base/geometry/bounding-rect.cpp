#include "bounding-rect.h"

namespace Base {
namespace Geometry {

BoundingRect::BoundingRect()
{
  _empty = true;
}


const Rectangle<vec2>& BoundingRect::rectangle() const
{
  return _rectangle;
}


bool BoundingRect::empty() const
{
  return _empty;
}


Signals::Signal<void()>& BoundingRect::signalChanged()
{
  return _signalChanged;
}


void BoundingRect::set(const Rectangle<vec2>& rectangle)
{
  if(this->_rectangle != rectangle || this->_empty)
  {
    this->_empty = false;
    this->_rectangle = rectangle;

    _signalChanged();
  }
}


void BoundingRect::reset()
{
  if(!this->_empty)
  {
    this->_empty = true;

    _signalChanged();
  }
}


bool BoundingRect::operator==(const BoundingRect& other)const
{
  if(this->empty())
    return other.empty();

  return this->rectangle() == other.rectangle();
}


bool BoundingRect::operator!=(const BoundingRect& other)const
{
  return !(*this == other);
}


BoundingRect& BoundingRect::operator|=(const BoundingRect& other)
{
  if(!other.empty())
    return *this |= other.rectangle();

  return *this;
}


BoundingRect& BoundingRect::operator|=(const Rectangle<vec2>& rectangle)
{
  if(this->empty())
    set(rectangle);
  else
    set(this->rectangle() | rectangle);

  return *this;
}


BoundingRect& BoundingRect::operator|=(const vec2& point)
{
  return *this |= Rectangle<vec2>(point, point);
}


std::ostream& operator<<(std::ostream& o, const BoundingRect& rectangle)
{
  o << "BoundingRect(";
  if(rectangle.empty())
    o << "<<empty>>";
  else
    o << rectangle.rectangle().min() <<",   " << rectangle.rectangle().max();
  return o <<")";
}

void PrintTo(const BoundingRect& rectangle, std::ostream* o)
{
  *o << rectangle;
}


} // namespace Geometry
} // namespace Base
