#include <base/geometry/coordinate.h>

namespace Base {


const vec3 unitX = vec3_unitX;
const vec3 unitY = vec3_unitY;
const vec3 unitZ = vec3_unitZ;

Coordinate::Coordinate(const vec3& position,
                       const quaternion& orientation)
  : orientation(orientation),
    position(position)
{
}


void Coordinate::move(const vec3& distance)
{
  this->position += distance;
}


void Coordinate::moveX(real distance)
{
  move(distance * unitX);
}


void Coordinate::moveY(real distance)
{
  move(distance * unitY);
}


void Coordinate::moveZ(real distance)
{
  move(distance * unitZ);
}


void Coordinate::moveLocal(const vec3& localDistance)
{
  move(this->orientation * localDistance);
}


void Coordinate::moveLocalX(real distance)
{
  moveLocal(distance * unitX);
}


void Coordinate::moveLocalY(real distance)
{
  moveLocal(distance * unitY);
}


void Coordinate::moveLocalZ(real distance)
{
  moveLocal(distance * unitZ);
}



vec3 Coordinate::localXAxis() const
{
  return this->orientation * unitX;
}


vec3 Coordinate::localYAxis() const
{
  return this->orientation * unitY;
}


vec3 Coordinate::localZAxis() const
{
  return this->orientation * unitZ;
}


void Coordinate::rotate(const quaternion& q)
{
  this->orientation = glm::normalize(q * this->orientation);
}


void Coordinate::rotate(real angle, const vec3& axis)
{
  rotate(glm::angleAxis(angle, glm::normalize(axis)));
}


void Coordinate::rotateX(real angle)
{
  this->rotate(angle, unitX);
}


void Coordinate::rotateY(real angle)
{
  this->rotate(angle, unitY);
}


void Coordinate::rotateZ(real angle)
{
  this->rotate(angle, unitZ);
}


void Coordinate::rotateLocal(const quaternion& q)
{
  this->orientation = glm::normalize(this->orientation *  q);
}


void Coordinate::rotateLocal(real angle, const vec3& localAxis)
{
  rotateLocal(glm::angleAxis(angle, glm::normalize(localAxis)));
}


void Coordinate::rotateLocalX(real angle)
{
  this->rotateLocal(angle, unitX);
}


void Coordinate::rotateLocalY(real angle)
{
  this->rotateLocal(angle, unitY);
}


void Coordinate::rotateLocalZ(real angle)
{
  this->rotateLocal(angle, unitZ);
}


vec3 Coordinate::orientationAngles() const
{
  const vec3 eulerAngles = glm::eulerAngles(orientation);

  const float pitch = eulerAngles.x;
  const float yaw = eulerAngles.z;
  const float roll = eulerAngles.y;

  const float pan = yaw;
  const float tilt = pitch;

  return vec3(tilt, roll, pan);
}


Coordinate Coordinate::invertedCoordinate() const
{
  Coordinate temp = *this;
  return temp.invert();
}


Coordinate& Coordinate::invert()
{
  this->orientation = inverse(this->orientation);

  // Note that the orientation is already inversed here:
  this->position = this->orientation * -this->position;

  return *this;
}


vec3 Coordinate::operator*(const vec3& v) const
{
  return this->orientation*v + this->position;
}


Coordinate& Coordinate::operator*=(const Coordinate& c)
{
  // the position has to be set before the orientation, as the old orientation is needed to set the position
  this->position += this->orientation * c.position;
  this->orientation *= c.orientation;

  return *this;
}


Coordinate Coordinate::operator*(const Coordinate& c) const
{
  Coordinate temp(*this);
  return temp *= c;
}


mat4 Coordinate::toMatrix4x4() const
{
  mat4 m = glm::mat4_cast(this->orientation);

  m[3].x = this->position.x;
  m[3].y = this->position.y;
  m[3].z = this->position.z;

  return m;
}


mat4x3 Coordinate::toMatrix4x3() const
{
  mat3 m3 = glm::mat3_cast(this->orientation);
  mat4x3 m;

  m[0] = m3[0].xyz();
  m[1] = m3[1].xyz();
  m[2] = m3[2].xyz();

  m[3] = this->position;

  return m;
}


bool Coordinate::operator==(const Coordinate& coordinate) const
{
  return this->orientation == coordinate.orientation
      && this->position == coordinate.position;
}

bool Coordinate::operator!=(const Coordinate& coordinate) const
{
  return !(*this == coordinate);
}


void Coordinate::applyToOgreSceneNode(Ogre::SceneNode* sceneNode) const
{
  sceneNode->setOrientation(Ogre::Quaternion_cast(this->orientation));
  sceneNode->setPosition(Ogre::Vector3_cast(this->position));
}


Coordinate inverse(const Coordinate& c)
{
  return c.invertedCoordinate();
}


} // namespace Base

