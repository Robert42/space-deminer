#ifndef BASE_CIRCLE_INL
#define BASE_CIRCLE_INL

#include "circle.h"
#include "rectangle.h"

namespace Base {


template<class Tvec2>
bool Circle::contains(const Rectangle<Tvec2>& rectangle) const
{
  return contains(vec2(rectangle.corner(0)))
      && contains(vec2(rectangle.corner(1)))
      && contains(vec2(rectangle.corner(2)))
      && contains(vec2(rectangle.corner(3)));
}


template<class Tvec2>
bool Circle::contains(const Rectangle<Tvec2>& rectangle, real epsilon) const
{
  return contains(rectangle.expandedRect(epsilon));
}


template<class Tvec2>
bool Circle::intersects(const Rectangle<Tvec2>& rectangle) const
{
  return rectangle.distanceTo(centerPosition()) <= radius();
}


} // namespace Base

#endif // BASE_CIRCLE_INL
