#include "rounded-rectangle.h"

namespace Base {


RoundedRectangle::RoundedRectangle(const Rectangle<vec2> &rectangle, real radius)
  : _rectangle(rectangle), _radius(radius)
{
}


void RoundedRectangle::setRectangle(const Rectangle<vec2>& rectangle)
{
  if(this->rectangle() != rectangle)
  {
    _rectangle = rectangle;
    setRadius(radius());
  }
}

void RoundedRectangle::setRadius(real radius)
{
  radius = clamp(radius, 0.f, min(rectangle().width(), rectangle().height())*.5f);
  if(this->radius() != radius)
  {
    _radius = radius;
  }
}


const Rectangle<vec2>& RoundedRectangle::rectangle() const
{
  return _rectangle;
}

real RoundedRectangle::radius() const
{
  return _radius;
}


bool RoundedRectangle::contains(const vec2& point) const
{
  return nearestCircleTo(point).contains(point);
}

real RoundedRectangle::distanceTo(const vec2& point) const
{
  return nearestCircleTo(point).distanceTo(point);
}

vec2 RoundedRectangle::nearestPointTo(const vec2& point) const
{
  return nearestCircleTo(point).nearestPointTo(point);
}


bool RoundedRectangle::intersects(const Rectangle<vec2>& rectangle) const
{
  if(!this->_rectangle.intersects(rectangle))
    return false;
  if(innerRectangle().intersects(rectangle))
    return true;

  vec2 nearestPointOfOtherRectangle = rectangle.nearestPointTo(this->_rectangle.centerPoint());

  return this->contains(nearestPointOfOtherRectangle);
}


Circle RoundedRectangle::nearestCircleTo(const vec2& point) const
{
  return Circle(innerRectangle().nearestPointTo(point),
                radius());
}

Rectangle<vec2> RoundedRectangle::innerRectangle() const
{
  return rectangle().expandedRect(-radius());
}


} // namespace Base
