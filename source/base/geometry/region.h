#ifndef BASE_REGION_H
#define BASE_REGION_H

#include "circle.h"
#include "rectangle.h"
#include "rounded-rectangle.h"

namespace Base {
namespace Geometry {

/**
   * @note Note that a difference of sets is not supported, otherwise checking, whether
   * a region is intersecting with simple shapes wouldn't be possible anymore in a straight
   * forward way.
   */
class Region
{
public:
  typedef std::shared_ptr<Region> Ptr;
  typedef std::shared_ptr<const Region> ConstPtr;

protected:
  Region();

public:
  virtual ~Region();

  static Ptr createEmptyRegion();
  static Ptr createCirclarRegion(const Circle& circle);
  static Ptr createRectangularRegion(const Rectangle<vec2>& rectangle);
  static Ptr createRoundedRectangelRegion(const RoundedRectangle& rectangle);
  static Ptr createUnitedRegions(const QVector<Ptr>& regions);
  static Ptr createIntersectedRegions(const QVector<Ptr>& regions);

  virtual bool contains(const vec2& point) const = 0;
  virtual real distanceTo(const vec2& point) const = 0;
  virtual bool intersects(const Rectangle<vec2>& rectangle) const = 0;
  virtual vec2 nearestPointTo(const vec2& point) const = 0;
  virtual bool empty() const;
  bool intersects(const Circle& circle) const;
};

} // namespace Geometry
} // namespace Base

#endif // BASE_REGION_H
