#ifndef BASE_GEOMETRY_CAMERAGEOMETRY_H
#define BASE_GEOMETRY_CAMERAGEOMETRY_H

#include "ray.h"
#include "coordinate.h"
#include "rectangle.h"

#include <base/signals/signal.h>

namespace Base {
namespace Geometry {

class Camera final
{
private:
  Signals::CallableSignal<void()> _signalInvalidated;

  real _fovX, _fovY;
  real _zNear, _zFar;
  Rectangle<vec2> _viewport;
  bool _flipY : 1;
  bool _useFovX : 1;

public:
  Coordinate coordinate;

public:
  Camera();

  real fovX() const;
  real fovY() const;
  vec2 fov() const;
  real zNear() const;
  real zFar() const;
  const Rectangle<vec2>& viewport() const;
  bool flipY() const;

  void setFovX(real fovX);
  void setFovY(real fovY);
  void setZNear(real zNear);
  void setZFar(real zFar);
  void setViewport(const Rectangle<vec2>& viewport);
  void setFlipY(bool flipY);

  mat4 viewportMatrix() const;
  mat4 viewMatrix() const;
  mat4 projectionMatrix() const;

  void invalidate();

  Ray rayForViewportPosition(const vec2& point) const;
  vec2 normalizePointInViewport(const vec2& point) const;

  Signals::Signal<void()>& signalInvalidated();

private:
  static real fovYForFovX(real fovX, const vec2& viewport);
  static real fovXForFovY(real fovY, const vec2& viewport);

  void _setFovY(real fovY);
  void _setFovX(real fovX);
  void updateOtherFovDimension();
};

} // namespace Geometry
} // namespace Base

#endif // BASE_GEOMETRY_CAMERAGEOMETRY_H
