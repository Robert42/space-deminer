#include "aabb.h"

namespace Base {
namespace Geometry {

AABB::AABB(const vec3& minCorner, const vec3& maxCorner)
  : minCorner(minCorner),
    maxCorner(maxCorner)
{
}

} // namespace Geometry
} // namespace Base
