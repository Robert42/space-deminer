#include "circle.h"

namespace Base {


Circle::Circle(const vec2& centerPosition, real radius)
  : _centerPosition(centerPosition),
    _radius(max(0.f, radius))
{
}


const vec2& Circle::centerPosition() const
{
  return _centerPosition;
}


real Circle::radius() const
{
  return _radius;
}


bool Circle::contains(const vec2& point) const
{
  return length(point - centerPosition()) <= radius();
}


Rectangle<vec2> Circle::boundingRect() const
{
  return Rectangle<vec2>(centerPosition()-radius(),
                         centerPosition()+radius());
}


vec2 Circle::clamp(const vec2& point) const
{
  return constrainPointDistance(point,
                                [this](real distance){
    return glm::clamp<real>(distance, 0.f, radius());
  });
}


vec2 Circle::wraparound(const vec2& point) const
{
  return constrainPointDistance(point,
                                [this](real distance){
    return Interval<real>(-radius(), radius()).wraparound(distance);
  });
}


vec2 Circle::nearestPointTo(const vec2& point) const
{
  return clamp(point);
}


vec2 Circle::constrainPointDistance(const vec2& point, const std::function<real(real)>& constraint) const
{
  real distance = length(point-centerPosition());

  vec2 direction;

  if(distance > 0.f)
    direction = (point-centerPosition())/distance;
  else
    direction = vec2(1, 0);

  return constraint(distance) * direction + centerPosition();
}


Circle Circle::expandedCircle(real space) const
{
  return Circle(centerPosition(), radius() + space);
}


real Circle::circumference() const
{
  return radius() * two_pi;
}


bool Circle::intersects(const Circle& other) const
{
  real distanceOfCenters = length(other.centerPosition() - this->centerPosition());

  return distanceOfCenters <= other.radius()+this->radius();
}


real Circle::distanceTo(const vec2& point) const
{
  return max(0.f, length(point - centerPosition()) - radius());
}


vec2 Circle::edgePointForAngle(real angle) const
{
  return this->centerPosition() + this->radius() * directionForAngle(angle);
}


vec2 Circle::edgePointForAngle(real angle, const vec2& scale) const
{
  return this->centerPosition() + this->radius() * scale * directionForAngle(angle);
}


vec2 Circle::directionForAngle(real angle)
{
  return vec2(cos(angle), sin(angle));
}


} // namespace Base
