#include "matrix4-helper.h"

namespace Base {
namespace Geometry {


vec3 transform(const mat4& m, const vec4& v)
{
  vec4 t = m * v;

  if(t.w)
    return t.xyz() / t.w;
  else
    return t.xyz();
}

vec3 transformVertex(const mat4& m, const vec3& v)
{
  return transform(m, vec4(v, 1));
}


vec3 transformDirection(const mat4& m, const vec3& v)
{
  return transform(m, vec4(v, 0));
}


vec3 operator*(const mat4& m, const vec3& v)
{
  return transformVertex(m, v);
}


real anisotropicAntialiasingRadiusAtVertex(const mat4& m,
                                           const vec3& v,
                                           const vec2& antialiasingDirection)
{
  const vec3 t = transformVertex(m, v);
  const vec3 x = transformVertex(m, v+vec3(1, 0, 0));
  const vec3 y = transformVertex(m, v+vec3(0, 1, 0));

  const vec2 screenSize = vec2(distance(t, x),
                               distance(t, y));

  const vec2 anisotropicFactor = min(abs(antialiasingDirection / screenSize),
                                     vec2(1.e5f));

  return anisotropicFactor.x + anisotropicFactor.y;
}


} // namespace Geometry
} // namespace Base
