#include "interval.h"

namespace Base {


TEST(base_Interval, wraparound)
{
  EXPECT_FLOAT_EQ(23.f, Interval<real>(23.f, 42.f).wraparound(23.f));
  EXPECT_FLOAT_EQ(23.f, Interval<real>(23.f, 42.f).wraparound(42.f));
  EXPECT_FLOAT_EQ(29.f, Interval<real>(23.f, 42.f).wraparound(48.f));
  EXPECT_FLOAT_EQ(39.f, Interval<real>(23.f, 42.f).wraparound(20.f));
  EXPECT_FLOAT_EQ(28.f, Interval<real>(-16.f, 32.f).wraparound(-20.f));
  EXPECT_FLOAT_EQ(0.f, Interval<real>(-16.f, 32.f).wraparound(-48.f));
}


} // namespace Base
