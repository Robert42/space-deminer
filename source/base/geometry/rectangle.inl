#ifndef BASE_RECTANGLE_INL
#define BASE_RECTANGLE_INL

#include "rectangle.h"
#include "interval.h"
#include "circle.h"
#include "line.h"
#include "matrix3-transformations.h"
#include "utilities.h"

#include <base/interpolation.h>

namespace Base {

template<typename Tvec2>
Rectangle<Tvec2>::Rectangle(const Tvec2& a, const Tvec2& b)
  : _min(glm::min(a, b)),
    _max(glm::max(a, b))
{
}

template<typename Tvec2>
Rectangle<Tvec2>::Rectangle()
  : Rectangle(Tvec2(0), Tvec2(1))
{
}


template<typename Tvec2>
void Rectangle<Tvec2>::move(const Tvec2& offset)
{
  _min += offset;
  _max += offset;
}


template<typename Tvec2>
typename Rectangle<Tvec2>::value_type Rectangle<Tvec2>::width() const
{
  return max().x-min().x;
}


template<typename Tvec2>
typename Rectangle<Tvec2>::value_type Rectangle<Tvec2>::height() const
{
  return max().y-min().y;
}


template<typename Tvec2>
Tvec2 Rectangle<Tvec2>::size() const
{
  return max()-min();
}


template<>
inline real Rectangle<vec2>::aspectRatio() const
{
  return width() / height();
}


template<typename Tvec2>
const Tvec2& Rectangle<Tvec2>::min() const
{
  return _min;
}


template<typename Tvec2>
const Tvec2& Rectangle<Tvec2>::max() const
{
  return _max;
}


template<typename Tvec2>
Tvec2 Rectangle<Tvec2>::corner(int index) const
{
  index = mod(index, 4);

  switch(index)
  {
  case 0:
    return min();
  case 1:
    return Tvec2(min().x, max().y);
  case 2:
    return max();
  case 3:
    return Tvec2(max().x, min().y);
  default:
    return min();
  }
}


template<typename Tvec2>
int Rectangle<Tvec2>::indexOfNearestCornerTo(const vec2& point) const
{
  real distance = INFINITY;
  int min = 0;

  for(int i=0; i<4; ++i)
  {
    real d = length(point - corner(i));

    if(distance >= d)
    {
      min = i;
      distance = d;
    }
  }

  return min;
}


template<typename Tvec2>
void Rectangle<Tvec2>::twoNearestEdgesTo(const Output<Line<Tvec2>>& e1, const Output<Line<Tvec2>>& e2, const vec2& point) const
{
  int i = indexOfNearestCornerTo(point);

  Tvec2 a = corner(i-1);
  Tvec2 b = corner(i);
  Tvec2 c = corner(i+1);

  e1.value = Line<Tvec2>(a, b);
  e2.value = Line<Tvec2>(b, c);
}


template<typename Tvec2>
Tvec2 Rectangle<Tvec2>::centerPoint() const
{
  return (min() + max()) * 0.5f;
}


template<typename Tvec2>
real Rectangle<Tvec2>::distanceTo(const Tvec2& point) const
{
  if(contains(point))
    return 0.f;

  Line<Tvec2> a, b;

  twoNearestEdgesTo(out(a), out(b), point);

  return glm::min(a.distanceTo(point),
                  b.distanceTo(point));
}


template<typename Tvec2>
vec2 Rectangle<Tvec2>::nearestPointTo(const Tvec2& point) const
{
  if(contains(point))
    return point;

  Line<Tvec2> a, b;

  twoNearestEdgesTo(out(a), out(b), point);

  vec2 candidateA = a.nearestPointTo(point);
  vec2 candidateB = b.nearestPointTo(point);

  if(squareDistance(candidateA, point) < squareDistance(candidateB, point))
    return candidateA;
  else
    return candidateB;
}


template<typename Tvec2>
bool Rectangle<Tvec2>::operator==(const Rectangle<Tvec2>& other) const
{
  return this->min() == other.min() && this->max()==other.max();
}


template<typename Tvec2>
bool Rectangle<Tvec2>::operator!=(const Rectangle<Tvec2>& other) const
{
  return !(*this == other);
}


template<typename Tvec2>
Rectangle<Tvec2>& Rectangle<Tvec2>::operator-=(Tvec2 offset)
{
  this->move(-offset);
  return *this;
}


template<typename Tvec2>
Rectangle<Tvec2>& Rectangle<Tvec2>::operator+=(Tvec2 offset)
{
  this->move(offset);
  return *this;
}


template<typename Tvec2>
Rectangle<Tvec2> Rectangle<Tvec2>::operator-(const Tvec2& offset) const
{
  Rectangle<Tvec2> result = *this;
  result -= offset;
  return result;
}


template<typename Tvec2>
Rectangle<Tvec2> Rectangle<Tvec2>::operator+(const Tvec2& offset) const
{
  Rectangle<Tvec2> result = *this;
  result += offset;
  return result;
}


template<typename Tvec2>
const Rectangle<Tvec2>& Rectangle<Tvec2>::operator|=(const Rectangle<Tvec2>& other)
{
  this->_min = glm::min(this->min(), other.min());
  this->_max = glm::max(this->max(), other.max());

  return *this;
}


template<typename Tvec2>
Rectangle<Tvec2> Rectangle<Tvec2>::operator|(const Rectangle<Tvec2>& other) const
{
  Rectangle<Tvec2> result = *this;
  result |= other;
  return result;
}


template<typename Tvec2>
const Rectangle<Tvec2>& Rectangle<Tvec2>::operator&=(const Rectangle<Tvec2>& other)
{
  this->_min = glm::max(this->min(), other.min());
  this->_max = glm::min(this->max(), other.max());

  return *this;
}


template<typename Tvec2>
Rectangle<Tvec2> Rectangle<Tvec2>::operator&(const Rectangle<Tvec2>& other) const
{
  Rectangle<Tvec2> result = *this;
  result &= other;
  return result;
}


template<typename Tvec2>
Optional< Rectangle<Tvec2> > Rectangle<Tvec2>::intersection(const Rectangle<Tvec2>& other) const
{
  if(!this->intersects(other))
    return nothing;

  return *this & other;
}


template<typename Tvec2>
bool Rectangle<Tvec2>::contains(const Tvec2& point) const
{
  return xInterval().contains(point.x)
      && yInterval().contains(point.y);
}


template<typename Tvec2>
bool Rectangle<Tvec2>::contains(const Rectangle<Tvec2>& other) const
{
  return contains(other.min()) && contains(other.max());
}


template<typename Tvec2>
bool Rectangle<Tvec2>::contains(const Rectangle<Tvec2>& other, value_type epsilon) const
{
  return contains(other.expandedRect(epsilon));
}


template<typename Tvec2>
bool Rectangle<Tvec2>::contains(const Circle& circle) const
{
  return contains(circle.boundingRect().cast<Tvec2>());
}


template<typename Tvec2>
bool Rectangle<Tvec2>::intersects(const Circle& circle) const
{
  return circle.intersects(*this);
}


template<typename Tvec2>
bool Rectangle<Tvec2>::intersects(const Rectangle<Tvec2>& other) const
{
  return xInterval().intersects(other.xInterval())
      && yInterval().intersects(other.yInterval());
}


template<typename Tvec2>
Tvec2 Rectangle<Tvec2>::clamp(const Tvec2& point) const
{
  return glm::clamp(point, this->min(), this->max());
}


template<typename Tvec2>
Tvec2 Rectangle<Tvec2>::wraparound(const Tvec2& point) const
{
  return mod(point - min(), size()) + min();
}


template<typename Tvec2>
real Rectangle<Tvec2>::diagonalLength() const
{
  return length(max()-min());
}


template<typename Tvec2>
Rectangle<Tvec2> Rectangle<Tvec2>::expandedRect(value_type space) const
{
  return Rectangle<Tvec2>(min()-space, max()+space);
}

template<typename Tvec2>
Rectangle<Tvec2> Rectangle<Tvec2>::absoluteSubRect(vec2 offset, const vec2& size, bool wraparoundPosition) const
{
  if(wraparoundPosition)
    offset = mod(offset, this->size());

  return Rectangle<Tvec2>(offset, offset+size) + this->min();
}

template<typename Tvec2>
Rectangle<Tvec2> Rectangle<Tvec2>::relativeSubRect(const Rectangle<vec2>& relativeCoordinates) const
{
  return Rectangle<Tvec2>(interpolate(relativeCoordinates.min(), this->min(), this->max()),
                          interpolate(relativeCoordinates.max(), this->min(), this->max()));
}


template<typename Tvec2>
typename Rectangle<Tvec2>::Interval Rectangle<Tvec2>::xInterval() const
{
  return Interval(min().x, max().x);
}


template<typename Tvec2>
typename Rectangle<Tvec2>::Interval Rectangle<Tvec2>::yInterval() const
{
  return Interval(min().y, max().y);
}


template<>
inline mat3 Rectangle<vec2>::mappingMatrixTo(const Rectangle<vec2>& target, bool flipY) const
{
  const vec2 sourceCenter = this->centerPoint();
  const vec2 targetCenter = target.centerPoint();

  vec2 sourceSize = this->size();
  const vec2 targetSize = target.size();

  vec2 invSourceSize;

  if(sourceSize.x == 0)
    invSourceSize.x = 0;
  else
    invSourceSize.x = 1.f / sourceSize.x;
  if(sourceSize.y == 0)
    invSourceSize.y = 0;
  else
    invSourceSize.y = 1.f / sourceSize.y;

  if(flipY)
    invSourceSize.y = -invSourceSize.y;

  return Geometry::translate3(targetCenter) * Geometry::scale3(targetSize*invSourceSize) * Geometry::translate3(-sourceCenter);
}

template<typename Tvec2>
Rectangle<Tvec2> Rectangle<Tvec2>::alignedRectangleWithAspectRatio(const vec2& aspectRatio, const Rectangle<Tvec2>& target, bool expand)
{
  return target.relativeAllocation(fitSizeKeepingAspectRatio(aspectRatio,
                                                             target.size(),
                                                             expand));
}


template<typename Tvec2>
Rectangle<vec2> Rectangle<Tvec2>::relativeAllocation(const vec2& sourceSize, const vec2& alignment, const vec2& expand) const
{
  const vec2 targetSize = this->size();
  const vec2 freeSpace = targetSize - sourceSize;
  const vec2 newSize = sourceSize + freeSpace*expand;
  const vec2 newPosition = (targetSize - newSize) * alignment;

  return Rectangle<Tvec2>(Tvec2(0), newSize) + newPosition + this->min();
}


inline Rectangle<vec2> operator*(const mat3& m, const Rectangle<vec2>& r)
{
  const vec2 a = m*r.corner(0);
  const vec2 b = m*r.corner(1);
  const vec2 c = m*r.corner(2);
  const vec2 d = m*r.corner(3);

  return Rectangle<vec2>(min(min(a, b), min(c, d)),
                         max(max(a, b), max(c, d)));
}


template<typename Tvec2, typename T>
std::basic_ostream<T>& operator<<(std::basic_ostream<T>& o, const Rectangle<Tvec2>& rectangle)
{
  return o << "Rectangle<"<< (std::is_same<Tvec2, vec2>::value?"vec2":"ivec2") << ">(" << rectangle.min() <<",   " << rectangle.max() << ")";
}


template<typename Tvec2>
void PrintTo(const Rectangle<Tvec2>& rectangle, std::ostream* o)
{
  *o << rectangle;
}

template<typename Tvec2>
Rectangle<Tvec2> round(const Rectangle<Tvec2>& rectangle)
{
  return Rectangle<Tvec2>(round(rectangle.min()),
                          round(rectangle.max()));
}

template<typename Tvec2>
Rectangle<Tvec2> floor(const Rectangle<Tvec2>& rectangle)
{
  return Rectangle<Tvec2>(round(rectangle.min()),
                          round(rectangle.max()));
}

template<typename Tvec2>
Rectangle<Tvec2> ceil(const Rectangle<Tvec2>& rectangle)
{
  return Rectangle<Tvec2>(ceil(rectangle.min()),
                          ceil(rectangle.max()));
}


} // namespace Base

#endif // BASE_RECTANGLE_INL
