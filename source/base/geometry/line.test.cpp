#include "line.h"

namespace Base {


TEST(base_Line, getNearestPointTo)
{
  Line<vec2> line1(vec2(1,1), vec2(5,1));
  Line<vec2> line2(vec2(5,1), vec2(1,1));

  vec2 p_1_1_a(-1, 1);
  vec2 p_1_1_b(-1, 3);
  vec2 p_1_1_c( 1, 3);
  vec2 p_1_1_d( 1, 1);
  vec2 p_2_1_a( 2, 3);
  vec2 p_3_1_a( 3,-1);
  vec2 p_5_1_a( 5, 1);
  vec2 p_5_1_b( 5, 2);
  vec2 p_5_1_c( 6, 2);
  vec2 p_5_1_d( 6, 1);
  vec2 p_5_1_e( 6,-1);

  EXPECT_EQ(vec2(1, 1), line1.nearestPointTo(p_1_1_a));
  EXPECT_EQ(vec2(1, 1), line1.nearestPointTo(p_1_1_b));
  EXPECT_EQ(vec2(1, 1), line1.nearestPointTo(p_1_1_c));
  EXPECT_EQ(vec2(1, 1), line1.nearestPointTo(p_1_1_d));
  EXPECT_EQ(vec2(2, 1), line1.nearestPointTo(p_2_1_a));
  EXPECT_EQ(vec2(3, 1), line1.nearestPointTo(p_3_1_a));
  EXPECT_EQ(vec2(5, 1), line1.nearestPointTo(p_5_1_a));
  EXPECT_EQ(vec2(5, 1), line1.nearestPointTo(p_5_1_b));
  EXPECT_EQ(vec2(5, 1), line1.nearestPointTo(p_5_1_c));
  EXPECT_EQ(vec2(5, 1), line1.nearestPointTo(p_5_1_d));
  EXPECT_EQ(vec2(5, 1), line1.nearestPointTo(p_5_1_e));

  EXPECT_EQ(vec2(1, 1), line2.nearestPointTo(p_1_1_a));
  EXPECT_EQ(vec2(1, 1), line2.nearestPointTo(p_1_1_b));
  EXPECT_EQ(vec2(1, 1), line2.nearestPointTo(p_1_1_c));
  EXPECT_EQ(vec2(1, 1), line2.nearestPointTo(p_1_1_d));
  EXPECT_EQ(vec2(2, 1), line2.nearestPointTo(p_2_1_a));
  EXPECT_EQ(vec2(3, 1), line2.nearestPointTo(p_3_1_a));
  EXPECT_EQ(vec2(5, 1), line2.nearestPointTo(p_5_1_a));
  EXPECT_EQ(vec2(5, 1), line2.nearestPointTo(p_5_1_b));
  EXPECT_EQ(vec2(5, 1), line2.nearestPointTo(p_5_1_c));
  EXPECT_EQ(vec2(5, 1), line2.nearestPointTo(p_5_1_d));
  EXPECT_EQ(vec2(5, 1), line2.nearestPointTo(p_5_1_e));
}


} // namespace Base
