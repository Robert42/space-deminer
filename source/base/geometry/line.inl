#ifndef BASE_LINE_INL
#define BASE_LINE_INL

#include "line.h"
#include "rectangle.h"

namespace Base {


template<typename Tvec2>
Line<Tvec2>::Line(const Tvec2& a, const Tvec2& b)
  : _a(a), _b(b)
{
}


template<typename Tvec2>
const Tvec2& Line<Tvec2>::a() const
{
  return _a;
}


template<typename Tvec2>
const Tvec2& Line<Tvec2>::b() const
{
  return _b;
}


template<typename Tvec2>
real Line<Tvec2>::length() const
{
  return Base::length(vec2(b())-vec2(a()));
}


template<typename Tvec2>
vec2 Line<Tvec2>::nearestPointTo(const Tvec2& point) const
{
  real len = length();

  if(len == 0.f)
    return a();

  vec2 lineDirection = (vec2(b())-vec2(a())) / len;

  real t = dot(lineDirection, vec2(point - a()));

  t = clamp(t, 0.f, len);

  return a() + lineDirection*t;
}


template<typename Tvec2>
real Line<Tvec2>::distanceTo(const Tvec2& point) const
{
  return Base::length(point - nearestPointTo(point));
}


template<typename Tvec2>
Rectangle<Tvec2> Line<Tvec2>::boundingRect() const
{
  return Rectangle<Tvec2>(a(), b());
}


} // namespace Base

#endif
