#include "utilities.h"

namespace Base {


TEST(base_geometry_utilities, fitSizeKeepingAspectRatio)
{
  EXPECT_EQ(vec2(100), fitSizeKeepingAspectRatio(vec2(1), vec2(200, 100), false));
  EXPECT_EQ(vec2(200), fitSizeKeepingAspectRatio(vec2(1), vec2(200, 100), true));
  EXPECT_EQ(vec2(100), fitSizeKeepingAspectRatio(vec2(1), vec2(100, 200), false));
  EXPECT_EQ(vec2(200), fitSizeKeepingAspectRatio(vec2(1), vec2(100, 200), true));

  EXPECT_EQ(vec2(100, 400) * 0.25f, fitSizeKeepingAspectRatio(vec2(1, 4), vec2(200, 100), false));
  EXPECT_EQ(vec2(100, 400) * 2.00f, fitSizeKeepingAspectRatio(vec2(1, 4), vec2(200, 100), true));
  EXPECT_EQ(vec2(100, 400) * 0.50f, fitSizeKeepingAspectRatio(vec2(1, 4), vec2(100, 200), false));
  EXPECT_EQ(vec2(100, 400) * 1.00f, fitSizeKeepingAspectRatio(vec2(1, 4), vec2(100, 200), true));

  EXPECT_EQ(vec2(400, 100) * 0.50f, fitSizeKeepingAspectRatio(vec2(4, 1), vec2(200, 100), false));
  EXPECT_EQ(vec2(400, 100) * 1.00f, fitSizeKeepingAspectRatio(vec2(4, 1), vec2(200, 100), true));
  EXPECT_EQ(vec2(400, 100) * 0.25f, fitSizeKeepingAspectRatio(vec2(4, 1), vec2(100, 200), false));
  EXPECT_EQ(vec2(400, 100) * 1.00f, fitSizeKeepingAspectRatio(vec2(4, 1), vec2(100, 100), true));
}


} // namespace Base
