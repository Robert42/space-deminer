#ifndef BASE_GEOMETRY_RAY_H
#define BASE_GEOMETRY_RAY_H

#include <base/optional.h>

namespace Base {

class Coordinate;

namespace Geometry {

class Plane;
class AABB;
class Ray
{
public:
  vec3 origin;
  vec3 direction;

public:
  Ray(const vec3& origin, const vec3& direction);

  static Ray fromTwoPoints(const vec3& origin, const vec3& target);

  Optional<real> signedDistanceToPlane(const Plane& plane) const;
  Optional<vec3> intersectionWithPlane(const Plane& plane) const;

  vec3 pointAlongRay(real t) const;

  Optional<real> signedDistanceToAxisPlane(real planePosition, int dimension) const;
  Optional<vec3> intersectionWithAxisPlane(real planePosition, int dimension) const;
  Optional<vec3> intersectionWithZPlane(real zPlane) const;
};

std::ostream& operator<<(std::ostream& s, const Ray& ray);
void PrintTo(const Ray& ray, std::ostream* o);

Ray operator*(const mat4& m, const Ray& ray);
Ray operator*(const Coordinate& coordinate, const Ray& ray);


} // namespace Geometry
} // namespace Base

#endif // BASE_GEOMETRY_RAY_H
