#ifndef BASE_INTERVAL_INL
#define BASE_INTERVAL_INL

#include "interval.h"

namespace Base {

template<typename T>
Interval<T>::Interval(T a, T b)
  : _min(Base::min(a, b)),
    _max(Base::max(a, b))
{
}


template<typename T>
T Interval<T>::min() const
{
  return this->_min;
}


template<typename T>
T Interval<T>::max() const
{
  return this->_max;
}


template<typename T>
T Interval<T>::length() const
{
  if(std::is_integral<T>::value)
    return max() - min() + 1;
  else
    return max() - min();
}


template<typename T>
bool Interval<T>::contains(T x) const
{
  return min()<=x && max()>=x;
}


template<typename T>
bool Interval<T>::intersects(const Interval<T>& other) const
{
  return this->contains(other.min()) ||
         this->contains(other.max()) ||
         other.contains(this->min()) ||
         other.contains(this->max());
}


template<typename T>
T Interval<T>::wraparound(T x) const
{
  return mod(x-min(), max()-min()) + min();
}



} // namespace Base

#endif // BASE_INTERVAL_INL
