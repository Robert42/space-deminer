#ifndef BASE_GEOMETRY_PLANE_H
#define BASE_GEOMETRY_PLANE_H

#include <base/optional.h>

namespace Base {
namespace Geometry {

class Plane
{
public:
  vec3 normal;
  real d;

public:
  Plane(const vec3& normal, real d);
};

} // namespace Geometry
} // namespace Base

#endif // BASE_GEOMETRY_PLANE_H
