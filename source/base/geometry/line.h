#ifndef BASE_LINE_H
#define BASE_LINE_H

#include <dependencies.h>

namespace Base {

template<typename Tvec2>
class Rectangle;

template<typename Tvec2>
class Line
{
  static_assert(std::is_same<Tvec2, vec2>::value || std::is_same<Tvec2, ivec2>::value,
                "Tvec2 is only allowed to be a vec2 or ivec2");
public:
  typedef typename Tvec2::value_type value_type;

private:
  Tvec2 _a, _b;


public:
  Line(const Tvec2& a=Tvec2(0), const Tvec2& b=Tvec2(1));

  const Tvec2& a() const;
  const Tvec2& b() const;

  real length() const;

  vec2 nearestPointTo(const Tvec2& point) const;
  real distanceTo(const Tvec2& point) const;

  Rectangle<Tvec2> boundingRect() const;
};

} // namespace Base

#include "line.inl"

#endif // BASE_LINE_H
