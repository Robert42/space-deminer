#include "matrix3-transformations.h"

namespace Base {
namespace Geometry {

real sinDiscrete(int angle);
real cosDiscrete(int angle);

TEST(base_geometry, discrete_sin_cos)
{
  EXPECT_EQ( 0, sinDiscrete(0));
  EXPECT_EQ( 1, sinDiscrete(1));
  EXPECT_EQ( 0, sinDiscrete(2));
  EXPECT_EQ(-1, sinDiscrete(3));

  EXPECT_EQ( 1, cosDiscrete(0));
  EXPECT_EQ( 0, cosDiscrete(1));
  EXPECT_EQ(-1, cosDiscrete(2));
  EXPECT_EQ( 0, cosDiscrete(3));
}


} // namespace Geometry
} // namespace Base
