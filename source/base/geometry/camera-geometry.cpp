#include "camera-geometry.h"

namespace Base {
namespace Geometry {


Camera::Camera()
  : _fovX(radians(80.f)),
    _fovY(radians(80.f)),
    _zNear(0.01f),
    _zFar(1000.f),
    _viewport(vec2(-1), vec2(1)),
    _flipY(false),
    _useFovX(false)
{
}


real Camera::fovX() const
{
  return _fovX;
}

real Camera::fovY() const
{
  return _fovY;
}

vec2 Camera::fov() const
{
  return vec2(_fovX, _fovY);
}

real Camera::zNear() const
{
  return _zNear;
}

real Camera::zFar() const
{
  return _zFar;
}

const Rectangle<vec2>& Camera::viewport() const
{
  return _viewport;
}

bool Camera::flipY() const
{
  return _flipY;
}

void Camera::setFovX(real fovX)
{
  _useFovX = true;
  _setFovX(fovX);
  updateOtherFovDimension();
}

void Camera::setFovY(real fovY)
{
  _useFovX = false;
  _setFovY(fovY);
  updateOtherFovDimension();
}

void Camera::updateOtherFovDimension()
{
  if(_useFovX)
  {
    _setFovY(fovYForFovX(fovX(), viewport().size()));
  }else
  {
    _setFovX(fovXForFovY(fovY(), viewport().size()));
  }
}

void Camera::_setFovY(real fovY)
{
  if(this->fovY() != fovY)
  {
    this->_fovY = fovY;

    invalidate();
  }
}

void Camera::_setFovX(real fovX)
{
  if(this->fovX() != fovX)
  {
    this->_fovX = fovX;

    invalidate();
  }
}

void Camera::setZNear(real zNear)
{
  if(this->zNear() != zNear)
  {
    this->_zNear = zNear;

    invalidate();
  }
}

void Camera::setZFar(real zFar)
{
  if(this->zFar() != zFar)
  {
    this->_zFar = zFar;

    invalidate();
  }
}

void Camera::setViewport(const Rectangle<vec2>& viewport)
{
  if(this->viewport() != viewport)
  {
    this->_viewport = viewport;
    updateOtherFovDimension();

    invalidate();
  }
}

void Camera::setFlipY(bool flipY)
{
  if(this->flipY() != flipY)
  {
    this->_flipY = flipY;

    invalidate();
  }
}


vec2 Camera::normalizePointInViewport(const vec2& point) const
{
  const vec2 translate = -viewport().centerPoint();
  const vec2 viewportSize = viewport().size();
  vec2 scale = vec2(2.f) / viewportSize;

  if(flipY())
    scale.y = -scale.y;

  return (point + translate) * scale;
}


mat4 Camera::viewportMatrix() const
{
  const vec2 viewportSize = viewport().size();
  const vec3 scale = vec3(viewportSize * vec2(0.5f, 0.5f), 1.f);

  mat4 m = glm::scale(glm::translate(mat4(1),
                                     vec3(viewport().centerPoint(), 0)),
                      scale);

  if(flipY())
  {
    return glm::scale(m,
                      vec3(1, -1, 1));
  }else
  {
    return m;
  }
}

mat4 Camera::projectionMatrix() const
{
  return glm::perspectiveFov(fovY(),
                             max(1.f, viewport().width()),
                             max(1.f, viewport().height()),
                             zNear(),
                             zFar());
}

mat4 Camera::viewMatrix() const
{
  return coordinate.invertedCoordinate().toMatrix4x4();
}


void Camera::invalidate()
{
  _signalInvalidated();
}


Ray Camera::rayForViewportPosition(const vec2& point) const
{
  // The origin of the ray if the position of the camera
  vec3 origin = coordinate.position;

  // Map the point within the viewport to a coordinate in -1..1 on each axis.
  // Afterwards map the direction with the tangens of the fov in each dimension, so (1,1), (-1,1),
  // (1,-1), (-1,-1) are mapped to the edgets of the viewing frustrum.
  // Please note, that within the viewport space, each mapping linear.
  vec3 direction(normalizePointInViewport(point) * tan(fov() * 0.5f),
                 -1);
  direction = normalize(direction);

  // Apply the orientation of the camera to the direction
  direction = coordinate.orientation * direction;

  return Ray(origin, direction);
}


Signals::Signal<void()>& Camera::signalInvalidated()
{
  return _signalInvalidated;
}


real Camera::fovYForFovX(real fovX, const vec2& viewport)
{
  return 2.f * atan(viewport.y * tan(0.5f * fovX) / viewport.x);
}

real Camera::fovXForFovY(real fovY, const vec2& viewport)
{
  return 2.f * atan(viewport.x * tan(0.5f * fovY) / viewport.y);
}


} // namespace Geometry
} // namespace Base
