#ifndef BASE_UTILITIES_H
#define BASE_UTILITIES_H

#include "rectangle.h"

namespace Base {


/**
 * @brief alignedRectangle returns a rectangle with the same aspectRatio as the current rectangle scaled up to
 * target.
 *
 * Useful for example for a texture viewer, where the image should can be zooomed to the window size without c
 * hanging the aspect ratio.
 *
 * @param aspectRatio If you want 16/9, you can use `aspectRatio=vec2(16,9)`  or `aspectRatio=vec2(16.f/9.f, 1.f)`
 * @param target
 * @param expand
 */
vec2 fitSizeKeepingAspectRatio(const vec2& aspectRatio, const vec2& targetSize, bool expand);


QPointF toQPointF(const vec2& p);
QPoint toQPoint(const ivec2& p);
QSizeF toQSizeF(const vec2& s);
QSize toQSize(const ivec2& s);
QRectF toQRectF(const Rectangle<vec2>& r);
QRect toQRect(const Rectangle<ivec2>& r);
QRect toQRect(const ivec2& a, const ivec2& b);
QRectF toQRectF(const vec2& a, const vec2& b);

vec2 fromQPointF(const QPointF& p);
ivec2 fromQPoint(const QPoint& p);
vec2 fromQSizeF(const QSizeF& s);
ivec2 fromQSize(const QSize& s);


} // namespace Base

#endif // BASE_UTILITIES_H
