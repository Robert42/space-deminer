#include "circle.h"




namespace Base {


TEST(base_Circle, clamp)
{

  Circle c_0_5(vec2(113, 64), 0.5f);
  Circle c_7(vec2(31, 41), 7.f);
  Circle c_1(vec2(-23, 42), 1.f);
  Circle c_0(vec2(-23, 42), 0.f);

  auto testClamp = [](const Circle& circle, real pointDistance) {
    const vec2 direction = normalize(vec2(13, 7));
    const vec2 point = circle.centerPosition() + direction*pointDistance;
    if(pointDistance>circle.radius())
      EXPECT_VEC2_EQ(circle.centerPosition() + direction*circle.radius(),
                     circle.clamp(point));
    else
      EXPECT_VEC2_EQ(point,
                     circle.clamp(point));
  };

  for(const Circle& circle : {c_0_5, c_7, c_1, c_0})
    for(real f=0.f; f<=11.f; f+=0.1)
      testClamp(circle, f);

}


} // namespace Base
