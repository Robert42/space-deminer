#ifndef _GRAPHIC_LOCALCOORDINATESYSTEM_H_
#define _GRAPHIC_LOCALCOORDINATESYSTEM_H_

#include <dependencies.h>

namespace Base {


class Coordinate
{
public:
  quaternion orientation;
  vec3 position;

public:
  explicit Coordinate(const vec3& position = vec3(0.f),
                      const quaternion& orientation = quaternion_noRotation);

public:
  vec3 localXAxis() const;
  vec3 localYAxis() const;
  vec3 localZAxis() const;

public:
  void move(const vec3& distance);
  void moveX(real distance);
  void moveY(real distance);
  void moveZ(real distance);
  void moveLocal(const vec3& localDistance);
  void moveLocalX(real distance);
  void moveLocalY(real distance);
  void moveLocalZ(real distance);

public:
  void rotate(const quaternion& q);
  void rotate(real angle, const vec3& axis);
  void rotateX(real angle);
  void rotateY(real angle);
  void rotateZ(real angle);
  void rotateLocal(const quaternion& q);
  void rotateLocal(real angle, const vec3& localAxis);
  void rotateLocalX(real angle);
  void rotateLocalY(real angle);
  void rotateLocalZ(real angle);

  vec3 orientationAngles() const;

public:
  Coordinate invertedCoordinate() const;
  Coordinate& invert();

  vec3 operator*(const vec3& v) const;

  Coordinate& operator*=(const Coordinate& c);
  Coordinate operator*(const Coordinate& c) const;

  mat4 toMatrix4x4() const;
  mat4x3 toMatrix4x3() const;

  bool operator==(const Coordinate& coordinate) const;
  bool operator!=(const Coordinate& coordinate) const;

public:
  void applyToOgreSceneNode(Ogre::SceneNode* sceneNode) const;
};


Coordinate inverse(const Coordinate& c);


} // namespace Base

#endif
