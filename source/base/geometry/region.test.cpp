#include "region.h"

namespace Base {
namespace Geometry {


TEST(base_geometry_Region, intersected_empty)
{
  Region::Ptr region = Region::createIntersectedRegions({});

  EXPECT_EQ(INFINITY, region->distanceTo(vec2(0)));
  EXPECT_NE(-INFINITY, region->distanceTo(vec2(0)));
}



TEST(base_geometry_Region, united_empty)
{
  Region::Ptr region = Region::createUnitedRegions({});

  EXPECT_EQ(INFINITY, region->distanceTo(vec2(0)));
  EXPECT_NE(-INFINITY, region->distanceTo(vec2(0)));
}

} // namespace Geometry
} // namespace Base
