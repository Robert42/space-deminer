#ifndef BASE_GEOMETRY_AABB_H
#define BASE_GEOMETRY_AABB_H

#include <dependencies.h>

namespace Base {
namespace Geometry {

class AABB
{
public:
  vec3 minCorner;
  vec3 maxCorner;

public:
  AABB(const vec3& minCorner, const vec3& maxCorner);
};

} // namespace Geometry
} // namespace Base

#endif // BASE_GEOMETRY_AABB_H
