#include "matrix3-transformations.h"

namespace Base {
namespace Geometry {


real sinDiscrete(int angle)
{
  angle = mod(angle, 4);

  return (angle & 1) * ((2-(angle&2))-1);
}

real cosDiscrete(int angle)
{
  return sinDiscrete(angle+1);
}


mat3 translate3(const vec2& t)
{
  return mat3(1.f, 0.f, 0.f,
              0.f, 1.f, 0.f,
              t.x, t.y, 1.f);
}


mat3 scale3(const vec2& s)
{
  return mat3(s.x, 0.f, 0.f,
              0.f, s.y, 0.f,
              0.f, 0.f, 1.f);
}


mat3 rotate3(real angle)
{
  real c = cos(angle);
  real s = sin(angle);

  return mat3x3(  c,   s, 0.f,
                 -s,   c, 0.f,
                0.f, 0.f, 1.f);
}


mat3 rotateDiscrete3(int angle)
{
  real c = cosDiscrete(angle);
  real s = sinDiscrete(angle);

  return mat3x3(  c,   s, 0.f,
                 -s,   c, 0.f,
                0.f, 0.f, 1.f);
}


mat3x2 mat3x3ToMat3x2(const mat3& m)
{
  return mat3x2(m[0].xy(),
                m[1].xy(),
                m[2].xy());
}


vec2 transform(const mat3& m, const vec2& v)
{
  return (m*vec3(v,1)).xy();
}


} // namespace Geometry

vec2 operator*(const mat3& m, const vec2& v)
{
  return Geometry::transform(m, v);
}

} // namespace Base
