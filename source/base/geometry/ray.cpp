#include "ray.h"
#include "plane.h"
#include "aabb.h"
#include "coordinate.h"

#include "matrix4-helper.h"

namespace Base {
namespace Geometry {


Ray::Ray(const vec3& origin, const vec3& direction)
  : origin(origin),
    direction(direction)
{
}

Ray Ray::fromTwoPoints(const vec3& origin, const vec3& target)
{
  return Ray(origin, target-origin);
}


Optional<real> Ray::signedDistanceToPlane(const Plane& plane) const
{
  real D = plane.d;
  vec3 n = plane.normal;
  vec3 o = this->origin;
  vec3 d = this->direction;

  real dot_n_d = dot(n, d);

  if(dot_n_d == 0.f)
    return nothing;
  else
    return (D - dot(n, o)) / dot_n_d;
}

Optional<vec3> Ray::intersectionWithPlane(const Plane& plane) const
{
  Optional<real> signedDistance = signedDistanceToPlane(plane);

  if(signedDistance && *signedDistance>=0.f)
    return pointAlongRay(*signedDistance);
  else
    return nothing;
}


vec3 Ray::pointAlongRay(real t) const
{
  return origin + direction * t;
}


Optional<real> Ray::signedDistanceToAxisPlane(real planePosition, int dimension) const
{
  if(direction[dimension] == 0.f)
    return nothing;

  real D = planePosition;
  real o = this->origin[dimension];
  real d = this->direction[dimension];

  return (D - o) / d;
}


Optional<vec3> Ray::intersectionWithAxisPlane(real planePosition, int dimension) const
{
  Optional<real> signedDistance = signedDistanceToAxisPlane(planePosition, dimension);

  if(signedDistance && *signedDistance>=0.f)
    return pointAlongRay(*signedDistance);
  else
    return nothing;
}


Optional<vec3> Ray::intersectionWithZPlane(real zPlane) const
{
  return intersectionWithAxisPlane(zPlane, 2);
}

std::ostream& operator<<(std::ostream& s, const Ray& ray)
{
  return s << "Ray(origin: "<<ray.origin<<",    direction: "<<ray.direction<<")";
}

void PrintTo(const Ray& ray, std::ostream* o)
{
  (*o) << ray;
}

Ray operator*(const mat4& m, const Ray& ray)
{
  return Ray(transformVertex(m, ray.origin),
             normalize(transformDirection(m, ray.direction)));
}

Ray operator*(const Coordinate& coordinate, const Ray& ray)
{
  return Ray(coordinate * ray.origin,
             coordinate.orientation * ray.direction);
}


} // namespace Geometry
} // namespace Base
