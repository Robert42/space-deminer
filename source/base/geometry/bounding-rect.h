#ifndef BASE_GEOMETRY_BOUNDINGRECT_H
#define BASE_GEOMETRY_BOUNDINGRECT_H

#include <base/geometry/rectangle.h>
#include <base/signals/signal.h>

namespace Base {
namespace Geometry {

class BoundingRect
{
private:
  Rectangle<vec2> _rectangle;
  bool _empty;

  Signals::CallableSignal<void()> _signalChanged;

public:
  BoundingRect();

  const Rectangle<vec2>& rectangle() const;
  bool empty() const;
  Signals::Signal<void()>& signalChanged();

  void set(const Rectangle<vec2>& rectangle);
  void reset();

  bool operator==(const BoundingRect& other)const;
  bool operator!=(const BoundingRect& other)const;

  BoundingRect& operator|=(const BoundingRect& rectangle);
  BoundingRect& operator|=(const Rectangle<vec2>& rectangle);
  BoundingRect& operator|=(const vec2& point);
};


std::ostream& operator<<(std::ostream& o, const BoundingRect& rectangle);

void PrintTo(const BoundingRect& rectangle, std::ostream* o);

} // namespace Geometry
} // namespace Base

#endif // BASE_GEOMETRY_BOUNDINGRECT_H
