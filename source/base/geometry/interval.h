#ifndef BASE_INTERVAL_H
#define BASE_INTERVAL_H

#include <dependencies.h>

namespace Base {

template<typename T>
class Interval
{
  T _min, _max;
public:
  Interval(T a, T b);

  T min() const;
  T max() const;

  /**
   * @return max-min for real types and max-min+1 for integral types
   */
  T length() const;

  bool contains(T x) const;
  bool intersects(const Interval<T>& other) const;
  T wraparound(T x) const;
};

} // namespace Base

#include "interval.inl"

#endif // BASE_INTERVAL_H
