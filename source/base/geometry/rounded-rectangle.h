#ifndef BASE_GEOMETRY_ROUNDEDRECTANGLE_H
#define BASE_GEOMETRY_ROUNDEDRECTANGLE_H

#include "rectangle.h"

namespace Base {

class RoundedRectangle final
{
private:
  Rectangle<vec2> _rectangle;
  real _radius;

public:
  RoundedRectangle(const Rectangle<vec2>& rectangle, real radius);

  void setRectangle(const Rectangle<vec2>& rectangle);
  void setRadius(real radius);
  const Rectangle<vec2>& rectangle() const;
  real radius() const;

  bool contains(const vec2& point) const;
  real distanceTo(const vec2& point) const;
  vec2 nearestPointTo(const vec2& point) const;

  bool intersects(const Rectangle<vec2>& rectangle) const;


  /** Calculatets and returns the nearest circle to the given point.
   *
   * It's guaranteed, hat the circle is completely contained within the rounded rectangles.
   * Also, the circle has the same radius as the roundings.
   *
   * @param point
   * @return a circle
   */
  Circle nearestCircleTo(const vec2& point) const;

private:
  Rectangle<vec2> innerRectangle() const;
};


} // namespace Base

#endif // BASE_GEOMETRY_ROUNDEDRECTANGLE_H
