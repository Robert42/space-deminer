#ifndef BASE_GEOMETRY_MATRIX4HELPER_H
#define BASE_GEOMETRY_MATRIX4HELPER_H

#include <dependencies.h>

namespace Base {
namespace Geometry {

vec3 transform(const mat4& m, const vec4& v);
vec3 transformVertex(const mat4& m, const vec3& v);
vec3 transformDirection(const mat4& m, const vec3& v);

real anisotropicAntialiasingRadiusAtVertex(const mat4& m,
                                           const vec3& v,
                                           const vec2& antialiasingDirection);

} // namespace Geometry

vec3 operator*(const mat4& m, const vec3& v);

} // namespace Base

#endif // BASE_GEOMETRY_MATRIX4HELPER_H
