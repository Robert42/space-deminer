#ifndef BASE_RECTANGLE_H
#define BASE_RECTANGLE_H

#include "interval.h"

#include <base/output.h>
#include <base/optional.h>

namespace Base {


template<typename Tvec2>
class Line;

class Circle;

template<typename Tvec2>
class Rectangle
{
  static_assert(std::is_same<Tvec2, vec2>::value || std::is_same<Tvec2, ivec2>::value,
                "Tvec2 is only allowed to be a vec2 or ivec2");
public:
  typedef typename Tvec2::value_type value_type;
  typedef typename Base::Interval<value_type> Interval;

private:
  Tvec2 _min, _max;

public:
  /** Creates a rectangle using two oposed edgepoints of the rectangle
     *
     * @param a
     * @param b
     */
  Rectangle(const Tvec2& a, const Tvec2& b);
  Rectangle();

  void move(const Tvec2& offset);

  value_type width() const;
  value_type height() const;
  Tvec2 size() const;
  real aspectRatio() const;

  const Tvec2& min() const;
  const Tvec2& max() const;

  /**
     * @brief Returns a corner of the rectangle
     * @param index of the corner.
     * - 0 for NW
     * - 1 for SW
     * - 2 for SE
     * - 3 for NE
     * @return the corner
     */
  Tvec2 corner(int index) const;
  int indexOfNearestCornerTo(const vec2& point) const;
  void twoNearestEdgesTo(const Output<Line<Tvec2>>& e1, const Output<Line<Tvec2>>& e2, const vec2& point) const;

  Tvec2 centerPoint() const;

  real distanceTo(const Tvec2& point) const;
  vec2 nearestPointTo(const Tvec2& point) const;

  bool operator==(const Rectangle<Tvec2>& other) const;
  bool operator!=(const Rectangle<Tvec2>& other) const;

  Rectangle<Tvec2>& operator-=(Tvec2 offset);
  Rectangle<Tvec2>& operator+=(Tvec2 offset);
  Rectangle<Tvec2> operator-(const Tvec2& offset) const;
  Rectangle<Tvec2> operator+(const Tvec2& offset) const;

  const Rectangle<Tvec2>& operator|=(const Rectangle<Tvec2>& other);
  Rectangle<Tvec2> operator|(const Rectangle<Tvec2>& other) const;

  /**
   * @note If the rectangles don't intersect, a rectangle touching both rectangles is returned
   * @param other
   * @return The intersection between both rectangles
   */
  const Rectangle<Tvec2>& operator&=(const Rectangle<Tvec2>& other);
  /**
   * @note If the rectangles don't intersect, a rectangle touching both rectangles is returned
   * @param other
   * @return The intersection between both rectangles
   */
  Rectangle<Tvec2> operator&(const Rectangle<Tvec2>& other) const;
  Optional< Rectangle<Tvec2> > intersection(const Rectangle<Tvec2>& other) const;

  bool contains(const Tvec2& point) const;
  bool contains(const Rectangle<Tvec2>& other) const;
  bool contains(const Rectangle<Tvec2>& other, value_type epsilon) const;
  bool contains(const Circle& circle) const;

  bool intersects(const Rectangle<Tvec2>& other) const;
  bool intersects(const Circle& circle) const;

  Tvec2 clamp(const Tvec2& point) const;
  Tvec2 wraparound(const Tvec2& point) const;

  real diagonalLength() const;
  Rectangle<Tvec2> expandedRect(value_type space) const;

  Rectangle<Tvec2> absoluteSubRect(vec2 offset, const vec2& size, bool wraparoundPosition=false) const;
  Rectangle<Tvec2> relativeSubRect(const Rectangle<vec2>& relativeCoordinates) const;

  Interval xInterval() const;
  Interval yInterval() const;

  template<class TOtherVec2>
  Rectangle<TOtherVec2> cast() const
  {
    return Rectangle<TOtherVec2>(TOtherVec2(min()), TOtherVec2(max()));
  }

  mat3 mappingMatrixTo(const Rectangle<Tvec2>& target, bool flipY) const;

  /**
   * @brief alignedRectangle returns a rectangle with the same aspectRatio as the current rectangle scaled up to
   * target.
   *
   * Useful for example for a texture viewer, where the image should can be zooomed to the window size without c
   * hanging the aspect ratio.
   *
   * @param aspectRatio If you want 16/9, you can use `aspectRatio=vec2(16,9)`  or `aspectRatio=vec2(16.f/9.f, 1.f)`
   * @param target
   * @param expand
   */
  static Rectangle<Tvec2> alignedRectangleWithAspectRatio(const vec2& aspectRatio, const Rectangle<Tvec2>& target, bool expand=false);

  Rectangle<vec2> relativeAllocation(const vec2& sourceSize, const vec2& alignment=vec2(0.5), const vec2& expand=vec2(0.f)) const;
};

Rectangle<vec2> operator*(const mat3& m, const Rectangle<vec2>& r);


template<typename Tvec2, typename T>
std::basic_ostream<T>& operator<<(std::basic_ostream<T>& o, const Rectangle<Tvec2>& rectangle);


template<typename Tvec2>
void PrintTo(const Rectangle<Tvec2>& rectangle, std::ostream* o);

template<typename Tvec2>
Rectangle<Tvec2> round(const Rectangle<Tvec2>& rectangle);

template<typename Tvec2>
Rectangle<Tvec2> floor(const Rectangle<Tvec2>& rectangle);

template<typename Tvec2>
Rectangle<Tvec2> ceil(const Rectangle<Tvec2>& rectangle);

} // namespace Base

#include "rectangle.inl"

#endif // BASE_RECTANGLE_H
