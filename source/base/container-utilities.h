#ifndef _BASE_CONTAINER_UTILITIES_H_
#define _BASE_CONTAINER_UTILITIES_H_

#include <dependencies.h>

namespace Base {


template<typename T_container>
typename T_container::value_type pop(T_container& container)
{
  if(container.empty())
    throw std::invalid_argument("Base:pop: empty container not alloed");

  typename T_container::value_type value = *container.begin();

  container.pop_front();

  return value;
}


} // namespace Base

#endif
