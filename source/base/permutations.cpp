#include "permutations.h"

namespace Base {

TEST(base_permutations, permutate)
{
  const int A = 0;
  const int B = 1;
  const int C = 2;

  EXPECT_EQ(QList< QList<int> >({{A, B, C},
                                 {B, A, C},
                                 {B, C, A},
                                 {A, C, B},
                                 {C, A, B},
                                 {C, B, A}}),
            permutate<int>({A, B, C}));
}

} // namespace Base
