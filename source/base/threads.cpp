#include <dependencies.h>

#include <base/threads.h>

namespace Base {


void sleepWhile(const std::function<bool()>& predicate,
                const std::chrono::milliseconds& maxWaitTime,
                const std::chrono::microseconds& updateRate)
{
  std::chrono::microseconds total_time = std::chrono::microseconds(0);

  while(total_time < maxWaitTime)
  {
    if(!predicate())
      return;

    sleepFor(updateRate);
    total_time += updateRate;
  }

  throw Ogre::Exception(0,
                        "Possible endless loop.",
                        "sleepWhile",
                        "PossibleEndlessLoop",
                        __FILE__,
                        __LINE__);
}


void sleepFor(const std::chrono::microseconds& sleepTime)
{
  std::this_thread::sleep_for(sleepTime);
}


} // namespace Base
