#include "range-iterator.h"

namespace Base {
namespace Private {


TEST(base_private_rangeIterator, _calcNumSteps_badInput)
{
  EXPECT_EQ(1, IterableRange<int>::_calcNumSteps(10, 0, 1, true));
  EXPECT_EQ(11, IterableRange<int>::_calcNumSteps(0, 10, 1, true));

  EXPECT_EQ(11, IterableRange<int>::_calcNumSteps(10, 0, -1, true));
  EXPECT_EQ(1, IterableRange<int>::_calcNumSteps(0, 10, -1, true));

  EXPECT_EQ(1, IterableRange<int>::_calcNumSteps(42, 23, 0, true));
}


TEST(base_private_rangeIterator, _calcNumSteps_roundUpToEnd)
{
  EXPECT_EQ(11, IterableRange<int>::_calcNumSteps(0, 10, 1, true));
  EXPECT_EQ(11, IterableRange<int>::_calcNumSteps(0, 10, 1, false));

  EXPECT_EQ(3, IterableRange<int>::_calcNumSteps(0, 3, 2, true));
  EXPECT_EQ(2, IterableRange<int>::_calcNumSteps(0, 3, 2, false));

  EXPECT_EQ(5, IterableRange<real>::_calcNumSteps(0, 1, 0.3, true));
  EXPECT_EQ(4, IterableRange<real>::_calcNumSteps(0, 1, 0.3, false));
}


TEST(base_private_rangeIterator, values)
{
  QVector<int> integerResult;

  for(int i : range(0, 10))
    integerResult.append(i);

  EXPECT_EQ(QVector<int>({0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}), integerResult);

  integerResult.clear();

  for(int i : range(0, 10, -1))
    integerResult.append(i);

  EXPECT_EQ(QVector<int>({0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}), integerResult);

  integerResult.clear();
  for(int i : range(10, 1))
    integerResult.append(i);

  EXPECT_EQ(QVector<int>({10, 9, 8, 7, 6, 5, 4, 3, 2, 1}), integerResult);

  integerResult.clear();
  for(int i : range(42, 1, 0))
    integerResult.append(i);

  EXPECT_EQ(QVector<int>({42}), integerResult);

  integerResult.clear();
  for(int i : range(5, 1, -1))
    integerResult.append(i);

  EXPECT_EQ(QVector<int>({5, 4, 3, 2, 1}), integerResult);

  integerResult.clear();
  for(int i : range(5, 1, 1))
    integerResult.append(i);

  EXPECT_EQ(QVector<int>({5, 4, 3, 2, 1}), integerResult);

  integerResult.clear();
  for(double i : range(0., 1., 0.3))
    integerResult.append(round(i*100));

  EXPECT_EQ(QVector<int>({0, 30, 60, 90}), integerResult);

  integerResult.clear();
  for(double i : range(0., 1., 0.3))
    integerResult.append(round(i*100));

  EXPECT_EQ(QVector<int>({0, 30, 60, 90}), integerResult);

  integerResult.clear();
  for(double i : range(0., 1., 0.3, true))
    integerResult.append(round(i*100));

  EXPECT_EQ(QVector<int>({0, 30, 60, 90, 120}), integerResult);
}


} // namespace Private
} // namespace Base
