#ifndef BASE_DECLARATIONS_H
#define BASE_DECLARATIONS_H

#include <dependencies.h>

namespace Base {
namespace Geometry {


class Plane;
class Ray;


} // namespace Geometry
} // namespace Base

#endif
