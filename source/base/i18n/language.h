#ifndef BASE_I18N_LANGUAGE_H
#define BASE_I18N_LANGUAGE_H

#include <base/strings/string.h>

namespace Base {
namespace I18n {

class Language
{
public:
  QHash<String, String> simpleTranslations;

public:
  Language();

  String simpleTranslation(const String& source) const;
};

} // namespace I18n
} // namespace Base

#endif // BASE_I18N_LANGUAGE_H
