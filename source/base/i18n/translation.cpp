#include <base/base-application.h>
#include "translation.h"

namespace Base {
namespace I18n {

class SimpleTranslationImplementation : public Translation::Implementation
{
private:
  const String stringSource;

public:
  SimpleTranslationImplementation(const String& stringSource)
    : stringSource(stringSource)
  {
    update();
  }

  void update() override
  {
    this->_translation = BaseApplication::language().simpleTranslation(stringSource);
  }

  static Ptr create(const String& stringSource)
  {
    return Ptr(new SimpleTranslationImplementation(stringSource));
  }
};

Translation::Translation()
  : Translation(Implementation::createFallback())
{
}

Translation::Translation(const Implementation::Ptr& implementation)
  : _implementation(implementation)
{
}


const String& Translation::translation() const
{
  return _implementation->translation();
}


bool Translation::operator==(const Translation& other) const
{
  return this->_implementation == other._implementation;
}

bool Translation::operator!=(const Translation& other) const
{
  return !(*this == other);
}


// ==== translate ========


Translation::Implementation::Implementation()
{
  BaseApplication::signalLanguageChanged().connect(std::bind(&Implementation::triggerUpdate, this), Signals::PREPEND_SLOT).track(this->trackable);
}

Translation::Implementation::~Implementation()
{
}

Translation::Implementation::Ptr Translation::Implementation::createFallback()
{
  static Ptr fallback(SimpleTranslationImplementation::create(String()));

  return fallback;
}


void Translation::Implementation::triggerUpdate()
{
  this->update();
}


const String& Translation::Implementation::translation() const
{
  return this->_translation;
}


} // namespace I18n


// ==== translate ========


Translation translate(const char* sourceString)
{
  return translate(String(sourceString));
}

Translation translate(const String& sourceString)
{
  return Translation(I18n::SimpleTranslationImplementation::create(sourceString));
}


} // namespace Base
