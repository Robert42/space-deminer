#ifndef _FRAMEWORK_TRANSLATION_H_
#define _FRAMEWORK_TRANSLATION_H_


#include <base/strings/string.h>
#include <base/signals/trackable.h>


namespace Base {
namespace I18n {


class Translation
{
public:
  class Implementation
  {
  public:
    typedef std::shared_ptr<Implementation> Ptr;

  protected:
    Signals::Trackable trackable;
    String _translation;

  public:
    Implementation();
    virtual ~Implementation();

    static Ptr createFallback();

    const String& translation() const;

  protected:
    virtual void update() = 0;

  private:
    void triggerUpdate();
  };

private:
  Implementation::Ptr _implementation;

public:
  Translation();
  Translation(const Implementation::Ptr& implementation);

public:
  const String& translation() const;

public:
  bool operator==(const Translation& other) const;
  bool operator!=(const Translation& other) const;
};


} // namespace I18n

typedef I18n::Translation Translation;

Translation translate(const char* sourceString);
Translation translate(const String& sourceString);

} // namespace Base

#endif
