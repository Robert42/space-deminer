#include "language.h"
#include <base/optional.h>

namespace Base {
namespace I18n {

Language::Language()
{
}

String Language::simpleTranslation(const String& source) const
{
  Optional<String> optionalTranslation = optionalFromHashMap(simpleTranslations, source);

  if(optionalTranslation)
    return *optionalTranslation;
  else
    return source;
}


} // namespace I18n
} // namespace Base
