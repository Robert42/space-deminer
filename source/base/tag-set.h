#ifndef BASE_TAGSET_H
#define BASE_TAGSET_H

#include "singleton.h"

#include "strings/string-id-manager.h"

namespace Base {


enum Tag : std::underlying_type<StringId>::type
{
};

typedef QSet<Tag> TagSet;

class TagIds
{
public:
  static Tag tagFromString(StringId stringId);
  static Tag tagFromString(const QString& string);
  static Tag tagFromString(const String& string);
  static Tag tagFromString(const char* string);
};



} // namespace Base

#endif // BASE_TAGSET_H
