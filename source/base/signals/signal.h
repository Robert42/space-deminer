#ifndef _BASE_SIGNAL_H_
#define _BASE_SIGNAL_H_

#include "connection.h"

#include <base/output.h>


namespace Base {
namespace Signals {


enum SlotPosition
{
  PREPEND_SLOT,
  APPEND_SLOT
};


namespace Private {

template<typename Signature>
class DefaultResultHandler;

template<typename... argument_types>
class DefaultResultHandler<void(argument_types...)>
{
public:
  template<typename iterator>
  static void handleSlotResults(iterator& i, const argument_types& ... arguments)
  {
    while(i.hasSomethingToCall())
    {
      i.callConnection(arguments...);
      i.moveToNext();
    }
  }
};

template<typename... argument_types>
class DefaultResultHandler<bool(argument_types...)>
{
public:
  template<typename iterator>
  static bool handleSlotResults(iterator& i, const argument_types& ... arguments)
  {
    while(i.hasSomethingToCall())
    {
      if(i.callConnection(arguments...))
        return true;
      i.moveToNext();
    }
    return false;
  }
};

template<typename return_type, typename... argument_types>
class DefaultResultHandler<return_type(argument_types...)>
{
public:
  template<typename iterator>
  static return_type handleSlotResults(iterator& i, const argument_types& ... arguments)
  {
    return_type lastResult = return_type();

    while(i.hasSomethingToCall())
    {
      lastResult = i.callConnection(arguments...);
      i.moveToNext();
    }

    return lastResult;
  }
};


template<typename Signature>
class SlotConnectionList;


template<typename return_type, typename... argument_types>
class SlotConnectionList<return_type(argument_types...)> : noncopyable
{
private:
  typedef return_type(signature_type)(argument_types...);
  typedef std::function<signature_type> slot_type;
  typedef SlotConnectionList<signature_type> this_type;

  enum State
  {
    STATE_IDLE,
    STATE_SENDING_SIGNAL = 1,
    STATE_THERES_SOMETHING_TO_DISCONNECT = 2,
    STATE_DISCONNECTING_ALL = 4,
    STATE_ABORTING = 8
  };

  DEFINE_ENUM_BOOLEAN_OPERATORS(State, friend)

  class ConnectionImplementation final : public Connection::Implementation
  {
  public:
    typedef std::shared_ptr<ConnectionImplementation> Ptr;

    const slot_type slot;
    this_type& slotConnectionList;

    ConnectionImplementation(const slot_type& slot, this_type* slotConnectionList)
      : slot(slot),
        slotConnectionList(*slotConnectionList)
    {
    }

    ConnectionImplementation(const ConnectionImplementation&);
    ConnectionImplementation& operator=(const ConnectionImplementation&);

    bool canBeCalled() const
    {
      return !isBlocked() && isValid();
    }

  private:
    void disconnectImplementation() final override
    {
      if(slotConnectionList.state == STATE_IDLE)
        removeThisFromConnecotList();
      else
        slotConnectionList.state |= STATE_THERES_SOMETHING_TO_DISCONNECT;
    }

    void removeThisFromConnecotList()
    {
      slotConnectionList.allConnections.remove_if([this](const Ptr &other){return other.get() == this;});
    }
  };

private:

  typedef typename ConnectionImplementation::Ptr ConnectionImplementationPtr;
  typedef std::list<ConnectionImplementationPtr> ConnectionList;
  typedef typename std::list<ConnectionImplementationPtr>::iterator ConnectionListIterator;

  State state;
  ConnectionList allConnections;
  size_t nIterators;

public:
  class iterator : noncopyable
  {
  private:
    const shared_ptr<this_type> connectionList;
    mutable bool nothingMoreToCall;
    ConnectionListIterator currentElement;

    // We are storing the last element to be called instead of the end iterator in order to allow new connection beeing created from one of the slots
    // this new connection will be ignored during by this iterator.
    ConnectionListIterator lastElement;

  public:
    iterator(const shared_ptr<this_type>& connectionList)
      : connectionList(connectionList),
        nothingMoreToCall(connectionList->allConnections.empty()),
        currentElement(connectionList->allConnections.begin()),
        lastElement(connectionList->allConnections.end())
    {
      ++connectionList->nIterators;

      connectionList->state |= STATE_SENDING_SIGNAL;

      if(!nothingMoreToCall)
      {
        --lastElement;
        skipAllInvalidConnections();
      }
    }

    ~iterator()
    {
      assert(connectionList->nIterators != 0);

      --connectionList->nIterators;

      if(connectionList->nIterators == 0)
      {
        if(connectionList->state & STATE_DISCONNECTING_ALL)
        {
          connectionList->allConnections.clear();
        }else if(connectionList->state & STATE_THERES_SOMETHING_TO_DISCONNECT)
        {
          for(auto i = connectionList->allConnections.begin(); i!=connectionList->allConnections.end();)
          {
            const ConnectionImplementationPtr& connection = *i;

            if(!connection->isValid())
              i = connectionList->allConnections.erase(i);
            else
              ++i;
          }
        }

        connectionList->state = STATE_IDLE;
      }
    }

    bool hasSomethingToCall() const
    {
      nothingMoreToCall |= (connectionList->state & (STATE_ABORTING | STATE_DISCONNECTING_ALL)) != 0;
      nothingMoreToCall |= !areThereValidConnections();

      return !nothingMoreToCall;
    }

    void moveToNext()
    {
      if(!hasSomethingToCall())
        return;

      ++currentElement;

      skipAllInvalidConnections();
    }

    return_type callConnection(const argument_types& ... arguments)
    {
      assert(hasSomethingToCall());

      const ConnectionImplementationPtr& connection = *currentElement;
      const slot_type& slot = connection->slot;

      return slot(arguments...);
    }

  private:
    void skipAllInvalidConnections()
    {
      nothingMoreToCall |= !skipAllInvalidConnections(inout(currentElement));
    }

    bool areThereValidConnections() const
    {
      ConnectionListIterator i = currentElement;
      return skipAllInvalidConnections(inout(i));
    }

    bool skipAllInvalidConnections(const InOutput<ConnectionListIterator>& iter) const
    {
      if(nothingMoreToCall)
        return false;

      ConnectionListIterator& i = iter.value;

      ConnectionListIterator end = lastElement;
      ++end;

      while(i!=end && !(*i)->canBeCalled())
      {
        if(!(*i)->isValid())
          connectionList->state |= STATE_THERES_SOMETHING_TO_DISCONNECT;
        ++i;
      }

      return i!=end;
    }
  };

public:
  SlotConnectionList()
    : state(STATE_IDLE)
  {
    nIterators = 0;
  }

  ~SlotConnectionList()
  {
    assert(nIterators == 0);
  }

  Connection connect(const slot_type& slot, SlotPosition slotPosition = APPEND_SLOT)
  {
    ConnectionImplementationPtr connection(new ConnectionImplementation(slot, this));

    if(slotPosition == APPEND_SLOT)
      allConnections.push_back(connection);
    else
      allConnections.push_front(connection);

    return Connection(connection);
  }

  void disconnectAll()
  {
    if(state == STATE_IDLE)
      allConnections.clear();
    else
      state |= STATE_DISCONNECTING_ALL;
  }

  void abortCalled()
  {
    if(state != STATE_IDLE)
      state |= STATE_ABORTING;
  }

};


} // namespace Private


namespace ResultHandler {

template<typename Signature>
class PassResultAsArgumentResultHandler;

template<typename return_type, typename first_argument_type, typename... argument_types>
class PassResultAsArgumentResultHandler<return_type(first_argument_type,argument_types...)>
{
public:
  template<typename iterator>
  static return_type handleSlotResults(iterator& i, const first_argument_type& firstArgument, const argument_types& ... arguments)
  {
    return_type lastResult = firstArgument;

    while(i.hasSomethingToCall())
    {
      lastResult = i.callConnection(lastResult, arguments...);
      i.moveToNext();
    }

    return lastResult;
  }
};

template<typename Signature>
class LogicalAndResultHandler;

template<typename... argument_types>
class LogicalAndResultHandler<bool(argument_types...)>
{
public:
  template<typename iterator>
  static bool handleSlotResults(iterator& i, const argument_types& ... arguments)
  {
    while(i.hasSomethingToCall())
    {
      if(!i.callConnection(arguments...))
        return false;
      i.moveToNext();
    }
    return true;
  }
};

} // namespace ResultHandler


template<typename Signature, typename ResultHandler=Private::DefaultResultHandler<Signature>>
class CallableSignal;

template<typename Signature, typename ResultHandler=Private::DefaultResultHandler<Signature>>
class Signal
{
public:
  typedef Signature signature_type;
  typedef std::function<signature_type> slot_type;
  typedef Signal<Signature, ResultHandler> this_type;

public:
  typedef Private::SlotConnectionList<signature_type> SlotConnectionList;

protected:
  shared_ptr<SlotConnectionList> slotConnectionList;
  bool abortCalledWhenDestroyed;

public:
  Signal()
    : slotConnectionList(new SlotConnectionList)
  {
    abortCalledWhenDestroyed = false;
  }

  ~Signal()
  {
    if(abortCalledWhenDestroyed)
      abortCalled();
  }

  Signal(const this_type&)
    : Signal()
  {
  }

  Signal& operator=(const this_type&)
  {
    return *this;
  }

  Connection connect(const slot_type& slot, SlotPosition slotPosition = APPEND_SLOT)
  {
    return slotConnectionList->connect(slot, slotPosition);
  }

  template<typename other_ResultHandler>
  Connection connect(const CallableSignal<signature_type, other_ResultHandler>& slot, SlotPosition slotPosition = APPEND_SLOT) = delete;

  template<typename other_ResultHandler>
  Connection connect(const Signal<signature_type, other_ResultHandler>& slot, SlotPosition slotPosition = APPEND_SLOT) = delete;

  template<typename other_ResultHandler>
  Connection connect(CallableSignal<signature_type, other_ResultHandler>& slot, SlotPosition slotPosition = APPEND_SLOT)
  {
    return connect(slot.asSlot(), slotPosition);
  }

  void disconnectAll()
  {
    slotConnectionList->disconnectAll();
  }

  void setAbortCalledWhenDestroyed(bool abortCalledWhenDestroyed = true)
  {
    this->abortCalledWhenDestroyed = abortCalledWhenDestroyed;
  }

  void abortCalled()
  {
    slotConnectionList->abortCalled();
  }
};

template<typename resultHandler, typename return_type, typename... argument_types>
class CallableSignal<return_type(argument_types...), resultHandler> : public Signal<return_type(argument_types...), resultHandler>
{
public:
  typedef Signal<return_type(argument_types...)> parent_class;
  typedef typename parent_class::signature_type signature_type;
  typedef typename parent_class::slot_type slot_type;
  typedef CallableSignal<return_type(argument_types...)> this_type;

public:
  typedef typename parent_class::SlotConnectionList SlotConnectionList;

public:
  return_type operator()(const argument_types& ... arguments)
  {
    typename SlotConnectionList::iterator i(this->slotConnectionList);

    return resultHandler::handleSlotResults(i, arguments...);
  }

  std::function<signature_type> asSlot()
  {
    return [this](const argument_types &... args) -> return_type {
      return (*this)(args...);
    };
  }
};


}
}


#endif
