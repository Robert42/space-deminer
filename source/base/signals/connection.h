#ifndef _BASE_SIGNAL_CONNECTION_IMPLEMENTATION_H_
#define _BASE_SIGNAL_CONNECTION_IMPLEMENTATION_H_

#include "trackable.h"

#include <base/enum-macros.h>

namespace Base {
namespace Signals {


class Connection
{
public:
  BEGIN_ENUMERATION(TrackingType,)
    NO_TRACKING = 0,
    TRACKING_BY_TRACKABLE_INSTANCE = 1,
    TRACKING_BY_VALIDATOR = 2,
    TRACKING_BY_SMART_POINTER = 4,
    DISCONNECT_MANUALLY = 8,
    SUPPRESS_WARNING = 16,
  ENUMERATION_BASIC_OPERATORS(TrackingType)
  ENUMERATION_FLAG_OPERATORS(TrackingType)
  END_ENUMERATION;

  class Implementation;

  class Validator
  {
  public:
    virtual ~Validator()
    {
    }

    virtual bool isValid() const = 0;
  };

  class Blocker
  {
  private:
    weak_ptr<Implementation> blockedImplementation;

  public:
    Blocker();
    Blocker(const Blocker& other);
    Blocker(const weak_ptr<Implementation>& blockedImplementation);
    Blocker& operator=(const Blocker&);
    Blocker& operator=(const weak_ptr<Implementation>& blockedImplementation);
    ~Blocker();

    void block(const weak_ptr<Implementation>& blockedImplementation);
    void unblock();
  };

  class Implementation : public Trackable::Listener, public noncopyable
  {
  public:
    typedef shared_ptr<Implementation> Ptr;
    typedef weak_ptr<Implementation> WeakPtr;

    friend class Blocker;

  private:
    size_t blocked;
    mutable bool isValid_Cache;
    TrackingType trackingType;
    std::forward_list<Validator*> validators;

  public:
    std::string debugName;

  public:
    Implementation();
    ~Implementation();

    void addTrackingType(TrackingType trackingType);

    void disconnect();
    bool isValid() const;

    bool isBlocked() const;

    void addValidator(Validator* validator);

  protected:
    virtual void disconnectImplementation() = 0;

  private:
    void trackableGotDestroyed(Trackable* trackable) final override;
  };

private:
  template<class T>
  class Validator_ByWeakPtr : public Validator
  {
  private:
    weak_ptr<T> weakPtr;

    bool isValid() const final override
    {
      return !weakPtr.expired();
    }

  public:
    Validator_ByWeakPtr(const weak_ptr<T>& weakPtr)
      : weakPtr(weakPtr)
    {
    }
  };

private:
  friend int qHash(const Connection &connection, uint seed);

  Implementation::WeakPtr implementation;

public:
  Connection();
  Connection(const Implementation::WeakPtr& implementation);

  void disconnect();

  Blocker block();

  bool isConnected() const;

  /** Tracks an instance of Trackable.
  *
  * As soon as the trackable gets destroyed, the connection gets disconnected.
  */
  Connection& track(Trackable* trackable);

  /** Tracks an instance of Trackable.
  *
  * As soon as the trackable gets destroyed, the connection gets disconnected.
  */
  Connection& track(Trackable& trackable);

  /** Tracks an instance represented by a weak pointer.
  *
  * If the instance gets destroyed, the connection will be disconnected the next time the signal gets called.
  *
  * @note If memory space is rare, avoid using this track method (and prefer the other track method).
  * <br>
  * This instances tracked using weak pointer break connections only in the moment the signal is beeing called.
  */
  template<class T>
  Connection& track(const weak_ptr<T>& weakPtr)
  {
    return track(new Validator_ByWeakPtr<T>(weakPtr));
  }

  /** Tracks an instance represented by a weak pointer.
  *
  * If the instance gets destroyed, the connection will be disconnected the next time the signal gets called.
  *
  * @note If memory space is rare, avoid using this track method (and prefer the other track method).
  * <br>
  * This instances tracked using weak pointer break connections only in the moment the signal is beeing called.
  */
  template<class T>
  Connection& track(const shared_ptr<T>& sharedPtr)
  {
    return track(new Validator_ByWeakPtr<T>(weak_ptr<T>(sharedPtr)));
  }

  /**
   * @note after calling this method, validator gets owned by this connection
   *
   * @note If memory space is rare, avoid using this track method (and prefer the other track method).
   * <br>
   * The Validator is only called when the signal is going to send it's signal
   */
  Connection& track(Validator* validator);

  Connection& trackManually()
  {
    return addTrackingType(TrackingType::DISCONNECT_MANUALLY);
  }

  Connection& addTrackingType(TrackingType trackingType);

  bool operator==(const Connection& other) const;
  bool operator!=(const Connection& other) const;
};

inline int qHash(const Connection& connection, uint seed=0)
{
  return qHash(connection.implementation, seed);
}


}
}

#endif
