#include <base/signals/signal.h>

namespace Base {
namespace Signals {

namespace Private {
TEST(base_signals_private_DefaultResultHandler, bool_result)
{
  CallableSignal<bool()> signal;

  bool aWasCalled = false;
  bool bWasCalled = false;
  bool cWasCalled = false;

  EXPECT_FALSE(signal());

  Connection connectionA = signal.connect([&](){aWasCalled = true; return false;});
  connectionA.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  Connection connectionB = signal.connect([&](){bWasCalled = true; return true;});
  connectionB.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  Connection connectionC = signal.connect([&](){cWasCalled = true; return false;});
  connectionC.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  EXPECT_TRUE(signal());
  EXPECT_TRUE(aWasCalled);
  EXPECT_TRUE(bWasCalled);
  EXPECT_FALSE(cWasCalled);

  aWasCalled = false;
  bWasCalled = false;

  connectionB.disconnect();

  EXPECT_FALSE(signal());
  EXPECT_TRUE(aWasCalled);
  EXPECT_FALSE(bWasCalled);
  EXPECT_TRUE(cWasCalled);
}

TEST(base_signals_private_DefaultResultHandler, unknown_result)
{
  enum UnknownType
  {
    DEFAULT_VALUE = 0,
    VALUE_A,
    VALUE_B
  };


  CallableSignal<UnknownType()> signal;

  EXPECT_EQ(DEFAULT_VALUE, signal());

  Connection connectionA = signal.connect([&](){return VALUE_A;});
  connectionA.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  EXPECT_EQ(VALUE_A, signal());

  Connection connectionB = signal.connect([&](){return VALUE_B;});
  connectionB.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  EXPECT_EQ(VALUE_B, signal());
}


TEST(base_signals_private_SlotConnectionList, simple_connection)
{
  // In this test a connection gets created. Does not really test anything, just whether the most basic usage doesn't crash
  SlotConnectionList<void()> connectionList;

  connectionList.connect([](){}).addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);
}

TEST(base_signals_private_SlotConnectionList, iterator_stress_test_empty_connection_list)
{
  shared_ptr<SlotConnectionList<void()>> connectionList(new SlotConnectionList<void()>());

  {
    SlotConnectionList<void()>::iterator i(connectionList);

    EXPECT_FALSE(i.hasSomethingToCall());

    Connection c = connectionList->connect([](){});
    c.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

    EXPECT_FALSE(i.hasSomethingToCall());
  }

  {
    SlotConnectionList<void()>::iterator i(connectionList);

    EXPECT_TRUE(i.hasSomethingToCall());
  }
}

TEST(base_signals_private_SlotConnectionList, iterator_stress_test_only_invalid_connections)
{
  class SayNo : public Connection::Validator
  {
  public:
    bool isValid() const override
    {
      return false;
    }
  };

  shared_ptr<SlotConnectionList<void()>> connectionList(new SlotConnectionList<void()>());

  Connection c1 = connectionList->connect([](){});
  c1.track(new SayNo);

  Connection c2 = connectionList->connect([](){});
  c2.track(new SayNo);

  {
    SlotConnectionList<void()>::iterator i(connectionList);

    EXPECT_FALSE(i.hasSomethingToCall());
  }
}

TEST(base_signals_Signal, connect_signal_with_signal)
{
  CallableSignal<void(int)> signalA;
  CallableSignal<void(int)> signalB;

  int bWasCalled = false;

  signalA.connect(signalB).addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);
  signalB.connect([&bWasCalled](int x){bWasCalled = x;}).addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  EXPECT_EQ(0, bWasCalled);

  signalB(-1);

  EXPECT_EQ(-1, bWasCalled);

  signalA(42);

  EXPECT_EQ(42, bWasCalled);
}

TEST(base_signals_private_SlotConnectionList, iterator_stress_test_only_invalid_followed_by_valid)
{
  bool wrongSlotGotCalled = false;
  bool rightSlotGotCalled = false;

  class SayNo : public Connection::Validator
  {
  public:
    bool isValid() const override
    {
      return false;
    }
  };

  shared_ptr<SlotConnectionList<void()>> connectionList(new SlotConnectionList<void()>());

  Connection c1 = connectionList->connect([&](){wrongSlotGotCalled=true;});
  c1.track(new SayNo);

  Connection c2 = connectionList->connect([&](){wrongSlotGotCalled=true;});
  c2.track(new SayNo);

  Connection c3 = connectionList->connect([&](){rightSlotGotCalled=true;});
  c3.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  {
    SlotConnectionList<void()>::iterator i(connectionList);

    EXPECT_TRUE(i.hasSomethingToCall());
    EXPECT_TRUE(i.hasSomethingToCall());
    EXPECT_TRUE(i.hasSomethingToCall());
    EXPECT_TRUE(i.hasSomethingToCall());

    i.callConnection();

    EXPECT_TRUE(i.hasSomethingToCall());
    EXPECT_TRUE(i.hasSomethingToCall());
    EXPECT_TRUE(i.hasSomethingToCall());
    EXPECT_TRUE(i.hasSomethingToCall());

    i.moveToNext();

    EXPECT_FALSE(i.hasSomethingToCall());
  }

  EXPECT_FALSE(wrongSlotGotCalled);
  EXPECT_TRUE(rightSlotGotCalled);
}

TEST(base_signals_private_SlotConnectionList, iterator_stress_test_abort)
{
  bool a=false, b=false, c=false;

  shared_ptr<SlotConnectionList<void()>> connectionList(new SlotConnectionList<void()>());

  Connection c1 = connectionList->connect([&](){a=true;});
  c1.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  Connection c2 = connectionList->connect([&](){b=true;});
  c2.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  Connection c3 = connectionList->connect([&](){c=true;});
  c3.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  {
    SlotConnectionList<void()>::iterator i1(connectionList);
    {
      SlotConnectionList<void()>::iterator i2(connectionList);
      {
        SlotConnectionList<void()>::iterator i3(connectionList);

        EXPECT_TRUE(i2.hasSomethingToCall());

        connectionList->abortCalled();

        EXPECT_FALSE(i3.hasSomethingToCall());
      }

      EXPECT_FALSE(i2.hasSomethingToCall());
    }

    EXPECT_FALSE(i1.hasSomethingToCall());
  }
}

TEST(base_signals_private_SlotConnectionList, iterator_stress_test_disconnect_all)
{
  int a=0, b=0, c=0;

  shared_ptr<SlotConnectionList<void()>> connectionList(new SlotConnectionList<void()>());

  Connection c1 = connectionList->connect([&](){a++;});
  c1.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  Connection c2 = connectionList->connect([&](){b++;});
  c2.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  Connection c3 = connectionList->connect([&](){c++;});
  c3.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  {
    SlotConnectionList<void()>::iterator i1(connectionList);
    {
      SlotConnectionList<void()>::iterator i2(connectionList);
      {
        SlotConnectionList<void()>::iterator i3(connectionList);

        EXPECT_TRUE(i2.hasSomethingToCall());

        connectionList->disconnectAll();

        EXPECT_FALSE(i3.hasSomethingToCall());
      }

      EXPECT_FALSE(i2.hasSomethingToCall());
    }

    EXPECT_FALSE(i1.hasSomethingToCall());
  }
}

TEST(base_signals_private_SlotConnectionList, iterator_stress_test_disconnect_during_call)
{
  int a=0, b=0;
  Connection c1, c2;

  shared_ptr<SlotConnectionList<void()>> connectionList(new SlotConnectionList<void()>());

  c1 = connectionList->connect([&](){a++; c1.disconnect();});
  c1.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  c2 = connectionList->connect([&](){b++; c2.disconnect();});
  c2.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  {
    SlotConnectionList<void()>::iterator i(connectionList);

    EXPECT_TRUE(i.hasSomethingToCall());

    i.callConnection();

    EXPECT_TRUE(i.hasSomethingToCall());
    EXPECT_TRUE(i.hasSomethingToCall());
    EXPECT_TRUE(i.hasSomethingToCall());
    EXPECT_TRUE(i.hasSomethingToCall());

    i.moveToNext();

    ASSERT_TRUE(i.hasSomethingToCall());

    i.callConnection();

    EXPECT_FALSE(i.hasSomethingToCall());
  }

  {
    SlotConnectionList<void()>::iterator i(connectionList);
    EXPECT_FALSE(i.hasSomethingToCall());
  }

  EXPECT_EQ(1, a);
  EXPECT_EQ(1, b);
}

TEST(base_signals_private_SlotConnectionList, iterator_stress_test_disconnect_during_call_with_bool_return_type)
{
  int a=0, b=0;
  Connection c1, c2;

  shared_ptr<SlotConnectionList<bool()>> connectionList(new SlotConnectionList<bool()>());

  c1 = connectionList->connect([&](){a++; c1.disconnect(); return false;});
  c1.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  c2 = connectionList->connect([&](){b++; c2.disconnect(); return false;});
  c2.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  {
    SlotConnectionList<bool()>::iterator i(connectionList);

    EXPECT_TRUE(i.hasSomethingToCall());

    EXPECT_FALSE(i.callConnection());

    EXPECT_TRUE(i.hasSomethingToCall());
    EXPECT_TRUE(i.hasSomethingToCall());
    EXPECT_TRUE(i.hasSomethingToCall());
    EXPECT_TRUE(i.hasSomethingToCall());
    EXPECT_TRUE(i.hasSomethingToCall());

    i.moveToNext();

    ASSERT_TRUE(i.hasSomethingToCall());

    EXPECT_FALSE(i.callConnection());

    EXPECT_FALSE(i.hasSomethingToCall());
  }

  {
    SlotConnectionList<bool()>::iterator i(connectionList);
    EXPECT_FALSE(i.hasSomethingToCall());
  }

  EXPECT_EQ(1, a);
  EXPECT_EQ(1, b);
}

TEST(base_signals_private_SlotConnectionList, iterator_stress_test_disconnect_nested)
{
  int a=0, b=0, c=0;

  shared_ptr<SlotConnectionList<void()>> connectionList(new SlotConnectionList<void()>());

  Connection c1 = connectionList->connect([&](){a++;});
  c1.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  Connection c2 = connectionList->connect([&](){b++;});
  c2.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  Connection c3 = connectionList->connect([&](){c++;});
  c3.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  {
    SlotConnectionList<void()>::iterator i1(connectionList);
    {
      SlotConnectionList<void()>::iterator i2(connectionList);
      {
        SlotConnectionList<void()>::iterator i3(connectionList);

        EXPECT_TRUE(i3.hasSomethingToCall());

        c1.disconnect();

        EXPECT_TRUE(i3.hasSomethingToCall());

        c2.disconnect();
        c3.disconnect();

        EXPECT_FALSE(i3.hasSomethingToCall());
      }

      EXPECT_FALSE(i2.hasSomethingToCall());
    }

    EXPECT_FALSE(i1.hasSomethingToCall());
  }
}

}

TEST(base_signals_Signal, stress_test_disconnecting_during_call)
{
  Signals::Connection connectionA, connectionB;
  int aWasCalled = 0;
  int bWasCalled = 0;

  CallableSignal<void()> s;

  connectionA = s.connect([&aWasCalled,&connectionA](){++aWasCalled; connectionA.disconnect();}).trackManually();
  connectionB = s.connect([&bWasCalled,&connectionB](){++bWasCalled; connectionB.disconnect();}).trackManually();

  s();

  EXPECT_EQ(1, aWasCalled);
  EXPECT_EQ(1, bWasCalled);
}

TEST(base_signals_Signal, stress_test_disconnecting_during_call_returntype_bool)
{
  Signals::Connection connectionA, connectionB;
  int aWasCalled = 0;
  int bWasCalled = 0;

  CallableSignal<bool()> s;

  connectionA = s.connect([&aWasCalled,&connectionA](){++aWasCalled; connectionA.disconnect(); return false;}).trackManually();
  connectionB = s.connect([&bWasCalled,&connectionB](){++bWasCalled; connectionB.disconnect(); return false;}).trackManually();

  s();

  EXPECT_EQ(1, aWasCalled);
  EXPECT_EQ(1, bWasCalled);
}

TEST(base_signals_Signal, disconnecting_manually)
{
  // Tests, whether disconnecting a connection manually works fine
  CallableSignal<void()> signal;
  int wasCalled = 0;

  Connection connection = signal.connect([&wasCalled](){wasCalled++;});
  connection.addTrackingType(Connection::TrackingType::DISCONNECT_MANUALLY);

  EXPECT_EQ(0, wasCalled);

  signal();

  EXPECT_EQ(1, wasCalled);

  signal();

  EXPECT_EQ(2, wasCalled);

  EXPECT_TRUE(connection.isConnected());
  connection.disconnect();

  signal();

  EXPECT_EQ(2, wasCalled);
}

TEST(base_signals_Signal, disconnecting_from_slot)
{
  // Tests, whether disconnecting a connection manually from within called slot works fine

  CallableSignal<void()> signal;
  int a = 0;
  int b = 0;

  Connection connectionA;
  Connection connectionB;

  connectionA = signal.connect([&a](){a++;});
  connectionA.addTrackingType(Connection::TrackingType::DISCONNECT_MANUALLY);

  Connection connectionDisconnector = signal.connect([&](){
    connectionA.disconnect();
    connectionB.disconnect();
  });
  connectionDisconnector.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  connectionB = signal.connect([&b](){b++;});
  connectionB.addTrackingType(Connection::TrackingType::DISCONNECT_MANUALLY);

  EXPECT_EQ(0, a);
  EXPECT_EQ(0, b);

  signal();

  // a should be added before the slot disconnecting both is called, so a is inremented, b not
  EXPECT_EQ(1, a);
  EXPECT_EQ(0, b);

  signal();

  EXPECT_EQ(1, a);
  EXPECT_EQ(0, b);
}

TEST(base_signals_Signal, calling_slot_from_slot)
{
  CallableSignal<void()> signal;
  int a = 0;
  int b = 0;
  int stack = 0;
  int expectedB = 0;

  Connection connectionA;
  Connection connectionB;

  connectionA = signal.connect([&a](){a++;});
  connectionA.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  Connection connectionDisconnector = signal.connect([&](){
    if(stack != 0)
      return;

    ++stack;

    EXPECT_EQ(1+expectedB, a);
    EXPECT_EQ(0+expectedB, b);

    signal();

    EXPECT_EQ(2+expectedB, a);
    EXPECT_EQ(1+expectedB, b);

    --stack;
  });
  connectionDisconnector.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  connectionB = signal.connect([&b](){b++;});
  connectionB.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  EXPECT_EQ(0, a);
  EXPECT_EQ(0, b);

  signal();

  EXPECT_EQ(2, a);
  EXPECT_EQ(2, b);

  expectedB = 2;

  signal();

  EXPECT_EQ(4, a);
  EXPECT_EQ(4, b);
}

TEST(base_signals_Signal, connecting_from_slot)
{
  CallableSignal<void()> signal;
  int a = 0;
  int b = 0;
  bool firstTime = true;

  Connection connectionA;
  Connection connectionB;

  connectionA = signal.connect([&a](){a++;});
  connectionA.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  Connection connectionDisconnector = signal.connect([&](){
    if(!firstTime)
      return;

    firstTime = false;

    connectionB = signal.connect([&b](){b++;});
    connectionB.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);
  });
  connectionDisconnector.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  EXPECT_EQ(0, a);
  EXPECT_EQ(0, b);

  signal();

  EXPECT_EQ(1, a);
  EXPECT_EQ(0, b);

  signal();

  EXPECT_EQ(2, a);
  EXPECT_EQ(1, b);
}


TEST(base_signals_Signal, disconnecting_all)
{
  CallableSignal<void()> signal;
  int a = 0;
  int b = 0;

  Connection connectionA;
  Connection connectionB;

  connectionA = signal.connect([&a](){a++;});
  connectionA.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  connectionB = signal.connect([&b](){b++;});
  connectionB.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  EXPECT_EQ(0, a);
  EXPECT_EQ(0, b);

  signal();

  EXPECT_EQ(1, a);
  EXPECT_EQ(1, b);

  signal.disconnectAll();
  signal();

  EXPECT_EQ(1, a);
  EXPECT_EQ(1, b);
}


TEST(base_signals_Signal, disconnecting_all_from_slot)
{
  CallableSignal<void()> signal;
  int a = 0;
  int b = 0;

  Connection connectionA;
  Connection connectionB;

  connectionA = signal.connect([&a](){a++;});
  connectionA.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  Connection connectionDisconnector = signal.connect([&](){
    signal.disconnectAll();
  });
  connectionDisconnector.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  connectionB = signal.connect([&b](){b++;});
  connectionB.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  EXPECT_EQ(0, a);
  EXPECT_EQ(0, b);

  signal();

  EXPECT_EQ(1, a);
  EXPECT_EQ(0, b);

  signal();

  EXPECT_EQ(1, a);
  EXPECT_EQ(0, b);
}


TEST(base_signals_Signal, delete_signal_from_list)
{
  CallableSignal<void()>* signal = new CallableSignal<void()>;

  bool bWasCalled = false;

  Connection connectionA;
  Connection connectionB;

  connectionA = signal->connect([](){});
  connectionA.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  Connection connectionDisconnector = signal->connect([&](){
    delete signal;
  });
  connectionDisconnector.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  connectionB = signal->connect([&](){bWasCalled=true;});
  connectionB.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  (*signal)();

  EXPECT_TRUE(bWasCalled);
}


TEST(base_signals_Signal, delete_signal_from_list_and_aborting)
{
  CallableSignal<void()>* signal = new CallableSignal<void()>;

  signal->setAbortCalledWhenDestroyed();

  bool bWasCalled = false;

  Connection connectionA;
  Connection connectionB;

  connectionA = signal->connect([](){});
  connectionA.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  Connection connectionDisconnector = signal->connect([&](){
    delete signal;
  });
  connectionDisconnector.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  connectionB = signal->connect([&](){bWasCalled=true;});
  connectionB.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  (*signal)();

  EXPECT_FALSE(bWasCalled);
}


TEST(base_signals_Signal, block_connection)
{
  CallableSignal<void()> signal;

  int a = 0;

  Connection connectionA;

  connectionA = signal.connect([&](){++a;});
  connectionA.addTrackingType(Connection::TrackingType::SUPPRESS_WARNING);

  EXPECT_EQ(0, a);

  signal();

  EXPECT_EQ(1, a);

  {
    Connection::Blocker blocker = connectionA.block();

    signal();

    EXPECT_EQ(1, a);
  }

  signal();

  EXPECT_EQ(2, a);

  {
    Connection::Blocker blockerA = connectionA.block();
    Connection::Blocker blockerB = blockerA;
    (void)blockerB;

    signal();

    EXPECT_EQ(2, a);
  }

  signal();

  EXPECT_EQ(3, a);
}


}
}
