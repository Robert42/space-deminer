#include <base/signals/connection.h>
#include <base/io/log.h>

namespace Base {
namespace Signals {


TEST(base_signals_Connection, track_gets_the_validator_deleted)
{
  bool gotDeleted = false;

  class TestValidator : public Connection::Validator
  {
  public:
    bool& gotDeleted;

    TestValidator(bool& gotDeleted)
      : gotDeleted(gotDeleted)
    {
    }

    ~TestValidator()
    {
      gotDeleted = true;
    }

    bool isValid() const override
    {
      return true;
    }
  };

  Connection c;

  c.track(new TestValidator(gotDeleted));

  EXPECT_TRUE(gotDeleted);
}


}
}
