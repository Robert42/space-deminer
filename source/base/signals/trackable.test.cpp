#include <base/signals/trackable.h>

namespace Base {
namespace Signals {

namespace Private {
class TestTrackable
{
public:
  Trackable trackable;
  int value;

  TestTrackable(int value = 0)
  {
    this->value = value;
  }
};

class TestTrackableListener : public Trackable::Listener
{
public:
  int value;

  TestTrackableListener(int value = 0)
  {
    this->value = value;
  }

public:
  std::list<Trackable*> destroyedTrackable;


  void trackableGotDestroyed(Trackable* trackable) final override
  {
    destroyedTrackable.push_back(trackable);
  }
};
}

using Private::TestTrackable;
using Private::TestTrackableListener;

TEST(base_signals_Trackable, destroying_trackable)
{
  TestTrackable* trackableA = new TestTrackable;
  TestTrackableListener* listenerA = new TestTrackableListener;

  listenerA->startListeningToTrackable(&trackableA->trackable);

  EXPECT_EQ(QSet<Trackable*>({&trackableA->trackable}), listenerA->listenedTrackables);
  EXPECT_EQ(std::list<Trackable*>({}), listenerA->destroyedTrackable);

  delete trackableA;

  EXPECT_EQ(QSet<Trackable*>({}), listenerA->listenedTrackables);
  EXPECT_EQ(std::list<Trackable*>({&trackableA->trackable}), listenerA->destroyedTrackable);

  delete listenerA;
}


TEST(base_signals_Trackable, copy_constructor)
{
  TestTrackable* trackableA = new TestTrackable(42);
  TestTrackableListener* listenerA = new TestTrackableListener;

  listenerA->startListeningToTrackable(&trackableA->trackable);

  TestTrackable* trackableB = new TestTrackable(*trackableA);

  EXPECT_EQ(QSet<Trackable::Listener*>({listenerA}), trackableA->trackable.listeners);
  EXPECT_EQ(QSet<Trackable::Listener*>({}), trackableB->trackable.listeners);
  EXPECT_EQ(42, trackableA->value);
  EXPECT_EQ(42, trackableB->value);

  trackableB->value = 16;
  *trackableA = *trackableB;

  EXPECT_EQ(QSet<Trackable::Listener*>({listenerA}), trackableA->trackable.listeners);
  EXPECT_EQ(QSet<Trackable::Listener*>({}), trackableB->trackable.listeners);
  EXPECT_EQ(16, trackableA->value);
  EXPECT_EQ(16, trackableB->value);

  delete trackableA;
  delete trackableB;
  delete listenerA;
}


TEST(base_signals_Trackable_Listener, destroying_listener)
{
  TestTrackable* trackableA = new TestTrackable;
  TestTrackableListener* listenerA = new TestTrackableListener;

  listenerA->startListeningToTrackable(&trackableA->trackable);

  EXPECT_EQ(QSet<Trackable::Listener*>({listenerA}), trackableA->trackable.listeners);

  delete listenerA;

  EXPECT_EQ(QSet<Trackable::Listener*>({}), trackableA->trackable.listeners);

  delete trackableA;
}


TEST(base_signals_Trackable_Listener, startListeningToTrackable)
{
  TestTrackable* trackableA = new TestTrackable;
  TestTrackableListener* listenerA = new TestTrackableListener;

  listenerA->startListeningToTrackable(&trackableA->trackable);

  EXPECT_EQ(QSet<Trackable*>({&trackableA->trackable}), listenerA->listenedTrackables);
  EXPECT_EQ(QSet<Trackable::Listener*>({listenerA}), trackableA->trackable.listeners);

  delete trackableA;
  delete listenerA;
}


TEST(base_signals_Trackable_Listener, stopListeningToTrackable)
{
  TestTrackable* trackableA = new TestTrackable;
  TestTrackableListener* listenerA = new TestTrackableListener;

  listenerA->startListeningToTrackable(&trackableA->trackable);

  EXPECT_EQ(QSet<Trackable*>({&trackableA->trackable}), listenerA->listenedTrackables);
  EXPECT_EQ(QSet<Trackable::Listener*>({listenerA}), trackableA->trackable.listeners);

  listenerA->stopListeningToTrackable(&trackableA->trackable);

  EXPECT_EQ(QSet<Trackable*>({}), listenerA->listenedTrackables);
  EXPECT_EQ(QSet<Trackable::Listener*>({}), trackableA->trackable.listeners);

  delete trackableA;
  delete listenerA;
}


TEST(base_signals_Trackable_Listener, copy_constructor)
{
  TestTrackable* trackableA = new TestTrackable;
  TestTrackableListener* listenerA = new TestTrackableListener(42);

  listenerA->startListeningToTrackable(&trackableA->trackable);
  EXPECT_EQ(QSet<Trackable::Listener*>({listenerA}), trackableA->trackable.listeners);

  TestTrackableListener* listenerB = new TestTrackableListener(*listenerA);

  EXPECT_EQ(QSet<Trackable*>({&trackableA->trackable}), listenerA->listenedTrackables);
  EXPECT_EQ(QSet<Trackable*>({&trackableA->trackable}), listenerB->listenedTrackables);
  EXPECT_EQ(42, listenerA->value);
  EXPECT_EQ(42, listenerB->value);
  EXPECT_EQ(QSet<Trackable::Listener*>({listenerA, listenerB}), trackableA->trackable.listeners);

  listenerB->value = 16;
  *listenerA = *listenerB;

  EXPECT_EQ(QSet<Trackable*>({&trackableA->trackable}), listenerA->listenedTrackables);
  EXPECT_EQ(QSet<Trackable*>({&trackableA->trackable}), listenerB->listenedTrackables);
  EXPECT_EQ(16, listenerA->value);
  EXPECT_EQ(16, listenerB->value);
  EXPECT_EQ(QSet<Trackable::Listener*>({listenerA, listenerB}), trackableA->trackable.listeners);

  delete listenerB;

  EXPECT_EQ(QSet<Trackable::Listener*>({listenerA}), trackableA->trackable.listeners);

  delete listenerA;

  EXPECT_EQ(QSet<Trackable::Listener*>({}), trackableA->trackable.listeners);

  delete trackableA;
}


TEST(base_signals_Trackable_Listener, assign_operator)
{
  TestTrackable* trackableA = new TestTrackable;
  TestTrackable* trackableB = new TestTrackable;

  TestTrackableListener* listenerA = new TestTrackableListener(42);
  TestTrackableListener* listenerB = new TestTrackableListener(16);

  listenerA->startListeningToTrackable(&trackableA->trackable);
  listenerB->startListeningToTrackable(&trackableB->trackable);

  EXPECT_EQ(QSet<Trackable*>({&trackableA->trackable}), listenerA->listenedTrackables);
  EXPECT_EQ(QSet<Trackable*>({&trackableB->trackable}), listenerB->listenedTrackables);
  EXPECT_EQ(QSet<Trackable::Listener*>({listenerA}), trackableA->trackable.listeners);
  EXPECT_EQ(QSet<Trackable::Listener*>({listenerB}), trackableB->trackable.listeners);
  EXPECT_EQ(42, listenerA->value);
  EXPECT_EQ(16, listenerB->value);

  *listenerB = *listenerA;

  EXPECT_EQ(QSet<Trackable*>({&trackableA->trackable}), listenerA->listenedTrackables);
  EXPECT_EQ(QSet<Trackable*>({&trackableA->trackable}), listenerB->listenedTrackables);
  EXPECT_EQ(QSet<Trackable::Listener*>({listenerA, listenerB}), trackableA->trackable.listeners);
  EXPECT_EQ(QSet<Trackable::Listener*>({}), trackableB->trackable.listeners);
  EXPECT_EQ(42, listenerA->value);
  EXPECT_EQ(42, listenerB->value);

  delete trackableB;


  EXPECT_EQ(QSet<Trackable*>({&trackableA->trackable}), listenerA->listenedTrackables);
  EXPECT_EQ(QSet<Trackable*>({&trackableA->trackable}), listenerB->listenedTrackables);
  EXPECT_EQ(QSet<Trackable::Listener*>({listenerA, listenerB}), trackableA->trackable.listeners);
  EXPECT_EQ(42, listenerA->value);
  EXPECT_EQ(42, listenerB->value);

  delete listenerB;


  EXPECT_EQ(QSet<Trackable*>({&trackableA->trackable}), listenerA->listenedTrackables);
  EXPECT_EQ(QSet<Trackable::Listener*>({listenerA}), trackableA->trackable.listeners);
  EXPECT_EQ(42, listenerA->value);

  delete listenerA;

  EXPECT_EQ(QSet<Trackable::Listener*>({}), trackableA->trackable.listeners);

  delete trackableA;
}


}
}
