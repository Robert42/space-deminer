#ifndef _BASE_SIGNAL_TRACKABLE_H_
#define _BASE_SIGNAL_TRACKABLE_H_


#include <dependencies.h>


namespace Base {
namespace Signals {

/** Helper class for classes who want to notify their listeners about when they get destroyed.
 *
 * If an tracked instance gets destroyed, all connections tracking the trackable object automatically disconnect.
 *
 * @note The convention is to use an instance of Trackable as first public member variable using the name `trackable`
 */
class Trackable final
{
public:
  class Listener
  {
    friend class Trackable;

  private:
    QSet<Trackable*> listenedTrackables;

  protected:
    Listener();
    virtual ~Listener();

    Listener(const Listener& other);
    Listener& operator=(const Listener& other);

  public:
    void startListeningToTrackable(Trackable* trackable);
    void stopListeningToTrackable(Trackable* trackable);

  protected:
    /** Abstract method getting called when a tracked trackable gets destroyed.
         *
         * @note Note that you don't have to call stopListeningToTrackable by yourself. This is already done by
         * the Trackable.
         */
    virtual void trackableGotDestroyed(Trackable* trackable) = 0;

  private:
    FRIEND_TEST(base_signals_Trackable, destroying_trackable);
    FRIEND_TEST(base_signals_Trackable_Listener, copy_constructor);
    FRIEND_TEST(base_signals_Trackable_Listener, startListeningToTrackable);
    FRIEND_TEST(base_signals_Trackable_Listener, stopListeningToTrackable);
    FRIEND_TEST(base_signals_Trackable_Listener, assign_operator);
  };

private:
  QSet<Listener*> listeners;

public:
  Trackable();
  ~Trackable();

  void clear();

  Trackable(const Trackable& other);
  Trackable& operator=(const Trackable& other);

private:
  FRIEND_TEST(base_signals_Trackable, copy_constructor);
  FRIEND_TEST(base_signals_Trackable_Listener, destroying_listener);
  FRIEND_TEST(base_signals_Trackable_Listener, startListeningToTrackable);
  FRIEND_TEST(base_signals_Trackable_Listener, stopListeningToTrackable);
  FRIEND_TEST(base_signals_Trackable_Listener, copy_constructor);
  FRIEND_TEST(base_signals_Trackable_Listener, assign_operator);
};

}
}


#endif
