#include <base/signals/connection.h>
#include <base/io/log.h>

namespace Base {
namespace Signals {


Connection::Connection()
{
}


Connection::Connection(const Implementation::WeakPtr& implementation)
  : implementation(implementation)
{
}


void Connection::disconnect()
{
  Implementation::Ptr i = implementation.lock();

  if(i)
    i->disconnect();
}


Connection::Blocker Connection::block()
{
  return Blocker(implementation);
}


bool Connection::isConnected() const
{
  Implementation::Ptr i = implementation.lock();

  if(!i)
    return false;

  return i->isValid();
}


Connection& Connection::track(Trackable* trackable)
{
  Implementation::Ptr i = implementation.lock();

  if(i)
  {
    i->startListeningToTrackable(trackable);
    i->addTrackingType(TrackingType::TRACKING_BY_TRACKABLE_INSTANCE);
  }

  return *this;
}


Connection& Connection::track(Trackable& trackable)
{
  return track(&trackable);
}


Connection& Connection::track(Validator* validator)
{
  Implementation::Ptr i = implementation.lock();

  if(i)
  {
    i->addValidator(validator);
    i->addTrackingType(TrackingType::TRACKING_BY_SMART_POINTER);
  }else
  {
    delete validator;
  }

  return *this;
}



Connection& Connection::addTrackingType(TrackingType trackingType)
{
  Implementation::Ptr i = implementation.lock();

  assert(i);

  if(i)
    i->addTrackingType(trackingType);

  return *this;
}



Connection::Implementation::Implementation()
{
  blocked = 0;
  trackingType = TrackingType::NO_TRACKING;
  isValid_Cache = true;
}


Connection::Implementation::~Implementation()
{
  if(this->trackingType == TrackingType::NO_TRACKING)
  {
    if(debugName.empty())
      IO::Log::logError("Signal without tracking detected.");
    else
      IO::Log::logError("The Signal named `%0` has no trackng enabled", debugName);
  }
  assert(this->trackingType != TrackingType::NO_TRACKING);

  for(Validator* v : validators)
    delete v;
}


void Connection::Implementation::addTrackingType(TrackingType trackingType)
{
  this->trackingType |= trackingType;
}


void Connection::Implementation::disconnect()
{
  isValid_Cache = false;
  disconnectImplementation();
}


bool Connection::Implementation::isValid() const
{
  if(!isValid_Cache)
    return false;

  for(Validator* v : validators)
    if(!v->isValid())
    {
      isValid_Cache = false;
      return false;
    }

  return true;
}


bool Connection::Implementation::isBlocked() const
{
  return this->blocked!=0;
}


void Connection::Implementation::addValidator(Validator* validator)
{
  validators.push_front(validator);
  addTrackingType(TrackingType::TRACKING_BY_VALIDATOR);
}


void Connection::Implementation::trackableGotDestroyed(Trackable*)
{
  disconnect();
}


Connection::Blocker::Blocker()
{
}


Connection::Blocker::Blocker(const Blocker& other)
{
  block(other.blockedImplementation);
}


Connection::Blocker& Connection::Blocker::operator=(const Blocker& other)
{
  block(other.blockedImplementation);
  return *this;
}


Connection::Blocker& Connection::Blocker::operator=(const weak_ptr<Implementation>& blockedImplementation)
{
  block(blockedImplementation);
  return *this;
}


Connection::Blocker::Blocker(const weak_ptr<Implementation>& blockedImplementation)
{
  block(blockedImplementation);
}


Connection::Blocker::~Blocker()
{
  unblock();
}


void Connection::Blocker::block(const weak_ptr<Implementation>& blockedImplementation)
{
  unblock();

  this->blockedImplementation =  blockedImplementation;

  shared_ptr<Implementation> i = this->blockedImplementation.lock();
  if(i)
    i->blocked++;
}


void Connection::Blocker::unblock()
{
  shared_ptr<Implementation> i = blockedImplementation.lock();
  if(i)
  {
    assert(i->blocked > 0);
    i->blocked--;
    blockedImplementation.reset();
  }
}


bool Connection::operator==(const Connection& other) const
{
  return this->implementation.lock() == other.implementation.lock();
}

bool Connection::operator!=(const Connection& other) const
{
  return !(*this == other);
}

}
}
