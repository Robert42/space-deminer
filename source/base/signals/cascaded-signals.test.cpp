#include <base/signals/cascaded-signals.h>

namespace Base {
namespace Signals {


TEST(base_signals_CascadedSignalBlocker, no_blocking)
{
  Private::CascadedSignalBlockerSet<int>::Ptr blockerSet = Private::CascadedSignalBlockerSet<int>::create(42);

  EXPECT_EQ(42, blockerSet->limit());
}


TEST(base_signals_CascadedSignalBlocker, simple_blocking)
{
  Private::CascadedSignalBlockerSet<int>::Ptr blockerSet = Private::CascadedSignalBlockerSet<int>::create(0);

  CascadedSignalBlocker::Ptr blocker = blockerSet->createBlocker(42);

  EXPECT_EQ(42, blockerSet->limit());
}


TEST(base_signals_CascadedSignalBlocker, multi_blocking)
{
  Private::CascadedSignalBlockerSet<int>::Ptr blockerSet = Private::CascadedSignalBlockerSet<int>::create(0);

  EXPECT_EQ(0, blockerSet->limit());

  CascadedSignalBlocker::Ptr blocker42 = blockerSet->createBlocker(42);

  EXPECT_EQ(42, blockerSet->limit());

  CascadedSignalBlocker::Ptr blocker16 = blockerSet->createBlocker(16);

  EXPECT_EQ(42, blockerSet->limit());

  blocker42.reset();

  EXPECT_EQ(16, blockerSet->limit());

  blocker42 = blockerSet->createBlocker(42);

  EXPECT_EQ(42, blockerSet->limit());

  CascadedSignalBlocker::Ptr blocker_ii_42 = blockerSet->createBlocker(42);

  EXPECT_EQ(42, blockerSet->limit());

  blocker42.reset();

  EXPECT_EQ(42, blockerSet->limit());

  blocker16.reset();

  EXPECT_EQ(42, blockerSet->limit());

  blocker_ii_42.reset();

  EXPECT_EQ(0, blockerSet->limit());
}


TEST(base_signals_CascadedSignal, order)
{
  std::list<int> order;
  std::list<int> expectedOrder = {3, 2, 1, 0};

  CallableCascadedSignals<bool(), int> s;

  s.connect(0, [&order](){order.push_back(0); return true;}).trackManually();
  s.connect(3, [&order](){order.push_back(3); return false;}).trackManually();
  s.connect(1, [&order](){order.push_back(1); return false;}).trackManually();
  s.connect(2, [&order](){order.push_back(2); return false;}).trackManually();
  s.connect(-1, [&order](){order.push_back(-1); return false;}).trackManually();

  s();

  EXPECT_EQ(expectedOrder, order);
}


TEST(base_signals_CascadedSignal, calling_with_same_priority)
{
  int aWasCalled = 0;
  int bWasCalled = 0;
  int cWasCalled = 0;

  CallableCascadedSignals<bool(), int> s;

  s.connect(42, [&aWasCalled](){++aWasCalled; return false;}).trackManually();
  s.connect(23, [&cWasCalled](){++cWasCalled; return false;}).trackManually();
  s.connect(42, [&bWasCalled](){++bWasCalled; return false;}).trackManually();

  s();

  EXPECT_EQ(1, aWasCalled);
  EXPECT_EQ(1, bWasCalled);
  EXPECT_EQ(1, cWasCalled);
}


TEST(base_signals_CascadedSignal, disconnecting_during_call)
{
  Signals::Connection connectionA, connectionB;
  int aWasCalled = 0;
  int bWasCalled = 0;

  CallableCascadedSignals<bool(), int> s;

  connectionA = s.connect(42, [&aWasCalled,&connectionA](){++aWasCalled; connectionA.disconnect(); return false;}).trackManually();
  connectionB = s.connect(42, [&bWasCalled,&connectionB](){++bWasCalled; connectionB.disconnect(); return false;}).trackManually();

  s();

  EXPECT_EQ(1, aWasCalled);
  EXPECT_EQ(1, bWasCalled);
}


}
}
