#include <base/signals/trackable.h>

namespace Base {
namespace Signals {


Trackable::Trackable()
{
}


Trackable::~Trackable()
{
  clear();
}


void Trackable::clear()
{
  QSet<Listener*> allListeners;

  allListeners.swap(this->listeners);

  for(Listener* listener : allListeners)
  {
    listener->listenedTrackables.remove(this);
    listener->trackableGotDestroyed(this);
  }
}


Trackable::Trackable(const Trackable&)
{
}

Trackable& Trackable::operator=(const Trackable&)
{
  return *this;
}


Trackable::Listener::Listener()
{
}

Trackable::Listener::~Listener()
{
  QSet<Trackable*> allListenedTrackables;

  allListenedTrackables.swap(listenedTrackables);

  for(Trackable* trackable : allListenedTrackables)
  {
    trackable->listeners.remove(this);
  }
}


void Trackable::Listener::startListeningToTrackable(Trackable* trackable)
{
  trackable->listeners.insert(this);
  this->listenedTrackables.insert(trackable);
}


void Trackable::Listener::stopListeningToTrackable(Trackable* trackable)
{
  trackable->listeners.remove(this);
  this->listenedTrackables.remove(trackable);
}


Trackable::Listener::Listener(const Listener& other)
{
  for(Trackable* trackable : other.listenedTrackables)
    startListeningToTrackable(trackable);
}

Trackable::Listener& Trackable::Listener::operator=(const Listener& other)
{
  QSet<Trackable*> allListenedTrackables;

  allListenedTrackables.swap(listenedTrackables);

  for(Trackable* trackable : allListenedTrackables)
    stopListeningToTrackable(trackable);

  for(Trackable* trackable : other.listenedTrackables)
    startListeningToTrackable(trackable);

  return *this;
}


}
}
