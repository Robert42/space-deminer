#ifndef _BASE_CASCADED_SIGNALS_H_
#define _BASE_CASCADED_SIGNALS_H_

#include "signal.h"

namespace Base {
namespace Signals {

class CascadedSignalBlocker
{
public:
  typedef shared_ptr<CascadedSignalBlocker> Ptr;

public:
  virtual ~CascadedSignalBlocker()
  {
  }
};


namespace Private {


template<typename priority_type>
class CascadedSignalBlockerSet : public std::enable_shared_from_this<CascadedSignalBlockerSet<priority_type>>, noncopyable
{
public:
  typedef shared_ptr<CascadedSignalBlockerSet<priority_type>> Ptr;

private:
  typedef CascadedSignalBlocker::Ptr BlockerPtr;

  class CascadedSignalBlockerImplementation : public CascadedSignalBlocker
  {
  public:
    typedef typename CascadedSignalBlockerSet<priority_type>::Ptr CascadedSignalBlockerSetPtr;

  private:
    const CascadedSignalBlockerSetPtr blockerSet;

  public:
    CascadedSignalBlockerImplementation(const CascadedSignalBlockerSetPtr& blockerSet)
      : blockerSet(blockerSet)
    {
    }

    ~CascadedSignalBlockerImplementation()
    {
      blockerSet->remove(this);
    }
  };

private:
  typedef std::multimap<priority_type, CascadedSignalBlocker*> MapType;

  MapType blockers;

public:
  priority_type globalLimit;

private:
  CascadedSignalBlockerSet(priority_type globalLimit)
    : globalLimit(globalLimit)
  {
  }

public:
  static Ptr create(priority_type globalLimit = priority_type())
  {
    return Ptr(new CascadedSignalBlockerSet<priority_type>(globalLimit));
  }

public:
  CascadedSignalBlocker::Ptr createBlocker(priority_type limit)
  {
    BlockerPtr blocker(new CascadedSignalBlockerImplementation(this->shared_from_this()));

    blockers.insert(typename MapType::value_type(limit, blocker.get()));

    return blocker;
  }

  priority_type limit()const
  {
    if(blockers.empty())
      return globalLimit;
    else
      return max(globalLimit, blockers.rbegin()->first);
  }

private:
  void remove(CascadedSignalBlocker* blocker)
  {
    for(typename MapType::iterator i = blockers.begin(); i != blockers.end();)
    {
      if(i->second == blocker)
        i = blockers.erase(i);
      else
        ++i;
    }
  }

};


}


template<typename signature, typename priority_type=int>
class CallableCascadedSignals;

template<typename T_signature, typename T_priority_type=int>
class CascadedSignals
{
public:
  typedef T_priority_type priority_type;
  typedef T_signature signature;
  typedef Signal<signature> signal_type;

  typedef CascadedSignals<signature, priority_type> this_type;

protected:
  typedef CallableSignal<signature> callable_signal_type;
  std::map<priority_type, callable_signal_type> signal_cascade;
  typename Private::CascadedSignalBlockerSet<priority_type>::Ptr blockerSet;

public:
  CascadedSignals()
    : blockerSet(Private::CascadedSignalBlockerSet<priority_type>::create())
  {
  }

  CascadedSignals(const this_type&) : CascadedSignals()
  {
  }

  this_type& operator=(const this_type&)
  {
  }

public:
  Connection connect(priority_type group, const std::function<signature>& slot, SlotPosition slotPosition = APPEND_SLOT)
  {
    return signal_cascade[group].connect(slot, slotPosition);
  }

  template<typename other_PriorityType>
  Connection connect(priority_type group, const CallableCascadedSignals<other_PriorityType, signature>& slot, SlotPosition slotPosition = APPEND_SLOT) = delete;

  template<typename other_PriorityType>
  Connection connect(priority_type group, const CascadedSignals<other_PriorityType, signature>& slot, SlotPosition slotPosition = APPEND_SLOT) = delete;

  template<typename other_PriorityType>
  Connection connect(priority_type group, CallableCascadedSignals<other_PriorityType, signature>& slot, SlotPosition slotPosition = APPEND_SLOT)
  {
    return connect(group,
                   slot.asSlot(), slotPosition);
  }

  template<typename other_ResultHandler>
  Connection connect(priority_type group, const CallableSignal<signature, other_ResultHandler>& slot, SlotPosition slotPosition = APPEND_SLOT) = delete;

  template<typename other_ResultHandler>
  Connection connect(priority_type group, const Signal<signature, other_ResultHandler>& slot, SlotPosition slotPosition = APPEND_SLOT) = delete;

  template<typename other_ResultHandler>
  Connection connect(priority_type group, CallableSignal<signature, other_ResultHandler>& slot, SlotPosition slotPosition = APPEND_SLOT)
  {
    CallableSignal<signature, other_ResultHandler>* slot_ptr = &slot;
    return connect(group,
                   slot.asSlot(), slotPosition);
  }

  CascadedSignalBlocker::Ptr block(priority_type limit)
  {
    return blockerSet->createBlocker(limit);
  }
};


template<typename T_priority_type, typename... argument_types>
class CallableCascadedSignals<bool(argument_types...), T_priority_type> : public CascadedSignals<bool(argument_types...), T_priority_type>
{
public:
  typedef CascadedSignals<bool(argument_types...), T_priority_type> parent_class;
  typedef CallableCascadedSignals<bool(argument_types...), T_priority_type> this_type;

  typedef typename parent_class::priority_type priority_type;
  typedef typename parent_class::signature signature;
  typedef typename parent_class::signal_type signal_type;

protected:
  typedef typename parent_class::callable_signal_type callable_signal_type;

public:
  bool operator()(const argument_types& ... arguments)
  {
    typedef typename std::map<priority_type, callable_signal_type>::value_type pair;
    typedef typename std::map<priority_type, callable_signal_type>::reverse_iterator iterator;

    for(iterator i = this->signal_cascade.rbegin(); i != this->signal_cascade.rend(); ++i)
    {
      pair& group = *i;

      if(group.first < this->blockerSet->limit())
        continue;

      if(group.second(arguments...))
        return true;
    }
    return false;
  }

  std::function<signature> asSlot()
  {
    return [this](const argument_types &... args) -> bool {
      return (*this)(args...);
    };
  }
};


}
}

#endif
