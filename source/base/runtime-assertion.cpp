#include <dependencies.h>
#include <base/runtime-assertion.h>
#include <base/io/log.h>

namespace Base {


int RuntimeAssertion::nRaisedDeveloperWarnings = 0;
int RuntimeAssertion::AreAllowedInThisScope::allowedScopes = 0;

RuntimeAssertion::AreAllowedInThisScope::AreAllowedInThisScope()
{
  ++allowedScopes;
}

RuntimeAssertion::AreAllowedInThisScope::~AreAllowedInThisScope()
{
  --allowedScopes;

  assert(allowedScopes >= 0);
}

bool RuntimeAssertion::AreAllowedInThisScope::areAllowed()
{
  return allowedScopes <= 0;
}

void RuntimeAssertion::assertionExceptional(bool condition,
                                            const Ogre::String& strCondition,
                                            const Ogre::String& message,
                                            const char* file,
                                            int line)
{
  if(condition)
    return;
  if(!AreAllowedInThisScope::areAllowed())
    return;

  throw Ogre::InvalidStateException(0,
                                    createMessage(strCondition,
                                                  message,
                                                  file,
                                                  line),
                                    "RuntimeAssertion::assertion_exceptional",
                                    __FILE__,
                                    __LINE__);
}

void RuntimeAssertion::assertionWarning(bool condition,
                                        const Ogre::String& strCondition,
                                        const Ogre::String& message,
                                        const char* file,
                                        int line)
{
  if(condition)
    return;
  if(!AreAllowedInThisScope::areAllowed())
    return;

  createMessage(strCondition, message, file, line);
}

void RuntimeAssertion::assertionDeveloperWarning(bool condition,
                                                 const Ogre::String& strCondition,
                                                 const Ogre::String& message,
                                                 const char* file,
                                                 int line)
{
  if(condition)
    return;
  if(!AreAllowedInThisScope::areAllowed())
    return;

  createMessage(strCondition, message, file, line);

  ++nRaisedDeveloperWarnings;
}


Ogre::String RuntimeAssertion::createMessage(const Ogre::String& strCondition,
                                             const Ogre::String& message,
                                             const char* file,
                                             int line)
{
  Ogre::String text;

  text = "Runtime-Assertion (" + strCondition + ") failed:\n" +
         message+"\n"
         "File: "+file+" (" + Ogre::StringConverter::toString(line) + ")\n";

  IO::Log::plainLog(text, Ogre::LML_NORMAL);

  return text;
}


void RuntimeAssertion::throwExceptionIfDeveloperWarningWasRaised()
{
  if(nRaisedDeveloperWarnings > 0)
  {
    Ogre::String text;

    if(nRaisedDeveloperWarnings == 1)
      text = "There was one runtime-assertion.\n\n";
    else
      text = "There were " + Ogre::StringConverter::toString(nRaisedDeveloperWarnings) + " runtime-assertion.\n\n";

    text += "You can find more details in the logfile.\n\n";

    throw Ogre::InvalidStateException(0,
                                      text,
                                      "RuntimeAssertion::throwExceptionIfDeveloperWarningWasRaised",
                                      __FILE__,
                                      __LINE__);
  }
}


} // namespace Base
