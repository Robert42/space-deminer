#ifndef BASE_OPTIONAL_H
#define BASE_OPTIONAL_H

#include <dependencies.h>

namespace Base {

enum _OptionalNothing
{
  nothing
};

template<typename T>
class Optional final
{
private:
  T _value;
  bool _valid : 1;

public:
  Optional(const T& value)
    : _value(value),
      _valid(true)
  {
  }

  Optional<T>& operator=(const T& value)
  {
    this->_valid = true;
    this->_value = value;
    return *this;
  }

  Optional(_OptionalNothing n=nothing)
    : _value(T()),
      _valid(false)
  {
    (void)n;
  }

  Optional(const Optional& other)
    : _value(other._value),
      _valid(other._valid)
  {
  }

  Optional<T>& operator=(const Optional<T>& other)
  {
    this->_valid = other._valid;
    this->_value = other._value;
    return *this;
  }

  template<class T_other>
  Optional(const Optional<T_other>& other)
    : _value(other.valid() ? other.value() : T_other()),
      _valid(other.valid())
  {
  }

  template<class T_other>
  Optional<T>& operator=(const Optional<T_other>& other)
  {
    this->_value = other.valid() ? other.value() : T_other();
    this->_valid = other.valid();
    return *this;
  }

  void reset()
  {
    this->_valid = false;

    // This is necessary, in case T is a shared ptr, so the reference counts don't get messed up:
    this->_value = T();
  }

  void reset(const T& value)
  {
    this->_valid = false;
    this->_value = value;
  }

  bool valid() const
  {
    return this->_valid;
  }

  operator bool() const
  {
    return this->valid();
  }

  T& value()
  {
    assert(this->_valid);
    return this->_value;
  }

  const T& value() const
  {
    assert(this->_valid);
    return this->_value;
  }

  T valueWithFallback(const T& fallbackValue) const
  {
    if(this->_valid)
      return this->value();
    else
      return fallbackValue;
  }

  const T& valueWithFallbackCRef(const T& fallbackValue) const
  {
    if(this->_valid)
      return this->value();
    else
      return fallbackValue;
  }

  T& operator*()
  {
    return value();
  }

  const T& operator*() const
  {
    return value();
  }

  T* operator->()
  {
    return &value();
  }

  const T* operator->() const
  {
    return &value();
  }

  bool operator==(const T& other) const
  {
    return this->_valid==true && this->_value==other;
  }

  bool operator!=(const T& other) const
  {
    return !(*this == other);
  }

  bool operator==(const Optional<T>& other) const
  {
    return this->_valid==other._valid && this->_value==other._value;
  }

  bool operator!=(const Optional<T>& other) const
  {
    return !(*this == other);
  }

};

template<typename T, typename T_Key>
static Optional<T> optionalFromHashMap(const QHash<T_Key, T>& hashMap, const T_Key& key)
{
  typename QHash<T_Key, T>::const_iterator i = hashMap.find(key);

  if(i==hashMap.end())
    return nothing;
  return i.value();
}

template<typename T, typename T_Key>
static Optional<T> optionalFromMap(const QHash<T_Key, T>& map, const T_Key& key)
{
  typename QMap<T_Key, T>::const_iterator i = map.find(key);

  if(i==map.end())
    return nothing;
  return i.value();
}

template<typename T>
static Optional< std::shared_ptr<T> > optionalFromWeakPtr(const std::weak_ptr<T>& weakPtr)
{
  std::shared_ptr<T> sharedPtr = weakPtr.lock();

  if(sharedPtr)
    return sharedPtr;
  else
    return nothing;
}

template<typename T>
static Optional< T* > optionalPlainPointerFromWeakPtr(const std::weak_ptr<T>& weakPtr)
{
  std::shared_ptr<T> sharedPtr = weakPtr.lock();

  if(sharedPtr)
    return sharedPtr.get();
  else
    return nothing;
}


} // namespace Base

#endif // BASE_OPTIONAL_H
