#ifndef _BASE_RANGE_H_
#define _BASE_RANGE_H_

#include <dependencies.h>

namespace Base {


template<typename T>
class Range
{
public:
  T begin, end;

public:
  Range(T begin, T end)
    : begin(begin), end(end)
  {
    assert(begin <= end);

    if(begin > end)
      throw std::invalid_argument("**EnumMapper<>::Range::Range** begin must be <= end.");
  }

  bool contains(const T& e) const
  {
    return e>=begin && e<end;
  }

  size_t indexOf(const T& e) const
  {
    return static_cast<size_t>(e) - static_cast<size_t>(begin);
  }

  size_t rangeLength() const
  {
    return indexOf(end);
  }

  bool empty() const
  {
    return begin == end;
  }

  bool isValid() const
  {
    return begin <= end;
  }

  bool isSubsetOf(const Range<T>& parent) const
  {
    return parent.contains(begin) && end<=parent.end;
  }

  bool overlap(const Range<T>& parent) const
  {
    return parent.contains(begin) || contains(parent.begin);
  }
};


} // namespace Base

#endif
