#include "cxx-typeid-name.h"

#include <base/io/log.h>

namespace Base {


#ifdef CXX_TYPEID_NAME_SUPPORTED
template<typename T>
std::string demangeledTypeName()
{
  int errorCode;
  char* resultBuffer;
  std::string result;

  resultBuffer = abi::__cxa_demangle(typeid(T).name(), nullptr, nullptr, &errorCode);

  if(resultBuffer)
  {
    result = resultBuffer;
    free(resultBuffer);
  }else
  {
    result = typeid(T).name();
  }

  if(errorCode!=0)
  {
    IO::Log::logError("Demangling the typename %1 failed (errorCode %2).",
                      typeid(T).name(),
                      errorCode);
  }

  return result;
}
#endif


template<typename T>
void assertSameNameAsType(const std::string& name)
{
  (void)name;

#ifdef CXX_TYPEID_NAME_SUPPORTED
  std::string cxxTypeName = demangeledTypeName<T>();

  if(cxxTypeName != name)
  {
    IO::Log::logError("ClassType::assertionsForNewClassType: wrong name used!.\n"
                      "cxxTypeName: %1\n"
                      "name: %2\n\n",
                      cxxTypeName,
                      name);
    assert(cxxTypeName == name);
  }
#endif
}


} // namespace Base


