#ifndef _BASE_ENUMMACROS_H_
#define _BASE_ENUMMACROS_H_

#include <base/strings/string.h>

#define TO_STRING_MACRO(x) #x

#define _DEFINE_ENUM_BOOLEAN_OPERATORS_BINARY(TargetEnumType, SrcEnumTypeA, SrcEnumTypeB, prefix, constexpr, underlyingTypeBegin, underlyingTypeEnd) \
  inline prefix constexpr TargetEnumType operator |(SrcEnumTypeA a, SrcEnumTypeB b) \
  { \
    return static_cast<TargetEnumType>(underlyingTypeBegin(a)underlyingTypeEnd | underlyingTypeBegin(b)underlyingTypeEnd); \
  } \
  \
  inline prefix constexpr TargetEnumType operator &(SrcEnumTypeA a, SrcEnumTypeB b) \
  { \
    return static_cast<TargetEnumType>(underlyingTypeBegin(a)underlyingTypeEnd & underlyingTypeBegin(b)underlyingTypeEnd); \
  } \
  \
  inline prefix constexpr TargetEnumType operator ^(SrcEnumTypeA a, SrcEnumTypeB b) \
  { \
    return static_cast<TargetEnumType>(underlyingTypeBegin(a)underlyingTypeEnd ^ underlyingTypeBegin(b)underlyingTypeEnd); \
  }

#define _DEFINE_ENUM_BOOLEAN_OPERATORS_UNARY(TargetEnumType, SrcEnumType, prefix, underlyingTypeBegin, underlyingTypeEnd) \
  inline prefix TargetEnumType operator ~(SrcEnumType a) \
  { \
    return static_cast<TargetEnumType>(~underlyingTypeBegin(a)underlyingTypeEnd); \
  } \


#define _DEFINE_ENUM_BOOLEAN_OPERATORS_OPEQUAL(TargetEnumType, SrcEnumType, prefix, underlyingTypeBegin, underlyingTypeEnd) \
  inline prefix TargetEnumType& operator &=(TargetEnumType &a, SrcEnumType b) \
  { \
   return a = (a&b); \
  } \
  \
  inline prefix TargetEnumType& operator |=(TargetEnumType &a, SrcEnumType b) \
  { \
   return a = (a|b); \
  } \
  \
  inline prefix TargetEnumType& operator ^=(TargetEnumType &a, SrcEnumType b) \
  { \
    return a = (a^b); \
  }


#define _DEFINE_ENUM_BOOLEAN_OPERATORS(EnumType, prefix, constexpr, underlyingTypeBegin, underlyingTypeEnd) \
  _DEFINE_ENUM_BOOLEAN_OPERATORS_BINARY(EnumType, EnumType, EnumType, prefix, constexpr, underlyingTypeBegin, underlyingTypeEnd) \
  _DEFINE_ENUM_BOOLEAN_OPERATORS_UNARY(EnumType, EnumType, prefix, underlyingTypeBegin, underlyingTypeEnd) \
  _DEFINE_ENUM_BOOLEAN_OPERATORS_OPEQUAL(EnumType, EnumType, prefix, underlyingTypeBegin, underlyingTypeEnd)

#define _DEFINE_ENUM_MASK_BOOLEAN_OPERATORS(MaskType, FlagType, prefix, constexpr, underlyingTypeBegin, underlyingTypeEnd) \
  _DEFINE_ENUM_BOOLEAN_OPERATORS_BINARY(MaskType, FlagType, FlagType, prefix, constexpr, underlyingTypeBegin, underlyingTypeEnd) \
  _DEFINE_ENUM_BOOLEAN_OPERATORS_BINARY(MaskType, FlagType, MaskType, prefix, constexpr, underlyingTypeBegin, underlyingTypeEnd) \
  _DEFINE_ENUM_BOOLEAN_OPERATORS_BINARY(MaskType, MaskType, FlagType, prefix, constexpr, underlyingTypeBegin, underlyingTypeEnd) \
  _DEFINE_ENUM_BOOLEAN_OPERATORS_BINARY(MaskType, MaskType, MaskType, prefix, constexpr, underlyingTypeBegin, underlyingTypeEnd) \
  _DEFINE_ENUM_BOOLEAN_OPERATORS_UNARY(MaskType, FlagType, prefix, underlyingTypeBegin, underlyingTypeEnd) \
  _DEFINE_ENUM_BOOLEAN_OPERATORS_UNARY(MaskType, MaskType, prefix, underlyingTypeBegin, underlyingTypeEnd) \
  _DEFINE_ENUM_BOOLEAN_OPERATORS_OPEQUAL(MaskType, FlagType, prefix, underlyingTypeBegin, underlyingTypeEnd) \
  _DEFINE_ENUM_BOOLEAN_OPERATORS_OPEQUAL(MaskType, MaskType, prefix, underlyingTypeBegin, underlyingTypeEnd)

#define DEFINE_ENUM_BOOLEAN_OPERATORS(EnumType, prefix) \
  _DEFINE_ENUM_BOOLEAN_OPERATORS(EnumType, prefix, constexpr, static_cast< typename std::underlying_type<EnumType>::type >, )

#define DEFINE_ENUM_MASK_BOOLEAN_OPERATORS(MaskType, FlagType, prefix) \
  _DEFINE_ENUM_MASK_BOOLEAN_OPERATORS(MaskType, FlagType, prefix, constexpr, static_cast< typename std::underlying_type<MaskType>::type >, )


#define BEGIN_ENUMERATION(name, underlyingType) \
class name final \
{ \
public: \
  enum Value underlyingType \
  {

#define ENUMERATION_BASIC_OPERATORS(name) \
  }; \
  typedef typename std::underlying_type<Value>::type underlying_type; \
  Value value; \
  name() \
   : value(Value()) \
  { \
  } \
  name(Value value) \
    : value(value) \
  { \
  } \
  explicit name(int value) \
    : value(static_cast<Value>(value)) \
  { \
  } \
  name(const name& other) \
    : value(other.value) \
  { \
  } \
  name& operator=(const name& other) \
  { \
    this->value = other.value; \
    return *this; \
  } \
  bool operator==(const name& other) const \
  { \
    return this->value == other.value; \
  } \
  bool operator!=(const name& other) const \
  { \
    return this->value != other.value; \
  } \
  friend bool operator==(Value value, const name& enumValue) \
  { \
    return value == enumValue.value; \
  } \
  friend bool operator!=(Value value, const name& enumValue) \
  { \
    return value != enumValue.value; \
  } \
  friend int qHash(const name& enumValue, uint seed=0) \
  { \
    return QT_PREPEND_NAMESPACE(qHash)(enumValue.value, seed); \
  }

#define ENUMERATION_COMPARISON_OPERATORS(name) \
  bool operator <(const name& other) const \
  { \
    return static_cast< typename std::underlying_type<Value>::type >(this->value) < static_cast< typename std::underlying_type<Value>::type >(other.value); \
  } \
  bool operator >(const name& other) const \
  { \
    return static_cast< typename std::underlying_type<Value>::type >(this->value) > static_cast< typename std::underlying_type<Value>::type >(other.value); \
  } \
  bool operator <=(const name& other) const \
  { \
    return static_cast< typename std::underlying_type<Value>::type >(this->value) <= static_cast< typename std::underlying_type<Value>::type >(other.value); \
  } \
  bool operator >=(const name& other) const \
  { \
    return static_cast< typename std::underlying_type<Value>::type >(this->value) >= static_cast< typename std::underlying_type<Value>::type >(other.value); \
  } \
  friend bool operator <(Value value, const name& enumValue) \
  { \
    return static_cast< typename std::underlying_type<Value>::type >(value) < static_cast< typename std::underlying_type<Value>::type >(enumValue.value); \
  } \
  friend bool operator >(Value value, const name& enumValue) \
  { \
    return static_cast< typename std::underlying_type<Value>::type >(value) > static_cast< typename std::underlying_type<Value>::type >(enumValue.value); \
  } \
  friend bool operator <=(Value value, const name& enumValue) \
  { \
    return static_cast< typename std::underlying_type<Value>::type >(value) <= static_cast< typename std::underlying_type<Value>::type >(enumValue.value); \
  } \
  friend bool operator >=(Value value, const name& enumValue) \
  { \
    return static_cast< typename std::underlying_type<Value>::type >(value) >= static_cast< typename std::underlying_type<Value>::type >(enumValue.value); \
  }

#define ENUMERATION_FLAG_OPERATORS(name) \
  DEFINE_ENUM_BOOLEAN_OPERATORS(Value, friend) \
  _DEFINE_ENUM_BOOLEAN_OPERATORS(name, friend,, ,.value)

#define ENUMERATION_MASK_OPERATORS(maskName, flagName) \
  DEFINE_ENUM_MASK_BOOLEAN_OPERATORS(maskName::Value, flagName::Value, friend) \
  _DEFINE_ENUM_MASK_BOOLEAN_OPERATORS(maskName, flagName, friend,, ,.value)


#define END_ENUMERATION \
}

#define ENUM_TO_STRING_DECLARATION(name) \
  static std::string toStdString(const name& enumValue); \
  std::string toStdString()const{return name::toStdString(*this);} \
  friend std::ostream& operator<<(std::ostream& o, const name& enumValue){return o<<enumValue.toStdString();} \
  friend std::wostream& operator<<(std::wostream& o, const name& enumValue){return o<<String::fromAnsi(enumValue.toStdString());} \
  friend void PrintTo(const name& enumValue, std::ostream* o){(*o)<<enumValue;}

#define ENUM_VALUE_AS_INTEGER(name) \
  underlying_type valueAsInteger() const \
  { \
    return static_cast<underlying_type>(this->value); \
  } \
  String valueAsHexCode() const \
  { \
    return integerToStringAsHex(valueAsInteger(), true); \
  }

#endif // #ifndef _BASE_ENUMMACROS_H_
