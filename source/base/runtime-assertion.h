#ifndef _BASE_RUNTIMEASSERTION_H_
#define _BASE_RUNTIMEASSERTION_H_

#include <dependencies.h>


namespace Base {


class RuntimeAssertion : nonconstructable
{
private:
  static int nRaisedDeveloperWarnings;

public:
  class AreAllowedInThisScope : noncopyable
  {
  private:
    static int allowedScopes;

  public:
    AreAllowedInThisScope();
    ~AreAllowedInThisScope();

    static bool areAllowed();
  };

  static void assertionExceptional(bool condition,
                                   const Ogre::String& strCondition,
                                   const Ogre::String& message,
                                   const char* file,
                                   int line);
  static void assertionWarning(bool condition,
                               const Ogre::String& strCondition,
                               const Ogre::String& message,
                               const char* file,
                               int line);
  static void assertionDeveloperWarning(bool condition,
                                        const Ogre::String& strCondition,
                                        const Ogre::String& message,
                                        const char* file,
                                        int line);

  static void throwExceptionIfDeveloperWarningWasRaised();

private:
  static Ogre::String createMessage(const Ogre::String& strCondition,
                                    const Ogre::String& message,
                                    const char* file,
                                    int line);
};


#if defined(runtimeAssertION_EXCEPTIONAL)
#define runtimeAssert(x, message) RuntimeAssertion::assertionExceptional((x), #x, message, __FILE__, __LINE__)

#elif defined(runtimeAssertION_DEVELOPER_WARNING)
#define runtimeAssert(x, message) RuntimeAssertion::assertionDeveloperWarning((x), #x, message, __FILE__, __LINE__)

#elif defined(runtimeAssertION_WARNING)
#define runtimeAssert(x, message) RuntimeAssertion::assertionWarning((x), #x, message, __FILE__, __LINE__)

#else
#define runtimeAssert(x, message)
#endif


} // namespace Base


#endif
