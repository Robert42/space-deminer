#include "module-manager.h"

namespace SpaceDeminer {
namespace Modules {

Manager::Manager()
{
  _scene = Scene::Interface::create();
  _graphic = Graphic::Interface::createDummy();
  _physic = Physic::Interface::createDummy();

  _graphic->init(sceneModule());
  _physic->init(sceneModule());
}

Manager::Ptr Manager::create()
{
  return Ptr(new Manager());
}

const Scene::Interface::Ptr& Manager::sceneModule()
{
  return _scene;
}

const Graphic::Interface::Ptr& Manager::graphicModule()
{
  return _graphic;
}

const Physic::Interface::Ptr& Manager::physicModule()
{
  return _physic;
}

} // namespace Modules
} // namespace SpaceDeminer
