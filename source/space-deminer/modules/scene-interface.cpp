#include "scene-interface.h"

#include <space-deminer/modules/graphic/entity-creator.h>

#include <framework/frame-signals.h>

#include <base/io/log.h>

namespace SpaceDeminer {
namespace Modules {
namespace Scene {

typedef Base::IO::Log Log;


class DummyEntityCreator : public Graphic::SimpleEntityCreator
{
public:
  void addContentToNode(Scene::Interface& scene, Ogre::SceneNode* ogreSceneNode, const Scene::EntitySettings&) override
  {
    Log::logWarning("SpaceDeminer::Modules::Scene::DummyEntityCreator WARNING: DummyEntityCreator was used to create an entity");

    Ogre::SceneManager* ogreSceneManager = scene.ogreSceneManager();

    Ogre::Entity* gizmo = ogreSceneManager->createEntity(generateUniqueName(), "gizmo.mesh");
    ogreSceneNode->attachObject(gizmo);
  }
};


Interface::Interface()
  : fallbackEntityCreator(new DummyEntityCreator)
{
  Ogre::Root& root = Ogre::Root::getSingleton();

  _ogreSceneManager = root.createSceneManager("DefaultSceneManager");
  ogreSceneManager()->setAmbientLight(Ogre::ColourValue(0.25f, 0.25f, 0.25f));

  Framework::FrameSignals::signalRenderingQueued().connect(std::bind(&Interface::onWait, this)).track(this->trackable);
}


Interface::~Interface()
{
  Ogre::Root& root = Ogre::Root::getSingleton();

  root.destroySceneManager(ogreSceneManager());
}


Interface::Ptr Interface::create()
{
  return Ptr(new Interface());
}


Ogre::SceneManager* Interface::ogreSceneManager()
{
  return _ogreSceneManager;
}


std::shared_ptr<Graphic::EntityCreator> Interface::entityCreator() const
{
  std::shared_ptr<Graphic::EntityCreator> entityCreator = this->_entityCreator.lock();

  if(!entityCreator)
    return this->fallbackEntityCreator;

  return entityCreator;
}


void Interface::setEntityCreator(const std::shared_ptr<Graphic::EntityCreator>& entityCreator)
{
  this->_entityCreator = entityCreator;
}


Signals::Signal<void(const Entity::Ptr&, const EntitySettings& entitySettings)>& Interface::signalEntityAdded()
{
  return _signalEntityAdded;
}


void Interface::addEntity(const EntitySettings& entitySettings)
{
  Entity::Ptr entity = entityCreator()->createEntity(*this, entitySettings);

  entities.push_back(entity);

  _signalEntityAdded(entity, entitySettings);

  entity->_copyPositionToNode();
}


void Interface::addLight(const vec3& position)
{
  Ogre::Light* light = ogreSceneManager()->createLight();
  light->setPosition(Ogre::Vector3_cast(position));
}


void Interface::onWait()
{
  for(const Entity::Ptr& entity : entities)
  {
    entity->_copyPositionToNode();
  }
}


} // namespace Scene
} // namespace Modules
} // namespace SpaceDeminer
