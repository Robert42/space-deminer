#ifndef SPACEDEMINER_MODULES_PHYSIC_PHYSICINTERFACE_H
#define SPACEDEMINER_MODULES_PHYSIC_PHYSICINTERFACE_H

#include <space-deminer/modules/scene-interface.h>

namespace SpaceDeminer {

using namespace Framework;

namespace Modules {
namespace Physic {

class Interface
{
public:
  typedef shared_ptr<Interface> Ptr;

public:
  Interface();
  virtual ~Interface();

  static Ptr createDummy();

  virtual void init(const Scene::Interface::Ptr& scene) = 0;
};

} // namespace Physic
} // namespace Modules
} // namespace SpaceDeminer

#endif // SPACEDEMINER_MODULES_PHYSIC_PHYSICINTERFACE_H
