#ifndef SPACEDEMINER_MODULES_SCENE_SCENEINTERFACE_H
#define SPACEDEMINER_MODULES_SCENE_SCENEINTERFACE_H

#include <base/signals.h>

#include <framework/declarations.h>

#include "scene/scene-entity.h"
#include "scene/entity-settings.h"

namespace SpaceDeminer {

using namespace Framework;

namespace Modules {
namespace Graphic {
class EntityCreator;
}
namespace Scene {


class Interface
{
public:
  typedef std::shared_ptr<Interface> Ptr;

public:
  Signals::Trackable trackable;

private:
  std::weak_ptr<Graphic::EntityCreator> _entityCreator;
  std::shared_ptr<Graphic::EntityCreator> fallbackEntityCreator;

  Ogre::SceneManager* _ogreSceneManager;
  std::list<Entity::Ptr> entities;
  Signals::CallableSignal<void(const Entity::Ptr&, const EntitySettings& entitySettings)> _signalEntityAdded;

private:
  Interface();

public:
  ~Interface();
  static Ptr create();

  Ogre::SceneManager* ogreSceneManager();

  std::shared_ptr<Graphic::EntityCreator> entityCreator() const;
  void setEntityCreator(const std::shared_ptr<Graphic::EntityCreator>& entityCreator);

  Signals::Signal<void(const Entity::Ptr&, const EntitySettings& entitySettings)>& signalEntityAdded();

  void addEntity(const EntitySettings& entitySettings);
  void addLight(const vec3& position);

private:
  void onWait();
};


} // namespace Scene
} // namespace Modules
} // namespace SpaceDeminer

#endif // SPACEDEMINER_MODULES_SCENE_SCENEINTERFACE_H
