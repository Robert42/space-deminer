#ifndef SPACEDEMINER_MODULES_GRAPHIC_DUMMY_ENTITYVISUALIZATION_H
#define SPACEDEMINER_MODULES_GRAPHIC_DUMMY_ENTITYVISUALIZATION_H

#include <space-deminer/modules/graphic/entity-creator.h>

namespace SpaceDeminer {
namespace Modules {
namespace Graphic {
namespace Dummy {

class EntityCreator : public Graphic::SimpleEntityCreator
{
public:
  void addContentToNode(Scene::Interface& scene, Ogre::SceneNode* ogreSceneNode, const Scene::EntitySettings& entitySettings) override;
};

} // namespace Dummy
} // namespace Graphic
} // namespace Modules
} // namespace SpaceDeminer

#endif // SPACEDEMINER_MODULES_GRAPHIC_DUMMY_ENTITYVISUALIZATION_H
