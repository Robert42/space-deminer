#include "dummygraphicmoduleimplementation.h"

#include "dummy-entity-creator.h"

#include <space-deminer/modules/graphic/entity-creator.h>

namespace SpaceDeminer {
namespace Modules {
namespace Graphic {
namespace Dummy {


DummyGraphicModuleImplementation::DummyGraphicModuleImplementation()
{
}


DummyGraphicModuleImplementation::~DummyGraphicModuleImplementation()
{
  background.reset();
}


void DummyGraphicModuleImplementation::init(const Scene::Interface::Ptr& scene)
{
  entityCreator = Graphic::EntityCreator::Ptr(new Dummy::EntityCreator());
  scene->setEntityCreator(entityCreator);

  Ogre::SceneManager* ogreSceneManager = scene->ogreSceneManager();

  background = MainMenuBackground::create(ogreSceneManager);
}


} // namespace Dummy

Interface::Ptr Interface::createDummy()
{
  return Ptr(new Dummy::DummyGraphicModuleImplementation);
}

} // namespace Graphic
} // namespace Modules
} // namespace SpaceDeminer
