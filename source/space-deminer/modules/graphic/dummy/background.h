#ifndef _SPACEDEMINER_DUMMY_GRAPHICMODULE_MAINMENUBACKGROUND_H_
#define _SPACEDEMINER_DUMMY_GRAPHICMODULE_MAINMENUBACKGROUND_H_

#include <dependencies.h>

#include <framework/application.h>
#include <framework/window/user-input/mouse.h>
#include <framework/window/user-input/keyboard.h>
#include <framework/developer-tools/debug-camera-controller.h>


namespace SpaceDeminer {

using namespace Framework;

namespace Modules {
namespace Graphic {
namespace Dummy {


class MainMenuBackground final
{
public:
  typedef shared_ptr<MainMenuBackground> Ptr;

public:
  Signals::Trackable trackable;

private:
  Ogre::SceneManager* ogreSceneManager;
  Ogre::Camera* ogreCamera;
  Ogre::SceneNode* ogreCameraNode;
  Ogre::Viewport* ogreViewport;

  DebugCameraController debugCameraController;

private:
  MainMenuBackground(Ogre::SceneManager* ogreSceneManager);

public:
  ~MainMenuBackground();

  bool onClick(KeyCode keyCode);

public:
  static Ptr create(Ogre::SceneManager* ogreSceneManager);

private:
  void rotateCamera();
};


} // namespace Dummy
} // namespace Graphic
} // namespace Modules
} // namespace SpaceDeminer

#endif
