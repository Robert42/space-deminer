#include "dummy-entity-creator.h"

#include <space-deminer/modules/scene-interface.h>

namespace SpaceDeminer {
namespace Modules {
namespace Graphic {
namespace Dummy {


void EntityCreator::addContentToNode(Scene::Interface& scene, Ogre::SceneNode* ogreSceneNode, const Scene::EntitySettings& entitySettings)
{
  Ogre::SceneManager* ogreSceneManager = scene.ogreSceneManager();

  std::string entityName = entitySettings.preferredName + "-" + generateUniqueName();

  if(entitySettings.isAsteroid)
  {
    Ogre::Entity* asteroid = ogreSceneManager->createEntity("ship-"+entityName, "hyper-mega-cool-alpha-asteroid.mesh");
    ogreSceneNode->attachObject(asteroid);
    ogreSceneNode->scale(3.f, 3.f, 3.f);
  }

  if(entitySettings.isShip)
  {
    Ogre::Entity* dummyShip = ogreSceneManager->createEntity("asteroid-"+entityName, "dummy-ship-001.mesh");
    ogreSceneNode->attachObject(dummyShip);
  }

  if(entitySettings.showGizmo)
  {
    Ogre::Entity* gizmo = ogreSceneManager->createEntity("gizmo-"+entityName, "gizmo.mesh");
    ogreSceneNode->attachObject(gizmo);
  }
}


} // namespace Dummy
} // namespace Graphic
} // namespace Modules
} // namespace SpaceDeminer
