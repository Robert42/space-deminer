#include <dependencies.h>
#include <framework/window/render-window.h>
#include <framework/frame-signals.h>

#include "background.h"


namespace SpaceDeminer {
namespace Modules {
namespace Graphic {
namespace Dummy {


MainMenuBackground::MainMenuBackground(Ogre::SceneManager* ogreSceneManager)
  : ogreSceneManager(ogreSceneManager),
    debugCameraController(InputDevice::ActionPriority::INGAME_DEBUG)
{
  Ogre::MaterialManager::getSingleton().setDefaultAnisotropy(5);

  ogreSceneManager->setSkyBox(true, "backgrounds/skybox/starfield-001");

  ogreCamera = ogreSceneManager->createCamera("dummy-camera");
  ogreCamera->setAutoAspectRatio(true);
  ogreCamera->setNearClipDistance(0.001f);
  ogreCamera->setFarClipDistance(100.f*1000.f);
  ogreCamera->pitch(Ogre::Degree(90.f));
  ogreCameraNode = ogreSceneManager->createSceneNode("dummy-camera-node");
  ogreCameraNode->attachObject(ogreCamera);


  ogreViewport = RenderWindow::ogreRenderWindow()->addViewport(ogreCamera, 0);

  Keyboard::signalKeyPressed().connect(Framework::InputDevice::ActionPriority::INGAME,
                                       std::bind(&MainMenuBackground::onClick, this, _1)).track(this->trackable);

  FrameSignals::signalRenderingQueued().connect(std::bind(&MainMenuBackground::rotateCamera, this)).track(this->trackable);
}


MainMenuBackground::~MainMenuBackground()
{
  ogreSceneManager->destroyAllCameras();
  RenderWindow::ogreRenderWindow()->removeViewport(ogreViewport->getZOrder());
}


MainMenuBackground::Ptr MainMenuBackground::create(Ogre::SceneManager* ogreSceneManager)
{
  return Ptr(new MainMenuBackground(ogreSceneManager));
}


bool MainMenuBackground::onClick(KeyCode keyCode)
{
  std::cout << "[" << Keyboard::labelOf(keyCode) << "]" << std::endl;

  if(keyCode == KeyCode::S)
    ogreSceneManager->setSkyBox(true, "backgrounds/skybox/starfield-001");
  else if(keyCode == KeyCode::T)
    ogreSceneManager->setSkyBox(true, "test-sky-box");
  else
    return false;

  return true;
}


void MainMenuBackground::rotateCamera()
{
  debugCameraController.coordinate.applyToOgreSceneNode(ogreCameraNode);
}



} // namespace Dummy
} // namespace Graphic
} // namespace Modules
} // namespace SpaceDeminer
