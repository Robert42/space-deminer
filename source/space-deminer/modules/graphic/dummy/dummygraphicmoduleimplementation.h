#ifndef SPACEDEMINER_MODULES_GRAPHIC_DUMMY_DUMMYGRAPHICMODULEIMPLEMENTATION_H
#define SPACEDEMINER_MODULES_GRAPHIC_DUMMY_DUMMYGRAPHICMODULEIMPLEMENTATION_H

#include <space-deminer/modules/graphic-interface.h>

#include "background.h"

namespace SpaceDeminer {
namespace Modules {
namespace Graphic {
class EntityCreator;

namespace Dummy {

class DummyGraphicModuleImplementation : public Graphic::Interface
{
private:
  std::shared_ptr<EntityCreator> entityCreator;

public:
  MainMenuBackground::Ptr background;

  DummyGraphicModuleImplementation();
  ~DummyGraphicModuleImplementation();

  void init(const Scene::Interface::Ptr& scene) override;
};

} // namespace Dummy
} // namespace Graphic
} // namespace Modules
} // namespace SpaceDeminer

#endif // SPACEDEMINER_MODULES_GRAPHIC_DUMMY_DUMMYGRAPHICMODULEIMPLEMENTATION_H
