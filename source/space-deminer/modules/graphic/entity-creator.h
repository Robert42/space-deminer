#ifndef SPACEDEMINER_MODULES_GRAPHIC_ENTITYCREATOR_H
#define SPACEDEMINER_MODULES_GRAPHIC_ENTITYCREATOR_H

#include <space-deminer/modules/scene/scene-entity.h>
#include <space-deminer/modules/scene/entity-settings.h>

namespace SpaceDeminer {
namespace Modules {
namespace Scene {
class Interface;
}
namespace Graphic {

class EntityCreator
{
public:
  typedef std::shared_ptr<EntityCreator> Ptr;

public:
  EntityCreator();

  virtual Scene::Entity::Ptr createEntity(Scene::Interface& scene, const Scene::EntitySettings& entitySettings) = 0;

  static Ogre::String generateUniqueName();

protected:
};

class SimpleEntityCreator : public EntityCreator
{
public:
  typedef std::shared_ptr<SimpleEntityCreator> Ptr;

public:
  SimpleEntityCreator();

  Scene::Entity::Ptr createEntity(Scene::Interface& scene, const Scene::EntitySettings& entitySettings) override;

  virtual void addContentToNode(Scene::Interface& scene, Ogre::SceneNode* ogreSceneNode, const Scene::EntitySettings& entitySettings) = 0;
};

} // namespace Graphic
} // namespace Modules
} // namespace SpaceDeminer

#endif // SPACEDEMINER_MODULES_GRAPHIC_ENTITYCREATOR_H
