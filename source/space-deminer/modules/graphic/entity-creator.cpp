#include "entity-creator.h"

#include <space-deminer/modules/scene-interface.h>

#include <base/strings/string.h>

namespace SpaceDeminer {
namespace Modules {
namespace Graphic {


EntityCreator::EntityCreator()
{
}


Ogre::String EntityCreator::generateUniqueName()
{
  static int i=0;

  return String::compose("%1", i++).toOgreString();
}


SimpleEntityCreator::SimpleEntityCreator()
{
}


Scene::Entity::Ptr SimpleEntityCreator::createEntity(Scene::Interface& scene, const Scene::EntitySettings& entitySettings)
{
  Ogre::SceneManager* ogreSceneManager = scene.ogreSceneManager();

  Ogre::SceneNode* ogreSceneNode = ogreSceneManager->getRootSceneNode()->createChildSceneNode(generateUniqueName());

  addContentToNode(scene, ogreSceneNode, entitySettings);

  Scene::Entity::Ptr entity(new Scene::Entity(ogreSceneNode));

  entity->setCoordinate(entitySettings.coordinate);

  return entity;
}


} // namespace Graphic
} // namespace Modules
} // namespace SpaceDeminer
