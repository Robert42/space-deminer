#include "physic-module-implementation-bullet.h"

namespace SpaceDeminer {
namespace Modules {
namespace Physic {

BulletPhysicModuleImplementation::BulletPhysicModuleImplementation()
{
}

void BulletPhysicModuleImplementation::init(const Scene::Interface::Ptr& scene)
{
  (void)scene;
}

Interface::Ptr Interface::createDummy()
{
  return Ptr(new BulletPhysicModuleImplementation);
}

} // namespace Physic
} // namespace Modules
} // namespace SpaceDeminer
