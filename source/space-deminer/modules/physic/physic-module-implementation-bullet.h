#ifndef SPACEDEMINER_MODULES_PHYSIC_PHYSICMODULEIMPLEMENTATION_BULLET_H
#define SPACEDEMINER_MODULES_PHYSIC_PHYSICMODULEIMPLEMENTATION_BULLET_H

#include <space-deminer/modules/physic-interface.h>

namespace SpaceDeminer {
namespace Modules {
namespace Physic {

class BulletPhysicModuleImplementation : public Interface
{
public:
  BulletPhysicModuleImplementation();

private:
  void init(const Scene::Interface::Ptr& scene);
};

} // namespace Physic
} // namespace Modules
} // namespace SpaceDeminer

#endif // SPACEDEMINER_MODULES_PHYSIC_PHYSICMODULEIMPLEMENTATIONODE_H
