#ifndef SPACEDEMINER_MODULES_SCENE_SCENEENTITY_H
#define SPACEDEMINER_MODULES_SCENE_SCENEENTITY_H

#include <base/geometry/coordinate.h>

namespace SpaceDeminer {

using namespace  Base;

namespace Modules {
namespace Scene {

class Interface;

class Entity
{
public:
  typedef std::shared_ptr<Entity> Ptr;

private:
  Coordinate _coordinate;
  Ogre::SceneNode* const _node;

public:
  Entity(Ogre::SceneNode* node);

public:
  const Coordinate& coordinate() const;
  void setCoordinate(const Coordinate& coordinate);

  Ogre::SceneNode* sceneNode();

  void _copyPositionToNode();
};


} // namespace Scene
} // namespace Modules
} // namespace SpaceDeminer

#endif // SPACEDEMINER_MODULES_SCENEENTITY_H
