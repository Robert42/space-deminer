#include "entity-settings.h"

namespace SpaceDeminer {
namespace Modules {
namespace Scene {

EntitySettings::EntitySettings()
{
  preferredName = "entity";
  showGizmo = false;
  isAsteroid = false;
  isShip = false;
}

} // namespace Scene
} // namespace Modules
} // namespace SpaceDeminer
