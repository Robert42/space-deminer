#include "scene-entity.h"

namespace SpaceDeminer {
namespace Modules {
namespace Scene {


Entity::Entity(Ogre::SceneNode* node)
  : _node(node)
{
}


const Coordinate& Entity::coordinate() const
{
  return _coordinate;
}


void Entity::setCoordinate(const Coordinate& coordinate)
{
  this->_coordinate = coordinate;
}


Ogre::SceneNode* Entity::sceneNode()
{
  return _node;
}


void Entity::_copyPositionToNode()
{
  coordinate().applyToOgreSceneNode(sceneNode());
}


} // namespace Scene
} // namespace Modules
} // namespace SpaceDeminer
