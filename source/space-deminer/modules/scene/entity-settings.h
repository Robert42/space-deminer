#ifndef SPACEDEMINER_MODULES_SCENE_ENTITYSETTINGS_H
#define SPACEDEMINER_MODULES_SCENE_ENTITYSETTINGS_H

#include <base/geometry/coordinate.h>

namespace SpaceDeminer {

using namespace Base;

namespace Modules {
namespace Scene {

class EntitySettings
{
public:
  EntitySettings();

  Coordinate coordinate;
  std::string preferredName;

  bool showGizmo;
  bool isAsteroid;
  bool isShip;
};

} // namespace Scene
} // namespace Modules
} // namespace SpaceDeminer

#endif // SPACEDEMINER_MODULES_SCENE_ENTITYSETTINGS_H
