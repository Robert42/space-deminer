#ifndef _SPACEDEMINER_MODULES_GRAPHICINTERFACE_
#define _SPACEDEMINER_MODULES_GRAPHICINTERFACE_

#include <space-deminer/modules/scene-interface.h>

namespace SpaceDeminer {
using namespace Framework;

namespace Modules {
namespace Graphic {


class Interface
{
public:
  typedef shared_ptr<Interface> Ptr;

public:

  Interface();
  virtual ~Interface();

  static Ptr createDummy();

  virtual void init(const Scene::Interface::Ptr& scene) = 0;
};


} // namespace Graphic
} // namespace Modules
} // namespace SpaceDeminer

#endif
