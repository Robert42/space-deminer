#ifndef SPACEDEMINER_SPACEDEMINERAPPLET_H
#define SPACEDEMINER_SPACEDEMINERAPPLET_H

#include <framework/applet.h>

#include <space-deminer/module-manager.h>

namespace SpaceDeminer {

using namespace Framework;

class Applet : public Framework::Applet
{
private:
  Modules::Manager::Ptr modules;

private:
  Applet();

public:
  ~Applet();

  static Ptr create();
};

} // namespace SpaceDeminer

#endif // SPACEDEMINER_SPACEDEMINERAPPLET_H
