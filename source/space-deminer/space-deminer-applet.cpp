#include "space-deminer-applet.h"
#include <framework/developer-tools/terminal.h>

namespace SpaceDeminer {

Applet::Applet()
{
  this->modules = Modules::Manager::create();

  Modules::Scene::Interface::Ptr scene = this->modules->sceneModule();

  Modules::Scene::EntitySettings entitySettings;
  entitySettings.showGizmo = true;
  entitySettings.coordinate = Coordinate(vec3(4, 5, 0));
  scene->addEntity(entitySettings);

  entitySettings.showGizmo = false;
  entitySettings.coordinate = Coordinate(vec3(0, 15, 0));
  entitySettings.isAsteroid = true;
  scene->addEntity(entitySettings);

  entitySettings.coordinate = Coordinate(vec3(-1, 2, 0));
  entitySettings.isAsteroid = false;
  entitySettings.isShip = true;
  scene->addEntity(entitySettings);

  scene->addLight(vec3(28.f, 40.f, 7.f));
}

Applet::~Applet()
{
}

Applet::Ptr Applet::create()
{
  return Ptr(new Applet);
}

} // namespace SpaceDeminer
