#include <dependencies.h>

#include <space-deminer/modules/graphic/dummy/dummygraphicmoduleimplementation.h>
#include <space-deminer/space-deminer-application.h>
#include <space-deminer/space-deminer-applet.h>

#include <framework/plugin-manager.h>
#include <framework/applet.h>

#include <base/io/special-directories.h>


namespace SpaceDeminer {


Application::Application() : Framework::Application("space-deminer")
{
}


Application::~Application()
{
}


void Application::loadPlugins()
{
  PluginManager pluginManager;

  pluginManager.loadRequiredPlugin("RenderSystem_GL");
  pluginManager.tryLoadPlugin("RenderSystem_GL3Plus");

  pluginManager.loadRequiredPlugin("Plugin_ParticleFX");
  pluginManager.loadRequiredPlugin("Plugin_PCZSceneManager");
  pluginManager.loadRequiredPlugin("Plugin_OctreeZone");
  pluginManager.loadRequiredPlugin("Plugin_OctreeSceneManager");
}


String Application::windowTitle()
{
  return "Space-Deminer";
}


shared_ptr<Framework::Applet> Application::createMainApplet()
{
  return Applet::create();
}


IO::RegularFile Application::splashScreenFile() const
{
  return IO::RegularFile(assetsDirectory() / "space-deminer" | "splashscreen.png");
}


}


int mainMethod()
{
  SpaceDeminer::Application application;

  application.run();

  return 0;
}
