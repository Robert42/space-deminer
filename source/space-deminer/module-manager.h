#ifndef SPACEDEMINER_MODULES_MODULEMANAGER_H
#define SPACEDEMINER_MODULES_MODULEMANAGER_H

#include "modules/graphic-interface.h"
#include "modules/physic-interface.h"
#include "modules/scene-interface.h"

#include <framework/declarations.h>

namespace SpaceDeminer {
namespace Modules {

class Manager
{
public:
  typedef std::shared_ptr<Manager> Ptr;

private:
  Scene::Interface::Ptr _scene;
  Graphic::Interface::Ptr _graphic;
  Physic::Interface::Ptr _physic;

private:
  Manager();

public:
  static Ptr create();

  const Scene::Interface::Ptr& sceneModule();
  const Graphic::Interface::Ptr& graphicModule();
  const Physic::Interface::Ptr& physicModule();
};

} // namespace Modules
} // namespace SpaceDeminer

#endif // SPACEDEMINER_MODULES_MODULEMANAGER_H
