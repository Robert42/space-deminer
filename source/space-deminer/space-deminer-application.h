#ifndef _SPACEDEMINER_APPLICATION_H_
#define _SPACEDEMINER_APPLICATION_H_

#include <framework/application.h>

namespace SpaceDeminer {
using namespace Framework;

/** The Space-Deminer application.
 *
 */
class Application : public Framework::Application
{
public:
  Application();
  ~Application();

  void loadPlugins() override;

  String windowTitle() override;

protected:
  shared_ptr<Framework::Applet> createMainApplet() final override;
  IO::RegularFile splashScreenFile() const final override;
};


}

#endif
