#include <dependencies.h>


namespace glm {
namespace detail {


void PrintTo(const vec2& vec, std::ostream* o)
{
  *o << vec;
}

void PrintTo(const vec3& vec, std::ostream* o)
{
  *o << vec;
}

void PrintTo(const vec4& vec, std::ostream* o)
{
  *o << vec;
}

void PrintTo(const ivec2& vec, std::ostream* o)
{
  *o << vec;
}

void PrintTo(const ivec3& vec, std::ostream* o)
{
  *o << vec;
}

void PrintTo(const ivec4& vec, std::ostream* o)
{
  *o << vec;
}

void PrintTo(const uvec2& vec, std::ostream* o)
{
  *o << vec;
}

void PrintTo(const uvec3& vec, std::ostream* o)
{
  *o << vec;
}

void PrintTo(const uvec4& vec, std::ostream* o)
{
  *o << vec;
}

void PrintTo(const mat2x2& matrix, std::ostream* o)
{
  *o << matrix;
}

void PrintTo(const mat2x3& matrix, std::ostream* o)
{
  *o << matrix;
}

void PrintTo(const mat2x4& matrix, std::ostream* o)
{
  *o << matrix;
}

void PrintTo(const mat3x2& matrix, std::ostream* o)
{
  *o << matrix;
}

void PrintTo(const mat3x3& matrix, std::ostream* o)
{
  *o << matrix;
}

void PrintTo(const mat3x4& matrix, std::ostream* o)
{
  *o << matrix;
}

void PrintTo(const mat4x2& matrix, std::ostream* o)
{
  *o << matrix;
}

void PrintTo(const mat4x3& matrix, std::ostream* o)
{
  *o << matrix;
}

void PrintTo(const mat4x4& matrix, std::ostream* o)
{
  *o << matrix;
}


} // namespace detail
} // namespace glm

namespace Base {


const Ogre::String UnitTest_ResourceGroup = "UNITTEST-TESTRESOURCES";


} // namespace Base

