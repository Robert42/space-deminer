#include <dependencies.h>


TEST(boost_filesyste_path, gtest_printing)
{
  EXPECT_EQ("\"/a/b\"", ::testing::PrintToString(Base::IO::Path("/a/b")));
}

TEST(boost_filesyste_path, gtest_default_print_to)
{
  std::ostringstream oss;

  ::testing::internal::DefaultPrintTo(::testing::internal::IsContainer(),
                                      ::testing::internal::false_type(),
                                      Base::IO::Path("/a/b"),
                                      &oss);

  EXPECT_EQ("{ \"/\", \"a\", \"b\" }", oss.str());
}

TEST(glm_vec, gtest_printing)
{
  EXPECT_EQ("[    1.000,    2.000]", ::testing::PrintToString(glm::vec2(1, 2)));
  EXPECT_EQ("[        1,        2]", ::testing::PrintToString(glm::ivec2(1, 2)));
  EXPECT_EQ("[        1,        2]", ::testing::PrintToString(glm::uvec2(1, 2)));
  EXPECT_EQ("[    1.000,    2.000,    3.000]", ::testing::PrintToString(glm::vec3(1, 2, 3)));
  EXPECT_EQ("[        1,        2,        3]", ::testing::PrintToString(glm::ivec3(1, 2, 3)));
  EXPECT_EQ("[        1,        2,        3]", ::testing::PrintToString(glm::uvec3(1, 2, 3)));
  EXPECT_EQ("[    1.000,    2.000,    3.000,    4.000]", ::testing::PrintToString(glm::vec4(1, 2, 3, 4)));
  EXPECT_EQ("[        1,        2,        3,        4]", ::testing::PrintToString(glm::ivec4(1, 2, 3, 4)));
  EXPECT_EQ("[        1,        2,        3,        4]", ::testing::PrintToString(glm::uvec4(1, 2, 3, 4)));
}

TEST(glm_mat, gtest_printing)
{
  EXPECT_EQ("\n[[    1.000,    2.000]\n [    3.000,    4.000]]", ::testing::PrintToString(glm::mat2(1, 2, 3, 4)));
  EXPECT_EQ("\n[[    1.000,    2.000,    3.000]\n [    4.000,    5.000,    6.000]\n [    7.000,    8.000,    9.000]]", ::testing::PrintToString(glm::mat3(1, 2, 3, 4, 5, 6, 7, 8, 9)));
  EXPECT_EQ("\n[[    1.000,    2.000,    3.000,    4.000]\n [    5.000,    6.000,    7.000,    8.000]\n [    9.000,   10.000,   11.000,   12.000]\n [   13.000,   14.000,   15.000,   16.000]]", ::testing::PrintToString(glm::mat4(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16)));

  EXPECT_EQ("\n[[    1.000,    2.000]\n [    3.000,    4.000]]", ::testing::PrintToString(glm::mat2x2(1, 2, 3, 4)));
  EXPECT_EQ("\n[[    1.000,    2.000,    3.000]\n [    4.000,    5.000,    6.000]]", ::testing::PrintToString(glm::mat2x3(1, 2, 3, 4, 5, 6)));
  EXPECT_EQ("\n[[    1.000,    2.000,    3.000,    4.000]\n [    5.000,    6.000,    7.000,    8.000]]", ::testing::PrintToString(glm::mat2x4(1, 2, 3, 4, 5, 6, 7, 8)));

  EXPECT_EQ("\n[[    1.000,    2.000]\n [    3.000,    4.000]\n [    5.000,    6.000]]", ::testing::PrintToString(glm::mat3x2(1, 2, 3, 4, 5, 6)));
  EXPECT_EQ("\n[[    1.000,    2.000,    3.000]\n [    4.000,    5.000,    6.000]\n [    7.000,    8.000,    9.000]]", ::testing::PrintToString(glm::mat3x3(1, 2, 3, 4, 5, 6, 7, 8, 9)));
  EXPECT_EQ("\n[[    1.000,    2.000,    3.000,    4.000]\n [    5.000,    6.000,    7.000,    8.000]\n [    9.000,   10.000,   11.000,   12.000]]", ::testing::PrintToString(glm::mat3x4(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)));

  EXPECT_EQ("\n[[    1.000,    2.000]\n [    3.000,    4.000]\n [    5.000,    6.000]\n [    7.000,    8.000]]", ::testing::PrintToString(glm::mat4x2(1, 2, 3, 4, 5, 6, 7, 8)));
  EXPECT_EQ("\n[[    1.000,    2.000,    3.000]\n [    4.000,    5.000,    6.000]\n [    7.000,    8.000,    9.000]\n [   10.000,   11.000,   12.000]]", ::testing::PrintToString(glm::mat4x3(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)));
  EXPECT_EQ("\n[[    1.000,    2.000,    3.000,    4.000]\n [    5.000,    6.000,    7.000,    8.000]\n [    9.000,   10.000,   11.000,   12.000]\n [   13.000,   14.000,   15.000,   16.000]]", ::testing::PrintToString(glm::mat4x4(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16)));
}

TEST(glm_Swizzle, gtest_swizzle)
{
  using glm::vec2;

  vec2 a(1, 2);
  vec2 b(42, 42);
  b.yx() = a.xy();

  // This test is here to get notified, when glm allows assigning values using the swizzle operators
  EXPECT_EQ(vec2(42, 42), b);
}

namespace Base {


TEST(base, constants)
{
  EXPECT_FLOAT_EQ(1.f, vec2_unitX.x);
  EXPECT_FLOAT_EQ(0.f, vec2_unitX.y);

  EXPECT_FLOAT_EQ(0.f, vec2_unitY.x);
  EXPECT_FLOAT_EQ(1.f, vec2_unitY.y);

  EXPECT_FLOAT_EQ(1.f, vec3_unitX.x);
  EXPECT_FLOAT_EQ(0.f, vec3_unitX.y);
  EXPECT_FLOAT_EQ(0.f, vec3_unitX.z);

  EXPECT_FLOAT_EQ(0.f, vec3_unitY.x);
  EXPECT_FLOAT_EQ(1.f, vec3_unitY.y);
  EXPECT_FLOAT_EQ(0.f, vec3_unitY.z);

  EXPECT_FLOAT_EQ(0.f, vec3_unitZ.x);
  EXPECT_FLOAT_EQ(0.f, vec3_unitZ.y);
  EXPECT_FLOAT_EQ(1.f, vec3_unitZ.z);

  EXPECT_FLOAT_EQ( 2.7182818284590, e);
  EXPECT_FLOAT_EQ( 3.1415926535897, pi);
  EXPECT_FLOAT_EQ( 6.2831853071799, two_pi);
  EXPECT_FLOAT_EQ( 1.5707963267949, half_pi);
  EXPECT_FLOAT_EQ( 2.3025850929940, ln_ten);
  EXPECT_FLOAT_EQ( 0.6931471805599, ln_two);
  EXPECT_FLOAT_EQ( 1.0000000000000, one);
  EXPECT_FLOAT_EQ( 0.3183098861838, one_over_pi);
  EXPECT_FLOAT_EQ( 0.7071067811865, one_over_sqrt_two);
  EXPECT_FLOAT_EQ( 0.7853981633974, quarter_pi);
  EXPECT_FLOAT_EQ( 2.2360679774998, sqrt_five);
  EXPECT_FLOAT_EQ( 1.7724538509055, sqrt_pi);
  EXPECT_FLOAT_EQ( 1.7320508075689, sqrt_three);
  EXPECT_FLOAT_EQ( 1.4142135623731, sqrt_two);
  EXPECT_FLOAT_EQ( 2.5066282746310, sqrt_two_pi);
  EXPECT_FLOAT_EQ( 0.3333333333333, third);
  EXPECT_FLOAT_EQ( 0.6366197723676, two_over_pi);
  EXPECT_FLOAT_EQ( 1.1283791670955, two_over_sqrt_pi);
  EXPECT_FLOAT_EQ( 0.6666666666666, two_thirds);
  EXPECT_FLOAT_EQ( 0.0000000000000, zero);

}


}


TEST(qt, qHash_std_shared_ptr)
{
  int* integer = new int(42);
  std::shared_ptr<int> a(integer);
  std::shared_ptr<int> b(a);

  EXPECT_EQ(qHash(integer), qHash(a));
  EXPECT_EQ(qHash(integer), qHash(b));
}
