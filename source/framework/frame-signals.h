#ifndef _FRAMEWORK_FRAME_SIGNALS_H_
#define _FRAMEWORK_FRAME_SIGNALS_H_


#include <dependencies.h>

#include <base/signals/signal.h>
#include <base/singleton.h>


namespace Framework {
using namespace Base;


class FrameSignals final : public Ogre::FrameListener, public Singleton<FrameSignals>
{
public:
  typedef Signals::Signal<void(real)> Signal;
  typedef Signals::Signal<bool(real)> BlockerSignal;

  class Blocker final
  {
  public:
    Signals::Trackable trackable;
  public:
    Blocker();
  private:
    static bool block(real);
  };

private:
  typedef Signals::CallableSignal<void(real)> CallableSignal;
  typedef Signals::CallableSignal<bool(real)> CallableBlockerSignal;

  CallableSignal _signalFrameStart, _signalRenderingQueued, _signalFrameEnd;
  CallableSignal _signalCallOnce_a, _signalCallOnce_b;
  CallableBlockerSignal _signalBlocker;

  Signal* _signalCallOnce;

  bool signalsAreBlocked;

public:

  static Signal& signalFrameStart();
  static Signal& signalRenderingQueued();
  static Signal& signalFrameEnd();
  static BlockerSignal& signalBlocker();

  static Signals::Connection callOnce(const std::function<void(real)>& slot);

public:
  FrameSignals();
  ~FrameSignals();

private:
  bool frameStarted(const Ogre::FrameEvent& evt) final override;
  bool frameRenderingQueued(const Ogre::FrameEvent& evt) final override;
  bool frameEnded(const Ogre::FrameEvent& evt) final override;
};


}

#endif
