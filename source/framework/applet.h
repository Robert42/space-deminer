#ifndef FRAMEWORK_APPLET_H
#define FRAMEWORK_APPLET_H

#include <dependencies.h>

namespace Framework {

using namespace Base;

class Applet
{
public:
  typedef shared_ptr<Applet> Ptr;

public:
  Applet();
  virtual ~Applet();
};

} // namespace Framework

#endif // FRAMEWORK_APPLET_H
