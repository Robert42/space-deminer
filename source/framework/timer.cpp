#include <framework/timer.h>
#include <framework/frame-signals.h>
#include <framework/frame-timer.h>

namespace Framework {


PassiveTimer::PassiveTimer(real intervall)
  : _intervall(intervall),
    _passedTime(0.)
{
}

void PassiveTimer::setIntervall(real intervall)
{
  this->_intervall = intervall;
}

real PassiveTimer::intervall() const
{
  return _intervall;
}

bool PassiveTimer::tick(real timeStep)
{
  _passedTime += timeStep;

  if(_passedTime >= intervall())
  {
    _passedTime = 0.;
    return true;
  }

  return false;
}

bool PassiveTimer::tick()
{
  return tick(FrameTimer::averageTime());
}


// ====


ActiveTimer::ActiveTimer()
{
}


ActiveTimer::~ActiveTimer()
{
  stop();
}


void ActiveTimer::startOnce(real time)
{
  stop();
  passiveTimer.setIntervall(time);
  connection = FrameSignals::signalRenderingQueued().connect(std::bind(&ActiveTimer::waitOnce, this, _1));
  connection.trackManually();
}


void ActiveTimer::start(real time)
{
  stop();
  passiveTimer.setIntervall(time);
  connection = FrameSignals::signalRenderingQueued().connect(std::bind(&ActiveTimer::wait, this, _1));
  connection.trackManually();
}


void ActiveTimer::stop()
{
  connection.disconnect();
}


Signals::Signal<void()>& ActiveTimer::signal()
{
  return _signal;
}


void ActiveTimer::waitOnce(real time)
{
  if(passiveTimer.tick(time))
  {
    stop();
    _signal();
  }
}


void ActiveTimer::wait(real time)
{
  if(passiveTimer.tick(time))
    _signal();
}


}
