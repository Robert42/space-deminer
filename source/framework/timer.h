#ifndef _FRAMEWORK_TIMER_H_
#define _FRAMEWORK_TIMER_H_

#include <base/signals/signal.h>

namespace Framework {
using namespace Base;


/**
 * This timer doesn't connet to any system timer or so. You have
 * to give the passed time using the tick method. Its return value will tell you,
 * whether the next interval has past.
 */
class PassiveTimer
{
public:
  PassiveTimer(real intervall = 1.0);

  void setIntervall(real intervall);
  real intervall() const;

  bool tick();
  bool tick(real timeStep);

private:
  real _intervall;
  real _passedTime;
};


class ActiveTimer
{
private:
  PassiveTimer passiveTimer;
  bool onlyOnce;

  Signals::CallableSignal<void()> _signal;
  Signals::Connection connection;

public:
  ActiveTimer();
  ~ActiveTimer();

  void startOnce(real time);
  void start(real time);
  void stop();

  Signals::Signal<void()>& signal();

private:
  void waitOnce(real time);
  void wait(real time);
};


}

#endif
