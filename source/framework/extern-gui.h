#ifndef _FRAMEWORK_EXTERN_GUI_H_
#define _FRAMEWORK_EXTERN_GUI_H_

#include <dependencies.h>
#include <base/io/regular-file.h>
#include <base/singleton.h>

namespace Framework {
using namespace Base;

class ExternGui : public Singleton<ExternGui>
{
public:
  typedef shared_ptr<ExternGui> Ptr;

protected:
  ExternGui()
  {
  }

public:
  virtual ~ExternGui()
  {
  }

  static Ptr create();

public:
  static void showSplashscreen(const IO::RegularFile& imageFile);
  static void hideSplashscreen();

  static void fatalErrorDialog(const String& message, const String& informativeText, const String& details);

private:
  virtual void showSplashscreenImpl(const IO::RegularFile& imageFile) = 0;
  virtual void hideSplashscreenImpl() = 0;

  virtual void fatalErrorDialogImpl(const String& message, const String& informativeText, const String& details) = 0;
};

} // namespace Framework

#endif
