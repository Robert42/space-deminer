#ifndef FRAMEWORK_CEGUISYSTEM_H
#define FRAMEWORK_CEGUISYSTEM_H


#include <framework/window/user-input/mouse.h>
#include <framework/window/user-input/keyboard.h>

#include <base/signals/trackable.h>


namespace Framework {

class CeguiSystem
{
private:
  Signals::Trackable trackable;

public:
  CeguiSystem();
  ~CeguiSystem();

  static bool handleMouseMoved(CEGUI::GUIContext*ceguiGuiContext);
  static bool handleMouseButtonPressed(const MouseButton& button, CEGUI::GUIContext*ceguiGuiContext);
  static bool handleMouseButtonReleased(const MouseButton& button, CEGUI::GUIContext*ceguiGuiContext);
  static bool handleKeyPressed(const KeyCode& keyCode, CEGUI::GUIContext*ceguiGuiContext);
  static bool handleKeyReleased(const KeyCode& keyCode, CEGUI::GUIContext*ceguiGuiContext);
  static bool handleTextInput(String::value_type character, CEGUI::GUIContext*ceguiGuiContext);

  static void updateMousePosition();
};

} // namespace Framework

#endif // FRAMEWORK_CEGUISYSTEM_H
