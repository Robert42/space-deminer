#ifndef FRAMEWOK_DECLARATIONS_H
#define FRAMEWOK_DECLARATIONS_H

#include <dependencies.h>

namespace Framework {


using namespace Base;

class Application;
class Applet;
class RenderWindow;
class UserInput;
class DeveloperSettings;
class DeveloperHud;
class FrameTimer;
class Keyboard;
class Mouse;


} // namespace Framework

#endif // FRAMEWOK_DECLARATIONS_H
