#ifndef _FRAMEWORK_FRAMERATE_TIMER_H_
#define _FRAMEWORK_FRAMERATE_TIMER_H_

#include <dependencies.h>

#include <base/average-of.h>

namespace Framework {
using namespace Base;

class FrameTimer
{
public:
  typedef std::unique_ptr<FrameTimer> Ptr;

  typedef unsigned long Microseconds;
  typedef real Seconds;
  typedef real FramesPerSecond;

private:
  Ogre::Timer timer_;

  static bool hasInstance_;

  static Microseconds minFrameTime_;
  static Microseconds fixedFrameTime_;

  static Microseconds frameTimeWithoutLocking_;
  static Microseconds frameTime_;
  static Microseconds totalMicroseconds_;


  static Seconds constrainedTime_;
  static Seconds averageTime_;

  static Seconds totalSeconds_;

  AverageOf<Seconds> averageOfTimes_;

private:
  FrameTimer();

public:
  ~FrameTimer();

  static Ptr create();

  void update();
  void reset();

  static bool ignoreNextFrame;

  static void lockMaxFramerate(FramesPerSecond maxFramerate);
  static FramesPerSecond lockedFrameRate();

  static void setFixedFramerate(FramesPerSecond fixedFramerate);
  static FramesPerSecond fixedFrameRate();

  static Microseconds frameTimeWithoutLocking();
  static Microseconds frameTime();

  static Seconds constrainedTime();
  static Seconds averageTime();

  static Microseconds totalMicroseconds();
  static Seconds totalSeconds();

public:
  static Microseconds framerateToMicroSeconds(FramesPerSecond framerate);
  static FramesPerSecond microsecondsToFramerate(Microseconds microseconds);
  static Microseconds secondsToMicroseconds(Seconds seconds);
  static Seconds microsecondsToSeconds(Microseconds microseconds);

private:
  void update(Microseconds time);
};


}


#endif // _FRAMEWORK_FRAMERATE_TIMER_H_
