#ifndef ADVANCEDGRAPHICS_SAMPLES_FALLBACKSAMPLE_H
#define ADVANCEDGRAPHICS_SAMPLES_FALLBACKSAMPLE_H

#include "sample.h"

namespace AdvancedGraphics {
namespace Samples {

class FallbackSample : public Sample
{
public:

  FallbackSample();
  virtual ~FallbackSample();
};

} // namespace Samples
} // namespace AdvancedGraphics

#endif // ADVANCEDGRAPHICS_SAMPLES_FALLBACKSAMPLE_H
