#ifndef ADVANCEDGRAPHICS_SAMPLES_SAMPLEMANAGER_H
#define ADVANCEDGRAPHICS_SAMPLES_SAMPLEMANAGER_H

#include "sample.h"

#include <base/io/regular-file.h>
#include <base/singleton.h>

namespace AdvancedGraphics {
namespace Samples {

class SampleBrowser;
class SampleManager : SmartSingleton<SampleManager>
{
public:
  typedef shared_ptr<SampleManager> Ptr;

  friend class Sample;
  friend class SmartSingleton<SampleManager>;

private:
  Ogre::SceneManager* _sceneManager;
  Ogre::Camera* _camera;
  Ogre::Viewport* _viewport;

  weak_ptr<Sample> sample;
  shared_ptr<SampleBrowser> sampleBrowser;


private:
  SampleManager();

public:
  ~SampleManager();

public:
  static void loadSample(const Ogre::String& sampleName);

public:
  static Ogre::SceneManager& sceneManager();
  static Ogre::Camera& camera();
  static Ogre::Viewport& viewport();

private:
  Sample::Ptr createSampleFromFile(const IO::RegularFile& file);
};

} // namespace Samples
} // namespace AdvancedGraphics

#endif // ADVANCEDGRAPHICS_SAMPLES_SAMPLEMANAGER_H
