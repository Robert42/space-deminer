#include "scriptfile-sample.h"
#include "utilities.h"

#include <framework/scripting/initialization.h>


namespace AdvancedGraphics {
namespace Samples {


ScriptFileSample::ScriptFileSample(const IO::RegularFile& file)
{
  Utilities::registerScriptFunctions();
  Base::Scripting::executeScript(file,
                                 std::chrono::milliseconds(1000),
                                 Framework::Scripting::AccessMasks::GLOBAL |
                                 Framework::Scripting::AccessMasks::MATH |
                                 Framework::Scripting::AccessMasks::SPECIAL_USAGE);
  Utilities::unregisterScriptFunctions();
}


} // namespace Samples
} // namespace AdvancedGraphics
