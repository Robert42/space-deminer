#ifndef ADVANCEDGRAPHICS_SAMPLES_SCRIPTFILESAMPLE_H
#define ADVANCEDGRAPHICS_SAMPLES_SCRIPTFILESAMPLE_H

#include "sample.h"
#include "utilities.h"

#include <base/io/regular-file.h>

namespace AdvancedGraphics {
namespace Samples {

class ScriptFileSample : public Sample
{
  Utilities utilities;
public:
  ScriptFileSample(const IO::RegularFile& file);
};

} // namespace Samples
} // namespace AdvancedGraphics

#endif // ADVANCEDGRAPHICS_SAMPLES_SCRIPTFILESAMPLE_H
