#include "sample-browser.h"
#include "sample-manager.h"

#include <base/io/regular-file.h>

#include <framework/developer-tools/tweak-ui/tweak-ui-root.h>
#include <framework/application.h>
#include <framework/frame-signals.h>

namespace AdvancedGraphics {
namespace Samples {

void switchBackToApplication()
{
  Application::startMainApplet();
}

void switchBackToApplicationNextFrame()
{
  FrameSignals::callOnce(std::bind(switchBackToApplication));
}

SampleBrowser::SampleBrowser()
{
  currentScene = "--";

  bar = Framework::TweakUI::Bar::create("Sample-Browser");
  bar->setHelp("The Sample Browser allos you to choose a sample out of the available ones.");

  Framework::TweakUI::Layers::sampleBrowser->addBar(bar);
  Framework::TweakUI::Layers::sampleBrowser->setVisible(true);

  enumeratedVariable = Framework::TweakUI::EnumeratedVariable::create("SampleScene", "Scene", bar, "help='Lists all available samples.'");
  enumeratedVariable->append("--");
  for(const IO::RegularFile regularFile : sampleDirectory().allRegularFiles())
    enumeratedVariable->append(regularFile.filenameWithoutExtension().toOgreString());
  enumeratedVariable->signalIndexChanged().connect([this](int){changeScene(enumeratedVariable->currentText(currentScene));}).track(this->trackable);

  bar->addButton("ReloadSample", std::bind(&SampleBrowser::reloadScene, this), "label='Reload' help='Quits and restarts the sample immediatly (without reloading its resources)' key=CTRL+r");
  bar->addSeperator();
  std::string buttonDef = "label='Back to "+Application::name().toAnsiString()+"' help='Quits the sample and the sample browser in order to go back to the main application.' key=CTRL+q";
  bar->addButton("BackToApplication", switchBackToApplicationNextFrame, buttonDef.c_str());

}

SampleBrowser::~SampleBrowser()
{
}

SampleBrowser::Ptr SampleBrowser::create()
{
  return Ptr(new SampleBrowser);
}


void SampleBrowser::handleLoadedScene(const std::string& scene)
{
  if(scene.empty())
    enumeratedVariable->setCurrentText("--");
  else
    enumeratedVariable->setCurrentText(scene);
}


void SampleBrowser::reloadScene()
{
  loadScene(currentScene);
}


void SampleBrowser::changeScene(const std::string& scene)
{
  if(currentScene != scene)
    loadScene(scene);
}


void SampleBrowser::loadScene(const std::string& scene)
{
  SampleManager::loadSample(scene);

  currentScene = scene;
}


IO::Directory SampleBrowser::sampleDirectory()
{
  return Application::assetsDirectory() / "advanced-graphic-samples";
}


} // namespace Samples
} // namespace AdvancedGraphics
