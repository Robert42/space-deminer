#ifndef ADVANCEDGRAPHICS_SAMPLES_SAMPLE_H
#define ADVANCEDGRAPHICS_SAMPLES_SAMPLE_H

#include <framework/applet.h>

namespace AdvancedGraphics {

using namespace Framework;

namespace Samples {

class SampleManager;

class Sample : public Applet
{
public:
  typedef shared_ptr<Sample> Ptr;

protected:
  const shared_ptr<SampleManager> sampleManager;

public:
  Sample();
};

} // namespace Samples
} // namespace AdvancedGraphics

#endif // ADVANCEDGRAPHICS_SAMPLES_SAMPLE_H
