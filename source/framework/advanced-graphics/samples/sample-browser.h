#ifndef ADVANCEDGRAPHICS_SAMPLES_SAMPLEBROWSER_H
#define ADVANCEDGRAPHICS_SAMPLES_SAMPLEBROWSER_H

#include <framework/developer-tools/tweak-ui/tweak-ui-bar.h>
#include <framework/developer-tools/tweak-ui/enumerated-variable.h>
#include <base/io/directory.h>

namespace AdvancedGraphics {

using namespace Framework;

namespace Samples {

class SampleBrowser final
{
public:
  typedef shared_ptr<SampleBrowser> Ptr;

public:
  Signals::Trackable trackable;

private:
  Framework::TweakUI::Bar::Ptr bar;
  Framework::TweakUI::EnumeratedVariable::Ptr enumeratedVariable;

  std::string currentScene;

private:
  SampleBrowser();

public:
  ~SampleBrowser();
  static Ptr create();

public:
  static IO::Directory sampleDirectory();

  void handleLoadedScene(const std::string& scene);

private:
  void changeScene(const std::string& scene);
  void loadScene(const std::string& scene);
  void reloadScene();
};

} // namespace Samples
} // namespace AdvancedGraphics

#endif // ADVANCEDGRAPHICS_SAMPLES_SAMPLEBROWSER_H
