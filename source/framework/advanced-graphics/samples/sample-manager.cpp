#include "sample-manager.h"
#include "fallback-sample.h"
#include "scriptfile-sample.h"
#include "sample-browser.h"

#include <framework/window/render-window.h>
#include <framework/application.h>

#include <base/io/log.h>

namespace AdvancedGraphics {
namespace Samples {


const int VIEWPORT_Z_ORDER = 0;


SampleManager::SampleManager()
{
  Ogre::Root& ogreRoot = Ogre::Root::getSingleton();

  _sceneManager = ogreRoot.createSceneManager(Ogre::ST_GENERIC, "fallback-graphics-sample");
  _camera = _sceneManager->createCamera("fallback-graphics-sample-camera");
  _viewport = RenderWindow::ogreRenderWindow()->addViewport(_camera, VIEWPORT_Z_ORDER);

  _viewport->setClearEveryFrame(true);
  _viewport->setBackgroundColour(Ogre::ColourValue::Black);

  sampleBrowser = SampleBrowser::create();
}


SampleManager::~SampleManager()
{
  sampleBrowser.reset();

  Ogre::Root& ogreRoot = Ogre::Root::getSingleton();

  _sceneManager->destroyAllCameras();
  RenderWindow::ogreRenderWindow()->removeViewport(_viewport->getZOrder());
  ogreRoot.destroySceneManager(_sceneManager);
}


void SampleManager::loadSample(const Ogre::String& sampleName)
{
  Ptr manager = singleton();
  Sample::Ptr sample;

  // Destroy the old sample first so the destroctor of the previous sample will be
  // called before the constroctur of the next sample
  // As manager is already stored in a smart pointer, the instance won't be destroyed
  Application::setCurrentApplet(Applet::Ptr());

  try
  {
    IO::RegularFile sampleFile = SampleBrowser::sampleDirectory()|(sampleName+".sample");

    if(sampleName=="" || sampleName=="--")
      manager->sample = sample = Sample::Ptr(new FallbackSample);
    else if(sampleFile.exists())
      manager->sample = sample = manager->createSampleFromFile(sampleFile);
    else
    {
      IO::Log::logError("Unknown Sample <%0>", sampleName);
      manager->sample = sample =  Sample::Ptr(new FallbackSample);
    }

    manager->sampleBrowser->handleLoadedScene(sampleName);

  }catch(...)
  {
    manager->sample = sample = Sample::Ptr(new FallbackSample);
    manager->sampleBrowser->handleLoadedScene("--");
    throw;
  }

  Application::setCurrentApplet(sample);
}

Ogre::SceneManager& SampleManager::sceneManager()
{
  return *singleton()->_sceneManager;
}

Ogre::Camera& SampleManager::camera()
{
  return *singleton()->_camera;
}

Ogre::Viewport& SampleManager::viewport()
{
  return *singleton()->_viewport;
}

Sample::Ptr SampleManager::createSampleFromFile(const IO::RegularFile& file)
{
  return Sample::Ptr(new ScriptFileSample(file));
}


} // namespace Samples
} // namespace AdvancedGraphics
