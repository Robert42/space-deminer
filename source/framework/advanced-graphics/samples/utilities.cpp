#include "utilities.h"
#include "sample-manager.h"

#include <framework/scripting/initialization.h>

namespace AdvancedGraphics {
namespace Samples {


Utilities::Utilities()
{
}


Utilities::~Utilities()
{
  SampleManager::sceneManager().setSkyBoxEnabled(false);
}


void Utilities::loadEnvMap(const std::string& textureName)
{
  SampleManager::sceneManager().setSkyBox(true, textureName);
}


void Utilities::registerScriptFunctions()
{
  int r;

  AngelScript::asIScriptEngine* engine = Base::Scripting::Engine::angelScriptEngine();

  AngelScript::asDWORD previousAccessMask = engine->SetDefaultAccessMask(Framework::Scripting::AccessMasks::SPECIAL_USAGE);
  r = engine->BeginConfigGroup(scriptConfigGroupName()); Base::Scripting::AngelScriptCheck(r);

  r = engine->RegisterGlobalFunction("void loadEnvMap(const string &in textureName)", AngelScript::asFUNCTION(loadEnvMap), AngelScript::asCALL_CDECL); Base::Scripting::AngelScriptCheck(r);

  r = engine->EndConfigGroup();
  engine->SetDefaultAccessMask(previousAccessMask);
}


void Utilities::unregisterScriptFunctions()
{
  int r;
  AngelScript::asIScriptEngine* engine = Base::Scripting::Engine::angelScriptEngine();

  r = engine->GarbageCollect(); Base::Scripting::AngelScriptCheck(r);
  r = engine->RemoveConfigGroup(scriptConfigGroupName()); Base::Scripting::AngelScriptCheck(r);
}


const char* Utilities::scriptConfigGroupName()
{
  return "AdvancedGraphics::Samples::SampleScript";
}


} // namespace Samples
} // namespace AdvancedGraphics
