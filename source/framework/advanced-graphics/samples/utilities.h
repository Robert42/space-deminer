#ifndef ADVANCEDGRAPHICS_SAMPLES_UTILITIES_H
#define ADVANCEDGRAPHICS_SAMPLES_UTILITIES_H

#include <dependencies.h>

namespace AdvancedGraphics {
namespace Samples {

class Utilities
{
public:
  Utilities();
  ~Utilities();

  static void loadEnvMap(const std::string& textureName);

  static void registerScriptFunctions();
  static void unregisterScriptFunctions();

private:
  static const char* scriptConfigGroupName();
};

} // namespace Samples
} // namespace AdvancedGraphics

#endif // ADVANCEDGRAPHICS_SAMPLES_UTILITIES_H
