#ifndef _FRAMEWORK_WINDOW_RENDERWINDOW_H_
#define _FRAMEWORK_WINDOW_RENDERWINDOW_H_

#include <dependencies.h>

#include <framework/window/user-input.h>

#include <base/signals/cascaded-signals.h>
#include <base/singleton.h>

namespace Framework {

class Application;


class RenderWindow : public Singleton<RenderWindow>
{
private:
  Signals::CallableSignal<void()> _signalWindowResized;
  Signals::CallableSignal<void(bool)> _signalFocusChanged;

protected:
  Ogre::RenderWindow* _ogreRenderWindow;
  bool focusGainedOnce;

protected:
  RenderWindow();

public:
  virtual ~RenderWindow();

  static shared_ptr<RenderWindow> create(const String& title);

public:
  /** Returns the class managing the user input.
   */
  static UserInput& userInput();

  static bool isClosed();

  static ivec2 size();

  static bool hasClipboardValue();
  static String clipboardValue();
  static void setClipboardValue(const String& value);

public:
  static Ogre::RenderWindow* ogreRenderWindow();

  static bool gotFocusAlreadyOnce();

  static Signals::Signal<void()>& signalWindowResized();
  static Signals::Signal<void(bool)>& signalFocusChanged();

protected:
  void sendSignalWindowResized();
  void sendSignalFocusChanged(bool gainedFocus);

  int defaultWidth() const;
  int defaultHeight() const;

  virtual void createOgreRenderWindowImpl() = 0;

  virtual bool isClosedImpl();
  virtual UserInput& userInputImpl() = 0;
  virtual ivec2 sizeImpl() const = 0;

  virtual bool hasClipboardValueImpl() const = 0;
  virtual String clipboardValueImpl() const = 0;
  virtual void setClipboardValueImpl(const String& value) const = 0;

public:
  void createOgreRenderWindow();

  virtual void run(Application& application) = 0;

  virtual void close() = 0;
};


}


#endif
