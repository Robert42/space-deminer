#include <dependencies.h>
#include <framework/window/render-window.h>

namespace Framework {
namespace Private {
RenderWindow* createRenderWindowImpl(const String& title);
}


RenderWindow::RenderWindow()
{
  focusGainedOnce = false;
  _ogreRenderWindow = nullptr;
}


RenderWindow::~RenderWindow()
{
}


void RenderWindow::sendSignalWindowResized()
{
  _signalWindowResized();
}


void RenderWindow::sendSignalFocusChanged(bool gainedFocus)
{
  focusGainedOnce |= gainedFocus;
  _signalFocusChanged(gainedFocus);
}

Ogre::RenderWindow* RenderWindow::ogreRenderWindow()
{
  return singleton()._ogreRenderWindow;
}

bool RenderWindow::isClosedImpl()
{
  return ogreRenderWindow()->isClosed();
}

shared_ptr<RenderWindow> RenderWindow::create(const String& title)
{
  return shared_ptr<RenderWindow>(Private::createRenderWindowImpl(title));
}

int RenderWindow::defaultWidth() const
{
  return 1024;
}

int RenderWindow::defaultHeight() const
{
  return 576;
}

bool RenderWindow::gotFocusAlreadyOnce()
{
  return singleton().focusGainedOnce;
}

void RenderWindow::createOgreRenderWindow()
{
  createOgreRenderWindowImpl();
  assert(ogreRenderWindow() != nullptr);
}

Signals::Signal<void()>& RenderWindow::signalWindowResized()
{
  return singleton()._signalWindowResized;
}

UserInput& RenderWindow::userInput()
{
  return singleton().userInputImpl();
}

bool RenderWindow::isClosed()
{
  return singleton().isClosedImpl();
}

ivec2 RenderWindow::size()
{
  return singleton().sizeImpl();
}

bool RenderWindow::hasClipboardValue()
{
  return singleton().hasClipboardValueImpl();
}

String RenderWindow::clipboardValue()
{
  return singleton().clipboardValue();
}

void RenderWindow::setClipboardValue(const String& value)
{
  singleton().setClipboardValueImpl(value);
}

Signals::Signal<void(bool)>& RenderWindow::signalFocusChanged()
{
  return singleton()._signalFocusChanged;
}


}
