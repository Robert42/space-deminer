#include <dependencies.h>
#include <framework/window/user-input/input-device.h>

namespace Framework {


TEST(framework_InputDevice, std_less_is_working_on_ActionPriority)
{
  std::vector<InputDevice::ActionPriority> priorities;

  priorities.push_back(InputDevice::ActionPriority::FALLBACK);
  priorities.push_back(InputDevice::ActionPriority::INGAME);
  priorities.push_back(InputDevice::ActionPriority::INGAME_DEBUG);
  priorities.push_back(InputDevice::ActionPriority::HUD);
  priorities.push_back(InputDevice::ActionPriority::GUI);
  priorities.push_back(InputDevice::ActionPriority::GUI_SPECIAL_EFFECT);
  priorities.push_back(InputDevice::ActionPriority::TWEAK_UI);
  priorities.push_back(InputDevice::ActionPriority::TERMINAL);

  for(size_t i=0; i<priorities.size(); ++i)
    for(size_t j=0; j<priorities.size(); ++j)
    {
      std::less<InputDevice::ActionPriority> priority_less;
      bool less_result = priority_less(priorities[i], priorities[j]);
      EXPECT_EQ(i<j, less_result) << "i: " << i << ", j: " << j << ", priorities[i]: " << priorities[i].value << ", priorities[j]: " << priorities[j].value;
    }
}


}
