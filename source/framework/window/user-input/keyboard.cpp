#include <dependencies.h>
#include <framework/window/user-input/keyboard.h>

namespace Framework {

Keyboard::Keyboard()
{
  keyLabel.resize(KeyCode::NUMBER_OF_KEYCODES);

  keyLabel[KeyCode::UNKNOWN] = translate("Unknown");
  keyLabel[KeyCode::OPENTERMINAL] = translate("Open Terminal");
  keyLabel[KeyCode::KEYPAD_0] = translate("Keypad 0");
  keyLabel[KeyCode::KEYPAD_1] = translate("Keypad 1");
  keyLabel[KeyCode::KEYPAD_2] = translate("Keypad 2");
  keyLabel[KeyCode::KEYPAD_3] = translate("Keypad 3");
  keyLabel[KeyCode::KEYPAD_4] = translate("Keypad 4");
  keyLabel[KeyCode::KEYPAD_5] = translate("Keypad 5");
  keyLabel[KeyCode::KEYPAD_6] = translate("Keypad 6");
  keyLabel[KeyCode::KEYPAD_7] = translate("Keypad 7");
  keyLabel[KeyCode::KEYPAD_8] = translate("Keypad 8");
  keyLabel[KeyCode::KEYPAD_9] = translate("Keypad 9");
  keyLabel[KeyCode::NUMPAD_0] = translate("Numpad 0");
  keyLabel[KeyCode::NUMPAD_1] = translate("Numpad 1");
  keyLabel[KeyCode::NUMPAD_2] = translate("Numpad 2");
  keyLabel[KeyCode::NUMPAD_3] = translate("Numpad 3");
  keyLabel[KeyCode::NUMPAD_4] = translate("Numpad 4");
  keyLabel[KeyCode::NUMPAD_5] = translate("Numpad 5");
  keyLabel[KeyCode::NUMPAD_6] = translate("Numpad 6");
  keyLabel[KeyCode::NUMPAD_7] = translate("Numpad 7");
  keyLabel[KeyCode::NUMPAD_8] = translate("Numpad 8");
  keyLabel[KeyCode::NUMPAD_9] = translate("Numpad 9");
  keyLabel[KeyCode::NUMPAD_PLUS] = translate("Numpad Plus");
  keyLabel[KeyCode::NUMPAD_MINUS] = translate("Numpad Minus");
  keyLabel[KeyCode::NUMPAD_ENTER] = translate("Numpad Enter");
  keyLabel[KeyCode::F1] = translate("F1");
  keyLabel[KeyCode::F2] = translate("F2");
  keyLabel[KeyCode::F3] = translate("F3");
  keyLabel[KeyCode::F4] = translate("F4");
  keyLabel[KeyCode::F5] = translate("F5");
  keyLabel[KeyCode::F6] = translate("F6");
  keyLabel[KeyCode::F7] = translate("F7");
  keyLabel[KeyCode::F8] = translate("F8");
  keyLabel[KeyCode::F9] = translate("F9");
  keyLabel[KeyCode::F10] = translate("F10");
  keyLabel[KeyCode::F11] = translate("F11");
  keyLabel[KeyCode::F12] = translate("F12");
  keyLabel[KeyCode::A] = translate("A");
  keyLabel[KeyCode::B] = translate("B");
  keyLabel[KeyCode::C] = translate("C");
  keyLabel[KeyCode::D] = translate("D");
  keyLabel[KeyCode::E] = translate("E");
  keyLabel[KeyCode::F] = translate("F");
  keyLabel[KeyCode::G] = translate("G");
  keyLabel[KeyCode::H] = translate("H");
  keyLabel[KeyCode::I] = translate("I");
  keyLabel[KeyCode::J] = translate("J");
  keyLabel[KeyCode::K] = translate("K");
  keyLabel[KeyCode::L] = translate("L");
  keyLabel[KeyCode::M] = translate("M");
  keyLabel[KeyCode::N] = translate("N");
  keyLabel[KeyCode::O] = translate("O");
  keyLabel[KeyCode::P] = translate("P");
  keyLabel[KeyCode::Q] = translate("Q");
  keyLabel[KeyCode::R] = translate("R");
  keyLabel[KeyCode::S] = translate("S");
  keyLabel[KeyCode::T] = translate("T");
  keyLabel[KeyCode::U] = translate("U");
  keyLabel[KeyCode::V] = translate("V");
  keyLabel[KeyCode::W] = translate("W");
  keyLabel[KeyCode::X] = translate("X");
  keyLabel[KeyCode::Y] = translate("Y");
  keyLabel[KeyCode::Z] = translate("Z");

  keyLabel[KeyCode::ESCAPE] = translate("Escape");
  keyLabel[KeyCode::SPACE] = translate("Space");
  keyLabel[KeyCode::RETURN] = translate("Return");
  keyLabel[KeyCode::ALT_LEFT] = translate("Left Alt");
  keyLabel[KeyCode::ALT_RIGHT] = translate("Right Alt");
  keyLabel[KeyCode::SHIFT_LEFT] = translate("Left Shift");
  keyLabel[KeyCode::SHIFT_RIGHT] = translate("Right Shift");
  keyLabel[KeyCode::CONTROL_LEFT] = translate("Left Control");
  keyLabel[KeyCode::CONTROL_RIGHT] = translate("Right Control");

  keyLabel[KeyCode::BACKSPACE] = translate("Backspace");
  keyLabel[KeyCode::TAB] = translate("Tab");
  keyLabel[KeyCode::INSERT] = translate("Insert");
  keyLabel[KeyCode::DELETE] = translate("Delete");
  keyLabel[KeyCode::HOME] = translate("Home");
  keyLabel[KeyCode::END] = translate("End");

  keyLabel[KeyCode::ARROW_UP] = translate("Arrow Up");
  keyLabel[KeyCode::ARROW_DOWN] = translate("Arrow Down");
  keyLabel[KeyCode::ARROW_LEFT] = translate("Arrow Left");
  keyLabel[KeyCode::ARROW_RIGHT] = translate("Arrow Right");

  keyLabel[KeyCode::PAUSE] = translate("Pause");
  keyLabel[KeyCode::PAGEDOWN] = translate("PageDown");
  keyLabel[KeyCode::PAGEUP] = translate("PageUp");
}


String Keyboard::labelOf(KeyCode keyCode)
{
  if(keyCode.value >= KeyCode::NUMBER_OF_KEYCODES)
    keyCode = KeyCode::UNKNOWN;

  return singleton().keyLabel[keyCode.value].translation();
}


bool Keyboard::isKeyCodeAllowedToBeMappedIngame(KeyCode keyCode)
{
  if(keyCode.value <= KeyCode::UNKNOWN)
    return false;
  if(keyCode.value >= KeyCode::ESCAPE)
    return false;
  return true;
}


Keyboard::CascadedActionSignal& Keyboard::signalKeyPressed()
{
  return singleton().signalActionBegin();
}


Keyboard::CascadedActionSignal& Keyboard::signalKeyReleased()
{
  return singleton().signalActionEnd();
}


Keyboard::CascadedUnicodeSignal& Keyboard::signalUnicodeEntered()
{
  return singleton()._signalUnicodeEntered;
}


void Keyboard::sendSignalSignalUnicodeEntered(String::value_type unicode)
{
  if(unicode == 0)
    return;
  _signalUnicodeEntered(unicode);
}


KeyModifier Keyboard::keyModifierForKeyCode(KeyCode key)
{
  switch(key.value)
  {
  case KeyCode::ALT_LEFT:
    return KeyModifier::ALT_LEFT;
  case KeyCode::ALT_RIGHT:
    return KeyModifier::ALT_RIGHT;
  case KeyCode::CONTROL_LEFT:
    return KeyModifier::CONTROL_LEFT;
  case KeyCode::CONTROL_RIGHT:
    return KeyModifier::CONTROL_RIGHT;
  case KeyCode::SHIFT_LEFT:
    return KeyModifier::SHIFT_LEFT;
  case KeyCode::SHIFT_RIGHT:
    return KeyModifier::SHIFT_RIGHT;
  default:
    return KeyModifier::NONE;
  }
}


bool Keyboard::isKeyDown(KeyCode keyCode)
{
  Keyboard& keyboard = singleton();

  return keyboard._injectedPressedKeys.contains(keyCode) || keyboard.isKeyDownImpl(keyCode);
}


KeyModifier Keyboard::modifierState()
{
  Keyboard& keyboard = singleton();

  return keyboard.modifierStateImpl() | keyboard._injectedModifiers;
}


bool Keyboard::isModifierDown(KeyModifier modifier)
{
  return (modifierState() & modifier) != KeyModifier::NONE;
}


bool Keyboard::areOnlyModifierDown(KeyModifier modifier, KeyModifier ignoredModifiers)
{
  if(modifier == KeyModifier::NONE)
    return !isAnyModifierDown(ignoredModifiers);

  KeyModifier notAllowedModifier = ~(modifier | ignoredModifiers);
  return !isModifierDown(notAllowedModifier) && isModifierDown(modifier);
}



bool Keyboard::areOnlyModifierDown(const std::initializer_list<KeyModifier>& modifier, KeyModifier ignoredModifiers)
{
  KeyModifier allModifiers = KeyModifier::NONE;

  for(KeyModifier m : modifier)
  {
    allModifiers |= m;
    if(!isModifierDown(m))
      return false;
  }

  return areOnlyModifierDown(allModifiers, ignoredModifiers);
}


bool Keyboard::isAnyModifierDown(KeyModifier ignoredModifiers)
{
  return (modifierState() | ignoredModifiers) != ignoredModifiers;
}


bool Keyboard::isActionCurrentlyActive(KeyCode actionId) const
{
  return isKeyDown(actionId);
}

void Keyboard::injectKeyDown(KeyCode key)
{
  UserInput::appendInjectedInput([key](){
    Keyboard& keyboard = singleton();

    keyboard._injectedModifiers |= keyModifierForKeyCode(key);
    keyboard._injectedPressedKeys.insert(key);
    keyboard.sendSignalActionBegin(key);
  });
}

void Keyboard::injectKeyUp(KeyCode key)
{
  UserInput::appendInjectedInput([key](){
    Keyboard& keyboard = singleton();

    keyboard._injectedModifiers &= ~keyModifierForKeyCode(key);
    keyboard._injectedPressedKeys.remove(key);
    keyboard.sendSignalActionEnd(key);
  });
}

void Keyboard::injectText(const String& text)
{
  for(String::value_type character : text)
  {
    UserInput::appendInjectedInput([character](){
      Keyboard& keyboard = singleton();

      keyboard._signalUnicodeEntered(character);
    });
  }
}


}
