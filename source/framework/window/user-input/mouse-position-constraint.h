#ifndef FRAMEWORK_MOUSEPOSITIONCONSTRAINT_H
#define FRAMEWORK_MOUSEPOSITIONCONSTRAINT_H

#include <base/output.h>
#include <base/geometry/rectangle.h>

#include <base/signals/signal.h>

namespace Framework {

using namespace Base;

class MousePositionConstraint : noncopyable
{
public:
  typedef std::shared_ptr<MousePositionConstraint> Ptr;

protected:
  Signals::CallableSignal<void()> _signalChanged;

public:
  MousePositionConstraint();
  virtual ~MousePositionConstraint();

  virtual void applyContraint(const InOutput<ivec2>& position, const InOutput<ivec2>& relativeMovement) const = 0;
  virtual void applyFirstTime(const InOutput<ivec2>& position) const = 0;

  Signals::Signal<void()>& signalChanged();

public:
  class BaseRectangleConstraint;
  class RectangleConstraint;
  class FullscreenRectangleConstraint;
  class NoConstraint;

  static std::shared_ptr<FullscreenRectangleConstraint> createFullscreenConstraint(bool wraparound);
  static std::shared_ptr<RectangleConstraint> createRectangleConstraint(const Rectangle<ivec2>& rectangle, bool wraparound);
  static std::shared_ptr<NoConstraint> createNoConstraint();
};


class MousePositionConstraint::BaseRectangleConstraint : public MousePositionConstraint
{
public:
  typedef std::shared_ptr<BaseRectangleConstraint> Ptr;

private:
  Rectangle<ivec2> _rectangle;
  bool _wraparound;

protected:
  BaseRectangleConstraint(const Rectangle<ivec2>& rectangle, bool wraparound);

  void _setRectangle(const Rectangle<ivec2>& rectangle);

public:
  void setWrapAround(bool wraparound);

  const Rectangle<ivec2>& rectangle() const;
  bool isWrappingAround() const;

  void applyContraint(const InOutput<ivec2>& position, const InOutput<ivec2>& relativeMovement) const override;
  void applyFirstTime(const InOutput<ivec2>& position) const override;
};


class MousePositionConstraint::RectangleConstraint : public BaseRectangleConstraint
{
public:
  typedef std::shared_ptr<RectangleConstraint> Ptr;

private:
  RectangleConstraint(const Rectangle<ivec2>& rectangle, bool wraparound);

public:
  static Ptr create(const Rectangle<ivec2>& rectangle, bool wraparound);

  void setRectangle(const Rectangle<ivec2>& rectangle);
};


class MousePositionConstraint::FullscreenRectangleConstraint : public BaseRectangleConstraint
{
public:
  typedef std::shared_ptr<FullscreenRectangleConstraint> Ptr;

public:
  Signals::Trackable trackable;

private:
  FullscreenRectangleConstraint(bool wraparound);

public:
  static Ptr create(bool wraparound);

private:
  static Rectangle<ivec2> calcFullscreenRectangle();
};


class MousePositionConstraint::NoConstraint : public MousePositionConstraint
{
public:
  typedef std::shared_ptr<NoConstraint> Ptr;

protected:
  NoConstraint();

public:
  static Ptr create();

  void applyContraint(const InOutput<ivec2>& position, const InOutput<ivec2>& relativeMovement) const final override;
  void applyFirstTime(const InOutput<ivec2>& position) const final override;
};


} // namespace Framework

#endif // FRAMEWORK_MOUSEPOSITIONCONSTRAINT_H
