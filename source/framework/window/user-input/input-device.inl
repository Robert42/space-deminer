#ifndef _FRAMEWORK_WINDOW_USERINPUT_INPUT_DEVICE_INL_
#define _FRAMEWORK_WINDOW_USERINPUT_INPUT_DEVICE_INL_

#include "input-device.h"

namespace Framework {


template<typename ActionId>
BaseInputDevice<ActionId>::BaseInputDevice()
{
}


template<typename ActionId>
bool BaseInputDevice<ActionId>::sendSignalActionBegin(ActionId actionId)
{
  return _signalActionBegin(actionId);
}


template<typename ActionId>
bool BaseInputDevice<ActionId>::sendSignalActionEnd(ActionId actionId)
{
  return _signalActionEnd(actionId);
}


template<typename ActionId>
typename BaseInputDevice<ActionId>::CascadedActionSignal& BaseInputDevice<ActionId>::signalActionBegin()
{
  return _signalActionBegin;
}


template<typename ActionId>
typename BaseInputDevice<ActionId>::CascadedActionSignal& BaseInputDevice<ActionId>::signalActionEnd()
{
  return _signalActionEnd;
}


}

#endif // _FRAMEWORK_WINDOW_USERINPUT_INPUT_DEVICE_INL_
