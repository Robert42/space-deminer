#include <dependencies.h>
#include <framework/window/user-input/keyboard.h>

namespace Framework {
namespace Private {
class TestKeyboard : public Keyboard
{
public:
  QHash<KeyCode, bool> _activeKeys;
  KeyModifier _modifierState;

  TestKeyboard()
  {
    _modifierState = KeyModifier::NONE;
  }

  KeyModifier modifierStateImpl() override
  {
    return _modifierState;
  }

  bool isKeyDownImpl(KeyCode code) const override
  {
    Optional<bool> isDown = optionalFromHashMap(_activeKeys, code);

    if(isDown)
      return isDown.value();
    else
      return false;
  }

  bool isSupported() override
  {
    return true;
  }
};

}
using Private::TestKeyboard;

TEST(framework_Keyboard, isModifierDown)
{
  TestKeyboard keyboard;

  keyboard._modifierState = KeyModifier::CONTROL_LEFT|KeyModifier::SHIFT_LEFT|KeyModifier::NUMLOCK;

  EXPECT_TRUE(Keyboard::isModifierDown(KeyModifier::CONTROL_LEFT));
  EXPECT_TRUE(Keyboard::isModifierDown(KeyModifier::SHIFT_LEFT));
  EXPECT_TRUE(Keyboard::isModifierDown(KeyModifier::NUMLOCK));

  EXPECT_FALSE(Keyboard::isModifierDown(KeyModifier::CONTROL_RIGHT));
  EXPECT_FALSE(Keyboard::isModifierDown(KeyModifier::SHIFT_RIGHT));
  EXPECT_FALSE(Keyboard::isModifierDown(KeyModifier::ALT));

  EXPECT_TRUE(Keyboard::isModifierDown(KeyModifier::CONTROL));
  EXPECT_TRUE(Keyboard::isModifierDown(KeyModifier::SHIFT));
}


TEST(framework_Keyboard, areOnlyModifierDown)
{
  TestKeyboard keyboard;
  KeyModifier modifier = KeyModifier::NONE;
  int i=0;

  while(i<2)
  {
    keyboard._modifierState = modifier;

    EXPECT_TRUE(Keyboard::areOnlyModifierDown(KeyModifier::NONE, KeyModifier::NUMLOCK));
    EXPECT_FALSE(Keyboard::areOnlyModifierDown(KeyModifier::CONTROL, KeyModifier::NUMLOCK));
    EXPECT_FALSE(Keyboard::areOnlyModifierDown(KeyModifier::CONTROL | KeyModifier::SHIFT, KeyModifier::NUMLOCK));

    keyboard._modifierState = KeyModifier::CONTROL_LEFT | modifier;

    EXPECT_FALSE(Keyboard::areOnlyModifierDown(KeyModifier::NONE, KeyModifier::NUMLOCK));
    EXPECT_TRUE(Keyboard::areOnlyModifierDown(KeyModifier::CONTROL, KeyModifier::NUMLOCK));
    EXPECT_TRUE(Keyboard::areOnlyModifierDown(KeyModifier::CONTROL | KeyModifier::SHIFT, KeyModifier::NUMLOCK));

    keyboard._modifierState = KeyModifier::CONTROL | modifier;

    EXPECT_FALSE(Keyboard::areOnlyModifierDown(KeyModifier::NONE, KeyModifier::NUMLOCK));
    EXPECT_TRUE(Keyboard::areOnlyModifierDown(KeyModifier::CONTROL, KeyModifier::NUMLOCK));
    EXPECT_TRUE(Keyboard::areOnlyModifierDown(KeyModifier::CONTROL | KeyModifier::SHIFT, KeyModifier::NUMLOCK));

    keyboard._modifierState = KeyModifier::CONTROL_LEFT | KeyModifier::SHIFT_LEFT | modifier;

    EXPECT_FALSE(Keyboard::areOnlyModifierDown(KeyModifier::NONE, KeyModifier::NUMLOCK));
    EXPECT_FALSE(Keyboard::areOnlyModifierDown(KeyModifier::CONTROL, KeyModifier::NUMLOCK));
    EXPECT_TRUE(Keyboard::areOnlyModifierDown(KeyModifier::CONTROL | KeyModifier::SHIFT, KeyModifier::NUMLOCK));

    modifier = KeyModifier::NUMLOCK;
    ++i;
  }
}


TEST(framework_Keyboard, areOnlyModifierDown_2)
{
  TestKeyboard keyboard;
  KeyModifier modifier = KeyModifier::NONE;
  int i=0;

  while(i<2)
  {
    keyboard._modifierState = KeyModifier::NONE | modifier;

    EXPECT_FALSE(Keyboard::areOnlyModifierDown({KeyModifier::NONE}, KeyModifier::NUMLOCK));
    EXPECT_FALSE(Keyboard::areOnlyModifierDown({KeyModifier::CONTROL}, KeyModifier::NUMLOCK));
    EXPECT_FALSE(Keyboard::areOnlyModifierDown({KeyModifier::CONTROL, KeyModifier::SHIFT}, KeyModifier::NUMLOCK));

    keyboard._modifierState = KeyModifier::CONTROL_LEFT | modifier;

    EXPECT_FALSE(Keyboard::areOnlyModifierDown({KeyModifier::NONE}, KeyModifier::NUMLOCK));
    EXPECT_TRUE(Keyboard::areOnlyModifierDown({KeyModifier::CONTROL}, KeyModifier::NUMLOCK));
    EXPECT_FALSE(Keyboard::areOnlyModifierDown({KeyModifier::CONTROL, KeyModifier::SHIFT}, KeyModifier::NUMLOCK));

    keyboard._modifierState = KeyModifier::CONTROL | modifier;

    EXPECT_FALSE(Keyboard::areOnlyModifierDown({KeyModifier::NONE}, KeyModifier::NUMLOCK));
    EXPECT_TRUE(Keyboard::areOnlyModifierDown({KeyModifier::CONTROL}, KeyModifier::NUMLOCK));
    EXPECT_FALSE(Keyboard::areOnlyModifierDown({KeyModifier::CONTROL, KeyModifier::SHIFT}, KeyModifier::NUMLOCK));

    keyboard._modifierState = KeyModifier::CONTROL_LEFT | KeyModifier::SHIFT_LEFT | modifier;

    EXPECT_FALSE(Keyboard::areOnlyModifierDown({KeyModifier::NONE}, KeyModifier::NUMLOCK));
    EXPECT_FALSE(Keyboard::areOnlyModifierDown({KeyModifier::CONTROL}, KeyModifier::NUMLOCK));
    EXPECT_TRUE(Keyboard::areOnlyModifierDown({KeyModifier::CONTROL, KeyModifier::SHIFT}, KeyModifier::NUMLOCK));

    ++i;
  }
}


TEST(framework_Keyboard, isAnyModifierDown)
{
  TestKeyboard keyboard;

  EXPECT_FALSE(Keyboard::isAnyModifierDown(KeyModifier::NONE));
  EXPECT_FALSE(Keyboard::isAnyModifierDown(KeyModifier::CONTROL));
  EXPECT_FALSE(Keyboard::isAnyModifierDown(KeyModifier::CONTROL | KeyModifier::SHIFT));

  keyboard._modifierState = KeyModifier::CONTROL;

  EXPECT_TRUE(Keyboard::isAnyModifierDown(KeyModifier::NONE));
  EXPECT_FALSE(Keyboard::isAnyModifierDown(KeyModifier::CONTROL));
  EXPECT_FALSE(Keyboard::isAnyModifierDown(KeyModifier::CONTROL | KeyModifier::SHIFT));

  keyboard._modifierState = KeyModifier::SHIFT;

  EXPECT_TRUE(Keyboard::isAnyModifierDown(KeyModifier::NONE));
  EXPECT_TRUE(Keyboard::isAnyModifierDown(KeyModifier::CONTROL));
  EXPECT_FALSE(Keyboard::isAnyModifierDown(KeyModifier::CONTROL | KeyModifier::SHIFT));

  keyboard._modifierState = KeyModifier::CONTROL | KeyModifier::SHIFT;

  EXPECT_TRUE(Keyboard::isAnyModifierDown(KeyModifier::NONE));
  EXPECT_TRUE(Keyboard::isAnyModifierDown(KeyModifier::CONTROL));
  EXPECT_FALSE(Keyboard::isAnyModifierDown(KeyModifier::CONTROL | KeyModifier::SHIFT));

  keyboard._modifierState = KeyModifier::ALT;

  EXPECT_TRUE(Keyboard::isAnyModifierDown(KeyModifier::NONE));
  EXPECT_TRUE(Keyboard::isAnyModifierDown(KeyModifier::CONTROL));
  EXPECT_TRUE(Keyboard::isAnyModifierDown(KeyModifier::CONTROL | KeyModifier::SHIFT));

  keyboard._modifierState = KeyModifier::ALT | KeyModifier::CONTROL;

  EXPECT_TRUE(Keyboard::isAnyModifierDown(KeyModifier::NONE));
  EXPECT_TRUE(Keyboard::isAnyModifierDown(KeyModifier::CONTROL));
  EXPECT_TRUE(Keyboard::isAnyModifierDown(KeyModifier::CONTROL | KeyModifier::SHIFT));
}


}
