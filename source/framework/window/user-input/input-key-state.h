#ifndef FRAMEWORK_WINDOW_USERINPUT_INPUTKEYSTATE_H
#define FRAMEWORK_WINDOW_USERINPUT_INPUTKEYSTATE_H

#include "input-device.h"

namespace Framework {

class UserInput;

namespace Window {
namespace UserInput {

template<typename T>
class InputKeyState
{
public:
  Signals::Trackable trackable;

private:
  bool stateIsPressed;
  bool stateWasChangedThisFrame;
  bool markNextFrame;
  T _actionId;

public:
  const InputDevice::ActionPriority priority;

  const BaseInputDevice<T>& device;

  bool blockLowerPriorities;

public:
  InputKeyState(T actionId, InputDevice::ActionPriority priority, BaseInputDevice<T>& device, bool blockLowerPriorities);
  virtual ~InputKeyState();

  bool isPressed() const;
  bool wasChangedThisFrame() const;
  bool wasPressedThisFrame() const;
  bool wasReleasedThisFrame() const;

  void setActionId(T actionId);
  T actionId() const;

private:
  void onWait();

  void updateState(bool pressed);
  bool handleActionEvent(T actionId, bool pressed);
  void handleAddedBlocker(InputDevice::ActionPriority priority);
};


} // namespace UserInput
} // namespace Window
} // namespace Framework

#include "input-key-state.inl"

#endif // FRAMEWORK_WINDOW_USERINPUT_INPUTKEYSTATE_H
