#ifndef _FRAMEWORK_WINDOW_USERINPUT_KEYBOARD_H_
#define _FRAMEWORK_WINDOW_USERINPUT_KEYBOARD_H_

#include <framework/window/user-input/input-key-state.h>

#include <base/strings/string.h>
#include <base/enum-macros.h>
#include <base/singleton.h>
#include <base/i18n/translation.h>

namespace Framework {


BEGIN_ENUMERATION(KeyCode,)
  UNKNOWN, ///< Not possible to be mapped ingame

  SPACE,
  RETURN,
  BACKSPACE,
  TAB,
  ALT_LEFT,
  ALT_RIGHT,
  SHIFT_LEFT,
  SHIFT_RIGHT,
  CONTROL_LEFT,
  CONTROL_RIGHT,

  INSERT,
  DELETE,
  HOME,
  END,
  PAGEDOWN,
  PAGEUP,

  ARROW_UP,
  ARROW_DOWN,
  ARROW_LEFT,
  ARROW_RIGHT,

  A,
  B,
  C,
  D,
  E,
  F,
  G,
  H,
  I,
  J,
  K,
  L,
  M,
  N,
  O,
  P,
  Q,
  R,
  S,
  T,
  U,
  V,
  W,
  X,
  Y,
  Z,

  KEYPAD_0,
  KEYPAD_1,
  KEYPAD_2,
  KEYPAD_3,
  KEYPAD_4,
  KEYPAD_5,
  KEYPAD_6,
  KEYPAD_7,
  KEYPAD_8,
  KEYPAD_9,

  NUMPAD_0,
  NUMPAD_1,
  NUMPAD_2,
  NUMPAD_3,
  NUMPAD_4,
  NUMPAD_5,
  NUMPAD_6,
  NUMPAD_7,
  NUMPAD_8,
  NUMPAD_9,
  NUMPAD_PLUS,
  NUMPAD_MINUS,
  NUMPAD_ENTER,

  F1,
  F2,
  F3,
  F4,
  F5,
  F6,
  F7,
  F8,
  F9,
  F10,
  F11,
  F12,

  ESCAPE, ///< Not possible to be mapped ingame
  PAUSE, ///< Not possible to be mapped ingame

  OPENTERMINAL, ///< Not possible to be mapped ingame

  NUMBER_OF_KEYCODES
ENUMERATION_BASIC_OPERATORS(KeyCode)
END_ENUMERATION;


/**
 * \@note Please note that the modifier ids are guaranteed to have the same id like the SDL keymod modifiers
 */
BEGIN_ENUMERATION(KeyModifier,)
  NONE,
  SHIFT_LEFT = 0x0001,
  SHIFT_RIGHT = 0x0002,
  SHIFT = SHIFT_LEFT|SHIFT_RIGHT,

  CONTROL_LEFT = 0x0040,
  CONTROL_RIGHT = 0x0080,
  CONTROL = CONTROL_LEFT|CONTROL_RIGHT,

  ALT_LEFT = 0x0100,
  ALT_RIGHT = 0x0200,
  ALT = ALT_LEFT|ALT_RIGHT,

  GUI_LEFT = 0x0400,
  GUI_RIGHT = 0x0800,
  GUI = GUI_LEFT | GUI_RIGHT,

  NUMLOCK = 0x1000,
  CAPSLOCK = 0x2000,

  ALL = SHIFT | CONTROL | ALT | GUI | NUMLOCK | CAPSLOCK
ENUMERATION_BASIC_OPERATORS(KeyModifier)
ENUMERATION_FLAG_OPERATORS(KeyModifier)
END_ENUMERATION;


/** Represents the Keyboard.
 */
class Keyboard : public BaseInputDevice<KeyCode>, public Singleton<Keyboard>
{
public:
  typedef Signals::CascadedSignals<bool(String::value_type), ActionPriority> CascadedUnicodeSignal;

  class KeyState : public Window::UserInput::InputKeyState<KeyCode>
  {
  public:
    KeyState(KeyCode key,
             ActionPriority priority,
             bool blockLowerPriorities)
      : Window::UserInput::InputKeyState<KeyCode>(key,
                                                  priority,
                                                  Keyboard::singleton(),
                                                  blockLowerPriorities)
    {
    }
  };

private:
  KeyModifier _injectedModifiers;
  QSet<KeyCode> _injectedPressedKeys;

  QVector<Translation> keyLabel;
  Signals::CallableCascadedSignals<bool(String::value_type), ActionPriority> _signalUnicodeEntered;

protected:
  Keyboard();

public:
  /** Returns, whether the key represented by the given keycode is currently pressed down.
   *
   * @param keyCode
   *
   * @return true if the key keyCode is pressed.
   */
  static bool isKeyDown(KeyCode keyCode);

  /** Returns the current modifier state.
   *
   * @code
   * if(KeyModifier::CONTROL & modifierState())
   *   std::cout << "control is pressed!";
   * if(KeyModifier::CONTROL == modifierState())
   *   std::cout << "only control is pressed!";
   * @endcode
   *
   * @note It is guaranted, that only bits described by the KeyModifier enum are set.
   *
   * @note Normally you shouldn't need to use this. use one of
   *     - isModifierDown
   *     - areOnlyModifierDown
   *     - isAnyModifierDown
   * instead
   *
   * @return An bitarray describing the state of each modifier.
   */
  static KeyModifier modifierState();

  /** Returns, whether one or multiple of the given modifiers are down.
   *
   * @code
   * // Let's assume, keyboard.modifierState() returns KeyModifier::CONTROL_LEFT|KeyModifier::SHIFT_LEFT|KeyModifier::NUMLOCK
   *
   * isModifierDown(KeyModifier::CONTROL_LEFT); // returns true
   * isModifierDown(KeyModifier::SHIFT_LEFT); // returns true
   * isModifierDown(KeyModifier::NUMLOCK); // returns true
   *
   * isModifierDown(KeyModifier::CONTROL_RIGHT); // returns false
   * isModifierDown(KeyModifier::SHIFT_RIGHT); // returns false
   * isModifierDown(KeyModifier::ALT); // returns false
   *
   * isModifierDown(KeyModifier::CONTROL); // returns true
   * isModifierDown(KeyModifier::SHIFT); // returns true
   * @endcode
   */
  static bool isModifierDown(KeyModifier modifier);

  /** Returns, whether only one or multiple of the given modifiers are down.
   *
   * @code
   * // Let's assume, keyboard.modifierState() returns KeyModifier::CONTROL_LEFT|KeyModifier::SHIFT_LEFT|KeyModifier::NUMLOCK
   *
   * areOnlyModifierDown(KeyModifier::CONTROL); // returns false
   * areOnlyModifierDown(KeyModifier::CONTROL | KeyModifier::SHIFT); // returns true
   *
   * // Let's assume, keyboard.modifierState() returns KeyModifier::CONTROL_LEFT|KeyModifier::NUMLOCK
   *
   * areOnlyModifierDown(KeyModifier::CONTROL); // returns true
   * areOnlyModifierDown(KeyModifier::CONTROL | KeyModifier::SHIFT); // returns true
   * @endcode
   */
  static bool areOnlyModifierDown(KeyModifier modifier, KeyModifier ignoredModifiers = KeyModifier::NUMLOCK);

  /** Returns, whether for each modifier, at least one modifier is pressed, and no other modifier.
   *
   * @code
   * // Let's assume, keyboard.modifierState() returns KeyModifier::CONTROL_LEFT|KeyModifier::SHIFT_LEFT|KeyModifier::NUMLOCK
   *
   * areOnlyModifierDown({KeyModifier::CONTROL}); // returns false
   * areOnlyModifierDown({KeyModifier::CONTROL, KeyModifier::SHIFT}); // returns true
   *
   * // Let's assume, keyboard.modifierState() returns KeyModifier::CONTROL_LEFT|KeyModifier::NUMLOCK
   *
   * areOnlyModifierDown({KeyModifier::CONTROL}); // returns true
   * areOnlyModifierDown({KeyModifier::CONTROL, KeyModifier::SHIFT}); // returns false
   * @endcode
   */
  static bool areOnlyModifierDown(const std::initializer_list<KeyModifier>& modifier, KeyModifier ignoredModifiers = KeyModifier::NUMLOCK);

  /** Returns, whether any modifier is pressed.
   */
  static bool isAnyModifierDown(KeyModifier ignoredModifiers = KeyModifier::NUMLOCK);

  static String labelOf(KeyCode keyCode);

  static bool isKeyCodeAllowedToBeMappedIngame(KeyCode keyCode);

  static CascadedActionSignal& signalKeyPressed();
  static CascadedActionSignal& signalKeyReleased();
  static CascadedUnicodeSignal& signalUnicodeEntered();

  static void injectKeyDown(KeyCode key);
  static void injectKeyUp(KeyCode key);
  static void injectText(const String& text);

public:
  bool isActionCurrentlyActive(KeyCode actionId) const override;

protected:
  void sendSignalSignalUnicodeEntered(String::value_type unicode);

  virtual KeyModifier modifierStateImpl() = 0;
  virtual bool isKeyDownImpl(KeyCode keyCode) const = 0;

  static KeyModifier keyModifierForKeyCode(KeyCode key);
};


}


#endif
