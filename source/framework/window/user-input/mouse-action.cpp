#include "mouse-action.h"
#include "mouse-position-constraint-layers.h"
#include "mouse-mode-layers.h"
#include "mouse-cursor-layers.h"

#include <framework/window/render-window.h>

namespace Framework {


MouseAction::MouseAction()
  : resultSignals(new ResultSignals)
{
  abort();

  assert(isTheOnlyInstance());

  Framework::RenderWindow::signalFocusChanged().connect(std::bind(&MouseAction::abort)).track(this->trackable);
}


MouseAction::~MouseAction()
{
  MouseCursorLayers& cursorLayers = Mouse::mouseCursorLayers();
  MousePositionConstraintLayers& positionConstraintLayers = Mouse::mousePositionConstraintLayers();
  MouseModeLayers& modeLayers = Mouse::mouseModeLayers();

  modeLayers.mouseActionLayer->hide();
  positionConstraintLayers.mouseActionLayer->hide();
  cursorLayers.mouseActionLayer->hide();
}


void MouseAction::setCursor(const shared_ptr<MouseCursor>& cursor)
{
  MouseCursorLayers& cursorLayers = Mouse::mouseCursorLayers();

  cursorLayers.mouseActionLayer->setCursor(cursor);
  cursorLayers.mouseActionLayer->show();
}


void MouseAction::setPositionConstraint(const MousePositionConstraint::Ptr& constraint)
{
  MousePositionConstraintLayers& positionConstraintLayers = Mouse::mousePositionConstraintLayers();

  positionConstraintLayers.mouseActionLayer->setConstraint(constraint);
  positionConstraintLayers.mouseActionLayer->show();
}


void MouseAction::setMode(MouseMode mode)
{
  MouseModeLayers& modeLayers = Mouse::mouseModeLayers();

  modeLayers.mouseActionLayer->setMouseMode(mode);
  modeLayers.mouseActionLayer->show();
}


MouseAction::Ptr MouseAction::create()
{
  Ptr action(new MouseAction);

  Mouse::setMouseAction(action);

  return action;
}


void MouseAction::blockOtherSignals(bool blockMouseSpeed)
{
  signalBlocker = UserInput::createSignalBlocker(InputDevice::ActionPriority::MOUSE_ACTION_BLOCKER, blockMouseSpeed);
}


void MouseAction::succeedWhenPressingButton(MouseButton mouseButton)
{
  Mouse::signalButtonPressed().connect(InputDevice::ActionPriority::MOUSE_ACTION,
                                       succeedForButton(mouseButton)).track(this->trackable);
}


void MouseAction::abortWhenPressingButton(MouseButton mouseButton)
{
  Mouse::signalButtonPressed().connect(InputDevice::ActionPriority::MOUSE_ACTION,
                                       abortForButton(mouseButton)).track(this->trackable);
}


void MouseAction::succeedWhenReleasingButton(MouseButton mouseButton)
{
  Mouse::signalButtonReleased().connect(InputDevice::ActionPriority::MOUSE_ACTION,
                                        succeedForButton(mouseButton)).track(this->trackable);
}


void MouseAction::abortWhenReleasingButton(MouseButton mouseButton)
{
  Mouse::signalButtonReleased().connect(InputDevice::ActionPriority::MOUSE_ACTION,
                                        abortForButton(mouseButton)).track(this->trackable);
}


void MouseAction::succeedWhenPressingKeyboardKey(KeyCode key)
{
  Keyboard::signalKeyPressed().connect(InputDevice::ActionPriority::MOUSE_ACTION,
                                       succeedForKey(key)).track(this->trackable);
}


void MouseAction::abortWhenPressingKeyboardKey(KeyCode key)
{
  Keyboard::signalKeyPressed().connect(InputDevice::ActionPriority::MOUSE_ACTION,
                                       abortForKey(key)).track(this->trackable);
}


void MouseAction::succeedWhenReleasingKeyboardKey(KeyCode key)
{
  Keyboard::signalKeyReleased().connect(InputDevice::ActionPriority::MOUSE_ACTION,
                                        succeedForKey(key)).track(this->trackable);
}


void MouseAction::abortWhenReleasingKeyboardKey(KeyCode key)
{
  Keyboard::signalKeyReleased().connect(InputDevice::ActionPriority::MOUSE_ACTION,
                                        abortForKey(key)).track(this->trackable);
}


std::function<bool(MouseButton)> MouseAction::succeedForButton(MouseButton button)
{
  return std::bind(&MouseAction::callIfButton, _1, button, std::function<void()>(&MouseAction::succeed));
}


std::function<bool(MouseButton)> MouseAction::abortForButton(MouseButton button)
{
  return std::bind(&MouseAction::callIfButton, _1, button, std::function<void()>(&MouseAction::abort));
}


std::function<bool(KeyCode)> MouseAction::succeedForKey(KeyCode key)
{
  return std::bind(&MouseAction::callIfKey, _1, key, std::function<void()>(&MouseAction::succeed));
}


std::function<bool(KeyCode)> MouseAction::abortForKey(KeyCode key)
{
  return std::bind(&MouseAction::callIfKey, _1, key, std::function<void()>(&MouseAction::abort));
}


Signals::Signal<void()>& MouseAction::signalActionFinished()
{
  return resultSignals->signalActionFinished();
}


Signals::Signal<void()>& MouseAction::signalActionSucceeded()
{
  return resultSignals->signalActionSucceeded();
}


Signals::Signal<void()>& MouseAction::signalActionAborted()
{
  return resultSignals->signalActionAborted();
}


void MouseAction::succeed()
{
  singleton().resultSignals->succeed();
  Mouse::unsetMouseAction();
}


void MouseAction::abort()
{
  Mouse::unsetMouseAction();
}


bool MouseAction::callIfButton(MouseButton realMouseButton, MouseButton expectedMouseButton, const std::function<void()>& fn)
{
  if(realMouseButton == expectedMouseButton)
    fn();
  return false;
}


bool MouseAction::callIfKey(KeyCode key, KeyCode expectedKey, const std::function<void()>& fn)
{
  if(key == expectedKey)
    fn();
  return false;
}

// ====

MouseAction::ResultSignals::ResultSignals()
  : signalsWereSend(false),
    succeeded(false)
{
}

MouseAction::ResultSignals::~ResultSignals()
{
  assert(signalsWereSend);
}


void MouseAction::ResultSignals::sendSignals()
{
  if(!signalsWereSend)
  {
    signalsWereSend = true;

    if(succeeded)
      _signalActionSucceeded();
    else
      _signalActionAborted();

    _signalActionFinished();
  }
}


Signals::Signal<void()>& MouseAction::ResultSignals::signalActionFinished()
{
  return _signalActionFinished;
}


Signals::Signal<void()>& MouseAction::ResultSignals::signalActionSucceeded()
{
  return _signalActionSucceeded;
}


Signals::Signal<void()>& MouseAction::ResultSignals::signalActionAborted()
{
  return _signalActionAborted;
}


void MouseAction::ResultSignals::succeed()
{
  succeeded = true;
}


} // namespace Framework
