#ifndef FRAMEWORK_WINDOW_USERINPUT_INPUTKEYSTATE_INL
#define FRAMEWORK_WINDOW_USERINPUT_INPUTKEYSTATE_INL

#include "input-key-state.h"
#include <framework/frame-signals.h>
#include <framework/window/user-input.h>

namespace Framework {
namespace Window {
namespace UserInput {


template<typename T>
InputKeyState<T>::InputKeyState(T actionId,
                                InputDevice::ActionPriority priority,
                                BaseInputDevice<T>& device,
                                bool blockLowerPriorities)
  : _actionId(actionId),
    priority(priority),
    device(device),
    blockLowerPriorities(blockLowerPriorities)
{
  markNextFrame = false;
  stateIsPressed = false;
  stateWasChangedThisFrame = false;

  FrameSignals::signalFrameStart().connect(std::bind(&InputKeyState<T>::onWait, this)).track(this->trackable);
  device.signalActionBegin().connect(priority, std::bind(&InputKeyState<T>::handleActionEvent, this, _1, true)).track(this->trackable);
  device.signalActionEnd().connect(priority, std::bind(&InputKeyState<T>::handleActionEvent, this, _1, false)).track(this->trackable);

  Framework::UserInput::signalBlockerAdded().connect(std::bind(&InputKeyState<T>::handleAddedBlocker, this, _1)).track(this->trackable);
}


template<typename T>
InputKeyState<T>::~InputKeyState()
{
}


template<typename T>
bool InputKeyState<T>::isPressed() const
{
  return stateIsPressed;
}


template<typename T>
bool InputKeyState<T>::wasChangedThisFrame() const
{
  return stateWasChangedThisFrame;
}


template<typename T>
bool InputKeyState<T>::wasPressedThisFrame() const
{
  return isPressed() && wasChangedThisFrame();
}


template<typename T>
bool InputKeyState<T>::wasReleasedThisFrame() const
{
  return !isPressed() && wasChangedThisFrame();
}


template<typename T>
void InputKeyState<T>::onWait()
{
  if(markNextFrame)
  {
    markNextFrame = false;
  }else
  {
    stateWasChangedThisFrame = false;
  }

  if(stateIsPressed && !device.isActionCurrentlyActive(actionId()))
  {
    updateState(false);
  }
}


template<typename T>
void InputKeyState<T>::updateState(bool pressed)
{
  if(pressed != stateIsPressed)
  {
    stateIsPressed = pressed;
    stateWasChangedThisFrame = true;
    markNextFrame = true;
  }
}


template<typename T>
bool InputKeyState<T>::handleActionEvent(T actionId, bool pressed)
{
  if(actionId == this->actionId())
  {
    updateState(pressed);
    return blockLowerPriorities;
  }

  return false;
}


template<typename T>
void InputKeyState<T>::handleAddedBlocker(InputDevice::ActionPriority priority)
{
  if(this->priority < priority)
    updateState(false);
}


template<typename T>
void InputKeyState<T>::setActionId(T actionId)
{
  if(actionId != this->actionId())
  {
    this->_actionId = actionId;
    stateIsPressed = device.isActionCurrentlyActive(actionId);
    stateWasChangedThisFrame = false;
    markNextFrame = false;
  }
}


template<typename T>
T InputKeyState<T>::actionId() const
{
  return _actionId;
}


} // namespace UserInput
} // namespace Window
} // namespace Framework

#endif // FRAMEWORK_WINDOW_USERINPUT_INPUTKEYSTATE_H
