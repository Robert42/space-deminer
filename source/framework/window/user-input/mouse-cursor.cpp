#include "mouse-cursor.h"

namespace Framework {


MouseCursor::MouseCursor()
  : activated(false)
{
}


MouseCursor::~MouseCursor()
{
  assert(!isActive());
}


void MouseCursor::activate(const MouseCursor::Ptr& prevCursor)
{
  if(isActive())
    return;

  this->activated = true;

  activateImplementation(prevCursor);
}


void MouseCursor::deactivate(const MouseCursor::Ptr& newCursor)
{
  if(!isActive())
    return;

  this->activated = false;

  deactivateImplementation(newCursor);
}


bool MouseCursor::isActive() const
{
  return activated;
}


DefaultMouseCursor::DefaultMouseCursor()
{
}


DefaultMouseCursor::Ptr DefaultMouseCursor::create()
{
  return Ptr(new DefaultMouseCursor);
}


Signals::Signal<void(const MouseCursor::Ptr&)>& DefaultMouseCursor::signalActivate()
{
  return _signalActivate;
}


Signals::Signal<void(const MouseCursor::Ptr&)>& DefaultMouseCursor::signalDeactivate()
{
  return _signalDeactivate;
}


void DefaultMouseCursor::activateImplementation(const MouseCursor::Ptr& prevCursor)
{
  _signalActivate(prevCursor);
}


void DefaultMouseCursor::deactivateImplementation(const MouseCursor::Ptr& newCursor)
{
  _signalDeactivate(newCursor);
}


namespace DefaultMouseCursors {
DefaultMouseCursor::Ptr noCursor;
DefaultMouseCursor::Ptr defaultCursor;
DefaultMouseCursor::Ptr textCursor;
DefaultMouseCursor::Ptr moveCursor;
DefaultMouseCursor::Ptr moveNSCursor;
DefaultMouseCursor::Ptr moveWECursor;
DefaultMouseCursor::Ptr moveSwNeCursor;
DefaultMouseCursor::Ptr moveNwSeCursor;

DefaultMouseCursor::Ptr tweakUIHover;
DefaultMouseCursor::Ptr tweakUIIdle;
DefaultMouseCursor::Ptr tweakUIRotate;

int _numberReferences = 0;


void initDefaultCursors()
{
  if(_numberReferences == 0)
  {
    noCursor = DefaultMouseCursor::create();
    defaultCursor = DefaultMouseCursor::create();
    textCursor = DefaultMouseCursor::create();
    moveCursor = DefaultMouseCursor::create();
    moveNSCursor = DefaultMouseCursor::create();
    moveWECursor = DefaultMouseCursor::create();
    moveSwNeCursor = DefaultMouseCursor::create();
    moveNwSeCursor = DefaultMouseCursor::create();

    tweakUIHover = DefaultMouseCursor::create();
    tweakUIIdle = DefaultMouseCursor::create();
    tweakUIRotate = DefaultMouseCursor::create();
  }

  _numberReferences++;
}


void deinitDefaultCursors()
{
  _numberReferences--;
  if(_numberReferences == 0)
  {
    noCursor.reset();
    defaultCursor.reset();
    textCursor.reset();
    moveCursor.reset();
    moveNSCursor.reset();
    moveWECursor.reset();
    moveSwNeCursor.reset();
    moveNwSeCursor.reset();

    tweakUIHover.reset();
    tweakUIIdle.reset();
    tweakUIRotate.reset();
  }
}


}
} // namespace Framework
