#ifndef FRAMEWORK_MOUSECURSOR_H
#define FRAMEWORK_MOUSECURSOR_H

#include <base/signals/signal.h>

namespace Framework {

using namespace Base;

class MouseCursor
{
public:
  typedef shared_ptr<MouseCursor> Ptr;
  typedef shared_ptr<const MouseCursor> ConstPtr;

private:
  bool activated;

public:
  MouseCursor();
  virtual ~MouseCursor();

  bool isActive() const;

private:
  friend class Mouse;
  void activate(const Ptr& newCursor);
  void deactivate(const Ptr& newCursor);

protected:
  virtual void activateImplementation(const Ptr& prevCursor) = 0;
  virtual void deactivateImplementation(const Ptr& newCursor) = 0;
};


class DefaultMouseCursor : public MouseCursor
{
public:
  typedef shared_ptr<DefaultMouseCursor> Ptr;
  typedef shared_ptr<const DefaultMouseCursor> ConstPtr;

private:
  Signals::CallableSignal<void(const MouseCursor::Ptr&)> _signalActivate;
  Signals::CallableSignal<void(const MouseCursor::Ptr&)> _signalDeactivate;

public:
  DefaultMouseCursor();

  static Ptr create();

public:
  Signals::Signal<void(const MouseCursor::Ptr&)>& signalActivate();
  Signals::Signal<void(const MouseCursor::Ptr&)>& signalDeactivate();

protected:
  void activateImplementation(const MouseCursor::Ptr& prevCursor) override;
  void deactivateImplementation(const MouseCursor::Ptr& newCursor) override;
};



namespace DefaultMouseCursors {
extern DefaultMouseCursor::Ptr noCursor;
extern DefaultMouseCursor::Ptr defaultCursor;
extern DefaultMouseCursor::Ptr textCursor;
extern DefaultMouseCursor::Ptr moveCursor;
extern DefaultMouseCursor::Ptr moveNSCursor;
extern DefaultMouseCursor::Ptr moveWECursor;
extern DefaultMouseCursor::Ptr moveSwNeCursor;
extern DefaultMouseCursor::Ptr moveNwSeCursor;

extern DefaultMouseCursor::Ptr tweakUIHover;
extern DefaultMouseCursor::Ptr tweakUIIdle;
extern DefaultMouseCursor::Ptr tweakUIRotate;
}
} // namespace Framework

#endif // FRAMEWORK_MOUSECURSOR_H
