#ifndef _FRAMEWORK_WINDOW_USERINPUT_MOUSE_H_
#define _FRAMEWORK_WINDOW_USERINPUT_MOUSE_H_

#include <framework/window/user-input/input-key-state.h>
#include <framework/window/user-input/mouse-position-constraint.h>
#include <base/strings/string.h>
#include <base/singleton.h>
#include <base/enum-macros.h>

namespace Framework {
class RenderWindow;
class MouseCursor;
class MouseAction;

class MouseModeLayers;
class MouseCursorLayers;
class MousePositionConstraintLayers;

BEGIN_ENUMERATION(MouseButton,)
  BUTTON_0,
  BUTTON_1,
  BUTTON_2,
  BUTTON_3,
  BUTTON_4,
  LEFT = BUTTON_0,
  RIGHT = BUTTON_1,
  MIDDLE = BUTTON_2,
ENUMERATION_BASIC_OPERATORS(MouseButton)
END_ENUMERATION;

BEGIN_ENUMERATION(MouseMode,)
  RELATIVE,
  INGAME,
  SYSTEM
ENUMERATION_BASIC_OPERATORS(MouseMode)
ENUM_TO_STRING_DECLARATION(MouseMode)
END_ENUMERATION;


/** Represents the Mouse.
 */
class Mouse : public BaseInputDevice<MouseButton>, public Singleton<Mouse>
{
public:
  class ButtonState : public Window::UserInput::InputKeyState<MouseButton>
  {
  public:
    ButtonState(MouseButton button,
                ActionPriority priority,
                bool blockLowerPriorities)
      : Window::UserInput::InputKeyState<MouseButton>(button,
                                                      priority,
                                                      Mouse::singleton(),
                                                      blockLowerPriorities)
    {
    }
  };

public:
  typedef Signals::CascadedSignals<bool(), ActionPriority> CascadedMouseMovedSignal;

public:
  Signals::Trackable trackable;

public:
  real speedFactor;

protected:
  Mouse();
  ~Mouse();

public:
  /** Returns, whether the key represented by the given keycode is currently pressed down.
   *
   * @param keyCode
   *
   * @return true if the key keyCode is pressed.
   */
  static bool isButtonPressed(MouseButton keyCode);

  /** Signal getting sent when the mouse has been moved.
   */
  static CascadedMouseMovedSignal& signalMouseMoved();

  /** Signal getting sent when the mouse-wheel has been moved.
   */
  static CascadedMouseMovedSignal& signalMouseWheelMoved();

  static Signals::Signal<void(MouseMode)>& signalModeChanged();
  static Signals::Signal<void(real)>& signalCursorScaleChanged();

  static CascadedActionSignal& signalButtonPressed();
  static CascadedActionSignal& signalButtonReleased();

  /** Sets the mouse position in pixels relative to the upper left window corner.
   *
   * Whether a trigger signal gets triggered or is undefined.
   */
  static void setAbsolutePosition(ivec2 pos, bool adaptRelativeMovement);

  static Blocker blockSpeed();

  static MouseMode mouseMode();

  static const shared_ptr<MouseCursor>& mouseCursor();

  static void setMouseCursorScale(real scale);
  static real mouseCursorScale();

  static void setMouseAction(const shared_ptr<MouseAction>& action);
  static void unsetMouseAction();
  static const shared_ptr<MouseAction>& mouseAction();

  static const MousePositionConstraint::Ptr& mousePositionConstraint();

  static ivec2 relativeMovement();
  static ivec2 absolutePosition();
  static vec2 currentSpeed();
  static real wheelMovement();

  static MouseModeLayers& mouseModeLayers();
  static MouseCursorLayers& mouseCursorLayers();
  static MousePositionConstraintLayers& mousePositionConstraintLayers();

protected:
  void setMouseCursor(const shared_ptr<MouseCursor>& cursor);
  void setMouseMode(MouseMode mode);
  void setMousePositionConstraint(const MousePositionConstraint::Ptr& constraint);

public:
  bool isActionCurrentlyActive(MouseButton actionId) const override;

private:
  typedef Signals::CallableCascadedSignals<bool(), ActionPriority> CallableCascadedMouseMovedSignal;

  shared_ptr<MouseModeLayers> _mouseModeLayers;
  shared_ptr<MouseCursorLayers> _mouseCursorLayers;
  shared_ptr<MousePositionConstraintLayers> _mousePositionConstraintLayers;

  CallableCascadedMouseMovedSignal _signalMouseMoved;
  CallableCascadedMouseMovedSignal _signalMouseWheelMoved;
  Signals::CallableSignal<void(MouseMode)> _signalModeChanged;
  Signals::CallableSignal<void(real)> _signalCursorScaleChanged;

  ivec2 _relativeMovement;
  ivec2 _absolutePosition;
  vec2 _currentSpeed;
  real _wheelMovement;

  real time;
  real windowDiagonal;

  int speedBlocked;

  MouseMode _mode;
  shared_ptr<MouseCursor> _cursor;
  shared_ptr<MouseAction> _mouseAction;
  real _mouseCursorScale;

  MousePositionConstraint::Ptr _mousePositionConstraint;
  Signals::Connection mousePositionConstraintSignalConnection;

  void applyConstraint();

protected:
  void sendSignalMouseMoved(const ivec2& relativeMovement);
  void sendSignalMouseWheelMoved(real relZ);

  virtual bool isButtonPressedImpl(MouseButton button) const = 0;

  void resetMouseState();

private:
  void updateTime(real time);
  void onWindowResized();
};
}


#endif
