#include <dependencies.h>
#include <framework/window/user-input/mouse.h>
#include <framework/window/user-input/mouse-action.h>
#include <framework/window/user-input/mouse-mode-layers.h>
#include <framework/window/user-input/mouse-cursor-layers.h>
#include <framework/window/user-input/mouse-position-constraint-layers.h>
#include <framework/window/render-window.h>
#include <framework/frame-signals.h>
#include <framework/developer-tools/terminal.h>

#include "mouse-cursor.h"


namespace Framework {
namespace DefaultMouseCursors {
void initDefaultCursors();
void deinitDefaultCursors();
}


ivec2 Mouse::relativeMovement()
{
  return singleton()._relativeMovement;
}


ivec2 Mouse::absolutePosition()
{
  return singleton()._absolutePosition;
}


vec2 Mouse::currentSpeed()
{
  return singleton()._currentSpeed;
}


real Mouse::wheelMovement()
{
  return singleton()._wheelMovement;
}


Mouse::Mouse()
{
  this->speedBlocked = 0;

  this->windowDiagonal = 0.0;
  this->time = 0.0;
  this->speedFactor = 1.0;

  this->_mousePositionConstraint = MousePositionConstraint::FullscreenRectangleConstraint::create(false);

  this->_mode = MouseMode::RELATIVE;

  this->_mouseCursorScale = 0.6f;

  this->_mouseModeLayers = MouseModeLayers::create(std::bind(&Mouse::setMouseMode, this, _1));
  this->_mouseCursorLayers = MouseCursorLayers::create(std::bind(&Mouse::setMouseCursor, this, _1));
  this->_mousePositionConstraintLayers = MousePositionConstraintLayers::create(std::bind(&Mouse::setMousePositionConstraint, this, _1));

  FrameSignals::signalFrameStart().connect(std::bind(&Mouse::updateTime,
                                                     this,
                                                     _1),
                                           Signals::PREPEND_SLOT).track(this->trackable);

  RenderWindow::signalWindowResized().connect(std::bind(&Mouse::onWindowResized,
                                                        this)).track(this->trackable);
  onWindowResized();

  DefaultMouseCursors::initDefaultCursors();
  setMouseCursor(DefaultMouseCursors::defaultCursor);
}


Mouse::~Mouse()
{
  _mouseModeLayers.reset();
  _mouseCursorLayers.reset();
  _mousePositionConstraintLayers.reset();

  MouseAction::abort();

  if(this->isTheOnlyInstance())
  {
    setMouseCursor(MouseCursor::Ptr());
  }else
  {
    setMouseCursor(previousSingletonPointer()->_cursor);
    setMousePositionConstraint(previousSingletonPointer()->mousePositionConstraint());
  }

  DefaultMouseCursors::deinitDefaultCursors();
}


void Mouse::sendSignalMouseMoved(const ivec2& relativeMovement)
{
  if(relativeMovement == ivec2(0, 0))
    return;

  this->_relativeMovement = relativeMovement;

  if(windowDiagonal>0.0 && time>0.0 && speedBlocked<=0)
  {
    vec2 speed = speedFactor * vec2(relativeMovement) / (windowDiagonal*time);
    this->_currentSpeed = speed;
  }

  if(mouseMode() != MouseMode::RELATIVE)
  {
    // If the mouse mode id not relative, the mouse cursor should be moved
    this->setAbsolutePosition(absolutePosition() + relativeMovement, true);
  }else
  {
    // If setAbsolutePosition wasn't called, no event was sent, so we have to catch it up
    this->_signalMouseMoved();
  }
}

void Mouse::sendSignalMouseWheelMoved(real relZ)
{
  if(relZ == 0.)
    return;

  _wheelMovement = relZ;

  _signalMouseWheelMoved();
}


void Mouse::resetMouseState()
{
  _relativeMovement = ivec2(0);
  _currentSpeed = vec2(0);
  _wheelMovement = 0.f;
}


void Mouse::setAbsolutePosition(ivec2 pos, bool adaptRelativeMovement)
{
  Mouse& mouse = singleton();

  ivec2 relative = relativeMovement();

  mouse._mousePositionConstraint->applyContraint(inout(pos), inout(relative));

  bool positionHasChanged = (absolutePosition() != pos);
  bool relativeHasChanged = false;

  if(adaptRelativeMovement)
    relativeHasChanged = (relative!=relativeMovement());

  if(!positionHasChanged && !relativeHasChanged)
    return;

  if(adaptRelativeMovement)
  {
    // This line is here apply the constraint of the mouse cursor also applied to the relative movement
    // Otherwise for example dragging editor elements will lead to the cursoer beeing outside the dragged object
    // because the object could move outside the window while the cursor couldn't
    mouse._relativeMovement = relative;
  }

  mouse._absolutePosition = pos;

  mouse._signalMouseMoved();
}


void Mouse::updateTime(real time)
{
  this->time = time;
}


void Mouse::onWindowResized()
{
  const ivec2 windowSize = RenderWindow::size();

  real windowDiagonalSquare = static_cast<real>(windowSize.x*windowSize.x + windowSize.y*windowSize.y);

  this->windowDiagonal = sqrt(windowDiagonalSquare);
}


Blocker Mouse::blockSpeed()
{
  Mouse* currentInstance = singletonPtr();

  return Blocker(&currentInstance->speedBlocked,
                 [currentInstance](){return currentInstance==singletonPtr();});
}


void Mouse::setMouseMode(MouseMode mode)
{
  Mouse& mouse = singleton();

  if(mouseMode() != mode)
  {
    mouse._mode = mode;

    mouse._signalModeChanged(mode);
  }
}


MouseMode Mouse::mouseMode()
{
  return singleton()._mode;
}


void Mouse::setMouseCursor(const shared_ptr<MouseCursor>& cursor)
{
  shared_ptr<MouseCursor> prevCursor = mouseCursor();
  const shared_ptr<MouseCursor>& newCursor = cursor;

  if(prevCursor == newCursor)
    return;

  if(prevCursor)
    prevCursor->deactivate(newCursor);

  singleton()._cursor = newCursor;

  if(newCursor)
    newCursor->activate(prevCursor);
}


const shared_ptr<MouseCursor>& Mouse::mouseCursor()
{
  return singleton()._cursor;
}


void Mouse::setMouseCursorScale(real scale)
{
  Mouse& mouse = singleton();

  scale = clamp<real>(scale, 0.25, 2);

  if(mouseCursorScale() != scale)
  {
    mouse._mouseCursorScale = scale;

    mouse._signalCursorScaleChanged(mouse._mouseCursorScale);
  }
}


real Mouse::mouseCursorScale()
{
  return singleton()._mouseCursorScale;
}


void Mouse::setMouseAction(const shared_ptr<MouseAction>& action)
{
  unsetMouseAction();
  singleton()._mouseAction = action;
}


void Mouse::unsetMouseAction()
{
  if(singleton()._mouseAction)
  {
    MouseAction::ResultSignals::Ptr resultSignals = singleton()._mouseAction->resultSignals;

    singleton()._mouseAction.reset();

    resultSignals->sendSignals();
  }
}


const shared_ptr<MouseAction>& Mouse::mouseAction()
{
  return singleton()._mouseAction;
}


void Mouse::setMousePositionConstraint(const MousePositionConstraint::Ptr& constraint)
{
  Mouse& mouse = singleton();

  mouse.mousePositionConstraintSignalConnection.disconnect();

  mouse._mousePositionConstraint = constraint;

  mouse.applyConstraint();

  mouse.mousePositionConstraintSignalConnection = constraint->signalChanged().connect(std::bind(&Mouse::applyConstraint, &mouse));
  mouse.mousePositionConstraintSignalConnection.track(mouse.trackable);
}


const MousePositionConstraint::Ptr& Mouse::mousePositionConstraint()
{
  return singleton()._mousePositionConstraint;
}


void Mouse::applyConstraint()
{
  setAbsolutePosition(absolutePosition(), false);
}


bool Mouse::isButtonPressed(MouseButton keyCode)
{
  return singleton().isButtonPressedImpl(keyCode);
}


bool Mouse::isActionCurrentlyActive(MouseButton actionId) const
{
  return isButtonPressed(actionId);
}


Mouse::CascadedMouseMovedSignal& Mouse::signalMouseMoved()
{
  return singleton()._signalMouseMoved;
}


Mouse::CascadedMouseMovedSignal& Mouse::signalMouseWheelMoved()
{
  return singleton()._signalMouseWheelMoved;
}


Signals::Signal<void(MouseMode)>& Mouse::signalModeChanged()
{
  return singleton()._signalModeChanged;
}


Signals::Signal<void(real)>& Mouse::signalCursorScaleChanged()
{
  return singleton()._signalCursorScaleChanged;
}


Mouse::CascadedActionSignal& Mouse::signalButtonPressed()
{
  return singleton().signalActionBegin();
}


Mouse::CascadedActionSignal& Mouse::signalButtonReleased()
{
  return singleton().signalActionEnd();
}


MouseModeLayers& Mouse::mouseModeLayers()
{
  return *singleton()._mouseModeLayers;
}


MouseCursorLayers& Mouse::mouseCursorLayers()
{
  return *singleton()._mouseCursorLayers;
}


MousePositionConstraintLayers& Mouse::mousePositionConstraintLayers()
{
  return *singleton()._mousePositionConstraintLayers;
}


std::string MouseMode::toStdString(const MouseMode& cursor)
{
  switch(cursor.value)
  {
  case MouseMode::RELATIVE:
    return "Mode::Relative";
  case MouseMode::INGAME:
    return "Mode::Ingame";
  case MouseMode::SYSTEM:
    return "Mode::System";
  }
  assert(false);
  throw std::logic_error("Unknown Mouse Mode "+std::to_string(static_cast<int>(cursor.value)));
}


}
