#ifndef _FRAMEWORK_WINDOW_USERINPUT_INPUT_DEVICE_H_
#define _FRAMEWORK_WINDOW_USERINPUT_INPUT_DEVICE_H_

#include <base/signals/cascaded-signals.h>

namespace Framework {
using namespace Base;


/** Interface for input devices.
 */
class InputDevice : public noncopyable
{
public:
  BEGIN_ENUMERATION(ActionPriority,)
    FALLBACK,
    INGAME_BLOCKER,
    INGAME,
    INGAME_DEBUG_BLOCKER,
    INGAME_DEBUG,
    HUD_BLOCKER,
    HUD,
    GUI_BLOCKER,
    GUI,
    GUI_SPECIAL_EFFECT,
    NODE_EDITOR_BLOCKER,
    NODE_EDITOR,
    TEXTURE_VIEWER_BLOCKER,
    TEXTURE_VIEWER,
    INGAME_EDITOR_BLOCKER,
    TWEAK_UI,
    INGAME_EDITOR,
    TERMINAL_BLOCKER,
    TERMINAL,
    MOUSE_ACTION_BLOCKER,
    MOUSE_ACTION,
    MOUSE_CURSOR,
    BLOCK_ABSOLUTELY_EVERY_INPUT
  ENUMERATION_BASIC_OPERATORS(ActionPriority)
  ENUMERATION_COMPARISON_OPERATORS(ActionPriority)
  END_ENUMERATION;

public:
  virtual ~InputDevice();

public:
  /** Returns, whether the device is supported by this game during this run.
   */
  virtual bool isSupported() = 0;
};


/** Base class for InputDevices.
 *
 * This class contains an action signal beeing sent, when the device receives a certain action.
 */
template<typename ActionId>
class BaseInputDevice : public InputDevice
{
public:
  typedef bool(SignalSignature)(ActionId);
  typedef Signals::CascadedSignals<SignalSignature, ActionPriority> CascadedActionSignal;

private:
  typedef Signals::CallableCascadedSignals<SignalSignature, ActionPriority> CallableCascadedSignal;

  CallableCascadedSignal _signalActionBegin;
  CallableCascadedSignal _signalActionEnd;

protected:
  BaseInputDevice();

public:
  virtual bool isActionCurrentlyActive(ActionId actionId) const = 0;

  /** The signal getting called if a certain action get's received from the input device.
   *
   * If you are connecting a method, you should use a priority. For example:
   * ```
   * keyboard->signalActionSent.connect(InputDevice::ActionPriority::INGAME_MIDDLE, &myFunction);
   * ```
   *
   * Your method connected to the signal must return false if it hadn't handled the signal and true, if it had handeled the
   * signal so one one other should handle it anymore.
   */
  CascadedActionSignal& signalActionBegin();
  CascadedActionSignal& signalActionEnd();

protected:
  bool sendSignalActionBegin(ActionId actionId);
  bool sendSignalActionEnd(ActionId actionId);
};


}


#include "input-device.inl"

#endif
