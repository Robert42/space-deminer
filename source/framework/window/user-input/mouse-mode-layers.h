#ifndef FRAMEWORK_WINDOW_USERINPUT_MOUSECURSORLAYERS_H
#define FRAMEWORK_WINDOW_USERINPUT_MOUSECURSORLAYERS_H

#include <framework/window/user-input/mouse.h>

#include <base/containers/layers.h>

namespace Framework {

using namespace Base;


class MouseModeLayers final : public noncopyable
{
public:
  typedef shared_ptr<MouseModeLayers> Ptr;
  typedef std::function<void(MouseMode)> Setter;

  class Layer : public Containers::BaseLayers::Layer
  {
  public:
    typedef shared_ptr<Layer> Ptr;
    typedef shared_ptr<const Layer> ConstPtr;

  private:
    MouseMode _mode;

    Signals::CallableSignal<void()> _signalMouseModeChanged;

  private:
    Layer(MouseMode mode);

  public:
    Signals::Signal<void()>& signalMouseModeChanged();

    void setMouseMode(MouseMode mode);
    MouseMode mouseMode() const;

  public:
    static Ptr create(MouseMode mode);

    ~Layer();
  };

private:
  Containers::Layers<Layer> layers;
  Setter setMouseMode;

public:
  Signals::Trackable trackable;
  Layer::Ptr mouseActionLayer, terminalLayer, ingameEditorLayer, tweakUILayer, ingameUILayer, ingameLayer;

private:
  MouseModeLayers(const Setter& setter);

public:
  static Ptr create(const Setter& setter);

private:
  void udpateMode();
};


} // namespace Framework

#endif // FRAMEWORK_WINDOW_USERINPUT_MOUSECURSORLAYERS_H
