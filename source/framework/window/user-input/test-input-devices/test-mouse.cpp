#include "test-mouse.h"

#include <framework/window/user-input/mouse-position-constraint-layers.h>

namespace Framework {


TestMouse::TestMouse()
{
  Mouse::mousePositionConstraintLayers().testLayer->setVisible(true);
  Mouse::mousePositionConstraintLayers().testLayer->setConstraint(MousePositionConstraint::createNoConstraint());

  setMouseMode(MouseMode::INGAME);
}


TestMouse::~TestMouse()
{
  Mouse::mousePositionConstraintLayers().testLayer->setVisible(false);
}


bool TestMouse::isSupported()
{
  return true;
}


bool TestMouse::isButtonPressedImpl(MouseButton button) const
{
  return _pressedMouseButtons.contains(button);
}


void TestMouse::press(MouseButton button)
{
  if(!isButtonPressed(button))
  {
    _pressedMouseButtons.insert(button);
    sendSignalActionBegin(button);
  }
}


void TestMouse::release(MouseButton button)
{
  if(isButtonPressed(button))
  {
    _pressedMouseButtons.remove(button);
    sendSignalActionEnd(button);
  }
}


void TestMouse::moveTo(const ivec2& position)
{
  move(position - absolutePosition());
}


void TestMouse::move(const ivec2& relative)
{
  this->sendSignalMouseMoved(relative);
}


void TestMouse::useWheel(real relZ)
{
  sendSignalMouseWheelMoved(relZ);
}


} // namespace Framework
