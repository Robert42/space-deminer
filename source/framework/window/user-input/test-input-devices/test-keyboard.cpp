#include "test-keyboard.h"

namespace Framework {


TestKeyboard::TestKeyboard()
{
  modifiers = KeyModifier::NONE;
}


void TestKeyboard::press(KeyCode key)
{
  if(!isKeyDown(key))
  {
    keys.insert(key);

    modifiers |= keyModifierForKeyCode(key);

    sendSignalActionBegin(key);
  }
}


void TestKeyboard::release(KeyCode key)
{
  if(isKeyDown(key))
  {
    keys.remove(key);

    modifiers &= ~keyModifierForKeyCode(key);

    sendSignalActionEnd(key);
  }
}


bool TestKeyboard::isSupported()
{
  return true;
}


KeyModifier TestKeyboard::modifierStateImpl()
{
  return modifiers;
}


bool TestKeyboard::isKeyDownImpl(KeyCode keyCode) const
{
  return keys.contains(keyCode);
}


} // namespace Framework
