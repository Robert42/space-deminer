#ifndef FRAMEWORK_TESTMOUSE_H
#define FRAMEWORK_TESTMOUSE_H

#include <framework/window/user-input/mouse.h>

namespace Framework {

class TestMouse : public Mouse
{
private:
  QSet<MouseButton> _pressedMouseButtons;

public:
  TestMouse();
  ~TestMouse();

  bool isSupported() override;

  void press(MouseButton button);
  void release(MouseButton button);

  void moveTo(const ivec2& position);
  void move(const ivec2& relative);

  void useWheel(real relZ);

private:
  bool isButtonPressedImpl(MouseButton button) const override;
};

} // namespace Framework

#endif // FRAMEWORK_TESTMOUSE_H
