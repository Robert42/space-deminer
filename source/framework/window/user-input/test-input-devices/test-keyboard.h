#ifndef FRAMEWORK_TESTKEYBOARD_H
#define FRAMEWORK_TESTKEYBOARD_H

#include <framework/window/user-input/keyboard.h>

namespace Framework {


class TestKeyboard : public Keyboard
{
private:
  KeyModifier modifiers;
  QSet<KeyCode> keys;

public:
  TestKeyboard();

  void press(KeyCode key);
  void release(KeyCode key);

private:
  bool isSupported() override;

  KeyModifier modifierStateImpl() override;
  bool isKeyDownImpl(KeyCode keyCode) const override;
};


} // namespace Framework

#endif // FRAMEWORK_TESTKEYBOARD_H
