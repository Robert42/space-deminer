#include "mouse-mode-layers.h"


namespace Framework {


MouseModeLayers::Layer::Ptr MouseModeLayers::Layer::create(MouseMode mode)
{
  return Ptr(new Layer(mode));
}


MouseModeLayers::Layer::Layer(MouseMode mode)
  : _mode(mode)
{
}


MouseModeLayers::Layer::~Layer()
{
}


void MouseModeLayers::Layer::setMouseMode(MouseMode mode)
{
  if(this->mouseMode() != mode)
  {
    this->_mode = mode;

    _signalMouseModeChanged();
  }
}


MouseMode MouseModeLayers::Layer::mouseMode() const
{
  return _mode;
}


Signals::Signal<void()>& MouseModeLayers::Layer::signalMouseModeChanged()
{
  return _signalMouseModeChanged;
}


// ====


MouseModeLayers::MouseModeLayers(const Setter& setter)
  : setMouseMode(setter)
{
  layers.pushTopLayer(ingameLayer = Layer::create(MouseMode::RELATIVE));
  layers.pushTopLayer(ingameUILayer = Layer::create(MouseMode::INGAME));
  layers.pushTopLayer(tweakUILayer = Layer::create(MouseMode::INGAME));
  layers.pushTopLayer(ingameEditorLayer = Layer::create(MouseMode::INGAME));
  layers.pushTopLayer(terminalLayer = Layer::create(MouseMode::RELATIVE));
  layers.pushTopLayer(mouseActionLayer = Layer::create(MouseMode::INGAME));

  ingameLayer->setVisible(true);

  for(const Layer::Ptr& l : layers.allLayers())
  {
    l->signalMouseModeChanged().connect(std::bind(&MouseModeLayers::udpateMode, this)).track(this->trackable);
  }

  layers.signalVisibleLayersChanged.connect(std::bind(&MouseModeLayers::udpateMode, this)).track(this->trackable);
}


MouseModeLayers::Ptr MouseModeLayers::create(const Setter& setter)
{
  return Ptr(new MouseModeLayers(setter));
}


void MouseModeLayers::udpateMode()
{
  if(layers.areAnyVisibleLayers())
  {
    setMouseMode(layers.topVisibleLayer()->mouseMode());
  }
}


} // namespace Framework
