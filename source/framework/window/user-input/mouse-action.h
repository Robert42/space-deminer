#ifndef FRAMEWORK_MOUSEACTION_H
#define FRAMEWORK_MOUSEACTION_H

#include "mouse.h"
#include "mouse-cursor.h"
#include "mouse-position-constraint.h"

#include <framework/window/user-input.h>

#include <base/singleton.h>

namespace Framework {


class MouseAction final : public Singleton<MouseAction>
{
public:
  typedef shared_ptr<MouseAction> Ptr;

  class ResultSignals
  {
  public:
    typedef shared_ptr<ResultSignals> Ptr;

  private:
    bool signalsWereSend;
    bool succeeded;
    Signals::CallableSignal<void()> _signalActionFinished;
    Signals::CallableSignal<void()> _signalActionSucceeded;
    Signals::CallableSignal<void()> _signalActionAborted;

  public:
    ResultSignals();
    ~ResultSignals();

  public:
    Signals::Signal<void()>& signalActionFinished();
    Signals::Signal<void()>& signalActionSucceeded();
    Signals::Signal<void()>& signalActionAborted();

    void sendSignals();
    void succeed();
  };

public:
  Signals::Trackable trackable;

private:
  UserInput::SignalBlocker::Ptr signalBlocker;

public:
  const ResultSignals::Ptr resultSignals;


private:
  MouseAction();

public:
  ~MouseAction();

  static Ptr create();

public:
  void setCursor(const shared_ptr<MouseCursor>& cursor);
  void setPositionConstraint(const MousePositionConstraint::Ptr& constraint);
  void setMode(MouseMode mode);

  void blockOtherSignals(bool blockMouseSpeed=true);

  void succeedWhenPressingButton(MouseButton mouseButton);
  void abortWhenPressingButton(MouseButton mouseButton);
  void succeedWhenReleasingButton(MouseButton mouseButton);
  void abortWhenReleasingButton(MouseButton mouseButton);

  void succeedWhenPressingKeyboardKey(KeyCode key);
  void abortWhenPressingKeyboardKey(KeyCode key);
  void succeedWhenReleasingKeyboardKey(KeyCode key);
  void abortWhenReleasingKeyboardKey(KeyCode key);

  Signals::Signal<void()>& signalActionFinished();
  Signals::Signal<void()>& signalActionSucceeded();
  Signals::Signal<void()>& signalActionAborted();

  static void succeed();
  static void abort();

private:
  static std::function<bool(MouseButton)> abortForButton(MouseButton button);
  static std::function<bool(MouseButton)> succeedForButton(MouseButton button);
  static std::function<bool(KeyCode)> abortForKey(KeyCode key);
  static std::function<bool(KeyCode)> succeedForKey(KeyCode key);

  static bool callIfButton(MouseButton realMouseButton, MouseButton expectedMouseButton, const std::function<void()>& fn);
  static bool callIfKey(KeyCode key, KeyCode expectedKey, const std::function<void()>& fn);
};


} // namespace Framework

#endif // FRAMEWORK_MOUSEACTION_H
