#include "mouse-position-constraint-layers.h"

namespace Framework {

void MousePositionConstraintLayers::Layer::setConstraint(const MousePositionConstraint::Ptr& constraint)
{
  if(this->constraint() != constraint)
  {
    this->_constraint = constraint;

    _signalConstraintChanged();
  }
}


const MousePositionConstraint::Ptr& MousePositionConstraintLayers::Layer::constraint() const
{
  return _constraint;
}


MousePositionConstraintLayers::Layer::Layer(const MousePositionConstraint::Ptr& constraint)
  : Containers::BaseLayers::Layer(true),
    _constraint(constraint)
{
}


Signals::Signal<void()>& MousePositionConstraintLayers::Layer::signalConstraintChanged()
{
  return _signalConstraintChanged;
}


MousePositionConstraintLayers::Layer::~Layer()
{
}


MousePositionConstraintLayers::Layer::Ptr MousePositionConstraintLayers::Layer::create(const MousePositionConstraint::Ptr& cursor)
{
  return Ptr(new MousePositionConstraintLayers::Layer(cursor));
}


// ====


MousePositionConstraintLayers::MousePositionConstraintLayers(const Setter& setter)
  : setMousePositionConstraint(setter)
{
  layers.pushTopLayer(fallbackLayer = Layer::create(MousePositionConstraint::createFullscreenConstraint(false)));
  layers.pushTopLayer(rotoCursorLayer = Layer::create(MousePositionConstraint::createNoConstraint()));
  layers.pushTopLayer(mouseActionLayer = Layer::create(MousePositionConstraint::createFullscreenConstraint(false)));
  layers.pushTopLayer(testLayer = Layer::create(MousePositionConstraint::createNoConstraint()));

  fallbackLayer->setVisible(true);

  for(const Layer::Ptr& l : layers.allLayers())
  {
    l->signalConstraintChanged().connect(std::bind(&MousePositionConstraintLayers::udpateConstraint, this)).track(this->trackable);
  }

  layers.signalVisibleLayersChanged.connect(std::bind(&MousePositionConstraintLayers::udpateConstraint, this)).track(this->trackable);
}


MousePositionConstraintLayers::Ptr MousePositionConstraintLayers::create(const Setter& setter)
{
  return Ptr(new MousePositionConstraintLayers(setter));
}


void MousePositionConstraintLayers::udpateConstraint()
{
  if(layers.areAnyVisibleLayers())
  {
    setMousePositionConstraint(layers.topVisibleLayer()->constraint());
  }
}


} // namespace Framework
