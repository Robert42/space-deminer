#include "mouse-position-constraint.h"

#include <framework/window/render-window.h>

namespace Framework {

MousePositionConstraint::MousePositionConstraint()
{
}


MousePositionConstraint::~MousePositionConstraint()
{
}


Signals::Signal<void()>& MousePositionConstraint::signalChanged()
{
  return _signalChanged;
}


std::shared_ptr<MousePositionConstraint::FullscreenRectangleConstraint> MousePositionConstraint::createFullscreenConstraint(bool wraparound)
{
  return FullscreenRectangleConstraint::create(wraparound);
}


std::shared_ptr<MousePositionConstraint::RectangleConstraint> MousePositionConstraint::createRectangleConstraint(const Rectangle<ivec2>& rectangle, bool wraparound)
{
  return RectangleConstraint::create(rectangle, wraparound);
}


std::shared_ptr<MousePositionConstraint::NoConstraint> MousePositionConstraint::createNoConstraint()
{
  return NoConstraint::create();
}


// ====

MousePositionConstraint::BaseRectangleConstraint::BaseRectangleConstraint(const Rectangle<ivec2>& rectangle, bool wraparound)
  : _rectangle(rectangle),
    _wraparound(wraparound)
{
}


void MousePositionConstraint::BaseRectangleConstraint::_setRectangle(const Rectangle<ivec2>& rectangle)
{
  if(this->rectangle() != rectangle)
  {
    this->_rectangle = rectangle;

    _signalChanged();
  }
}


void MousePositionConstraint::BaseRectangleConstraint::setWrapAround(bool wraparound)
{
  if(this->isWrappingAround() != wraparound)
  {
    this->_wraparound = wraparound;

    _signalChanged();
  }
}


const Rectangle<ivec2>& MousePositionConstraint::BaseRectangleConstraint::rectangle() const
{
  return this->_rectangle;
}


bool MousePositionConstraint::BaseRectangleConstraint::isWrappingAround() const
{
  return this->_wraparound;
}


void MousePositionConstraint::BaseRectangleConstraint::applyContraint(const InOutput<ivec2>& position, const InOutput<ivec2>& relativeMovement) const
{
  ivec2& pos = position.value;
  ivec2& relative = relativeMovement.value;

  if(this->isWrappingAround())
  {
    pos = this->rectangle().wraparound(pos);
  }else
  {
    ivec2 oldPos = pos-relative;

    pos = this->rectangle().clamp(pos);
    relative = pos - oldPos;
  }
}


void MousePositionConstraint::BaseRectangleConstraint::applyFirstTime(const InOutput<ivec2>& position) const
{
  ivec2& pos = position.value;

  pos = this->rectangle().clamp(pos);
}


// ====


MousePositionConstraint::RectangleConstraint::RectangleConstraint(const Rectangle<ivec2>& rectangle, bool wraparound)
  : BaseRectangleConstraint(rectangle, wraparound)
{
}


MousePositionConstraint::RectangleConstraint::Ptr MousePositionConstraint::RectangleConstraint::create(const Rectangle<ivec2>& rectangle, bool wraparound)
{
  return Ptr(new RectangleConstraint(rectangle, wraparound));
}


void MousePositionConstraint::RectangleConstraint::setRectangle(const Rectangle<ivec2>& rectangle)
{
  BaseRectangleConstraint::_setRectangle(rectangle);
}


// ====


MousePositionConstraint::FullscreenRectangleConstraint::FullscreenRectangleConstraint(bool wraparound)
  : BaseRectangleConstraint(calcFullscreenRectangle(), wraparound)
{
  RenderWindow::signalWindowResized().connect([this](){_setRectangle(calcFullscreenRectangle());}).track(this->trackable);
}


MousePositionConstraint::FullscreenRectangleConstraint::Ptr MousePositionConstraint::FullscreenRectangleConstraint::create(bool wraparound)
{
  return Ptr(new FullscreenRectangleConstraint(wraparound));
}


Rectangle<ivec2> MousePositionConstraint::FullscreenRectangleConstraint::calcFullscreenRectangle()
{
  return Rectangle<ivec2>(ivec2(0), RenderWindow::size());
}


// ====


MousePositionConstraint::NoConstraint::NoConstraint()
{
}


MousePositionConstraint::NoConstraint::Ptr MousePositionConstraint::NoConstraint::create()
{
  return Ptr(new NoConstraint);
}


void MousePositionConstraint::NoConstraint::applyContraint(const InOutput<ivec2>& position, const InOutput<ivec2>& relativeMovement) const
{
  (void)position;
  (void)relativeMovement;
}


void MousePositionConstraint::NoConstraint::applyFirstTime(const InOutput<ivec2>& position) const
{
  (void)position;
}



} // namespace Framework
