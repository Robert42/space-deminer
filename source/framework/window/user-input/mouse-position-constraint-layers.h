#ifndef FRAMEWORK_MOUSEPOSITIONCONSTRAINTLAYERS_H
#define FRAMEWORK_MOUSEPOSITIONCONSTRAINTLAYERS_H

#include <framework/window/user-input/mouse.h>

#include <base/containers/layers.h>

namespace Framework {

class MousePositionConstraintLayers final : public noncopyable
{
public:
  typedef shared_ptr<MousePositionConstraintLayers> Ptr;
  typedef std::function<void(const MousePositionConstraint::Ptr&)> Setter;

  class Layer : public Containers::BaseLayers::Layer
  {
  public:
    typedef shared_ptr<Layer> Ptr;
    typedef shared_ptr<const Layer> ConstPtr;

  private:
    MousePositionConstraint::Ptr _constraint;

    Signals::CallableSignal<void()> _signalConstraintChanged;

  private:
    Layer(const MousePositionConstraint::Ptr& constraint);

  public:
    Signals::Signal<void()>& signalConstraintChanged();

    void setConstraint(const MousePositionConstraint::Ptr& constraint);
    const MousePositionConstraint::Ptr& constraint() const;

  public:
    static Ptr create(const MousePositionConstraint::Ptr& constraint);

    ~Layer();
  };

public:
  Signals::Trackable trackable;

private:
  Containers::Layers<Layer> layers;
  Setter setMousePositionConstraint;

public:
  Layer::Ptr mouseActionLayer, rotoCursorLayer, testLayer, fallbackLayer;

private:
  MousePositionConstraintLayers(const Setter& setter);

public:
  static Ptr create(const Setter& setter);

private:
  void udpateConstraint();
};

} // namespace Framework

#endif // FRAMEWORK_MOUSEPOSITIONCONSTRAINTLAYERS_H
