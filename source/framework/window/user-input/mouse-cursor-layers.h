#ifndef FRAMEWORK_MOUSECURSORLAYERS_H
#define FRAMEWORK_MOUSECURSORLAYERS_H

#include <framework/window/user-input/mouse-cursor.h>

#include <base/containers/layers.h>

#include "mouse.h"

namespace Framework {

using namespace Base;

class MouseCursorLayers final : public noncopyable
{
public:
  typedef shared_ptr<MouseCursorLayers> Ptr;
  typedef std::function<void(const MouseCursor::Ptr&)> Setter;

  class Layer : public Containers::BaseLayers::Layer
  {
  public:
    typedef shared_ptr<Layer> Ptr;
    typedef shared_ptr<const Layer> ConstPtr;

  private:
    MouseCursor::Ptr _cursor;

    Signals::CallableSignal<void(const MouseCursor::Ptr&)> _signalCursorChanged;

  public:
    void setCursor(const MouseCursor::Ptr& cursor);
    const MouseCursor::Ptr& cursor() const;

    Signals::Signal<void(const MouseCursor::Ptr&)>& signalCursorChanged();

  private:
    Layer(const MouseCursor::Ptr& cursor);

  public:
    static Ptr create(const MouseCursor::Ptr& cursor);

    ~Layer();
  };

public:
  Signals::Trackable trackable;

private:
  Containers::Layers<Layer> layers;
  Setter setMouseCursor;

public:
  Layer::Ptr suppressMouseCursor, mouseActionLayer, terminalLayer, ingameEditorLayer, codeNodeEditorLayer, tweakUILayer, ingameUILayer, unused;

private:
  MouseCursorLayers(const Setter& setter);

public:
  static Ptr create(const Setter& setter);

private:
  void udpateCursor();
};

} // namespace Framework

#endif // FRAMEWORK_MOUSECURSORLAYERS_H
