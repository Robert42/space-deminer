#include "mouse-cursor-layers.h"

#include <framework/window/user-input/mouse.h>

namespace Framework {

void MouseCursorLayers::Layer::setCursor(const MouseCursor::Ptr& cursor)
{
  if(this->cursor() != cursor)
  {
    this->_cursor = cursor;

    _signalCursorChanged(this->cursor());
  }
}


const MouseCursor::Ptr& MouseCursorLayers::Layer::cursor() const
{
  return _cursor;
}


MouseCursorLayers::Layer::Layer(const MouseCursor::Ptr& cursor)
  : Containers::BaseLayers::Layer(true),
    _cursor(cursor)
{
}


MouseCursorLayers::Layer::~Layer()
{
}


MouseCursorLayers::Layer::Ptr MouseCursorLayers::Layer::create(const MouseCursor::Ptr& cursor)
{
  return Ptr(new MouseCursorLayers::Layer(cursor));
}


Signals::Signal<void(const MouseCursor::Ptr&)>& MouseCursorLayers::Layer::signalCursorChanged()
{
  return _signalCursorChanged;
}


// ====


MouseCursorLayers::MouseCursorLayers(const Setter& setter)
  : setMouseCursor(setter)
{
  layers.pushTopLayer(unused = Layer::create(DefaultMouseCursors::defaultCursor));
  layers.pushTopLayer(ingameUILayer = Layer::create(DefaultMouseCursors::defaultCursor));
  layers.pushTopLayer(tweakUILayer = Layer::create(DefaultMouseCursors::defaultCursor));
  layers.pushTopLayer(codeNodeEditorLayer = Layer::create(DefaultMouseCursors::defaultCursor));
  layers.pushTopLayer(ingameEditorLayer = Layer::create(DefaultMouseCursors::defaultCursor));
  layers.pushTopLayer(terminalLayer = Layer::create(DefaultMouseCursors::defaultCursor));
  layers.pushTopLayer(mouseActionLayer = Layer::create(DefaultMouseCursors::defaultCursor));
  layers.pushTopLayer(suppressMouseCursor = Layer::create(DefaultMouseCursors::noCursor));

  suppressMouseCursor->setVisible(true);

  for(const Layer::Ptr& l : layers.allLayers())
  {
    l->signalCursorChanged().connect(std::bind(&MouseCursorLayers::udpateCursor, this)).track(this->trackable);
  }

  layers.signalVisibleLayersChanged.connect(std::bind(&MouseCursorLayers::udpateCursor, this)).track(this->trackable);
}


MouseCursorLayers::Ptr MouseCursorLayers::create(const Setter& setter)
{
  return Ptr(new MouseCursorLayers(setter));
}


void MouseCursorLayers::udpateCursor()
{
  if(layers.areAnyVisibleLayers())
  {
    setMouseCursor(layers.topVisibleLayer()->cursor());
  }
}


} // namespace Framework
