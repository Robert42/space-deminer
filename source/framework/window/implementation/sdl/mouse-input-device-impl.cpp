#include "./mouse-input-device-impl.h"

#include <framework/developer-tools/developer-settings.h>

namespace Framework {
namespace Private {


SdlMouse::SdlMouse(SdlRenderWindowImpl& window)
  : sdlWindow(window.sdlWindow())
{
  cursor = SdlCursorImplementation::create();
  cursor->hide();

  mouseWasMoved = false;

  addMapping(SDL_BUTTON_LEFT, MouseButton::LEFT);
  addMapping(SDL_BUTTON_MIDDLE, MouseButton::MIDDLE);
  addMapping(SDL_BUTTON_RIGHT, MouseButton::RIGHT);
  addMapping(SDL_BUTTON_X1, MouseButton::BUTTON_3);
  addMapping(SDL_BUTTON_X2, MouseButton::BUTTON_4);
}


SdlMouse::~SdlMouse()
{
}


bool SdlMouse::isSupported()
{
  return true;
}


void SdlMouse::reset()
{
  this->resetMouseState();
}


void SdlMouse::handleMouseButtonPressedEvent(SDL_MouseButtonEvent& event)
{
  if(keyMapper.contains(event.button))
    this->executePressedMouseButton(keyMapper[event.button]);
}


void SdlMouse::handleMouseButtonReleasedEvent(SDL_MouseButtonEvent& event)
{
  if(keyMapper.contains(event.button))
    this->executeReleasedMouseButton(keyMapper[event.button]);
}


void SdlMouse::handleMouseMovedEvent(SDL_MouseMotionEvent&)
{
  // SDL doesn't send one mouse movement event per frame but multiple with small deltas
  // This can disturb the relative mouse mode, as after centering the cursor still events for the previous
  // position are sent, although the mouse has been already placed at another position
  mouseWasMoved = true;
}

void SdlMouse::allSdlEventsAreHandeled()
{
  if(mouseWasMoved)
  {
    executeMouseMovement(systemMousePosition());
    mouseWasMoved = false;
  }
}


void SdlMouse::handleMouseWheelEvent(SDL_MouseWheelEvent& event)
{
  this->executeMouseWheel(event.y);
}


void SdlMouse::handleMouseEnter()
{
  this->executeEnterWindow();
}


void SdlMouse::handleMouseLeave()
{
  this->executeLeftWindow();
}


void SdlMouse::moveSystemMouseCursor(const ivec2& pos)
{
  SDL_WarpMouseInWindow(sdlWindow, pos.x, pos.y);
}


ivec2 SdlMouse::systemMousePosition() const
{
  int x, y;
  SDL_GetMouseState(&x, &y);

  return ivec2(x, y);
}


void SdlMouse::addMapping(int sdlButton, MouseButton button)
{
  keyMapper[sdlButton] = button.value;
  invKeyMapper[button.value] = sdlButton;
}


bool SdlMouse::isButtonPressedImpl(MouseButton button)const
{
  return SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(invKeyMapper[button.value]);
}


void SdlMouse::setCursorVisibility(bool show)
{
  cursor->show(show);
}


}
}
