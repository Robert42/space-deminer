#ifndef _FRAMEWORK_PRIVATE_SDL_CURSOR_H_
#define _FRAMEWORK_PRIVATE_SDL_CURSOR_H_

#include <framework/window/render-window.h>

#include <SDL.h>

namespace Framework {
namespace Private {


class SdlCursorImplementation
{
public:
  typedef std::shared_ptr<SdlCursorImplementation> Ptr;

private:
  const bool neverHideCursor;
  SDL_Cursor* defaultCursor;
  SDL_Cursor* invisibleCursor;

public:
  SdlCursorImplementation(bool neverHideCursor);
  ~SdlCursorImplementation();

  void show(bool show);
  void show();
  void hide();

  bool isVisible() const;

  static Ptr create();
};


SDL_bool sdlBool(bool booleanValue);


}
}

#endif
