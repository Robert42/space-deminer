#include <framework/window/render-window.h>
#include <framework/developer-tools/developer-settings.h>
#include <framework/application.h>

#include "./render-window-impl.h"
#include "./user-input-impl.h"
#include "./sdl-error.h"

#include <SDL_syswm.h>

namespace Framework {
namespace Private {


SDL_bool sdlBool(bool booleanValue);


const bool USE_EXTERNAL_GL_CONTEXT = false;

unique_ptr<SdlRenderWindowImpl::OpenGlContext> createSdlGlContextManager();
unique_ptr<SdlRenderWindowImpl::OpenGlContext> createDummySdlGlContextManager();

std::string getExternalWindowHandle(SDL_Window* sdlWindow, Ogre::NameValuePairList& miscParameters);


SdlRenderWindowImpl::SdlRenderWindowImpl(const String& title)
{
  SDL_TRY(SDL_Init(SDL_INIT_VIDEO));

  _ogreRenderWindow = nullptr;
  _sdlWindow = nullptr;
  windowIsOpen = true;

  if(USE_EXTERNAL_GL_CONTEXT)
    openGlContext = createSdlGlContextManager();
  else
    openGlContext = createDummySdlGlContextManager();

  _sdlWindow = SDL_CreateWindow(title.toUtf8String().c_str(),
                                SDL_WINDOWPOS_CENTERED,
                                SDL_WINDOWPOS_CENTERED,
                                defaultWidth(),
                                defaultHeight(),
                                SDL_WINDOW_HIDDEN|SDL_WINDOW_RESIZABLE|openGlContext->sdlWindowFlags());

  if(sdlWindow() == NULL)
    throw Ogre::InvalidStateException(0,
                                      Ogre::String("Couldn't create SDL Window: ") + SDL_GetError(),
                                      "createOgreRenderWindow",
                                      __FILE__,
                                      __LINE__);

  openGlContext->createContext(sdlWindow());

  signalWindowResized().connect(std::bind(&SdlRenderWindowImpl::resizeOgreWindow, this)).track(this->trackable);
}


SdlRenderWindowImpl::~SdlRenderWindowImpl()
{
  sdlUserInput.reset();

  openGlContext.reset();

  if(sdlWindow() != NULL)
    SDL_DestroyWindow(sdlWindow());

  SDL_Quit();
}


bool SdlRenderWindowImpl::isClosedImpl()
{
  return !windowIsOpen;
}


void SdlRenderWindowImpl::createOgreRenderWindowImpl()
{
  const DeveloperSettings& developerSettings = Application::developerSettings();

  Ogre::Root::getSingleton().initialise(false);

  Ogre::NameValuePairList miscParameters;

  openGlContext->fillMiscParams(out(miscParameters));
  miscParameters["externalWindowHandle"] = getExternalWindowHandle(sdlWindow(), miscParameters);

  _ogreRenderWindow = Ogre::Root::getSingleton().createRenderWindow("RenderWindow", defaultWidth(), defaultHeight(), false, &miscParameters);
  _ogreRenderWindow->setVisible(true);

  SDL_SetWindowGrab(sdlWindow(), sdlBool(developerSettings.input.grabMouse));

  sdlUserInput.reset(new SdlUserInput(*this));
}


void SdlRenderWindowImpl::close()
{
  windowIsOpen = false;
}


void SdlRenderWindowImpl::run(Application& application)
{
  try
  {
    application.startup();

    SDL_ShowWindow(sdlWindow());

    while(!this->isClosedImpl())
    {
      sdlUserInput->reset();

      handleSdlEvents();

      application.renderOneFrame();

      SDL_GL_SwapWindow(sdlWindow());
    }

  }catch(...)
  {
    if(sdlWindow() != nullptr)
    {
      SDL_SetWindowGrab(sdlWindow(), SDL_FALSE);
      SDL_HideWindow(sdlWindow());
    }
    throw;
  }
}


UserInput& SdlRenderWindowImpl::userInputImpl()
{
  return *sdlUserInput;
}


void SdlRenderWindowImpl::handleSdlEvents()
{
  SDL_Event event;

  while(SDL_PollEvent(&event))
  {
    switch(event.type)
    {
    case SDL_KEYDOWN:
      this->sdlUserInput->handleKeyPressedEvent(event.key);
      break;
    case SDL_KEYUP:
      this->sdlUserInput->handleKeyReleasedEvent(event.key);
      break;
    case SDL_MOUSEBUTTONDOWN:
      this->sdlUserInput->handleMouseButtonPressedEvent(event.button);
      break;
    case SDL_MOUSEBUTTONUP:
      this->sdlUserInput->handleMouseButtonReleasedEvent(event.button);
      break;
    case SDL_MOUSEMOTION:
      this->sdlUserInput->handleMouseMovedEvent(event.motion);
      break;
    case SDL_MOUSEWHEEL:
      this->sdlUserInput->handleMouseWheelEvent(event.wheel);
      break;
    case SDL_TEXTINPUT:
      this->sdlUserInput->handleTextEvent(event.text);
      break;
    case SDL_WINDOWEVENT:
      handleSdlWindowEvent(event.window);
      break;
    }
  }

  this->sdlUserInput->allSdlEventsAreHandeled();
}


void SdlRenderWindowImpl::handleSdlWindowEvent(SDL_WindowEvent& event)
{
  switch(event.event)
  {
  case SDL_WINDOWEVENT_CLOSE:
    close();
    break;
  case SDL_WINDOWEVENT_RESIZED:
  case SDL_WINDOWEVENT_SIZE_CHANGED:
    windowSizeChanged(event.data1, event.data2);
    break;
  case SDL_WINDOWEVENT_MAXIMIZED:
    windowSizeChanged(event.data1, event.data2);
    break;
  case SDL_WINDOWEVENT_MINIMIZED:
    windowSizeChanged(event.data1, event.data2);
    break;
  case SDL_WINDOWEVENT_ENTER:
    this->sdlUserInput->handleMouseEnter();
    break;
  case SDL_WINDOWEVENT_LEAVE:
    this->sdlUserInput->handleMouseLeave();
    break;
  case SDL_WINDOWEVENT_FOCUS_LOST:
    sendSignalFocusChanged(false);
    break;
  case SDL_WINDOWEVENT_FOCUS_GAINED:
    sendSignalFocusChanged(true);
    break;
  }
}


void SdlRenderWindowImpl::resizeOgreWindow()
{
  ivec2 size = this->sizeImpl();

  ogreRenderWindow().resize(size.x, size.y);
}


void SdlRenderWindowImpl::windowSizeChanged(int w, int h)
{
  if(window_width==w && window_height==h)
    return;

  window_width = w;
  window_height = h;

  sendSignalWindowResized();
}


ivec2 SdlRenderWindowImpl::sizeImpl() const
{
  ivec2 size;

  SDL_GetWindowSize(sdlWindow(), &size.x, &size.y);

  return size;
}


bool SdlRenderWindowImpl::hasClipboardValueImpl() const
{
  return SDL_HasClipboardText() != SDL_FALSE;
}


String SdlRenderWindowImpl::clipboardValueImpl() const
{
  return String::fromUtf8(SDL_GetClipboardText());
}


void SdlRenderWindowImpl::setClipboardValueImpl(const String& value) const
{
  SDL_SetClipboardText(value.toUtf8String().c_str());
}


RenderWindow* createRenderWindowImpl(const String& title)
{
  return new SdlRenderWindowImpl(title);
}

std::string getOSSpecificWindowHandle(const SDL_SysWMinfo& windowInfo, Ogre::NameValuePairList& miscParameters);
std::string getExternalWindowHandle(SDL_Window* sdlWindow, Ogre::NameValuePairList& miscParameters)
{
  SDL_SysWMinfo windowInfo;
  SDL_VERSION(&windowInfo.version);
  if(!SDL_GetWindowWMInfo(sdlWindow, &windowInfo))
    throw Ogre::InvalidStateException(0,
                                      Ogre::String("Couldn't acquire window information of the SDL Window: ") + SDL_GetError(),
                                      "getExternalWindowHandle",
                                      __FILE__,
                                      __LINE__);


  return getOSSpecificWindowHandle(windowInfo, miscParameters);
}

}
}
