#ifndef _FRAMEWORK_PRIVATE_SDL_KEYBOARD_INPUT_H_
#define _FRAMEWORK_PRIVATE_SDL_KEYBOARD_INPUT_H_

#include "./render-window-impl.h"

#include <base/enum-mapper.h>

#include <SDL_scancode.h>


namespace Framework {
namespace Private {

class SdlKeyboard : public Keyboard
{
public:
  EnumMapper<SDL_Scancode, KeyCode::Value> keyMapper;
  EnumMapper<KeyCode::Value, SDL_Scancode> inverseKeyMapper;

private:
  KeyModifier _modifierState;

public:
  SdlKeyboard();
  ~SdlKeyboard();

  bool isSupported() final override;

  bool isKeyDownImpl(KeyCode keyCode) const final override;

  KeyModifier modifierStateImpl() final override;

  void handleKeyPressedEvent(SDL_KeyboardEvent& event);
  void handleKeyReleasedEvent(SDL_KeyboardEvent& event);
  void handleTextEvent(SDL_TextInputEvent& event);

  void reset();

private:
  void mapKeys(SDL_Scancode sdlKeyCode, const KeyCode& keyCode);

  void captureModifierState();
};


}
}

#endif
