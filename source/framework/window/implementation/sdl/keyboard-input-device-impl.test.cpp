#include "./keyboard-input-device-impl.h"
#include <SDL_keycode.h>

namespace Framework {
namespace Private {


TEST(framework_SdlKeyboard, are_same_keycodes)
{
  EXPECT_EQ(static_cast<uint>(KMOD_NONE), static_cast<uint>(KeyModifier::NONE));

  EXPECT_EQ(static_cast<uint>(KMOD_LSHIFT), static_cast<uint>(KeyModifier::SHIFT_LEFT));
  EXPECT_EQ(static_cast<uint>(KMOD_RSHIFT), static_cast<uint>(KeyModifier::SHIFT_RIGHT));
  EXPECT_EQ(static_cast<uint>(KMOD_SHIFT), static_cast<uint>(KeyModifier::SHIFT));

  EXPECT_EQ(static_cast<uint>(KMOD_LCTRL), static_cast<uint>(KeyModifier::CONTROL_LEFT));
  EXPECT_EQ(static_cast<uint>(KMOD_RCTRL), static_cast<uint>(KeyModifier::CONTROL_RIGHT));
  EXPECT_EQ(static_cast<uint>(KMOD_CTRL), static_cast<uint>(KeyModifier::CONTROL));

  EXPECT_EQ(static_cast<uint>(KMOD_LALT), static_cast<uint>(KeyModifier::ALT_LEFT));
  EXPECT_EQ(static_cast<uint>(KMOD_RALT), static_cast<uint>(KeyModifier::ALT_RIGHT));
  EXPECT_EQ(static_cast<uint>(KMOD_ALT), static_cast<uint>(KeyModifier::ALT));

  EXPECT_EQ(static_cast<uint>(KMOD_LGUI), static_cast<uint>(KeyModifier::GUI_LEFT));
  EXPECT_EQ(static_cast<uint>(KMOD_RGUI), static_cast<uint>(KeyModifier::GUI_RIGHT));
  EXPECT_EQ(static_cast<uint>(KMOD_GUI), static_cast<uint>(KeyModifier::GUI));

  EXPECT_EQ(static_cast<uint>(KMOD_NUM), static_cast<uint>(KeyModifier::NUMLOCK));
  EXPECT_EQ(static_cast<uint>(KMOD_CAPS), static_cast<uint>(KeyModifier::CAPSLOCK));
}


}
}
