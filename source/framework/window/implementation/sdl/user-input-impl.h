#ifndef _FRAMEWORK_PRIVATE_SDL_USER_INPUT_H_
#define _FRAMEWORK_PRIVATE_SDL_USER_INPUT_H_

#include "./render-window-impl.h"

namespace Framework {
namespace Private {

class SdlKeyboard;
class SdlMouse;

class SdlUserInput : public UserInput
{
private:
  shared_ptr<SdlKeyboard> _keyboard;
  shared_ptr<SdlMouse> _mouse;

public:
  SdlUserInput(SdlRenderWindowImpl& sdlWindow);
  ~SdlUserInput();

public:
  Mouse& mouse() override;
  Keyboard& keyboard() override;

  void handleKeyPressedEvent(SDL_KeyboardEvent& event);
  void handleKeyReleasedEvent(SDL_KeyboardEvent& event);
  void handleMouseButtonPressedEvent(SDL_MouseButtonEvent& event);
  void handleMouseButtonReleasedEvent(SDL_MouseButtonEvent& event);
  void handleMouseMovedEvent(SDL_MouseMotionEvent& event);
  void handleMouseWheelEvent(SDL_MouseWheelEvent& event);
  void handleTextEvent(SDL_TextInputEvent& event);
  void handleMouseEnter();
  void handleMouseLeave();

  void allSdlEventsAreHandeled();

  void reset();

private:
  SdlMouse* createMouse(SdlRenderWindowImpl& sdlWindow);
  SdlKeyboard* createKeyboard();
};


}
}

#endif
