#include "./keyboard-input-device-impl.h"
#include <SDL_keycode.h>

namespace Framework {
namespace Private {


SdlKeyboard::SdlKeyboard()
{
  _modifierState = KeyModifier::NONE;

  mapKeys(SDL_SCANCODE_GRAVE, KeyCode::OPENTERMINAL);
  mapKeys(SDL_SCANCODE_ESCAPE, KeyCode::ESCAPE);
  mapKeys(SDL_SCANCODE_SPACE, KeyCode::SPACE);
  mapKeys(SDL_SCANCODE_A, KeyCode::A);
  mapKeys(SDL_SCANCODE_B, KeyCode::B);
  mapKeys(SDL_SCANCODE_C, KeyCode::C);
  mapKeys(SDL_SCANCODE_D, KeyCode::D);
  mapKeys(SDL_SCANCODE_E, KeyCode::E);
  mapKeys(SDL_SCANCODE_F, KeyCode::F);
  mapKeys(SDL_SCANCODE_G, KeyCode::G);
  mapKeys(SDL_SCANCODE_H, KeyCode::H);
  mapKeys(SDL_SCANCODE_I, KeyCode::I);
  mapKeys(SDL_SCANCODE_J, KeyCode::J);
  mapKeys(SDL_SCANCODE_K, KeyCode::K);
  mapKeys(SDL_SCANCODE_L, KeyCode::L);
  mapKeys(SDL_SCANCODE_M, KeyCode::M);
  mapKeys(SDL_SCANCODE_N, KeyCode::N);
  mapKeys(SDL_SCANCODE_O, KeyCode::O);
  mapKeys(SDL_SCANCODE_P, KeyCode::P);
  mapKeys(SDL_SCANCODE_Q, KeyCode::Q);
  mapKeys(SDL_SCANCODE_R, KeyCode::R);
  mapKeys(SDL_SCANCODE_S, KeyCode::S);
  mapKeys(SDL_SCANCODE_T, KeyCode::T);
  mapKeys(SDL_SCANCODE_U, KeyCode::U);
  mapKeys(SDL_SCANCODE_V, KeyCode::V);
  mapKeys(SDL_SCANCODE_W, KeyCode::W);
  mapKeys(SDL_SCANCODE_X, KeyCode::X);
  mapKeys(SDL_SCANCODE_Y, KeyCode::Y);
  mapKeys(SDL_SCANCODE_Z, KeyCode::Z);
  mapKeys(SDL_SCANCODE_0, KeyCode::KEYPAD_0);
  mapKeys(SDL_SCANCODE_1, KeyCode::KEYPAD_1);
  mapKeys(SDL_SCANCODE_2, KeyCode::KEYPAD_2);
  mapKeys(SDL_SCANCODE_3, KeyCode::KEYPAD_3);
  mapKeys(SDL_SCANCODE_4, KeyCode::KEYPAD_4);
  mapKeys(SDL_SCANCODE_5, KeyCode::KEYPAD_5);
  mapKeys(SDL_SCANCODE_6, KeyCode::KEYPAD_6);
  mapKeys(SDL_SCANCODE_7, KeyCode::KEYPAD_7);
  mapKeys(SDL_SCANCODE_8, KeyCode::KEYPAD_8);
  mapKeys(SDL_SCANCODE_9, KeyCode::KEYPAD_9);
  mapKeys(SDL_SCANCODE_KP_0, KeyCode::NUMPAD_0);
  mapKeys(SDL_SCANCODE_KP_1, KeyCode::NUMPAD_1);
  mapKeys(SDL_SCANCODE_KP_2, KeyCode::NUMPAD_2);
  mapKeys(SDL_SCANCODE_KP_3, KeyCode::NUMPAD_3);
  mapKeys(SDL_SCANCODE_KP_4, KeyCode::NUMPAD_4);
  mapKeys(SDL_SCANCODE_KP_5, KeyCode::NUMPAD_5);
  mapKeys(SDL_SCANCODE_KP_6, KeyCode::NUMPAD_6);
  mapKeys(SDL_SCANCODE_KP_7, KeyCode::NUMPAD_7);
  mapKeys(SDL_SCANCODE_KP_8, KeyCode::NUMPAD_8);
  mapKeys(SDL_SCANCODE_KP_9, KeyCode::NUMPAD_9);
  mapKeys(SDL_SCANCODE_KP_PLUS, KeyCode::NUMPAD_PLUS);
  mapKeys(SDL_SCANCODE_KP_MINUS, KeyCode::NUMPAD_MINUS);
  mapKeys(SDL_SCANCODE_KP_ENTER, KeyCode::NUMPAD_ENTER);
  mapKeys(SDL_SCANCODE_F1, KeyCode::F1);
  mapKeys(SDL_SCANCODE_F2, KeyCode::F2);
  mapKeys(SDL_SCANCODE_F3, KeyCode::F3);
  mapKeys(SDL_SCANCODE_F4, KeyCode::F4);
  mapKeys(SDL_SCANCODE_F5, KeyCode::F5);
  mapKeys(SDL_SCANCODE_F6, KeyCode::F6);
  mapKeys(SDL_SCANCODE_F7, KeyCode::F7);
  mapKeys(SDL_SCANCODE_F8, KeyCode::F8);
  mapKeys(SDL_SCANCODE_F9, KeyCode::F9);
  mapKeys(SDL_SCANCODE_F10, KeyCode::F10);
  mapKeys(SDL_SCANCODE_F11, KeyCode::F11);
  mapKeys(SDL_SCANCODE_F12, KeyCode::F12);
  mapKeys(SDL_SCANCODE_PAUSE, KeyCode::PAUSE);
  mapKeys(SDL_SCANCODE_PAGEDOWN, KeyCode::PAGEDOWN);
  mapKeys(SDL_SCANCODE_PAGEUP, KeyCode::PAGEUP);
  mapKeys(SDL_SCANCODE_RETURN, KeyCode::RETURN);
  mapKeys(SDL_SCANCODE_LALT, KeyCode::ALT_LEFT);
  mapKeys(SDL_SCANCODE_RALT, KeyCode::ALT_RIGHT);
  mapKeys(SDL_SCANCODE_LSHIFT, KeyCode::SHIFT_LEFT);
  mapKeys(SDL_SCANCODE_RSHIFT, KeyCode::SHIFT_RIGHT);
  mapKeys(SDL_SCANCODE_LCTRL, KeyCode::CONTROL_LEFT);
  mapKeys(SDL_SCANCODE_RCTRL, KeyCode::CONTROL_RIGHT);
  mapKeys(SDL_SCANCODE_BACKSPACE, KeyCode::BACKSPACE);
  mapKeys(SDL_SCANCODE_TAB, KeyCode::TAB);
  mapKeys(SDL_SCANCODE_HOME, KeyCode::HOME);
  mapKeys(SDL_SCANCODE_END, KeyCode::END);
  mapKeys(SDL_SCANCODE_DELETE, KeyCode::DELETE);
  mapKeys(SDL_SCANCODE_INSERT, KeyCode::INSERT);
  mapKeys(SDL_SCANCODE_LEFT, KeyCode::ARROW_LEFT);
  mapKeys(SDL_SCANCODE_RIGHT, KeyCode::ARROW_RIGHT);
  mapKeys(SDL_SCANCODE_UP, KeyCode::ARROW_UP);
  mapKeys(SDL_SCANCODE_DOWN, KeyCode::ARROW_DOWN);

  keyMapper[SDL_SCANCODE_KP_ENTER] = KeyCode::RETURN;
}


SdlKeyboard::~SdlKeyboard()
{
}


bool SdlKeyboard::isSupported()
{
  return true;
}

inline bool getSdlKeyboadsState(SDL_Scancode sdlScancode)
{
  return 0 != SDL_GetKeyboardState(0)[sdlScancode];
}


bool SdlKeyboard::isKeyDownImpl(KeyCode keyCode) const
{
  switch(keyCode.value)
  {
  case KeyCode::RETURN:
    return getSdlKeyboadsState(SDL_SCANCODE_RETURN) || getSdlKeyboadsState(SDL_SCANCODE_KP_ENTER);
  default:
    return getSdlKeyboadsState(inverseKeyMapper[keyCode.value]);
  }
}


void SdlKeyboard::reset()
{
  captureModifierState();
}


KeyModifier SdlKeyboard::modifierStateImpl()
{
  return _modifierState;
}

void SdlKeyboard::captureModifierState()
{
  _modifierState = static_cast<KeyModifier::Value>(SDL_GetModState()) & KeyModifier::ALL;
}


void SdlKeyboard::mapKeys(SDL_Scancode sdlKeyCode, const KeyCode& keyCode)
{
  keyMapper[sdlKeyCode] = keyCode.value;
  inverseKeyMapper[keyCode.value] = sdlKeyCode;
}


void SdlKeyboard::handleKeyPressedEvent(SDL_KeyboardEvent& event)
{
  sendSignalActionBegin(keyMapper[event.keysym.scancode]);
}


void SdlKeyboard::handleKeyReleasedEvent(SDL_KeyboardEvent& event)
{
  sendSignalActionEnd(keyMapper[event.keysym.scancode]);
}


void SdlKeyboard::handleTextEvent(SDL_TextInputEvent& event)
{
  for(String::value_type unicode : String::fromUtf8(event.text))
    sendSignalSignalUnicodeEntered(unicode);
}


}
}
