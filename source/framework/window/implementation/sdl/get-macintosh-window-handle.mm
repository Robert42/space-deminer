#include <dependencies.h>
#include <SDL_syswm.h>

namespace Framework
{
  namespace Private
  {
    
    const bool USE_NSVIEW = true;

    std::string getOSSpecificWindowHandle(const SDL_SysWMinfo& windowInfo, Ogre::NameValuePairList &miscParameters)
    {
      if(windowInfo.subsystem != SDL_SYSWM_COCOA)
        throw Ogre::InvalidStateException(0,
                                          Ogre::String("Unexpeced SDL Window subsystem: ") + std::to_string(windowInfo.subsystem),
                                          "getOSSpecificWindowHandle",
                                          __FILE__,
                                          __LINE__);
      
      NSWindow *window = windowInfo.info.cocoa.window;
      size_t handle;
      
      miscParameters["macAPI"] = "cocoa";
      
      if(USE_NSVIEW)
      {
        NSView *view = [window contentView];
        
        miscParameters["macAPICocoaUseNSView"] = "true";
        handle = reinterpret_cast<size_t>(view);
      }else
      {
        miscParameters["macAPICocoaUseNSView"] = "false";
        handle = reinterpret_cast<size_t>(window);
      }
      
      return Ogre::StringConverter::toString(handle, 0);
    }

  }
}
