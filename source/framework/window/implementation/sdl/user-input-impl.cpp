#include "./user-input-impl.h"
#include "./mouse-input-device-impl.h"
#include "./keyboard-input-device-impl.h"

namespace Framework {
namespace Private {


SdlUserInput::SdlUserInput(SdlRenderWindowImpl& sdlWindow)
{
  this->_keyboard.reset(createKeyboard());
  this->_mouse.reset(createMouse(sdlWindow));
}


SdlUserInput::~SdlUserInput()
{
  SDL_Quit();
}


Mouse& SdlUserInput::mouse()
{
  return *this->_mouse;
}


Keyboard& SdlUserInput::keyboard()
{
  return *this->_keyboard;
}


void SdlUserInput::handleKeyPressedEvent(SDL_KeyboardEvent& event)
{
  this->_keyboard->handleKeyPressedEvent(event);
}


void SdlUserInput::handleKeyReleasedEvent(SDL_KeyboardEvent& event)
{
  this->_keyboard->handleKeyReleasedEvent(event);
}


void SdlUserInput::handleMouseButtonPressedEvent(SDL_MouseButtonEvent& event)
{
  this->_mouse->handleMouseButtonPressedEvent(event);
}


void SdlUserInput::handleMouseButtonReleasedEvent(SDL_MouseButtonEvent& event)
{
  this->_mouse->handleMouseButtonReleasedEvent(event);
}


void SdlUserInput::handleMouseMovedEvent(SDL_MouseMotionEvent& event)
{
  this->_mouse->handleMouseMovedEvent(event);
}


void SdlUserInput::handleMouseWheelEvent(SDL_MouseWheelEvent& event)
{
  this->_mouse->handleMouseWheelEvent(event);
}


void SdlUserInput::handleTextEvent(SDL_TextInputEvent& event)
{
  this->_keyboard->handleTextEvent(event);
}


void SdlUserInput::handleMouseEnter()
{
  this->_mouse->handleMouseEnter();
}


void SdlUserInput::handleMouseLeave()
{
  this->_mouse->handleMouseLeave();
}


void SdlUserInput::allSdlEventsAreHandeled()
{
  this->_mouse->allSdlEventsAreHandeled();
}


void SdlUserInput::reset()
{
  this->_mouse->reset();
  this->_keyboard->reset();
}


SdlMouse* SdlUserInput::createMouse(SdlRenderWindowImpl& sdlWindow)
{
  return new SdlMouse(sdlWindow);
}


SdlKeyboard* SdlUserInput::createKeyboard()
{
  return new SdlKeyboard();
}


}
}
