#include "./render-window-impl.h"

namespace Framework {
namespace Private {

class SdlGlContextManager : public SdlRenderWindowImpl::OpenGlContext
{
private:
  SDL_GLContext sdlGlContext;

  ~SdlGlContextManager()
  {
    if(sdlGlContext != 0)
      SDL_GL_DeleteContext(sdlGlContext);
  }

  Uint32 sdlWindowFlags() final override
  {
    return SDL_WINDOW_OPENGL;
  }

  void createContext(SDL_Window* sdlWindow) final override
  {
    sdlGlContext = SDL_GL_CreateContext(sdlWindow);

    if(sdlGlContext == NULL)
      throw Ogre::InvalidStateException(0,
                                        Ogre::String("Couldn't create SDL OpenGL-Context: ") + SDL_GetError(),
                                        "createOgreRenderWindow",
                                        __FILE__,
                                        __LINE__);
  }

  void fillMiscParams(const Output<Ogre::NameValuePairList>& miscParameters) final override
  {
    miscParameters.value["currentGLContext"] = "True";
    miscParameters.value["externalGLContext"] = "True";
  }
};


unique_ptr<SdlRenderWindowImpl::OpenGlContext> createSdlGlContextManager()
{
  return unique_ptr<SdlRenderWindowImpl::OpenGlContext>(new SdlGlContextManager);
}


}
}
