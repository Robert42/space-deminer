#include "cursor.h"
#include "./sdl-error.h"

#include <framework/application.h>
#include <framework/developer-tools/developer-settings.h>

namespace Framework {
namespace Private {


SdlCursorImplementation::SdlCursorImplementation(bool neverHideCursor)
  : neverHideCursor(neverHideCursor)
{
  defaultCursor = SDL_GetCursor();

  uint8 data = 0;
  invisibleCursor = SDL_CreateCursor(&data, &data, 1, 1, 0, 0);
}


SdlCursorImplementation::~SdlCursorImplementation()
{
  SDL_FreeCursor(defaultCursor);
  SDL_FreeCursor(invisibleCursor);
}


void SdlCursorImplementation::show(bool show)
{
  if(neverHideCursor)
    return;

  SDL_TRY(SDL_ShowCursor(sdlBool(show)));
  if(show)
  {
    SDL_SetCursor(defaultCursor);
  }else
  {
    SDL_SetCursor(invisibleCursor);
  }
}


void SdlCursorImplementation::show()
{
  show(true);
}


void SdlCursorImplementation::hide()
{
  show(false);
}


bool SdlCursorImplementation::isVisible() const
{
  return SDL_ShowCursor(-1);
}


SdlCursorImplementation::Ptr SdlCursorImplementation::create()
{
  bool neverHideCursor = !Application::developerSettings().input.hideMouseCursor;

  return Ptr(new SdlCursorImplementation(neverHideCursor));
}


SDL_bool sdlBool(bool booleanValue)
{
  if(booleanValue)
    return SDL_TRUE;
  else
    return SDL_FALSE;
}


}
}
