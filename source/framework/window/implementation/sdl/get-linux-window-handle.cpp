#include <dependencies.h>
#include <SDL2/SDL_syswm.h>

namespace Framework {
namespace Private {

std::string getOSSpecificWindowHandle(const SDL_SysWMinfo& windowInfo, Ogre::NameValuePairList& miscParameters)
{
  (void)miscParameters;

  if(windowInfo.subsystem != SDL_SYSWM_X11)
    throw Ogre::InvalidStateException(0,
                                      Ogre::String("Unexpeced SDL Window subsystem: ") + std::to_string(windowInfo.subsystem),
                                      "getOSSpecificWindowHandle",
                                      __FILE__,
                                      __LINE__);

  unsigned long windowHandle = static_cast<unsigned long>(windowInfo.info.x11.window);

  return std::to_string(windowHandle);
}

}
}
