#include "./render-window-impl.h"

namespace Framework {
namespace Private {


class DummySdlGlContextManager : public SdlRenderWindowImpl::OpenGlContext
{
private:
  ~DummySdlGlContextManager()
  {
  }

  Uint32 sdlWindowFlags() final override
  {
    return 0;
  }

  void createContext(SDL_Window*) final override
  {
  }

  void fillMiscParams(const Output<Ogre::NameValuePairList>& miscParameters) final override
  {
    miscParameters.value["currentGLContext"] = "False";
    miscParameters.value["externalGLContext"] = "False";
  }
};


unique_ptr<SdlRenderWindowImpl::OpenGlContext> createDummySdlGlContextManager()
{
  return unique_ptr<SdlRenderWindowImpl::OpenGlContext>(new DummySdlGlContextManager);
}


}
}
