#ifndef _FRAMEWORK_PRIVATE_SDL_MOUSE_INPUT_H_
#define _FRAMEWORK_PRIVATE_SDL_MOUSE_INPUT_H_

#include <dependencies.h>

#include "./render-window-impl.h"

#include <framework/private/mouse-state-machine.h>

#include <base/enum-mapper.h>

#include <SDL_mouse.h>

#include "cursor.h"

namespace Framework {
namespace Private {

class SdlMouse : public MouseStateMachine
{
public:
  EnumMapper<int, MouseButton::Value> keyMapper;
  EnumMapper<MouseButton::Value, int> invKeyMapper;

  SDL_Window* const sdlWindow;

  SdlCursorImplementation::Ptr cursor;

  bool mouseWasMoved;

  SdlMouse(SdlRenderWindowImpl& sdlWindow);
  ~SdlMouse();

  void handleMouseButtonPressedEvent(SDL_MouseButtonEvent& event);
  void handleMouseButtonReleasedEvent(SDL_MouseButtonEvent& event);
  void handleMouseMovedEvent(SDL_MouseMotionEvent& event);
  void handleMouseWheelEvent(SDL_MouseWheelEvent& event);
  void handleMouseEnter();
  void handleMouseLeave();

  void allSdlEventsAreHandeled();

  void reset();

  void setCursorVisibility(bool show) override;

private:
  bool isSupported() final override;

  void addMapping(int sdlButton, MouseButton button);

  void moveSystemMouseCursor(const ivec2& pos) final override;
  ivec2 systemMousePosition() const final override;

  bool isButtonPressedImpl(MouseButton button)const final override;
};


}
}

#endif
