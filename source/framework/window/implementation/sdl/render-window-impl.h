#ifndef _FRAMEWORK_PRIVATE_SDL_RENDER_WINDOW_H_
#define _FRAMEWORK_PRIVATE_SDL_RENDER_WINDOW_H_

#include <base/output.h>
#include <framework/window/render-window.h>

#include <SDL.h>

namespace Framework {
namespace Private {

class SdlUserInput;

class SdlRenderWindowImpl : public RenderWindow
{
public:
  class OpenGlContext : public noncopyable
  {
  public:
    virtual Uint32 sdlWindowFlags() = 0;
    virtual void createContext(SDL_Window* sdlWindow) = 0;
    virtual void fillMiscParams(const Output<Ogre::NameValuePairList>& miscParameters) = 0; // Fixme: use InOutput
    virtual ~OpenGlContext(){}
  };

public:
  Signals::Trackable trackable;

private:
  SDL_Window* _sdlWindow;
  unique_ptr<OpenGlContext> openGlContext;
  bool windowIsOpen;

  int window_width, window_height;

  shared_ptr<SdlUserInput> sdlUserInput;

public:
  SdlRenderWindowImpl(const String& title);
  ~SdlRenderWindowImpl();

public:
  Ogre::RenderWindow& ogreRenderWindow()
  {
    return *this->_ogreRenderWindow;
  }

  SDL_Window* sdlWindow() const
  {
    return _sdlWindow;
  }

private:
  bool isClosedImpl() override;

  void createOgreRenderWindowImpl() override;
  UserInput& userInputImpl() override;

  void run(Application& application) override;
  void close() override;

  ivec2 sizeImpl() const override;

  bool hasClipboardValueImpl() const override;
  String clipboardValueImpl() const override;
  void setClipboardValueImpl(const String& value) const override;

  void windowSizeChanged(int w, int h);

  void handleSdlEvents();
  void handleSdlWindowEvent(SDL_WindowEvent& event);

  void resizeOgreWindow();
};


}
}

#endif
