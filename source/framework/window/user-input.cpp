#include <dependencies.h>
#include <framework/window/user-input.h>

namespace Framework {


UserInput::UserInput()
{
}


UserInput::~UserInput()
{
}


shared_ptr<UserInput::SignalBlocker> UserInput::createSignalBlocker(InputDevice::ActionPriority minPassPriority, bool blockMouseSpeed)
{
  return shared_ptr<SignalBlocker>(new SignalBlocker(minPassPriority, blockMouseSpeed));
}


Signals::Signal<void(InputDevice::ActionPriority)>& UserInput::signalBlockerAdded()
{
  return singleton()._signalBlockerAdded;
}


Signals::Signal<void(InputDevice::ActionPriority)>& UserInput::signalBlockerRemoved()
{
  return singleton()._signalBlockerRemoved;
}

void UserInput::appendInjectedInput(const std::function<void()>& injectedInput)
{
  UserInput& userInput = singleton();

  if(userInput.injectedInputQueue.empty())
    FrameSignals::callOnce(std::bind(&UserInput::injectNextInput, &userInput));

  userInput.injectedInputQueue.append(injectedInput);
}

void UserInput::injectNextInput()
{
  assert(!injectedInputQueue.empty());

  injectedInputQueue.first()();
  injectedInputQueue.pop_front();

  if(!injectedInputQueue.empty())
    FrameSignals::callOnce(std::bind(&UserInput::injectNextInput, this));
}


UserInput::SignalBlocker::SignalBlocker(InputDevice::ActionPriority minPassPriority, bool blockMouseSpeed)
  : minPassPriority(minPassPriority)
{
  UserInput& userInput = UserInput::singleton();

  const size_t BLOCKER_VECTOR_SIZE = 7;

  blocker.reserve(BLOCKER_VECTOR_SIZE);

  blocker.push_back(Keyboard::signalKeyPressed().block(minPassPriority));
  blocker.push_back(Keyboard::signalKeyReleased().block(minPassPriority));
  blocker.push_back(Keyboard::signalUnicodeEntered().block(minPassPriority));
  blocker.push_back(Mouse::signalButtonPressed().block(minPassPriority));
  blocker.push_back(Mouse::signalButtonReleased().block(minPassPriority));
  blocker.push_back(Mouse::signalMouseMoved().block(minPassPriority));
  blocker.push_back(Mouse::signalMouseWheelMoved().block(minPassPriority));

  if(blockMouseSpeed)
    mouseSpeedBlocker = Mouse::blockSpeed();

  assert(blocker.size() == BLOCKER_VECTOR_SIZE && "If this assertion fails, please correct the value of BLOCKER_VECTOR_SIZE");

  userInput._signalBlockerAdded(minPassPriority);
}


UserInput::SignalBlocker::~SignalBlocker()
{
  UserInput& userInput = UserInput::singleton();

  blocker.clear();
  mouseSpeedBlocker.reset();

  userInput._signalBlockerRemoved(minPassPriority);
}


}
