#ifndef _FRAMEWORK_WINDOW_USERINPUT_H_
#define _FRAMEWORK_WINDOW_USERINPUT_H_

#include <framework/window/user-input/input-device.h>
#include <framework/declarations.h>

#include <base/singleton.h>
#include <base/implicit-shared-blocker.h>

namespace Framework {


/** Class managing all aviable input devices.
 */
class UserInput : public Singleton<UserInput>
{
public:
  class SignalBlocker
  {
  public:
    typedef shared_ptr<SignalBlocker> Ptr;

  private:
    friend class UserInput;

    std::vector<Signals::CascadedSignalBlocker::Ptr> blocker;
    Blocker mouseSpeedBlocker;
    const InputDevice::ActionPriority minPassPriority;

    SignalBlocker(InputDevice::ActionPriority minPassPriority, bool blockMouseSpeed);

  public:
    ~SignalBlocker();
  };

private:
  Signals::CallableSignal<void(InputDevice::ActionPriority)> _signalBlockerAdded;
  Signals::CallableSignal<void(InputDevice::ActionPriority)> _signalBlockerRemoved;

  QList< std::function<void()> > injectedInputQueue;

public:
  UserInput();

  virtual Keyboard& keyboard() = 0;
  virtual Mouse& mouse() = 0;

  static SignalBlocker::Ptr createSignalBlocker(InputDevice::ActionPriority minPassPriority, bool blockMouseSpeed = true);

  static Signals::Signal<void(InputDevice::ActionPriority)>& signalBlockerAdded();
  static Signals::Signal<void(InputDevice::ActionPriority)>& signalBlockerRemoved();

  static void appendInjectedInput(const std::function<void()>& injectedInput);

public:
  virtual ~UserInput();

private:
  void injectNextInput();
};


}

#include <framework/window/user-input/keyboard.h>
#include <framework/window/user-input/mouse.h>

#endif
