#ifndef _FRAMEWORK_PLUGINMANAGER_H_
#define _FRAMEWORK_PLUGINMANAGER_H_

#include <dependencies.h>
#include <base/io/directory.h>
#include <base/io/regular-file.h>

namespace Framework {
using namespace Base;


/** A small helper class for loading ogre plugins.
 *
 * It uses internal `Ogre::Root::getSingleton().loadPlugin();` using the full path of the plugin.
 */
class PluginManager
{
private:
  IO::Directory pluginDirectory;

public:
  PluginManager();

public:
  /** Loads a single plugin by it's name.
   */
  void loadRequiredPlugin(const std::string& pluginName);

  /** Tries loading a single plugin by it's name.
   *
   * If the plugin couldn't be found, no exception is thrown. However, other exception can still be thrown.
   */
  bool tryLoadPlugin(std::string pluginName);

private:
  static std::string pluginExtension();
  IO::RegularFile pluginFile(const std::string& pluginName) const;
  std::string addDebugSuffixIfReasonable(const std::string& pluginName) const;
};


}


#endif
