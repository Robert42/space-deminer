#ifndef _FRAMEWORK_APPLICATION_H_
#define _FRAMEWORK_APPLICATION_H_

#include "declarations.h"

#include <framework/window.h>
#include <framework/frame-signals.h>

#include <base/base-application.h>

#include "./cegui-system.h"


namespace Framework {

namespace Gui {
class Animation;
} // namespace Gui

namespace Terminal {
class Manager;
} // namespace Terminal

class DeveloperTools;


/** The Applicaion class is the root object of every application.
 *
 * It
 * - initializes Ogre
 * - searches for the most importand directories
 */
class Application : public Base::BaseApplication, public PublicSingleton<Application>
{
private:
  std::shared_ptr<RenderWindow> _renderWindow;
  std::shared_ptr<DeveloperSettings> _developerSettings;
  std::shared_ptr<DeveloperHud> _developerHud;
  std::shared_ptr<DeveloperTools> _developerTools;
  std::shared_ptr<Terminal::Manager> terminalManager;
  std::shared_ptr<FrameTimer> frameTimer;
  std::shared_ptr<Applet> _currentApplet;
  std::shared_ptr<Gui::Animation> _frametimeAnimation;
  std::shared_ptr<CeguiSystem> _ceguiSystem;

  IO::Directory _assetsDirectory;
  FrameSignals frameSignals;

  void initializeResourceLocations();
  void loadEngineConfiguration();

  static RenderWindow& renderWindow();

protected:
  Application(const String& applicationName);
  virtual ~Application();

protected:
  /** Override this method to initialize plugins.
   *
   * This method is the best place to load plugins;
   *
   * \param pluginManager a sall helper class makeing loading plugins much more easy
   */
  virtual void loadPlugins() = 0;

  /**
   * @return The string beeing used ans window title.
   */
  virtual String windowTitle() = 0;

  virtual IO::RegularFile splashScreenFile() const = 0;

  virtual shared_ptr<Applet> createMainApplet() = 0;

private:
  bool handleKey(KeyCode key);

public:
  /** Initializes and runs the application.
   */
  void run();

  /** Shutdowns the application.
   */
  static void quit();

  static const DeveloperSettings& developerSettings();

  static DeveloperHud& developerHud();
  static DeveloperTools& developerTools();

  /** Returns the assets directory of this application.
   *
   * If the directory `SpecialDirectories::binaryPrefix() / "share" / applicationName / "assets"` exists and it contains the files
   * - `"/config/resource-locations.cfg"`
   * - and `"/config/plugins.cfg"`
   * then this is our path.
   *
   * otherwise, if the directory
   * `SpecialDirectories::binaryPrefix() / "assets"` exists with thee same files as emntioned above exists, than that's our path.
   *
   * Otherwise the constructor of Application has already raised an exception.
   *
   * @return a path to a existing directory
   */
  static const IO::Directory& assetsDirectory();

  static const shared_ptr<Applet>& currentApplet();
  static void setCurrentApplet(const shared_ptr<Applet>& applet);

  void startup() final override;
  void renderOneFrame();

  static const std::shared_ptr<Gui::Animation>& frametimeAnimation();

  static void openSampleBrowser(const std::string& sampleName);
  static void openSampleBrowser();
  static void startSampleBrowser();
  static void startMainApplet();

  static Application* singletonPtr();
  static Application& singleton();
private:
  void quitIfWindowClosed();

  void initializeResourceGroups();

  void executeDeveloperStartupScript();
};


} // namespace Framework


#endif
