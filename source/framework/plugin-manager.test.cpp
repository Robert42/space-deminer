#include <dependencies.h>
#include <framework/plugin-manager.h>
#include <base/io/special-directories.h>
#include <base/strings/string.h>


namespace Framework {

TEST(framework_PluginManager, tryLoadPlugin_handle_not_existing_plugins)
{
  PluginManager pluginManager;

  EXPECT_FALSE(pluginManager.tryLoadPlugin("aNotExistingPluginForTests"));
}


TEST(framework_PluginManager, loadRequiredPlugin_handle_not_existing_plugins)
{
  PluginManager pluginManager;

  EXPECT_THROW(pluginManager.loadRequiredPlugin("aNotExistingPluginForTests"), std::runtime_error);
}


}
