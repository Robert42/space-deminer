#include <dependencies.h>
#include <framework/plugin-manager.h>
#include <base/io/special-directories.h>
#include <base/strings/string.h>


namespace Framework {
IO::Directory searchPluginDirectory();
bool isPluginDirectory(const IO::Directory& directory);
bool existsPluginFile(const IO::Directory& pluginDirectory, const std::string& pluginName);


PluginManager::PluginManager()
{
  this->pluginDirectory = searchPluginDirectory();
}


bool PluginManager::tryLoadPlugin(std::string pluginName)
{
  pluginName = addDebugSuffixIfReasonable(pluginName);

  if(!pluginFile(pluginName + pluginExtension()).exists())
    return false;

  std::string str = pluginFile(pluginName).pathAsOgreString();

  Ogre::Root::getSingleton().loadPlugin(str);

  return true;
}


void PluginManager::loadRequiredPlugin(const std::string& pluginName)
{
  if(!tryLoadPlugin(pluginName))
    throw std::runtime_error("**PluginManager::loadPlugin* Couldn't load the required plugin '"+pluginName+"'");
}


IO::RegularFile PluginManager::pluginFile(const std::string& pluginName) const
{
  return this->pluginDirectory | pluginName;
}


std::string PluginManager::addDebugSuffixIfReasonable(const std::string& pluginName) const
{
  // If there's an debugging version of the plugin, we should use it
  if(pluginFile(pluginName + "_d" + pluginExtension()).exists())
    return pluginName + "_d";

  return pluginName;
}


std::string PluginManager::pluginExtension()
{
#if OGRE_PLATFORM == OGRE_PLATFORM_LINUX
  return ".so";
#elif OGRE_PLATFORM == OGRE_PLATFORM_APPLE
  return ".dylib";
#elif OGRE_PLATFORM == OGRE_PLATFORM_WIN32
  return ".dll";
#else
#error Unknown Platform
#endif
}


IO::Directory searchPluginDirectory()
{
  typedef std::list<IO::Directory>::iterator PathIterator;

  std::list<IO::Directory> possibleDirectories;
  possibleDirectories.push_back(IO::SpecialDirectories::binaryPrefix() / "lib");
  possibleDirectories.push_back(IO::SpecialDirectories::directoryOfRunningBinary());
  possibleDirectories.push_back(IO::Path("/usr/local/lib/OGRE"));
  possibleDirectories.push_back(IO::Path("/usr/local/lib/Ogre"));
  possibleDirectories.push_back(IO::Path("/usr/local/lib"));
  possibleDirectories.push_back(IO::Path("/usr/lib/OGRE"));
  possibleDirectories.push_back(IO::Path("/usr/lib/Ogre"));
  possibleDirectories.push_back(IO::Path("/usr/lib"));

  for(PathIterator iter=possibleDirectories.begin(); iter!=possibleDirectories.end(); ++iter)
  {
    const IO::Directory& possibleDirectory = *iter;

    if(isPluginDirectory(possibleDirectory))
      return possibleDirectory;
  }

  throw Ogre::FileNotFoundException(0,
                                    "Couldn't find the Plugin Directory",
                                    "searchPluginDirectory()",
                                    __FILE__,
                                    __LINE__);
}


bool isPluginDirectory(const IO::Directory& directory)
{
  if(!directory.exists())
    return false;

  return existsPluginFile(directory, "RenderSystem_Direct3D") || existsPluginFile(directory, "RenderSystem_GL");
}


bool existsPluginFile(const IO::Directory& pluginDirectory, const std::string& pluginName)
{
  return pluginDirectory.existsFileWith([&pluginName](const IO::File& file)
  {
    return String::fromPath(file.path().filename()).subString(0, pluginName.length()) == String::fromUtf8(pluginName);
  });
}


}
