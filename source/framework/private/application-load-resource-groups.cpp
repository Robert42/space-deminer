#include <dependencies.h>

#include <base/io/special-directories.h>
#include <base/io/regular-file.h>

#include <framework/application.h>
#include <framework/scripting/initialization.h>

namespace Framework {


void loadResourceLocationsOfAssetsDirectory(const IO::Directory& assetsDirectory, bool required);

class ResourceGroupLoadingHelper
{
public:
  IO::Directory assetsDirectory;
  Ogre::ResourceGroupManager& resourceManager;
  Ogre::DriverVersion driverVersion;

  std::string groupName;
  bool recursive;

  ResourceGroupLoadingHelper(const IO::Directory& assetsDirectory)
    : assetsDirectory(assetsDirectory),
      resourceManager(Ogre::ResourceGroupManager::getSingleton())
  {
    Ogre::Root& root =  Ogre::Root::getSingleton();
    Ogre::RenderSystem& renderSystem = *root.getRenderSystem();

    driverVersion =renderSystem.getDriverVersion();

    groupName = Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME;
  }

  void set_Group(const std::string& groupName)
  {
    this->groupName = groupName;

    if(groupName == "Default")
      this->groupName = Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME;
  }

  void set_Zip(const std::string& zipArchivePath)
  {
    addResourceLocation(zipArchivePath, "Zip");
  }

  void set_FileSystem(const std::string& directoryPath)
  {
    addResourceLocation(directoryPath, "FileSystem");
  }

  int OpenGL_get_version()
  {
    return driverVersion.major * 100 + driverVersion.minor*10 + driverVersion.release;
  }

private:
  void addResourceLocation(const std::string& path, const std::string& type)
  {
    Ogre::String location = (assetsDirectory / path).pathAsOgreString();

    std::cout << location << std::endl;

    resourceManager.addResourceLocation(location,
                                        type,
                                        groupName);
  }
};

void Application::initializeResourceLocations()
{
  ResourceGroupLoadingHelper helper(assetsDirectory());
  int r;

  AngelScript::asIScriptEngine* engine = Base::Scripting::Engine::angelScriptEngine();
  const char* configGroupName = "ResourceGroupConfigurations";

  uint32 oldAccessMask = engine->SetDefaultAccessMask(Framework::Scripting::AccessMasks::SPECIAL_USAGE);
  r = engine->BeginConfigGroup(configGroupName); Base::Scripting::AngelScriptCheck(r);

  r = engine->SetDefaultNamespace("OpenGL");
  r = engine->RegisterGlobalFunction("int get_version()", AngelScript::asMETHOD(ResourceGroupLoadingHelper, OpenGL_get_version), AngelScript::asCALL_THISCALL_ASGLOBAL, &helper); Base::Scripting::AngelScriptCheck(r);
  r = engine->SetDefaultNamespace("");

  r = engine->RegisterGlobalFunction("void set_Group(const string &in groupName)", AngelScript::asMETHOD(ResourceGroupLoadingHelper, set_Group), AngelScript::asCALL_THISCALL_ASGLOBAL, &helper); Base::Scripting::AngelScriptCheck(r);
  r = engine->RegisterGlobalFunction("void set_Zip(const string &in zipArchivePath)", AngelScript::asMETHOD(ResourceGroupLoadingHelper, set_Zip), AngelScript::asCALL_THISCALL_ASGLOBAL, &helper); Base::Scripting::AngelScriptCheck(r);
  r = engine->RegisterGlobalFunction("void set_FileSystem(const string &in directoryPath)", AngelScript::asMETHOD(ResourceGroupLoadingHelper, set_FileSystem), AngelScript::asCALL_THISCALL_ASGLOBAL, &helper); Base::Scripting::AngelScriptCheck(r);

  r = engine->EndConfigGroup(); Base::Scripting::AngelScriptCheck(r);
  engine->SetDefaultAccessMask(oldAccessMask);

  Scripting::executeScript(assetsDirectory() / "config" | "resource-locations.cfg",
                           std::chrono::milliseconds(1000),
                           Framework::Scripting::AccessMasks::SPECIAL_USAGE);

  r = engine->GarbageCollect(); Base::Scripting::AngelScriptCheck(r);
  r = engine->RemoveConfigGroup(configGroupName); Base::Scripting::AngelScriptCheck(r);
}


} // namespace Framework

