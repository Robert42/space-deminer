#ifndef FRAMEWORK_PRIVATE_MOUSEINPUTDEVICESTATEMACHINE_H
#define FRAMEWORK_PRIVATE_MOUSEINPUTDEVICESTATEMACHINE_H

#include <framework/window/user-input/mouse.h>

#include <base/state-machines/function-pointer-based/final-state-machine.h>

namespace Framework {
namespace Private {


class MouseCursorState : public StateMachines::FunctionPointerBased::State
{
public:
  virtual void executeMouseMove(const ivec2& position) = 0;
  virtual void executeWheel(real relZ) = 0;
  virtual void executePressedMouseButton(MouseButton button) = 0;
  virtual void executeReleasedMouseButton(MouseButton button) = 0;
  virtual void executeLeftWindow() = 0;
  virtual void executeEnterWindow() = 0;
  virtual void executeLostWindowFocus() = 0;
  virtual void executeGainedWindowFocus() = 0;
  virtual void executeMouseMode(MouseMode mode) = 0;
};

template<class T>
class MouseCursorStateTemplate : public StateMachines::FunctionPointerBased::FinalStateMachine<MouseCursorState>::StateTemplate<T>
{
protected:
  typedef StateMachines::FunctionPointerBased::FinalStateMachine<MouseCursorState>::StateTemplate<T> parent_class;

  typedef void (T::*PFNMOUSEMOVE)(const ivec2&);
  typedef void (T::*PFNWHEELMOVE)(real);
  typedef void (T::*PFNBUTTON)(MouseButton);
  typedef void (T::*PFNSTATE)();
  typedef void (T::*PFNMODE)(MouseMode);

  PFNMOUSEMOVE mouseMove;
  PFNWHEELMOVE wheel;
  PFNBUTTON pressedMouseButton, releasedMouseButton;
  PFNSTATE leftWindow, enterWindow;
  PFNSTATE lostWindowFocus, gainedWindowFocus;
  PFNMODE mouseMode;

public:
  void set(T* instance,
           PFNSTATE beginState,
           PFNSTATE state,
           PFNSTATE endState,
           PFNMOUSEMOVE mouseMove,
           PFNWHEELMOVE wheel,
           PFNBUTTON pressedMouseButton,
           PFNBUTTON releasedMouseButton,
           PFNSTATE leftWindow,
           PFNSTATE enterWindow,
           PFNSTATE lostWindowFocus,
           PFNSTATE gainedWindowFocus,
           PFNMODE mouseMode)
  {
    parent_class::set(instance,
                      beginState,
                      state,
                      endState);
    this->mouseMove = mouseMove;
    this->wheel = wheel;
    this->pressedMouseButton = pressedMouseButton;
    this->releasedMouseButton = releasedMouseButton;
    this->leftWindow = leftWindow;
    this->enterWindow = enterWindow;
    this->lostWindowFocus = lostWindowFocus;
    this->gainedWindowFocus = gainedWindowFocus;
    this->mouseMode = mouseMode;
  }

  void executeMouseMove(const ivec2& position) override
  {
    assert(this->mouseMove != nullptr);

    (this->instance->*mouseMove)(position);
  }

  void executeWheel(real relZ) override
  {
    assert(this->wheel != nullptr);

    (this->instance->*wheel)(relZ);
  }

  void executePressedMouseButton(MouseButton button) override
  {
    assert(this->pressedMouseButton != nullptr);

    (this->instance->*pressedMouseButton)(button);
  }

  void executeReleasedMouseButton(MouseButton button) override
  {
    assert(this->releasedMouseButton != nullptr);

    (this->instance->*releasedMouseButton)(button);
  }

  void executeLeftWindow() override
  {
    assert(this->leftWindow != nullptr);

    (this->instance->*leftWindow)();
  }

  void executeEnterWindow() override
  {
    assert(this->enterWindow != nullptr);

    (this->instance->*enterWindow)();
  }

  void executeLostWindowFocus() override
  {
    assert(this->lostWindowFocus != nullptr);

    (this->instance->*lostWindowFocus)();
  }

  void executeGainedWindowFocus() override
  {
    assert(this->gainedWindowFocus != nullptr);

    (this->instance->*gainedWindowFocus)();
  }

  void executeMouseMode(MouseMode mode) override
  {
    assert(this->mouseMode != nullptr);

    (this->instance->*mouseMode)(mode);
  }

};


class MouseStateMachine : public Mouse
{
public:
  typedef Mouse parent_class;

public:
  Signals::Trackable trackable;

private:
  MouseCursorStateTemplate<MouseStateMachine> initialState;
  MouseCursorStateTemplate<MouseStateMachine> capturedMouseState;
  MouseCursorStateTemplate<MouseStateMachine> systemMouseState;
  MouseCursorStateTemplate<MouseStateMachine> lostFocusState;

  StateMachines::FunctionPointerBased::FinalStateMachine<MouseCursorState> stateMachine;

  ivec2 mouseAreaUpperLeft, mouseAreaLowerRight;
  ivec2 lastMousePosition;

public:
  MouseStateMachine();

  void executeMouseMovement(const ivec2& pos);
  void executeMouseWheel(real relZ);
  void executeLeftWindow();
  void executeEnterWindow();
  void executePressedMouseButton(MouseButton button);
  void executeReleasedMouseButton(MouseButton button);
  void executeWindowFocusChanged(bool gainedFocus);
  void executeWindowResized();
  void executeMouseMode(MouseMode mode);

protected:
  virtual void moveSystemMouseCursor(const ivec2& position) = 0;
  virtual ivec2 systemMousePosition() const = 0;
  virtual void setCursorVisibility(bool show) = 0;

private:
  void handleCapturedMouseMove(const ivec2& pos);
  void handleUncapturedMouseMove(const ivec2& pos);
  void sendEventMouseWheel(real relZ);
  void sendEventPressedMouseButton(MouseButton button);
  void sendEventReleasedMouseButton(MouseButton button);
  void handleEnterWindow();
  void handleEnterWindowSystemMode();
  void gotoLostWindowFocusState();
  void gotoDefaultState();
  void setInitialCursorPositionAndGotoDefaultState();
  void handleGainedWindowFocus();
  void handleMouseMode(MouseMode mode);
  void stopSuppressingMouseCursor();

  bool isWithinMouseArea(const ivec2& pos);
  void setSystemMouseCursor(const ivec2& pos);
  void centerSystemMouseCursor();

  void captureMouse();
  void uncaptureMouse();
  void storeSystemCursorPos();

  void showCursor();
  void hideCursor();

  void handlePressedButtonAfterGainedFocus(MouseButton button);

  void ignoreMouseMove(const ivec2&){}
  void ignoreMouseWheel(real){}
  void ignoreButton(MouseButton){}
  void ignore(){}
  void ignoreMouseMode(MouseMode){}

  ivec2 centerOfWindow() const;
};


} // namespace Private
} // namespace Framework

#endif // FRAMEWORK_PRIVATE_MOUSEINPUTDEVICESTATEMACHINE_H
