#include "mouse-state-machine.h"
#include <framework/window.h>
#include <framework/window/user-input/mouse-cursor-layers.h>

namespace Framework {
namespace Private {


const bool useSystemCursorAsInitialIngameCursorPosition = true;


MouseStateMachine::MouseStateMachine()
  : stateMachine(&initialState)
{
  initialState.set(this, // instance
                   &MouseStateMachine::ignore, // beginState
                   &MouseStateMachine::ignore, // state
                   &MouseStateMachine::stopSuppressingMouseCursor, // endState
                   &MouseStateMachine::ignoreMouseMove, // mouseMove
                   &MouseStateMachine::ignoreMouseWheel, // mouseWheel
                   &MouseStateMachine::ignoreButton, // pressedMouseButton
                   &MouseStateMachine::ignoreButton, // releasedMouseButton
                   &MouseStateMachine::ignore, // leftWindow
                   &MouseStateMachine::ignore, // enterWindow
                   &MouseStateMachine::gotoLostWindowFocusState, // lostWindowFocus
                   &MouseStateMachine::setInitialCursorPositionAndGotoDefaultState, // gainedWindowFocus
                   &MouseStateMachine::ignoreMouseMode // mouseMode
                  );

  capturedMouseState.set(this, // instance
                         &MouseStateMachine::captureMouse, // beginState
                         &MouseStateMachine::ignore, // state
                         &MouseStateMachine::ignore, // endState
                         &MouseStateMachine::handleCapturedMouseMove, // mouseMove
                         &MouseStateMachine::sendEventMouseWheel, // mouseWheel
                         &MouseStateMachine::sendEventPressedMouseButton, // pressedMouseButton
                         &MouseStateMachine::sendEventReleasedMouseButton, // releasedMouseButton
                         &MouseStateMachine::ignore, // leftWindow
                         &MouseStateMachine::handleEnterWindow, // enterWindow
                         &MouseStateMachine::gotoLostWindowFocusState, // lostWindowFocus
                         &MouseStateMachine::ignore, // gainedWindowFocus
                         &MouseStateMachine::handleMouseMode // mouseMode
                        );

  systemMouseState.set(this, // instance
                       &MouseStateMachine::uncaptureMouse, // beginState
                       &MouseStateMachine::ignore, // state
                       &MouseStateMachine::storeSystemCursorPos, // endState
                       &MouseStateMachine::handleUncapturedMouseMove, // mouseMove
                       &MouseStateMachine::sendEventMouseWheel, // mouseWheel
                       &MouseStateMachine::sendEventPressedMouseButton, // pressedMouseButton
                       &MouseStateMachine::sendEventReleasedMouseButton, // releasedMouseButton
                       &MouseStateMachine::ignore, // leftWindow
                       &MouseStateMachine::handleEnterWindowSystemMode, // enterWindow
                       &MouseStateMachine::gotoLostWindowFocusState, // lostWindowFocus
                       &MouseStateMachine::ignore, // gainedWindowFocus
                       &MouseStateMachine::handleMouseMode // mouseMode
                      );

  lostFocusState.set(this, // instance
                     &MouseStateMachine::showCursor, // beginState
                     &MouseStateMachine::ignore, // state
                     &MouseStateMachine::ignore, // endState
                     &MouseStateMachine::ignoreMouseMove, // mouseMove
                     &MouseStateMachine::ignoreMouseWheel, // mouseWheel
                     &MouseStateMachine::handlePressedButtonAfterGainedFocus, // pressedMouseButton
                     &MouseStateMachine::ignoreButton, // releasedMouseButton
                     &MouseStateMachine::ignore, // leftWindow
                     &MouseStateMachine::ignore, // enterWindow
                     &MouseStateMachine::ignore, // lostWindowFocus
                     &MouseStateMachine::ignore, // gainedWindowFocus
                     &MouseStateMachine::ignoreMouseMode // mouseMode
                    );

  executeWindowResized();

  RenderWindow::signalWindowResized().connect(std::bind(&MouseStateMachine::executeWindowResized,
                                                        this)).track(this->trackable);
  RenderWindow::signalFocusChanged().connect(std::bind(&MouseStateMachine::executeWindowFocusChanged,
                                                       this, _1)).track(this->trackable);

  signalModeChanged().connect(std::bind(&MouseStateMachine::executeMouseMode,
                                        this, _1)).track(this->trackable);
}


void MouseStateMachine::executeMouseMovement(const ivec2& pos)
{
  stateMachine.currentState().executeMouseMove(pos);
}


void MouseStateMachine::executeMouseWheel(real relZ)
{
  stateMachine.currentState().executeWheel(relZ);
}


void MouseStateMachine::executeLeftWindow()
{
  stateMachine.currentState().executeLeftWindow();
}


void MouseStateMachine::executeEnterWindow()
{
  stateMachine.currentState().executeEnterWindow();
}


void MouseStateMachine::executePressedMouseButton(MouseButton button)
{
  stateMachine.currentState().executePressedMouseButton(button);
}


void MouseStateMachine::executeReleasedMouseButton(MouseButton button)
{
  stateMachine.currentState().executeReleasedMouseButton(button);
}


void MouseStateMachine::executeWindowFocusChanged(bool gainedFocus)
{
  if(gainedFocus)
    stateMachine.currentState().executeGainedWindowFocus();
  else
    stateMachine.currentState().executeLostWindowFocus();
}


void MouseStateMachine::executeMouseMode(MouseMode mode)
{
  stateMachine.currentState().executeMouseMode(mode);
}


void MouseStateMachine::handleCapturedMouseMove(const ivec2& pos)
{
  handleUncapturedMouseMove(pos);

  if(!isWithinMouseArea(pos))
    centerSystemMouseCursor();
}


void MouseStateMachine::handleUncapturedMouseMove(const ivec2& pos)
{
  ivec2 relativeMovement = pos-lastMousePosition;
  lastMousePosition = pos;

  sendSignalMouseMoved(relativeMovement);
}


void MouseStateMachine::sendEventMouseWheel(real relZ)
{
  parent_class::sendSignalMouseWheelMoved(relZ);
}


void MouseStateMachine::sendEventPressedMouseButton(MouseButton button)
{
  sendSignalActionBegin(button);
}


void MouseStateMachine::sendEventReleasedMouseButton(MouseButton button)
{
  sendSignalActionEnd(button);
}


void MouseStateMachine::handlePressedButtonAfterGainedFocus(MouseButton button)
{
  handleEnterWindowSystemMode();
  gotoDefaultState();
  stateMachine.currentState().executePressedMouseButton(button);
}


void MouseStateMachine::handleEnterWindow()
{
  lastMousePosition = this->systemMousePosition();
}


void MouseStateMachine::handleEnterWindowSystemMode()
{
  lastMousePosition = this->systemMousePosition();
  this->setAbsolutePosition(lastMousePosition, false);
}


void MouseStateMachine::gotoLostWindowFocusState()
{
  stateMachine.gotoState(lostFocusState);
  stateMachine.updateState();
}


void MouseStateMachine::handleMouseMode(MouseMode mode)
{
  if(mode == MouseMode::SYSTEM)
    stateMachine.gotoState(systemMouseState);
  else
    stateMachine.gotoState(capturedMouseState);
  stateMachine.updateState();
}


void MouseStateMachine::gotoDefaultState()
{
  handleMouseMode(mouseMode());
}


void MouseStateMachine::setInitialCursorPositionAndGotoDefaultState()
{
  const ivec2 systemCursor = systemMousePosition();

  // If the systemCursor is set to ivec2(0, 0) the real system cursor position couldn't be fetched
  // for now (most probably, because the mouse hasn't entered the window until now)
  bool validCursorPosition = systemCursor!=ivec2(0, 0);

  if(useSystemCursorAsInitialIngameCursorPosition && validCursorPosition)
  {
    setAbsolutePosition(systemCursor, false);
  }else
  {
    setAbsolutePosition(centerOfWindow(), false);
  }

  gotoDefaultState();
}


void MouseStateMachine::executeWindowResized()
{
  const ivec2 windowSize = RenderWindow::size();

  this->mouseAreaUpperLeft = windowSize/4;
  this->mouseAreaLowerRight= (windowSize*3)/4;
}


void MouseStateMachine::captureMouse()
{
  hideCursor();
  centerSystemMouseCursor();
}


void MouseStateMachine::uncaptureMouse()
{
  showCursor();
  setSystemMouseCursor(Mouse::absolutePosition());
}


bool MouseStateMachine::isWithinMouseArea(const ivec2& pos)
{
  return pos.x > mouseAreaUpperLeft.x &&
         pos.y > mouseAreaUpperLeft.y &&
         pos.x < mouseAreaLowerRight.x &&
         pos.y < mouseAreaLowerRight.y;
}


void MouseStateMachine::setSystemMouseCursor(const ivec2& pos)
{
  lastMousePosition = pos;
  moveSystemMouseCursor(pos);
}


void MouseStateMachine::centerSystemMouseCursor()
{
  setSystemMouseCursor(centerOfWindow());
}


void MouseStateMachine::storeSystemCursorPos()
{
  setAbsolutePosition(systemMousePosition(), false);
}


void MouseStateMachine::showCursor()
{
  setCursorVisibility(true);
}


void MouseStateMachine::hideCursor()
{
  setCursorVisibility(false);
}


ivec2 MouseStateMachine::centerOfWindow() const
{
  return (mouseAreaUpperLeft+mouseAreaLowerRight)/2;
}


void MouseStateMachine::stopSuppressingMouseCursor()
{
  Mouse::mouseCursorLayers().suppressMouseCursor->setVisible(false);
}



} // namespace Private
} // namespace Framework
