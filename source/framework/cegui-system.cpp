#include "cegui-system.h"

#include <framework/window/render-window.h>

#include <framework/developer-tools/procedural-assets/gui-themes/dark-mouse-cursors.h>
#include <framework/developer-tools/procedural-assets/gui-themes/dark-theme.h>

#include <base/io/special-directories.h>
#include <base/io/regular-file.h>

#include <CEGUI/RendererModules/Ogre/Renderer.h>
#include <CEGUI/System.h>

#include <CEGUI/DefaultResourceProvider.h>
#include <CEGUI/XMLParser.h>
#include <CEGUI/GUIContext.h>

namespace Framework {


CeguiSystem::CeguiSystem()
{
  bool useXmlSchemas = false;
#ifdef SPACEDEMINER_DEVELOPER
  const IO::Directory xmlSchemaDirectory = IO::SpecialDirectories::binaryPrefix() / "development" / "cegui" / "xml-schemas";
  useXmlSchemas = xmlSchemaDirectory.exists()
                  && (xmlSchemaDirectory|"Animation.xsd").exists()
                  && (xmlSchemaDirectory|"CEGUIConfig.xsd").exists()
                  && (xmlSchemaDirectory|"Falagard.xsd").exists()
                  && (xmlSchemaDirectory|"Font.xsd").exists()
                  && (xmlSchemaDirectory|"GUILayout.xsd").exists()
                  && (xmlSchemaDirectory|"GUIScheme.xsd").exists()
                  && (xmlSchemaDirectory|"Imageset.xsd").exists();
  if(useXmlSchemas)
    CEGUI::System::setDefaultXMLParserName("XercesParser");
#endif

  CEGUI::OgreRenderer& renderer = CEGUI::OgreRenderer::create(*RenderWindow::ogreRenderWindow());
  CEGUI::System::create(renderer);

  CEGUI::DefaultResourceProvider* defaultResourceProvider = dynamic_cast<CEGUI::DefaultResourceProvider*>(CEGUI::System::getSingleton().getResourceProvider());

  assert(defaultResourceProvider != nullptr);

  if(useXmlSchemas)
  {
    defaultResourceProvider->setResourceGroupDirectory("xml-schemas", xmlSchemaDirectory.pathAsCeguiString());

    CEGUI::XMLParser* parser = CEGUI::System::getSingleton().getXMLParser();
    if(parser->isPropertyPresent("SchemaDefaultResourceGroup"))
      parser->setProperty("SchemaDefaultResourceGroup", "xml-schemas");
  }

  typedef ProceduralAssets::GuiThemes::DarkTheme DarkTheme;
  typedef ProceduralAssets::GuiThemes::DarkMouseCursors DarkMouseCursors;
  typedef ProceduralAssets::GuiThemes::TextureAtlas TextureAtlas;

  DarkMouseCursors mouseCursors;
  DarkTheme darkTheme;

  TextureAtlas textureAtlas = mouseCursors.createTextureAtlas();
  textureAtlas.registerImagesToCegui();

  darkTheme.registerToCEGUI();


  CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage(DarkMouseCursors::defaultCursor().toCeguiString());
  CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultTooltipType("dark-theme.tooltip");

  CEGUI::GUIContext* ceguiGuiContext = &CEGUI::System::getSingleton().getDefaultGUIContext();
  ceguiGuiContext->getMouseCursor().setVisible(true);

  InputDevice::ActionPriority actionPriority(InputDevice::ActionPriority::GUI);
  Mouse::signalMouseMoved().connect(actionPriority, std::bind(&CeguiSystem::handleMouseMoved, ceguiGuiContext)).track(this->trackable);
  Mouse::signalButtonPressed().connect(actionPriority, std::bind(&CeguiSystem::handleMouseButtonPressed, _1, ceguiGuiContext)).track(this->trackable);
  Mouse::signalButtonReleased().connect(actionPriority, std::bind(&CeguiSystem::handleMouseButtonReleased, _1, ceguiGuiContext)).track(this->trackable);
  Keyboard::signalKeyReleased().connect(actionPriority, std::bind(&CeguiSystem::handleKeyPressed, _1, ceguiGuiContext)).track(this->trackable);
  Keyboard::signalKeyReleased().connect(actionPriority, std::bind(&CeguiSystem::handleKeyReleased, _1, ceguiGuiContext)).track(this->trackable);
  Keyboard::signalUnicodeEntered().connect(actionPriority, std::bind(&CeguiSystem::handleTextInput, _1, ceguiGuiContext)).track(this->trackable);

  // Current Mouse Position
  updateMousePosition();
}


CeguiSystem::~CeguiSystem()
{
  CEGUI::Renderer* renderer = CEGUI::System::getSingleton().getRenderer();
  CEGUI::System::destroy();
  delete renderer;
}

void CeguiSystem::updateMousePosition()
{
  CEGUI::GUIContext* ceguiGuiContext = &CEGUI::System::getSingleton().getDefaultGUIContext();
  handleMouseMoved(ceguiGuiContext);
}


bool CeguiSystem::handleMouseMoved(CEGUI::GUIContext* ceguiGuiContext)
{
  ivec2 mousePosition = Mouse::absolutePosition();

  return ceguiGuiContext->injectMousePosition(mousePosition.x, mousePosition.y);
}


CEGUI::MouseButton castMouseButton(const MouseButton& button)
{
  // assert, that the following equations are true, as we are just casting the values
  assert((int)MouseButton::LEFT == (int)CEGUI::LeftButton);
  assert((int)MouseButton::RIGHT == (int)CEGUI::RightButton);
  assert((int)MouseButton::MIDDLE == (int)CEGUI::MiddleButton);
  assert((int)MouseButton::BUTTON_3 == (int)CEGUI::X1Button);
  assert((int)MouseButton::BUTTON_4 == (int)CEGUI::X2Button);

  return static_cast<CEGUI::MouseButton>(button.value);
}


bool CeguiSystem::handleMouseButtonPressed(const MouseButton& button,
                                           CEGUI::GUIContext* ceguiGuiContext)
{
  CEGUI::MouseButton mouseButton = castMouseButton(button.value);

  if(mouseButton >= CEGUI::MouseButtonCount)
    return false;

  return ceguiGuiContext->injectMouseButtonDown(mouseButton);
}


bool CeguiSystem::handleMouseButtonReleased(const MouseButton& button,
                                            CEGUI::GUIContext* ceguiGuiContext)
{
  CEGUI::MouseButton mouseButton = castMouseButton(button.value);

  if(mouseButton >= CEGUI::MouseButtonCount)
    return false;

  return ceguiGuiContext->injectMouseButtonUp(mouseButton);
}

inline CEGUI::Key::Scan toCeguiKey(KeyCode::Value keyCode)
{
  switch(keyCode)
  {
  case KeyCode::A:
    return CEGUI::Key::A;
  case KeyCode::ALT_LEFT:
    return CEGUI::Key::LeftAlt;
  case KeyCode::ALT_RIGHT:
    return CEGUI::Key::RightAlt;
  case KeyCode::ARROW_DOWN:
    return CEGUI::Key::ArrowDown;
  case KeyCode::ARROW_LEFT:
    return CEGUI::Key::ArrowLeft;
  case KeyCode::ARROW_RIGHT:
    return CEGUI::Key::ArrowRight;
  case KeyCode::ARROW_UP:
    return CEGUI::Key::ArrowUp;
  case KeyCode::B:
    return CEGUI::Key::B;
  case KeyCode::BACKSPACE:
    return CEGUI::Key::Backspace;
  case KeyCode::C:
    return CEGUI::Key::C;
  case KeyCode::CONTROL_LEFT:
    return CEGUI::Key::LeftControl;
  case KeyCode::CONTROL_RIGHT:
    return CEGUI::Key::RightControl;
  case KeyCode::D:
    return CEGUI::Key::D;
  case KeyCode::DELETE:
    return CEGUI::Key::Delete;
  case KeyCode::E:
    return CEGUI::Key::E;
  case KeyCode::END:
    return CEGUI::Key::End;
  case KeyCode::ESCAPE:
    return CEGUI::Key::Escape;
  case KeyCode::F:
    return CEGUI::Key::F;
  case KeyCode::F1:
    return CEGUI::Key::F1;
  case KeyCode::F2:
    return CEGUI::Key::F2;
  case KeyCode::F3:
    return CEGUI::Key::F3;
  case KeyCode::F4:
    return CEGUI::Key::F4;
  case KeyCode::F5:
    return CEGUI::Key::F5;
  case KeyCode::F6:
    return CEGUI::Key::F6;
  case KeyCode::F7:
    return CEGUI::Key::F7;
  case KeyCode::F8:
    return CEGUI::Key::F8;
  case KeyCode::F9:
    return CEGUI::Key::F9;
  case KeyCode::F10:
    return CEGUI::Key::F10;
  case KeyCode::F11:
    return CEGUI::Key::F11;
  case KeyCode::F12:
    return CEGUI::Key::F12;
  case KeyCode::G:
    return CEGUI::Key::G;
  case KeyCode::H:
    return CEGUI::Key::H;
  case KeyCode::HOME:
    return CEGUI::Key::Home;
  case KeyCode::I:
    return CEGUI::Key::I;
  case KeyCode::INSERT:
    return CEGUI::Key::Insert;
  case KeyCode::J:
    return CEGUI::Key::J;
  case KeyCode::K:
    return CEGUI::Key::K;
  case KeyCode::KEYPAD_0:
    return CEGUI::Key::Zero;
  case KeyCode::KEYPAD_1:
    return CEGUI::Key::One;
  case KeyCode::KEYPAD_2:
    return CEGUI::Key::Two;
  case KeyCode::KEYPAD_3:
    return CEGUI::Key::Three;
  case KeyCode::KEYPAD_4:
    return CEGUI::Key::Four;
  case KeyCode::KEYPAD_5:
    return CEGUI::Key::Five;
  case KeyCode::KEYPAD_6:
    return CEGUI::Key::Six;
  case KeyCode::KEYPAD_7:
    return CEGUI::Key::Seven;
  case KeyCode::KEYPAD_8:
    return CEGUI::Key::Eight;
  case KeyCode::KEYPAD_9:
    return CEGUI::Key::Nine;
  case KeyCode::L:
    return CEGUI::Key::L;
  case KeyCode::M:
    return CEGUI::Key::M;
  case KeyCode::N:
    return CEGUI::Key::N;
  case KeyCode::NUMPAD_0:
    return CEGUI::Key::Numpad0;
  case KeyCode::NUMPAD_1:
    return CEGUI::Key::Numpad1;
  case KeyCode::NUMPAD_2:
    return CEGUI::Key::Numpad2;
  case KeyCode::NUMPAD_3:
    return CEGUI::Key::Numpad3;
  case KeyCode::NUMPAD_4:
    return CEGUI::Key::Numpad4;
  case KeyCode::NUMPAD_5:
    return CEGUI::Key::Numpad5;
  case KeyCode::NUMPAD_6:
    return CEGUI::Key::Numpad6;
  case KeyCode::NUMPAD_7:
    return CEGUI::Key::Numpad7;
  case KeyCode::NUMPAD_8:
    return CEGUI::Key::Numpad8;
  case KeyCode::NUMPAD_9:
    return CEGUI::Key::Numpad9;
  case KeyCode::NUMPAD_ENTER:
    return CEGUI::Key::NumpadEnter;
  case KeyCode::NUMPAD_MINUS:
    return CEGUI::Key::Scan::Unknown;
  case KeyCode::NUMPAD_PLUS:
    return CEGUI::Key::Scan::Unknown;
  case KeyCode::O:
    return CEGUI::Key::O;
  case KeyCode::P:
    return CEGUI::Key::P;
  case KeyCode::Q:
    return CEGUI::Key::Q;
  case KeyCode::R:
    return CEGUI::Key::R;
  case KeyCode::RETURN:
    return CEGUI::Key::Return;
  case KeyCode::S:
    return CEGUI::Key::S;
  case KeyCode::SHIFT_LEFT:
    return CEGUI::Key::LeftShift;
  case KeyCode::SHIFT_RIGHT:
    return CEGUI::Key::RightShift;
  case KeyCode::SPACE:
    return CEGUI::Key::Space;
  case KeyCode::T:
    return CEGUI::Key::T;
  case KeyCode::TAB:
    return CEGUI::Key::Tab;
  case KeyCode::U:
    return CEGUI::Key::U;
  case KeyCode::V:
    return CEGUI::Key::V;
  case KeyCode::W:
    return CEGUI::Key::W;
  case KeyCode::X:
    return CEGUI::Key::X;
  case KeyCode::Y:
    return CEGUI::Key::Y;
  case KeyCode::Z:
    return CEGUI::Key::Z;
  default:
    return CEGUI::Key::Scan::Unknown;
  }
}

bool CeguiSystem::handleKeyPressed(const KeyCode& keyCode, CEGUI::GUIContext* ceguiGuiContext)
{
  CEGUI::Key::Scan key = toCeguiKey(keyCode.value);

  if(key != CEGUI::Key::Unknown)
    return ceguiGuiContext->injectKeyDown(key);
  else
    return false;
}

bool CeguiSystem::handleKeyReleased(const KeyCode& keyCode, CEGUI::GUIContext* ceguiGuiContext)
{
  CEGUI::Key::Scan key = toCeguiKey(keyCode.value);

  if(key != CEGUI::Key::Unknown)
    return ceguiGuiContext->injectKeyUp(key);
  else
    return false;
}

bool CeguiSystem::handleTextInput(String::value_type character, CEGUI::GUIContext* ceguiGuiContext)
{
  return ceguiGuiContext->injectChar(character);
}



} // namespace Framework

