#include <framework/scripting/terminal-syntax-highlighter.h>

#define HIGHLIGHT_AREA_BETWEEN_UNMATCHED_BRACKETS

namespace Framework {
namespace Scripting {
namespace SyntaxHightlighting {


Settings::Settings()
{
}


typedef Base::Scripting::TokenizedScript TokenizedScript;
typedef TokenizedScript::Token Token;


Result parse(const String& script,
             const std::function<void(const Token& token, const Terminal::Format& format)>& callback,
             const Settings& settings)
{
  (void)settings;

  Result result = Result::NO_ERRORS;

  const Terminal::Format normal;
  const Terminal::Format numericValue(Terminal::Format::Color::ORANGE);
  const Terminal::Format stringValue(Terminal::Format::Color::GREEN, Terminal::Format::Light::DARKER);
  const Terminal::Format keyWord(Terminal::Format::Color::ORANGE, Terminal::Format::Light::BRIGHTER);
  const Terminal::Format errorBold(Terminal::Format::Color::MAGENTA, Terminal::Format::Light::BRIGHTER);
  const Terminal::Format errorNormal(Terminal::Format::Color::MAGENTA, Terminal::Format::Light::NORMAL);
  const Terminal::Format comment(Terminal::Format::Color::NORMAL, Terminal::Format::Light::DARKER);

  Base::Scripting::TokenizedScript tokenizedScript(script);

  if(tokenizedScript.tokens().empty())
    return result;

  for(const Token& token : tokenizedScript.tokens())
  {
    Terminal::Format currentFormat;

    switch(token.type.value)
    {
    case Token::Type::COMMENT:
      currentFormat = comment;
      break;
    case Token::Type::VALUE_OTHER:
      currentFormat = numericValue;
      break;
    case Token::Type::VALUE_STRING:
      currentFormat = stringValue;
      break;
    case Token::Type::KEYWORD_WORD:
    case Token::Type::VALUE_BOOLEAN:
      currentFormat = keyWord;
      break;
    case Token::Type::UNKNOWN:
      currentFormat = errorBold;
      result = Result::ERRORS;
      break;
    case Token::Type::KEYWORD_OPENING_BRACKET:
    case Token::Type::KEYWORD_CLOSING_BRACKET:
      currentFormat = normal;
      if(!token.hasMatchingBracket())
      {
        currentFormat = errorBold;
        result = Result::ERRORS;
      }
      break;
    default:
      currentFormat = normal;
    }

#ifdef HIGHLIGHT_AREA_BETWEEN_UNMATCHED_BRACKETS
    if(token.isInRangeOfUnmachedBrackets && currentFormat!=errorBold)
    {
      currentFormat = errorNormal;
      result = Result::ERRORS;
    }
#endif

    callback(token, currentFormat);
  }

  return result;
}


Result reformat(const Terminal::Buffer::Line::Ptr& line,
                size_t begin,
                size_t end,
                const Settings& settings)
{
  String script = line->content().subString(begin, end-begin);
  size_t offset = begin;

  auto writeOutToken = [&line, offset](const Token &token, const Terminal::Format &format){

    size_t begin = offset+token.indexBegin;
    size_t end = begin + token.length();

    line->reformat(format, begin, end);
  };

  return parse(script,
               writeOutToken,
               settings);
}


Result writeOut(Terminal::InteractiveArea& interactiveArea,
                const String& script,
                const Settings& settings)
{
  auto writeOutToken = [&interactiveArea](const Token &token, const Terminal::Format &format){
    interactiveArea.writeOut(format, token.tokenAsString());
  };

  return parse(script,
               writeOutToken,
               settings);
}

}
}
}
