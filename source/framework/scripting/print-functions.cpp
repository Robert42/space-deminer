#include <framework/scripting/initialization.h>
#include <framework/developer-tools/terminal.h>

#include <base/scripting/engine-library-format.h>

namespace Framework {
namespace Scripting {


void printUtf8(const std::string& text, Terminal::Format::Color color, Terminal::Format::Light lightness)
{
  Terminal::writeOut(Terminal::Format(color, lightness), String::fromUtf8(text));
}


void printUtf8(const FormatedValue& v, Terminal::Format::Color color, Terminal::Format::Light lightness)
{
  printUtf8(v.get(), color, lightness);
}


void printUtf8(const std::string& text, const FormatedValue& v1, Terminal::Format::Color color, Terminal::Format::Light lightness)
{
  printUtf8(format(text, v1), color, lightness);
}


void printUtf8(const std::string& text, const FormatedValue& v1, const FormatedValue& v2, Terminal::Format::Color color, Terminal::Format::Light lightness)
{
  printUtf8(format(text, v1, v2), color, lightness);
}


void printUtf8(const std::string& text, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, Terminal::Format::Color color, Terminal::Format::Light lightness)
{
  printUtf8(format(text, v1, v2, v3), color, lightness);
}


void printUtf8(const std::string& text, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, Terminal::Format::Color color, Terminal::Format::Light lightness)
{
  printUtf8(format(text, v1, v2, v3, v4), color, lightness);
}


void printUtf8(const std::string& text, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, Terminal::Format::Color color, Terminal::Format::Light lightness)
{
  printUtf8(format(text, v1, v2, v3, v4, v5), color, lightness);
}


void printUtf8(const std::string& text, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6, Terminal::Format::Color color, Terminal::Format::Light lightness)
{
  printUtf8(format(text, v1, v2, v3, v4, v5, v6), color, lightness);
}


void printUtf8(const std::string& text, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6, const FormatedValue& v7, Terminal::Format::Color color, Terminal::Format::Light lightness)
{
  printUtf8(format(text, v1, v2, v3, v4, v5, v6, v7), color, lightness);
}


void printUtf8(const std::string& text, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6, const FormatedValue& v7, const FormatedValue& v8, Terminal::Format::Color color, Terminal::Format::Light lightness)
{
  printUtf8(format(text, v1, v2, v3, v4, v5, v6, v7, v8), color, lightness);
}


void printUtf8(const std::string& text, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6, const FormatedValue& v7, const FormatedValue& v8, const FormatedValue& v9, Terminal::Format::Color color, Terminal::Format::Light lightness)
{
  printUtf8(format(text, v1, v2, v3, v4, v5, v6, v7, v8, v9), color, lightness);
}


void printUtf8(const std::string& text, const FormatedValue& v1, const FormatedValue& v2, const FormatedValue& v3, const FormatedValue& v4, const FormatedValue& v5, const FormatedValue& v6, const FormatedValue& v7, const FormatedValue& v8, const FormatedValue& v9, const FormatedValue& v10, Terminal::Format::Color color, Terminal::Format::Light lightness)
{
  printUtf8(format(text, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10), color, lightness);
}


void registerPrint(AngelScript::asIScriptEngine* as_engine)
{
  int r;

  r = as_engine->RegisterGlobalFunction("void print(string &in str, Terminal::Color color=Terminal::Color::Normal, Terminal::Lightness lightness = Terminal::Lightness::Normal)", AngelScript::asFUNCTIONPR(printUtf8, (const std::string&, Terminal::Format::Color, Terminal::Format::Light), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void print(FormatedValue &in v, Terminal::Color color=Terminal::Color::Normal, Terminal::Lightness lightness = Terminal::Lightness::Normal)", AngelScript::asFUNCTIONPR(printUtf8, (const FormatedValue&, Terminal::Format::Color, Terminal::Format::Light), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void print(const string &in str, FormatedValue &in v1, Terminal::Color color=Terminal::Color::Normal, Terminal::Lightness lightness = Terminal::Lightness::Normal)", AngelScript::asFUNCTIONPR(printUtf8, (const std::string&, const FormatedValue&, Terminal::Format::Color, Terminal::Format::Light), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void print(const string &in str, FormatedValue &in v1, FormatedValue &in v2, Terminal::Color color=Terminal::Color::Normal, Terminal::Lightness lightness = Terminal::Lightness::Normal)", AngelScript::asFUNCTIONPR(printUtf8, (const std::string&, const FormatedValue&, const FormatedValue&, Terminal::Format::Color, Terminal::Format::Light), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void print(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, Terminal::Color color=Terminal::Color::Normal, Terminal::Lightness lightness = Terminal::Lightness::Normal)", AngelScript::asFUNCTIONPR(printUtf8, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, Terminal::Format::Color, Terminal::Format::Light), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void print(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, Terminal::Color color=Terminal::Color::Normal, Terminal::Lightness lightness = Terminal::Lightness::Normal)", AngelScript::asFUNCTIONPR(printUtf8, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, Terminal::Format::Color, Terminal::Format::Light), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void print(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, Terminal::Color color=Terminal::Color::Normal, Terminal::Lightness lightness = Terminal::Lightness::Normal)", AngelScript::asFUNCTIONPR(printUtf8, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, Terminal::Format::Color, Terminal::Format::Light), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void print(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, FormatedValue &in v6, Terminal::Color color=Terminal::Color::Normal, Terminal::Lightness lightness = Terminal::Lightness::Normal)", AngelScript::asFUNCTIONPR(printUtf8, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, Terminal::Format::Color, Terminal::Format::Light), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void print(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, FormatedValue &in v6, FormatedValue &in v7, Terminal::Color color=Terminal::Color::Normal, Terminal::Lightness lightness = Terminal::Lightness::Normal)", AngelScript::asFUNCTIONPR(printUtf8, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, Terminal::Format::Color, Terminal::Format::Light), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void print(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, FormatedValue &in v6, FormatedValue &in v7, FormatedValue &in v8, Terminal::Color color=Terminal::Color::Normal, Terminal::Lightness lightness = Terminal::Lightness::Normal)", AngelScript::asFUNCTIONPR(printUtf8, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, Terminal::Format::Color, Terminal::Format::Light), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void print(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, FormatedValue &in v6, FormatedValue &in v7, FormatedValue &in v8, FormatedValue &in v9, Terminal::Color color=Terminal::Color::Normal, Terminal::Lightness lightness = Terminal::Lightness::Normal)", AngelScript::asFUNCTIONPR(printUtf8, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, Terminal::Format::Color, Terminal::Format::Light), void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
  r = as_engine->RegisterGlobalFunction("void print(const string &in str, FormatedValue &in v1, FormatedValue &in v2, FormatedValue &in v3, FormatedValue &in v4, FormatedValue &in v5, FormatedValue &in v6, FormatedValue &in v7, FormatedValue &in v8, FormatedValue &in v9, FormatedValue &in v10, Terminal::Color color=Terminal::Color::Normal, Terminal::Lightness lightness = Terminal::Lightness::Normal)", AngelScript::asFUNCTIONPR(printUtf8, (const std::string&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, const FormatedValue&, Terminal::Format::Color, Terminal::Format::Light),void), AngelScript::asCALL_CDECL); AngelScriptCheck(r);
}


} // namespace Scripting
} // namespace Framework
