#include <framework/scripting/initialization.h>
#include <framework/developer-tools/terminal.h>

namespace Framework {
namespace Scripting {

void printUtf8(const std::string& text, Terminal::Format::Color color, Terminal::Format::Light lightness);
bool printAnglescriptMessage(const Engine::MessageInfo& msg);

void registerPrint(AngelScript::asIScriptEngine* as_engine);


void initializeScripting()
{
  int r;

  AngelScript::asIScriptEngine* as_engine = Engine::angelScriptEngine();

  r = as_engine->SetDefaultNamespace("Terminal");
  AngelScriptCheck(r);

  uint32 oldAccessMask = as_engine->SetDefaultAccessMask(AccessMasks::ALL);
  r = as_engine->RegisterEnum("Color");
  AngelScriptCheck(r);

  r = as_engine->RegisterEnumValue("Color", "Normal", Terminal::Format::Color::NORMAL);
  AngelScriptCheck(r);
  r = as_engine->RegisterEnumValue("Color", "Red", Terminal::Format::Color::RED);
  AngelScriptCheck(r);
  r = as_engine->RegisterEnumValue("Color", "Green", Terminal::Format::Color::GREEN);
  AngelScriptCheck(r);
  r = as_engine->RegisterEnumValue("Color", "Blue", Terminal::Format::Color::BLUE);
  AngelScriptCheck(r);
  r = as_engine->RegisterEnumValue("Color", "Yellow", Terminal::Format::Color::YELLOW);
  AngelScriptCheck(r);
  r = as_engine->RegisterEnumValue("Color", "Orange", Terminal::Format::Color::ORANGE);
  AngelScriptCheck(r);
  r = as_engine->RegisterEnumValue("Color", "Magenta", Terminal::Format::Color::MAGENTA);
  AngelScriptCheck(r);
  r = as_engine->RegisterEnumValue("Color", "Cyan", Terminal::Format::Color::CYAN);
  AngelScriptCheck(r);

  r = as_engine->RegisterEnum("Lightness");
  AngelScriptCheck(r);
  r = as_engine->RegisterEnumValue("Lightness", "Darker", Terminal::Format::Light::DARKER);
  AngelScriptCheck(r);
  r = as_engine->RegisterEnumValue("Lightness", "Normal", Terminal::Format::Light::NORMAL);
  AngelScriptCheck(r);
  r = as_engine->RegisterEnumValue("Lightness", "Brighter", Terminal::Format::Light::BRIGHTER);
  AngelScriptCheck(r);

  registerPrint(as_engine);

  r = as_engine->SetDefaultNamespace("");
  AngelScriptCheck(r);

  registerPrint(as_engine);

  as_engine->SetDefaultAccessMask(oldAccessMask);
  AngelScriptCheck(r);

  Engine::scriptMessageSignal().connect(printAnglescriptMessage).trackManually();
}


bool printAnglescriptMessage(const Engine::MessageInfo& msg)
{
  Terminal::Format::Color color;
  switch(msg.type)
  {
  case AngelScript::asMSGTYPE_ERROR:
    color = Terminal::Format::Color::RED;
    break;
  case AngelScript::asMSGTYPE_INFORMATION:
    color = Terminal::Format::Color::BLUE;
    break;
  case AngelScript::asMSGTYPE_WARNING:
    color = Terminal::Format::Color::ORANGE;
    break;
  }
  Terminal::writeOut(Terminal::Format(color, Terminal::Format::Light::BRIGHTER), msg.format() + "\n");
  return false;
}


} // namespace Scripting
} // namespace Framework
