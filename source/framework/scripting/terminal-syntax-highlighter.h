#ifndef _FRAMEWORK_SCRIPTING_SYNTAX_HIGHLIGHTER_H_
#define _FRAMEWORK_SCRIPTING_SYNTAX_HIGHLIGHTER_H_

#include "initialization.h"
#include <framework/developer-tools/terminal/interactive-area.h>
#include <framework/developer-tools/terminal/buffer/terminal-buffer.h>
#include <base/scripting/tokenized-script.h>

namespace Framework {
namespace Scripting {
namespace SyntaxHightlighting {

struct Settings
{
public:
  Settings();
};

BEGIN_ENUMERATION(Result,)
  NO_ERRORS,
  ERRORS
ENUMERATION_BASIC_OPERATORS(Result)
END_ENUMERATION;


Result parse(const String& script,
             const std::function<void(const TokenizedScript::Token& token, const Terminal::Format& format)>& callback,
             const Settings& settings=Settings());

Result reformat(const Terminal::Buffer::Line::Ptr& line,
                size_t begin,
                size_t end,
                const Settings& settings=Settings());

Result writeOut(Terminal::InteractiveArea& interactiveArea,
                const String& script,
                const Settings& settings=Settings());

} // namespace SyntaxHightlighting
} // namespace Scripting
} // namespace Framework

#endif
