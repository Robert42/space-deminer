#ifndef _FRAMEWORK_SCRIPTING_INITIALIZATION_H_
#define _FRAMEWORK_SCRIPTING_INITIALIZATION_H_

#include <base/scripting/engine.h>

namespace Framework {
using namespace Base;

namespace Scripting {
namespace AccessMasks {
using namespace Base::Scripting::AccessMasks;

const uint32 TERMINAL_COMMANDS = 0x00000010;
const uint32 SPECIAL_USAGE = 0x00008000;
const uint32 USER_MASKS = 0x00010000;
}

using namespace Base::Scripting;


void initializeScripting();


} // namespace Scripting
} // namespace Framework

#endif
