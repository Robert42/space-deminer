#include <framework/private-rendering-loop.h>
#include <framework/application.h>
#include <framework/frame-timer.h>

namespace Framework {


PrivateRenderLoop::PrivateRenderLoop(float frameRate)
  : milliSecondsToWait(ceil(1000.f/frameRate))
{
  Application* application = Application::singletonPtr();

  timer.reset();
  signalRenderOneFrame().connect(std::bind(&Application::renderOneFrame, application)).track(application->trackable);
}

PrivateRenderLoop::PrivateRenderLoop()
  : PrivateRenderLoop(16.f)
{
}

void PrivateRenderLoop::update()
{
  if(timer.getMilliseconds() >= milliSecondsToWait)
  {
    FrameTimer::ignoreNextFrame |= milliSecondsToWait > 1000/30;
    _signalRenderOneFrame();
    timer.reset();
  }
}

Signals::Signal<void()>& PrivateRenderLoop::signalRenderOneFrame()
{
  return _signalRenderOneFrame;
}


}
