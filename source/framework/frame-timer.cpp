#include <framework/frame-timer.h>
#include <base/threads.h>

namespace Framework {
FrameTimer::Microseconds FrameTimer::minFrameTime_ = 0;
FrameTimer::Microseconds FrameTimer::fixedFrameTime_ = 0;
FrameTimer::Microseconds FrameTimer::frameTimeWithoutLocking_ = 0;
FrameTimer::Microseconds FrameTimer::frameTime_ = 0;
FrameTimer::Microseconds FrameTimer::totalMicroseconds_ = 0;

FrameTimer::Seconds FrameTimer::constrainedTime_ = 0;
FrameTimer::Seconds FrameTimer::averageTime_ = 0;

FrameTimer::Seconds FrameTimer::totalSeconds_ = 0;

bool FrameTimer::ignoreNextFrame = false;

bool FrameTimer::hasInstance_ = false;


FrameTimer::Ptr FrameTimer::create()
{
  return Ptr(new FrameTimer);
}

FrameTimer::FrameTimer()
{
  assert(hasInstance_ == false);
  hasInstance_ = true;

  minFrameTime_ = framerateToMicroSeconds(160);
  totalMicroseconds_ = 0;
  averageTime_ = 0;

  averageOfTimes_.setNumberValuesToStore(64);

  reset();
}

FrameTimer::~FrameTimer()
{
  hasInstance_ = false;
}

void FrameTimer::reset()
{
  if(minFrameTime_ > 0)
    update(minFrameTime_);
  else
    update(10000);
}

void FrameTimer::update()
{
  update(timer_.getMicroseconds());
}

void FrameTimer::update(Microseconds time)
{
  frameTimeWithoutLocking_ = time;

  Microseconds minFrameTime = fixedFrameTime_!=0 ? fixedFrameTime_ : minFrameTime_;

  if(minFrameTime > 0 && time < minFrameTime)
  {
    Base::sleepFor(std::chrono::microseconds(minFrameTime - time));
    time = minFrameTime_;
  }

  timer_.reset();

  if(ignoreNextFrame)
  {
    ignoreNextFrame = false;
    return;
  }

  frameTime_ = time;

  if(fixedFrameTime_ != 0)
    frameTime_ = fixedFrameTime_;

  frameTime_ = max<Microseconds>(1, frameTime_);

  totalMicroseconds_ += frameTime();
  averageTime_ += microsecondsToSeconds(frameTime());

  constrainedTime_ = min<Seconds>(1./8., microsecondsToSeconds(time));

  averageOfTimes_.addValue(constrainedTime_);
  averageTime_ = averageOfTimes_.calcAverageWithFalloff(0.95);
}



void FrameTimer::lockMaxFramerate(FramesPerSecond maxFramerate)
{
  if(maxFramerate == 0)
  {
    minFrameTime_ = 0;
  }else
  {
    maxFramerate = clamp<FramesPerSecond>(maxFramerate, 60, 1000);

    minFrameTime_ = framerateToMicroSeconds(maxFramerate);
  }
}


FrameTimer::FramesPerSecond FrameTimer::lockedFrameRate()
{
  return microsecondsToFramerate(minFrameTime_);
}


void FrameTimer::setFixedFramerate(FramesPerSecond fixedFramerate)
{
  fixedFrameTime_ = framerateToMicroSeconds(fixedFramerate);
}


FrameTimer::FramesPerSecond FrameTimer::fixedFrameRate()
{
  return microsecondsToFramerate(fixedFrameTime_);
}


FrameTimer::Microseconds FrameTimer::frameTimeWithoutLocking()
{
  return frameTimeWithoutLocking_;
}

FrameTimer::Microseconds FrameTimer::frameTime()
{
  return frameTime_;
}

FrameTimer::Seconds FrameTimer::constrainedTime()
{
  return constrainedTime_;
}

FrameTimer::Seconds FrameTimer::averageTime()
{
  return averageTime_;
}

FrameTimer::Microseconds FrameTimer::totalMicroseconds()
{
  return totalMicroseconds_;
}

FrameTimer::Seconds FrameTimer::totalSeconds()
{
  return totalSeconds_;
}



FrameTimer::Microseconds FrameTimer::framerateToMicroSeconds(FramesPerSecond framerate)
{
  return secondsToMicroseconds(real(1.0)/framerate);
}

FrameTimer::FramesPerSecond FrameTimer::microsecondsToFramerate(Microseconds microseconds)
{
  return real(1.0)/microsecondsToSeconds(microseconds);
}

FrameTimer::Seconds FrameTimer::microsecondsToSeconds(Microseconds microseconds)
{
  return real(1.e-6) * microseconds;
}

FrameTimer::Microseconds FrameTimer::secondsToMicroseconds(Seconds seconds)
{
  return static_cast<Microseconds>(round(real(1.e6) * seconds));
}


}
