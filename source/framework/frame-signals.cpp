#include <dependencies.h>
#include <framework/frame-signals.h>


namespace Framework {


FrameSignals::FrameSignals()
{
  signalsAreBlocked = false;
  _signalCallOnce = &_signalCallOnce_a;
}


FrameSignals::~FrameSignals()
{
}


bool FrameSignals::frameStarted(const Ogre::FrameEvent& evt)
{
  real time = evt.timeSinceLastFrame;

  signalsAreBlocked = _signalBlocker(time);

  if(signalsAreBlocked)
    return true;

  _signalFrameStart(time);

  return true;
}


bool FrameSignals::frameRenderingQueued(const Ogre::FrameEvent& evt)
{
  if(signalsAreBlocked)
    return true;

  real time = evt.timeSinceLastFrame;

  _signalRenderingQueued(time);

  FrameSignals::CallableSignal* signalToSend;

  if(_signalCallOnce == &_signalCallOnce_a)
  {
    _signalCallOnce = &_signalCallOnce_b;
    signalToSend = &_signalCallOnce_a;
  }else
  {
    _signalCallOnce = &_signalCallOnce_a;
    signalToSend = &_signalCallOnce_b;
  }

  (*signalToSend)(time);
  signalToSend->disconnectAll();

  return true;
}


bool FrameSignals::frameEnded(const Ogre::FrameEvent& evt)
{
  if(signalsAreBlocked)
    return true;

  real time = evt.timeSinceLastFrame;

  _signalFrameEnd(time);

  return true;
}


Signals::Connection FrameSignals::callOnce(const std::function<void(real)>& slot)
{
  return singleton()._signalCallOnce->connect(slot).trackManually();
}

FrameSignals::Signal& FrameSignals::signalFrameStart()
{
  return singleton()._signalFrameStart;
}

FrameSignals::Signal& FrameSignals::signalRenderingQueued()
{
  return singleton()._signalRenderingQueued;
}

FrameSignals::Signal& FrameSignals::signalFrameEnd()
{
  return singleton()._signalFrameEnd;
}


FrameSignals::BlockerSignal& FrameSignals::signalBlocker()
{
  return singleton()._signalBlocker;
}


FrameSignals::Blocker::Blocker()
{
  FrameSignals::signalBlocker().connect(&Blocker::block).track(this->trackable);
}

bool FrameSignals::Blocker::block(real)
{
  return true;
}


}
