#ifndef FRAMEWORK_GUI_TYPES_H
#define FRAMEWORK_GUI_TYPES_H

#include <base/types/class-type.h>

namespace Framework {

using namespace Base;

namespace Gui {
namespace Types {

extern ClassType Widget;
extern ClassType Container;
extern ClassType Layout;
extern ClassType FontImplementation;

namespace Widgets
{
extern ClassType Canvas;
extern ClassType DummyWidget;
extern ClassType Label;
}

namespace Containers
{
extern ClassType Background;
extern ClassType Bin;
extern ClassType Button;
extern ClassType DummyContainer;
extern ClassType WindowFrame;
}

namespace Ingame
{
extern ClassType GorillaFont;
}

namespace Layouts
{
extern ClassType Alignment;
extern ClassType BaseBox;
extern ClassType BinaryLayout;
extern ClassType Border;
extern ClassType Box;
extern ClassType ButtonBox;
extern ClassType LimitSize;
extern ClassType OrderedLayout;
extern ClassType StackedLayout;
extern ClassType UnorderedLayout;
extern ClassType WholeArea;
extern ClassType ZOffset;
}

} // namespace Types

void registerTypes();

} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_TYPES_H
