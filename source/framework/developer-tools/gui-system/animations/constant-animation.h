#ifndef FRAMEWORK_GUI_ANIMATIONS_CONSTANTANIMATION_H
#define FRAMEWORK_GUI_ANIMATIONS_CONSTANTANIMATION_H

#include "../animation.h"

namespace Framework {
namespace Gui {
namespace Animations {

class ConstantAnimation final : public Animation
{
public:
  typedef std::shared_ptr<ConstantAnimation> Ptr;

public:
  const real value;

private:
  ConstantAnimation(real value);

public:
  static Ptr create(real value);

private:
  void _updateValues(const Output<real>& currentValue,
                     const Output<real>& currentVelocity,
                     const Output<real>& currentAcceleration) override;
};

} // namespace Animations
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_ANIMATIONS_CONSTANTANIMATION_H
