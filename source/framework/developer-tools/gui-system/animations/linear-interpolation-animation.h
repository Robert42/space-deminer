#ifndef FRAMEWORK_GUI_ANIMATIONS_LINEARINTERPOLATIONANIMATION_H
#define FRAMEWORK_GUI_ANIMATIONS_LINEARINTERPOLATIONANIMATION_H

#include "base-interpolation-animation.h"

namespace Framework {
namespace Gui {
namespace Animations {

class LinearInterpolationAnimation final : public BaseInterpolation
{
public:
  typedef std::shared_ptr<LinearInterpolationAnimation> Ptr;

private:
  LinearInterpolationAnimation(const Animation::Ptr& timeAnimation, real totalInterpolationTime);
  LinearInterpolationAnimation(real totalInterpolationTime);

public:
  static Ptr create(const Animation::Ptr& timeAnimation, real totalInterpolationTime);
  static Ptr create(real totalInterpolationTime);

private:
  void _updateValues(const Output<real>& currentValue,
                     const Output<real>& currentVelocity,
                     const Output<real>& currentAcceleration) override;
};

} // namespace Animations
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_ANIMATIONS_LINEARINTERPOLATIONANIMATION_H
