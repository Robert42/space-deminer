#ifndef FRAMEWORK_GUI_ANIMATIONS_FADEANIMATION_H
#define FRAMEWORK_GUI_ANIMATIONS_FADEANIMATION_H

#include "../animation.h"

namespace Framework {
namespace Gui {
namespace Animations {


class FadeAnimation final : public Animation
{
public:
  typedef std::shared_ptr<FadeAnimation> Ptr;

public:
  Signals::Trackable trackableFirst;
  Signals::Trackable trackableSecond;
  Signals::Trackable trackableInterpolation;
  const bool finishAfterInterpolation : 1;

private:
  Animation::Ptr _firstAnimation;
  Animation::Ptr _secondAnimation;
  Animation::Ptr _interpolationAnimation;

private:
  FadeAnimation(const Animation::Ptr& firstAnimation,
                const Animation::Ptr& secondAnimation,
                const Animation::Ptr& interpolationAnimation,
                bool finishAfterInterpolation);

public:
  ~FadeAnimation();

public:
  /**
   * @brief create
   * @param firstAnimation
   * @param secondAnimation
   * @param interpolationAnimation
   * @param finishAfterInterpolation
   *   - if true, the fading animation will stop as soon as the second animation of the interpolation stops
   *   - if false, the fading animation will only stop, when the second animatino has stopped
   * @return
   */
  static Ptr create(const Animation::Ptr& firstAnimation,
                    const Animation::Ptr& secondAnimation,
                    const Animation::Ptr& interpolationAnimation,
                    bool finishAfterInterpolation);
  /**
   * @brief create
   * @param firstAnimation
   * @param secondAnimation
   * @param interpolation
   * @param finishAfterInterpolation
   *   - if true, the fading animation will stop as soon as the second animation of the interpolation stops
   *   - if false, the fading animation will only stop, when the second animatino has stopped
   * @return
   */
  static Ptr create(const Animation::Ptr& firstAnimation,
                    const Animation::Ptr& secondAnimation,
                    FadeInterpolation interpolation,
                    real interpolationDuration,
                    real interpolationDelay,
                    const Animation::Ptr& timeAnimation,
                    bool finishAfterInterpolation);

  const Animation::Ptr& firstAnimation();
  const Animation::Ptr& secondAnimation();
  const Animation::Ptr& interpolationAnimation();

private:
  void _updateValues(const Output<real>& currentValue,
                     const Output<real>& currentVelocity,
                     const Output<real>& currentAcceleration) override;

  Animation::Ptr _garbageCollect(const Animation::Ptr& self) override;

  void setFirstAnimation(const Animation::Ptr& firstAnimation);
  void setSecondAnimation(const Animation::Ptr& secondAnimation);
  void setInterpolationAnimation(const Animation::Ptr& interpolationAnimation);
};


} // namespace Animations
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_ANIMATIONS_FADEANIMATION_H
