#ifndef FRAMEWORK_GUI_ANIMATIONS_EASYINANDOUTINTERPOLATION_H
#define FRAMEWORK_GUI_ANIMATIONS_EASYINANDOUTINTERPOLATION_H

#include "base-interpolation-animation.h"

namespace Framework {
namespace Gui {
namespace Animations {

class EasyInAndOutInterpolation : public BaseInterpolation
{
public:
  typedef std::shared_ptr<EasyInAndOutInterpolation> Ptr;

private:
  EasyInAndOutInterpolation(const Animation::Ptr& timeAnimation, real totalInterpolationTime);
  EasyInAndOutInterpolation(real totalInterpolationTime);

public:
  static Ptr create(const Animation::Ptr& timeAnimation, real totalInterpolationTime);
  static Ptr create(real totalInterpolationTime);

private:
  void _updateValues(const Output<real>& currentValue,
                     const Output<real>& currentVelocity,
                     const Output<real>& currentAcceleration) override;
};

} // namespace Animations
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_ANIMATIONS_EASYINANDOUTINTERPOLATION_H
