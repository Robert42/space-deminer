#include "timeline-animation.h"

namespace Framework {
namespace Gui {
namespace Animations {


TimelineAnimation::TimelineAnimation()
  : _time(-1.f)
{
  updateValues();
}

TimelineAnimation::Ptr TimelineAnimation::create()
{
  return Ptr(new TimelineAnimation);
}


void TimelineAnimation::increaseTime(real timeStep)
{
  setTime(this->time() + timeStep);
}

void TimelineAnimation::setTime(real time)
{
  if(time > this->time())
  {
    this->_time = time;
    updateValues();
  }
}

real TimelineAnimation::time() const
{
  return _time;
}


void TimelineAnimation::_updateValues(const Output<real> &currentValue,
                                      const Output<real> &currentVelocity,
                                      const Output<real> &currentAcceleration)
{
  currentValue.value = time();
  currentVelocity.value = 0.f;
  currentAcceleration.value = 0.f;
}


} // namespace Animations
} // namespace Gui
} // namespace Framework
