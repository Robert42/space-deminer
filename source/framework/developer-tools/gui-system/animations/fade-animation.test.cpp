#include "fade-animation.h"
#include "timeline-animation.h"
#include "constant-animation.h"

#include "../animation-slot-ids.h"

namespace Framework {
namespace Gui {
namespace Animations {


TEST(framework_gui_animations_FallAnimation, destroy_animation_with_time_animation_sending_signal)
{
  for(int i=0; i<1000; ++i)
  {
    TimelineAnimation::Ptr time = TimelineAnimation::create();

    for(int i=0; i<100; ++i)
    {
      time->increaseTime(0.1);
    }

    {
      AnimationMap animationMap;

      animationMap.setCurrentUnanimatedValue(AnimationSlots::RenderState::mouseHover, 0);
      animationMap.setCurrentUnanimatedValue(AnimationSlots::RenderState::mouseHover, 1);
      animationMap.fadeToDefault(AnimationSlots::RenderState::mouseHover,
                                 0.25f,
                                 Animation::FadeInterpolation::EASY_IN,
                                 0.f,
                                 time);

      for(int i=0; i<100; ++i)
      {
        time->increaseTime(0.1);
      }
    }

    for(int i=0; i<100; ++i)
    {
      time->increaseTime(0.1);
    }
  }
}


TEST(framework_gui_animations_FallAnimation, finished_signal)
{
  Animation::Ptr a, b;

  const Gui::Animations::TimelineAnimation::Ptr timeAnimation = Gui::Animations::TimelineAnimation::create();
  a = ConstantAnimation::create(1.f);
  b = ConstantAnimation::create(0.f);
  FadeAnimation::Ptr fadeAnimation = Gui::Animations::FadeAnimation::create(a, b,
                                                                            Animation::FadeInterpolation::EASY_IN_AND_OUT,
                                                                            2.f,
                                                                            4.f,
                                                                            timeAnimation,
                                                                            true);

  bool interpolationFinished = false;
  bool fadingFinished = false;

  fadeAnimation->signalFinished().connect([&](){fadingFinished=true;}).trackManually();
  fadeAnimation->interpolationAnimation()->signalFinished().connect([&](){interpolationFinished=true;}).trackManually();

  EXPECT_FALSE(interpolationFinished);
  EXPECT_FALSE(fadingFinished);

  for(real t : range(0.f, 10.f, 0.01f))
  {
    timeAnimation->setTime(t);
  }

  EXPECT_TRUE(interpolationFinished);
  EXPECT_TRUE(fadingFinished);
}



} // namespace Animations
} // namespace Gui
} // namespace Framework
