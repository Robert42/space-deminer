#include "finish-animation.h"

namespace Framework {
namespace Gui {
namespace Animations {


FinishAnimation::FinishAnimation(const Animation::Ptr& animation,
                                 const Animation::Ptr& triggerAnimation,
                                 real triggerValue)
  : BaseAnimationFilter(animation),
    triggerAnimation(triggerAnimation),
    triggerValue(triggerValue)
{
  triggerAnimation->signalFinished().connect(std::bind(&FinishAnimation::setFinished, this)).track(this->trackable);
  triggerAnimation->signalGarbageCollectionRequested().connect(std::bind(&FinishAnimation::sendSignalGarbageCollectionRequested, this)).track(this->trackable);
}

FinishAnimation::~FinishAnimation()
{
  this->trackable.clear();
}


FinishAnimation::Ptr FinishAnimation::create(const Animation::Ptr &animation,
                                             real waitTime)
{
  Animation::Ptr totalApplicationTime = Animation::totalApplicationTimeAnimation();

  return create(animation, totalApplicationTime, waitTime + totalApplicationTime->currentValue());
}

FinishAnimation::Ptr FinishAnimation::create(const Animation::Ptr& animation,
                                             const Animation::Ptr& triggerAnimation,
                                             real triggerValue)
{
  return Ptr(new FinishAnimation(animation,
                                 triggerAnimation,
                                 triggerValue));
}

void FinishAnimation::_updateValues(const Output<real>& currentValue,
                                    const Output<real>& currentVelocity,
                                    const Output<real>& currentAcceleration)
{
  currentValue.value = this->filteredAnimation()->currentValue();
  currentVelocity.value = this->filteredAnimation()->currentVelocity();
  currentAcceleration.value = this->filteredAnimation()->currentAcceleration();

  checkForTrigger();
}

void FinishAnimation::checkForTrigger()
{
  if(triggerAnimation->currentValue() >= this->triggerValue)
    this->setFinished();
}


} // namespace Animations
} // namespace Gui
} // namespace Framework
