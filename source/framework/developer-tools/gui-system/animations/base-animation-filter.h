#ifndef FRAMEWORK_GUI_ANIMATIONS_BASEANIMATIONFILTER_H
#define FRAMEWORK_GUI_ANIMATIONS_BASEANIMATIONFILTER_H

#include "../animation.h"

namespace Framework {
namespace Gui {
namespace Animations {


class BaseAnimationFilter : public Animation
{
public:
  typedef std::shared_ptr<BaseAnimationFilter> Ptr;

private:
  Signals::Trackable _trackFilteredAnimation;
  Animation::Ptr _filteredAnimation;

public:
  BaseAnimationFilter(const Animation::Ptr& filteredAnimation);
  ~BaseAnimationFilter();

public:
  const Animation::Ptr& filteredAnimation();

private:
  void setFilteredAnimation(const Animation::Ptr& filteredAnimation);

  Animation::Ptr _garbageCollect(const Animation::Ptr &self) final override;
};


} // namespace Animations
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_ANIMATIONS_BASEANIMATIONFILTER_H
