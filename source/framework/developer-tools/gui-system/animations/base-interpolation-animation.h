#ifndef FRAMEWORK_GUI_ANIMATIONS_BASEINTERPOLATION_H
#define FRAMEWORK_GUI_ANIMATIONS_BASEINTERPOLATION_H

#include "base-animation-filter.h"

namespace Framework {
namespace Gui {
namespace Animations {

class BaseInterpolation : public BaseAnimationFilter
{
public:
  typedef std::shared_ptr<BaseInterpolation> Ptr;

private:
  const real _invTime;

protected:
  BaseInterpolation(const Animation::Ptr& timeAnimation, real totalInterpolationTime);
  BaseInterpolation(real totalInterpolationTime);

public:
  real time();
  real unclamptedTime();
  real timeVelocity();
  real timeAcceleration();

private:
  static real _invertTime(real time);
};

} // namespace Animations
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_ANIMATIONS_BASEINTERPOLATION_H
