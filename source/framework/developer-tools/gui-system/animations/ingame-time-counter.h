#ifndef FRAMEWORK_GUI_ANIMATIONS_INGAMETIMECOUNTER_H
#define FRAMEWORK_GUI_ANIMATIONS_INGAMETIMECOUNTER_H

#include "../animation.h"

namespace Framework {
namespace Gui {
namespace Animations {


class IngameTimeCounter final : public Animation
{
public:
  typedef std::shared_ptr<IngameTimeCounter> Ptr;

public:
  Signals::Trackable trackable;

private:
  real totalTime;

private:
  IngameTimeCounter();

public:
  ~IngameTimeCounter();

public:
  static Ptr create();

private:
  void _updateValues(const Output<real>& currentValue,
                     const Output<real>& currentVelocity,
                     const Output<real>& currentAcceleration) override;

  void handleTick(real time);
};


} // namespace Animations
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_ANIMATIONS_INGAMETIMECOUNTER_H
