#include "constant-animation.h"

namespace Framework {
namespace Gui {
namespace Animations {


ConstantAnimation::ConstantAnimation(real value)
  : value(value)
{
  updateValues();
}


ConstantAnimation::Ptr ConstantAnimation::create(real value)
{
  return Ptr(new ConstantAnimation(value));
}

void ConstantAnimation::_updateValues(const Output<real>& currentValue,
                                      const Output<real>& currentVelocity,
                                      const Output<real>& currentAcceleration)
{
  currentValue.value = value;
  currentVelocity.value = 0.f;
  currentAcceleration.value = 0.f;
}


} // namespace Animations
} // namespace Gui
} // namespace Framework
