#include "easy-out-interpolation.h"

namespace Framework {
namespace Gui {
namespace Animations {


EasyOutInterpolation::EasyOutInterpolation(const Animation::Ptr& timeAnimation,
                                           real totalInterpolationTime)
  : BaseInterpolation(timeAnimation, totalInterpolationTime)
{
}

EasyOutInterpolation::EasyOutInterpolation(real totalInterpolationTime)
  : BaseInterpolation(totalInterpolationTime)
{
}


EasyOutInterpolation::Ptr EasyOutInterpolation::create(const Animation::Ptr& timeAnimation, real totalInterpolationTime)
{
  return Ptr(new EasyOutInterpolation(timeAnimation, totalInterpolationTime));
}

EasyOutInterpolation::Ptr EasyOutInterpolation::create(real totalInterpolationTime)
{
  return Ptr(new EasyOutInterpolation(totalInterpolationTime));
}


void EasyOutInterpolation::_updateValues(const Output<real>& currentValue,
                                         const Output<real>& currentVelocity,
                                         const Output<real>& currentAcceleration)
{
  real x = time();
  currentValue.value = x*x;
  currentVelocity.value = 2*x;
  currentAcceleration.value = x;
}


} // namespace Animations
} // namespace Gui
} // namespace Framework
