#ifndef FRAMEWORK_GUI_ANIMATIONS_FINISHANIMATION_H
#define FRAMEWORK_GUI_ANIMATIONS_FINISHANIMATION_H

#include "base-animation-filter.h"

namespace Framework {
namespace Gui {
namespace Animations {

class FinishAnimation final : public BaseAnimationFilter
{
public:
  typedef std::shared_ptr<FinishAnimation> Ptr;

public:
  Signals::Trackable trackable;
  const Animation::Ptr triggerAnimation;
  const real triggerValue;

private:
  FinishAnimation(const Animation::Ptr& animation, const Animation::Ptr& triggerAnimation, real triggerValue);

public:
  ~FinishAnimation();

  static Ptr create(const Animation::Ptr& animation, real waitTime);
  static Ptr create(const Animation::Ptr& animation, const Animation::Ptr& triggerAnimation, real triggerValue);

private:
  void _updateValues(const Output<real>& currentValue,
                     const Output<real>& currentVelocity,
                     const Output<real>& currentAcceleration) override;

  void checkForTrigger();
};

} // namespace Animations
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_ANIMATIONS_FINISHANIMATION_H
