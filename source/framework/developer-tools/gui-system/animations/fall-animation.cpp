#include "fall-animation.h"

namespace Framework {
namespace Gui {
namespace Animations {

FreeFallAnimation::PhysicDescription::PhysicDescription()
  : PhysicDescription(InterpolationPreset::STICK_TO_GROUND)
{
}

FreeFallAnimation::PhysicDescription::PhysicDescription(InterpolationPreset::Value preset)
  : PhysicDescription(InterpolationPreset(preset))
{
}

FreeFallAnimation::PhysicDescription::PhysicDescription(const InterpolationPreset& preset)
{
  ground = 1.f;
  dampingFactor = 1.f;
  bounceFactor = 1.f;

  switch(preset.value)
  {
  case InterpolationPreset::BOUNCE:
    maxContactsWithTarget = 3;
    bounceFactor = -0.25f;
    break;
  case InterpolationPreset::DAMPING:
    maxContactsWithTarget = 5;
    dampingFactor = 0.5f;
    bounceFactor = 0.75f;
    break;
  case InterpolationPreset::STICK_TO_GROUND:
  default:
    maxContactsWithTarget = 0;
    break;
  }
}


FreeFallAnimation::FreeFallAnimation(const Animation::Ptr& time,
                                     real startValue,
                                     real startVelocity,
                                     real gravity,
                                     const PhysicDescription& physicDescription)
  : BaseAnimationFilter(time),
    _lastTimeValue(this->time()),
    _touchedGroundLastFrame(false),
    maxContactsWithTarget(physicDescription.maxContactsWithTarget),
    prevValue(startValue),
    prevVelocity(startVelocity),
    dampingFactor(1.f),
    gravity(abs(gravity)),
    physicDescription(physicDescription)
{
  time->signalStarted().connect([this](){this->_lastTimeValue = this->time();}).track(this->trackable);
}

FreeFallAnimation::~FreeFallAnimation()
{
  this->trackable.clear();
}

FreeFallAnimation::Ptr FreeFallAnimation::createByGravity(const Animation::Ptr& time,
                                                          real startValue,
                                                          real startVelocity,
                                                          real gravity,
                                                          const PhysicDescription& physicDescription)
{
  return Ptr(new FreeFallAnimation(time, startValue, startVelocity, gravity, physicDescription));
}

FreeFallAnimation::Ptr FreeFallAnimation::createByFallDuration(const Animation::Ptr& time,
                                                               real startValue,
                                                               real startVelocity,
                                                               real fallDuration,
                                                               const PhysicDescription& physicDescription)
{
  return createByGravity(time,
                         startValue,
                         startVelocity,
                         gravityForFallDuration(startValue,
                                                startVelocity,
                                                fallDuration,
                                                physicDescription.ground),
                         physicDescription);
}

FreeFallAnimation::Ptr FreeFallAnimation::createByFallDuration(const Animation::Ptr& time,
                                                               const Animation::Ptr& previousAnimation,
                                                               real fallDuration,
                                                               const PhysicDescription& physicDescription)
{
  return createByFallDuration(time,
                              previousAnimation->currentValue(),
                              previousAnimation->currentVelocity(),
                              fallDuration,
                              physicDescription);
}

real FreeFallAnimation::time()
{
  return filteredAnimation()->currentValue();
}

real FreeFallAnimation::gravityForFallDuration(real startValue,
                                               real startVelocity,
                                               real fallDuration,
                                               real ground)
{
  return 2.f * (ground-startVelocity*fallDuration-startValue) / sq(fallDuration);
}

void FreeFallAnimation::_updateValues(const Output<real>& currentValue,
                                      const Output<real>& currentVelocity,
                                      const Output<real>& currentAcceleration)
{
  real deltaTime = time() - _lastTimeValue;
  _lastTimeValue = time();

  if(!started())
    deltaTime = 0;

  const real ground = physicDescription.ground;

  real& value = currentValue.value;
  real& velocity = currentVelocity.value;
  real& acceleration = currentAcceleration.value;

  acceleration = value<ground ? gravity : -gravity;
  velocity = prevVelocity + acceleration * deltaTime;
  value = prevValue + velocity * deltaTime;

  // Within a second, this line will be executed $n:=\frac{1}{deltaTime}$ times. Find a factor $x$, so $x^n=dampingFactor$, otherwise
  // something different than the dampingFactor will be multiplied over the time of a second. $x^n=dampingFactor <=> x=dampingFactor^(1/n)$
  // 1/n = deltaTime
  velocity *= pow(dampingFactor, deltaTime);

  bool touchedGround = sign(value-ground)!=sign(prevValue-ground) || value == ground;

  if(touchedGround && !_touchedGroundLastFrame)
  {
    _touchedGroundLastFrame = true;

    value = ground;

    if(maxContactsWithTarget==0)
    {
      setFinished();
    }else
    {
      maxContactsWithTarget--;

      velocity *= physicDescription.bounceFactor;

      dampingFactor = physicDescription.dampingFactor;
    }
  }else
  {
    _touchedGroundLastFrame = false;
  }

  this->prevVelocity = velocity;
  this->prevValue = value;
}


} // namespace Animations
} // namespace Gui
} // namespace Framework
