#include "fade-animation.h"

#include "linear-interpolation-animation.h"
#include "easy-in-and-out-interpolation.h"
#include "easy-in-interpolation.h"
#include "easy-out-interpolation.h"
#include "delay-animation.h"

namespace Framework {
namespace Gui {
namespace Animations {


FadeAnimation::FadeAnimation(const Animation::Ptr& firstAnimation,
                             const Animation::Ptr& secondAnimation,
                             const Animation::Ptr& interpolationAnimation,
                             bool finishAfterInterpolation)
  : finishAfterInterpolation(finishAfterInterpolation),
    _firstAnimation(Animation::createFallback()),
    _secondAnimation(Animation::createFallback()),
    _interpolationAnimation(Animation::createFallback())
{
  setFirstAnimation(firstAnimation);
  setSecondAnimation(secondAnimation);
  setInterpolationAnimation(interpolationAnimation);
}

FadeAnimation::~FadeAnimation()
{
  this->trackableFirst.clear();
  this->trackableSecond.clear();
  this->trackableInterpolation.clear();
}

FadeAnimation::Ptr FadeAnimation::create(const Animation::Ptr& firstAnimation,
                                         const Animation::Ptr& secondAnimation,
                                         const Animation::Ptr& interpolationAnimation,
                                         bool finishAfterInterpolation)
{
  return Ptr(new FadeAnimation(firstAnimation,
                               secondAnimation,
                               interpolationAnimation,
                               finishAfterInterpolation));
}

FadeAnimation::Ptr FadeAnimation::create(const Animation::Ptr& firstAnimation,
                                         const Animation::Ptr& secondAnimation,
                                         FadeInterpolation interpolation,
                                         real interpolationDuration,
                                         real interpolationDelay,
                                         const Animation::Ptr& timeAnimation,
                                         bool finishAfterInterpolation)
{
  return Ptr(new FadeAnimation(firstAnimation,
                               secondAnimation,
                               Animation::_createInterpolationAnimation(interpolationDuration,
                                                                        interpolation,
                                                                        interpolationDelay,
                                                                        timeAnimation),
                               finishAfterInterpolation));
}

void FadeAnimation::_updateValues(const Output<real>& currentValue,
                                  const Output<real>& currentVelocity,
                                  const Output<real>& currentAcceleration)
{
  const Animation* a = firstAnimation().get();
  const Animation* b = secondAnimation().get();
  const Animation* i = interpolationAnimation().get();

  real a0 = a->currentValue();
  real b0 = b->currentValue();
  real i0 = i->currentValue();
  real a1 = a->currentVelocity();
  real b1 = b->currentVelocity();
  real i1 = i->currentVelocity();
  real a2 = a->currentAcceleration();
  real b2 = b->currentAcceleration();
  real i2 = i->currentAcceleration();

  currentValue.value = a0 + i0 * (b0 - a0);
  currentVelocity.value = a1 + i0 * (b1 - a1) + i1 * (b0 - a0);
  currentAcceleration.value = a2 + i0 * (b2 - a2) + 2 * i1 * (b1 - a1) + i2 * (b0 - a0);
}

Animation::Ptr FadeAnimation::_garbageCollect(const Animation::Ptr& self)
{
  setFirstAnimation(garbageCollect(this->firstAnimation()));
  setSecondAnimation(garbageCollect(this->secondAnimation()));
  setInterpolationAnimation(garbageCollect(this->interpolationAnimation()));

  if(finishAfterInterpolation)
  {
    if(this->firstAnimation()->finished())
      return this->secondAnimation();
    else
      return self;
  }else
  {
    if(this->firstAnimation()->finished() || this->interpolationAnimation()->finished())
      return this->secondAnimation();
    else
      return self;
  }
}

void FadeAnimation::setFirstAnimation(const Animation::Ptr& firstAnimation)
{
  if(this->firstAnimation() == firstAnimation)
    return;

  trackableFirst.clear();

  this->_firstAnimation = firstAnimation;

  firstAnimation->signalUpdated().connect(std::bind(&Animation::updateValues, this)).track(this->trackableFirst);
  firstAnimation->signalGarbageCollectionRequested().connect(std::bind(&FadeAnimation::sendSignalGarbageCollectionRequested, this)).track(this->trackableFirst);
  firstAnimation->signalFinished().connect(std::bind(&FadeAnimation::sendSignalGarbageCollectionRequested, this)).track(this->trackableFirst);
}

void FadeAnimation::setSecondAnimation(const Animation::Ptr& secondAnimation)
{
  if(this->secondAnimation() == secondAnimation)
    return;

  trackableSecond.clear();

  this->_secondAnimation = secondAnimation;

  if(secondAnimation->finished())
    setFinished();
  secondAnimation->signalUpdated().connect(std::bind(&Animation::updateValues, this)).track(this->trackableSecond);
  secondAnimation->signalGarbageCollectionRequested().connect(std::bind(&FadeAnimation::sendSignalGarbageCollectionRequested, this)).track(this->trackableSecond);
  secondAnimation->signalFinished().connect(std::bind(&Animation::setFinished, this)).track(this->trackableSecond);
}

void FadeAnimation::setInterpolationAnimation(const Animation::Ptr& interpolationAnimation)
{
  if(this->interpolationAnimation() == interpolationAnimation)
    return;

  trackableInterpolation.clear();

  this->_interpolationAnimation = interpolationAnimation;

  interpolationAnimation->signalUpdated().connect(std::bind(&Animation::updateValues, this)).track(this->trackableInterpolation);
  interpolationAnimation->signalGarbageCollectionRequested().connect(std::bind(&FadeAnimation::sendSignalGarbageCollectionRequested, this)).track(this->trackableInterpolation);
  if(finishAfterInterpolation)
  {
    interpolationAnimation->signalFinished().connect(std::bind(&Animation::setFinished, this)).track(this->trackableInterpolation);
    if(interpolationAnimation->finished())
      setFinished();
  }else
  {
    interpolationAnimation->signalFinished().connect(std::bind(&FadeAnimation::sendSignalGarbageCollectionRequested, this)).track(this->trackableInterpolation);
    if(interpolationAnimation->finished())
      sendSignalGarbageCollectionRequested();
  }
}

const Animation::Ptr& FadeAnimation::firstAnimation()
{
  return _firstAnimation;
}

const Animation::Ptr& FadeAnimation::secondAnimation()
{
  return _secondAnimation;
}

const Animation::Ptr& FadeAnimation::interpolationAnimation()
{
  return _interpolationAnimation;
}


} // namespace Animations
} // namespace Gui
} // namespace Framework
