#include "easy-in-interpolation.h"

namespace Framework {
namespace Gui {
namespace Animations {


EasyInInterpolation::EasyInInterpolation(const Animation::Ptr& timeAnimation,
                                         real totalInterpolationTime)
  : BaseInterpolation(timeAnimation, totalInterpolationTime)
{
}

EasyInInterpolation::EasyInInterpolation(real totalInterpolationTime)
  : BaseInterpolation(totalInterpolationTime)
{
}


EasyInInterpolation::Ptr EasyInInterpolation::create(const Animation::Ptr& timeAnimation, real totalInterpolationTime)
{
  return Ptr(new EasyInInterpolation(timeAnimation, totalInterpolationTime));
}

EasyInInterpolation::Ptr EasyInInterpolation::create(real totalInterpolationTime)
{
  return Ptr(new EasyInInterpolation(totalInterpolationTime));
}


void EasyInInterpolation::_updateValues(const Output<real>& currentValue,
                                        const Output<real>& currentVelocity,
                                        const Output<real>& currentAcceleration)
{
  real x = time();
  currentValue.value = x * (2-x);
  currentVelocity.value = 2-2*x;
  currentAcceleration.value = -2;
}


} // namespace Animations
} // namespace Gui
} // namespace Framework
