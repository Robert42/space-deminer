#ifndef FRAMEWORK_GUI_ANIMATIONS_FALLANIMATION_H
#define FRAMEWORK_GUI_ANIMATIONS_FALLANIMATION_H

#include "base-animation-filter.h"

namespace Framework {
namespace Gui {
namespace Animations {


class FreeFallAnimation : public BaseAnimationFilter
{
public:
  typedef std::shared_ptr<FreeFallAnimation> Ptr;

  struct PhysicDescription
  {
    BEGIN_ENUMERATION(InterpolationPreset,)
      STICK_TO_GROUND,
      BOUNCE,
      DAMPING
    ENUMERATION_BASIC_OPERATORS(InterpolationPreset)
    END_ENUMERATION;

    int maxContactsWithTarget;
    real dampingFactor;
    real bounceFactor;
    real ground;

    PhysicDescription(const InterpolationPreset& preset);
    PhysicDescription(InterpolationPreset::Value preset);
    PhysicDescription();
  };

private:
  real _lastTimeValue;
  bool _touchedGroundLastFrame : 1;
  int maxContactsWithTarget;

  real prevValue;
  real prevVelocity;
  real dampingFactor;

public:
  Signals::Trackable trackable;
  const real gravity;
  const PhysicDescription physicDescription;

public:
  FreeFallAnimation(const Animation::Ptr& time, real startValue, real startVelocity, real gravity, const PhysicDescription& physicDescription);
  ~FreeFallAnimation();

public:
  static Ptr createByGravity(const Animation::Ptr& time, real startValue, real startVelocity, real gravity, const PhysicDescription& physicDescription);
  static Ptr createByFallDuration(const Animation::Ptr& time, real startValue, real startVelocity, real fallDuration, const PhysicDescription& physicDescription);
  static Ptr createByFallDuration(const Animation::Ptr& time, const Animation::Ptr& previousAnimation, real fallDuration, const PhysicDescription& physicDescription);

  real time();

private:
  static real gravityForFallDuration(real startValue, real startVelocity, real fallDuration, real ground);

  void _updateValues(const Output<real>& currentValue,
                     const Output<real>& currentVelocity,
                     const Output<real>& currentAcceleration) override;
};


} // namespace Animations
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_ANIMATIONS_FALLANIMATION_H
