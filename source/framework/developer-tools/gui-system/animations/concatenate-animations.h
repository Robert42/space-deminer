#ifndef FRAMEWORK_GUI_ANIMATIONS_CONCATENATEANIMATIONS_H
#define FRAMEWORK_GUI_ANIMATIONS_CONCATENATEANIMATIONS_H

#include "../animation.h"

namespace Framework {
namespace Gui {
namespace Animations {


class ConcatenateAnimations final : public Animation
{
public:
  typedef std::shared_ptr<ConcatenateAnimations> Ptr;

private:
  QVector<Animation::Ptr> animations;

public:
  Signals::Trackable trackable;

private:
  ConcatenateAnimations(const QVector<Animation::Ptr>& animations);

public:
  ~ConcatenateAnimations();

  static Ptr create(const QVector<Animation::Ptr>& animations);

private:
  void _updateValues(const Output<real>& currentValue,
                     const Output<real>& currentVelocity,
                     const Output<real>& currentAcceleration) override;

  void tidyUp();

  Animation::Ptr _garbageCollect(const Animation::Ptr& self) override;
};


} // namespace Animations
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_ANIMATIONS_CONCATENATEANIMATIONS_H
