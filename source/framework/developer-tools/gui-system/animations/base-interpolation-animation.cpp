#include "base-interpolation-animation.h"
#include "delay-animation.h"

namespace Framework {
namespace Gui {
namespace Animations {


BaseInterpolation::BaseInterpolation(const Animation::Ptr& timeAnimation, real totalInterpolationTime)
  : BaseAnimationFilter(timeAnimation),
    _invTime(_invertTime(totalInterpolationTime))
{
}

BaseInterpolation::BaseInterpolation(real totalInterpolationTime)
  : BaseInterpolation(Animation::timeAnimationStartingNow(), totalInterpolationTime)
{
}


real BaseInterpolation::time()
{
  return clamp<real>(unclamptedTime(), 0.f, 1.f);
}

real BaseInterpolation::unclamptedTime()
{
  real t = filteredAnimation()->currentValue() * _invTime;

  if(t >= 1.f)
    setFinished();

  return t;
}

real BaseInterpolation::timeVelocity()
{
  real t = unclamptedTime();

  if(t < 0.f || t > 1.f)
    return 0.f;

  return filteredAnimation()->currentVelocity() * _invTime;
}

real BaseInterpolation::timeAcceleration()
{
  real t = unclamptedTime();

  if(t < 0.f || t > 1.f)
    return 0.f;

  return filteredAnimation()->currentAcceleration() * _invTime;
}

real BaseInterpolation::_invertTime(real time)
{
  if(time <= 0.f || std::isinf(time) || std::isnan(time))
    return 1.f;

  return 1.f / time;
}


} // namespace Animations
} // namespace Gui
} // namespace Framework
