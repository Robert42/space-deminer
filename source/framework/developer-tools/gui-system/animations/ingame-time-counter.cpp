#include "ingame-time-counter.h"

#include <framework/frame-signals.h>

namespace Framework {
namespace Gui {
namespace Animations {


IngameTimeCounter::IngameTimeCounter()
{
  totalTime = 0;
  FrameSignals::signalRenderingQueued().connect(std::bind(&IngameTimeCounter::handleTick, this, _1)).track(this->trackable);
}

IngameTimeCounter::~IngameTimeCounter()
{
  this->trackable.clear();
}

IngameTimeCounter::Ptr IngameTimeCounter::create()
{
  return Ptr(new IngameTimeCounter);
}


void IngameTimeCounter::_updateValues(const Output<real>& currentValue,
                                      const Output<real>& currentVelocity,
                                      const Output<real>& currentAcceleration)
{
  currentValue.value = totalTime;
  currentVelocity.value = 1.f;
  currentAcceleration.value = 0.f;
}

void IngameTimeCounter::handleTick(real time)
{
  totalTime += time;
  updateValues();
}


} // namespace Animations
} // namespace Gui
} // namespace Framework
