#include "easy-in-and-out-interpolation.h"

namespace Framework {
namespace Gui {
namespace Animations {


EasyInAndOutInterpolation::EasyInAndOutInterpolation(const Animation::Ptr& timeAnimation,
                                                     real totalInterpolationTime)
  : BaseInterpolation(timeAnimation, totalInterpolationTime)
{
}

EasyInAndOutInterpolation::EasyInAndOutInterpolation(real totalInterpolationTime)
  : BaseInterpolation(totalInterpolationTime)
{
}


EasyInAndOutInterpolation::Ptr EasyInAndOutInterpolation::create(const Animation::Ptr& timeAnimation, real totalInterpolationTime)
{
  return Ptr(new EasyInAndOutInterpolation(timeAnimation, totalInterpolationTime));
}

EasyInAndOutInterpolation::Ptr EasyInAndOutInterpolation::create(real totalInterpolationTime)
{
  return Ptr(new EasyInAndOutInterpolation(totalInterpolationTime));
}


void EasyInAndOutInterpolation::_updateValues(const Output<real>& currentValue,
                                              const Output<real>& currentVelocity,
                                              const Output<real>& currentAcceleration)
{
  real x = time();
  real x2 = x*x;
  real x3 = x2*x;
  currentValue.value = -2*x3+3*x2;
  currentVelocity.value = -6*x2+6*x;
  currentAcceleration.value = -12*x+6;
}


} // namespace Animations
} // namespace Gui
} // namespace Framework
