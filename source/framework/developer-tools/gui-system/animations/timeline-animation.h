#ifndef FRAMEWORK_GUI_ANIMATIONS_TIMELINEANIMATION_H
#define FRAMEWORK_GUI_ANIMATIONS_TIMELINEANIMATION_H

#include "../animation.h"

namespace Framework {
namespace Gui {
namespace Animations {

class TimelineAnimation final : public Animation
{
public:
  typedef std::shared_ptr<TimelineAnimation> Ptr;

private:
  real _time;

private:
  TimelineAnimation();

public:
  static Ptr create();

  void increaseTime(real timeStep);
  void setTime(real time);
  real time() const;

private:
  void _updateValues(const Output<real> &currentValue,
                     const Output<real> &currentVelocity,
                     const Output<real> &currentAcceleration) override;
};

} // namespace Animations
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_ANIMATIONS_TIMELINEANIMATION_H
