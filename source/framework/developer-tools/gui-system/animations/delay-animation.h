#ifndef FRAMEWORK_GUI_ANIMATIONS_DELAYANIMATION_H
#define FRAMEWORK_GUI_ANIMATIONS_DELAYANIMATION_H

#include "base-animation-filter.h"

namespace Framework {
namespace Gui {
namespace Animations {

class DelayAnimation final : public BaseAnimationFilter
{
public:
  typedef std::shared_ptr<DelayAnimation> Ptr;

public:
  const real delay;

private:
  DelayAnimation(const Animation::Ptr& timeAnimation, real delay);

public:
  static Ptr create(const Animation::Ptr& timeAnimation, real delay);
  static Ptr create(real delay);

private:
  void _updateValues(const Output<real>& currentValue,
                     const Output<real>& currentVelocity,
                     const Output<real>& currentAcceleration) override;
};

} // namespace Animations
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_ANIMATIONS_DELAYANIMATION_H
