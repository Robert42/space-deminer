#include "delay-animation.h"

namespace Framework {
namespace Gui {
namespace Animations {

DelayAnimation::DelayAnimation(const Animation::Ptr& timeAnimation, real delay)
  : BaseAnimationFilter(timeAnimation),
    delay(delay)
{
}

DelayAnimation::Ptr DelayAnimation::create(const Animation::Ptr& timeAnimation, real delay)
{
  return Ptr(new DelayAnimation(timeAnimation, delay));
}

DelayAnimation::Ptr DelayAnimation::create(real delay)
{
  return create(Animation::totalApplicationTimeAnimation(), delay);
}

void DelayAnimation::_updateValues(const Output<real>& currentValue,
                                   const Output<real>& currentVelocity,
                                   const Output<real>& currentAcceleration)
{
  const Animation::Ptr& timeAnimation = this->filteredAnimation();

  currentValue.value = timeAnimation->currentValue() - this->delay;
  currentVelocity.value = timeAnimation->currentVelocity();
  currentAcceleration.value = timeAnimation->currentAcceleration();
}


} // namespace Animations
} // namespace Gui
} // namespace Framework
