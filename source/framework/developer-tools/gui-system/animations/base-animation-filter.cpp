#include "base-animation-filter.h"

namespace Framework {
namespace Gui {
namespace Animations {


BaseAnimationFilter::BaseAnimationFilter(const Animation::Ptr& filteredAnimation)
  : _filteredAnimation(Animation::createFallback())
{
  setFilteredAnimation(filteredAnimation);
}

BaseAnimationFilter::~BaseAnimationFilter()
{
  this->_trackFilteredAnimation.clear();
}

const Animation::Ptr& BaseAnimationFilter::filteredAnimation()
{
  return _filteredAnimation;
}

void BaseAnimationFilter::setFilteredAnimation(const Animation::Ptr& filteredAnimation)
{
  if(this->_filteredAnimation == filteredAnimation)
    return;

  this->_trackFilteredAnimation.clear();

  _filteredAnimation = filteredAnimation;

  if(filteredAnimation->finished())
    setFinished();

  filteredAnimation->signalFinished().connect(std::bind(&Animation::setFinished, this)).track(this->_trackFilteredAnimation);
  filteredAnimation->signalUpdated().connect(std::bind(&Animation::updateValues, this)).track(this->_trackFilteredAnimation);
  filteredAnimation->signalGarbageCollectionRequested().connect(std::bind(&BaseAnimationFilter::sendSignalGarbageCollectionRequested, this)).track(this->_trackFilteredAnimation);
}


Animation::Ptr BaseAnimationFilter::_garbageCollect(const Animation::Ptr &self)
{
  setFilteredAnimation(garbageCollect(_filteredAnimation));

  return self;
}


} // namespace Animations
} // namespace Gui
} // namespace Framework
