#include "linear-interpolation-animation.h"

namespace Framework {
namespace Gui {
namespace Animations {


LinearInterpolationAnimation::LinearInterpolationAnimation(const Animation::Ptr& timeAnimation,
                                                           real totalInterpolationTime)
  : BaseInterpolation(timeAnimation, totalInterpolationTime)
{
}

LinearInterpolationAnimation::LinearInterpolationAnimation(real totalInterpolationTime)
  : BaseInterpolation(totalInterpolationTime)
{
}


LinearInterpolationAnimation::Ptr LinearInterpolationAnimation::create(const Animation::Ptr& timeAnimation, real totalInterpolationTime)
{
  return Ptr(new LinearInterpolationAnimation(timeAnimation, totalInterpolationTime));
}

LinearInterpolationAnimation::Ptr LinearInterpolationAnimation::create(real totalInterpolationTime)
{
  return Ptr(new LinearInterpolationAnimation(totalInterpolationTime));
}


void LinearInterpolationAnimation::_updateValues(const Output<real>& currentValue,
                                                 const Output<real>& currentVelocity,
                                                 const Output<real>& currentAcceleration)
{
  currentValue.value = time();
  currentVelocity.value = timeVelocity();
  currentAcceleration.value = timeAcceleration();
}


} // namespace Animations
} // namespace Gui
} // namespace Framework
