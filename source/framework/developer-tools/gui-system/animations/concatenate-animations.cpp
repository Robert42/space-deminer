#include "concatenate-animations.h"

namespace Framework {
namespace Gui {
namespace Animations {


ConcatenateAnimations::ConcatenateAnimations(const QVector<Animation::Ptr>& animations)
  : animations(animations)
{
  for(const Animation::Ptr& a : animations)
  {
    a->signalFinished().connect(std::bind(&ConcatenateAnimations::tidyUp, this)).track(this->trackable);
    a->signalGarbageCollectionRequested().connect(std::bind(&ConcatenateAnimations::sendSignalGarbageCollectionRequested, this)).track(this->trackable);
  }

  tidyUp();
}

ConcatenateAnimations::~ConcatenateAnimations()
{
  this->trackable.clear();
}

ConcatenateAnimations::Ptr ConcatenateAnimations::create(const QVector<Animation::Ptr>& animations)
{
  return Ptr(new ConcatenateAnimations(animations));
}

void ConcatenateAnimations::_updateValues(const Output<real>& currentValue,
                                          const Output<real>& currentVelocity,
                                          const Output<real>& currentAcceleration)
{
  tidyUp();
  if(animations.empty())
    return;

  Animation::Ptr a = animations.first();

  currentValue.value = a->currentValue();
  currentVelocity.value = a->currentVelocity();
  currentAcceleration.value = a->currentAcceleration();
}

Animation::Ptr ConcatenateAnimations::_garbageCollect(const Animation::Ptr& self)
{
  tidyUp();

  if(animations.size() == 1)
    return animations.last();
  else
    return self;
}

void ConcatenateAnimations::tidyUp()
{
  if(this->finished())
    return;

  QVector<Animation::Ptr> tidiedUpAnimations;
  tidiedUpAnimations.reserve(animations.size());

  for(Animation::Ptr a : animations)
  {
    if(a->finished())
      continue;

    Animation::Ptr newAnimation = garbageCollect(a);

    if(a != newAnimation)
    {
      newAnimation->signalFinished().connect(std::bind(&ConcatenateAnimations::tidyUp, this)).track(this->trackable);
      newAnimation->signalGarbageCollectionRequested().connect(std::bind(&ConcatenateAnimations::sendSignalGarbageCollectionRequested, this)).track(this->trackable);
    }
  }

  this->animations = tidiedUpAnimations;

  if(this->animations.empty())
    setFinished();
}

} // namespace Animations
} // namespace Gui
} // namespace Framework
