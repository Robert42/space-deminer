#include "window.h"
#include "tags.h"

#include "widgets/dummy-widget.h"
#include "base-window-container.h"

#include <base/geometry/ray.h>

namespace Framework {
namespace Gui {

Window::Window()
  : _roundAllocationHint(true)
{
  _appearance = Appearance::fallbackAppearance();
  _widgetModel = std::shared_ptr<WidgetModel>(new WidgetModel);
  _size = vec2(512, 256);
  _redrawSheduled = false;
  _reorderSheduled = true;
  _dirtyFlags = DirtyFlags::NONE;

  _widgetModel->signalWidgetAdded().connect(std::bind(&Window::invalidateKeyFocusedWidget, this)).track(this->trackable);
  _widgetModel->signalWidgetAdded().connect(std::bind(&Window::invalidateMouseOverWidget, this)).track(this->trackable);
  _widgetModel->signalWidgetWasRemoved().connect(std::bind(&Window::invalidateKeyFocusedWidget, this)).track(this->trackable);
  _widgetModel->signalWidgetWasRemoved().connect(std::bind(&Window::invalidateMouseOverWidget, this)).track(this->trackable);

  rootWidgetSlot.setWindow(this);
  setRootWidget(Widgets::DummyWidget::create());
}


Window::~Window()
{
  rootWidgetSlot.unsetWidget();
  rootWidgetSlot.unsetWindow();
  _widgetModel.reset();
  _rootWidget.reset();
}


const WidgetModel::Ptr& Window::widgetModel()
{
  return _widgetModel;
}


const vec2& Window::size() const
{
  return _size;
}


const Appearance::Ptr& Window::appearance()
{
  return _appearance;
}


void Window::setAppearance(const Appearance::Ptr& appearance)
{
  if(this->appearance() != appearance)
  {
    this->_appearance = appearance;
    _signalAppereanceChanged();
    rootWidget()->markThemeDirty();
  }
}


const Widget::Ptr& Window::rootWidget()
{
  assert(_rootWidget);
  return _rootWidget;
}


void Window::setRootWidget(const Widget::Ptr& widget)
{
  _unsetRootWidget();
  _rootWidget = widget;

  rootWidgetSlot.setWidget(widget);
  rootWidgetSlot.setWidgetBoundingRect(Rectangle<vec2>(vec2(0), size()));
  rootWidgetSlot.setWidgetZPosition(0.f);

  widget->rtti.tags.insert(Tags::windowRoot);

  widget->markChildAllocationDirty();
  widget->markChildZPositionDirty();
  widget->markThemeDirty();
  widget->markMinimumSizeDirty();
  widget->markOptimalSizeDirty();
  widget->markZIndexDirty();
}

void Window::_unsetRootWidget()
{
  if(_rootWidget)
  {
    _rootWidget->rtti.tags.remove(Tags::windowRoot);
    rootWidgetSlot.unsetWidget();
    _rootWidget.reset();
  }
}

void Window::unsetRootWidget()
{
  _unsetRootWidget();

  setRootWidget(Widgets::DummyWidget::create());
}


Signals::Signal<void()>& Window::signalRedrawSheduled()
{
  return _signalRedrawSheduled;
}


Signals::Signal<void()>& Window::signalAppereanceChanged()
{
  return _signalAppereanceChanged;
}


void Window::widgetMarkedDirty(DirtyFlags dirtyFlags)
{
  _dirtyFlags |= dirtyFlags;
}


void Window::sheduleRedraw()
{
  if(!_redrawSheduled)
  {
    _redrawSheduled = true;
    _signalRedrawSheduled();
  }
}


void Window::sheduleReorderRenderOrder()
{
  if(!_reorderSheduled)
  {
    _reorderSheduled = true;
    sheduleRedraw();
  }
}


bool Window::redrawSheduled() const
{
  return _redrawSheduled;
}


void Window::updateAllWidgets()
{
  allWidgetsUpdatedAndSorted(false);
}


void Window::updateAllVisibleWidgets()
{
  allWidgetsUpdatedAndSorted(true);
}


void Window::setSize(const vec2& size)
{
  _size = size;

  Rectangle<vec2> rectangle(vec2(0), size);

  if(rectangle != rootWidget()->boundingRect())
    rootWidgetSlot.setWidgetBoundingRect(rectangle);

  invalidateMouseOverWidget();
  invalidateKeyFocusedWidget();
}

Optional<Widget::Ptr> widgetWhichCanHaveFocus(const Optional<Widget::Ptr>& widget)
{
  if(!widget)
    return nothing;

  const Widget::Ptr& w = *widget;

  if(w->canHaveKeyFocus())
    return widget;
  else
    return nothing;
}

Optional<Widget::Ptr> Window::defaultWidget()
{
  return widgetWhichCanHaveFocus(findFirstWidgetWithTag(Tags::window_defaultWidget));
}

Optional<Widget::Ptr> Window::defaultEscapeWidget()
{
  return widgetWhichCanHaveFocus(findFirstWidgetWithTag(Tags::window_defaultEscapeWidget));
}

Optional<Widget::Ptr> Window::widgetWithKeyFocus()
{
  assert(!_widgetWithKeyFocus || (*_widgetWithKeyFocus)->hasKeyFocus());
  return _widgetWithKeyFocus;
}

Optional<Widget::FocusHint> Window::keyFocusPriority()
{
  return _keyFocusPriority;
}

void Window::askForKeyFocus(const Widget::Ptr& widget, const Widget::FocusHint& focusHint)
{
  if(_windowContainer)
  {
    BaseWindowContainer* windowContainer = *_windowContainer;

    windowContainer->_askForKeyFocus(widget, focusHint);

    if(widgetWithKeyFocus() && *widgetWithKeyFocus() == widget)
      _keyFocusPriority = focusHint;
  }
}

void Window::_tagWidgetAsKeyFocused(Widget* widget)
{
  assert(widget->window() && widget->window()==this);
  Optional<Widget::Ptr> optionalWidget = widget->asSmartPtr();

  assert(optionalWidget);

  if(!optionalWidget)
    return;

  Widget::Ptr w = *optionalWidget;

  _widgetWithKeyFocus = w;
}

void Window::_untagWidgetAsKeyFocused(Widget* widget)
{
  if(_widgetWithKeyFocus && _widgetWithKeyFocus->get()==widget)
  {
    _widgetWithKeyFocus.reset();
    _keyFocusPriority.reset();
  }
}

Optional<Widget::Ptr> Window::findFirstKeyFocusableWidget()
{
  Optional<Widget::Ptr> result = defaultWidget();

  if(!result)
  {
    rootWidget()->visitWholeHierarchy([&result](const Widget::Ptr& w){
      if(w->canHaveKeyFocus())
      {
        result = w;
        return true;
      }

      return false;
    });
  }
  return result;
}

Optional<Widget::Ptr> Window::findFirstWidgetWithTag(Tag tag)
{
  Optional<Widget::Ptr> result;
  rootWidget()->visitWholeHierarchy([&result, tag](const Widget::Ptr& w){
    if(w->rtti.tags.contains(tag))
    {
      result = w;
      return true;
    }

    return false;
  });
  return result;
}

QSet<Widget::Ptr> Window::findWidgetsWithTag(Tag tag)
{
  QSet<Widget::Ptr> result;
  rootWidget()->visitWholeHierarchy([&result, tag](const Widget::Ptr& w){
    if(w->rtti.tags.contains(tag))
      result.insert(w);

    return false;
  });
  return result;
}

bool Window::isAnyWidgetAtPoint(const vec2& point) const
{
  for(const Widget::Ptr& widget : _widgetModel->widgetsAtPoint(point))
    if(widget->isWidgetAtPoint(point))
      return true;
  return false;
}

bool Window::isAnyWidgetAtRay(const Geometry::Ray& ray) const
{
  for(const Widget::Ptr& widget : _widgetModel->widgetsAtRay(ray))
  {
    Optional<vec3> intersection = ray.intersectionWithZPlane(widget->zPosition());

    if(intersection && widget->isWidgetAtPoint(intersection->xy()))
      return true;
  }
  return false;
}

void Window::invalidateMouseOverWidget()
{
  _signalInvalidatedMouseOverWidget();
}

void Window::invalidateKeyFocusedWidget()
{
  _signalInvalidatedKeyFocusedWidget();
}

Signals::Signal<void()>& Window::signalInvalidatedMouseOverWidget()
{
  return _signalInvalidatedMouseOverWidget;
}

Signals::Signal<void()>& Window::signalInvalidatedKeyFocusedWidget()
{
  return _signalInvalidatedKeyFocusedWidget;
}


Signals::Signal<void(const InputHandlers::MouseAction::Ptr&)>& Window::signalMouseActionStarted()
{
  return _signalMouseActionStarted;
}


QVector<Widget::Ptr> Window::allWidgetsUpdatedAndSorted(bool onlyVisible)
{
  WidgetSet widgetSet = widgetModel()->allWidgets();

  if(onlyVisible)
  {
    for(const Widget::Ptr& w : widgetSet)
    {
      if(!w->visible())
        widgetSet.remove(w);
    }
  }

  if(widgetSet.empty())
    return QVector<Widget::Ptr>();

  DirtyFlags dirtyFlags = this->_dirtyFlags;
  this->_dirtyFlags = DirtyFlags::NONE;


  if(_reorderSheduled)
  {
    rootWidget()->updateIndices(0);

    this->sortedWidgets = Widget::sortSetIntoVector(widgetSet);
  }else
  {
    assert(((DirtyFlags::CHILD_LAYOUT_INDEX |
             DirtyFlags::Z_INDEX |
             DirtyFlags::CHILD_Z_POSITION |
             DirtyFlags::SUBTREE_INDEX)
            & rootWidget()->dirtyFlags()) == DirtyFlags::NONE);
  }

  if(dirtyFlags != DirtyFlags::NONE)
  {
    // We're workong on a copy of sortedWidgets instead of this->sortedWidgets dirtectly to avoid
    // introducing bugs, in case one of called functions will change this->sortedWidgets itself
    QVector<Widget::Ptr> sortedWidgets = this->sortedWidgets;

    const DirtyFlags themeIsDirty = DirtyFlags::THEME;
    const DirtyFlags sizeRequestIsDirty = DirtyFlags::MINIMUM_SIZE |
                                          DirtyFlags::OPTIMAL_SIZE;
    const DirtyFlags locationIsDirty = DirtyFlags::CHILD_ALLOCATION |
                                       DirtyFlags::SCISSOR |
                                       DirtyFlags::CHILD_Z_POSITION;

    // Widgets with a low zindex first
    if((dirtyFlags & themeIsDirty) != DirtyFlags::NONE)
    {
      for(const Widget::Ptr& w : sortedWidgets)
      {
        w->updateTheme();
        w->_clearDirtyFlags(themeIsDirty);
      }
    }

    // Widgets with a high zindex first (as the minimum/optimal size of
    // containers depends on the minimum/optimal size of their children)
    if((dirtyFlags & sizeRequestIsDirty) != DirtyFlags::NONE)
    {
      for(int i=sortedWidgets.size()-1; i>=0; --i)
      {
        const Widget::Ptr& w = sortedWidgets[i];

        w->updateMinimumSize();
        w->updateOptimalSize();
        w->_clearDirtyFlags(sizeRequestIsDirty);
      }
    }

    // Widgets with a low zindex first
    if((dirtyFlags & locationIsDirty) != DirtyFlags::NONE)
    {
      for(const Widget::Ptr& w : sortedWidgets)
      {
        w->updateChildAllocation();
        w->updateScissor();
        w->updateChildZPosition();
        w->_clearDirtyFlags(locationIsDirty);
      }
    }
  }

  return sortedWidgets;
}

bool Window::roundAllocationHint() const
{
  return _roundAllocationHint;
}


void Window::startMouseAction(const InputHandlers::MouseAction::Ptr& mouseAction)
{
  _signalMouseActionStarted(mouseAction);
}

vec2 Window::currentMousePosition(real zPosition) const
{
  if(!_windowContainer)
    return vec2(0);

  BaseWindowContainer* windowContainer = *_windowContainer;

  return windowContainer->currentMousePosition(zPosition);
}


} // namespace Gui
} // namespace Framework
