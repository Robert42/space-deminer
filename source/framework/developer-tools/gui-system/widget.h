#ifndef FRAMEWORK_GUI_WIDGET_H
#define FRAMEWORK_GUI_WIDGET_H

#include "theme.h"
#include "dirty-flags.h"
#include "painter.h"
#include "types.h"
#include "input-handlers/focusable-input-handler.h"

#include <framework/developer-tools/editor/base/object-editor-2d/object.h>

#include <base/signals.h>
#include <base/optional.h>

namespace Framework {
namespace Gui {

class WidgetModel;
class Window;
class Container;
class Layout;

class Widget : public Editor::Base::ObjectEditor2D::Object
{
  RTTI_CLASS_MEMBER

public:
  friend class WidgetModel;

public:
  typedef std::shared_ptr<Widget> Ptr;
  typedef std::shared_ptr<const Widget> ConstPtr;
  typedef std::weak_ptr<Widget> WeakPtr;

  BEGIN_ENUMERATION(InputHandlerWrapperFlags,)
    NONE = 0,
    DRAW_STATE_CHANGE = 1,
    USE_LOCAL_COORDINATES = DRAW_STATE_CHANGE<<1,
    CALL_PARENTS_AS_FALLBACK = USE_LOCAL_COORDINATES<<1,
    UPDATE_KEY_FOCUS = CALL_PARENTS_AS_FALLBACK<<1,
    DEFAULT = UPDATE_KEY_FOCUS | DRAW_STATE_CHANGE | CALL_PARENTS_AS_FALLBACK
  ENUMERATION_BASIC_OPERATORS(InputHandlerWrapperFlags)
  ENUMERATION_FLAG_OPERATORS(InputHandlerWrapperFlags)
  END_ENUMERATION;

  BEGIN_ENUMERATION(FocusHint,)
    DEFAULT_WIDGET_SHOWN,
    NAVIGATION_BY_USER,
    USER,
    START_USER_INPUT,
    SECURITY,
  ENUMERATION_BASIC_OPERATORS(FocusHint)
  END_ENUMERATION;

  class Index
  {
  public:
    uint32 renderOrder;

    Index();
    Index(uint16 zOrder, uint16 layoutOrder);

    uint16 zOrder() const;
    uint16 layoutOrder() const;
    void setZOrder(uint16 order);
    void setLayoutOrder(uint16 order);

    bool operator<(const Index& other) const;
    bool operator>(const Index& other) const;
    bool operator<=(const Index& other) const;
    bool operator>=(const Index& other) const;
    bool operator==(const Index& other) const;
    bool operator!=(const Index& other) const;
  };

public:
  Signals::Trackable trackable;
  AnimationMap animationMap;

private:
  friend class Window;
  friend class Container;

  DirtyFlags _dirtyFlags;
  bool _includedToWidgetSlot : 1;

  bool _sensitive : 1;
  bool _localSensitive : 1;
  bool _parentIsSensitive : 1;

  bool _visible : 1;
  bool _shown : 1;
  bool _parentIsVisible : 1;

  Index _index;
  real _zPosition;

  Theme::Ptr _theme;

  Window* _window;
  Container* _parent;
  Layout* _layout;

  Optional< Rectangle<vec2> > _scissor;
  Optional< InputHandlers::FocusableInputHandler::Ptr > _inputHandler;

  vec2 _minimumSize, _optimalSize;

  Signals::CallableSignal<void()> _signalMinimumSizeChanged;
  Signals::CallableSignal<void()> _signalOptimalSizeChanged;
  Signals::CallableSignal<void()> _signalMoved;
  Signals::CallableSignal<void()> _signalMovedAlongZ;

  Signals::CallableSignal<void()> _signalVisibilityChanged;
  Signals::CallableSignal<void()> _signalSensitivityChanged;
  QSet<Signals::Connection> _parentConnections;

public:
  Widget();
  virtual ~Widget();

public:
  const Index& index() const;
  real zPosition() const;

  Optional<Window*> window();
  Optional<const Window*> window() const;
  Optional<Container*> parent();
  Optional<const Container*> parent() const;
  Optional<Layout*> layout();
  Optional<const Layout*> layout() const;
  Optional< std::shared_ptr<Container> > asContainer();
  Optional< std::shared_ptr<const Container> > asContainer() const;

  bool isContainer() const;

  const Optional< Rectangle<vec2> >& scissor() const;

  bool visible() const;
  void show();
  void hide();

  bool globallySensitive() const;

  bool canHaveKeyFocus() const;
  bool canHaveMouseFocus() const;

  bool hasKeyFocus() const;
  void askForKeyFocus(const FocusHint& focusHint);

  bool sensitive() const;
  void setSensitive(bool sensitive);
  const Optional< InputHandlers::FocusableInputHandler::Ptr >& inputHandler() const;
  void setInputHandler(const Optional< InputHandlers::FocusableInputHandler::Ptr >& inputHandler);
  void setInputHandlerWithWrapper(InputHandler::Ptr inputHandler,
                                  InputHandlerWrapperFlags flags = InputHandlerWrapperFlags::DEFAULT);

  const vec2& minimumSize() const;
  const vec2& optimalSize() const;
  void setMinimumSize(const vec2& minimumSize);
  void setOptimalSize(const vec2& optimalSize);

  const Theme::Ptr& theme();

  /** Plays an animation for this widget.
   *
   * Whether animations also are going to be played for child widgets, is decided by the theme.
   *
   * @param animationTags the tags used by the theme to start the right animation.
   */
  void playAnimation(TagSet animationTags);

  DirtyFlags dirtyFlags() const;
  bool dirtyFlagsChecked(DirtyFlags dirtyFlags) const;
  void markZIndexDirty();
  void markThemeDirty();
  void markMinimumSizeDirty();
  void markOptimalSizeDirty();
  void markParentMinimumSizeDirty();
  void markParentOptimalSizeDirty();
  void markChildAllocationDirty();
  void markChildZPositionDirty();

  void markAppereanceDirty();

  /** Halper function alowing to visit the whole widget tree easily.
   *
   * The search will be seen as successful and immediatly discontinued, as soon as the visitor
   * returns true for the first time.
   *
   * @param visitor the visitor function visiting every widget.
   *
   * @return true, if the visitor returned true once, otherwise false.
   */
  virtual bool visitWholeHierarchy(const std::function<bool(const Widget::Ptr& widget)>& visitor, int minDepth=0, int maxDepth=std::numeric_limits<int>::max());
  virtual void getRenderData(const Output<Theme::RenderData>& renderData) const;

  std::shared_ptr<Widget> shared_from_this();
  std::shared_ptr<const Widget> shared_from_this() const;

  virtual bool isWidgetAtPoint(const vec2& point) const;
  vec2 currentMousePosition(bool localCoordinate) const;

public:
  virtual void draw(Painter& painter) = 0;
  void sheduleRedraw();
  virtual void sheduleRedrawRecursive();
  void sheduleReorderRenderOrder();

  void invalidateMouseOverWidget();
  void invalidateKeyFocusedWidget();

protected:
  void _markDirty(DirtyFlags dirtyFlags);
  void _markParentsDirty(DirtyFlags dirtyFlags);
  virtual void _markChildrenDirty(DirtyFlags dirtyFlags);

  /**
   * Gets called when the widget was added to a window.
   *
   * The default implementation does nothing.
   */
  virtual void onAddedToWindow();

  /**
   * Gets called when the widget was removed from a window.
   *
   * The default implementation does nothing.
   */
  virtual void onRemovedFromWindow();

  /**
   * Gets called when the widget was added to a container-widget.
   *
   * The default implementation does nothing.
   */
  virtual void onAddedToContainer();

  /**
   * Gets called when the widget was removed from a container-widget.
   *
   * The default implementation does nothing.
   */
  virtual void onAboutToBeRemovedFromContainer();

  /**
   * Gets called when the widget was moved.
   *
   * The default implementation does nothing than sending a `signalMoved` signal.
   */
  virtual void onMoved();

  /**
   * Gets called when the widget was moved.
   *
   * The default implementation does nothing than sending a `signalMovedAlongZ` signal.
   */
  virtual void onMovedAlongZ();

  virtual void onThemeChanged();
  virtual void onUpdateMinimumSize();
  virtual void onUpdateOptimalSize();
  virtual void onUpdateChildAllocation();
  virtual void onUpdateChildZPosition();

  virtual void updateIndices(uint16 zOrder);
  void updateTheme();
  void updateScissor();
  void updateMinimumSize();
  void updateOptimalSize();
  void updateChildAllocation();
  void updateChildZPosition();

public:
  Signals::Signal<void()>& signalMoved();
  Signals::Signal<void()>& signalMovedAlongZ();

  Signals::Signal<void ()>& signalMinimumSizeChanged();
  Signals::Signal<void ()>& signalOptimalSizeChanged();

  Signals::Signal<void()>& signalVisibilityChanged();
  Signals::Signal<void()>& signalSensitivityChanged();

private:
  void _clearDirtyFlags(DirtyFlags dirtyFlags);

  void _updateParentVisibility();
  void _updateParentSensitivity();
  void _updateVisibility();
  void _updateSensitivity();
  void _handleAddedOrRemovedAnimation(Animation::SlotId slotId);

  Optional<Widget::Ptr> asSmartPtr();

private:
  class InputWidget_NextParentInHierarchy;
  class InputWidget_DrawStateChanges;
  class InputWidget_UpdateFocus;

  void _tagAsKeyFocused();
  void _untagAsKeyFocused();

public:
  static void sortWidgets(const InOutput< QVector<Widget::Ptr> >& widgets);
  static QVector<Widget::Ptr> sortSetIntoVector(const QSet<Widget::Ptr>& widgetSet);

private:
  static bool widgetOrderLessThan(const Widget::Ptr& a, const Widget::Ptr& b);
};

RTTI_CLASS_DECLARE(Framework::Gui::Widget)

} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_WIDGET_H
