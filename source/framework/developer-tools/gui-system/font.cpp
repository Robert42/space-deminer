#include "font.h"

namespace Framework {
namespace Gui {


Font::Font()
{
  _implementation = Implementation::createFallbackImplementation();
}

Font::Font(const Implementation::Ptr& implementation)
  : _implementation(implementation)
{
}


const Font::Implementation& Font::implementation() const
{
  return *_implementation;
}

Font::Implementation& Font::implementation()
{
  return *_implementation;
}


vec2 Font::textSize(const String& text) const
{
  return implementation().textSize(text);
}

real Font::lineSpacing() const
{
  return implementation().lineSpacing();
}

real Font::lineHeight() const
{
  return implementation().lineHeight();
}

// ==== Font::Implementation ====


Font::Implementation::Implementation()
{
  RTTI_CLASS_SET_TYPE(Framework::Gui::Font::Implementation);
}

Font::Implementation::~Implementation()
{
}


Font::Implementation::Ptr Font::Implementation::createFallbackImplementation()
{
  class FallbackImplementation : public Implementation
  {
  public:
    vec2 textSize(const String&) const override
    {
      return vec2(0);
    }

    real lineSpacing() const override
    {
      return 0.f;
    }

    real lineHeight() const override
    {
      return 0.f;
    }
  };

  static Ptr fallback(new FallbackImplementation);

  return fallback;
}


} // namespace Gui
} // namespace Framework
