#include "animation-slot-ids.h"

namespace Framework {
namespace Gui {
namespace AnimationSlots {
namespace RenderState {


Animation::SlotId alpha;
Animation::SlotId sensitive;
Animation::SlotId activated;
Animation::SlotId keyFocused;
Animation::SlotId mouseHover;
Animation::SlotId mousePressed;

Animation::SlotId xAllocationOffset;
Animation::SlotId yAllocationOffset;
Animation::SlotId zPositionOffset;

} // namespace RenderState

Animation::SlotId suppressMouseInput;

} // namespace AnimationSlots


void initAnimationSlots()
{
  AnimationSlots::RenderState::alpha = Animation::slotIdFromName("Framework::Gui::Animation::AnimationSlots::RenderState::alpha");
  AnimationSlots::RenderState::sensitive = Animation::slotIdFromName("Framework::Gui::Animation::AnimationSlots::RenderState::sensitive");
  AnimationSlots::RenderState::activated = Animation::slotIdFromName("Framework::Gui::Animation::AnimationSlots::RenderState::activated");
  AnimationSlots::RenderState::keyFocused = Animation::slotIdFromName("Framework::Gui::Animation::AnimationSlots::RenderState::keyFocused");
  AnimationSlots::RenderState::mouseHover = Animation::slotIdFromName("Framework::Gui::Animation::AnimationSlots::RenderState::mouseHover");
  AnimationSlots::RenderState::mousePressed = Animation::slotIdFromName("Framework::Gui::Animation::AnimationSlots::RenderState::mousePressed");

  AnimationSlots::RenderState::xAllocationOffset = Animation::slotIdFromName("Framework::Gui::Animation::AnimationSlots::RenderState::xAllocationOffset");
  AnimationSlots::RenderState::yAllocationOffset = Animation::slotIdFromName("Framework::Gui::Animation::AnimationSlots::RenderState::yAllocationOffset");
  AnimationSlots::RenderState::zPositionOffset = Animation::slotIdFromName("Framework::Gui::Animation::AnimationSlots::RenderState::zPositionOffset");

  AnimationSlots::suppressMouseInput = Animation::slotIdFromName("Framework::Gui::Animation::AnimationSlots::suppressMouseInput");
}


} // namespace Gui
} // namespace Framework
