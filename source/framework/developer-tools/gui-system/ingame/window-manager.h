#ifndef FRAMEWORK_GUI_INGAME_WINDOWMANAGER_H
#define FRAMEWORK_GUI_INGAME_WINDOWMANAGER_H

#include "../base-window-manager.h"

#include "ingame-appearance.h"
#include "window-container.h"

#include <framework/window/user-input/mouse-cursor-layers.h>
#include <framework/window/user-input/mouse-mode-layers.h>

namespace Framework {
namespace Gui {
namespace Ingame {

class WindowManager : public BaseWindowManager
{
public:
  Signals::Trackable trackable;

private:
  const MouseCursorLayers::Layer::Ptr mouseCursorLayer;
  const MouseModeLayers::Layer::Ptr mouseModeLayer;
  std::shared_ptr<UserInput::SignalBlocker> signalBlocker;

  InputHandler::Ptr inputHandler;

public:
  WindowManager(InputDevice::ActionPriority inputPriority,
                const Optional<InputDevice::ActionPriority>& inputBlockerPriority,
                const MouseCursorLayers::Layer::Ptr& mouseCursorLayer,
                const MouseModeLayers::Layer::Ptr& mouseModeLayer,
                const IngameAppearance::Ptr& appearance);

public:
  IngameAppearance::Ptr ingameAppereance();

  void sheduleRedraw();
  void handleAddedWindow(const WindowContainer::Ptr& windowContainer);

private:
  QVector<BaseWindowContainer::Ptr> _windowOrderToHandleUserInput() override;
};

} // namespace Ingame
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_INGAME_WINDOWMANAGER_H
