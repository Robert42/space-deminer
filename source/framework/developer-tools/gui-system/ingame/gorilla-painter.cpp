#include "gorilla-painter.h"
#include <base/types/type-manager.h>
#include <base/io/log.h>
#include <base/geometry/matrix4-helper.h>

namespace Framework {
namespace Gui {
namespace Ingame {


class GorillaPainter::OwnCustomPainter : public GorillaHelper::ViewportRenderer
{
private:
  friend class GorillaPainter;

private:
  const std::function<void()> redraw;

public:
  mat4 windowTransformation;

public:
  OwnCustomPainter(std::function<void()> redraw)
    : redraw(redraw),
      windowTransformation(1)
  {
  }

private:
  void _redrawImplementation() final override
  {
    redraw();
  }

  void _pushVertex(const _Vertex &vertex) override
  {
    _Vertex v = vertex;

    v.position = Ogre::Vector3_cast(applyWindowTransformation(vec3_cast(v.position)));

    GorillaHelper::ViewportRenderer::_pushVertex(v);
  }

  real anisotropicAntialiasingRadiusAtVertex(const vec2& point,
                                             const vec2& antialiasingDirection) const override
  {
    return Geometry::anisotropicAntialiasingRadiusAtVertex(windowTransformation, vec3(point, zPosition), antialiasingDirection);
  }

  vec3 applyWindowTransformation(const vec3& p) const
  {
    return Geometry::transformVertex(windowTransformation, p);
  }
};

class GorillaPainter::OwnCWCustomPainter : public OwnCustomPainter
{
public:
  OwnCWCustomPainter(std::function<void()> redraw)
    : OwnCustomPainter(redraw)
  {
  }

private:
  void _pushTriangle(size_t indexA, size_t indexB, size_t indexC, const Ogre::Vector2 *positions, const Ogre::ColourValue *colors, const Ogre::Vector2 *uvs) override
  {
    OwnCustomPainter::_pushTriangle(indexC, indexB, indexA, positions, colors, uvs);
  }
};


GorillaPainter::FontImplementation::FontImplementation(Gorilla::GlyphData* gorillaGlyphData, int gorillaFontIndex)
  : gorillaGlyphData(gorillaGlyphData),
    gorillaFontIndex(gorillaFontIndex)
{
  RTTI_CLASS_SET_TYPE(Framework::Gui::Ingame::GorillaPainter::FontImplementation);
}

vec2 GorillaPainter::FontImplementation::textSize(const String& text) const
{
  GorillaHelper::PrimitiveRenderer::AbstractFont abstractFont(gorillaGlyphData);

  return vec2(abstractFont.textLineWidth(text),
              abstractFont.lineHeight());
}

real GorillaPainter::FontImplementation::lineSpacing() const
{
  return GorillaHelper::PrimitiveRenderer::AbstractFont::lineSpacingFor(gorillaGlyphData);
}

real GorillaPainter::FontImplementation::lineHeight() const
{
  return gorillaGlyphData->mLineHeight;
}

GorillaPainter::FontImplementation::Ptr GorillaPainter::FontImplementation::create(Gorilla::GlyphData* gorillaGlyphData, int gorillaFontIndex)
{
  return Ptr(new FontImplementation(gorillaGlyphData, gorillaFontIndex));
}



GorillaPainter::GorillaPainter(const std::shared_ptr<DeveloperHud::Layer>& devHudLayer,
                               const std::function<void()>& redraw,
                               bool antialiasingForAxisAligned,
                               bool counterclockwise)
  : devHudLayer(devHudLayer),
    gorillaLayer(devHudLayer->gorillaLayer()),
    antialiasingForAxisAligned(antialiasingForAxisAligned)
{
  setAntialiasingHint(Painter::AntialiasingHint::AXIS_ALIGNED);

  if(counterclockwise)
    customRenderer = new OwnCustomPainter(redraw);
  else
    customRenderer = new OwnCWCustomPainter(redraw);
  gorillaLayer.appendCustomRenderer(customRenderer);
}

GorillaPainter::~GorillaPainter()
{
  trackable.clear();

  gorillaLayer.destroyCustomRenderer(customRenderer);
}

GorillaPainter::Ptr GorillaPainter::create(const std::shared_ptr<DeveloperHud::Layer>& devHudLayer,
                                           const std::function<void()>& redraw,
                                           bool antialiasingForAxisAligned,
                                           bool counterclockwise)
{
  return Ptr(new GorillaPainter(devHudLayer, redraw, antialiasingForAxisAligned, counterclockwise));
}

GorillaPainter::Ptr GorillaPainter::createFlat(const std::shared_ptr<DeveloperHud::Layer>& devHudLayer,
                                               const std::function<void()>& redraw,
                                               bool counterclockwise)
{
  return create(devHudLayer, redraw, false, counterclockwise);
}

GorillaPainter::Ptr GorillaPainter::createSpatial(const std::shared_ptr<DeveloperHud::Layer>& devHudLayer,
                                               const std::function<void()>& redraw,
                                               bool counterclockwise)
{
  return create(devHudLayer, redraw, true, counterclockwise);
}

void GorillaPainter::beginRedrawing()
{
  customRenderer->setInfiniteScissor();
  customRenderer->setInfiniteViewport();
  customRenderer->mVertices.remove_all();
}

void GorillaPainter::endRedrawing()
{
}

void GorillaPainter::sheduleRedraw()
{
  customRenderer->makeDirty();
}


void GorillaPainter::setAntialiasingHint(AntialiasingHint hint)
{
  switch(hint.value)
  {
  case AntialiasingHint::ASK_FOR_ANTIALIASING:
    axisAlignedAntialiasing = DEFAULT_ANTIALIASING_WIDTH;
    antialiasing = DEFAULT_ANTIALIASING_WIDTH;
    break;
  case AntialiasingHint::FORBID_ANTIALIASING:
    axisAlignedAntialiasing = 0.f;
    antialiasing = 0.f;
    break;
  case AntialiasingHint::AXIS_ALIGNED:
  default:
    axisAlignedAntialiasing = antialiasingForAxisAligned ? DEFAULT_ANTIALIASING_WIDTH : 0.f;
    antialiasing = DEFAULT_ANTIALIASING_WIDTH;
    break;
  }
}


void GorillaPainter::setWindowTransformation(const mat4& windowTransformation)
{
  customRenderer->windowTransformation = windowTransformation;
}

void GorillaPainter::setZPosition(real z)
{
  customRenderer->zPosition = z;
}

void GorillaPainter::drawFilledTriangle(const vec4& color, const vec2& a, const vec2& b, const vec2& c)
{
  customRenderer->drawTriangle(color,
                               a,
                               b,
                               c);

  if(antialiasing)
  {
    bool ccw = cross(vec3(b-a, 0), vec3(c-a, 0)).z < 0;

    drawAntialiasing(ccw, !ccw, {a, b, c}, true, color);
  }
}

void GorillaPainter::drawFilledRectangle(const Rectangle<vec2>& rectangle, const vec4 &color)
{
  customRenderer->drawRectangle(rectangle,
                                color);

  if(axisAlignedAntialiasing)
    drawAntialiasing(true, false, {rectangle.corner(0), rectangle.corner(1), rectangle.corner(2), rectangle.corner(3)}, true, color);
}

void GorillaPainter::drawFilledRectangle(const Rectangle<vec2>& rectangle, const vec4& colorUpper, const vec4& colorLower)
{
  customRenderer->drawFilledRectangle(rectangle, colorUpper, colorLower);

  if(axisAlignedAntialiasing)
    drawAntialiasing(true, false, {rectangle.corner(0), rectangle.corner(1), rectangle.corner(2), rectangle.corner(3)}, true, colorLower);
}

void GorillaPainter::drawRoundedRectangle(const RoundedRectangle& roundedRectangle, const vec4 &color, real border, real strokeOffset)
{
  customRenderer->drawRoundedRectangle(roundedRectangle,
                                       color,
                                       border,
                                       strokeOffset,
                                       axisAlignedAntialiasing,
                                       antialiasing);
}

void GorillaPainter::drawQuadStrip(const QVector<vec2>& verticesLeft,
                                   const QVector<vec2>& verticesRight,
                                   const vec4& color,
                                   bool loop)
{
  assert(verticesLeft.size() == verticesRight.size());

  if(antialiasing)
  {
    GorillaHelper::PrimitiveRenderer::LineStripDrawer lineStripDrawer(*this->customRenderer);

    lineStripDrawer.outline.thickness = 0.f;
    lineStripDrawer.outline.color = color;
    lineStripDrawer.drawOutline(verticesLeft,
                                verticesRight,
                                false,
                                true,
                                loop);
  }

  const int nVertices = verticesLeft.size();

  int nQuads = nVertices;
  if(!loop)
    nQuads--;

  for(int i=0; i<nQuads; ++i)
  {
    int j = (i+1)%nVertices;
    vec2 quadVertices[4];

    quadVertices[0] = verticesLeft[i];
    quadVertices[1] = verticesRight[i];
    quadVertices[2] = verticesRight[j];
    quadVertices[3] = verticesLeft[j];

    customRenderer->drawQuad(quadVertices, color);
  }
}

void GorillaPainter::drawText(const String& text, const Font& font, const vec2& position, const vec4& color)
{
  if(font.implementation().rtti.typeId() != Types::Ingame::GorillaFont)
  {
    IO::Log::logError("GorillaPainter::drawText(...): Trying to draw text using a non gorilla font");
    return;
  }

  const GorillaPainter::FontImplementation& fontImplementation = static_cast<const GorillaPainter::FontImplementation&>(font.implementation());

  OwnCustomPainter::Font gorillaFont(*customRenderer,
                                     fontImplementation.gorillaFontIndex);

  gorillaFont.color = color;
  gorillaFont.cursor = position;
  gorillaFont.drawTextLine(text);
}


void GorillaPainter::setScissorRect(const Optional< Rectangle<vec2> >& scissorRect)
{
  if(scissorRect)
    customRenderer->setScissor(*scissorRect);
  else
    customRenderer->setInfiniteScissor();
}

void GorillaPainter::drawAntialiasing(bool right, bool left, const QVector<vec2>& vertices, bool loop, const vec4& color)
{
  GorillaHelper::PrimitiveRenderer::LineStripDrawer lineStripDrawer(*this->customRenderer);

  lineStripDrawer.outline.thickness = 0.f;
  lineStripDrawer.outline.color = color;

  QVector<vec2> leftVertices, rightVertices;

  if(left)
    leftVertices = vertices;
  if(right)
    rightVertices = vertices;

  lineStripDrawer.drawOutline(leftVertices,
                              rightVertices,
                              false,
                              true,
                              loop);
}


class GorillaPainter::LinePenImplementation : public Painter::LinePen::Implementation
{
public:
  typedef std::shared_ptr<LinePenImplementation> Ptr;

public:
  GorillaHelper::PrimitiveRenderer::LineStripDrawer lineStripDrawer;
  real dashLength;

public:
  LinePenImplementation(GorillaPainter& gorillaPainter)
    : Implementation(gorillaPainter),
      lineStripDrawer(*gorillaPainter.customRenderer),
      dashLength(0)
  {
  }

  static Ptr create(GorillaPainter& gorillaPainter)
  {
    return Ptr(new LinePenImplementation(gorillaPainter));
  }

public:
  void setColor(const vec4& color) override
  {
    this->lineStripDrawer.color = color;
  }

  void setThickness(real thickness) override
  {
    this->lineStripDrawer.thickness = thickness;
  }

  void setDashLength(Optional<real> dashLength) override
  {
    this->dashLength = dashLength;
  }

  void beginStroke(bool loop) override
  {
    if(loop)
      this->lineStripDrawer.beginLoop();
    else
      this->lineStripDrawer.begin();
  }

  void endStroke() override
  {
    this->lineStripDrawer.end();
  }

  void drawVertex(const vec2& point) override
  {
    this->lineStripDrawer.drawTo(point);
  }
};


Painter::LinePen GorillaPainter::startDrawingLine()
{
  return LinePen(LinePenImplementation::create(*this));
}


} // namespace Ingame
} // namespace Gui
} // namespace Framework
