#include "ingame-appearance.h"

namespace Framework {
namespace Gui {
namespace Ingame {

IngameAppearance::IngameAppearance(const Renderer::Ptr &renderer, const Theme::Ptr &rootTheme)
  : _renderer(renderer),
    _rootTheme(rootTheme)
{
}

IngameAppearance::Ptr IngameAppearance::create(const Renderer::Ptr& painter, const Theme::Ptr& rootTheme)
{
  return Ptr(new IngameAppearance(painter, rootTheme));
}


void IngameAppearance::sheduleRedraw()
{
  _renderer->sheduleRedraw();
}

Renderer::Ptr IngameAppearance::renderer()
{
  return _renderer;
}

Theme::Ptr IngameAppearance::rootTheme(const TagSet&)
{
  return _rootTheme;
}

} // namespace Ingame
} // namespace Gui
} // namespace Framework
