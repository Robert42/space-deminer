#include "window-manager.h"
#include "../gorilla-painter.h"
#include "../gorilla-font-manager.h"
#include "../ingame-appearance.h"
#include "../../themes/dark-theme.h"
#include "holo-appereance/holo-appereance.h"

#include <framework/application.h>

#include <base/geometry/matrix4-helper.h>

namespace Framework {
namespace Gui {
namespace Ingame {
namespace Spatial {

class IngameDevHudLayer : public DeveloperHud::Layer
{
public:
  typedef std::shared_ptr<IngameDevHudLayer> Ptr;

private:
  IngameDevHudLayer(ZLayer z, Gorilla::Screen& gorillaScreen)
    : DeveloperHud::Layer(z, gorillaScreen)
  {
    show();
  }

public:
  static Ptr create(ZLayer z, Gorilla::Screen& gorillaScreen)
  {
    return Ptr(new IngameDevHudLayer(z, gorillaScreen));
  }

  static Ptr create()
  {
    return create(IngameDevHudLayer::Layer::ZLayer::INGAME_GUI,
                  Application::developerHud().gorillaScreenIngameEditor());
  }
};

WindowManager::WindowManager(InputDevice::ActionPriority inputPriority,
                             InputDevice::ActionPriority inputBlockerPriority,
                             const MouseCursorLayers::Layer::Ptr& mouseCursorLayer,
                             const MouseModeLayers::Layer::Ptr& mouseModeLayer,
                             const IngameAppearance::Ptr& appearance,
                             const std::shared_ptr<DeveloperHud::Layer>& devHudLayer)
  : Ingame::WindowManager(inputPriority,
                          inputBlockerPriority,
                          mouseCursorLayer,
                          mouseModeLayer,
                          appearance),
    devHudLayer(devHudLayer),
    _dirtyMatrix(true)
{
  this->camera.signalInvalidated().connect(std::bind(&WindowManager::_markMatrixDirty, this)).track(this->trackable);
  this->camera.coordinate.position = vec3(0, 0, 5);
  this->camera.setFlipY(true);

  renderWindowGotResized();
  RenderWindow::signalWindowResized().connect(std::bind(&WindowManager::renderWindowGotResized, this)).track(this->trackable);
}

WindowManager::WindowManager(InputDevice::ActionPriority inputPriority,
                             InputDevice::ActionPriority inputBlockerPriority,
                             const MouseCursorLayers::Layer::Ptr& mouseCursorLayer,
                             const MouseModeLayers::Layer::Ptr& mouseModeLayer,
                             const std::shared_ptr<DeveloperHud::Layer>& devHudLayer)
  : WindowManager(inputPriority,
                  inputBlockerPriority,
                  mouseCursorLayer,
                  mouseModeLayer,
                  HoloAppereance::create(Ingame::GorillaPainter::createSpatial(devHudLayer,
                                                                               std::bind(&WindowManager::redraw,
                                                                                         this)),
                                         Themes::DarkTheme::create(GorillaFontManager::create(devHudLayer))),
                  devHudLayer)
{
}

WindowManager::WindowManager(InputDevice::ActionPriority inputPriority,
                             InputDevice::ActionPriority inputBlockerPriority,
                             const MouseCursorLayers::Layer::Ptr& mouseCursorLayer,
                             const MouseModeLayers::Layer::Ptr& mouseModeLayer)
  : WindowManager(inputPriority,
                  inputBlockerPriority,
                  mouseCursorLayer,
                  mouseModeLayer,
                  IngameDevHudLayer::create())
{
}

WindowManager::WindowManager()
  : WindowManager(InputDevice::ActionPriority::GUI,
                  InputDevice::ActionPriority::GUI_BLOCKER,
                  Mouse::mouseCursorLayers().ingameUILayer,
                  Mouse::mouseModeLayers().ingameUILayer)
{
}

QVector<BaseWindowContainer::Ptr> WindowManager::_windowOrderToHandleUserInput()
{
  QVector<BaseWindowContainer::Ptr> windows;

  windows.reserve(this->windowContainers.size());

  this->sortWindows();
  for(WindowContainer::Ptr& w : this->windowContainers)
    windows.append(std::static_pointer_cast<Ingame::WindowContainer>(w));

  return windows;
}

void WindowManager::sortWindows()
{
  qStableSort(windowContainers.begin(),
              windowContainers.end(),
              std::bind(&WindowManager::containerShouldBeDrawnBefore, this, _1, _2));
}

bool WindowManager::containerShouldBeDrawnBefore(const WindowContainer::Ptr& a, const WindowContainer::Ptr& b)
{
  return a->order(camera.coordinate) < b->order(camera.coordinate);
}

void WindowManager::_addMainWindow(const Window::Ptr& window)
{
  WindowContainer::Ptr windowContainer = WindowContainer::createMainWindow(*this, window);
  windowContainers.prepend(windowContainer);
  handleAddedWindow(windowContainer);
}

void WindowManager::_addDialog(const Window::Ptr& window)
{
  WindowContainer::Ptr windowContainer = WindowContainer::createDialog(*this, window);
  windowContainers.append(windowContainer);
  handleAddedWindow(windowContainer);
}

void WindowManager::_closeWindow(const BaseWindowContainer::Ptr& windowContainer)
{
  windowContainers.remove(windowContainers.indexOf(std::static_pointer_cast<WindowContainer>(windowContainer)));
}

const mat4& WindowManager::targetProjectionMatrix() const
{
  if(_dirtyMatrix)
  {
    _targetProjectionMatrix = camera.viewportMatrix() * camera.projectionMatrix();
    _dirtyMatrix = false;
  }
  return _targetProjectionMatrix;
}

mat4 WindowManager::targetProjectionViewMatrix() const
{
  return targetProjectionMatrix() * camera.viewMatrix();
}

vec3 WindowManager::projectFromWorldToTarget(const vec3& v) const
{
  return Geometry::transformVertex(this->targetProjectionViewMatrix(),
                                   v);
}

void WindowManager::renderWindowGotResized()
{
  this->camera.setViewport(Rectangle<vec2>(vec2(0), RenderWindow::size()));
  this->_markMatrixDirty();
  sheduleRedraw();
}

void WindowManager::_markMatrixDirty()
{
  _dirtyMatrix = true;
}

InputHandler::Ptr WindowManager::createInputHandlerForWindow(BaseWindowContainer& windowContainer)
{
  return createInputHandlerForWindow_GameUIStyle(windowContainer);
}


} // namespace Spatial
} // namespace Ingame
} // namespace Gui
} // namespace Framework
