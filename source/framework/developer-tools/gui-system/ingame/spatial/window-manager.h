#ifndef FRAMEWORK_GUI_INGAME_SPATIAL_WINDOWMANAGER_H
#define FRAMEWORK_GUI_INGAME_SPATIAL_WINDOWMANAGER_H

#include "../window-manager.h"

#include "./window-container.h"

#include <framework/developer-tools/developer-hud.h>
#include <base/geometry/camera-geometry.h>

namespace Framework {
namespace Gui {
namespace Ingame {
namespace Spatial {

class WindowManager final : public Ingame::WindowManager
{
private:
  QVector<WindowContainer::Ptr> windowContainers;

  const std::shared_ptr<DeveloperHud::Layer> devHudLayer;

public:
  Geometry::Camera camera;

private:
  mutable bool _dirtyMatrix;
  mutable mat4 _targetProjectionMatrix;
  mutable mat4 _targetProjectionViewMatrix;
  vec2 _renderWindowSize;

public:
  WindowManager(InputDevice::ActionPriority inputPriority,
                InputDevice::ActionPriority inputBlockerPriority,
                const MouseCursorLayers::Layer::Ptr& mouseCursorLayer,
                const MouseModeLayers::Layer::Ptr& mouseModeLayer,
                const IngameAppearance::Ptr& appearance,
                const std::shared_ptr<DeveloperHud::Layer>& devHudLayer);
  WindowManager(InputDevice::ActionPriority inputPriority,
                InputDevice::ActionPriority inputBlockerPriority,
                const MouseCursorLayers::Layer::Ptr& mouseCursorLayer,
                const MouseModeLayers::Layer::Ptr& mouseModeLayer,
                const std::shared_ptr<DeveloperHud::Layer>& devHudLayer);
  WindowManager(InputDevice::ActionPriority inputPriority,
                InputDevice::ActionPriority inputBlockerPriority,
                const MouseCursorLayers::Layer::Ptr& mouseCursorLayer,
                const MouseModeLayers::Layer::Ptr& mouseModeLayer);
  WindowManager();

private:
  void _addMainWindow(const Window::Ptr& window) override;
  void _addDialog(const Window::Ptr& window) override;
  void _closeWindow(const BaseWindowContainer::Ptr& windowContainer) override;

public:
  const mat4& targetProjectionMatrix() const;
  mat4 targetProjectionViewMatrix() const;

  void setFov(real fov);
  void setZNear(real zNear);
  void setZFar(real zFar);

  real fov() const;
  real zNear() const;
  real zFar() const;

  vec3 projectFromWorldToTarget(const vec3& v) const;

private:
  QVector<BaseWindowContainer::Ptr> _windowOrderToHandleUserInput() final override;

  InputHandler::Ptr createInputHandlerForWindow(BaseWindowContainer& windowContainer) override;

  void sortWindows();
  bool containerShouldBeDrawnBefore(const WindowContainer::Ptr& a, const WindowContainer::Ptr& b);

  void renderWindowGotResized();

  void _markMatrixDirty();
  void _updateMatrices();
};

} // namespace Spatial
} // namespace Ingame
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_INGAME_SPATIAL_WINDOWMANAGER_H
