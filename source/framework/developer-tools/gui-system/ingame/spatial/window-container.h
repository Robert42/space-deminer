#ifndef FRAMEWORK_GUI_INGAME_SPATIAL_WINDOWCONTAINER_H
#define FRAMEWORK_GUI_INGAME_SPATIAL_WINDOWCONTAINER_H

#include "../window-container.h"

#include <base/geometry/coordinate.h>

namespace Framework {
namespace Gui {
namespace Ingame {
namespace Spatial {

class WindowManager;

class WindowContainer final : public Ingame::WindowContainer
{
public:
  typedef std::shared_ptr<WindowContainer> Ptr;

public:
  Signals::Trackable trackable;

private:
  Coordinate _coordinate;
  vec2 _relativeOriginPosition;
  real _zOffset;
  vec3 _scale;
  real _guiPixelUnit;

  mutable mat4 _modelViewProjectionMatrix;
  mutable mat4 _modelMatrix;
  mutable mat4 _invertedModelMatrix;
  mutable bool _modelMatrixDirty;

public:
  WindowContainer(WindowManager& windowManager, const Window::Ptr& window);

  static Ptr createMainWindow(WindowManager& windowManager, const Window::Ptr& window);
  static Ptr createDialog(WindowManager& windowManager, const Window::Ptr& window);

public:
  WindowManager& windowManager();
  const WindowManager& windowManager() const;

public:
  const mat4& invertedModelMatrix() const;
  const mat4& modelMatrix() const;
  const mat4& modelViewProjectionMatrix() const;

  Coordinate coordinate() const;
  vec2 relativeOriginPosition() const;
  real zOffset() const;
  vec3 scale() const;
  real guiPixelUnit() const;
  void setCoordinate(const Coordinate& coordinate);
  void setRelativeOriginPosition(const vec2& relativeOriginPosition);
  void setZOffset(real zOffset);
  void setScale(const vec3& scale);
  void setGuiPixelUnit(real guiPixelUnit);

  Geometry::Ray rayForScreenSpacePixel(const vec2& point) const;
  Geometry::Ray rayForCurrentMouseCursor() const;

  real order(const Coordinate& cameraCoordinate) const;

  void beginDrawing(Painter& painter) override;
  void endDrawing(Painter& painter) override;

  bool canHandleMouseInputAtCurrentMousePosition() const override;
  vec2 currentMousePosition(real zPosition) const override;

private:
  QSet<Widget::Ptr> widgetsAtCurrentMousePositionImplementation() const override;

  void _markModelMatrixDirty();
  void updateMatrix() const;
};

} // namespace Spatial
} // namespace Ingame
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_INGAME_SPATIAL_WINDOWCONTAINER_H
