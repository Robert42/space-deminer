#include "window-container.h"
#include "window-manager.h"

#include <base/geometry/ray.h>

namespace Framework {
namespace Gui {
namespace Ingame {
namespace Spatial {


WindowContainer::WindowContainer(WindowManager& windowManager, const Window::Ptr& window)
  : Ingame::WindowContainer(windowManager, window, false),
    _relativeOriginPosition(0.5),
    _zOffset(0),
    _scale(1.f),
    _guiPixelUnit(0.015f),
    _modelViewProjectionMatrix(1),
    _modelMatrix(1),
    _invertedModelMatrix(1),
    _modelMatrixDirty(true)
{
}

WindowContainer::Ptr WindowContainer::createMainWindow(WindowManager& windowManager, const Window::Ptr& window)
{
  return createDialog(windowManager, window);
}

WindowContainer::Ptr WindowContainer::createDialog(WindowManager& windowManager, const Window::Ptr& window)
{
  return Ptr(new WindowContainer(windowManager, window));
}

WindowManager& WindowContainer::windowManager()
{
  return reinterpret_cast<WindowManager&>(BaseWindowContainer::windowManager);
}

const WindowManager& WindowContainer::windowManager() const
{
  return static_cast<const WindowManager&>(const_cast<WindowContainer*>(this)->windowManager());
}


Coordinate WindowContainer::coordinate() const
{
  return this->_coordinate;
}

vec2 WindowContainer::relativeOriginPosition() const
{
  return this->_relativeOriginPosition;
}

real WindowContainer::zOffset() const
{
  return this->_zOffset;
}

vec3 WindowContainer::scale() const
{
  return this->_scale;
}

real WindowContainer::guiPixelUnit() const
{
  return this->_guiPixelUnit;
}


void WindowContainer::setCoordinate(const Coordinate& coordinate)
{
  if(this->coordinate() != coordinate)
  {
    this->_coordinate = coordinate;
    _markModelMatrixDirty();
    _handleMovedWindow();
  }
}

void WindowContainer::setRelativeOriginPosition(const vec2& relativeOriginPosition)
{
  if(this->relativeOriginPosition() != relativeOriginPosition)
  {
    this->_relativeOriginPosition = relativeOriginPosition;
    _markModelMatrixDirty();
    _handleMovedWindow();
  }
}

void WindowContainer::setZOffset(real zOffset)
{
  if(this->zOffset() != zOffset)
  {
    this->_zOffset = zOffset;
    _markModelMatrixDirty();
    _handleMovedWindow();
  }
}

void WindowContainer::setScale(const vec3& scale)
{
  if(this->scale() != scale)
  {
    this->_scale = scale;
    _markModelMatrixDirty();
    _handleMovedWindow();
  }
}

void WindowContainer::setGuiPixelUnit(real guiPixelUnit)
{
  if(this->guiPixelUnit() != guiPixelUnit)
  {
    this->_guiPixelUnit = guiPixelUnit;
    _markModelMatrixDirty();
    _handleMovedWindow();
  }
}


const mat4& WindowContainer::invertedModelMatrix() const
{
  updateMatrix();

  return _invertedModelMatrix;
}

const mat4& WindowContainer::modelMatrix() const
{
  updateMatrix();

  return _modelMatrix;
}

const mat4& WindowContainer::modelViewProjectionMatrix() const
{
  updateMatrix();

  return _modelViewProjectionMatrix;
}

void WindowContainer::_markModelMatrixDirty()
{
  _modelMatrixDirty = true;
}

void WindowContainer::updateMatrix() const
{
  if(_modelMatrixDirty)
  {
    _modelMatrixDirty = false;

    const vec3 origin = vec3(this->window->size()*this->relativeOriginPosition(), this->zOffset());
    const vec3 scale = this->scale() * this->guiPixelUnit();

    _modelMatrix = coordinate().toMatrix4x4() * glm::translate(glm::scale(mat4(1), scale*vec3(1,-1,1)), vec3(0,1, 0)-origin);
    _invertedModelMatrix = inverse(_modelMatrix);
  }

  _modelViewProjectionMatrix = windowManager().targetProjectionViewMatrix() * _modelMatrix;
}


Geometry::Ray WindowContainer::rayForScreenSpacePixel(const vec2& point) const
{
  return invertedModelMatrix() * windowManager().camera.rayForViewportPosition(point);
}

Geometry::Ray WindowContainer::rayForCurrentMouseCursor() const
{
  return rayForScreenSpacePixel(Mouse::absolutePosition());
}


real WindowContainer::order(const Coordinate& cameraCoordinate) const
{
  return length(cameraCoordinate.position - this->coordinate().position);
}

void WindowContainer::beginDrawing(Painter& painter)
{
  painter.setWindowTransformation(modelViewProjectionMatrix());
}

void WindowContainer::endDrawing(Painter& painter)
{
  painter.setWindowTransformation(mat4(1));
}


vec2 WindowContainer::currentMousePosition(real zPosition) const
{
  Optional<vec3> optionalVec3 = rayForCurrentMouseCursor().intersectionWithZPlane(zPosition);

  if(optionalVec3)
    return optionalVec3->xy();
  return vec2(0);
}

QSet<Widget::Ptr> WindowContainer::widgetsAtCurrentMousePositionImplementation() const
{
  return window->widgetModel()->widgetsAtRay(rayForCurrentMouseCursor());
}

bool WindowContainer::canHandleMouseInputAtCurrentMousePosition() const
{
  return window->isAnyWidgetAtRay(rayForCurrentMouseCursor());
}


} // namespace Spatial
} // namespace Ingame
} // namespace Gui
} // namespace Framework
