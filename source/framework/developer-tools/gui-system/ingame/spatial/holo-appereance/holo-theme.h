#ifndef FRAMEWORK_GUI_INGAME_SPATIAL_HOLOTHEME_H
#define FRAMEWORK_GUI_INGAME_SPATIAL_HOLOTHEME_H

#include "../../../themes/theme-with-default-animations.h"

#include <framework/developer-tools/gui-system/themes/holo-frame.h>

namespace Framework {
namespace Gui {
namespace Ingame {
namespace Spatial {

class HoloTheme final : public Themes::ThemeWithDefaultAnimations
{
public:
  typedef std::shared_ptr<HoloTheme> Ptr;

  typedef Themes::HoloFrame HoloFrame;

  class Tweaking
  {
  public:
    vec4 buttonBorder_normal;
    vec4 buttonBorder_insensitive;
    vec4 buttonBorder_hover;
    vec4 buttonBorder_pressed;
    vec4 buttonBorder_focused_normal;
    vec4 buttonBorder_focused_hover;
    vec4 buttonBorder_focused_pressed;

    real buttonCornerRadius;
    real buttonBorderThickness_minimumSize;
    real buttonBorderThickness_optimalSize;
    real buttonMinimumSize;
    vec2 buttonOptimalSize;

    real zLayerDistance;
    real pressZLayerOffset;

    vec4 windowFrame;

    Tweaking();
  };

public:
  Tweaking tweaking;

private:
  QHash<ClassType, Theme::SimpleWidget::Ptr> _simpleWidgets;

public:
  HoloTheme();

  static Ptr create();

public:
  Optional<SimpleWidget::Ptr> simpleWidget(const ClassType& widgetType,
                                           const TagSet& tags=TagSet()) override;
  Optional<SimpleTextWidget::Ptr> simpleTextWidget(const ClassType& widgetType,
                                                   const TagSet& tags) override;
  Optional<const Metrics*> layoutMetrics(const ClassType& typeLayout,
                                         const ClassType& typeWidget,
                                         const TagSet& tagsLayout=TagSet(),
                                         const TagSet& tagsWidget=TagSet()) override;
  Optional<const Metrics*> layoutSpacingItemMetrics(const ClassType& typeLayout,
                                                    const ClassType& typeWidget,
                                                    const TagSet& tagsLayout=TagSet(),
                                                    const TagSet& tagsWidget=TagSet()) override;
  bool handleClickOffset(const InOutput<AnimationData>& animationData,
                         bool start) override;
};

} // namespace Spatial
} // namespace Ingame
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_INGAME_SPATIAL_HOLOTHEME_H
