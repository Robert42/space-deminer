#include "holo-appereance.h"
#include "../../../themes/mixed-theme.h"
#include "../../../themes/dark-theme.h"
#include "holo-theme.h"

namespace Framework {
namespace Gui {
namespace Ingame {
namespace Spatial {

HoloAppereance::HoloAppereance(const Renderer::Ptr& renderer, const Theme::Ptr& fallbackTheme)
  : IngameAppearance(renderer,
                     Themes::MixedTheme::create({HoloTheme::create(),
                                                 fallbackTheme}))
{
}

HoloAppereance::Ptr HoloAppereance::create(const Renderer::Ptr& renderer,
                                           const Theme::Ptr& fallbackTheme)
{
  return Ptr(new HoloAppereance(renderer,
                                fallbackTheme));
}


} // namespace Spatial
} // namespace Ingame
} // namespace Gui
} // namespace Framework
