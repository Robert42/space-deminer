#include "holo-theme.h"
#include "../../../animation-slot-ids.h"
#include "../../../tags.h"

#include <base/enum-macros.h>

namespace Framework {
namespace Gui {
namespace Ingame {
namespace Spatial {

typedef Themes::HoloFrame HoloFrame;

namespace HoloTheme_SimpleWidgets
{

class SimpleWidget : public Theme::SimpleWidget
{
public:
  HoloTheme& holoTheme;

public:
  SimpleWidget(HoloTheme& holoTheme)
    : holoTheme(holoTheme)
  {
  }
};

class WindowFrame : public SimpleWidget
{
public:
  BEGIN_ENUMERATION(Decoration,)
    NONE = 0,
    CORNER_SE_THICKNESS = 1,
    EDGE_WEST_THICKNESS = 2,
    EDGE_SOUTH_INNER_THICKNESS = 4,
    EDGE_SOUTH_OUTER_THICKNESS = 8,
    EDGE_NORTH_OFFSET = 16,
    EDGE_EAST_THICKNESS_LARGE = 32,
    EDGE_WEST_THICKNESS_OUTER = 64,
    EDGE_EAST_THICKNESS_SMALL = 128,
  ENUMERATION_BASIC_OPERATORS(Decoration)
  ENUMERATION_FLAG_OPERATORS(Decoration)
  END_ENUMERATION;

private:
  HoloFrame frames[4];

private:
  WindowFrame(HoloTheme& holoTheme)
    : SimpleWidget(holoTheme)
  {
    initFrame(frames[0], Decoration::EDGE_NORTH_OFFSET|Decoration::EDGE_WEST_THICKNESS|Decoration::EDGE_SOUTH_OUTER_THICKNESS|Decoration::EDGE_EAST_THICKNESS_LARGE);
    initFrame(frames[1], Decoration::EDGE_NORTH_OFFSET|Decoration::EDGE_WEST_THICKNESS|Decoration::EDGE_SOUTH_OUTER_THICKNESS|Decoration::CORNER_SE_THICKNESS);
    initFrame(frames[2], Decoration::EDGE_NORTH_OFFSET|Decoration::EDGE_WEST_THICKNESS|Decoration::EDGE_SOUTH_OUTER_THICKNESS|Decoration::EDGE_WEST_THICKNESS_OUTER|Decoration::EDGE_EAST_THICKNESS_SMALL);
    initFrame(frames[3], Decoration::EDGE_NORTH_OFFSET|Decoration::EDGE_WEST_THICKNESS|Decoration::EDGE_SOUTH_OUTER_THICKNESS|Decoration::EDGE_SOUTH_INNER_THICKNESS);
  }

  void initFrame(HoloFrame& frame, Decoration decorations)
  {
    HoloFrame::FlattenedCorner::Ptr cornerNW = HoloFrame::FlattenedCorner::create(vec2(64));
    HoloFrame::FlattenedCorner::Ptr cornerSW = HoloFrame::FlattenedCorner::create(vec2(32));
    HoloFrame::FlattenedCorner::Ptr cornerSE = HoloFrame::FlattenedCorner::create(vec2(64));
    HoloFrame::SimpleCorner::Ptr cornerNE = HoloFrame::SimpleCorner::create();

    frame.cornerNW = cornerNW;
    frame.cornerSW = cornerSW;
    frame.cornerSE = cornerSE;
    frame.cornerNE = cornerNE;

    HoloFrame::ChangeThickness::Ptr thicknessChange;
    HoloFrame::OffsetSection::Ptr offsetSection;
    real thickness;
    real thicknessChangeLength;

    thickness = 20.f;
    thicknessChangeLength = 24.f;

    if((decorations & Decoration::EDGE_SOUTH_OUTER_THICKNESS) != Decoration::NONE)
    {
      thicknessChange = HoloFrame::ChangeThickness::create(thicknessChangeLength, 0, thickness);
      thicknessChange->setPaddingStart(128.f);
      thicknessChange->setPaddingEnd(16.f);
      thicknessChange->setRelativePosition(0.);
      frame.south.addSection(thicknessChange);

      thicknessChange = HoloFrame::ChangeThickness::create(thicknessChangeLength, 0, -thickness);
      thicknessChange->setPaddingStart(128.f);
      thicknessChange->setPaddingEnd(32.f);
      thicknessChange->setRelativePosition(0.75f);
      frame.south.addSection(thicknessChange);
    }

    thickness = 8.f;
    thicknessChangeLength = 8.f;

    if((decorations & Decoration::EDGE_SOUTH_INNER_THICKNESS) != Decoration::NONE)
    {
      thicknessChange = HoloFrame::ChangeThickness::create(thicknessChangeLength, thickness, 0);
      thicknessChange->setPaddingStart(132.f);
      thicknessChange->setPaddingEnd(16.f);
      thicknessChange->setRelativePosition(0.);
      frame.south.addSection(thicknessChange);

      thicknessChange = HoloFrame::ChangeThickness::create(thicknessChangeLength, -thickness, 0);
      thicknessChange->setPaddingStart(132.f);
      thicknessChange->setPaddingEnd(64.f);
      thicknessChange->setRelativePosition(0.25f);
      frame.south.addSection(thicknessChange);
    }

    thickness = 8.f;
    thicknessChangeLength = 8.f;

    if((decorations & Decoration::EDGE_WEST_THICKNESS) != Decoration::NONE)
    {
      thicknessChange = HoloFrame::ChangeThickness::create(thicknessChangeLength, thickness, 0);
      thicknessChange->setPaddingStart(32.f);
      thicknessChange->setPaddingEnd(16.f);
      thicknessChange->setRelativePosition(0.25f);
      frame.west.addSection(thicknessChange);

      thicknessChange = HoloFrame::ChangeThickness::create(thicknessChangeLength, -thickness, 0);
      thicknessChange->setPaddingStart(48.f);
      thicknessChange->setPaddingEnd(32.f);
      thicknessChange->setRelativePosition(0.75f);
      frame.west.addSection(thicknessChange);
    }

    thicknessChangeLength = 16.f;

    if((decorations & Decoration::CORNER_SE_THICKNESS) != Decoration::NONE)
    {
      thicknessChange = HoloFrame::ChangeThickness::create(thicknessChangeLength, 0, thickness);
      thicknessChange->setPaddingStart(8.f);
      thicknessChange->setPaddingEnd(8.f);
      thicknessChange->setRelativePosition(0.3f);
      cornerSE->subSections.addSection(thicknessChange);

      thicknessChange = HoloFrame::ChangeThickness::create(thicknessChangeLength, 0, -thickness);
      thicknessChange->setPaddingStart(16.f);
      thicknessChange->setPaddingEnd(0.f);
      thicknessChange->setRelativePosition(0.1f);
      frame.east.addSection(thicknessChange);
    }

    if((decorations & Decoration::EDGE_NORTH_OFFSET) != Decoration::NONE)
    {
      offsetSection = HoloFrame::OffsetSection::create(48.f, 48.f);
      offsetSection->setRelativePosition(0.5f);
      frame.north.addSection(offsetSection);
    }

    if((decorations & Decoration::EDGE_EAST_THICKNESS_LARGE) != Decoration::NONE)
    {
      thicknessChange = HoloFrame::ChangeThickness::create(thicknessChangeLength, 0, thickness);
      thicknessChange->setPaddingStart(0.f);
      thicknessChange->setPaddingEnd(32.f);
      thicknessChange->setRelativePosition(0.f);
      frame.east.addSection(thicknessChange);

      thicknessChange = HoloFrame::ChangeThickness::create(thicknessChangeLength, 0, -thickness);
      thicknessChange->setPaddingStart(32.f);
      thicknessChange->setPaddingEnd(32.f);
      thicknessChange->setRelativePosition(0.75f);
      frame.east.addSection(thicknessChange);
    }

    if((decorations & Decoration::EDGE_WEST_THICKNESS_OUTER) != Decoration::NONE)
    {
      thicknessChange = HoloFrame::ChangeThickness::create(thicknessChangeLength, 0, thickness);
      thicknessChange->setPaddingStart(32.f);
      thicknessChange->setPaddingEnd(16.f);
      thicknessChange->setRelativePosition(0.);
      frame.west.addSection(thicknessChange);

      thicknessChange = HoloFrame::ChangeThickness::create(thicknessChangeLength, 0, -thickness);
      thicknessChange->setPaddingStart(64.f);
      thicknessChange->setPaddingEnd(32.f);
      thicknessChange->setRelativePosition(0.3f);
      frame.west.addSection(thicknessChange);
    }

    thickness = 5;
    thicknessChangeLength = 8;

    if((decorations & Decoration::EDGE_EAST_THICKNESS_SMALL) != Decoration::NONE)
    {
      thicknessChange = HoloFrame::ChangeThickness::create(thicknessChangeLength, 0, thickness);
      thicknessChange->setPaddingStart(16.f);
      thicknessChange->setPaddingEnd(32.f);
      thicknessChange->setRelativePosition(0.25f);
      frame.east.addSection(thicknessChange);

      thicknessChange = HoloFrame::ChangeThickness::create(thicknessChangeLength, 0, -thickness);
      thicknessChange->setPaddingStart(32.f);
      thicknessChange->setPaddingEnd(32.f);
      thicknessChange->setRelativePosition(0.9f);
      frame.east.addSection(thicknessChange);
    }
  }

public:
  static Ptr create(HoloTheme* holoTheme)
  {
    return Ptr(new WindowFrame(*holoTheme));
  }

  Optional<vec2> minimumSize() const override
  {
    return vec2(256, 256);
  }

  Optional<vec2> optimalSize() const override
  {
    return vec2(720, 480);
  }

  void draw(Painter &painter, const Theme::RenderData &renderData) override
  {
    const Rectangle<vec2>& allocation = renderData.allocation;
    float zPosition = renderData.zPosition;

    vec4 defaultColor = holoTheme.tweaking.windowFrame;

    defaultColor.a *= renderData.state.interpolateValues<real>(0.f,
                                                               0.4f,
                                                               1.f, 1.f, 1.f,
                                                               1.f, 1.f, 1.f);

    Themes::HoloFramePainter p(painter);
    p.setColor(defaultColor);

    zPosition -= 3*holoTheme.tweaking.zLayerDistance;

    for(int i=0; i<4; ++i)
    {
      painter.setZPosition(zPosition);
      frames[i].rectangle = allocation;
      frames[i].draw(p);

      zPosition += holoTheme.tweaking.zLayerDistance;
    }
  }

  bool isWidgetAtPoint(const vec2&, const Theme::RenderData&) const override
  {
    // ISSUE-195
    return true;
  }
};

class Button : public SimpleWidget
{
private:
  Button(HoloTheme& holoTheme)
    : SimpleWidget(holoTheme)
  {
  }

public:
  static Ptr create(HoloTheme* holoTheme)
  {
    return Ptr(new Button(*holoTheme));
  }

  Optional<vec2> minimumSize() const override
  {
    return vec2(holoTheme.tweaking.buttonMinimumSize);
  }

  Optional<vec2> optimalSize() const override
  {
    return holoTheme.tweaking.buttonOptimalSize;
  }

  void draw(Painter &painter, const Theme::RenderData &renderData) override
  {
    const Rectangle<vec2>& allocation = renderData.allocation;
    float zPosition = renderData.zPosition;

    vec4 color = renderData.state.interpolateColors(holoTheme.tweaking.buttonBorder_insensitive,
                                                    holoTheme.tweaking.buttonBorder_normal, holoTheme.tweaking.buttonBorder_hover, holoTheme.tweaking.buttonBorder_pressed,
                                                    holoTheme.tweaking.buttonBorder_focused_normal, holoTheme.tweaking.buttonBorder_focused_hover, holoTheme.tweaking.buttonBorder_focused_pressed);

    const RoundedRectangle roundedRectangle(allocation, holoTheme.tweaking.buttonCornerRadius);
    const real cornerRadius = roundedRectangle.radius();

    real thickness = interpolate(clamp(interpolationWeightOf(allocation.height(),
                                                             holoTheme.tweaking.buttonMinimumSize,
                                                             holoTheme.tweaking.buttonOptimalSize.y),
                                       0.f,
                                       1.f),
                                 holoTheme.tweaking.buttonBorderThickness_minimumSize,
                                 holoTheme.tweaking.buttonBorderThickness_optimalSize);

    HoloFrame holoFrame;

    HoloFrame::FlattenedCorner::Ptr cornerNW = HoloFrame::FlattenedCorner::create(vec2(cornerRadius));
    HoloFrame::SimpleCorner::Ptr cornerSW = HoloFrame::SimpleCorner::create();
    HoloFrame::FlattenedCorner::Ptr cornerSE = HoloFrame::FlattenedCorner::create(vec2(cornerRadius));
    HoloFrame::SimpleCorner::Ptr cornerNE = HoloFrame::SimpleCorner::create();

    holoFrame.cornerNW = cornerNW;
    holoFrame.cornerSW = cornerSW;
    holoFrame.cornerSE = cornerSE;
    holoFrame.cornerNE = cornerNE;

    Themes::HoloFramePainter p(painter);
    p.setColor(color);
    p.setInnerThickness(thickness);
    p.setOuterThickness(0.f);

    painter.setZPosition(zPosition);
    holoFrame.rectangle = allocation;
    holoFrame.draw(p);

    painter.drawRoundedRectangle(RoundedRectangle(allocation, 4), vec4(1, 0.5, 0, renderData.state.keyFocused), 2);
  }

  bool isWidgetAtPoint(const vec2&, const Theme::RenderData&) const override
  {
    // ISSUE-195
    return true;
  }
};

} // HoloTheme_SimpleWidgets

using namespace HoloTheme_SimpleWidgets;

HoloTheme::Tweaking::Tweaking()
{
  const vec4 baseColor = vec4(1, 0.5f, 0, 1);

  buttonBorder_normal = baseColor * vec4(1, 1, 1, 0.3);
  buttonBorder_insensitive = vec4(0.5f, 0.5f, 0.5f, 0.3);
  buttonBorder_hover = baseColor * vec4(1, 1, 1, 0.6);
  buttonBorder_pressed = min(vec4(1), baseColor * vec4(1.25, 1.25, 1.25, 0.8));
  buttonBorder_focused_normal = buttonBorder_normal;
  buttonBorder_focused_hover = buttonBorder_hover;
  buttonBorder_focused_pressed = buttonBorder_pressed;

  buttonBorderThickness_minimumSize = 2.f;
  buttonBorderThickness_optimalSize = 4.f;

  buttonMinimumSize = buttonBorderThickness_minimumSize*3;
  buttonOptimalSize = vec2(75, 25);
  buttonCornerRadius = buttonOptimalSize.y * 0.5f;

  zLayerDistance = 10.f;
  pressZLayerOffset = 0.75f * zLayerDistance;

  windowFrame = baseColor * vec4(1, 1, 1, 0.3);
}

HoloTheme::HoloTheme()
{
  _simpleWidgets[Types::Containers::WindowFrame] = WindowFrame::create(this);
  _simpleWidgets[Types::Containers::Button] = Button::create(this);
}

HoloTheme::Ptr HoloTheme::create()
{
  return Ptr(new HoloTheme);
}


Optional<Theme::SimpleWidget::Ptr> HoloTheme::simpleWidget(const ClassType& widgetType,
                                                           const TagSet& tags)
{
  (void)tags;
  return optionalFromHashMap(_simpleWidgets, widgetType);
}

Optional<Theme::SimpleTextWidget::Ptr> HoloTheme::simpleTextWidget(const ClassType& widgetType,
                                                                   const TagSet& tags)
{
  (void)widgetType;
  (void)tags;

  return nothing;
}

Optional<const Metrics*> HoloTheme::layoutMetrics(const ClassType& typeLayout,
                                                  const ClassType& typeWidget,
                                                  const TagSet& tagsLayout,
                                                  const TagSet& tagsWidget)
{
  (void)typeLayout;
  (void)typeWidget;
  (void)tagsLayout;
  (void)tagsWidget;

  return nothing;
}

Optional<const Metrics*> HoloTheme::layoutSpacingItemMetrics(const ClassType& typeLayout,
                                                             const ClassType& typeWidget,
                                                             const TagSet& tagsLayout,
                                                             const TagSet& tagsWidget)
{
  (void)typeLayout;
  (void)typeWidget;
  (void)tagsLayout;
  (void)tagsWidget;

  return nothing;
}

bool HoloTheme::handleClickOffset(const InOutput<Framework::Gui::Theme::AnimationData>& animationData,
                                  bool start)
{
  AnimationMap& animationMap = animationData.value.animationMap;

  if(start)
  {
    real duration = 0.125f;
    animationMap.fadeToValue(AnimationSlots::RenderState::zPositionOffset,
                             -tweaking.pressZLayerOffset,
                             duration,
                             Animation::FadeInterpolation::EASY_OUT);
  }else
  {
    real duration = 0.125f;
    animationMap.snapToDefault(AnimationSlots::RenderState::zPositionOffset,
                               duration,
                               Animation::SnapInterpolation::DAMPED);
  }
  return true;
}


} // namespace Spatial
} // namespace Ingame
} // namespace Gui
} // namespace Framework
