#ifndef FRAMEWORK_GUI_INGAME_SPATIAL_HOLOAPPEREANCE_H
#define FRAMEWORK_GUI_INGAME_SPATIAL_HOLOAPPEREANCE_H

#include "../../ingame-appearance.h"

namespace Framework {
namespace Gui {
namespace Ingame {
namespace Spatial {


class HoloAppereance : public IngameAppearance
{
public:
  typedef std::shared_ptr<HoloAppereance> Ptr;

public:
  HoloAppereance(const Renderer::Ptr& renderer, const Theme::Ptr& fallbackTheme);

  static Ptr create(const Renderer::Ptr& renderer, const Theme::Ptr& fallbackTheme);
};


} // namespace Spatial
} // namespace Ingame
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_INGAME_SPATIAL_HOLOAPPEREANCE_H
