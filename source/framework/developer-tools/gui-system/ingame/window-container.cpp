#include "window-container.h"
#include "window-manager.h"

namespace Framework {
namespace Gui {
namespace Ingame {


WindowContainer::WindowContainer(WindowManager& windowManager, const Window::Ptr& window, bool roundAllocationHint)
  : BaseWindowContainer(windowManager, window, roundAllocationHint)
{
}


} // namespace Ingame
} // namespace Gui
} // namespace Framework
