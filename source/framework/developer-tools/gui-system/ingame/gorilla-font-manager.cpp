#include "gorilla-font-manager.h"
#include "gorilla-painter.h"
#include <framework/developer-tools/developer-hud.h>
#include <base/io/log.h>

namespace Framework {
namespace Gui {
namespace Ingame {

GorillaFontManager::GorillaFontManager()
{
}

class SimpleGorillaFontManager : public GorillaFontManager
{
private:
  const size_t fontIndex;
  Font _defaultFont;

public:
  SimpleGorillaFontManager(Gorilla::GlyphData* gorillaGlyphData, size_t fontIndex)
    : fontIndex(fontIndex)
  {
    _defaultFont = Font(GorillaPainter::FontImplementation::create(gorillaGlyphData, fontIndex));
  }

  Font defaultFont() override
  {
    return _defaultFont;
  }

  static Ptr create(Gorilla::GlyphData* gorillaGlyphData, size_t fontIndex)
  {
    return Ptr(new SimpleGorillaFontManager(gorillaGlyphData, fontIndex));
  }

  static Ptr create(Gorilla::TextureAtlas* gorillaTextureAtlas, size_t fontIndex)
  {
    Gorilla::GlyphData* gorillaGlyphData = gorillaTextureAtlas->getGlyphData(fontIndex);
    return create(gorillaGlyphData, fontIndex);
  }
};

FontManager::Ptr GorillaFontManager::create(const std::shared_ptr<DeveloperHud::Layer>& devHudLayer)
{
  Gorilla::TextureAtlas* gorillaTextureAtlas = devHudLayer->gorillaScreen.getAtlas();
  Ogre::String textureName = gorillaTextureAtlas->getTexture()->getName();

  if(textureName == "dejavu.png")
  {
    return SimpleGorillaFontManager::create(gorillaTextureAtlas, 9);
  }else if(textureName == "freemono.png")
  {
    return SimpleGorillaFontManager::create(gorillaTextureAtlas, 141);
  }else
  {
    IO::Log::logError("GorillaFontManager::create(...): Unknown Texture atlas %0", textureName);
    assert("GorillaFontManager::create(...): Unknown Texture atlas." && false);
    return FontManager::createFallbackTextureManager();
  }
}


} // namespace Ingame
} // namespace Gui
} // namespace Framework
