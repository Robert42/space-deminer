#ifndef FRAMEWORK_GUI_INGAME_WINDOWCONTAINER_H
#define FRAMEWORK_GUI_INGAME_WINDOWCONTAINER_H

#include "../base-window-container.h"

#include <framework/window/user-input/mouse.h>
#include <framework/window/user-input/keyboard.h>

namespace Framework {
namespace Gui {
namespace Ingame {

class WindowManager;
class WindowContainer : public BaseWindowContainer
{
public:
  typedef std::shared_ptr<WindowContainer> Ptr;

public:
  WindowContainer(WindowManager& windowManager, const Window::Ptr& window, bool roundAllocationHint=true);
};

} // namespace Ingame
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_INGAME_WINDOWCONTAINER_H
