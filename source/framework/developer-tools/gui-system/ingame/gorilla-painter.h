#ifndef FRAMEWORK_GUI_INGAME_GORILLARENDERER_H
#define FRAMEWORK_GUI_INGAME_GORILLARENDERER_H

#include "../renderer.h"

#include <framework/developer-tools/developer-hud.h>
#include <framework/developer-tools/gorilla-helper/viewport-renderer.h>
#include <framework/developer-tools/developer-hud.h>

namespace Framework {
namespace Gui {
namespace Ingame {

class GorillaPainter : public Renderer
{
public:
  typedef std::shared_ptr<GorillaPainter> Ptr;

  class FontImplementation : public Font::Implementation
  {
  public:
    typedef std::shared_ptr<GorillaPainter::FontImplementation> Ptr;

  public:
    Gorilla::GlyphData* const gorillaGlyphData;
    const int gorillaFontIndex;

  public:
    FontImplementation(Gorilla::GlyphData* gorillaGlyphData, int gorillaFontIndex);

    vec2 textSize(const String& text) const override;
    real lineSpacing() const override;
    real lineHeight() const override;

    static Ptr create(Gorilla::GlyphData* gorillaGlyphData, int gorillaFontIndex);
  };

private:
  class LinePenImplementation;
  class OwnCustomPainter;
  class OwnCWCustomPainter;

  std::shared_ptr<DeveloperHud::Layer> devHudLayer;
  Gorilla::Layer& gorillaLayer;
  OwnCustomPainter* customRenderer;

  bool antialiasingForAxisAligned;
  real axisAlignedAntialiasing;
  real antialiasing;

private:
  GorillaPainter(const std::shared_ptr<DeveloperHud::Layer>& devHudLayer, const std::function<void()>& redraw, bool antialiasingForAxisAligned, bool counterclockwise=true);

  static Ptr create(const std::shared_ptr<DeveloperHud::Layer>& devHudLayer, const std::function<void()>& redraw, bool antialiasingForAxisAligned, bool counterclockwise=true);

public:
  ~GorillaPainter();

  static Ptr createSpatial(const std::shared_ptr<DeveloperHud::Layer>& devHudLayer, const std::function<void()>& redraw, bool counterclockwise=true);
  static Ptr createFlat(const std::shared_ptr<DeveloperHud::Layer>& devHudLayer, const std::function<void()>& redraw, bool counterclockwise=true);

  void beginRedrawing() override;
  void endRedrawing() override;
  void sheduleRedraw() override;

  void setAntialiasingHint(AntialiasingHint hint) override;

  void setWindowTransformation(const mat4& windowTransformation) override;
  void setZPosition(real z) override;
  void drawFilledTriangle(const vec4& color, const vec2& a, const vec2& b, const vec2& c) override;
  void drawFilledRectangle(const Rectangle<vec2>& rectangle, const vec4 &color) override;
  void drawFilledRectangle(const Rectangle<vec2>& rectangle, const vec4& colorUpper, const vec4& colorLower) override;
  void drawRoundedRectangle(const RoundedRectangle& rectangle, const vec4 &color, real border, real strokeOffset) override;

  void drawQuadStrip(const QVector<vec2>& verticesLeft,
                     const QVector<vec2>& verticesRight,
                     const vec4& color,
                     bool loop);

  void drawText(const String& text, const Font& font, const vec2& position, const vec4& color) override;

  LinePen startDrawingLine() override;

  void setScissorRect(const Optional< Rectangle<vec2> >& scissorRect) override;

private:
  void drawAntialiasing(bool right, bool left, const QVector<vec2>& vertices, bool loop, const vec4& color);
};

RTTI_CLASS_DECLARE(Framework::Gui::Ingame::GorillaPainter::FontImplementation)

} // namespace Ingame
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_INGAME_GORILLARENDERER_H
