#include "window-container.h"

#include <framework/window.h>

namespace Framework {
namespace Gui {
namespace Ingame {
namespace Flat {

WindowContainer::WindowContainer(WindowManager& windowManager, const Window::Ptr& window, WindowType type)
  : Ingame::WindowContainer(windowManager, window),
    type(type)
{
  _position = vec2(0);
  z = 0;

  if(type==WindowType::MAIN_WINDOW)
  {
    setFullscreenPosition();
    RenderWindow::signalWindowResized().connect(std::bind(&WindowContainer::setFullscreenPosition, this)).track(this->trackable);
  }
}


vec2 WindowContainer::currentMousePosition(real) const
{
  return vec2(Mouse::absolutePosition()) - position();
}

bool WindowContainer::canHandleMouseInputAtCurrentMousePosition() const
{
  return window->isAnyWidgetAtPoint(Mouse::absolutePosition());
}

QSet<Widget::Ptr> WindowContainer::widgetsAtCurrentMousePositionImplementation() const
{
  return window->widgetModel()->widgetsAtPoint(currentMousePosition(0));
}

void WindowContainer::setFullscreenPosition()
{
  setPosition(vec2(0));
  setWindowSize(vec2(RenderWindow::size()));
}

WindowContainer::Order WindowContainer::order() const
{
  return Order(QPair<WindowType, int>(type, z),
               QPair<real, real>(position().x, position().y));
}

const vec2& WindowContainer::position() const
{
  return _position;
}

void WindowContainer::setPosition(const vec2& position)
{
  _position = position;
  _handleMovedWindow();
}

WindowContainer::Ptr WindowContainer::createMainWindow(WindowManager& windowManager, const Window::Ptr& window)
{
  Ptr windowContainer(new WindowContainer(windowManager, window, WindowType::MAIN_WINDOW));

  return windowContainer;
}

WindowContainer::Ptr WindowContainer::createDialog(WindowManager& windowManager, const Window::Ptr& window)
{
  Ptr windowContainer(new WindowContainer(windowManager, window, WindowType::DIALOG));

  return windowContainer;
}

void WindowContainer::beginDrawing(Painter& painter)
{
  mat4 matrix(1);
  matrix = glm::translate(matrix, vec3(position(), 0.f)); // apply the window position
  matrix = glm::scale(matrix, vec3(1, 1, 0)); // ignore the z-value, so no widget will be placed outside the frustrum

  painter.setWindowTransformation(matrix);
}

void WindowContainer::endDrawing(Painter& painter)
{
  painter.setWindowTransformation(mat4(1));
}


} // namespace Flat
} // namespace Ingame
} // namespace Gui
} // namespace Framework
