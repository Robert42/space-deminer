#ifndef FRAMEWORK_GUI_INGAME_FLATWINDOWMANAGER_H
#define FRAMEWORK_GUI_INGAME_FLATWINDOWMANAGER_H

#include "../window-manager.h"

#include "./window-container.h"

#include <framework/developer-tools/developer-hud.h>

namespace Framework {
namespace Gui {
namespace Ingame {
namespace Flat {

class WindowManager final : public Ingame::WindowManager
{
private:
  QVector<WindowContainer::Ptr> windowContainers;

  const std::shared_ptr<DeveloperHud::Layer> devHudLayer;

public:
  WindowManager(InputDevice::ActionPriority inputPriority,
                InputDevice::ActionPriority inputBlockerPriority,
                const MouseCursorLayers::Layer::Ptr& mouseCursorLayer,
                const MouseModeLayers::Layer::Ptr& mouseModeLayer,
                const IngameAppearance::Ptr& appearance,
                const std::shared_ptr<DeveloperHud::Layer>& devHudLayer);
  WindowManager(InputDevice::ActionPriority inputPriority,
                InputDevice::ActionPriority inputBlockerPriority,
                const MouseCursorLayers::Layer::Ptr& mouseCursorLayer,
                const MouseModeLayers::Layer::Ptr& mouseModeLayer,
                const std::shared_ptr<DeveloperHud::Layer>& devHudLayer);
  WindowManager(InputDevice::ActionPriority inputPriority,
                InputDevice::ActionPriority inputBlockerPriority,
                const MouseCursorLayers::Layer::Ptr& mouseCursorLayer,
                const MouseModeLayers::Layer::Ptr& mouseModeLayer);
  WindowManager();

private:
  void _addMainWindow(const Window::Ptr& window) override;
  void _addDialog(const Window::Ptr& window) override;
  void _closeWindow(const BaseWindowContainer::Ptr& windowContainer) override;

public:
  InputHandler::Ptr createInputHandlerForWindow(BaseWindowContainer& windowContainer) override;

private:
  QVector<BaseWindowContainer::Ptr> _windowOrderToHandleUserInput() final override;

  void sortWindows();
  static bool containerShouldBeDrawnBefore(const WindowContainer::Ptr& a, const WindowContainer::Ptr& b);
};

} // namespace Flat
} // namespace Ingame
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_INGAME_FLATWINDOWMANAGER_H
