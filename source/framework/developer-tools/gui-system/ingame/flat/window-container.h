#ifndef FRAMEWORK_GUI_INGAME_FLATWINDOWCONTAINER_H
#define FRAMEWORK_GUI_INGAME_FLATWINDOWCONTAINER_H

#include "../window-container.h"

namespace Framework {
namespace Gui {
namespace Ingame {
namespace Flat {

class WindowContainer final : public Ingame::WindowContainer
{
public:
  typedef std::shared_ptr<WindowContainer> Ptr;

  BEGIN_ENUMERATION(WindowType,)
    MAIN_WINDOW,
    DIALOG
  ENUMERATION_BASIC_OPERATORS(WindowType)
  ENUMERATION_COMPARISON_OPERATORS(WindowType)
  END_ENUMERATION;

  typedef QPair<QPair<WindowType, int>, QPair<real, real>> Order;

public:
  Signals::Trackable trackable;

private:
  const WindowType type;
  int z;
  vec2 _position;

public:
  WindowContainer(WindowManager& windowManager, const Window::Ptr& window, WindowType type);

  static Ptr createMainWindow(WindowManager& windowManager,const Window::Ptr& window);
  static Ptr createDialog(WindowManager& windowManager,const Window::Ptr& window);

public:
  Order order() const;

  const vec2& position() const;
  void setPosition(const vec2& position);

private:
  void beginDrawing(Painter& painter) override;
  void endDrawing(Painter& painter) override;

  vec2 currentMousePosition(real zPosition) const override;
  bool canHandleMouseInputAtCurrentMousePosition() const override;

  void setFullscreenPosition();

private:
  QSet<Widget::Ptr> widgetsAtCurrentMousePositionImplementation() const override;
};

} // namespace Flat
} // namespace Ingame
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_INGAME_FLATWINDOWCONTAINER_H
