#include "window-manager.h"
#include "../gorilla-painter.h"
#include "../gorilla-font-manager.h"
#include "../ingame-appearance.h"
#include "../../themes/dark-theme.h"

#include <framework/application.h>

namespace Framework {
namespace Gui {
namespace Ingame {
namespace Flat {


class IngameEditorDevHudLayer : public DeveloperHud::Layer
{
public:
  typedef std::shared_ptr<IngameEditorDevHudLayer> Ptr;

private:
  IngameEditorDevHudLayer(ZLayer z, Gorilla::Screen& gorillaScreen)
    : DeveloperHud::Layer(z, gorillaScreen)
  {
    show();
  }

public:
  static Ptr create(ZLayer z, Gorilla::Screen& gorillaScreen)
  {
    return Ptr(new IngameEditorDevHudLayer(z, gorillaScreen));
  }

  static Ptr create()
  {
    return create(IngameEditorDevHudLayer::Layer::ZLayer::INGAME_EDITOR,
                  Application::developerHud().gorillaScreenIngameEditor());
  }
};

WindowManager::WindowManager(InputDevice::ActionPriority inputPriority,
                             InputDevice::ActionPriority inputBlockerPriority,
                             const MouseCursorLayers::Layer::Ptr& mouseCursorLayer,
                             const MouseModeLayers::Layer::Ptr& mouseModeLayer,
                             const IngameAppearance::Ptr& appearance,
                             const std::shared_ptr<DeveloperHud::Layer>& devHudLayer)
  : Ingame::WindowManager(inputPriority,
                          inputBlockerPriority,
                          mouseCursorLayer,
                          mouseModeLayer,
                          appearance),
    devHudLayer(devHudLayer)
{
}

WindowManager::WindowManager(InputDevice::ActionPriority inputPriority,
                             InputDevice::ActionPriority inputBlockerPriority,
                             const MouseCursorLayers::Layer::Ptr& mouseCursorLayer,
                             const MouseModeLayers::Layer::Ptr& mouseModeLayer,
                             const std::shared_ptr<DeveloperHud::Layer>& devHudLayer)
  : WindowManager(inputPriority,
                  inputBlockerPriority,
                  mouseCursorLayer,
                  mouseModeLayer,
                  IngameAppearance::create(Ingame::GorillaPainter::createFlat(devHudLayer,
                                                                              std::bind(&WindowManager::redraw,
                                                                                        this)),
                                           Themes::DarkTheme::create(GorillaFontManager::create(devHudLayer))),
                  devHudLayer)
{
}

WindowManager::WindowManager(InputDevice::ActionPriority inputPriority,
                             InputDevice::ActionPriority inputBlockerPriority,
                             const MouseCursorLayers::Layer::Ptr& mouseCursorLayer,
                             const MouseModeLayers::Layer::Ptr& mouseModeLayer)
  : WindowManager(inputPriority,
                  inputBlockerPriority,
                  mouseCursorLayer,
                  mouseModeLayer,
                  IngameEditorDevHudLayer::create())
{
}

WindowManager::WindowManager()
  : WindowManager(InputDevice::ActionPriority::INGAME_EDITOR,
                  InputDevice::ActionPriority::INGAME_EDITOR_BLOCKER,
                  Mouse::mouseCursorLayers().ingameEditorLayer,
                  Mouse::mouseModeLayers().ingameEditorLayer)
{
}

QVector<BaseWindowContainer::Ptr> WindowManager::_windowOrderToHandleUserInput()
{
  QVector<BaseWindowContainer::Ptr> windows;

  windows.reserve(this->windowContainers.size());

  this->sortWindows();
  for(WindowContainer::Ptr& w : this->windowContainers)
    windows.append(std::static_pointer_cast<Ingame::WindowContainer>(w));

  return windows;
}

void WindowManager::sortWindows()
{
  qStableSort(windowContainers.begin(),
              windowContainers.end(),
              containerShouldBeDrawnBefore);
}

bool WindowManager::containerShouldBeDrawnBefore(const WindowContainer::Ptr& a, const WindowContainer::Ptr& b)
{
  return a->order() < b->order();
}

void WindowManager::_addMainWindow(const Window::Ptr& window)
{
  WindowContainer::Ptr windowContainer = WindowContainer::createMainWindow(*this, window);
  windowContainers.prepend(windowContainer);
  handleAddedWindow(windowContainer);
}

void WindowManager::_addDialog(const Window::Ptr& window)
{
  WindowContainer::Ptr windowContainer = WindowContainer::createDialog(*this, window);
  windowContainers.append(windowContainer);
  handleAddedWindow(windowContainer);
}

void WindowManager::_closeWindow(const BaseWindowContainer::Ptr& windowContainer)
{
  windowContainers.remove(windowContainers.indexOf(std::static_pointer_cast<WindowContainer>(windowContainer)));
}

InputHandler::Ptr WindowManager::createInputHandlerForWindow(BaseWindowContainer& windowContainer)
{
  return createInputHandlerForWindow_BlenderStyle(windowContainer);
}


} // namespace Flat
} // namespace Ingame
} // namespace Gui
} // namespace Framework
