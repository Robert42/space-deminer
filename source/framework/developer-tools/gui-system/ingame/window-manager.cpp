#include "window-manager.h"
#include "../input-handlers/ingame-input-handler.h"

namespace Framework {
namespace Gui {
namespace Ingame {


WindowManager::WindowManager(InputDevice::ActionPriority inputPriority,
                             const Optional<InputDevice::ActionPriority>& inputBlockerPriority,
                             const MouseCursorLayers::Layer::Ptr& mouseCursorLayer, const MouseModeLayers::Layer::Ptr& mouseModeLayer,
                             const IngameAppearance::Ptr& appearance)
  : BaseWindowManager(appearance),
    mouseCursorLayer(mouseCursorLayer),
    mouseModeLayer(mouseModeLayer)
{
  this->mouseModeLayer->setMouseMode(MouseMode::INGAME);
  this->mouseModeLayer->setVisible(true);
  this->mouseCursorLayer->setCursor(DefaultMouseCursors::defaultCursor);
  this->mouseCursorLayer->setVisible(true);

  inputHandler = InputHandlers::IngameInputHandler::create(createInputHandler_BlenderStyle(),
                                                           inputPriority);

  if(inputBlockerPriority)
    signalBlocker = UserInput::createSignalBlocker(*inputBlockerPriority, true);

  signalAppereanceChanged().connect(std::bind(&WindowManager::sheduleRedraw, this)).track(this->trackable);
}

IngameAppearance::Ptr WindowManager::ingameAppereance()
{
  return std::static_pointer_cast<IngameAppearance>(this->appearance());
}

void WindowManager::sheduleRedraw()
{
  ingameAppereance()->sheduleRedraw();
}

void WindowManager::handleAddedWindow(const WindowContainer::Ptr& windowContainer)
{
  windowContainer->window->signalRedrawSheduled().connect(std::bind(&WindowManager::sheduleRedraw, this)).track(this->trackable);
}


QVector<BaseWindowContainer::Ptr> WindowManager::_windowOrderToHandleUserInput()
{
  return QVector<BaseWindowContainer::Ptr>();
}


} // namespace Ingame
} // namespace Gui
} // namespace Framework
