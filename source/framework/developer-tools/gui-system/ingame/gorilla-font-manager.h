#ifndef FRAMEWORK_GUI_INGAME_GORILLAFONTMANAGER_H
#define FRAMEWORK_GUI_INGAME_GORILLAFONTMANAGER_H

#include "../font-manager.h"

#include <framework/developer-tools/developer-hud.h>

namespace Framework {
namespace Gui {
namespace Ingame {

class GorillaFontManager : public FontManager
{
public:
  typedef std::shared_ptr<GorillaFontManager> Ptr;

public:
  GorillaFontManager();

  static FontManager::Ptr create(const std::shared_ptr<DeveloperHud::Layer>& devHudLayer);
};

} // namespace Ingame
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_INGAME_GORILLAFONTMANAGER_H
