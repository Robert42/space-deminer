#ifndef FRAMEWORK_GUI_INGAME_INGAMEAPPEARANCE_H
#define FRAMEWORK_GUI_INGAME_INGAMEAPPEARANCE_H

#include "../appearance.h"

namespace Framework {
namespace Gui {
namespace Ingame {

class IngameAppearance : public Appearance
{
public:
  typedef std::shared_ptr<IngameAppearance> Ptr;

private:
  Renderer::Ptr _renderer;
  Theme::Ptr _rootTheme;

public:
  IngameAppearance(const Renderer::Ptr& renderer, const Theme::Ptr& rootTheme);

  static Ptr create(const Renderer::Ptr& renderer, const Theme::Ptr& rootTheme);

  void sheduleRedraw();
  Renderer::Ptr renderer() override;
  Theme::Ptr rootTheme(const TagSet& tags = TagSet()) override;
};

} // namespace Ingame
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_INGAME_INGAMEAPPEARANCE_H
