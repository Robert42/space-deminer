#ifndef FRAMEWORK_GUI_WIDGETMODEL_H
#define FRAMEWORK_GUI_WIDGETMODEL_H

#include <dependencies.h>

#include "container.h"

#include <framework/developer-tools/editor/base/object-editor-2d/model.h>

#include <base/declarations.h>

namespace Framework {
namespace Gui {

/**
 * @brief The WidgetModel class stores all widgets and allows the input quickly access the widgets.
 *
 * Note, that the acceleretation is only usable for the input handler, as the animation may cause
 * the widget to be rendered at a diffeerent position (while the input position can't be animated, to prevent flickering)
 */
class WidgetModel final : public Editor::Base::ObjectEditor2D::Model
{
public:
  typedef std::shared_ptr<WidgetModel> Ptr;
  typedef QSet<Widget::Ptr> WidgetSet;
  typedef Editor::Base::ObjectEditor2D::Object Object;

  /**
   * A class used to actually set the position of a widget
   */
  class WidgetSlot final
  {
  private:
    Window* _window;
    Container* _container;
    Widget::Ptr _widget;

  public:
    WidgetSlot();
    ~WidgetSlot();

    void setWidgetBoundingRect(const Rectangle<vec2>& boundingRect);
    void setWidgetZPosition(real z);

  public:
    void setWidget(const Widget::Ptr& widget, Layout* layout=nullptr);
    void unsetWidget();
    void setWidget(const Optional<Widget::Ptr>& widget);

    void setWindow(Window* window);
    void unsetWindow();
    void setWindow(const Optional<Window*>& window);

    void setContainer(Container* container);
    void unsetContainer();
    void setContainer(const Optional<Container*>& container);

    void setWindowLayoutIndex(uint16 index);

    Optional<Widget::Ptr> widget();
    Optional<Widget::ConstPtr> widget() const;
    Optional<Window*> window();
    Optional<const Window*> window() const;
    Optional<Container*> container();
    Optional<const Container*> container() const;

  private:
    bool canBeAddedToModel() const;
    bool isAddedToModel() const;

    void addToModel();
    void removeFromModel();

    void addWidgetToContainer();
    void removeWidgetFromContainer();
  };

public:
  Signals::Trackable trackable;

private:
  real _minZ, _maxZ;

  Signals::CallableSignal<void(const Widget::Ptr&)> _signalWidgetWasAdded;
  Signals::CallableSignal<void(const Widget::Ptr&)> _signalWidgetWasRemoved;
  Signals::CallableSignal<void(const Widget::Ptr&)> _signalWidgetWasMoved;
  Signals::CallableSignal<void(const Widget::Ptr&, real)> _signalWidgetWasMovedAlongZ;
  Signals::CallableSignal<void()> _signalZBoundingsChanged;

public:
  WidgetModel();

  Signals::Signal<void(const Widget::Ptr&)>& signalWidgetAdded();
  Signals::Signal<void(const Widget::Ptr&)>& signalWidgetWasRemoved();
  Signals::Signal<void(const Widget::Ptr&)>& signalWidgetWasMoved();
  Signals::Signal<void(const Widget::Ptr&, real)>& signalWidgetWasMovedAlongZ();
  Signals::Signal<void()>& signalZBoundingsChanged();

  real minZ() const;
  real maxZ() const;

  WidgetSet allWidgets();
  WidgetSet widgetsAtPoint(const vec2& point);
  WidgetSet widgetsInRectangle(const Rectangle<vec2>& rectangle);
  WidgetSet widgetsAtRay(const Geometry::Ray& ray);

private:
  void removeWidget(const Widget::Ptr& widget);
  void addWidget(const Widget::Ptr& widget, real z, const Rectangle<vec2>& boundingRect);

  void moveWidgetZ(const Widget::Ptr& widget, real z);
  void moveWidget(const Widget::Ptr& widget, const Rectangle<vec2>& boundingRect);

private:
  void recalculateMinMaxZ();

  static bool isRayIntersectingWithWidget(const Geometry::Ray& ray, const Widget::Ptr& w);
  static Optional<vec2> intersectRayWithWidget(const Geometry::Ray& ray, const Widget::Ptr& w);
  static Optional<vec2> pointHitForRay(const Geometry::Ray& ray, real z);
  static WidgetSet castSet(const ObjectSet& objectSet);

  void forwardAddSignal(const Object::Ptr& widget);
  void forwardRemoveSignal(const Object::Ptr& widget);
  void forwardMoveSignal(const Object::Ptr& widget);

  std::pair<real, real> zBounding();
};

} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_WIDGETMODEL_H
