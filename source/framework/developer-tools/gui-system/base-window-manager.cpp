#include "base-window-manager.h"

#include "input-handlers/fallback-input-handler.h"
#include "input-handlers/concatenated-input-handler.h"
#include "input-handlers/key-mouse-event-splitter.h"
#include "input-handlers/mouse-action-input-handler.h"
#include "input-handlers/branch-off-input-handler.h"

#include "layout.h"

namespace Framework {
namespace Gui {

BaseWindowManager::BaseWindowManager(const Appearance::Ptr& appearance)
  : _appearance(appearance),
    _dirtyWindowOrder(true)
{
}

BaseWindowManager::~BaseWindowManager()
{
  for(ConnectionSet& connectionSet : _connectionsToWindow)
    connectionSet.disconnectAll();
}

void BaseWindowManager::redraw()
{
  Renderer::Ptr renderer = this->appearance()->renderer();

  renderer->beginRedrawing();

  for(const BaseWindowContainer::Ptr& w : windowOrderToHandleUserInput())
  {
    if(!w->window->rootWidget()->visible())
      continue;
    w->redraw(*renderer);
  }

  renderer->endRedrawing();
}

const Appearance::Ptr& BaseWindowManager::appearance()
{
  return _appearance;
}

void BaseWindowManager::addMainWindow(const Window::Ptr& window)
{
  _addWindow(window, std::bind(&BaseWindowManager::_addMainWindow, this, _1));
}

void BaseWindowManager::addDialog(const Window::Ptr& window)
{
  _addWindow(window, std::bind(&BaseWindowManager::_addDialog, this, _1));
}

bool BaseWindowManager::_addWindow(const Window::Ptr& window, const std::function<void(const Window::Ptr&)>& _insertWindwoIntoList)
{
  if(windowContainerForWindow(window))
  {
    IO::Log::logWarning("BaseWindowManager::_addWindow, the given window is already managed by this window-manager");
    return false;
  }

  _insertWindwoIntoList(window);

  invalidateWindowOrderToHandleUserInput();

  // We can assert this, as the window shouldn't be already added
  assert(_connectionsToWindow.find(window) == _connectionsToWindow.end());

  ConnectionSet& connectionSet = _connectionsToWindow[window];
  connectionSet.insert(window->signalInvalidatedMouseOverWidget().connect(_signalInvalidatedMouseOverWidget).trackManually());
  connectionSet.insert(window->signalInvalidatedKeyFocusedWidget().connect(_signalInvalidatedKeyFocusedWidget).trackManually());
  connectionSet.insert(window->signalMouseActionStarted().connect(std::bind(&BaseWindowManager::_sendSignalMouseActionStarted, this, _1, window)).trackManually());

  return true;
}

void BaseWindowManager::closeWindow(const Window::Ptr& window)
{
  Optional<BaseWindowContainer::Ptr> windowContainer = windowContainerForWindow(window);

  if(windowContainer)
  {
    Optional<ConnectionSet> connectionsToWindow = optionalFromHashMap(_connectionsToWindow, window);

    if(connectionsToWindow)
    {
      connectionsToWindow->disconnectAll();
      _connectionsToWindow.remove(window);
    }

    _closeWindow(*windowContainer);
    invalidateWindowOrderToHandleUserInput();
  }else
  {
    IO::Log::logWarning("BaseWindowManager::closeWindow: trying to remove a window which is not part of the WindowManager");
  }
}

const QVector<BaseWindowContainer::Ptr>& BaseWindowManager::windowOrderToHandleUserInput()
{
  if(_dirtyWindowOrder)
  {
    _cachedWindowOrderToHandleUserInput = _windowOrderToHandleUserInput();

    _dirtyWindowOrder = false;
  }

  return _cachedWindowOrderToHandleUserInput;
}

void BaseWindowManager::invalidateWindowOrderToHandleUserInput()
{
  _dirtyWindowOrder = true;

  invalidateMouseOverWidget();
  invalidateKeyFocusedWidget();
}

Optional<BaseWindowContainer::Ptr> BaseWindowManager::windowContainerForWindow(const Window::Ptr& window)
{
  for(const BaseWindowContainer::Ptr& w : windowOrderToHandleUserInput())
  {
    if(w->window == window)
      return w;
  }

  return nothing;
}

Signals::Signal<void()>& BaseWindowManager::signalInvalidatedMouseOverWidget()
{
  return _signalInvalidatedMouseOverWidget;
}

Signals::Signal<void()>& BaseWindowManager::signalInvalidatedKeyFocusedWidget()
{
  return _signalInvalidatedKeyFocusedWidget;
}

void BaseWindowManager::invalidateMouseOverWidget()
{
  _signalInvalidatedMouseOverWidget();
}

void BaseWindowManager::invalidateKeyFocusedWidget()
{
  _signalInvalidatedKeyFocusedWidget();
}

Signals::Signal<void(const InputHandlers::MouseAction::Ptr&, BaseWindowContainer&)>& BaseWindowManager::signalMouseActionStarted()
{
  return _signalMouseActionStarted;
}

Signals::Signal<void(const Widget::Ptr&, Window& window)>& BaseWindowManager::signalAskForKeyFocus()
{
  return _signalAskForKeyFocus;
}

void BaseWindowManager::_setAppereance(const Appearance::Ptr& appereance)
{
  if(this->_appearance != appereance)
  {
    this->_appearance = appereance;
    _signalAppereanceChanged();
  }
}

Signals::Signal<void()>& BaseWindowManager::signalAppereanceChanged()
{
  return _signalAppereanceChanged;
}

void BaseWindowManager::_sendSignalMouseActionStarted(const InputHandlers::MouseAction::Ptr& mouseAction, const Window::Ptr& window)
{
  Optional<BaseWindowContainer::Ptr> windowContainer = windowContainerForWindow(window);

  assert(windowContainer.valid());

  if(windowContainer)
    _signalMouseActionStarted(mouseAction, **windowContainer);
}

void BaseWindowManager::_askForKeyFocus(const Widget::Ptr& widget, const Widget::FocusHint& focusHint)
{
  Optional<Window*> optionalWindow = widget->window();

  if(!optionalWindow)
      return;

  Window* window = *optionalWindow;

  if(canBeFocused(*widget, *window, focusHint))
  {
    _signalAskForKeyFocus(widget, *window);
  }
}

bool BaseWindowManager::canBeFocused(Widget& widget, Window& window, const Widget::FocusHint& focusHint)
{
  (void)widget;

  auto focusHintToPriority = [](const Widget::FocusHint& focusHint) -> int {
    return int(focusHint.value);
  };

  if(!window.keyFocusPriority())
    return true;

  int oldPriority = focusHintToPriority(*window.keyFocusPriority());
  int newPriority = focusHintToPriority(focusHint);

  return oldPriority <= newPriority;
}

class MouseActionObserver final
{
public:
  Signals::Trackable trackable;

private:
  Optional<InputHandler::Ptr> _inputHandler;
  Optional<InputHandlers::MouseAction::Ptr> _mouseAction;

  Signals::CallableSignal<void()> _signalMouseActionStarted;
  Signals::CallableSignal<void()> _signalMouseActionFinished;

public:
  MouseActionObserver(BaseWindowManager& windowManager)
  {
    windowManager.signalMouseActionStarted().connect(std::bind(&MouseActionObserver::handleStartedMouseAction, this, _1, _2)).track(this->trackable);
  }

  MouseActionObserver(BaseWindowContainer& windowContainer)
  {
    windowContainer.window->signalMouseActionStarted().connect(std::bind(&MouseActionObserver::handleStartedMouseAction, this, _1, std::ref(windowContainer))).track(this->trackable);
  }

public:
  const Optional<InputHandler::Ptr>& inputHandler()
  {
    return _inputHandler;
  }

  const Optional<InputHandlers::MouseAction::Ptr>& mouseAction()
  {
    return _mouseAction;
  }

  Signals::Signal<void()>& signalMouseActionStarted()
  {
    return _signalMouseActionStarted;
  }

  Signals::Signal<void()>& signalMouseActionFinished()
  {
    return _signalMouseActionFinished;
  }

private:
  void handleStartedMouseAction(const InputHandlers::MouseAction::Ptr& mouseAction, BaseWindowContainer& windowContainer)
  {
    this->_inputHandler = windowContainer.inputHandler;
    this->_mouseAction = mouseAction;

    mouseAction->signalActionFinished().connect(std::bind(&MouseActionObserver::removeWindow, this, mouseAction)).track(this->trackable);

    _signalMouseActionStarted();
  }

  void removeWindow(const InputHandlers::MouseAction::Ptr& mouseAction)
  {
    assert(this->_mouseAction.valid() == this->_inputHandler.valid());
    if(this->_mouseAction && this->_mouseAction == mouseAction)
    {
      this->_inputHandler.reset();
      this->_mouseAction.reset();
    }

    if(!this->_mouseAction.valid())
      _signalMouseActionFinished();
  }
};


class WindowManagerMouseInputHandler : public InputHandlers::MouseFocusInputHandler
{
public:
  BaseWindowManager& baseWindowManager;
  MouseActionObserver mouseActionObserver;
  Signals::Trackable trackable;

public:
  WindowManagerMouseInputHandler(BaseWindowManager& baseWindowManager)
    : baseWindowManager(baseWindowManager),
      mouseActionObserver(baseWindowManager)
  {
    baseWindowManager.signalInvalidatedMouseOverWidget().connect(std::bind(&WindowManagerMouseInputHandler::onInvalidateFocusedInputHandler,
                                                                           this)).track(this->trackable);

    mouseActionObserver.signalMouseActionStarted().connect(std::bind(&WindowManagerMouseInputHandler::onInvalidateFocusedInputHandler,
                                                                     this)).track(this->trackable);
    mouseActionObserver.signalMouseActionFinished().connect(std::bind(&WindowManagerMouseInputHandler::onInvalidateFocusedInputHandler,
                                                                      this)).track(this->trackable);
  }

  QVector<InputHandlers::FocusableInputHandler::Ptr> inputHandlersForCursorPosition() override
  {
    QVector<InputHandlers::FocusableInputHandler::Ptr> inputHandlers;
    QVector<BaseWindowContainer::Ptr> windowContainers = baseWindowManager.windowOrderToHandleUserInput();

    // When there's a running mouse-action, no other widgets should get the mouse focus (or be drawn as if it had it)
    if(mouseActionObserver.mouseAction())
      return inputHandlers;

    inputHandlers.reserve(windowContainers.size());

    for(const BaseWindowContainer::Ptr& windowContainer : windowContainers)
    {
      if(windowContainer->canHandleMouseInputAtCurrentMousePosition())
        inputHandlers.append(windowContainer->inputHandler);
    }

    return inputHandlers;
  }
};


class WindowMouseInputHandler : public InputHandlers::MouseFocusInputHandler
{
public:
  BaseWindowContainer& windowContainer;
  MouseActionObserver mouseActionObserver;
  Signals::Trackable trackable;

public:
  WindowMouseInputHandler(BaseWindowContainer& windowContainer)
    : windowContainer(windowContainer),
      mouseActionObserver(windowContainer)
  {
    windowContainer.windowManager.signalInvalidatedMouseOverWidget().connect(std::bind(&WindowMouseInputHandler::onInvalidateFocusedInputHandler,
                                                                                       this)).track(this->trackable);

    mouseActionObserver.signalMouseActionStarted().connect(std::bind(&WindowMouseInputHandler::onInvalidateFocusedInputHandler,
                                                                     this)).track(this->trackable);
    mouseActionObserver.signalMouseActionFinished().connect(std::bind(&WindowMouseInputHandler::onInvalidateFocusedInputHandler,
                                                                      this)).track(this->trackable);
  }

  QVector<InputHandlers::FocusableInputHandler::Ptr> inputHandlersForCursorPosition() override
  {
    windowContainer.window->updateAllVisibleWidgets();

    QVector<InputHandlers::FocusableInputHandler::Ptr> inputHandlers;
    QVector<Widget::Ptr> widgets = windowContainer.widgetsAtCurrentMousePosition();

    // When there's a running mouse-action, no other widgets should get the mouse focus (or be drawn as if it had it)
    if(mouseActionObserver.mouseAction())
      return inputHandlers;

    inputHandlers.reserve(widgets.size());

    for(const Widget::Ptr& widget : widgets)
    {
      if(widget->canHaveMouseFocus())
        inputHandlers.append(*widget->inputHandler());
    }

    return inputHandlers;
  }
};


class DefaultButtonInputHandler : public InputHandlers::FallbackInputHandler
{
public:
  BaseWindowContainer& windowContainer;
  QSet<KeyCode> defaultWidgetKeys;
  QSet<KeyCode> defaultEscapeWidgetKeys;

public:
  DefaultButtonInputHandler(BaseWindowContainer& windowContainer,
                            const QSet<KeyCode>& defaultWidgetKeys,
                            const QSet<KeyCode>& defaultEscapeWidgetKeys)
    : windowContainer(windowContainer),
      defaultWidgetKeys(defaultWidgetKeys),
      defaultEscapeWidgetKeys(defaultEscapeWidgetKeys)
  {
    if(!(defaultWidgetKeys&defaultEscapeWidgetKeys).empty())
      IO::Log::logWarning("DefaultButtonInputHandler::DefaultButtonInputHandler: defaultWidgetKeys and defaultEscapeWidgetKeys are not disjunct");
  }

  bool handleKeyPressed(KeyCode keyCode) override
  {
    Optional<InputHandler::Ptr> optionalInputHandler = inputHandlerForKey(keyCode);
    if(!optionalInputHandler)
      return false;
    const InputHandler::Ptr& inputHandler = *optionalInputHandler;

    return inputHandler->handleKeyPressed(keyCode);
  }

  bool handleKeyReleased(KeyCode keyCode) override
  {
    Optional<InputHandler::Ptr> optionalInputHandler = inputHandlerForKey(keyCode);
    if(!optionalInputHandler)
      return false;
    const InputHandler::Ptr& inputHandler = *optionalInputHandler;

    return inputHandler->handleKeyReleased(keyCode);
  }

private:
  Optional<InputHandler::Ptr> inputHandlerForKey(KeyCode keyCode) const
  {
    Optional<Widget::Ptr> optionalWidget = widgetForKey(keyCode);
    if(!optionalWidget)
      return nothing;
    const Widget::Ptr& widget = *optionalWidget;

    return widget->inputHandler();
  }

  Optional<Widget::Ptr> widgetForKey(KeyCode keyCode) const
  {
    if(defaultWidgetKeys.contains(keyCode))
      return windowContainer.window->defaultWidget();
    else if(defaultEscapeWidgetKeys.contains(keyCode))
      return windowContainer.window->defaultEscapeWidget();
    else
      return nothing;
  }
};

class MouseActionFocusWindowEnforcmenet final : public InputHandlers::DelegatingInputHandler
{
private:
  MouseActionObserver mouseActionObserver;

public:
  MouseActionFocusWindowEnforcmenet(BaseWindowManager& windowManager)
    : mouseActionObserver(windowManager)
  {
  }

private:
  InputHandler::Ptr inputHandlerDelegatedTo() final override
  {
    if(mouseActionObserver.inputHandler())
      return *mouseActionObserver.inputHandler();
    else
      return InputHandler::fallbackInputHandler();
  }
};


InputHandlers::MouseFocusInputHandler::Ptr BaseWindowManager::createMouseFocusInputHandler()
{
  return InputHandlers::MouseFocusInputHandler::Ptr(new WindowManagerMouseInputHandler(*this));
}

InputHandlers::MouseFocusInputHandler::Ptr BaseWindowManager::createMouseFocusInputHandlerForWindow(BaseWindowContainer& windowContainer)
{
  return InputHandlers::MouseFocusInputHandler::Ptr(new WindowMouseInputHandler(windowContainer));
}

InputHandler::Ptr BaseWindowManager::createMouseActionFocusWindowEnforcmenet()
{
  return InputHandler::Ptr(new MouseActionFocusWindowEnforcmenet(*this));
}

InputHandler::Ptr BaseWindowManager::createDefaultButtonInputHandler(BaseWindowContainer& windowContainer,
                                                                     const QSet<KeyCode>& defaultWidgetKeys,
                                                                     const QSet<KeyCode>& defaultEscapeWidgetKeys)
{
  return InputHandler::Ptr(new DefaultButtonInputHandler(windowContainer,
                                                         defaultWidgetKeys,
                                                         defaultEscapeWidgetKeys));
}


class WindowManagerKeyFocusInputHandler : public InputHandlers::KeyFocusInputHandler
{
private:
  BaseWindowManager& windowManager;
  Signals::Trackable trackable;

public:
  WindowManagerKeyFocusInputHandler(BaseWindowManager& windowManager, bool forceHavingFocusedWindow)
    : KeyFocusInputHandler(forceHavingFocusedWindow),
      windowManager(windowManager)
  {
    onInvalidateFocusedInputHandler();

    windowManager.signalInvalidatedKeyFocusedWidget().connect(std::bind(&WindowManagerKeyFocusInputHandler::onInvalidateFocusedInputHandler,
                                                                        this)).track(this->trackable);
  }

  Optional<InputHandlers::FocusableInputHandler::Ptr> searchFallback() override
  {
    for(const BaseWindowContainer::Ptr& container : windowManager.windowOrderToHandleUserInput())
    {
      Window& window = *container->window;

      if(window.rootWidget()->visible() && window.rootWidget()->sensitive() && container->inputHandler->keySensitive())
        return container->inputHandler;
    }

    return nothing;
  }
};

InputHandlers::KeyFocusInputHandler::Ptr BaseWindowManager::createKeyFocusInputHandler(bool forceHavingFocusedWindow)
{
  return InputHandlers::KeyFocusInputHandler::Ptr(new WindowManagerKeyFocusInputHandler(*this, forceHavingFocusedWindow));
}


class WindowKeyFocusInputHandler : public InputHandlers::KeyFocusInputHandler
{
private:
  BaseWindowContainer& windowContainer;
  Signals::Trackable trackable;

public:
  WindowKeyFocusInputHandler(BaseWindowContainer& windowContainer, bool forceHavingFocusedWindow)
    : KeyFocusInputHandler(forceHavingFocusedWindow),
      windowContainer(windowContainer)
  {
    BaseWindowManager& windowManager = windowContainer.windowManager;

    onInvalidateFocusedInputHandler();

    windowContainer.window->signalInvalidatedKeyFocusedWidget().connect(std::bind(&WindowKeyFocusInputHandler::onInvalidateFocusedInputHandler,
                                                                                  this)).track(this->trackable);
    windowManager.signalAskForKeyFocus().connect(std::bind(&WindowKeyFocusInputHandler::onAskForKeyFocus,
                                                           this,
                                                           _1,
                                                           _2)).track(this->trackable);
  }

  Optional<InputHandlers::FocusableInputHandler::Ptr> searchFallback() override
  {
    Optional<Widget::Ptr> optionalWidget = windowContainer.window->findFirstKeyFocusableWidget();
    if(!optionalWidget)
      return nothing;

    Widget::Ptr widget = *optionalWidget;

    return widget->inputHandler();
  }

  void onAskForKeyFocus(const Widget::Ptr& widget, Window& window)
  {
    if(windowContainer.window.get() != &window)
      return;

    Optional<InputHandlers::FocusableInputHandler::Ptr> inputHandler = widget->inputHandler();
    if(inputHandler)
      setFocusedInputHandler(*inputHandler);
  }
};

class BaseWidgetNavigator : public InputHandlers::FallbackInputHandler
{
public:
  BaseWindowContainer& windowContainer;

  BaseWidgetNavigator(BaseWindowContainer& windowContainer)
    : windowContainer(windowContainer)
  {
  }

  virtual Optional<Layout::Navigation::Value> navigationForKey(KeyCode keyCode) = 0;

  bool handleKeyPressed(KeyCode key) final override
  {
    Optional<Layout::Navigation::Value> navigation = navigationForKey(key);

    if(!navigation)
      return false;

    navigate(*navigation);

    return true;
  }

  void navigate(Layout::Navigation navigation)
  {
    Optional<Widget::Ptr> optionalWidget = windowContainer.window->widgetWithKeyFocus();

    if(!optionalWidget)
      return;

    Widget::Ptr widget = *optionalWidget;

    Optional<Layout::Ptr> optionalLayout = widget->layout();

    if(!optionalLayout)
      return;

    Optional<Widget::Ptr> nextWidget = Layout::navigateToNextWidget(widget, navigation);

    if(nextWidget)
      nextWidget.value()->askForKeyFocus(Widget::FocusHint::NAVIGATION_BY_USER);
  }
};

class TabWidgetNavigator : public BaseWidgetNavigator
{
public:
  typedef std::shared_ptr<TabWidgetNavigator> Ptr;

  TabWidgetNavigator(BaseWindowContainer& windowContainer)
    : BaseWidgetNavigator(windowContainer)
  {
  }

  Optional<Layout::Navigation::Value> navigationForKey(KeyCode keyCode)
  {
    if(keyCode != KeyCode::TAB)
      return nothing;

    if(Keyboard::areOnlyModifierDown(KeyModifier::SHIFT))
      return Layout::Navigation::TAB_ORDER_REVERSE;
    if(!Keyboard::isAnyModifierDown())
      return Layout::Navigation::TAB_ORDER;

    return nothing;
  }

  static Ptr create(BaseWindowContainer& windowContainer)
  {
    return Ptr(new TabWidgetNavigator(windowContainer));
  }
};

class ArrowWidgetNavigator : public BaseWidgetNavigator
{
public:
  typedef std::shared_ptr<ArrowWidgetNavigator> Ptr;

  ArrowWidgetNavigator(BaseWindowContainer& windowContainer)
    : BaseWidgetNavigator(windowContainer)
  {
  }

  Optional<Layout::Navigation::Value> navigationForKey(KeyCode keyCode)
  {
    switch(keyCode.value)
    {
    case KeyCode::ARROW_LEFT:
      return Layout::Navigation::LEFT;
    case KeyCode::ARROW_RIGHT:
      return Layout::Navigation::RIGHT;
    case KeyCode::ARROW_DOWN:
      return Layout::Navigation::DOWN;
    case KeyCode::ARROW_UP:
      return Layout::Navigation::UP;
    default:
      return nothing;
    }
  }

  static Ptr create(BaseWindowContainer& windowContainer)
  {
    return Ptr(new ArrowWidgetNavigator(windowContainer));
  }
};

class WASDWidgetNavigator : public BaseWidgetNavigator
{
public:
  typedef std::shared_ptr<WASDWidgetNavigator> Ptr;

  WASDWidgetNavigator(BaseWindowContainer& windowContainer)
    : BaseWidgetNavigator(windowContainer)
  {
  }

  Optional<Layout::Navigation::Value> navigationForKey(KeyCode keyCode)
  {
    switch(keyCode.value)
    {
    case KeyCode::A:
      return Layout::Navigation::LEFT;
    case KeyCode::D:
      return Layout::Navigation::RIGHT;
    case KeyCode::S:
      return Layout::Navigation::DOWN;
    case KeyCode::W:
      return Layout::Navigation::UP;
    default:
      return nothing;
    }
  }

  static Ptr create(BaseWindowContainer& windowContainer)
  {
    return Ptr(new WASDWidgetNavigator(windowContainer));
  }
};

class JoystickWidgetNavigator : public BaseWidgetNavigator
{
public:
  typedef std::shared_ptr<JoystickWidgetNavigator> Ptr;

  JoystickWidgetNavigator(BaseWindowContainer& windowContainer)
    : BaseWidgetNavigator(windowContainer)
  {
  }


  Optional<Layout::Navigation::Value> navigationForKey(KeyCode)
  {
    // ISSUE-197: create ticket, that this class should be able to handle Joystick input
    return nothing;
  }

  static Ptr create(BaseWindowContainer& windowContainer)
  {
    return Ptr(new JoystickWidgetNavigator(windowContainer));
  }
};

InputHandlers::KeyFocusInputHandler::Ptr BaseWindowManager::createKeyFocusInputHandlerForWindow(BaseWindowContainer& windowContainer,
                                                                                                bool forceHavingFocusedWindow)
{
  return InputHandlers::KeyFocusInputHandler::Ptr(new WindowKeyFocusInputHandler(windowContainer,
                                                                                 forceHavingFocusedWindow));
}

InputHandler::Ptr BaseWindowManager::createKeyFocusNavigationInputHandler()
{
  // ISSUE-197: INPUT: implement an input handler allowing to navigate between multiple windows by using Ctrl+Tab, and Ctrl+Alt+Down
  return InputHandler::fallbackInputHandler();
}

InputHandler::Ptr BaseWindowManager::createKeyFocusNavigationInputHandlerForWindow(BaseWindowContainer& windowContainer, bool gameUi)
{
  if(gameUi)
    return InputHandlers::ConcatenatedBasedInputHandler::create({TabWidgetNavigator::create(windowContainer),
                                                                 ArrowWidgetNavigator::create(windowContainer),
                                                                 WASDWidgetNavigator::create(windowContainer),
                                                                 JoystickWidgetNavigator::create(windowContainer)});
  else
    return TabWidgetNavigator::create(windowContainer);
}

typedef InputHandlers::BranchOffInputHandler BranchOffInputHandler;
typedef InputHandlers::KeyMouseEventSplitter KeyMouseEventSplitter;

InputHandler::Ptr BaseWindowManager::createInputHandler_BlenderStyle()
{
  InputHandler::Ptr mouseActionHandler = createMouseActionFocusWindowEnforcmenet();
  InputHandler::Ptr keyFocusInputHandling = createKeyFocusInputHandler(true);
  InputHandler::Ptr mouseOverInputHandling = createMouseFocusInputHandler();
  InputHandler::Ptr keyFocusNavigationInputHandling = createKeyFocusNavigationInputHandler();

  InputHandler::Ptr keyBranch = InputHandlers::ConcatenatedBasedInputHandler::create({keyFocusInputHandling,
                                                                                      keyFocusNavigationInputHandling,
                                                                                      mouseOverInputHandling});

  return BranchOffInputHandler::create(mouseActionHandler,
                                       InputHandlers::KeyMouseEventSplitter::create(keyBranch,
                                                                                    mouseOverInputHandling));
}

InputHandler::Ptr BaseWindowManager::createInputHandlerForWindow_BlenderStyle(BaseWindowContainer& windowContainer)
{
  InputHandler::Ptr mouseActionHandler = InputHandlers::MouseActionInputHandler::create(windowContainer.window);
  InputHandler::Ptr defaultActionInputHandling = createDefaultButtonInputHandler(windowContainer);
  InputHandler::Ptr keyFocusInputHandling = createKeyFocusInputHandlerForWindow(windowContainer, false);
  InputHandler::Ptr mouseOverInputHandling = createMouseFocusInputHandlerForWindow(windowContainer);
  InputHandler::Ptr keyFocusNavigationInputHandling = createKeyFocusNavigationInputHandlerForWindow(windowContainer, false);

  InputHandler::Ptr keyBranch = InputHandlers::ConcatenatedBasedInputHandler::create({keyFocusInputHandling,
                                                                                      defaultActionInputHandling,
                                                                                      keyFocusNavigationInputHandling,
                                                                                      mouseOverInputHandling});

  return BranchOffInputHandler::create(mouseActionHandler,
                                       KeyMouseEventSplitter::create(keyBranch,
                                                                     mouseOverInputHandling));
}

InputHandler::Ptr BaseWindowManager::createInputHandlerForWindow_GameUIStyle(BaseWindowContainer& windowContainer)
{
  InputHandler::Ptr mouseActionHandler = InputHandlers::MouseActionInputHandler::create(windowContainer.window);
  InputHandler::Ptr defaultActionInputHandling = createDefaultButtonInputHandler(windowContainer);
  InputHandler::Ptr keyFocusInputHandling = createKeyFocusInputHandlerForWindow(windowContainer, true);
  InputHandler::Ptr mouseOverInputHandling = createMouseFocusInputHandlerForWindow(windowContainer);
  InputHandler::Ptr keyFocusNavigationInputHandling = createKeyFocusNavigationInputHandlerForWindow(windowContainer, true);

  InputHandler::Ptr keyBranch = InputHandlers::ConcatenatedBasedInputHandler::create({keyFocusInputHandling,
                                                                                      defaultActionInputHandling,
                                                                                      keyFocusNavigationInputHandling});

  return BranchOffInputHandler::create(mouseActionHandler,
                                       KeyMouseEventSplitter::create(keyBranch,
                                                                     mouseOverInputHandling,
                                                                     true));
}

// ====

void BaseWindowManager::ConnectionSet::disconnectAll()
{
  for(Signals::Connection connection : this->connections)
    connection.disconnect();
}

void BaseWindowManager::ConnectionSet::insert(const Signals::Connection& connection)
{
  this->connections.insert(connection);
}


} // namespace Gui
} // namespace Framework
