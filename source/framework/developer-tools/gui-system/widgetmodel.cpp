#include "widgetmodel.h"
#include "window.h"

#include <base/geometry/ray.h>

namespace Framework {
namespace Gui {

using Geometry::Ray;


WidgetModel::WidgetModel()
  : Model(vec2(512)),
    _minZ(0.f),
    _maxZ(0.f)
{
  signalObjectWasAdded().connect(std::bind(&WidgetModel::forwardAddSignal, this, _1)).track(this->trackable);
  signalObjectWasRemoved().connect(std::bind(&WidgetModel::forwardRemoveSignal, this, _1)).track(this->trackable);
  signalObjectWasMoved().connect(std::bind(&WidgetModel::forwardMoveSignal, this, _1)).track(this->trackable);
}

Signals::Signal<void(const Widget::Ptr&)>& WidgetModel::signalWidgetAdded()
{
  return _signalWidgetWasAdded;
}

Signals::Signal<void(const Widget::Ptr&)>& WidgetModel::signalWidgetWasRemoved()
{
  return _signalWidgetWasRemoved;
}

Signals::Signal<void(const Widget::Ptr&, real)>& WidgetModel::signalWidgetWasMovedAlongZ()
{
  return _signalWidgetWasMovedAlongZ;
}

Signals::Signal<void()>& WidgetModel::signalZBoundingsChanged()
{
  return _signalZBoundingsChanged;
}

real WidgetModel::minZ() const
{
  return _minZ;
}

real WidgetModel::maxZ() const
{
  return _maxZ;
}

void WidgetModel::removeWidget(const Widget::Ptr& widget)
{
  assert(&widget->model() == this);

  real z = widget->zPosition();

  widget->sheduleRedraw();
  removeObject(widget);

  if(minZ() == z || maxZ()==z)
  {
    auto oldZBonding = zBounding();

    recalculateMinMaxZ();

    if(zBounding() != oldZBonding)
      _signalZBoundingsChanged();
  }
}

void WidgetModel::moveWidgetZ(const Widget::Ptr& widget, real z)
{
  auto oldZBonding = zBounding();

  const real currentZ = widget->zPosition();
  if((minZ() == currentZ && z>minZ())||
     (maxZ() == currentZ && z<maxZ()))
  {
    recalculateMinMaxZ();
  }else
  {
    _minZ = min(z, minZ());
    _maxZ = max(z, maxZ());
  }

  widget->_zPosition = z;

  widget->onMovedAlongZ();
  _signalWidgetWasMovedAlongZ(widget, z);

  if(zBounding() != oldZBonding)
    _signalZBoundingsChanged();
  widget->sheduleRedraw();
}

void WidgetModel::moveWidget(const Widget::Ptr& widget, const Rectangle<vec2>& boundingRect)
{
  widget->sheduleRedraw();

  moveObject(widget,
             Rectangle<vec2>(boundingRect.min(),
                             boundingRect.min() + max(widget->minimumSize(), boundingRect.size())));
  widget->sheduleRedraw();
}

void WidgetModel::addWidget(const Widget::Ptr& widget, real z, const Rectangle<vec2>& boundingRect)
{
  assert(&widget->model() == nullptr);
  auto oldZBonding = zBounding();

  _minZ = min(z, _minZ);
  _maxZ = max(z, _maxZ);

  widget->_zPosition = z;
  addObject(widget,
            Rectangle<vec2>(boundingRect.min(),
                            boundingRect.min() + max(widget->minimumSize(), boundingRect.size())));

  if(zBounding() != oldZBonding)
    _signalZBoundingsChanged();
  widget->sheduleRedraw();
}


WidgetModel::WidgetSet WidgetModel::allWidgets()
{
  return castSet(allObjects());
}

WidgetModel::WidgetSet WidgetModel::widgetsAtPoint(const vec2& point)
{
  return castSet(objectsAtPoint(point));
}

WidgetModel::WidgetSet WidgetModel::widgetsInRectangle(const Rectangle<vec2>& rectangle)
{
  return castSet(objectsInRectangle(rectangle));
}

WidgetModel::WidgetSet WidgetModel::widgetsAtRay(const Geometry::Ray& ray)
{
  Optional<vec2> a, b;

  if(!(a=pointHitForRay(ray, minZ())) ||
     !(b=pointHitForRay(ray, maxZ())))
  {
    // Ray is parallel to widgets. widgets are infinite thin => no intersection
    return WidgetSet();
  }

  WidgetSet hitCandidates = widgetsInRectangle(Rectangle<vec2>(*a, *b));
  WidgetSet widgets;

  for(const Widget::Ptr& w : hitCandidates)
  {
    if(isRayIntersectingWithWidget(ray, w))
      widgets.insert(w);
  }

  return widgets;
}

void WidgetModel::recalculateMinMaxZ()
{
  if(allObjects().empty())
  {
    _minZ = _maxZ = 0.f;
    return;
  }

  _minZ = std::numeric_limits<real>::max();
  _maxZ = -std::numeric_limits<real>::max();

  for(const Widget::Ptr& w : allWidgets())
  {
    real z = w->zPosition();

    _minZ = min(z, _minZ);
    _maxZ = max(z, _maxZ);
  }
}

bool WidgetModel::isRayIntersectingWithWidget(const Geometry::Ray& ray, const Widget::Ptr& w)
{
  return intersectRayWithWidget(ray, w);
}

Optional<vec2> WidgetModel::intersectRayWithWidget(const Geometry::Ray& ray, const Widget::Ptr& w)
{
  Optional<vec2> point = pointHitForRay(ray, w->zPosition());
    // Ray is parallel to widget. widgets are infinite thin => no intersection
  if(!point)
    return nothing;

  if(!w->boundingRect().contains(*point))
    return nothing;

  return point;
}

Optional<vec2> WidgetModel::pointHitForRay(const Geometry::Ray& ray, real z)
{
  Optional<vec3> maybeIntersection = ray.intersectionWithZPlane(z);

  if(!maybeIntersection)
    return nothing;

  return maybeIntersection->xy();
}

WidgetModel::WidgetSet WidgetModel::castSet(const ObjectSet& objectSet)
{
  WidgetSet widgetSet;

  for(const Object::Ptr& o : objectSet)
    widgetSet.insert(std::static_pointer_cast<Widget>(o));

  return widgetSet;
}

void WidgetModel::forwardAddSignal(const Object::Ptr& widget)
{
  const Widget::Ptr& w = std::static_pointer_cast<Widget>(widget);

  w->onAddedToWindow();
  _signalWidgetWasAdded(w);
}

void WidgetModel::forwardRemoveSignal(const Object::Ptr& widget)
{
  const Widget::Ptr& w = std::static_pointer_cast<Widget>(widget);

  w->onRemovedFromWindow();
  _signalWidgetWasRemoved(w);
}

void WidgetModel::forwardMoveSignal(const Object::Ptr& widget)
{
  const Widget::Ptr& w = std::static_pointer_cast<Widget>(widget);

  w->onMoved();
  _signalWidgetWasMoved(w);
}

std::pair<real, real> WidgetModel::zBounding()
{
  return std::pair<real, real>(minZ(), maxZ());
}

// ====

WidgetModel::WidgetSlot::WidgetSlot()
  : _window(nullptr),
    _container(nullptr),
    _widget(nullptr)
{
}

WidgetModel::WidgetSlot::~WidgetSlot()
{
  unsetWidget();
  unsetWindow();
}

void WidgetModel::WidgetSlot::setWidget(const Widget::Ptr& widget, Layout* layout)
{
  if(this->_widget != widget)
  {
    assert(!widget->_includedToWidgetSlot);
    assert(!widget->window().valid());
    assert(!widget->parent().valid());

    unsetWidget();

    this->_widget = widget;
    this->_widget->_includedToWidgetSlot = true;
    this->_widget->_layout = layout;

    addWidgetToContainer();
    addToModel();
  }
}

void WidgetModel::WidgetSlot::unsetWidget()
{
  if(this->_widget != nullptr)
  {
    removeWidgetFromContainer();
    removeFromModel();

    this->_widget->_includedToWidgetSlot = false;
    this->_widget->_layout = nullptr;
    this->_widget.reset();
  }
}

void WidgetModel::WidgetSlot::setWidget(const Optional<Widget::Ptr>& widget)
{
  if(widget)
    setWidget(*widget);
  else
    unsetWidget();
}

void WidgetModel::WidgetSlot::setWindow(Window* window)
{
  if(this->_window != window)
  {
    unsetWindow();

    Optional<Container*> container = this->container();

    assert(!container || ((*container)->window() && window==*(*container)->window()));

    this->_window = window;

    addToModel();
  }
}

void WidgetModel::WidgetSlot::unsetWindow()
{
  if(this->_window != nullptr)
  {
    removeFromModel();

    this->_window = nullptr;
  }
}

void WidgetModel::WidgetSlot::setWindow(const Optional<Window*>& window)
{
  if(window)
    setWindow(*window);
  else
    unsetWindow();
}

void WidgetModel::WidgetSlot::setContainer(Container *container)
{
  if(this->_container != container)
  {
    unsetContainer();

    this->_container = container;

    addWidgetToContainer();

    setWindow(container->window());
  }
}

void WidgetModel::WidgetSlot::unsetContainer()
{
  unsetWindow();

  if(this->_container != nullptr)
  {
    removeWidgetFromContainer();
    removeFromModel();
    this->_container = nullptr;
  }
}

void WidgetModel::WidgetSlot::setContainer(const Optional<Container *> &container)
{
  if(container)
    setContainer(*container);
  else
    unsetContainer();
}

void WidgetModel::WidgetSlot::setWindowLayoutIndex(uint16 index)
{
  if(widget())
  {
    _widget->_index.setLayoutOrder(index);
  }
}

Optional<Widget::Ptr> WidgetModel::WidgetSlot::widget()
{
  if(_widget != nullptr)
    return _widget;
  else
    return nothing;
}

Optional<Widget::ConstPtr> WidgetModel::WidgetSlot::widget() const
{
  if(_widget != nullptr)
    return Widget::ConstPtr(_widget);
  else
    return nothing;
}

Optional<Window*> WidgetModel::WidgetSlot::window()
{
  if(_window != nullptr)
    return _window;
  else
    return nothing;
}

Optional<const Window*> WidgetModel::WidgetSlot::window() const
{
  if(_window != nullptr)
  return _window;
  else
    return nothing;
}

Optional<Container*> WidgetModel::WidgetSlot::container()
{
  if(_container != nullptr)
    return _container;
  else
    return nothing;
}

Optional<const Container*> WidgetModel::WidgetSlot::container() const
{
  if(_container != nullptr)
    return _container;
  else
    return nothing;
}

bool WidgetModel::WidgetSlot::canBeAddedToModel() const
{
  return widget() && window();
}

bool WidgetModel::WidgetSlot::isAddedToModel() const
{
  Optional<Widget::ConstPtr> widget = this->widget();

  return widget && (*widget)->window();
}

void WidgetModel::WidgetSlot::setWidgetBoundingRect(const Rectangle<vec2>& boundingRect)
{
  Optional<Widget::Ptr> widget = this->widget();

  if(isAddedToModel())
    this->_window->widgetModel()->moveWidget(*widget, boundingRect);
}

void WidgetModel::WidgetSlot::setWidgetZPosition(real z)
{
  Optional<Widget::Ptr> widget = this->widget();

  if(isAddedToModel())
    this->_window->widgetModel()->moveWidgetZ(*widget, z);
}

void WidgetModel::WidgetSlot::addToModel()
{
  if(canBeAddedToModel() && !isAddedToModel())
  {
    _widget->_window = _window;
    _window->widgetModel()->addWidget(_widget,
                                      this->_widget->zPosition(),
                                      this->_widget->boundingRect());
  }
}

void WidgetModel::WidgetSlot::removeFromModel()
{
  if(isAddedToModel())
  {
    _widget->_window = nullptr;

    _window->widgetModel()->removeWidget(this->_widget);
  }
}

void WidgetModel::WidgetSlot::addWidgetToContainer()
{
  if(widget() && container())
  {
    if(this->_widget->_parent != _container)
    {
      assert(!this->_widget->parent());

      this->_container->onChildAdded();
      this->_widget->_parent = _container;
      this->_widget->onAddedToContainer();
    }
  }
}

void WidgetModel::WidgetSlot::removeWidgetFromContainer()
{
  if(widget() && container())
  {
    if(this->_widget->_parent == _container)
    {
      this->_container->onChildAboutToBeRemoved();
      this->_widget->onAboutToBeRemovedFromContainer();
      this->_widget->_parent = nullptr;
    }

    assert(this->_widget->_parent == nullptr);
  }
}


} // namespace Gui
} // namespace Framework
