#ifndef FRAMEWORK_GUI_LAYOUTS_BOX_H
#define FRAMEWORK_GUI_LAYOUTS_BOX_H

#include "base-box.h"

namespace Framework {
namespace Gui {
namespace Layouts {

class Box : public BaseBox
{
public:
  typedef std::shared_ptr<Box> Ptr;
  typedef std::weak_ptr<Box> WeakPtr;

public:
  Box(Direction direction = Direction::VERTICAL);
  Box(Direction direction, real spacing);

  static Ptr create(Direction direction = Direction::VERTICAL);
  static Ptr create(Direction direction, real spacing);

protected:
  void updateChildAllocation() override;
};

RTTI_CLASS_DECLARE(Framework::Gui::Layouts::Box)

} // namespace Layouts
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_LAYOUTS_BOX_H
