#include "border.h"

namespace Framework {
namespace Gui {
namespace Layouts {

Border::Border()
  : Border(Metrics::Padding(0))
{
  _customPadding = false;
}

Border::Ptr Border::create()
{
  return Ptr(new Border());
}

Border::Border(const Metrics::Padding& padding)
  : _padding(padding),
    _customPadding(true)
{
  RTTI_CLASS_SET_TYPE(Framework::Gui::Layouts::Border);
}

Border::Ptr Border::create(const Metrics::Padding& padding)
{
  return Ptr(new Border(padding));
}

bool Border::customPadding() const
{
  return _customPadding;
}

const Metrics::Padding& Border::padding()
{
  return _padding;
}

void Border::setPadding(const Metrics::Padding& padding)
{
  _customPadding = true;
  _setPadding(padding);
}

void Border::setThemePadding()
{
  if(customPadding())
  {
    _customPadding = false;
    applyMetricsFromTheme();
  }
}

void Border::_setPadding(const Metrics::Padding& padding)
{
  if(this->padding() != padding)
  {
    this->_padding = padding;
    markEverythingLocationRelatedDirty();
  }
}

void Border::updateMinimumSize()
{
  if(item())
    setMinimumSize((*item())->minimumSize() + padding().totalWidth());
  else
    setMinimumSize(padding().totalWidth());
}

void Border::updateOptimalSize()
{
  if(item())
    setOptimalSize((*item())->optimalSize() + padding().totalWidth());
  else
    setOptimalSize(padding().totalWidth());
}

void Border::updateChildAllocation()
{
  if(item())
  {
    Rectangle<vec2> childAllocation(this->allocation().min() + vec2(padding().left,
                                                                    padding().top),
                                    this->allocation().max() - vec2(padding().right,
                                                                    padding().bottom));

    this->setItemAllocation(*item(), childAllocation);
  }
}

void Border::onApplyMetricsFromTheme(const Theme::Ptr &theme)
{
  if(!customPadding())
  {
    const Metrics& metrics = *theme->layoutMetricsEnforced(this);

    _setPadding(metrics.padding);
  }
}

} // namespace Layouts
} // namespace Gui
} // namespace Framework
