#ifndef FRAMEWORK_GUI_LAYOUTS_LIMITSIZE_H
#define FRAMEWORK_GUI_LAYOUTS_LIMITSIZE_H

#include "binary-layout.h"

namespace Framework {
namespace Gui {
namespace Layouts {

class LimitSize final : public BinaryLayout
{
public:
  typedef std::shared_ptr<LimitSize> Ptr;
  typedef std::weak_ptr<LimitSize> WeakPtr;

private:
  Optional<real> _widthLimit, _heightLimit;
  bool _customWidthLimit, _customHeightLimit;

public:
  LimitSize(const Optional<real>& widthLimit, const Optional<real>& heightLimit=nothing);
  LimitSize();

  static Ptr create(const Optional<real>& widthLimit, const Optional<real>& heightLimit=nothing);
  static Ptr create();

  const Optional<real>& widthLimit() const;
  const Optional<real>& heightLimit() const;

  void setWidthLimit(const Optional<real>& widthLimit);
  void setHeightLimit(const Optional<real>& heightLimit);
  void setThemeWidthLimit();
  void setThemeHeightLimit();

  bool customWidthLimit() const;
  bool customHeightLimit() const;

protected:
  void _setWidthLimit(const Optional<real>& widthLimit);
  void _setHeightLimit(const Optional<real>& heightLimit);

  void onApplyMetricsFromTheme(const Theme::Ptr& theme) override;
  void updateChildAllocation() override;
};

RTTI_CLASS_DECLARE(Framework::Gui::Layouts::LimitSize)

} // namespace Layouts
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_LIMITSIZE_H
