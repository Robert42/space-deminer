#ifndef FRAMEWORK_GUI_LAYOUTS_BASEBOX_H
#define FRAMEWORK_GUI_LAYOUTS_BASEBOX_H

#include "ordered-layout.h"

namespace Framework {
namespace Gui {
namespace Layouts {

class BaseBox : public OrderedLayout
{
public:
  typedef std::shared_ptr<BaseBox> Ptr;
  typedef std::weak_ptr<BaseBox> WeakPtr;

  BEGIN_ENUMERATION(Direction,)
    HORIZONTAL = 0,
    VERTICAL = 1
  ENUMERATION_BASIC_OPERATORS(Direction)
  END_ENUMERATION;

  class SpaceDistributor
  {
  public:
    typedef std::function<real(const Item::Ptr& item)> ExpansionPriorityFunction;

    const ExpansionPriorityFunction _expansionPriority;
    const int direction;
    const bool round;
    real totalExpansionPriority;

  private:
    const real totalSpace;
    real leftSpace;

  public:
    SpaceDistributor(Layout& layout,
                     int direction,
                     real totalSpace,
                     bool miniumSizeToMaximumSizeTransition);
    SpaceDistributor(Layout& layout,
                     int direction,
                     real totalSpace,
                     const ExpansionPriorityFunction& expansionPriority);

    real pickNextSpaceUsage(const Item::Ptr& item);
    real pickNextSpaceUsage(real expansionPriority);

  private:
    static bool _shouldRound(const Layout& layout);
    real _totalExpansionPriority(Layout& layout);

  public:
    static real _itemExpansionPriority(const Item::Ptr& item);
    static real _differenceBetweenOptimalAndMinimumSize(int direction, const Item::Ptr& item);
  };

private:
  Direction _direction;
  real _spacing;
  bool _customSpacing;

protected:
  BaseBox(Direction direction, real spacing);
  BaseBox(Direction direction = Direction::VERTICAL);

public:
  Direction direction() const;
  Direction otherDirection() const;
  void setDirection(Direction direction);

  bool customSpacing() const;
  real spacing() const;
  void setSpacing(real spacing);
  void setThemeSpacing();

protected:
  void updateMinimumSize() override;
  void updateOptimalSize() override;
  void onApplyMetricsFromTheme(const Theme::Ptr& theme) override;

  vec2 calcRequiredSize(const std::function<vec2(const Item::Ptr&)>& requiredSize);

  Optional<Item::Ptr> navigateToNextItem(const Item::Ptr& item, Navigation navigation) final override;
  Optional<Item::Ptr> navigateToFirstItem(Navigation navigation) final override;

private:
  void _setSpacing(real spacing);

  int _navigationToOrderDirection(Navigation navigation) const;
};

RTTI_CLASS_DECLARE(Framework::Gui::Layouts::BaseBox)

} // namespace Layouts
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_LAYOUTS_BASEBOX_H
