#ifndef FRAMEWORK_GUI_LAYOUTS_ZOFFSET_H
#define FRAMEWORK_GUI_LAYOUTS_ZOFFSET_H

#include "binary-layout.h"

namespace Framework {
namespace Gui {
namespace Layouts {

class ZOffset : public BinaryLayout
{
public:
  typedef std::shared_ptr<ZOffset> Ptr;
  typedef std::weak_ptr<ZOffset> WeakPtr;

private:
  real _zOffset;
  bool _customZOffset;

public:
  ZOffset();
  ZOffset(real zOffset);
  static Ptr create();
  static Ptr create(real zOffset);

public:
  bool customZOffset() const;
  real zOffset() const;
  void setZOffset(real zOffset);
  void setThemeZOffset();

protected:
  void updateChildAllocation() override;
  void updateChildZPosition() override;

  void onApplyMetricsFromTheme(const Theme::Ptr& theme) override;

private:
  void _setZOffset(real zOffset);
};

RTTI_CLASS_DECLARE(Framework::Gui::Layouts::ZOffset)

} // namespace Layouts
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_LAYOUTS_ZOFFSET_H
