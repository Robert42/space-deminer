#include "stacked-layout.h"

namespace Framework {
namespace Gui {
namespace Layouts {


StackedLayout::StackedLayout()
{
  RTTI_CLASS_SET_TYPE(Framework::Gui::Layouts::StackedLayout);
}

StackedLayout::Ptr StackedLayout::create()
{
  return Ptr(new StackedLayout);
}

void StackedLayout::updateMinimumSize()
{
  vec2 minimumSize(0);

  for(const Item::Ptr& item : orderedItems())
    minimumSize = max(minimumSize, item->minimumSize());

  setMinimumSize(minimumSize);
}

void StackedLayout::updateOptimalSize()
{
  vec2 optimalSize(0);

  for(const Item::Ptr& item : orderedItems())
    optimalSize = max(optimalSize, item->optimalSize());

  setOptimalSize(optimalSize);
}

void StackedLayout::updateChildAllocation()
{
  for(const Item::Ptr& item : orderedItems())
    setItemAllocation(item, this->allocation());
}

void StackedLayout::onApplyMetricsFromTheme(const Theme::Ptr&)
{
}

Optional<Layout::Item::Ptr> StackedLayout::navigateToNextItem(const Item::Ptr& item, Navigation navigation)
{
  switch(_navigationToOrderDirection(navigation))
  {
  case -1:
    return _navigateToNextItem(item, false);
    break;
  case 1:
    return _navigateToNextItem(item, true);
    break;
  default:
    return nothing;
  }
}

Optional<Layout::Item::Ptr> StackedLayout::navigateToFirstItem(Navigation navigation)
{
  switch(_navigationToOrderDirection(navigation))
  {
  case -1:
    return _navigateToFirstItem(false);
  case 1:
  default:
    return _navigateToFirstItem(true);
  }
}

int StackedLayout::_navigationToOrderDirection(Navigation navigation) const
{
  switch(navigation.value)
  {
  case Navigation::TAB_ORDER:
    return 1;
  case Navigation::TAB_ORDER_REVERSE:
    return -1;
  case Navigation::RIGHT:
  case Navigation::LEFT:
  case Navigation::DOWN:
  case Navigation::UP:
  default:
    return 0;
  }
}


} // namespace Layouts
} // namespace Gui
} // namespace Framework
