#include "unordered-layout.h"

namespace Framework {
namespace Gui {
namespace Layouts {

UnorderedLayout::UnorderedLayout()
{
}

void UnorderedLayout::removeAll()
{
  Layout::removeAllItems();
}

void UnorderedLayout::remove(const Layout::Item::Ptr& layoutItem)
{
  Layout::removeItem(layoutItem);
}

void UnorderedLayout::remove(const Widget::Ptr& widget)
{
  Optional<Layout::WidgetItem::Ptr> item = Layout::findItemWithWidget(widget);
  if(item)
    remove(*item);
}

void UnorderedLayout::remove(const Layout::Ptr& layout)
{
  Optional<Layout::SubLayoutItem::Ptr> item = Layout::findItemWithLayout(layout);
  if(item)
    remove(*item);
}

Layout::Item::Ptr UnorderedLayout::addWidget(const Widget::Ptr& widget)
{
  return addLayoutItem(Layout::WidgetItem::create(widget, *this));
}

Layout::Item::Ptr UnorderedLayout::addLayout(const Layout::Ptr& layout)
{
  return addLayoutItem(Layout::SubLayoutItem::create(layout, *this));
}

Layout::Item::Ptr UnorderedLayout::addLayoutItem(const Layout::Item::Ptr& layoutItem)
{
  Layout::insertItem(layoutItem);
  return layoutItem;
}

void UnorderedLayout::updateIndices(const InOutput<uint16>& index)
{
  for(const Layout::Item::Ptr& item : this->items())
    setItemIndex(item, index);
}

} // namespace Layouts
} // namespace Gui
} // namespace Framework
