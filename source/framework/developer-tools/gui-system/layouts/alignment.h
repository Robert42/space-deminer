#ifndef FRAMEWORK_GUI_LAYOUTS_ALIGNMENT_H
#define FRAMEWORK_GUI_LAYOUTS_ALIGNMENT_H

#include "binary-layout.h"

namespace Framework {
namespace Gui {
namespace Layouts {

class Alignment : public BinaryLayout
{
public:
  typedef std::shared_ptr<Alignment> Ptr;
  typedef std::weak_ptr<Alignment> WeakPtr;

private:
  vec2 _alignment;
  bool _customAlignment;

  vec2 _expandingFactor;
  bool _customExpandingFactor;

public:
  Alignment();
  Alignment(const vec2& alignment, const vec2& expandingFactor=vec2(0));
  static Ptr create();
  static Ptr create(const vec2& alignment, const vec2& expandingFactor=vec2(0));

public:
  bool customAlignment() const;
  const vec2& alignment() const;
  void setAlignment(const vec2& alignment);
  void setThemeAlignment();

  bool customExpandingFactor() const;
  const vec2& expandingFactor() const;
  void setExpandingFactor(const vec2& expandingFactor);
  void setThemeExpandingFactor();

protected:
  void updateChildAllocation() override;

  void onApplyMetricsFromTheme(const Theme::Ptr& theme) override;

private:
  void _setAlignment(vec2 alignment);
  void _setExpandingFactor(vec2 expandingFactor);
};

RTTI_CLASS_DECLARE(Framework::Gui::Layouts::Alignment)

} // namespace Layouts
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_LAYOUTS_ALIGNMENT_H
