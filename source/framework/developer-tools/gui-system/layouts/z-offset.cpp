#include "z-offset.h"

namespace Framework {
namespace Gui {
namespace Layouts {

ZOffset::ZOffset(real zOffset)
  : _zOffset(zOffset),
    _customZOffset(true)
{
  RTTI_CLASS_SET_TYPE(Framework::Gui::Layouts::ZOffset);
}

ZOffset::Ptr ZOffset::create(real zOffset)
{
  return Ptr(new ZOffset(zOffset));
}

ZOffset::ZOffset()
  : ZOffset(0.f)
{
  _customZOffset = false;
}

ZOffset::Ptr ZOffset::create()
{
  return Ptr(new ZOffset());
}

real ZOffset::zOffset() const
{
  return _zOffset;
}

bool ZOffset::customZOffset() const
{
  return _customZOffset;
}

void ZOffset::setZOffset(real zOffset)
{
  this->_customZOffset = true;
  _setZOffset(zOffset);
}

void ZOffset::setThemeZOffset()
{
  if(customZOffset())
  {
    this->_customZOffset = false;
    applyMetricsFromTheme();
  }
}

void ZOffset::_setZOffset(real zOffset)
{
  if(this->_zOffset != zOffset)
  {
    this->_zOffset = zOffset;
    markChildZPositionDirty();
  }
}

void ZOffset::updateChildAllocation()
{
  if(item())
    this->setItemAllocation(*item(), this->allocation());
}

void ZOffset::updateChildZPosition()
{
  if(item())
    this->setItemZPosition(*item(), this->zPosition() + this->zOffset());
}

void ZOffset::onApplyMetricsFromTheme(const Theme::Ptr& theme)
{
  if(!customZOffset())
  {
    const Metrics& metrics = *theme->layoutMetricsEnforced(this);

    _setZOffset(metrics.zOffset);
  }
}

} // namespace Layouts
} // namespace Gui
} // namespace Framework
