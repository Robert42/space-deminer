#include "alignment.h"

namespace Framework {
namespace Gui {
namespace Layouts {


Alignment::Alignment()
  : Alignment(vec2(0.f))
{
  _customAlignment = false;
  _customExpandingFactor = false;
}

Alignment::Alignment(const vec2& alignment, const vec2& expandingFactor)
  : _alignment(alignment),
    _customAlignment(true),
    _expandingFactor(expandingFactor),
    _customExpandingFactor(true)
{
  RTTI_CLASS_SET_TYPE(Framework::Gui::Layouts::Alignment);
}


Alignment::Ptr Alignment::create()
{
  return Ptr(new Alignment);
}

Alignment::Ptr Alignment::create(const vec2& alignment, const vec2& expandingFactor)
{
  return Ptr(new Alignment(alignment, expandingFactor));
}

const vec2& Alignment::alignment() const
{
  return _alignment;
}

bool Alignment::customAlignment() const
{
  return _customAlignment;
}

void Alignment::setAlignment(const vec2& alignment)
{
  this->_customAlignment = true;
  _setAlignment(alignment);
}

void Alignment::setThemeAlignment()
{
  if(customAlignment())
  {
    this->_customAlignment = false;
    applyMetricsFromTheme();
  }
}

void Alignment::_setAlignment(vec2 alignment)
{
  alignment = clamp(alignment, vec2(0), vec2(1));

  if(this->_alignment != alignment)
  {
    this->_alignment = alignment;
    markChildAllocationDirty();
  }
}


const vec2& Alignment::expandingFactor() const
{
  return _expandingFactor;
}

bool Alignment::customExpandingFactor() const
{
  return _customExpandingFactor;
}

void Alignment::setExpandingFactor(const vec2& expandingFactor)
{
  this->_customExpandingFactor = true;
  _setExpandingFactor(expandingFactor);
}

void Alignment::setThemeExpandingFactor()
{
  if(customExpandingFactor())
  {
    this->_customExpandingFactor = false;
    applyMetricsFromTheme();
  }
}

void Alignment::_setExpandingFactor(vec2 expandingFactor)
{
  expandingFactor = clamp(expandingFactor, vec2(0), vec2(1));

  if(this->_expandingFactor != expandingFactor)
  {
    this->_expandingFactor = expandingFactor;
    markChildAllocationDirty();
  }
}


void Alignment::updateChildAllocation()
{
  if(this->item())
  {
    Item::Ptr item = *this->item();

    const vec2 optimalChildSize = item->optimalSize();
    const vec2 parentSize = this->allocation().size();
    vec2 childPosition = this->allocation().min();
    vec2 childSize = parentSize;

    for(int i=0; i<2; ++i)
    {
      if(parentSize[i] > optimalChildSize[i])
      {
        childSize[i] = expandingFactor()[i] * (parentSize[i]-optimalChildSize[i]) +
                       optimalChildSize[i];

        childPosition[i] = alignment()[i] * (parentSize[i]-childSize[i]) +
                           childPosition[i];
      }

    }

    if(this->shouldRound())
    {
      childPosition = round(childPosition);
      childSize = round(childSize);
    }

    this->setItemAllocation(item,
                            Rectangle<vec2>(childPosition,
                                            childPosition+childSize));
  }
}


void Alignment::onApplyMetricsFromTheme(const Theme::Ptr& theme)
{
  if(customAlignment() && customExpandingFactor())
    return;

  const Metrics& metrics = *theme->layoutMetricsEnforced(this);

  if(!customAlignment())
    _setAlignment(metrics.alignment);

  if(!customExpandingFactor())
    _setExpandingFactor(metrics.childExpansion);
}


} // namespace Layouts
} // namespace Gui
} // namespace Framework
