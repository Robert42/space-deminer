#include "binary-layout.h"

namespace Framework {
namespace Gui {
namespace Layouts {


BinaryLayout::BinaryLayout()
{
  RTTI_CLASS_SET_TYPE(Framework::Gui::Layouts::BinaryLayout)
}

Optional<Layout::Item::Ptr> BinaryLayout::item()
{
  return _item;
}

void BinaryLayout::remove()
{
  if(_item)
    Layout::removeItem(*_item);
}

Layout::Item::Ptr BinaryLayout::addWidget(const Widget::Ptr& widget)
{
  return addLayoutItem(Layout::WidgetItem::create(widget, *this));
}

Layout::Item::Ptr BinaryLayout::addLayout(const Layout::Ptr& layout)
{
  return addLayoutItem(Layout::SubLayoutItem::create(layout, *this));
}

Layout::Item::Ptr BinaryLayout::addLayoutItem(const Layout::Item::Ptr& layoutItem)
{
  remove();
  _item = layoutItem;
  Layout::insertItem(layoutItem);
  return layoutItem;
}

void BinaryLayout::updateIndices(const InOutput<uint16>& index)
{
  if(item())
    setItemIndex(*item(), index);
}

void BinaryLayout::onItemRemoved(const std::shared_ptr<Item>& item)
{
  if(_item == item)
    _item.reset();
}

void BinaryLayout::updateMinimumSize()
{
  if(item())
    setMinimumSize((*item())->minimumSize());
  else
    setMinimumSize(vec2(0));
}

void BinaryLayout::updateOptimalSize()
{
  if(item())
    setOptimalSize((*item())->optimalSize());
  else
    setOptimalSize(vec2(0));
}

void BinaryLayout::updateChildAllocation()
{
  if(item())
    setItemAllocation(*item(), this->allocation());
}

Optional<Layout::Item::Ptr> BinaryLayout::navigateToNextItem(const Item::Ptr& item, Navigation navigation)
{
  (void)navigation;

  assert(!this->item().valid() || item == this->item().value());

  return nothing;
}

Optional<Layout::Item::Ptr> BinaryLayout::navigateToFirstItem(Navigation navigation)
{
  (void)navigation;

  return item();
}


} // namespace Layouts
} // namespace Gui
} // namespace Framework
