#ifndef FRAMEWORK_GUI_LAYOUTS_UNORDEREDLAYOUT_H
#define FRAMEWORK_GUI_LAYOUTS_UNORDEREDLAYOUT_H

#include "../layout.h"

namespace Framework {
namespace Gui {
namespace Layouts {

class UnorderedLayout : public Layout
{
public:
  typedef std::shared_ptr<UnorderedLayout> Ptr;
  typedef std::weak_ptr<UnorderedLayout> WeakPtr;

public:
  UnorderedLayout();

public:
  void removeAll();
  void remove(const Layout::Item::Ptr& layoutItem);
  void remove(const Widget::Ptr& widget);
  void remove(const Layout::Ptr& layout);
  Layout::Item::Ptr addWidget(const Widget::Ptr& widget);
  Layout::Item::Ptr addLayout(const Layout::Ptr& layout);
  Layout::Item::Ptr addLayoutItem(const Layout::Item::Ptr& layoutItem);

protected:
  void updateIndices(const InOutput<uint16>& index) override;
};

RTTI_CLASS_DECLARE(Framework::Gui::Layouts::UnorderedLayout)

} // namespace Layouts
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_LAYOUTS_UNORDEREDLAYOUT_H
