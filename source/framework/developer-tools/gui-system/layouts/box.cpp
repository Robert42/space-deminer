#include "box.h"

namespace Framework {
namespace Gui {
namespace Layouts {

Box::Box(Direction direction)
  : BaseBox(direction)
{
  RTTI_CLASS_SET_TYPE(Framework::Gui::Layouts::Box);
}


Box::Ptr Box::create(Direction direction)
{
  return Ptr(new Box(direction));
}

Box::Box(Direction direction, real spacing)
  : BaseBox(direction, spacing)
{
  RTTI_CLASS_SET_TYPE(Framework::Gui::Layouts::Box);
}


Box::Ptr Box::create(Direction direction, real spacing)
{
  return Ptr(new Box(direction, spacing));
}

void Box::updateChildAllocation()
{
  int direction = this->direction().value;
  int otherDirection = this->otherDirection().value;

  const Rectangle<vec2>& allocation = this->allocation();
  const vec2 allocationSize = allocation.size();

  real maxLength = allocationSize[direction];
  real leftSpace;
  bool miniumSizeToMaximumSizeTransition;
  std::function<real(const Item::Ptr& item)> requiredSpaceForItem;

  if(optimalSize()[direction] <= maxLength)
  {
    leftSpace = maxLength - optimalSize()[direction];
    miniumSizeToMaximumSizeTransition = false;
    requiredSpaceForItem = [direction](const Item::Ptr& item){return item->optimalSize()[direction];};
  }else
  {
    leftSpace = maxLength - minimumSize()[direction];
    miniumSizeToMaximumSizeTransition = true;
    requiredSpaceForItem = [direction](const Item::Ptr& item){return item->minimumSize()[direction];};
  }

  SpaceDistributor spaceDistributor(*this, direction, leftSpace, miniumSizeToMaximumSizeTransition);

  vec2 position = allocation.min();

  for(const Item::Ptr& item : orderedItems())
  {
    vec2 itemSize;
    itemSize[direction] = requiredSpaceForItem(item) + spaceDistributor.pickNextSpaceUsage(item);
    itemSize[otherDirection] = allocationSize[otherDirection];

    setItemAllocation(item, Rectangle<vec2>(vec2(0), itemSize) + position);

    position[direction] += itemSize[direction] + this->spacing();
  }
}



} // namespace Layouts
} // namespace Gui
} // namespace Framework
