#include "button-box.h"

namespace Framework {
namespace Gui {
namespace Layouts {


ButtonBox::ButtonBox(Direction direction)
  : BaseBox(direction),
    _otherPositionAlignment(0.5f),
    _prependedSpaceStretching(0.f),
    _interSpaceStretching(0.f),
    _appendedSpaceStretching(0.f)
{
  RTTI_CLASS_SET_TYPE(Framework::Gui::Layouts::ButtonBox);
}

ButtonBox::Ptr ButtonBox::create(Direction direction)
{
  return Ptr(new ButtonBox(direction));
}

ButtonBox::ButtonBox(Direction direction, real spacing)
  : BaseBox(direction, spacing),
    _otherPositionAlignment(0.5f),
    _prependedSpaceStretching(0.f),
    _interSpaceStretching(0.f),
    _appendedSpaceStretching(0.f)
{
  RTTI_CLASS_SET_TYPE(Framework::Gui::Layouts::ButtonBox);
}

ButtonBox::Ptr ButtonBox::create(Direction direction, real spacing)
{
  return Ptr(new ButtonBox(direction, spacing));
}



real ButtonBox::prependedSpaceStretching() const
{
  return _prependedSpaceStretching;
}

void ButtonBox::setPrependedSpaceStretching(real prependedSpaceStretching)
{
  if(this->prependedSpaceStretching() != prependedSpaceStretching)
  {
    this->_prependedSpaceStretching = prependedSpaceStretching;
    markChildAllocationDirty();
  }
}


real ButtonBox::interSpaceStretching() const
{
  return _interSpaceStretching;
}

void ButtonBox::setInterSpaceStretching(real interSpaceStretching)
{
  if(this->interSpaceStretching() != interSpaceStretching)
  {
    this->_interSpaceStretching = interSpaceStretching;
    markChildAllocationDirty();
  }
}


real ButtonBox::appendedSpaceStretching() const
{
  return _appendedSpaceStretching;
}

void ButtonBox::setAppendedSpaceStretching(real appendedSpaceStretching)
{
  if(this->appendedSpaceStretching() != appendedSpaceStretching)
  {
    this->_appendedSpaceStretching = appendedSpaceStretching;
    markChildAllocationDirty();
  }
}


real ButtonBox::otherPositionAlignment() const
{
  return _otherPositionAlignment;
}

void ButtonBox::setOtherPositionAlignment(real otherPositionAlignment)
{
  if(this->otherPositionAlignment() != otherPositionAlignment)
  {
    this->_otherPositionAlignment = otherPositionAlignment;
    markChildAllocationDirty();
  }
}


void ButtonBox::updateOptimalSize()
{
  int direction = this->direction().value;

  this->maxOptimalSize = vec2(0);

  for(const Item::Ptr& item : items())
    maxOptimalSize = max(maxOptimalSize, item->optimalSize());

  vec2 totalOptimalSize = maxOptimalSize;

  totalOptimalSize[direction] *= items().size();

  if(!items().empty())
    totalOptimalSize[direction] += spacing() * (items().size()-1);

  setOptimalSize(totalOptimalSize);
}

void ButtonBox::updateChildAllocation()
{
  int direction = this->direction().value;
  int otherDirection = this->otherDirection().value;

  const Rectangle<vec2>& allocation = this->allocation();
  const vec2 allocationSize = allocation.size();

  real maxLength = allocationSize[direction];
  real leftSpace;

  const vec2 optimalSize = this->optimalSize();
  const vec2 minimumSize = this->minimumSize();
  const real otherSize = min(allocationSize[otherDirection], optimalSize[otherDirection]);

  std::function<real(const Item::Ptr& item)> requiredSpaceForItem;
  std::function<real(const Item::Ptr& item)> expandPriorityForItem;
  real additionalExpansionPriority = 0.f;
  if(optimalSize[direction] <= maxLength)
  {
    leftSpace = maxLength - optimalSize[direction];
    requiredSpaceForItem = [this, direction](const Item::Ptr&){return maxOptimalSize[direction];};
    expandPriorityForItem = std::bind(&BaseBox::SpaceDistributor::_itemExpansionPriority, _1);
    additionalExpansionPriority = this->prependedSpaceStretching() + this->appendedSpaceStretching() + max(0.f, this->interSpaceStretching()*(this->items().size()-1));
  }else
  {
    leftSpace = maxLength - minimumSize[direction];
    requiredSpaceForItem = [direction](const Item::Ptr& item){return item->minimumSize()[direction];};
    expandPriorityForItem = [direction, this](const Item::Ptr& item){return maxOptimalSize[direction] - item->minimumSize()[direction];};
  }

  SpaceDistributor spaceDistributor(*this, direction, leftSpace, expandPriorityForItem);
  vec2 position = allocation.min();
  position[otherDirection] += (allocationSize[otherDirection]-otherSize) * otherPositionAlignment();

  if(spaceDistributor.round)
    position = glm::round(position);

  spaceDistributor.totalExpansionPriority += additionalExpansionPriority;

  // if additionalExpansionPriority==0, no stretching necessary or below optimal size
  const bool addSpaceStetching = additionalExpansionPriority > 0.f;

  if(addSpaceStetching)
    position[direction] += spaceDistributor.pickNextSpaceUsage(this->prependedSpaceStretching());

  for(const Item::Ptr& item : orderedItems())
  {
    real spacing = this->spacing();
    vec2 itemSize;
    itemSize[direction] = requiredSpaceForItem(item) + spaceDistributor.pickNextSpaceUsage(item);
    itemSize[otherDirection] = otherSize;

    setItemAllocation(item, Rectangle<vec2>(vec2(0), itemSize) + position);

    if(addSpaceStetching)
      spacing += spaceDistributor.pickNextSpaceUsage(this->interSpaceStretching());

    position[direction] += itemSize[direction] + spacing;
  }

  if(addSpaceStetching)
    position[direction] += spaceDistributor.pickNextSpaceUsage(this->appendedSpaceStretching());
}


} // namespace Layouts
} // namespace Gui
} // namespace Framework
