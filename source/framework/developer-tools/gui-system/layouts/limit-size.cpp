#include "limit-size.h"

namespace Framework {
namespace Gui {
namespace Layouts {

LimitSize::LimitSize(const Optional<real>& widthLimit, const Optional<real>& heightLimit)
  : _widthLimit(widthLimit),
    _heightLimit(heightLimit),
    _customWidthLimit(true),
    _customHeightLimit(true)
{
  RTTI_CLASS_SET_TYPE(Framework::Gui::Layouts::LimitSize);
}

LimitSize::LimitSize()
  : LimitSize(nothing, nothing)
{
  _customWidthLimit = false;
  _customHeightLimit = false;
}

LimitSize::Ptr LimitSize::create(const Optional<real>& widthLimit, const Optional<real>& heightLimit)
{
  return Ptr(new LimitSize(widthLimit, heightLimit));
}

LimitSize::Ptr LimitSize::create()
{
  return Ptr(new LimitSize());
}


const Optional<real>& LimitSize::widthLimit() const
{
  return this->_widthLimit;
}

const Optional<real>& LimitSize::heightLimit() const
{
  return this->_heightLimit;
}


void LimitSize::setWidthLimit(const Optional<real>& widthLimit)
{
  _customWidthLimit = true;
  _setWidthLimit(widthLimit);
}

void LimitSize::setHeightLimit(const Optional<real>& heightLimit)
{
  _customHeightLimit = true;
  _setHeightLimit(heightLimit);
}

void LimitSize::setThemeWidthLimit()
{
  if(customWidthLimit())
  {
    _customWidthLimit = false;
    applyMetricsFromTheme();
  }
}

void LimitSize::setThemeHeightLimit()
{
  if(customHeightLimit())
  {
    _customHeightLimit = false;
    applyMetricsFromTheme();
  }
}

bool LimitSize::customWidthLimit() const
{
  return _customWidthLimit;
}

bool LimitSize::customHeightLimit() const
{
  return _customHeightLimit;
}


void LimitSize::_setWidthLimit(const Optional<real>& widthLimit)
{
  if(this->widthLimit() != widthLimit)
  {
    this->_widthLimit = widthLimit;
    markEverythingLocationRelatedDirty();
  }
}

void LimitSize::_setHeightLimit(const Optional<real>& heightLimit)
{
  if(this->heightLimit() != heightLimit)
  {
    this->_heightLimit = heightLimit;
    markEverythingLocationRelatedDirty();
  }
}


void LimitSize::onApplyMetricsFromTheme(const Theme::Ptr& theme)
{
  if(customWidthLimit() && customHeightLimit())
    return;

  const Metrics& metrics = *theme->layoutMetricsEnforced(this);

  if(!customWidthLimit())
    _setWidthLimit(metrics.widthLimit);

  if(!customHeightLimit())
    _setHeightLimit(metrics.heightLimit);
}

void LimitSize::updateChildAllocation()
{
  if(!item())
    return;

  const vec2 position = this->allocation().min();
  vec2 size = this->allocation().size();

  if(widthLimit())
    size.x = min(size.x, *widthLimit());

  if(heightLimit())
    size.y = min(size.y, *heightLimit());

  setItemAllocation(*item(),
                    Rectangle<vec2>(vec2(0), size) + position);
}

} // namespace Layouts
} // namespace Gui
} // namespace Framework
