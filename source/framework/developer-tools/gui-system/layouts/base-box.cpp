#include "base-box.h"

namespace Framework {
namespace Gui {
namespace Layouts {

BaseBox::BaseBox(Direction direction, float spacing)
  : _direction(direction),
    _spacing(spacing),
    _customSpacing(true)
{
  RTTI_CLASS_SET_TYPE(Framework::Gui::Layouts::BaseBox);
}

BaseBox::BaseBox(Direction direction)
  : BaseBox(direction, 0.f)
{
  _customSpacing = false;
}


BaseBox::Direction BaseBox::direction() const
{
  return _direction;
}

BaseBox::Direction BaseBox::otherDirection() const
{
  return static_cast<Direction::Value>(1 - direction().value);
}

void BaseBox::setDirection(Direction direction)
{
  if(this->direction() != direction)
  {
    this->_direction = direction;
    markEverythingLocationRelatedDirty();
  }
}


bool BaseBox::customSpacing() const
{
  return _customSpacing;
}

real BaseBox::spacing() const
{
  return _spacing;
}

void BaseBox::setSpacing(real spacing)
{
  _customSpacing = true;
  _setSpacing(spacing);
}

void BaseBox::setThemeSpacing()
{
  if(customSpacing())
  {
    _customSpacing = false;
    applyMetricsFromTheme();
  }
}

void BaseBox::_setSpacing(real spacing)
{
  if(this->spacing() != spacing)
  {
    this->_spacing = spacing;
    markEverythingLocationRelatedDirty();
  }
}

void BaseBox::onApplyMetricsFromTheme(const Theme::Ptr& theme)
{
  if(customSpacing())
    return;

  const Metrics& metrics = *theme->layoutMetricsEnforced(this);

  _setSpacing(metrics.spacing);
}

void BaseBox::updateMinimumSize()
{
  setMinimumSize(calcRequiredSize([](const Item::Ptr& i){return i->minimumSize();}));
}

void BaseBox::updateOptimalSize()
{
  setOptimalSize(calcRequiredSize([](const Item::Ptr& i){return i->optimalSize();}));
}

vec2 BaseBox::calcRequiredSize(const std::function<vec2(const Item::Ptr&)>& requiredSize)
{
  int direction = this->direction().value;
  int otherDirection = this->otherDirection().value;

  vec2 totalRequiredSize(0);

  for(const Item::Ptr& item : items())
  {
    const vec2& itemRequiredSize = requiredSize(item);

    totalRequiredSize[direction] += itemRequiredSize[direction];
    totalRequiredSize[otherDirection] = max(itemRequiredSize[otherDirection],
                                            totalRequiredSize[otherDirection]);
  }

  if(!items().empty())
    totalRequiredSize[direction] += spacing() * (items().size()-1);

  return totalRequiredSize;
}


Optional<Layout::Item::Ptr> BaseBox::navigateToNextItem(const Item::Ptr& item, Navigation navigation)
{
  switch(_navigationToOrderDirection(navigation))
  {
  case -1:
    return _navigateToNextItem(item, false);
    break;
  case 1:
    return _navigateToNextItem(item, true);
    break;
  default:
    return nothing;
  }
}

Optional<Layout::Item::Ptr> BaseBox::navigateToFirstItem(Navigation navigation)
{
  switch(_navigationToOrderDirection(navigation))
  {
  case -1:
    return _navigateToFirstItem(false);
  case 1:
  default:
    return _navigateToFirstItem(true);
  }
}

int BaseBox::_navigationToOrderDirection(Navigation navigation) const
{
  if(this->direction() == Direction::HORIZONTAL)
  {
    switch(navigation.value)
    {
    case Navigation::LEFT:
    case Navigation::TAB_ORDER_REVERSE:
      return -1;
    case Navigation::RIGHT:
    case Navigation::TAB_ORDER:
      return 1;
    case Navigation::UP:
    case Navigation::DOWN:
    default:
      return 0;
    }
  }else
  {
    switch(navigation.value)
    {
    case Navigation::UP:
    case Navigation::TAB_ORDER_REVERSE:
      return -1;
    case Navigation::DOWN:
    case Navigation::TAB_ORDER:
      return 1;
    case Navigation::LEFT:
    case Navigation::RIGHT:
    default:
      return 0;
    }
  }
}

// ====


BaseBox::SpaceDistributor::SpaceDistributor(Layout& layout,
                                            int direction,
                                            real totalSpace,
                                            bool miniumSizeToMaximumSizeTransition)
  : SpaceDistributor(layout,
                     direction,
                     totalSpace,
                     miniumSizeToMaximumSizeTransition ? ExpansionPriorityFunction(std::bind(&SpaceDistributor::_differenceBetweenOptimalAndMinimumSize, direction, _1))
                                                       : ExpansionPriorityFunction(std::bind(&SpaceDistributor::_itemExpansionPriority, _1)))
{
}

BaseBox::SpaceDistributor::SpaceDistributor(Layout& layout,
                                            int direction,
                                            real totalSpace,
                                            const ExpansionPriorityFunction& expansionPriority)
  : _expansionPriority(expansionPriority),
    direction(direction),
    round(_shouldRound(layout)),
    totalExpansionPriority(_totalExpansionPriority(layout)),
    totalSpace(totalSpace),
    leftSpace(totalSpace)
{
}

real BaseBox::SpaceDistributor::pickNextSpaceUsage(const Item::Ptr& item)
{
  return pickNextSpaceUsage(_expansionPriority(item));
}

real BaseBox::SpaceDistributor::pickNextSpaceUsage(real expansionPriority)
{
  if(totalExpansionPriority > 0.f)
  {
    real currentSpace = totalSpace * expansionPriority / totalExpansionPriority;

    if(this->round)
      currentSpace = glm::round(currentSpace);

    currentSpace = min(currentSpace, leftSpace);
    leftSpace -= currentSpace;

    return currentSpace;
  }else
  {
    return 0.f;
  }
}

bool BaseBox::SpaceDistributor::_shouldRound(const Layout& layout)
{
  return layout.shouldRound();
}

real BaseBox::SpaceDistributor::_totalExpansionPriority(Layout& layout)
{
  real sum = 0.f;

  for(const Item::Ptr& item : layout.items())
    sum += _expansionPriority(item);

  return sum;
}

real BaseBox::SpaceDistributor::_differenceBetweenOptimalAndMinimumSize(int direction, const Item::Ptr& item)
{
  return item->optimalSize()[direction] - item->minimumSize()[direction];
}

real BaseBox::SpaceDistributor::_itemExpansionPriority(const Item::Ptr& item)
{
  return item->expandPriority();
}


} // namespace Layouts
} // namespace Gui
} // namespace Framework
