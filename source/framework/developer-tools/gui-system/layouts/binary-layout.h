#ifndef FRAMEWORK_GUI_LAYOUTS_BINARYLAYOUT_H
#define FRAMEWORK_GUI_LAYOUTS_BINARYLAYOUT_H

#include "../layout.h"

namespace Framework {
namespace Gui {
namespace Layouts {

class BinaryLayout : public Layout
{
public:
  typedef std::shared_ptr<BinaryLayout> Ptr;
  typedef std::weak_ptr<BinaryLayout> WeakPtr;

private:
  Optional<Layout::Item::Ptr> _item;

public:
  BinaryLayout();

public:
  Optional<Layout::Item::Ptr> item();

  void remove();
  Layout::Item::Ptr addWidget(const Widget::Ptr& widget);
  Layout::Item::Ptr addLayout(const Layout::Ptr& layout);
  Layout::Item::Ptr addLayoutItem(const Layout::Item::Ptr& layoutItem);

protected:
  void updateIndices(const InOutput<uint16>& index) override;

  void onItemRemoved(const std::shared_ptr<Item>& item) override;

  void updateMinimumSize() override;
  void updateOptimalSize() override;
  void updateChildAllocation() override;

  Optional<Item::Ptr> navigateToNextItem(const Item::Ptr& item, Navigation navigation) final override;
  Optional<Item::Ptr> navigateToFirstItem(Navigation navigation) final override;
};

RTTI_CLASS_DECLARE(Framework::Gui::Layouts::BinaryLayout)

} // namespace Layouts
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_LAYOUTS_BINARYLAYOUT_H
