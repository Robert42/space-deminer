#ifndef FRAMEWORK_GUI_LAYOUTS_BUTTONBOX_H
#define FRAMEWORK_GUI_LAYOUTS_BUTTONBOX_H

#include "base-box.h"

namespace Framework {
namespace Gui {
namespace Layouts {

class ButtonBox : public BaseBox
{
public:
  typedef std::shared_ptr<ButtonBox> Ptr;
  typedef std::weak_ptr<ButtonBox> WeakPtr;

private:
  vec2 maxOptimalSize;
  real _otherPositionAlignment;
  real _prependedSpaceStretching;
  real _interSpaceStretching;
  real _appendedSpaceStretching;

public:
  ButtonBox(Direction direction = Direction::VERTICAL);
  ButtonBox(Direction direction, real spacing);

  static Ptr create(Direction direction = Direction::VERTICAL);
  static Ptr create(Direction direction, real spacing);

  real prependedSpaceStretching() const;
  void setPrependedSpaceStretching(real prependedSpaceStretching);

  real interSpaceStretching() const;
  void setInterSpaceStretching(real interSpaceStretching);

  real appendedSpaceStretching() const;
  void setAppendedSpaceStretching(real appendedSpaceStretching);

  real otherPositionAlignment() const;
  void setOtherPositionAlignment(real otherPositionAlignment);

protected:
  void updateOptimalSize() override;
  void updateChildAllocation() override;
};

RTTI_CLASS_DECLARE(Framework::Gui::Layouts::ButtonBox)

} // namespace Layouts
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_LAYOUTS_BUTTONBOX_H
