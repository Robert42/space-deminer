#include "ordered-layout.h"

namespace Framework {
namespace Gui {
namespace Layouts {

OrderedLayout::OrderedLayout()
{
  RTTI_CLASS_SET_TYPE(Framework::Gui::Layouts::OrderedLayout);
}

const QList<Layout::Item::Ptr>& OrderedLayout::orderedItems()
{
  return _orderedItems;
}

int OrderedLayout::indexOf(const Layout::Item::Ptr& item)
{
  return _orderedItems.indexOf(item);
}

int OrderedLayout::indexOf(const Layout::Ptr& layout)
{
  Optional<Layout::SubLayoutItem::Ptr> item = Layout::findItemWithLayout(layout);
  if(item)
    return indexOf(*item);
  else
    return -1;
}

int OrderedLayout::indexOf(const Widget::Ptr& widget)
{
  Optional<Layout::WidgetItem::Ptr> item = Layout::findItemWithWidget(widget);
  if(item)
    return indexOf(*item);
  else
    return -1;
}

void OrderedLayout::appendItem(const Layout::Item::Ptr& item)
{
  _orderedItems.append(item);
  Layout::insertItem(item);
}

void OrderedLayout::appendSpace(const vec2& space, real expandPriority)
{
  appendItem(Layout::SpacingItem::create(space, space, *this, expandPriority));
}

void OrderedLayout::appendSubLayout(const Layout::Ptr& layout, real expandPriority)
{
  appendItem(Layout::SubLayoutItem::create(layout, *this, expandPriority));
}

void OrderedLayout::appendWidget(const Widget::Ptr& widget, real expandPriority)
{
  appendItem(Layout::WidgetItem::create(widget, *this, expandPriority));
}

void OrderedLayout::prependItem(const Layout::Item::Ptr& item)
{
  _orderedItems.prepend(item);
  Layout::insertItem(item);
}

void OrderedLayout::prependSpace(const vec2& space, real expandPriority)
{
  prependItem(Layout::SpacingItem::create(space, space, *this, expandPriority));
}

void OrderedLayout::prependSubLayout(const Layout::Ptr& layout, real expandPriority)
{
  prependItem(Layout::SubLayoutItem::create(layout, *this, expandPriority));
}

void OrderedLayout::prependWidget(const Widget::Ptr& widget, real expandPriority)
{
  prependItem(Layout::WidgetItem::create(widget, *this, expandPriority));
}

void OrderedLayout::removeItem(const Layout::Item::Ptr& item)
{
  Layout::removeItem(item);
}

void OrderedLayout::insertItem(int i, const Layout::Item::Ptr& item)
{
  _orderedItems.insert(i, item);
  Layout::insertItem(item);
}

void OrderedLayout::insertSpace(int i, const vec2& space, real expandPriority)
{
  insertItem(i, Layout::SpacingItem::create(space, space, *this, expandPriority));
}

void OrderedLayout::insertSubLayout(int i, const Layout::Ptr& layout, real expandPriority)
{
  insertItem(i, Layout::SubLayoutItem::create(layout, *this, expandPriority));
}

void OrderedLayout::insertWidget(int i, const Widget::Ptr& widget, real expandPriority)
{
  insertItem(i, Layout::WidgetItem::create(widget, *this, expandPriority));
}


void OrderedLayout::removeSubLayout(const Layout::Ptr& layout)
{
  Optional<Layout::SubLayoutItem::Ptr> item = Layout::findItemWithLayout(layout);
  if(item)
    removeItem(*item);
}

void OrderedLayout::removeWidget(const Widget::Ptr& widget)
{
  Optional<Layout::WidgetItem::Ptr> item = Layout::findItemWithWidget(widget);
  if(item)
    removeItem(*item);
}

void OrderedLayout::updateIndices(const InOutput<uint16>& index)
{
  for(const Layout::Item::Ptr& item : this->orderedItems())
    setItemIndex(item, index);
}

void OrderedLayout::onItemRemoved(const std::shared_ptr<Item>& item)
{
  _orderedItems.removeAll(item);
}


Optional<Layout::Item::Ptr> OrderedLayout::_navigateToNextItem(const Item::Ptr& item, bool sameOrder)
{
  QListIterator<Item::Ptr> i(this->orderedItems());
  bool previousWasTheItem = false;

  if(sameOrder)
  {
    while(i.hasNext())
    {
      const Item::Ptr& currentItem = i.next();

      if(currentItem == item)
        previousWasTheItem = true;
      else if(previousWasTheItem)
        return currentItem;
    }
  }else
  {
    i.toBack();
    while(i.hasPrevious())
    {
      const Item::Ptr& currentItem = i.previous();

      if(currentItem == item)
        previousWasTheItem = true;
      else if(previousWasTheItem)
        return currentItem;
    }
  }

  return nothing;
}

Optional<Layout::Item::Ptr> OrderedLayout::_navigateToFirstItem(bool sameOrder)
{
  if(this->orderedItems().empty())
    return nothing;

  if(sameOrder)
    return this->orderedItems().first();
  else
    return this->orderedItems().last();
}


} // namespace Layouts
} // namespace Gui
} // namespace Framework
