#ifndef FRAMEWORK_GUI_LAYOUTS_ORDEREDLAYOUT_H
#define FRAMEWORK_GUI_LAYOUTS_ORDEREDLAYOUT_H

#include "../layout.h"

namespace Framework {
namespace Gui {
namespace Layouts {

class OrderedLayout : public Layout
{
public:
  typedef std::shared_ptr<OrderedLayout> Ptr;
  typedef std::weak_ptr<OrderedLayout> WeakPtr;

private:
  QList<Layout::Item::Ptr> _orderedItems;

public:
  OrderedLayout();

public:
  const QList<Layout::Item::Ptr>& orderedItems();

  int indexOf(const Layout::Item::Ptr& item);
  int indexOf(const Layout::Ptr& layout);
  int indexOf(const Widget::Ptr& widget);

  void appendItem(const Layout::Item::Ptr& item);
  void appendSpace(const vec2& space, real expandPriority=0.f);
  void appendSubLayout(const Layout::Ptr& layout, real expandPriority=0.f);
  void appendWidget(const Widget::Ptr& widget, real expandPriority=0.f);

  void prependItem(const Layout::Item::Ptr& item);
  void prependSpace(const vec2& space, real expandPriority=0.f);
  void prependSubLayout(const Layout::Ptr& layout, real expandPriority=0.f);
  void prependWidget(const Widget::Ptr& widget, real expandPriority=0.f);

  void insertItem(int i, const Layout::Item::Ptr& item);
  void insertSpace(int i, const vec2& space, real expandPriority=0.f);
  void insertSubLayout(int i, const Layout::Ptr& layout, real expandPriority=0.f);
  void insertWidget(int i, const Widget::Ptr& widget, real expandPriority=0.f);

  void removeItem(const Layout::Item::Ptr& item);
  void removeSubLayout(const Layout::Ptr& layout);
  void removeWidget(const Widget::Ptr& widget);

protected:
  void updateIndices(const InOutput<uint16>& index) override;

  void onItemRemoved(const std::shared_ptr<Item>& item) override;

  Optional<Layout::Item::Ptr> _navigateToNextItem(const Item::Ptr& item, bool sameOrder=true);
  Optional<Layout::Item::Ptr> _navigateToFirstItem(bool sameOrder=true);
};

RTTI_CLASS_DECLARE(Framework::Gui::Layouts::OrderedLayout)

} // namespace Layouts
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_LAYOUTS_ORDEREDLAYOUT_H
