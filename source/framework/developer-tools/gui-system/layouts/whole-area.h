#ifndef FRAMEWORK_GUI_LAYOUTS_WHOLEAREA_H
#define FRAMEWORK_GUI_LAYOUTS_WHOLEAREA_H

#include "binary-layout.h"

namespace Framework {
namespace Gui {
namespace Layouts {

class WholeArea final : public BinaryLayout
{
public:
  typedef std::shared_ptr<WholeArea> Ptr;
  typedef std::weak_ptr<WholeArea> WeakPtr;

public:
  WholeArea();

  static Ptr create();

protected:
  void onApplyMetricsFromTheme(const Theme::Ptr& theme) override;
};

RTTI_CLASS_DECLARE(Framework::Gui::Layouts::WholeArea)

} // namespace Layouts
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_LAYOUTS_WHOLEAREA_H
