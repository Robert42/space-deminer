#ifndef FRAMEWORK_GUI_LAYOUTS_STACKEDLAYOUT_H
#define FRAMEWORK_GUI_LAYOUTS_STACKEDLAYOUT_H

#include "ordered-layout.h"

namespace Framework {
namespace Gui {
namespace Layouts {

class StackedLayout : public OrderedLayout
{
public:
  typedef std::shared_ptr<StackedLayout> Ptr;
  typedef std::weak_ptr<StackedLayout> WeakPtr;

public:
  StackedLayout();

  static Ptr create();

  void updateMinimumSize() override;
  void updateOptimalSize() override;
  void updateChildAllocation() override;
  void onApplyMetricsFromTheme(const Theme::Ptr& theme) override;

protected:
  Optional<Item::Ptr> navigateToNextItem(const Item::Ptr& item, Navigation navigation) final override;
  Optional<Item::Ptr> navigateToFirstItem(Navigation navigation) final override;

private:
  int _navigationToOrderDirection(Navigation navigation) const;
};

RTTI_CLASS_DECLARE(Framework::Gui::Layouts::StackedLayout)

} // namespace Layouts
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_LAYOUTS_STACKEDLAYOUT_H
