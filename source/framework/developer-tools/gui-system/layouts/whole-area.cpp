#include "whole-area.h"

namespace Framework {
namespace Gui {
namespace Layouts {

WholeArea::WholeArea()
{
  RTTI_CLASS_SET_TYPE(Framework::Gui::Layouts::WholeArea);
}

WholeArea::Ptr WholeArea::create()
{
  return Ptr(new WholeArea);
}

void WholeArea::onApplyMetricsFromTheme(const Theme::Ptr& theme)
{
  (void)theme;
}


} // namespace Layouts
} // namespace Gui
} // namespace Framework
