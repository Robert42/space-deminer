#ifndef FRAMEWORK_GUI_LAYOUTS_BORDER_H
#define FRAMEWORK_GUI_LAYOUTS_BORDER_H

#include "binary-layout.h"
#include "../metrics.h"

namespace Framework {
namespace Gui {
namespace Layouts {

class Border : public BinaryLayout
{
public:
  typedef std::shared_ptr<Border> Ptr;
  typedef std::weak_ptr<Border> WeakPtr;

private:
  Metrics::Padding _padding;
  bool _customPadding;

public:
  Border();
  Border(const Metrics::Padding& padding);

  static Ptr create();
  static Ptr create(const Metrics::Padding& padding);

public:
  bool customPadding() const;
  const Metrics::Padding& padding();
  void setPadding(const Metrics::Padding& padding);
  void setThemePadding();

protected:
  void updateMinimumSize() override;
  void updateOptimalSize() override;
  void updateChildAllocation() override;

  void onApplyMetricsFromTheme(const Theme::Ptr &theme) override;

private:
  void _setPadding(const Metrics::Padding& padding);
};

RTTI_CLASS_DECLARE(Framework::Gui::Layouts::Border)

} // namespace Layouts
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_LAYOUTS_BORDER_H
