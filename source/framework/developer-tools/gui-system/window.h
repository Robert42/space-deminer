#ifndef FRAMEWORK_GUI_WINDOW_H
#define FRAMEWORK_GUI_WINDOW_H

#include "widget.h"
#include "widgetmodel.h"
#include "types.h"
#include "appearance.h"
#include "input-handlers/mouse-action.h"

#include <base/declarations.h>

namespace Framework {
namespace Gui {

class BaseWindowContainer;
class Window final
{
public:
  typedef std::shared_ptr<Window> Ptr;
  typedef std::weak_ptr<Window> WeakPtr;
  typedef QSet<Widget::Ptr> WidgetSet;

public:
  Signals::Trackable trackable;

private:
  WidgetModel::Ptr _widgetModel;
  WidgetModel::WidgetSlot rootWidgetSlot;
  Widget::Ptr _rootWidget;

  DirtyFlags _dirtyFlags;

  QVector<Widget::Ptr> sortedWidgets;

  bool _roundAllocationHint : 1;

  vec2 _size;

  Appearance::Ptr _appearance;

  bool _redrawSheduled : 1;
  bool _reorderSheduled : 1;

  Signals::CallableSignal<void()> _signalRedrawSheduled;
  Signals::CallableSignal<void()> _signalAppereanceChanged;

  Signals::CallableSignal<void()> _signalInvalidatedMouseOverWidget;
  Signals::CallableSignal<void()> _signalInvalidatedKeyFocusedWidget;

  Signals::CallableSignal<void(const InputHandlers::MouseAction::Ptr&)> _signalMouseActionStarted;

  Optional<BaseWindowContainer*> _windowContainer;
  Optional<Widget::Ptr> _widgetWithKeyFocus;
  Optional<Widget::FocusHint> _keyFocusPriority;

public:
  Window();
  ~Window();

  const WidgetModel::Ptr& widgetModel();

  const vec2& size() const;

  const Appearance::Ptr& appearance();

  const Widget::Ptr& rootWidget();
  void setRootWidget(const Widget::Ptr& widget);
  void unsetRootWidget();

  Signals::Signal<void()>& signalRedrawSheduled();
  Signals::Signal<void()>& signalAppereanceChanged();

  void widgetMarkedDirty(DirtyFlags dirtyFlags);
  void sheduleRedraw();
  void sheduleReorderRenderOrder();
  bool redrawSheduled() const;

  bool roundAllocationHint() const;

  void startMouseAction(const InputHandlers::MouseAction::Ptr& mouseAction);
  vec2 currentMousePosition(real zPosition) const;

  void updateAllWidgets();
  void updateAllVisibleWidgets();
  QVector<Widget::Ptr> allWidgetsUpdatedAndSorted(bool onlyVisible);

  Optional<Widget::Ptr> defaultWidget();
  Optional<Widget::Ptr> defaultEscapeWidget();

  Optional<Widget::Ptr> widgetWithKeyFocus();
  Optional<Widget::FocusHint> keyFocusPriority();
  void askForKeyFocus(const Widget::Ptr& widget, const Widget::FocusHint& focusHint);

  void _tagWidgetAsKeyFocused(Widget* widget);
  void _untagWidgetAsKeyFocused(Widget* widget);

  Optional<Widget::Ptr> findFirstKeyFocusableWidget();

  Optional<Widget::Ptr> findFirstWidgetWithTag(Tag tag);
  QSet<Widget::Ptr> findWidgetsWithTag(Tag tag);

  bool isAnyWidgetAtPoint(const vec2& point) const;
  bool isAnyWidgetAtRay(const Geometry::Ray& ray) const;

  void invalidateMouseOverWidget();
  void invalidateKeyFocusedWidget();

  Signals::Signal<void()>& signalInvalidatedMouseOverWidget();
  Signals::Signal<void()>& signalInvalidatedKeyFocusedWidget();

  Signals::Signal<void (const InputHandlers::MouseAction::Ptr&)>& signalMouseActionStarted();

private:
  friend class BaseWindowContainer;

  void setAppearance(const Appearance::Ptr& appearance);
  void setSize(const vec2& size);
  void _unsetRootWidget();
};

} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_WINDOW_H
