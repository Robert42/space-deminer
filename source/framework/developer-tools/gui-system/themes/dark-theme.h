#ifndef FRAMEWORK_GUI_THEMES_DARKTHEME_H
#define FRAMEWORK_GUI_THEMES_DARKTHEME_H

#include "palette-based-theme.h"
#include "../font-manager.h"

namespace Framework {
namespace Gui {
namespace Themes {

class DarkTheme : public PaletteBasedTheme
{
public:
  typedef std::shared_ptr<DarkTheme> Ptr;

  class LayoutMetrics;

public:
  const int smallRoundingRadius;

private:
  typedef QPair<TagSet, Theme::SimpleWidget::Ptr> TagSetWidgetRelation;

  QHash<ClassType, QList<TagSetWidgetRelation> > _simpleWidgetsDependingOnTags;
  QHash<ClassType, Theme::SimpleWidget::Ptr> _simpleWidgets;
  QHash<ClassType, Theme::SimpleTextWidget::Ptr> _simpleTextWidgets;
  QHash<ClassType, std::shared_ptr<LayoutMetrics> > _layoutMetricsForWidget, _layoutMetricsForLayout;

public:
  DarkTheme(const FontManager::Ptr& fontManager);

  static Ptr create(const FontManager::Ptr& fontManager);

  Optional<SimpleWidget::Ptr> simpleWidget(const ClassType& widgetType,
                                           const TagSet& tags=TagSet()) override;
  Optional<SimpleTextWidget::Ptr> simpleTextWidget(const ClassType& widgetType,
                                                   const TagSet& tags) override;
  Optional<const Metrics*> layoutMetrics(const ClassType& typeLayout,
                                         const ClassType& typeWidget,
                                         const TagSet& tagsLayout=TagSet(),
                                         const TagSet& tagsWidget=TagSet()) override;
  Optional<const Metrics*> layoutSpacingItemMetrics(const ClassType& typeLayout,
                                                    const ClassType& typeWidget,
                                                    const TagSet& tagsLayout=TagSet(),
                                                    const TagSet& tagsWidget=TagSet()) override;

public:
  void drawButtonShadow(Painter& painter, const Rectangle<vec2>& allocation, const State& state);
  void drawButtonFrame(Painter& painter, const Rectangle<vec2>& allocation, const Theme::State& state, real alpha=1.f);
  void drawButtonShine(Painter& painter, const Rectangle<vec2>& allocation, const Theme::State& state);
  void drawDefaultButton(Painter& painter, const Rectangle<vec2>& allocation, const State& state);
  void drawButton(Painter& painter, const Rectangle<vec2>& allocation, const State& state);

  static int buttonBorderWidth();
  static int windowButtonHeight();

  static Rectangle<vec2> recommendedFillRectangleForButtons(const Rectangle<vec2>& allocation);

  bool isPointWithinButton(const vec2& point, const Rectangle<vec2>& allocation);
  bool isPointWithinWindowFrame(const vec2& point, const Rectangle<vec2>& allocation);

  void drawGradientVertical(Painter& painter,
                            const Rectangle<vec2>& allocation,
                            const ColorPalette::Gradient& gradient) const;
};

} // namespace Themes
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_THEMES_DARKTHEME_H
