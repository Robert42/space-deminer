#ifndef FRAMEWORK_GUI_THEMES_THEMEWITHDEFAULTANIMATIONS_H
#define FRAMEWORK_GUI_THEMES_THEMEWITHDEFAULTANIMATIONS_H

#include "../theme.h"

namespace Framework {
namespace Gui {
namespace Themes {


class ThemeWithDefaultAnimations : public Theme
{
public:
  ThemeWithDefaultAnimations();

  bool playAnimation(const InOutput<AnimationData>& animationData) override;

  virtual bool handleHover(const InOutput<AnimationData>& animationData, bool start);
  virtual bool handleClick(const InOutput<AnimationData>& animationData, bool start);
  virtual bool handleClickOffset(const InOutput<AnimationData>& animationData, bool start);
};


} // namespace Themes
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_THEMES_THEMEWITHDEFAULTANIMATIONS_H
