#include "theme-with-default-animations.h"
#include "../animation-slot-ids.h"
#include "../tags.h"
#include "../animations/constant-animation.h"
#include "../animations/finish-animation.h"

namespace Framework {
namespace Gui {
namespace Themes {

ThemeWithDefaultAnimations::ThemeWithDefaultAnimations()
{
}

bool ThemeWithDefaultAnimations::playAnimation(const InOutput<AnimationData>& animationData)
{
  const ClassType& typeWidget = animationData.value.typeWidget;

  TagSet& animationTags = animationData.value.animationTags;
  bool& recursive = animationData.value.recursive;

  bool handeledAnimation = false;

  if(typeWidget.isInheritingOrSame(Types::Containers::Button))
  {
    recursive = true;

    if(animationTags.contains(Tags::Animations::startClick))
    {
      animationTags.insert(Tags::Animations::startClickOffset);
    }else if(animationTags.contains(Tags::Animations::endClick))
    {
      animationTags.insert(Tags::Animations::endClickOffset);
    }
  }

  if(animationTags.contains(Tags::Animations::startClick))
  {
    handeledAnimation |= handleClick(animationData, true);
  }else if(animationTags.contains(Tags::Animations::endClick))
  {
    handeledAnimation |= handleClick(animationData, false);
  }

  if(animationTags.contains(Tags::Animations::startClickOffset))
  {
    handeledAnimation |= handleClickOffset(animationData, true);
  }else if(animationTags.contains(Tags::Animations::endClickOffset))
  {
    handeledAnimation |= handleClickOffset(animationData, false);
  }

  if(animationTags.contains(Tags::Animations::mouseEnter))
  {
    handeledAnimation |= handleHover(animationData, true);
  }else if(animationTags.contains(Tags::Animations::mouseLeave))
  {
    handeledAnimation |= handleHover(animationData, false);
  }

  return handeledAnimation;
}

bool ThemeWithDefaultAnimations::handleHover(const InOutput<AnimationData>& animationData, bool start)
{
  AnimationMap& animationMap = animationData.value.animationMap;

  real mouseOverFadeDuration = 0.25f;

  animationMap.fadeToDefault(AnimationSlots::RenderState::mouseHover,
                             start ? mouseOverFadeDuration : mouseOverFadeDuration*2.f,
                             Animation::FadeInterpolation::EASY_IN);

  return true;
}

bool ThemeWithDefaultAnimations::handleClick(const InOutput<AnimationData>& animationData,
                                             bool start)
{
  AnimationMap& animationMap = animationData.value.animationMap;

  if(start)
  {
    animationMap.snapToValue(AnimationSlots::RenderState::mousePressed, 1.f);
    animationMap.snapToValue(AnimationSlots::RenderState::mouseHover, 0.f);
  }else
  {
    animationMap.snapToDefault(AnimationSlots::RenderState::mousePressed);
    animationMap.snapToDefault(AnimationSlots::RenderState::mouseHover);
  }
  return true;
}

bool ThemeWithDefaultAnimations::handleClickOffset(const InOutput<AnimationData>& animationData,
                                                   bool start)
{
  AnimationMap& animationMap = animationData.value.animationMap;

  if(start)
  {
    animationMap.snapToValue(AnimationSlots::RenderState::xAllocationOffset, 1.f);
    animationMap.snapToValue(AnimationSlots::RenderState::yAllocationOffset, 1.f);
  }else
  {
    animationMap.snapToDefault(AnimationSlots::RenderState::xAllocationOffset);
    animationMap.snapToDefault(AnimationSlots::RenderState::yAllocationOffset);
  }
  return true;
}


} // namespace Themes
} // namespace Gui
} // namespace Framework
