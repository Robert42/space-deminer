#ifndef FRAMEWORK_GUI_THEMES_HOLOFRAME_H
#define FRAMEWORK_GUI_THEMES_HOLOFRAME_H

#include "../theme.h"

#include <framework/developer-tools/gui-system/painter.h>
#include <framework/developer-tools/gui-system/base-line-drawer.h>

namespace Framework {
namespace Gui {
namespace Themes {

class HoloFramePainter;
class HoloFrame
{
public:
  class Section
  {
  public:
    typedef std::shared_ptr<Section> Ptr;

  private:
    real _length;
    real _totalWidth;
    real _paddingStart, _paddingEnd;
    real _relativePosition;
    real _absolutePosition;
    real _scaleLengthOrigin;
    real _scaleLengthFactor;
    real _scaleEffectFactor;

  public:
    Section();
    virtual ~Section();

    virtual real offset(real xPosition) const;
    virtual real outerThicknessChange(real xPosition) const;
    virtual real innerThicknessChange(real xPosition) const;
    virtual real offset() const;
    virtual real innerThicknessChange() const;
    virtual real outerThicknessChange() const;

    bool useThisSection() const;
    real scaleEffectFactor() const;
    void setScaleEffectFactor(real scaleEffectFactor);
    real scaleLengthFactor() const;
    void setScaleLengthFactor(real scaleLengthFactor);
    real scaleLengthOrigin() const;
    void setScaleLengthOrigin(real scaleLengthOrigin);

    real paddingStart() const;
    void setPaddingStart(real padding);

    real paddingEnd() const;
    void setPaddingEnd(real padding);

    real relativePosition() const;
    void setRelativePosition(real position);

    real totalWidth() const;
    void setTotalWidth(real totalWidth);

    real length() const;
    void setLength(real length);

    real absolutePosition() const;

    real sectionStart() const;
    real sectionEnd() const;

  public:
    virtual void paint(const InOutput< std::set<real> >& points) = 0;
    virtual void update();

  private:
    void _updateAbsolutePosition();
  };

  class SectionList
  {
  private:
    mat3 matrix;
    real _width;

    QVector<Section::Ptr> sections;

  public:
    SectionList();

    real offset() const;
    real offset(real xPosition) const;

    real innerThicknessChange() const;
    real innerThicknessChange(real xPosition) const;

    real outerThicknessChange() const;
    real outerThicknessChange(real xPosition) const;

    void addSection(const Section::Ptr& section);
    real width() const;
    void setWidth(real _width);

    void setMatrix(const vec2& start, const vec2& end);
    void setMatrix(const vec2& startPoint, real angle, real additionalOffset=0.f);
    void setMatrixDiscrete(const vec2& startPoint, int angle, real additionalOffset=0.f);

    void update();

  private:
    friend class HoloFramePainter;

    void _sortSections();
    void _paint(HoloFramePainter& holoFramePainter) const;
  };

  class LinearInterpolatedSection : public Section
  {
  protected:
    real interpolationFactorForXPosition(const real xPosition) const;

    void paint(const InOutput< std::set<real> >& points) final override;
  };

  class ChangeThickness final : public LinearInterpolatedSection
  {
  public:
    typedef std::shared_ptr<ChangeThickness> Ptr;

  private:
    real _innerThicknessChange, _outerThicknessChange;

  public:
    ChangeThickness(real width, real innerThicknessChange, real outerThicknessChange);

    static Ptr create(real width, real innerThicknessChange, real outerThicknessChange);

    real innerThicknessChange(real xPosition) const override;
    real innerThicknessChange() const override;
    void setInnerThicknessChange(real innerThicknessChange);

    real outerThicknessChange(real xPosition) const override;
    real outerThicknessChange() const override;
    void setOuterThicknessChange(real outerThicknessChange);
  };

  class OffsetSection final : public LinearInterpolatedSection
  {
  public:
    typedef std::shared_ptr<OffsetSection> Ptr;

  private:
    real _offset;

  public:
    OffsetSection(real width, real offset);

    static Ptr create(real width, real offset);

    real offset(real xPosition) const override;
    real offset() const override;
    void setOffset(real offset);
  };

  class Corner
  {
  public:
    typedef std::shared_ptr<Corner> Ptr;

  public:
    vec2 cornerPoint;

  public:
    Corner();
    virtual ~Corner();

  public:
    virtual void paint(HoloFramePainter& holoFramePainter) = 0;
    virtual vec2 size() const = 0;
    virtual void update();
  };

  class SimpleCorner final : public Corner
  {
  public:
    typedef std::shared_ptr<SimpleCorner> Ptr;

  public:
    void paint(HoloFramePainter& holoFramePainter) override;
    vec2 size() const override;

    static Ptr create();
  };

  class FlattenedCorner final : public Corner
  {
  public:
    typedef std::shared_ptr<FlattenedCorner> Ptr;

  private:
    vec2 _virtualSize;
    vec2 _size;

  public:
    SectionList subSections;

  public:
    FlattenedCorner(const vec2& virtualSize);

    void paint(HoloFramePainter& holoFramePainter) override;

    void setVirtualSize(const vec2& size);
    vec2 virtualSize() const;
    vec2 size() const override;

    static Ptr create(const vec2& size);

    void update() override;

  private:
    vec2 startPoint() const;
    vec2 endPoint() const;
    vec2 virtualStartPoint() const;
    vec2 virtualEndPoint() const;
  };

public:
  Rectangle<vec2> rectangle;
  Corner::Ptr cornerNW, cornerSW, cornerSE, cornerNE;
  SectionList north, west, south, east;

public:
  HoloFrame();

public:
  void draw(HoloFramePainter& holoFramePainter);
};


class HoloFramePainter final
{
private:
  VertexBufferLineDrawer lineDrawer;
  Painter& painter;

  real _innerThickness;
  real _outerThickness;
  real _offset;
  mat3 matrix;
  vec4 _color;

public:
  HoloFramePainter(Painter& painter);

public:
  void setTotalThickness(real thickness);
  void setThickness(real inner, real right);
  void setInnerThickness(real inner);
  void setOuterThickness(real outer);

  real innerThickness() const;
  real outerThickness() const;

  real offset() const;
  void setOffset(real offset);

  const vec4& color() const;
  void setColor(const vec4& color);

  void begin();
  void drawPoint(const vec2& point);
  void end();

  void paintCorner(const HoloFrame::Corner::Ptr& corner, int angle);
  void paintSectionList(const HoloFrame::SectionList& sectionList);
};

} // namespace Themes
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_THEMES_HOLOFRAME_H
