#ifndef FRAMEWORK_GUI_THEMES_PALETTEBASEDTHEME_H
#define FRAMEWORK_GUI_THEMES_PALETTEBASEDTHEME_H

#include "theme-with-default-animations.h"

namespace Framework {
namespace Gui {
namespace Themes {

class PaletteBasedTheme : public ThemeWithDefaultAnimations
{
public:
  class ColorPalette
  {
  public:
    struct Gradient
    {
      vec4 colorsA[3];
      vec4 colorsB[3];
      real positionA[3];
      real positionB[3];

      Gradient(const vec4& color = vec4(0));

      int nPositions()const{return 3;}
      bool uninitialiedPositions() const {return positionB[nPositions()-1]==0.f;}

      Gradient& operator *=(const vec4& factor);
      Gradient operator *(const vec4& factor) const;
      Gradient& operator *=(real factor);
      Gradient operator *(real factor) const;
      Gradient& operator /=(real factor);
      Gradient operator /(real factor) const;
      Gradient& operator +=(const Gradient& other);
      Gradient operator +(const Gradient& other) const;
      Gradient& operator -=(const Gradient& other);
      Gradient operator -(const Gradient& other) const;

      void fill(const vec4& value);

    private:
      void copyOthersPositionsIfNecessary(const Gradient& other);
    };

    Gradient invisibleGradient;

    vec4 background;

    vec4 buttonShadow;
    vec4 buttonShadow_Insensitive;
    vec4 buttonShadow_Pressed;
    vec4 buttonOuterBorder;
    vec4 buttonOuterBorder_Insensitive;
    vec4 buttonOuterBorder_Pressed;
    vec4 buttonInnerBorder;
    vec4 buttonInnerBorder_Insensitive;
    vec4 buttonInnerBorder_Pressed;
    vec4 buttonInnerBorder_KeyFocus;

    vec4 buttonHighlight_Hover;
    vec4 buttonHighlight_Pressed;
    vec4 buttonHighlight_ActivatedHover;
    vec4 buttonHighlight_KeyFocusHover;

    vec4 defaultButtonColor;

    Gradient buttonShine_Default;
    Gradient buttonShine_Insensitive;
    Gradient buttonShine_Hover;
    Gradient buttonShine_Pressed;

    vec4 embossLight;
    vec4 embossDark;

    vec4 textColor;

    void initDark();
    void init(const vec4& backgroundColor);
    void init(const vec4& backgroundColor, const vec4& textColor);
  }colors;

  class VerySimpleTextWidget final : public SimpleTextWidget
  {
  public:
    typedef std::shared_ptr<VerySimpleTextWidget> Ptr;

  public:
    const bool prependLineSpacing;
    const Font font;
    PaletteBasedTheme& paletteBasedTheme;

  public:
    VerySimpleTextWidget(PaletteBasedTheme& paletteBasedTheme, const Font& font, bool prependLineSpacing = true);

    static Ptr create(PaletteBasedTheme& paletteBasedTheme, const Font& font, bool prependLineSpacing = true);
    static Ptr create(PaletteBasedTheme* paletteBasedTheme, const Font& font, bool prependLineSpacing = true);

    vec2 minimumSize(const String& text) const override;
    vec2 optimalSize(const String& text) const override;

    void draw(const String &text, Painter &painter, const RenderData &renderData) override;
  };

public:
  PaletteBasedTheme();
};

PaletteBasedTheme::ColorPalette::Gradient premultiply(PaletteBasedTheme::ColorPalette::Gradient gradient);
PaletteBasedTheme::ColorPalette::Gradient unpremultiply(PaletteBasedTheme::ColorPalette::Gradient gradient);

} // namespace Themes
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_THEMES_PALETTEBASEDTHEME_H
