#ifndef FRAMEWORK_GUI_THEMES_MIXEDTHEME_H
#define FRAMEWORK_GUI_THEMES_MIXEDTHEME_H

#include "../theme.h"

namespace Framework {
namespace Gui {
namespace Themes {


class MixedTheme final : public Theme
{
public:
  typedef std::shared_ptr<MixedTheme> Ptr;

public:
  const QVector<Theme::Ptr> themes;

public:
  MixedTheme(const QVector<Theme::Ptr>& themes);

  static Ptr create(const QVector<Theme::Ptr>& themes);

public:
  Optional<SimpleWidget::Ptr> simpleWidget(const ClassType& widgetType,
                                           const TagSet& tags=TagSet()) override;
  Optional<SimpleTextWidget::Ptr> simpleTextWidget(const ClassType& widgetType,
                                                   const TagSet& tags) override;
  Optional<const Metrics*> layoutMetrics(const ClassType& typeLayout,
                                         const ClassType& typeWidget,
                                         const TagSet& tagsLayout=TagSet(),
                                         const TagSet& tagsWidget=TagSet()) override;
  Optional<const Metrics*> layoutSpacingItemMetrics(const ClassType& typeLayout,
                                                    const ClassType& typeWidget,
                                                    const TagSet& tagsLayout=TagSet(),
                                                    const TagSet& tagsWidget=TagSet()) override;

  bool playAnimation(const InOutput<AnimationData>& animationData) override;
};


} // namespace Themes
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_THEMES_MIXEDTHEME_H
