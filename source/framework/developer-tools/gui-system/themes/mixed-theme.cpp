#include "mixed-theme.h"

namespace Framework {
namespace Gui {
namespace Themes {


MixedTheme::MixedTheme(const QVector<Theme::Ptr>& themes)
  : themes(themes)
{
}

MixedTheme::Ptr MixedTheme::create(const QVector<Theme::Ptr>& themes)
{
  return Ptr(new MixedTheme(themes));
}


Optional<Theme::SimpleWidget::Ptr> MixedTheme::simpleWidget(const ClassType& widgetType,
                                                            const TagSet& tags)
{
  for(const Theme::Ptr& theme : this->themes)
  {
    Optional<Theme::SimpleWidget::Ptr> result = theme->simpleWidget(widgetType,
                                                                    tags);
    if(result)
      return result;
  }

  return nothing;
}

Optional<Theme::SimpleTextWidget::Ptr> MixedTheme::simpleTextWidget(const ClassType& widgetType,
                                                                    const TagSet& tags)
{
  for(const Theme::Ptr& theme : this->themes)
  {
    Optional<Theme::SimpleTextWidget::Ptr> result = theme->simpleTextWidget(widgetType,
                                                                            tags);
    if(result)
      return result;
  }

  return nothing;
}

Optional<const Metrics*> MixedTheme::layoutMetrics(const ClassType& typeLayout,
                                                   const ClassType& typeWidget,
                                                   const TagSet& tagsLayout,
                                                   const TagSet& tagsWidget)
{
  for(const Theme::Ptr& theme : this->themes)
  {
    Optional<const Metrics*> result = theme->layoutMetrics(typeLayout,
                                                           typeWidget,
                                                           tagsLayout,
                                                           tagsWidget);
    if(result)
      return result;
  }

  return nothing;
}

Optional<const Metrics*> MixedTheme::layoutSpacingItemMetrics(const ClassType& typeLayout,
                                                              const ClassType& typeWidget,
                                                              const TagSet& tagsLayout,
                                                              const TagSet& tagsWidget)
{
  for(const Theme::Ptr& theme : this->themes)
  {
    Optional<const Metrics*> result = theme->layoutSpacingItemMetrics(typeLayout,
                                                                      typeWidget,
                                                                      tagsLayout,
                                                                      tagsWidget);
    if(result)
      return result;
  }

  return nothing;
}


bool MixedTheme::playAnimation(const InOutput<AnimationData>& animationData)
{
  for(const Theme::Ptr& theme : this->themes)
  {
    bool result = theme->playAnimation(animationData);
    if(result)
      return true;
  }

  return false;
}


} // namespace Themes
} // namespace Gui
} // namespace Framework
