#include "holo-frame.h"
#include <base/geometry/matrix3-transformations.h>
#include <base/interpolation.h>

namespace Framework {
namespace Gui {
namespace Themes {

const real smallestPossibleLength = 1.e-6f;

HoloFramePainter::HoloFramePainter(Painter& painter)
  : painter(painter),
    matrix(1),
    _color(1, 0.5f, 0, 1)
{
  lineDrawer.leftAntialiasing = false;
  lineDrawer.rightAntialiasing = false;
  lineDrawer.antialiasing = 0.f;
  setThickness(0.f, 8.f);
  setOffset(0.f);
}

void HoloFramePainter::setTotalThickness(real thickness)
{
  setThickness(thickness*0.5f,
               thickness*0.5f);
}

void HoloFramePainter::setThickness(real inner, real outer)
{
  this->_innerThickness = inner;
  this->_outerThickness = outer;

  this->lineDrawer.thickness = inner+outer;
  this->lineDrawer.trailOffset = (outer-inner)*0.5f;
}

void HoloFramePainter::setInnerThickness(real inner)
{
  setThickness(inner, this->_outerThickness);
}

void HoloFramePainter::setOuterThickness(real outer)
{
  setThickness(this->_innerThickness, outer);
}

real HoloFramePainter::innerThickness() const
{
  return this->_innerThickness;
}

real HoloFramePainter::outerThickness() const
{
  return this->_outerThickness;
}

real HoloFramePainter::offset() const
{
  return _offset;
}

void HoloFramePainter::setOffset(real offset)
{
  this->_offset = offset;
}

const vec4& HoloFramePainter::color() const
{
  return this->_color;
}

void HoloFramePainter::setColor(const vec4& color)
{
  this->_color = color;
}

void HoloFramePainter::begin()
{
  lineDrawer.beginLoop();
}

void HoloFramePainter::drawPoint(const vec2& point)
{
  vec2 transformedPoint = (matrix * vec3(point + vec2(0.f, offset()), 1)).xy();

  lineDrawer.drawTo(transformedPoint);
}

void HoloFramePainter::end()
{
  lineDrawer.end();
  lineDrawer.removeLast();

  painter.drawQuadStrip(lineDrawer.outlineVerticesLeft(),
                        lineDrawer.outlineVerticesRight(),
                        color(),
                        true);
}

void HoloFramePainter::paintCorner(const HoloFrame::Corner::Ptr& corner, int angle)
{
  mat3 previous = this->matrix;
  this->matrix *= Geometry::translate3(corner->cornerPoint) * Geometry::rotateDiscrete3(angle);

  corner->paint(*this);

  this->matrix = previous;
}

void HoloFramePainter::paintSectionList(const HoloFrame::SectionList& sectionList)
{
  mat3 previous = this->matrix;
  this->matrix *= sectionList.matrix;

  sectionList._paint(*this);

  this->matrix = previous;
}


// ====


HoloFrame::HoloFrame()
{
  this->cornerNW = SimpleCorner::create();
  this->cornerSW = SimpleCorner::create();
  this->cornerSE = SimpleCorner::create();
  this->cornerNE = SimpleCorner::create();
}


void HoloFrame::draw(HoloFramePainter& holoFramePainter)
{
  cornerNW->update();
  cornerSW->update();
  cornerSE->update();
  cornerNE->update();

  cornerNW->cornerPoint = rectangle.min();
  cornerSW->cornerPoint.x = rectangle.min().x;
  cornerSW->cornerPoint.y = rectangle.max().y;
  cornerSE->cornerPoint = rectangle.max();
  cornerNE->cornerPoint.x = rectangle.max().x;
  cornerNE->cornerPoint.y = rectangle.min().y;

  cornerSW->cornerPoint.x -= west.offset();
  cornerSE->cornerPoint.y += south.offset();
  cornerNE->cornerPoint.x += east.offset();
  cornerNW->cornerPoint.y -= north.offset();

  north.setWidth(distance(cornerNW->cornerPoint, cornerNE->cornerPoint) - cornerNW->size().x - cornerNE->size().x);
  south.setWidth(distance(cornerSW->cornerPoint, cornerSE->cornerPoint) - cornerSW->size().x - cornerSE->size().x);
  west.setWidth(distance(cornerNW->cornerPoint, cornerSW->cornerPoint) - cornerNW->size().y - cornerSW->size().y);
  east.setWidth(distance(cornerNE->cornerPoint, cornerSE->cornerPoint) - cornerNE->size().y - cornerSE->size().y);

  north.setMatrixDiscrete(cornerNE->cornerPoint, 2, cornerNE->size().x);
  west.setMatrixDiscrete(cornerNW->cornerPoint, 1, cornerNW->size().y);
  south.setMatrixDiscrete(cornerSW->cornerPoint, 0, cornerSW->size().x);
  east.setMatrixDiscrete(cornerSE->cornerPoint, 3, cornerSE->size().y);

  north.update();
  west.update();
  south.update();
  east.update();

  holoFramePainter.begin();
  holoFramePainter.paintCorner(cornerNW, 0);
  holoFramePainter.paintSectionList(west);
  holoFramePainter.paintCorner(cornerSW, 3);
  holoFramePainter.paintSectionList(south);
  holoFramePainter.paintCorner(cornerSE, 2);
  holoFramePainter.paintSectionList(east);
  holoFramePainter.paintCorner(cornerNE, 1);
  holoFramePainter.paintSectionList(north);
  holoFramePainter.end();
}


HoloFrame::Corner::Corner()
  : cornerPoint(0)
{
}

HoloFrame::Corner::~Corner()
{
}

void HoloFrame::Corner::update()
{
}


void HoloFrame::SimpleCorner::paint(HoloFramePainter& holoFramePainter)
{
  holoFramePainter.drawPoint(vec2(0));
}

vec2 HoloFrame::SimpleCorner::size() const
{
  return vec2(0);
}

HoloFrame::SimpleCorner::Ptr HoloFrame::SimpleCorner::create()
{
  return Ptr(new SimpleCorner);
}


HoloFrame::FlattenedCorner::FlattenedCorner(const vec2 &virtualSize)
  : _virtualSize(0.f),
    _size(0.f)
{
  setVirtualSize(virtualSize);
}

void HoloFrame::FlattenedCorner::paint(HoloFramePainter& holoFramePainter)
{
  const vec2 start = startPoint();
  const vec2 end = endPoint();

  holoFramePainter.drawPoint(start);

  if(size() == vec2(0))
    return;

  holoFramePainter.paintSectionList(subSections);

  holoFramePainter.drawPoint(end);
}

void HoloFrame::FlattenedCorner::setVirtualSize(const vec2& size)
{
  if(this->virtualSize() != size)
  {
    this->_virtualSize = size;

    update();
  }
}

void HoloFrame::FlattenedCorner::update()
{
  vec2 size = virtualSize();

  size.y += dot(vec2(0, 1),
                normalize(perpendicular270Degree(_virtualSize))*subSections.offset());

  size = max(vec2(0), size);

  if(this->size() != size)
  {
    this->_size = size;

    if(this->size() != vec2(0))
    {
      const vec2 start = virtualStartPoint();
      const vec2 end = virtualEndPoint();

      subSections.setMatrix(start, end);
      subSections.setWidth(length(this->size()));
    }
  }

  this->subSections.update();
}

vec2 HoloFrame::FlattenedCorner::virtualSize() const
{
  return _virtualSize;
}

vec2 HoloFrame::FlattenedCorner::size() const
{
  return _size;
}

HoloFrame::FlattenedCorner::Ptr HoloFrame::FlattenedCorner::create(const vec2& size)
{
  return Ptr(new FlattenedCorner(size));
}

vec2 HoloFrame::FlattenedCorner::startPoint() const
{
  return vec2(size().x, 0);
}

vec2 HoloFrame::FlattenedCorner::endPoint() const
{
  return vec2(0, size().y);
}

vec2 HoloFrame::FlattenedCorner::virtualStartPoint() const
{
  return vec2(virtualSize().x, 0);
}

vec2 HoloFrame::FlattenedCorner::virtualEndPoint() const
{
  return vec2(0, virtualSize().y);
}


HoloFrame::Section::Section()
  : _length(smallestPossibleLength),
    _totalWidth(0.f),
    _paddingStart(0),
    _paddingEnd(0),
    _relativePosition(0.5f),
    _absolutePosition(0.f),
    _scaleLengthOrigin(0.5f),
    _scaleLengthFactor(1.f),
    _scaleEffectFactor(1.f)
{
}

HoloFrame::Section::~Section()
{
}

real HoloFrame::Section::offset(real xPosition) const
{
  (void)xPosition;
  return 0.f;
}

real HoloFrame::Section::innerThicknessChange(real xPosition) const
{
  (void)xPosition;
  return 0.f;
}

real HoloFrame::Section::outerThicknessChange(real xPosition) const
{
  (void)xPosition;
  return 0.f;
}

real HoloFrame::Section::offset() const
{
  return offset(totalWidth());
}

real HoloFrame::Section::innerThicknessChange() const
{
  return innerThicknessChange(totalWidth());
}

real HoloFrame::Section::outerThicknessChange() const
{
  return outerThicknessChange(totalWidth());
}

bool HoloFrame::Section::useThisSection() const
{
  return scaleEffectFactor()!=0.f;
}

real HoloFrame::Section::scaleLengthOrigin() const
{
  return _scaleLengthOrigin;
}

void HoloFrame::Section::setScaleLengthOrigin(real scaleLengthOrigin)
{
  if(this->scaleLengthOrigin() != scaleLengthOrigin)
  {
    this->_scaleLengthOrigin = scaleLengthOrigin;
  }
}

real HoloFrame::Section::scaleLengthFactor() const
{
  return _scaleLengthFactor;
}

void HoloFrame::Section::setScaleLengthFactor(real scaleLengthFactor)
{
  if(this->scaleLengthFactor() != scaleLengthFactor)
  {
    this->_scaleLengthFactor = scaleLengthFactor;
  }
}

real HoloFrame::Section::scaleEffectFactor() const
{
  return _scaleEffectFactor;
}

void HoloFrame::Section::setScaleEffectFactor(real scaleEffectFactor)
{
  if(this->scaleEffectFactor() != scaleEffectFactor)
  {
    this->_scaleEffectFactor = scaleEffectFactor;
  }
}

real HoloFrame::Section::paddingStart() const
{
  return _paddingStart;
}

void HoloFrame::Section::setPaddingStart(real padding)
{
  if(this->paddingStart() != padding)
  {
    this->_paddingStart = padding;
  }
}

real HoloFrame::Section::paddingEnd() const
{
  return _paddingEnd;
}

void HoloFrame::Section::setPaddingEnd(real padding)
{
  if(this->paddingEnd() != padding)
  {
    this->_paddingEnd = padding;
  }
}

real HoloFrame::Section::relativePosition() const
{
  return _relativePosition;
}

void HoloFrame::Section::setRelativePosition(real position)
{
  if(this->relativePosition() != position)
  {
    this->_relativePosition = position;
  }
}

real HoloFrame::Section::totalWidth() const
{
  return this->_totalWidth;
}

void HoloFrame::Section::setTotalWidth(real totalWidth)
{
  if(_totalWidth != totalWidth)
  {
    _totalWidth = totalWidth;
  }
}

real HoloFrame::Section::length() const
{
  return _length;
}

void HoloFrame::Section::setLength(real length)
{
  length = max(smallestPossibleLength, length);

  if(this->length() != length)
  {
    this->_length = length;
  }
}

real HoloFrame::Section::absolutePosition() const
{
  return _absolutePosition;
}

real HoloFrame::Section::sectionStart() const
{
  real origin = length() * scaleLengthOrigin();

  return absolutePosition() + origin * (1.f - scaleLengthFactor());
}

real HoloFrame::Section::sectionEnd() const
{
  return sectionStart() + max(smallestPossibleLength,
                              length()*scaleLengthFactor());
}

void HoloFrame::Section::update()
{
  _updateAbsolutePosition();
}

void HoloFrame::Section::_updateAbsolutePosition()
{
  const real length = this->length();

  _absolutePosition = max(0.f, this->totalWidth() - paddingStart() - paddingEnd() - length) * relativePosition() + paddingStart();
  _absolutePosition = clamp(absolutePosition(), 0.f, this->totalWidth()-length);
}


HoloFrame::SectionList::SectionList()
  : matrix(1),
    _width(0)
{
}

real HoloFrame::SectionList::offset() const
{
  real totalOffset = 0.f;

  for(const Section::Ptr& section : this->sections)
    totalOffset += section->scaleEffectFactor() *
                   section->offset();

  return totalOffset;
}

real HoloFrame::SectionList::offset(real xPosition) const
{
  real totalOffset = 0.f;

  for(const Section::Ptr& section : this->sections)
    totalOffset += section->scaleEffectFactor() *
                   section->offset(xPosition);

  return totalOffset;
}

real HoloFrame::SectionList::innerThicknessChange() const
{
  real totalThicknessChange = 0.f;

  for(const Section::Ptr& section : this->sections)
    totalThicknessChange += section->scaleEffectFactor() *
                            section->innerThicknessChange();

  return totalThicknessChange;
}

real HoloFrame::SectionList::innerThicknessChange(real xPosition) const
{
  real totalThicknessChange = 0.f;

  for(const Section::Ptr& section : this->sections)
    totalThicknessChange += section->scaleEffectFactor() *
                            section->innerThicknessChange(xPosition);

  return totalThicknessChange;
}

real HoloFrame::SectionList::outerThicknessChange() const
{
  real totalThicknessChange = 0.f;

  for(const Section::Ptr& section : this->sections)
    totalThicknessChange += section->scaleEffectFactor() *
                            section->outerThicknessChange();

  return totalThicknessChange;
}

real HoloFrame::SectionList::outerThicknessChange(real xPosition) const
{
  real totalThicknessChange = 0.f;

  for(const Section::Ptr& section : this->sections)
    totalThicknessChange += section->scaleEffectFactor() *
                            section->outerThicknessChange(xPosition);

  return totalThicknessChange;
}

void HoloFrame::SectionList::addSection(const Section::Ptr& section)
{
  section->setTotalWidth(_width);
  sections.append(section);

  _sortSections();
}

void HoloFrame::SectionList::setWidth(real width)
{
  this->_width = width;

  for(const Section::Ptr& section : this->sections)
    section->setTotalWidth(width);

  _sortSections();
}

real HoloFrame::SectionList::width() const
{
  return this->_width;
}

void HoloFrame::SectionList::setMatrix(const vec2& start, const vec2& end)
{
  vec2 direction = normalize(end-start);
  vec2 orthoDirection = perpendicular90Degree(direction);

  matrix = mat3(direction.x, direction.y, 0.f,
                orthoDirection.x, orthoDirection.y, 0.f,
                start.x, start.y, 1.f);
}

void HoloFrame::SectionList::setMatrix(const vec2& startPoint, real angle, real additionalOffset)
{
  matrix = Geometry::translate3(startPoint) * Geometry::rotate3(angle) * Geometry::translate3(vec2(additionalOffset, 0.f));
}

void HoloFrame::SectionList::setMatrixDiscrete(const vec2& startPoint, int angle, real additionalOffset)
{
  matrix = Geometry::translate3(startPoint) * Geometry::rotateDiscrete3(angle) * Geometry::translate3(vec2(additionalOffset, 0.f));
}

void HoloFrame::SectionList::update()
{
  for(const Section::Ptr& section : this->sections)
    section->update();
}

void HoloFrame::SectionList::_paint(HoloFramePainter& holoFramePainter) const
{
  real originalInnerThickness = holoFramePainter.innerThickness();
  real originalOuterThickness = holoFramePainter.outerThickness();
  real totalOffset = 0.f;
  std::set<real> unclampedPoints;

  for(const Section::Ptr& section : this->sections)
  {
    if(section->useThisSection())
      section->paint(inout(unclampedPoints));
  }

  std::set<real> points;
  for(real p : unclampedPoints)
  {
    if(p>=smallestPossibleLength &&
       smallestPossibleLength <=this->width()-smallestPossibleLength)
      points.insert(p);
  }

  for(real p : points)
  {
    holoFramePainter.setOffset(totalOffset + offset(p));
    holoFramePainter.setInnerThickness(originalInnerThickness + innerThicknessChange(p));
    holoFramePainter.setOuterThickness(originalOuterThickness + outerThicknessChange(p));
    holoFramePainter.drawPoint(vec2(p, 0.f));
  }

  holoFramePainter.setOffset(0.f);
  holoFramePainter.setInnerThickness(originalInnerThickness + innerThicknessChange());
  holoFramePainter.setOuterThickness(originalOuterThickness + outerThicknessChange());
}

void HoloFrame::SectionList::_sortSections()
{
  qSort(sections.begin(),
        sections.end(),
        [](const Section::Ptr& a, const Section::Ptr& b) {
    return a->absolutePosition() < b->absolutePosition();
  });
}


real HoloFrame::LinearInterpolatedSection::interpolationFactorForXPosition(const real xPosition) const
{
  return clamp(interpolationWeightOf(xPosition,
                                     sectionStart(),
                                     sectionEnd()),
               0.f,
               1.f);
}

void HoloFrame::LinearInterpolatedSection::paint(const InOutput< std::set<real> >& points)
{
  points.value.insert(sectionStart());
  points.value.insert(sectionEnd());
}


HoloFrame::ChangeThickness::ChangeThickness(real width, real innerThicknessChange, real outerThicknessChange)
  : _innerThicknessChange(innerThicknessChange),
    _outerThicknessChange(outerThicknessChange)
{
  setLength(width);
}

HoloFrame::ChangeThickness::Ptr HoloFrame::ChangeThickness::create(real width, real innerThicknessChange, real outerThicknessChange)
{
  return Ptr(new ChangeThickness(width, innerThicknessChange, outerThicknessChange));
}


real HoloFrame::ChangeThickness::innerThicknessChange(real xPosition) const
{
  return interpolationFactorForXPosition(xPosition) * _innerThicknessChange;
}

real HoloFrame::ChangeThickness::innerThicknessChange() const
{
  return _innerThicknessChange;
}

void HoloFrame::ChangeThickness::setInnerThicknessChange(real innerThicknessChange)
{
  if(this->innerThicknessChange() != innerThicknessChange)
  {
    this->_innerThicknessChange = innerThicknessChange;
  }
}


real HoloFrame::ChangeThickness::outerThicknessChange(real xPosition) const
{
  return interpolationFactorForXPosition(xPosition) * _outerThicknessChange;
}

real HoloFrame::ChangeThickness::outerThicknessChange() const
{
  return _outerThicknessChange;
}

void HoloFrame::ChangeThickness::setOuterThicknessChange(real outerThicknessChange)
{
  if(this->outerThicknessChange() != outerThicknessChange)
  {
    this->_outerThicknessChange = outerThicknessChange;
  }
}


HoloFrame::OffsetSection::OffsetSection(real width, real offset)
  : _offset(offset)
{
  setLength(width);
}

HoloFrame::OffsetSection::Ptr HoloFrame::OffsetSection::create(real width, real offset)
{
  return Ptr(new OffsetSection(width, offset));
}

real HoloFrame::OffsetSection::offset(real xPosition) const
{
  return _offset * interpolationFactorForXPosition(xPosition);
}

real HoloFrame::OffsetSection::offset() const
{
  return _offset;
}

void HoloFrame::OffsetSection::setOffset(real offset)
{
  if(this->offset() != offset)
  {
    this->_offset = offset;
  }
}


} // namespace Themes
} // namespace Gui
} // namespace Framework
