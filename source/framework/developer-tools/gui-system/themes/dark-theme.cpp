#include "dark-theme.h"
#include "../types.h"
#include "../tags.h"

#include <base/geometry/rounded-rectangle.h>

namespace Framework {
namespace Gui {
namespace Themes {

class DarkTheme::LayoutMetrics
{
public:
  typedef std::shared_ptr<LayoutMetrics> Ptr;

public:
  virtual Optional<const Metrics*> metrics(const ClassType& layoutType,
                                           const ClassType& widgetType,
                                           const TagSet& layoutTags,
                                           const TagSet& widgetTags) = 0;
};


namespace DarkTheme_SimpleWidgets
{


class SimpleWidget : public Theme::SimpleWidget
{
public:
  DarkTheme& darkTheme;

public:
  SimpleWidget(DarkTheme& darkTheme)
    : darkTheme(darkTheme)
  {
  }
};

class Background : public SimpleWidget
{
private:
  Background(DarkTheme& darkTheme)
    : SimpleWidget(darkTheme)
  {
  }

public:
  static Ptr create(DarkTheme* darkTheme)
  {
    return Ptr(new Background(*darkTheme));
  }

  void draw(Painter &painter, const Theme::RenderData &renderData) override
  {
    painter.setZPosition(renderData.zPosition);
    painter.drawFilledRectangle(renderData.allocation,
                                darkTheme.colors.background);
  }

  bool isWidgetAtPoint(const vec2&, const Theme::RenderData&) const override
  {
    return true;
  }
};

class Button : public SimpleWidget
{
private:
  Button(DarkTheme& darkTheme)
    : SimpleWidget(darkTheme)
  {
  }

public:
  static Ptr create(DarkTheme* darkTheme)
  {
    return Ptr(new Button(*darkTheme));
  }

  Optional<vec2> minimumSize() const override
  {
    return vec2(darkTheme.smallRoundingRadius*2+1);
  }

  Optional<vec2> optimalSize() const override
  {
    return vec2(75, 25);
  }

  void draw(Painter &painter, const Theme::RenderData &renderData) override
  {
    const Rectangle<vec2>& allocation = renderData.allocation;
    real zPosition = renderData.zPosition;

    painter.setZPosition(zPosition);

    if(renderData.tags.contains(Tags::window_defaultWidget))
      darkTheme.drawDefaultButton(painter, allocation, renderData.state);
    else
      darkTheme.drawButton(painter, allocation, renderData.state);
  }

  bool isWidgetAtPoint(const vec2& point, const Theme::RenderData& renderData) const override
  {
    return darkTheme.isPointWithinButton(point, renderData.allocation);
  }
};

class WindowFrame : public SimpleWidget
{
private:
  WindowFrame(DarkTheme& darkTheme)
    : SimpleWidget(darkTheme)
  {
  }

public:
  static Ptr create(DarkTheme* darkTheme)
  {
    return Ptr(new WindowFrame(*darkTheme));
  }

  Optional<vec2> minimumSize() const override
  {
    return vec2(128, 128);
  }

  Optional<vec2> optimalSize() const override
  {
    return vec2(720, 480);
  }

  void draw(Painter &painter, const Theme::RenderData &renderData) override
  {
    const Rectangle<vec2>& allocation = renderData.allocation;
    real zPosition = renderData.zPosition;

    const real alpha = renderData.state.interpolateValues(0.f,
                                                          0.4f,
                                                          1.f, 1.f, 1.f,
                                                          1.f, 1.f, 1.f);
    const vec4 colorFactor = vec4(vec3(1), alpha);


    painter.setZPosition(zPosition);
    painter.drawFilledRectangle(darkTheme.recommendedFillRectangleForButtons(allocation),
                                darkTheme.colors.background * colorFactor);
    Theme::State dummyState;
    darkTheme.drawButtonFrame(painter, allocation, dummyState, alpha);
  }

  bool isWidgetAtPoint(const vec2& point, const Theme::RenderData& renderData) const override
  {
    return darkTheme.isPointWithinWindowFrame(point, renderData.allocation);
  }
};

class Canvas : public SimpleWidget
{
private:
  const Optional<vec2> _minimumSize;
  const Optional<vec2> _optimalSize;

  Canvas(DarkTheme& darkTheme, const Optional<vec2>& minimumSize, const Optional<vec2>& optimalSize)
    : SimpleWidget(darkTheme),
      _minimumSize(minimumSize),
      _optimalSize(optimalSize)
  {
  }

public:
  static Ptr create(DarkTheme* darkTheme, const Optional<vec2>& minimumSize, const Optional<vec2>& optimalSize)
  {
    return Ptr(new Canvas(*darkTheme, minimumSize, optimalSize));
  }

  Optional<vec2> minimumSize() const override
  {
    return _minimumSize;
  }

  Optional<vec2> optimalSize() const override
  {
    return _optimalSize;
  }

  void draw(Painter&, const Theme::RenderData&) override
  {
  }

  bool isWidgetAtPoint(const vec2&, const Theme::RenderData&) const override
  {
    return true;
  }
};

} // namespace DarkTheme_SimpleWidgets

namespace DarkTheme_Metrics
{


class WindowFrameMetrics : public DarkTheme::LayoutMetrics
{
private:
  Metrics mainArea;
  Metrics windowButtonArea;

public:
  WindowFrameMetrics()
  {
    mainArea.childExpansion = vec2(1);
    mainArea.padding = Metrics::Padding(DarkTheme::buttonBorderWidth());
    mainArea.padding.top += DarkTheme::windowButtonHeight();

    windowButtonArea.alignment = vec2(1, 0);
    windowButtonArea.heightLimit = DarkTheme::windowButtonHeight();
  }

  static Ptr create()
  {
    return Ptr(new WindowFrameMetrics);
  }

  Optional<const Metrics*> metrics(const ClassType& layoutType,
                                   const ClassType& widgetType,
                                   const TagSet& layoutTags,
                                   const TagSet& widgetTags) override
  {
    (void)layoutType;
    (void)widgetType;
    (void)widgetTags;

    if(layoutTags.contains(Tags::windowFrame_mainArea))
      return &mainArea;
    if(layoutTags.contains(Tags::windowFrame_windowButtonArea))
      return &windowButtonArea;
    return nothing;
  }
};


class Alignment : public DarkTheme::LayoutMetrics
{
private:
  Metrics noEffect;
  Metrics center;

public:
  Alignment()
  {
    center.alignment = vec2(0.5f);
  }

  static Ptr create()
  {
    return Ptr(new Alignment);
  }

  Optional<const Metrics*> metrics(const ClassType& layoutType,
                                   const ClassType& widgetType,
                                   const TagSet& layoutTags,
                                   const TagSet& widgetTags) override
  {
    (void)layoutType;
    (void)widgetType;
    (void)layoutTags;

    if(widgetTags.contains(Type(Types::Containers::Button).tag()))
    {
      return &center;
    }

    return &noEffect;
  }
};

class Border : public DarkTheme::LayoutMetrics
{
private:
  Metrics buttonBorder;
  Metrics defaultBorder;
  Metrics rootWidgetBorder;
  Metrics noBorder;

public:
  Border()
  {
    buttonBorder.padding = Metrics::Padding(3);

    defaultBorder.padding = Metrics::Padding(8);

    rootWidgetBorder.padding = Metrics::Padding(12);
  }

  static Ptr create()
  {
    return Ptr(new Border);
  }

  Optional<const Metrics*> metrics(const ClassType& layoutType,
                                   const ClassType& widgetType,
                                   const TagSet& layoutTags,
                                   const TagSet& widgetTags) override
  {
    (void)layoutType;
    (void)layoutTags;

    if(widgetType.isInheritingOrSame(Type(Types::Containers::Button)))
      return &buttonBorder;

    if(widgetTags.contains(Tags::windowRoot))
    {
      return &rootWidgetBorder;
    }

    return &defaultBorder;
  }
};

class BaseBox : public DarkTheme::LayoutMetrics
{
private:
  Metrics defaultBox;

public:
  BaseBox()
  {
    defaultBox.spacing = 6.f;
  }

  static Ptr create()
  {
    return Ptr(new BaseBox);
  }

  Optional<const Metrics*> metrics(const ClassType& layoutType,
                                   const ClassType& widgetType,
                                   const TagSet& layoutTags,
                                   const TagSet& widgetTags) override
  {
    (void)layoutType;
    (void)layoutTags;
    (void)widgetType;
    (void)widgetTags;

    return &defaultBox;
  }
};

class ZOffset : public DarkTheme::LayoutMetrics
{
private:
  Metrics noZOffset;

public:
  ZOffset()
  {
    noZOffset.zOffset = 0.f;
  }

  static Ptr create()
  {
    return Ptr(new ZOffset);
  }

  Optional<const Metrics*> metrics(const ClassType& layoutType,
                                   const ClassType& widgetType,
                                   const TagSet& layoutTags,
                                   const TagSet& widgetTags) override
  {
    (void)layoutType;
    (void)layoutTags;
    (void)widgetType;
    (void)widgetTags;

    return &noZOffset;
  }
};

} // namespace DarkTheme_Metrics

using namespace DarkTheme_SimpleWidgets;
using namespace DarkTheme_Metrics;

DarkTheme::DarkTheme(const FontManager::Ptr& fontManager)
  : smallRoundingRadius(4)
{
  colors.initDark();

  QList< TagSetWidgetRelation > canvasSimpleWidgets;
  canvasSimpleWidgets.append(TagSetWidgetRelation({Tags::Canvas::largeEditor}, Canvas::create(this, vec2(128), vec2(512))));
  canvasSimpleWidgets.append(TagSetWidgetRelation({Tags::Canvas::smallView}, Canvas::create(this, vec2(32), vec2(256))));
  canvasSimpleWidgets.append(TagSetWidgetRelation({Tags::Canvas::textSize},
                                                  Canvas::create(this,
                                                                 vec2(fontManager->defaultFont().lineHeight()*0.25f),
                                                                 vec2(fontManager->defaultFont().lineHeight()))));

  _simpleWidgetsDependingOnTags[Types::Widgets::Canvas] = canvasSimpleWidgets;

  _simpleWidgets[Types::Containers::Button] = Button::create(this);
  _simpleWidgets[Types::Containers::Background] = Background::create(this);
  _simpleWidgets[Types::Containers::WindowFrame] = WindowFrame::create(this);

  _simpleTextWidgets[Types::Widgets::Label] = PaletteBasedTheme::VerySimpleTextWidget::create(this, fontManager->defaultFont());

  _layoutMetricsForWidget[Types::Containers::WindowFrame] = WindowFrameMetrics::create();

  _layoutMetricsForLayout[Types::Layouts::Alignment] = Alignment::create();
  _layoutMetricsForLayout[Types::Layouts::Border] = Border::create();
  _layoutMetricsForLayout[Types::Layouts::BaseBox] = BaseBox::create();
  _layoutMetricsForLayout[Types::Layouts::ZOffset] = ZOffset::create();

}

DarkTheme::Ptr DarkTheme::create(const FontManager::Ptr& fontManager)
{
  return Ptr(new DarkTheme(fontManager));
}

Optional<Theme::SimpleWidget::Ptr> DarkTheme::simpleWidget(const ClassType& widgetType,
                                                           const TagSet& tags)
{
  Optional<QList<TagSetWidgetRelation>> tagDependentSimpleWidget = optionalFromHashMap(_simpleWidgetsDependingOnTags, widgetType);

  if(tagDependentSimpleWidget)
  {
    for(const TagSetWidgetRelation& r : *tagDependentSimpleWidget)
    {
      const TagSet& t = r.first;
      const Theme::SimpleWidget::Ptr& w = r.second;

      if(tags.contains(t))
        return w;
    }
  }

  return optionalFromHashMap(_simpleWidgets, widgetType);
}

Optional<Theme::SimpleTextWidget::Ptr> DarkTheme::simpleTextWidget(const ClassType& widgetType,
                                                                   const TagSet& tags)
{
  (void)tags;
  Optional<Theme::SimpleTextWidget::Ptr> simpleWidget = optionalFromHashMap(_simpleTextWidgets, widgetType);

  if(simpleWidget)
    return simpleWidget;
  else
    return optionalFromHashMap(_simpleTextWidgets, Types::Widgets::Label);
}

Optional<const Metrics*> DarkTheme::layoutMetrics(const ClassType& typeLayout,
                                                  const ClassType& typeWidget,
                                                  const TagSet& tagsLayout,
                                                  const TagSet& tagsWidget)
{
  Optional<LayoutMetrics::Ptr> _layoutMetrics;
  Optional<const Metrics*>  metrics;
  LayoutMetrics::Ptr layoutMetrics;

  _layoutMetrics = optionalFromHashMap(this->_layoutMetricsForWidget, typeWidget);
  if(_layoutMetrics)
  {
    layoutMetrics = *_layoutMetrics;

    metrics = layoutMetrics->metrics(typeLayout, typeWidget, tagsLayout, tagsWidget);
  }

  if(metrics)
    return metrics;

  _layoutMetrics = optionalFromHashMap(this->_layoutMetricsForLayout, typeLayout);
  if(_layoutMetrics)
  {
    layoutMetrics = *_layoutMetrics;

    metrics = layoutMetrics->metrics(typeLayout, typeWidget, tagsLayout, tagsWidget);
  }

  return metrics;
}

Optional<const Metrics*> DarkTheme::layoutSpacingItemMetrics(const ClassType& typeLayout,
                                                             const ClassType& typeWidget,
                                                             const TagSet& tagsLayout,
                                                             const TagSet& tagsWidget)
{
  (void)typeLayout;
  (void)typeWidget;
  (void)tagsLayout;
  (void)tagsWidget;
  return nothing;
}

typedef PaletteBasedTheme::ColorPalette::Gradient Gradient;
void DarkTheme::drawButtonShadow(Painter& painter, const Rectangle<vec2>& allocation, const Theme::State& state)
{
  RoundedRectangle shape(allocation, smallRoundingRadius);
  painter.drawRoundedRectangle(shape,
                               state.interpolateColors(colors.buttonShadow_Insensitive,
                                                       colors.buttonShadow, colors.buttonShadow, colors.buttonShadow_Pressed,
                                                       colors.buttonShadow, colors.buttonShadow, colors.buttonShadow_Pressed),
                               1.f,
                               0.5f);
}

void DarkTheme::drawButtonFrame(Painter& painter, const Rectangle<vec2>& allocation, const State& state, real alpha)
{
  vec4 colorFactor = vec4(vec3(1), alpha);

  RoundedRectangle shape(allocation, smallRoundingRadius);
  painter.drawRoundedRectangle(shape,
                               state.interpolateColors(colors.buttonOuterBorder_Insensitive,
                                                       colors.buttonOuterBorder, colors.buttonOuterBorder, colors.buttonOuterBorder_Pressed,
                                                       colors.buttonOuterBorder, colors.buttonOuterBorder, colors.buttonOuterBorder_Pressed) * colorFactor,
                               1.f,
                               -0.5f);
  painter.drawRoundedRectangle(shape,
                               state.interpolateColors(colors.buttonInnerBorder_Insensitive,
                                                       colors.buttonInnerBorder, colors.buttonInnerBorder, colors.buttonInnerBorder_Pressed,
                                                       colors.buttonInnerBorder_KeyFocus, colors.buttonInnerBorder_KeyFocus, colors.buttonInnerBorder_Pressed) * colorFactor,
                               1.0f,
                               -1.5f);
}

void DarkTheme::drawGradientVertical(Painter& painter,
                                     const Rectangle<vec2>& allocation,
                                     const Gradient& gradient) const
{
  for(int i=0; i<gradient.nPositions(); ++i)
  {
    painter.drawFilledRectangle(recommendedFillRectangleForButtons(allocation).relativeSubRect(Rectangle<vec2>(vec2(0, gradient.positionA[i]),
                                                                                                               vec2(1, gradient.positionB[i]))),
                                gradient.colorsA[i],
                                gradient.colorsB[i]);
  }
}

void DarkTheme::drawButtonShine(Painter& painter, const Rectangle<vec2>& allocation, const Theme::State& state)
{
  painter.drawFilledRectangle(recommendedFillRectangleForButtons(allocation).expandedRect(1),
                              state.interpolateColors(vec4(0),
                                                      vec4(0), colors.buttonHighlight_Hover, colors.buttonHighlight_Pressed,
                                                      vec4(0), colors.buttonHighlight_KeyFocusHover, colors.buttonHighlight_Pressed));
  drawGradientVertical(painter,
                       allocation,
                       state.interpolateColors(colors.buttonShine_Insensitive,
                                               colors.buttonShine_Default, colors.buttonShine_Hover, colors.buttonShine_Pressed,
                                               colors.buttonShine_Default, colors.buttonShine_Hover, colors.buttonShine_Pressed));
}

void DarkTheme::drawDefaultButton(Painter& painter, const Rectangle<vec2>& allocation, const Theme::State& state)
{
  painter.drawFilledRectangle(recommendedFillRectangleForButtons(allocation).expandedRect(1),
                              colors.defaultButtonColor * vec4(vec3(1), state.sensitive));
  drawButton(painter, allocation, state);
}

void DarkTheme::drawButton(Painter& painter, const Rectangle<vec2>& allocation, const State& state)
{
  drawButtonShadow(painter, allocation, state);
  drawButtonShine(painter, allocation, state);
  drawButtonFrame(painter, allocation, state);
}

int DarkTheme::buttonBorderWidth()
{
  return 2;
}

int DarkTheme::windowButtonHeight()
{
  return 22;
}

Rectangle<vec2> DarkTheme::recommendedFillRectangleForButtons(const Rectangle<vec2>& allocation)
{
  return allocation.expandedRect(-buttonBorderWidth());
}

bool DarkTheme::isPointWithinButton(const vec2& point, const Rectangle<vec2>& allocation)
{
  return RoundedRectangle(allocation, smallRoundingRadius).contains(point);
}

bool DarkTheme::isPointWithinWindowFrame(const vec2& point, const Rectangle<vec2>& allocation)
{
  return isPointWithinButton(point, allocation);
}


} // namespace Themes
} // namespace Gui
} // namespace Framework
