#include "palette-based-theme.h"

#include <base/color-tools.h>
#include <base/tango-colors.h>

namespace Framework {
namespace Gui {
namespace Themes {


PaletteBasedTheme::PaletteBasedTheme()
{
}


// ====

PaletteBasedTheme::ColorPalette::Gradient premultiply(PaletteBasedTheme::ColorPalette::Gradient gradient)
{
  for(int i=0; i<gradient.nPositions(); ++i)
  {
    gradient.colorsA[i] = premultiply(gradient.colorsA[i]);
    gradient.colorsB[i] = premultiply(gradient.colorsB[i]);
  }
  return gradient;
}

PaletteBasedTheme::ColorPalette::Gradient unpremultiply(PaletteBasedTheme::ColorPalette::Gradient gradient)
{
  for(int i=0; i<gradient.nPositions(); ++i)
  {
    gradient.colorsA[i] = unpremultiply(gradient.colorsA[i]);
    gradient.colorsB[i] = unpremultiply(gradient.colorsB[i]);
  }
  return gradient;
}

PaletteBasedTheme::ColorPalette::Gradient& PaletteBasedTheme::ColorPalette::Gradient::operator*=(const vec4& factor)
{
  for(int i=0; i<nPositions(); ++i)
  {
    colorsA[i] *= factor;
    colorsB[i] *= factor;
  }
  return *this;
}

PaletteBasedTheme::ColorPalette::Gradient PaletteBasedTheme::ColorPalette::Gradient::operator*(const vec4& factor) const
{
  PaletteBasedTheme::ColorPalette::Gradient result = *this;

  return result *= factor;
}

PaletteBasedTheme::ColorPalette::Gradient& PaletteBasedTheme::ColorPalette::Gradient::operator*=(real factor)
{
  for(int i=0; i<nPositions(); ++i)
  {
    colorsA[i] *= factor;
    colorsB[i] *= factor;
  }
  return *this;
}

PaletteBasedTheme::ColorPalette::Gradient PaletteBasedTheme::ColorPalette::Gradient::operator*(real factor) const
{
  PaletteBasedTheme::ColorPalette::Gradient result = *this;

  return result *= factor;
}

PaletteBasedTheme::ColorPalette::Gradient& PaletteBasedTheme::ColorPalette::Gradient::operator/=(real factor)
{
  for(int i=0; i<nPositions(); ++i)
  {
    colorsA[i] /= factor;
    colorsB[i] /= factor;
  }
  return *this;
}

PaletteBasedTheme::ColorPalette::Gradient PaletteBasedTheme::ColorPalette::Gradient::operator/(real factor) const
{
  PaletteBasedTheme::ColorPalette::Gradient result = *this;

  return result /= factor;
}

PaletteBasedTheme::ColorPalette::Gradient& PaletteBasedTheme::ColorPalette::Gradient::operator+=(const Gradient& other)
{
  for(int i=0; i<nPositions(); ++i)
  {
    colorsA[i] += other.colorsA[i];
    colorsB[i] += other.colorsB[i];
  }
  this->copyOthersPositionsIfNecessary(other);
  return *this;
}

PaletteBasedTheme::ColorPalette::Gradient PaletteBasedTheme::ColorPalette::Gradient::operator+(const Gradient& other) const
{
  PaletteBasedTheme::ColorPalette::Gradient result = *this;

  return result += other;
}

PaletteBasedTheme::ColorPalette::Gradient& PaletteBasedTheme::ColorPalette::Gradient::operator-=(const Gradient& other)
{
  for(int i=0; i<nPositions(); ++i)
  {
    colorsA[i] -= other.colorsA[i];
    colorsB[i] -= other.colorsB[i];
  }
  this->copyOthersPositionsIfNecessary(other);
  return *this;
}

PaletteBasedTheme::ColorPalette::Gradient PaletteBasedTheme::ColorPalette::Gradient::operator-(const Gradient& other) const
{
  PaletteBasedTheme::ColorPalette::Gradient result = *this;

  return result -= other;
}

PaletteBasedTheme::ColorPalette::Gradient::Gradient(const vec4& value)
{
  for(int i=0; i<nPositions(); ++i)
  {
    positionA[i] = 0.f;
    positionB[i] = 0.f;
  }

  fill(value);
}

void PaletteBasedTheme::ColorPalette::Gradient::fill(const vec4& value)
{
  for(int i=0; i<nPositions(); ++i)
  {
    colorsA[i] = value;
    colorsB[i] = value;
  }
}

void PaletteBasedTheme::ColorPalette::Gradient::copyOthersPositionsIfNecessary(const Gradient& other)
{
  if(this->uninitialiedPositions())
  {
    for(int i=0; i<nPositions(); ++i)
    {
      positionA[i] = other.positionA[i];
      positionB[i] = other.positionB[i];
    }
  }
}


void PaletteBasedTheme::ColorPalette::init(const vec4& backgroundColor, const vec4& textColor)
{
  Gradient baseShineGradient;
  baseShineGradient.colorsA[0] = vec4(vec3(1),  94.f/255.f);
  baseShineGradient.colorsB[0] = vec4(vec3(1),  31.f/255.f);
  baseShineGradient.colorsA[1] = vec4(vec3(1),  31.f/255.f);
  baseShineGradient.colorsB[1] = vec4(vec3(1),   0.f/255.f);
  baseShineGradient.colorsA[2] = vec4(vec3(0),   0.f/255.f);
  baseShineGradient.colorsB[2] = vec4(vec3(0), 161.f/255.f);
  baseShineGradient.positionA[0] = 0.00f;
  baseShineGradient.positionB[0] = 0.38f;
  baseShineGradient.positionA[1] = 0.38f;
  baseShineGradient.positionB[1] = 0.45f;
  baseShineGradient.positionA[2] = 0.66f;
  baseShineGradient.positionB[2] = 1.00f;

  this->background = backgroundColor;

  this->buttonShadow = vec4(vec3(0), 24.f/255.f);
  this->buttonShadow_Insensitive = buttonShadow * vec4(1, 1, 1, 0.5f);
  this->buttonShadow_Pressed = vec4(vec3(0), 32.f/255.f);
  this->buttonOuterBorder = vec4(vec3(0), 64.f/255.f);
  this->buttonOuterBorder_Insensitive = buttonOuterBorder * vec4(1, 1, 1, 0.5f);
  this->buttonOuterBorder_Pressed = buttonOuterBorder;
  this->buttonInnerBorder = vec4(vec3(1), 16.f/255.f);
  this->buttonInnerBorder_Insensitive = vec4(vec3(1), 8.f/255.f);
  this->buttonInnerBorder_Pressed = vec4(vec3(0), 32.f/255.f);
  this->buttonInnerBorder_KeyFocus = vec4(1, 0.451f, 0.008f, 72.f/255.f);

  this->defaultButtonColor = vec4(106.f/255.f, 57.f/255.f, 0.f/255.f, 53.f/255.f);

  this->invisibleGradient = baseShineGradient;
  this->invisibleGradient.fill(vec4(0));

  this->buttonShine_Default = baseShineGradient * vec4(vec3(1), 0.1f);
  this->buttonShine_Insensitive = buttonShine_Default * vec4(vec3(1), 0.5f);
  this->buttonShine_Hover = baseShineGradient * vec4(vec3(1), 0.16f);
  this->buttonShine_Pressed = baseShineGradient * vec4(vec3(0), 0.2f);

  this->buttonHighlight_Hover = vec4(vec3(1), 10.f/255.f);
  this->buttonHighlight_Pressed = vec4(82, 39, 0, 35) / 255.f;
  this->buttonHighlight_ActivatedHover = vec4(149, 70, 0, 35) / 255.f;
  this->buttonHighlight_KeyFocusHover = vec4(255, 152, 58, 17) / 255.f;
/*
  Gradient interpolatedGradient = interpolate<real, Gradient>(0.f, invisibleGradient, buttonShine_Default);
  assert(interpolatedGradient.colorsA[0] == vec4(0));
  interpolatedGradient = interpolate<real, Gradient>(1.f, invisibleGradient, buttonShine_Default);
  assert(interpolatedGradient.colorsA[0] == buttonShine_Default.colorsA[0]);
*/
  assert(buttonShine_Default.colorsA[0] == (buttonShine_Default* 0.25f +  buttonShine_Default*0.75f).colorsA[0]);
  assert(buttonShine_Default.colorsA[0] == interpolateWithFallback(buttonShine_Default).colorsA[0]);

  this->embossLight = vec4(vec3(1), 0.25f);
  this->embossDark = vec4(vec3(0), 0.25f);

  this->textColor = textColor;
}

void PaletteBasedTheme::ColorPalette::init(const vec4& backgroundColor)
{
  vec4 textColor = vec4(vec3(1), 0.8);

  if(brightness(backgroundColor) > 0.6)
    textColor = vec4(vec3(0), 0.8);

  init(backgroundColor, textColor);
}

void PaletteBasedTheme::ColorPalette::initDark()
{
  init(vec4(Tango::GreyDark_Dark.rgb()*1.1f, 1));
}


// ====


PaletteBasedTheme::VerySimpleTextWidget::VerySimpleTextWidget(PaletteBasedTheme& paletteBasedTheme,
                                                              const Font& font, bool prependLineSpacing)
  : prependLineSpacing(prependLineSpacing),
    font(font),
    paletteBasedTheme(paletteBasedTheme)
{
}


PaletteBasedTheme::VerySimpleTextWidget::Ptr PaletteBasedTheme::VerySimpleTextWidget::create(PaletteBasedTheme& paletteBasedTheme, const Font& font, bool prependLineSpacing)
{
  return Ptr(new VerySimpleTextWidget(paletteBasedTheme, font, prependLineSpacing));
}

PaletteBasedTheme::VerySimpleTextWidget::Ptr PaletteBasedTheme::VerySimpleTextWidget::create(PaletteBasedTheme* paletteBasedTheme, const Font& font, bool prependLineSpacing)
{
  return create(*paletteBasedTheme, font, prependLineSpacing);
}


vec2 PaletteBasedTheme::VerySimpleTextWidget::minimumSize(const String& text) const
{
  vec2 additionalSpacing(2);

  if(prependLineSpacing)
    additionalSpacing.y += font.lineSpacing();

  return font.textSize(text) + additionalSpacing;
}

vec2 PaletteBasedTheme::VerySimpleTextWidget::optimalSize(const String& text) const
{
  return minimumSize(text);
}

void PaletteBasedTheme::VerySimpleTextWidget::draw(const String& text,
                                                   Painter& painter,
                                                   const RenderData& renderData)
{
  real zPosition = renderData.zPosition;
  const Rectangle<vec2>& allocation = renderData.allocation;
  const State& state = renderData.state;

  vec4 colorFactor = vec4(vec3(1), renderData.state.alpha);

  vec4 color = paletteBasedTheme.colors.textColor;
  vec2 additionalSpacing(2);

  painter.setZPosition(zPosition);

  if(prependLineSpacing)
    additionalSpacing.y += font.lineSpacing();

  if(state.sensitive != 1.f)
  {
    painter.drawText(text, font, allocation.min()+additionalSpacing+vec2(1.f-state.sensitive), paletteBasedTheme.colors.embossLight * colorFactor);
    color = paletteBasedTheme.colors.embossDark;
  }
  painter.drawText(text, font, allocation.min()+additionalSpacing, color * colorFactor);

}


} // namespace Themes
} // namespace Gui
} // namespace Framework
