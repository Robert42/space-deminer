#include "container.h"
#include "window.h"
#include "containers/dummy-container.h"
#include "layouts/border.h"
#include "layouts/whole-area.h"
#include "layouts/ordered-layout.h"
#include "widgets/dummy-widget.h"

#include <framework/developer-tools/terminal.h>
#include <base/permutations.h>
#include <base/types/class-type.h>

namespace Framework {
namespace Gui {

#define VERBOSE(text) //std::cout << text << std::endl;

namespace Private {
class DummyLayout final : public Layouts::OrderedLayout
{
public:
  typedef std::shared_ptr<DummyLayout> Ptr;
  typedef std::weak_ptr<DummyLayout> WeakPtr;

private:
  real _testZOffset;

public:
  DummyLayout(real testZOffset = 0.f);

  static Ptr create(real testZOffset = 0.f);

  void updateMinimumSize() override;
  void updateOptimalSize() override;
  void updateChildAllocation() override;
  void updateChildZPosition() override;

  void onApplyMetricsFromTheme(const Theme::Ptr& theme) override;

  void setLayoutTestZOffset(real testZOffset);
  real testZOffset() const;

  Optional<Item::Ptr> navigateToNextItem(const Item::Ptr&, Navigation) final override {return nothing;}
  Optional<Item::Ptr> navigateToFirstItem(Navigation) final override {return nothing;}

private:
  QPair<vec2, vec2> update(const std::function<void(const Layout::Item::Ptr&, float, const vec2&)>& itemFunction);
  QPair<vec2, vec2> update();
};

RTTI_CLASS_DECLARE(Framework::Gui::Private::DummyLayout)

DummyLayout::DummyLayout(real testZOffset)
  : _testZOffset(testZOffset)
{
  RTTI_CLASS_TRY_REGISTER_TYPE_WITH_BASE(Framework::Gui::Private::DummyLayout, Layouts::OrderedLayout);
  RTTI_CLASS_SET_TYPE(Framework::Gui::Private::DummyLayout);
}

DummyLayout::Ptr DummyLayout::create(real testZOffset)
{
  return Ptr(new DummyLayout(testZOffset));
}

void DummyLayout::updateMinimumSize()
{
  setMinimumSize(update().first);
}

void DummyLayout::updateOptimalSize()
{
  setOptimalSize(update().second);
}

void DummyLayout::updateChildAllocation()
{
  update([this](const Layout::Item::Ptr& item, float y, const vec2& minSize){
    setItemAllocation(item, Rectangle<vec2>(vec2(0), minSize)+vec2(0,y)+this->allocation().min());
  });
}

void DummyLayout::updateChildZPosition()
{
  update([this](const Layout::Item::Ptr& item, float, const vec2&){
    setItemZPosition(item, orderedItems().indexOf(item) * testZOffset() + this->zPosition());
  });
}

void DummyLayout::onApplyMetricsFromTheme(const Theme::Ptr&)
{
}

void DummyLayout::setLayoutTestZOffset(real testZOffset)
{
  this->_testZOffset = testZOffset;

  markChildZPositionDirty();
}

real DummyLayout::testZOffset() const
{
  return _testZOffset;
}

QPair<vec2, vec2> DummyLayout::update(const std::function<void(const Item::Ptr&, float, const vec2&)>& itemFunction)
{
  vec2 minimumSize(0);
  vec2 optimalSize(0);
  real& y=minimumSize.y;
  real& optY=optimalSize.y;

  for(const Layout::Item::Ptr& item : this->orderedItems())
  {
    vec2 minSize = item->minimumSize();
    vec2 optSize = item->optimalSize();

    itemFunction(item, y, minSize);

    minimumSize.x = max(minimumSize.x, minSize.x);
    optimalSize.x = max(optimalSize.x, optSize.x);

    y += minSize.y;
    optY += optSize.y;
  }

  return QPair<vec2, vec2>(minimumSize, optimalSize);
}

QPair<vec2, vec2> DummyLayout::update()
{
  return update([](const Item::Ptr&, float, const vec2&){});
}

} // namespace Private

using Private::DummyLayout;

class ContainerTestPermutations
{
public:
  class SinglePermutation
  {
  public:
    typedef std::function<void(SinglePermutation*)> SingleStep;

  private:
    struct Frame
    {
    private:
      Window::Ptr _window;
      Containers::DummyContainer::Ptr _container;
      Containers::DummyContainer::Ptr _rootContainer;
      DummyLayout::Ptr _layout;
      Layouts::Border::Ptr _rootContainerLayout;
      Widgets::DummyWidget::Ptr _widgets[4];

    public:
      const Window::Ptr& window()
      {
        if(!_window)
          _window = Window::Ptr(new Window);
        return _window;
      }

      const Containers::DummyContainer::Ptr& container()
      {
        if(!_container)
          _container = Containers::DummyContainer::create();
        return _container;
      }

      const Containers::DummyContainer::Ptr& rootContainer()
      {
        if(!_rootContainer)
          _rootContainer = Containers::DummyContainer::create();
        return _rootContainer;
      }

      const DummyLayout::Ptr& layout()
      {
        if(!_layout)
          _layout = DummyLayout::Ptr(new DummyLayout);
        return _layout;
      }

      const Layouts::Border::Ptr& rootContainerLayout()
      {
        if(!_rootContainerLayout)
          _rootContainerLayout = Layouts::Border::create(12.f);
        return _rootContainerLayout;
      }

      const Widgets::DummyWidget::Ptr& widget(int i)
      {
        if(!_widgets[i])
          _widgets[i] = Widgets::DummyWidget::create();
        return _widgets[i];
      }

      void addWidgetsToLayout()
      {
        VERBOSE("addWidgetsToLayout()");
        layout()->appendWidget(widget(0));
        layout()->appendWidget(widget(1));
        layout()->appendWidget(widget(2));
      }

      void stepAddOrRemoveFourthWidgetToOrFromLayout()
      {
        VERBOSE("stepAddOrRemoveFourthWidgetToOrFromLayout()");
        if(_widgets[3])
        {
          if(_widgets[3]->layout())
            (*_widgets[3]->layout())->removeChildWidget(_widgets[3]);
          _widgets[3].reset();
        }else
        {
          layout()->appendWidget(widget(3));
        }
      }

      void addLayoutToContainer()
      {
        VERBOSE("addLayoutToContainer()");
        container()->addLayout(layout());
      }

      void addContainerToRootContainerLayout()
      {
        VERBOSE("addContainerToRootContainerLayout()");
        rootContainerLayout()->addWidget(container());
      }

      void addLayoutToRootContainerLayout()
      {
        VERBOSE("addLayoutToRootContainerLayout()");
        rootContainerLayout()->addLayout(layout());
      }

      void addRootContainerToWindow()
      {
        VERBOSE("addRootContainerToWindow()");
        window()->setRootWidget(rootContainer());
      }

      void addRootContainerLayoutToRootContainer()
      {
        VERBOSE("addRootContainerLayoutToRootContainer()");
        rootContainer()->addLayout(rootContainerLayout());
      }

      void setLayoutTestZOffset(real z)
      {
        VERBOSE("setLayoutTestZOffset()");
        layout()->setLayoutTestZOffset(z);
      }

      void takeWidgetsFrom(Frame& other)
      {
        VERBOSE("takeWidgetsFrom()");
        for(int i=0; i<3; ++i)
        {
          this->_widgets[i] = other._widgets[i];
          other._widgets[i].reset();

          if(this->widget(i)->parent())
          {
            Container* p = *this->widget(i)->parent();
            ASSERT_TRUE(p->removeChildWidget(this->widget(i)));
          }

          ASSERT_FALSE(this->widget(i)->parent());
          ASSERT_FALSE(this->widget(i)->window());
        }

      }

      void takeLayoutFrom(Frame& other)
      {
        VERBOSE("takeLayoutFrom()");

        this->_layout = other._layout;
        other._layout.reset();

        if(this->_layout->container())
        {
          Container* c =* this->_layout->container();
          c->removeChildLayout(this->_layout);
        }
        ASSERT_FALSE(this->_layout->container());

        for(int i=0; i<3; ++i)
        {
          this->_widgets[i] = other._widgets[i];
          other._widgets[i].reset();

          if(this->widget(i))
          {
            ASSERT_FALSE(this->widget(i)->parent());
            ASSERT_FALSE(this->widget(i)->window());
          }
        }
      }

      void takeContainerFrom(Frame& other)
      {
        VERBOSE("takeContainerFrom()");

        this->_container = other._container;
        other._container.reset();

        if(this->container()->parent())
        {
          Container* p = *this->container()->parent();
          ASSERT_TRUE(p->removeChildWidget(this->container()));
        }

        ASSERT_FALSE(this->container()->parent());
        ASSERT_FALSE(this->container()->window());

        this->_layout = other._layout;
        other._layout.reset();

        for(int i=0; i<3; ++i)
        {
          this->_widgets[i] = other._widgets[i];
          other._widgets[i].reset();

          if(this->widget(i))
          {
            ASSERT_EQ(this->_container.get(), *this->widget(i)->parent());
            ASSERT_FALSE(this->widget(i)->window());
          }
        }
      }

      void takeRootContainerFrom(Frame& other)
      {
        VERBOSE("takeRootContainerFrom()");

        this->_rootContainer = other._rootContainer;
        other._rootContainer.reset();

        if(this->_rootContainer->window())
        {
          Window* w = *this->_rootContainer->window();
          w->unsetRootWidget();
        }

        ASSERT_FALSE(this->rootContainer()->parent());
        ASSERT_FALSE(this->rootContainer()->window());

        this->_rootContainerLayout = other._rootContainerLayout;
        other._rootContainerLayout.reset();

        this->_container = other._container;
        other._container.reset();

        this->_layout = other._layout;
        other._layout.reset();

        for(int i=0; i<3; ++i)
        {
          this->_widgets[i] = other._widgets[i];
          other._widgets[i].reset();

          if(this->widget(i))
          {
            ASSERT_EQ(this->_container.get(), *this->widget(i)->parent());
            ASSERT_FALSE(this->widget(i)->window());
          }
        }
      }

      void takeRootLayoutFrom(Frame& other)
      {
        VERBOSE("takeRootLayoutFrom()");

        ASSERT_FALSE(this->rootContainer()->parent());
        ASSERT_FALSE(this->rootContainer()->window());

        this->_rootContainerLayout = other._rootContainerLayout;
        other._rootContainerLayout.reset();

        if(this->_rootContainerLayout->container())
        {
          Container* c = *this->_rootContainerLayout->container();
          c->removeChildLayout(_rootContainerLayout);
        }

        this->_container = other._container;
        other._container.reset();

        this->_layout = other._layout;
        other._layout.reset();

        for(int i=0; i<3; ++i)
        {
          this->_widgets[i] = other._widgets[i];
          other._widgets[i].reset();

          if(this->widget(i))
          {
            if(this->_container)
              ASSERT_EQ(this->_container.get(), *this->widget(i)->parent());
            ASSERT_FALSE(this->widget(i)->window());
          }
        }
      }

      void resetWidgets()
      {
        VERBOSE("resetWidgets()");

        for(int i=0; i<3; ++i)
          this->_widgets[i].reset();
      }

      void resetLayout()
      {
        VERBOSE("resetLayout()");

        this->_layout.reset();
      }

      void resetRootContainerLayout()
      {
        VERBOSE("resetRootContainerLayout()");

        this->_rootContainerLayout.reset();
      }

      void checkResult1();
      void checkResult2();
    };
  public:
    Frame frame[2];

    QList<SingleStep> steps;

  public:
    SinglePermutation(const QList<SingleStep>& steps)
      : steps(steps)
    {
    }

    void runSteps()
    {
      VERBOSE("========");
      for(const SingleStep& step : steps)
      {
        step(this);
      }
    }

    void checkResult(int resultId)
    {
      switch(resultId)
      {
      case 1:
        frame[0].checkResult1();
        break;
      case 2:
        frame[0].checkResult1();
        frame[1].checkResult1();
        break;
      case 3:
        frame[0].checkResult2();
        break;
      case 4:
        frame[0].checkResult2();
        frame[1].checkResult2();
        break;
      default:
        GTEST_FAIL();
      }
    }
  };

  typedef SinglePermutation::SingleStep SingleStep;

private:
  QList< QList<SingleStep> > allPermutations;
  int maxNumPermutations;

public:
  ContainerTestPermutations(int maxNumPermutations=31)
    : maxNumPermutations(fastTest() ? maxNumPermutations : std::numeric_limits<int>::max())
  {
  }

  void addSteps(const QList<QList<SingleStep>>& newPermutations)
  {
    QList< QList<SingleStep> > concatenatedPermutations;

    if(allPermutations.empty())
    {
      concatenatedPermutations = newPermutations;
    }else
    {
      for(const QList<SingleStep>& _p : allPermutations)
      {
        for(const QList<SingleStep>& newPermutation : newPermutations)
        {
          QList<SingleStep> p = _p;
          for(const SingleStep& step : newPermutation)
            p.append(step);
          concatenatedPermutations.append(p);
        }
      }
    }

    this->allPermutations = concatenatedPermutations;
  }

  void addPermutations(const QList<SingleStep>& steps)
  {
    addSteps(permutate(steps));
  }

  void simpleSteps()
  {
    addPermutations({stepAddWidgetsToLayout(),
                     stepAddLayoutToContainer(),
                     stepAddContainerToRootContainerLayout(),
                     stepAddRootContainerToWindow(),
                     stepAddRootContainerLayoutToRootContainer()});
    addPermutations({stepSetLayoutTestZOffset()});
  }

  void addAndRemoveFourthWidget()
  {
    addPermutations({stepAddWidgetsToLayout(),
                     stepAddOrRemoveFourthWidgetToOrFromLayout(),
                     stepAddOrRemoveFourthWidgetToOrFromLayout(),
                     stepAddLayoutToContainer(),
                     stepAddContainerToRootContainerLayout(),
                     stepAddRootContainerToWindow(),
                     stepAddRootContainerLayoutToRootContainer()});
    addPermutations({stepSetLayoutTestZOffset()});
  }

  void layoutHierarchy()
  {
    addPermutations({stepAddWidgetsToLayout(),
                     stepAddLayoutToRootContainerLayout(),
                     stepAddRootContainerToWindow(),
                     stepAddRootContainerLayoutToRootContainer()});
  }

  void removeAndAddWidget()
  {
    addSteps({{stepAddWidgetsToLayout(0),
               stepAddLayoutToContainer(0),
               stepAddContainerToRootContainerLayout(0),
               stepAddRootContainerToWindow(0),
               stepAddRootContainerLayoutToRootContainer(0),
               stepSetLayoutTestZOffset(0)}});
    addSteps({{stepTakeWidgetsFromFrame0ToFrame1(),
               stepAddWidgetsToLayout(0)}});
    addPermutations({stepAddWidgetsToLayout(1),
                     stepAddLayoutToContainer(1),
                     stepAddContainerToRootContainerLayout(1),
                     stepAddRootContainerToWindow(1),
                     stepAddRootContainerLayoutToRootContainer(1)});
    addSteps({{stepSetLayoutTestZOffset(1)}});
  }

  void removeAndAddLayout()
  {
    addSteps({{stepAddWidgetsToLayout(0),
               stepAddLayoutToContainer(0),
               stepAddContainerToRootContainerLayout(0),
               stepAddRootContainerToWindow(0),
               stepAddRootContainerLayoutToRootContainer(0),
               stepSetLayoutTestZOffset(0),
               stepTakeLayoutFromFrame0ToFrame1(),
               stepAddLayoutToContainer(0),
               stepSetLayoutTestZOffset(0),
               stepAddWidgetsToLayout(0)}});
    addPermutations({stepAddLayoutToContainer(1),
                     stepAddContainerToRootContainerLayout(1),
                     stepAddRootContainerToWindow(1),
                     stepAddRootContainerLayoutToRootContainer(1)});
  }

  void removeAndAddContainer()
  {
    addSteps({{stepAddWidgetsToLayout(0),
               stepAddLayoutToContainer(0),
               stepAddContainerToRootContainerLayout(0),
               stepAddRootContainerToWindow(0),
               stepAddRootContainerLayoutToRootContainer(0),
               stepSetLayoutTestZOffset(0),
               stepTakeContainerFromFrame0ToFrame1(),
               stepAddWidgetsToLayout(0),
               stepAddLayoutToContainer(0),
               stepAddContainerToRootContainerLayout(0),
               stepSetLayoutTestZOffset(0)}});
    addPermutations({stepAddContainerToRootContainerLayout(1),
                     stepAddRootContainerToWindow(1),
                     stepAddRootContainerLayoutToRootContainer(1)});
  }

  void removeAndAddRootLayout()
  {
    addSteps({{stepAddWidgetsToLayout(0),
               stepAddLayoutToContainer(0),
               stepAddContainerToRootContainerLayout(0),
               stepAddRootContainerToWindow(0),
               stepAddRootContainerLayoutToRootContainer(0),
               stepSetLayoutTestZOffset(0),
               stepTakeRootLayoutFromFrame0ToFrame1(),
               stepAddWidgetsToLayout(0),
               stepAddLayoutToContainer(0),
               stepAddContainerToRootContainerLayout(0),
               stepAddRootContainerLayoutToRootContainer(0),
               stepSetLayoutTestZOffset(0)}});
    addPermutations({stepAddRootContainerToWindow(1),
                     stepAddRootContainerLayoutToRootContainer(1)});
  }

  void removeAndAddRootContainer()
  {
    addSteps({{stepAddWidgetsToLayout(0),
               stepAddLayoutToContainer(0),
               stepAddContainerToRootContainerLayout(0),
               stepAddRootContainerToWindow(0),
               stepAddRootContainerLayoutToRootContainer(0),
               stepSetLayoutTestZOffset(0),
               stepTakeRootContainerFromFrame0ToFrame1(),
               stepAddWidgetsToLayout(0),
               stepAddLayoutToContainer(0),
               stepAddContainerToRootContainerLayout(0),
               stepAddRootContainerToWindow(0),
               stepAddRootContainerLayoutToRootContainer(0),
               stepSetLayoutTestZOffset(0)}});
    addPermutations({stepAddRootContainerToWindow(1)});
  }

  void replaceRootLayout()
  {
    addSteps({{stepAddWidgetsToLayout(0),
               stepAddLayoutToRootContainerLayout(0),
               stepAddRootContainerToWindow(0),
               stepAddRootContainerLayoutToRootContainer(0),
               resetWidgets(),
               resetLayout(),
               resetRootContainerLayout()}});
    addPermutations({stepAddWidgetsToLayout(0),
                     stepAddLayoutToRootContainerLayout(0),
                     stepAddRootContainerLayoutToRootContainer(0)});
  }

  void removeAndAddRootLayoutWithSubLayout()
  {
    addSteps({{stepAddWidgetsToLayout(0),
               stepAddLayoutToRootContainerLayout(0),
               stepAddRootContainerToWindow(0),
               stepAddRootContainerLayoutToRootContainer(0),
               stepTakeRootLayoutFromFrame0ToFrame1(),
               stepAddWidgetsToLayout(0),
               stepAddLayoutToRootContainerLayout(0),
               stepAddRootContainerLayoutToRootContainer(0)}});
    addPermutations({stepAddRootContainerLayoutToRootContainer(1),
                     stepAddRootContainerToWindow(1)});
  }

  void removeAndAddASubLayout()
  {
    addSteps({{stepAddWidgetsToLayout(0),
               stepAddLayoutToRootContainerLayout(0),
               stepAddRootContainerToWindow(0),
               stepAddRootContainerLayoutToRootContainer(0),
               stepTakeLayoutFromFrame0ToFrame1(),
               stepAddWidgetsToLayout(0),
               stepAddLayoutToRootContainerLayout(0)}});
    addPermutations({stepAddRootContainerLayoutToRootContainer(1),
                     stepAddRootContainerToWindow(1),
                     stepAddLayoutToRootContainerLayout(1)});
  }

  void runSteps(int resultId)
  {
    QList<QList<SingleStep>> permutationsToRun;

    if(allPermutations.size() > maxNumPermutations)
    {
      permutationsToRun.reserve(maxNumPermutations+1);
      int d = allPermutations.size() / maxNumPermutations;

      assert(d > 0);

      for(int i=0; i<allPermutations.size(); i+=d)
      {
        permutationsToRun.append(allPermutations[i]);
      }
    }else
    {
      permutationsToRun = allPermutations;
    }

    for(const QList<SingleStep>& steps : permutationsToRun)
    {
      SinglePermutation permutation(steps);

      permutation.runSteps();
      permutation.checkResult(resultId);
    }
  }

  static SingleStep stepAddWidgetsToLayout(int i=0)
  {
    return [i](SinglePermutation* p){p->frame[i].addWidgetsToLayout();};
  }

  static SingleStep stepAddOrRemoveFourthWidgetToOrFromLayout(int i=0)
  {
    return [i](SinglePermutation* p){p->frame[i].stepAddOrRemoveFourthWidgetToOrFromLayout();};
  }

  static SingleStep stepAddLayoutToContainer(int i=0)
  {
    return [i](SinglePermutation* p){p->frame[i].addLayoutToContainer();};
  }

  static SingleStep stepAddContainerToRootContainerLayout(int i=0)
  {
    return [i](SinglePermutation* p){p->frame[i].addContainerToRootContainerLayout();};
  }

  static SingleStep stepAddLayoutToRootContainerLayout(int i=0)
  {
    return [i](SinglePermutation* p){p->frame[i].addLayoutToRootContainerLayout();};
  }

  static SingleStep stepAddRootContainerToWindow(int i=0)
  {
    return [i](SinglePermutation* p){p->frame[i].addRootContainerToWindow();};
  }

  static SingleStep stepAddRootContainerLayoutToRootContainer(int i=0)
  {
    return [i](SinglePermutation* p){p->frame[i].addRootContainerLayoutToRootContainer();};
  }

  static SingleStep stepSetLayoutTestZOffset(int i=0, real z = 10.f)
  {
    return [i,z](SinglePermutation* p){p->frame[i].setLayoutTestZOffset(z);};
  }

  static SingleStep stepTakeWidgetsFromFrame0ToFrame1()
  {
    return [](SinglePermutation* p){p->frame[1].takeWidgetsFrom(p->frame[0]);};
  }

  static SingleStep stepTakeLayoutFromFrame0ToFrame1()
  {
    return [](SinglePermutation* p){p->frame[1].takeLayoutFrom(p->frame[0]);};
  }

  static SingleStep stepTakeContainerFromFrame0ToFrame1()
  {
    return [](SinglePermutation* p){p->frame[1].takeContainerFrom(p->frame[0]);};
  }

  static SingleStep stepTakeRootLayoutFromFrame0ToFrame1()
  {
    return [](SinglePermutation* p){p->frame[1].takeRootLayoutFrom(p->frame[0]);};
  }

  static SingleStep stepTakeRootContainerFromFrame0ToFrame1()
  {
    return [](SinglePermutation* p){p->frame[1].takeRootContainerFrom(p->frame[0]);};
  }

  static SingleStep resetWidgets(int i=0)
  {
    return [i](SinglePermutation* p){p->frame[i].resetWidgets();};
  }

  static SingleStep resetLayout(int i=0)
  {
    return [i](SinglePermutation* p){p->frame[i].resetLayout();};
  }

  static SingleStep resetRootContainerLayout(int i=0)
  {
    return [i](SinglePermutation* p){p->frame[i].resetRootContainerLayout();};
  }
};


TEST(framework_gui_Container, simple)
{
  ContainerTestPermutations permutations;

  permutations.simpleSteps();
  permutations.runSteps(1);
}


TEST(framework_gui_Container, addAndRemoveFourthWidget)
{
  ContainerTestPermutations permutations;

  permutations.addAndRemoveFourthWidget();
  permutations.runSteps(1);
}


TEST(framework_gui_Container, layoutHierarchy)
{
  ContainerTestPermutations permutations;

  permutations.layoutHierarchy();
  permutations.runSteps(3);
}


TEST(framework_gui_Container, removeAndAddWidget)
{
  ContainerTestPermutations permutations;

  permutations.removeAndAddWidget();
  permutations.runSteps(2);
}


TEST(framework_gui_Container, removeAndAddLayout)
{
  ContainerTestPermutations permutations;

  permutations.removeAndAddLayout();
  permutations.runSteps(2);
}


TEST(framework_gui_Container, removeAndAddContainer)
{
  ContainerTestPermutations permutations;

  permutations.removeAndAddContainer();
  permutations.runSteps(2);
}


TEST(framework_gui_Container, removeAndAddRootLayout)
{
  ContainerTestPermutations permutations;

  permutations.removeAndAddRootLayout();
  permutations.runSteps(2);
}



TEST(framework_gui_Container, removeAndAddRootContainer)
{
  ContainerTestPermutations permutations;

  permutations.removeAndAddRootContainer();
  permutations.runSteps(2);
}


TEST(framework_gui_Container, replaceRootLayout)
{
  ContainerTestPermutations permutations;

  permutations.replaceRootLayout();
  permutations.runSteps(3);
}


TEST(framework_gui_Container, removeAndAddRootLayoutWithSubLayout)
{
  ContainerTestPermutations permutations;

  permutations.removeAndAddRootLayoutWithSubLayout();
  permutations.runSteps(4);
}


TEST(framework_gui_Container, removeAndAddASubLayout)
{
  ContainerTestPermutations permutations;

  permutations.removeAndAddASubLayout();
  permutations.runSteps(4);
}


void ContainerTestPermutations::SinglePermutation::Frame::checkResult1()
{
  window()->updateAllWidgets();

  vec2 minimumSize = widget(0)->minimumSize();
  vec2 optimalSize = widget(0)->optimalSize();

  EXPECT_EQ(rootContainer(), window()->rootWidget());

  ASSERT_TRUE(rootContainer()->window());
  EXPECT_EQ(window().get(), *rootContainer()->window());
  EXPECT_FALSE(rootContainer()->parent());
  EXPECT_EQ(rootContainerLayout(), rootContainer()->rootLayout());
  EXPECT_EQ(1, rootContainer()->allChildren().size());
  EXPECT_TRUE(rootContainer()->allChildren().contains(container()));
  EXPECT_EQ(minimumSize*vec2(1,3) + vec2(24), rootContainer()->minimumSize());
  EXPECT_EQ(optimalSize*vec2(1,3) + vec2(24), rootContainer()->optimalSize());
  EXPECT_EQ(0.f, rootContainer()->zPosition());
  EXPECT_EQ(0, rootContainer()->index().zOrder());
  EXPECT_EQ(0xffff, rootContainer()->index().layoutOrder());

  ASSERT_TRUE(container()->window());
  ASSERT_TRUE(container()->parent());
  EXPECT_EQ(window().get(), *container()->window());
  EXPECT_EQ(rootContainer().get(), *container()->parent());
  EXPECT_EQ(layout(), container()->rootLayout());
  EXPECT_EQ(3, container()->allChildren().size());
  EXPECT_TRUE(container()->allChildren().contains(widget(0)));
  EXPECT_TRUE(container()->allChildren().contains(widget(1)));
  EXPECT_TRUE(container()->allChildren().contains(widget(2)));
  EXPECT_EQ(minimumSize*vec2(1,3), container()->minimumSize());
  EXPECT_EQ(optimalSize*vec2(1,3), container()->optimalSize());
  EXPECT_EQ(0.f, container()->zPosition());
  EXPECT_EQ(1, container()->index().zOrder());
  EXPECT_EQ(0, container()->index().layoutOrder());

  real y = 0.f;
  for(int i=0; i<3; ++i)
  {
    Widget::Ptr w = widget(i);

    ASSERT_TRUE(w->window());
    ASSERT_TRUE(w->parent());
    EXPECT_EQ(window().get(), *w->window());
    EXPECT_EQ(container().get(), *w->parent());
    EXPECT_EQ(minimumSize, w->minimumSize());
    EXPECT_EQ(optimalSize, w->optimalSize());
    EXPECT_EQ(Rectangle<vec2>(vec2(0.f), w->minimumSize())+vec2(0,y)+vec2(12), w->boundingRect());
    EXPECT_EQ(i*10.f, w->zPosition());
    EXPECT_EQ(2, w->index().zOrder());
    EXPECT_EQ(i, w->index().layoutOrder());

    y += w->minimumSize().y;
  }

  EXPECT_EQ(5, window()->widgetModel()->allWidgets().size());
  EXPECT_TRUE(window()->widgetModel()->allWidgets().contains(widget(0)));
  EXPECT_TRUE(window()->widgetModel()->allWidgets().contains(widget(1)));
  EXPECT_TRUE(window()->widgetModel()->allWidgets().contains(widget(2)));
  EXPECT_TRUE(window()->widgetModel()->allWidgets().contains(container()));
  EXPECT_TRUE(window()->widgetModel()->allWidgets().contains(rootContainer()));
}

void ContainerTestPermutations::SinglePermutation::Frame::checkResult2()
{
  window()->updateAllWidgets();

  vec2 minimumSize = widget(0)->minimumSize();
  vec2 optimalSize = widget(0)->optimalSize();

  EXPECT_EQ(rootContainer(), window()->rootWidget());

  ASSERT_TRUE(rootContainer()->window());
  EXPECT_EQ(window().get(), *rootContainer()->window());
  EXPECT_FALSE(rootContainer()->parent());
  EXPECT_EQ(rootContainerLayout(), rootContainer()->rootLayout());
  EXPECT_EQ(3, rootContainer()->allChildren().size());
  EXPECT_TRUE(rootContainer()->allChildren().contains(widget(0)));
  EXPECT_TRUE(rootContainer()->allChildren().contains(widget(1)));
  EXPECT_TRUE(rootContainer()->allChildren().contains(widget(2)));
  EXPECT_EQ(minimumSize*vec2(1,3) + vec2(24), rootContainer()->minimumSize());
  EXPECT_EQ(optimalSize*vec2(1,3) + vec2(24), rootContainer()->optimalSize());
  EXPECT_EQ(0.f, rootContainer()->zPosition());
  EXPECT_EQ(0, rootContainer()->index().zOrder());
  EXPECT_EQ(0xffff, rootContainer()->index().layoutOrder());

  real y = 0.f;
  for(int i=0; i<3; ++i)
  {
    Widget::Ptr w = widget(i);

    ASSERT_TRUE(w->window());
    ASSERT_TRUE(w->parent());
    EXPECT_EQ(window().get(), *w->window());
    EXPECT_EQ(rootContainer().get(), *w->parent());
    EXPECT_EQ(minimumSize, w->minimumSize());
    EXPECT_EQ(optimalSize, w->optimalSize());
    EXPECT_EQ(Rectangle<vec2>(vec2(0.f), w->minimumSize())+vec2(0,y)+vec2(12), w->boundingRect());
    EXPECT_EQ(0.f, w->zPosition());
    EXPECT_EQ(1, w->index().zOrder());
    EXPECT_EQ(i, w->index().layoutOrder());

    y += w->minimumSize().y;
  }

  EXPECT_EQ(4, window()->widgetModel()->allWidgets().size());
  EXPECT_TRUE(window()->widgetModel()->allWidgets().contains(widget(0)));
  EXPECT_TRUE(window()->widgetModel()->allWidgets().contains(widget(1)));
  EXPECT_TRUE(window()->widgetModel()->allWidgets().contains(widget(2)));
  EXPECT_TRUE(window()->widgetModel()->allWidgets().contains(rootContainer()));
}


class ContainerA : public Containers::DummyContainer
{
public:
  int onLayoutMinimumSizeChangedWasCalledInA;
  Layouts::WholeArea::Ptr l;

  ContainerA()
  {
    onLayoutMinimumSizeChangedWasCalledInA = 0;

    setRootLayout(l = Layouts::WholeArea::create());
  }

  void onLayoutMinimumSizeChanged() override
  {
    Containers::DummyContainer::onLayoutMinimumSizeChanged();
    ++onLayoutMinimumSizeChangedWasCalledInA;
  }

  void updateMinimumSize()
  {
    Containers::DummyContainer::updateMinimumSize();
  }
};

class ContainerB : public ContainerA
{
public:
  int onLayoutMinimumSizeChangedWasCalledInB;

  ContainerB()
  {
    onLayoutMinimumSizeChangedWasCalledInB = 0;
  }

  void onLayoutMinimumSizeChanged() override
  {
    ContainerA::onLayoutMinimumSizeChanged();
    ++onLayoutMinimumSizeChangedWasCalledInB;
  }
};

TEST(framework_gui_Container, onLayoutMinimumSizeChanged)
{
  ContainerB b;

  EXPECT_EQ(0, b.onLayoutMinimumSizeChangedWasCalledInA);
  EXPECT_EQ(0, b.onLayoutMinimumSizeChangedWasCalledInB);

  b.l->addWidget(Widgets::DummyWidget::create());
  b.updateMinimumSize();

  EXPECT_EQ(1, b.onLayoutMinimumSizeChangedWasCalledInA);
  EXPECT_EQ(1, b.onLayoutMinimumSizeChangedWasCalledInB);
}

TEST(framework_gui_Container, testRemovingFromModel)
{
  Window window;
  Containers::DummyContainer::Ptr a = Containers::DummyContainer::create();
  Containers::DummyContainer::Ptr b = Containers::DummyContainer::create();
  Containers::DummyContainer::Ptr c = Containers::DummyContainer::create();

  Layouts::WholeArea::Ptr la = Layouts::WholeArea::create();
  Layouts::WholeArea::Ptr lb = Layouts::WholeArea::create();

  a->addLayout(la);
  b->addLayout(lb);

  la->addWidget(b);

  EXPECT_FALSE(window.widgetModel()->allWidgets().contains(a));
  EXPECT_FALSE(window.widgetModel()->allWidgets().contains(b));
  EXPECT_FALSE(window.widgetModel()->allWidgets().contains(c));

  window.setRootWidget(a);

  EXPECT_TRUE(window.widgetModel()->allWidgets().contains(a));
  EXPECT_TRUE(window.widgetModel()->allWidgets().contains(b));
  EXPECT_FALSE(window.widgetModel()->allWidgets().contains(c));

  lb->addWidget(c);

  EXPECT_TRUE(window.widgetModel()->allWidgets().contains(a));
  EXPECT_TRUE(window.widgetModel()->allWidgets().contains(b));
  EXPECT_TRUE(window.widgetModel()->allWidgets().contains(c));

  la->remove();

  EXPECT_TRUE(window.widgetModel()->allWidgets().contains(a));
  EXPECT_FALSE(window.widgetModel()->allWidgets().contains(b));
  EXPECT_FALSE(window.widgetModel()->allWidgets().contains(c));

  la->addWidget(b);

  EXPECT_TRUE(window.widgetModel()->allWidgets().contains(a));
  EXPECT_TRUE(window.widgetModel()->allWidgets().contains(b));
  EXPECT_TRUE(window.widgetModel()->allWidgets().contains(c));

  window.setRootWidget(Containers::DummyContainer::create());

  EXPECT_FALSE(window.widgetModel()->allWidgets().contains(a));
  EXPECT_FALSE(window.widgetModel()->allWidgets().contains(b));
  EXPECT_FALSE(window.widgetModel()->allWidgets().contains(c));
}

} // namespace Gui
} // namespace Framework
