#include "container.h"
#include "layout.h"

#include "layouts/whole-area.h"

namespace Framework {
namespace Gui {


Container::Container()
  : Container(Layouts::WholeArea::create())
{
}

Container::Container(const Layout::Ptr& rootLayout)
{
  setRootLayout(rootLayout);
}

Container::~Container()
{
}

const Layout::Ptr& Container::rootLayout()
{
  return _rootLayout;
}

QSet<Widget::Ptr> Container::allChildren()
{
  return rootLayout()->allChildWidgets();
}

void Container::markChildLayoutIndexDirty()
{
  this->sheduleReorderRenderOrder();
  this->_markDirty(DirtyFlags::CHILD_LAYOUT_INDEX | DirtyFlags::SUBTREE_INDEX);
  this->_markParentsDirty(DirtyFlags::SUBTREE_INDEX);
}

void Container::markChildScissorDirty()
{
  this->_markChildrenDirty(DirtyFlags::SCISSOR);
}

Optional<Rectangle<vec2> > Container::finalScissorForChildren() const
{
  if(this->scissor() && this->scissorForChildren())
    return this->scissorForChildren()->intersection(*this->scissor());
  else if(this->scissor())
    return this->scissor();
  else
    return this->scissorForChildren();
}

const Optional< Rectangle<vec2> >& Container::scissorForChildren() const
{
  return _scissorForChildren;
}

void Container::setScissorForChildren(const Optional< Rectangle<vec2> >& scissorForChildren)
{
  if(this->scissorForChildren() != scissorForChildren)
  {
    this->_scissorForChildren = scissorForChildren;
    markChildScissorDirty();
    sheduleRedrawRecursive();
  }
}

void Container::sheduleRedrawRecursive()
{
  if(!this->visible())
    return;

  this->sheduleRedraw();

  for(const Widget::Ptr& w : this->allChildren())
    w->sheduleRedrawRecursive();
}

bool Container::visitWholeHierarchy(const std::function<bool(const Widget::Ptr& widget)>& visitor,
                                    int minDepth,
                                    int maxDepth)
{
  if(Widget::visitWholeHierarchy(visitor, minDepth, maxDepth))
    return true;

  minDepth = max(minDepth-1, 0);
  maxDepth--;

  if(maxDepth < 0)
    return false;

  for(const Widget::Ptr& w : this->allChildren())
    if(w->visitWholeHierarchy(visitor, minDepth, maxDepth))
      return true;

  return false;
}

bool Container::removeChildWidget(const Widget::Ptr& widget)
{
  return removeChildWidget(widget.get());
}

bool Container::removeChildWidget(const Widget* widget)
{
  return rootLayout()->removeChildWidget(widget);
}

bool Container::removeChildLayout(const LayoutPtr& layout)
{
  return removeChildLayout(layout.get());
}

bool Container::removeChildLayout(const Layout* layout)
{
  if(rootLayout().get() == layout)
  {
    unsetRootLayout();
    return true;
  }else
  {
    return rootLayout()->removeChildLayout(layout);
  }
}


void Container::setRootLayout(const Layout::Ptr& layout)
{
  if(_rootLayout == layout)
    return;

  assert(!layout->parentLayout());
  assert(!layout->container());

  if(_rootLayout)
    _rootLayout->setContainer(nothing);

  trackLayout.clear();
  _rootLayout = layout;

  _rootLayout->signalMinimumSizeChanged().connect(std::bind(&Container::onLayoutMinimumSizeChanged, this)).track(trackLayout);
  _rootLayout->signalOptimalSizeChanged().connect(std::bind(&Container::onLayoutOptimalSizeChanged, this)).track(trackLayout);

  rootLayout()->setAllocation(this->boundingRect());
  rootLayout()->setZPosition(this->zPosition());
  rootLayout()->setContainer(this);
}

void Container::unsetRootLayout()
{
  setRootLayout(Layouts::WholeArea::create());
}

void Container::onMoved()
{
  rootLayout()->setAllocation(this->boundingRect());

  Widget::onMoved();
}

void Container::onMovedAlongZ()
{
  rootLayout()->setZPosition(this->zPosition());

  Widget::onMovedAlongZ();
}

void Container::onAddedToWindow()
{
  rootLayout()->onWindowChanged(window());
  Widget::onAddedToWindow();
}

void Container::onRemovedFromWindow()
{
  rootLayout()->onWindowChanged(nothing);
  Widget::onRemovedFromWindow();
}

void Container::updateIndices(uint16 zOrder)
{
  Widget::updateIndices(zOrder);

  if(this->dirtyFlagsChecked(DirtyFlags::CHILD_LAYOUT_INDEX))
  {
    uint16 layoutIndex = 0;
    this->rootLayout()->updateIndices(inout(layoutIndex));
  }

  if(this->dirtyFlagsChecked(DirtyFlags::SUBTREE_INDEX))
  {
    zOrder++;

    for(const Widget::Ptr& widget : allChildren())
      widget->updateIndices(zOrder);
  }
}

void Container::onThemeChanged()
{
  Widget::onThemeChanged();

  rootLayout()->onThemeChanged();

  for(const Widget::Ptr& w : this->allChildren())
    w->markThemeDirty();
}

void Container::onUpdateMinimumSize()
{
  Widget::onUpdateMinimumSize();

  rootLayout()->_updateMinimumSize();
}

void Container::onUpdateOptimalSize()
{
  Widget::onUpdateOptimalSize();

  rootLayout()->_updateOptimalSize();
}

void Container::onUpdateChildAllocation()
{
  Widget::onUpdateChildAllocation();

  rootLayout()->_updateChildAllocation();
}

void Container::onUpdateChildZPosition()
{
  Widget::onUpdateChildZPosition();

  rootLayout()->_updateChildZPosition();
}

void Container::onChildAdded()
{
  markChildLayoutIndexDirty();
}

void Container::onChildAboutToBeRemoved()
{
  markChildLayoutIndexDirty();
}

void Container::onLayoutMinimumSizeChanged()
{
  this->setMinimumSize(this->rootLayout()->minimumSize());
}

void Container::onLayoutOptimalSizeChanged()
{
  this->setOptimalSize(this->rootLayout()->optimalSize());
}

void Container::_markChildrenDirty(DirtyFlags dirtyFlags)
{
  if(!this->visible())
    return;

  for(const Widget::Ptr& w : this->allChildren())
  {
    if(!w->dirtyFlagsChecked(dirtyFlags))
    {
      w->_markDirty(dirtyFlags);
      w->_markChildrenDirty(dirtyFlags);
    }
  }
}


} // namespace Gui
} // namespace Framework
