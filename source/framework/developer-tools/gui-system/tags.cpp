#include "tags.h"

#include <base/types/type-manager.h>

namespace Framework {
namespace Gui {
namespace Tags {


Tag windowRoot;
Tag window_defaultWidget;
Tag window_defaultEscapeWidget;

Tag windowFrame_mainArea;
Tag windowFrame_windowButtonArea;

namespace Canvas
{

Tag textSize;
Tag smallView;
Tag largeEditor;

} // namespace Canvas

namespace Animations
{

Tag succeeded;
Tag aborted;

Tag show;
Tag hide;

Tag suspended;
Tag continued;

Tag key;

Tag leftMouseButton;
Tag middleMouseButton;
Tag rightMouseButton;

Tag startClick;
Tag endClick;
Tag startClickOffset;
Tag endClickOffset;

Tag mouseEnter;
Tag mouseLeave;
Tag keyFocusEnter;
Tag keyFocusLeave;

} // namespace Animations
} // namespace Tags


void initTags()
{
  Tags::windowRoot = TagIds::tagFromString("Framework::Gui::Window::rootWidget()");
  Tags::window_defaultWidget = TagIds::tagFromString("Framework::Gui::Window::defaultWidget()");
  Tags::window_defaultEscapeWidget = TagIds::tagFromString("Framework::Gui::Window::defaultEscapeWidget()");

  Tags::windowFrame_mainArea = TagIds::tagFromString("Framework::Gui::WindowFrame::mainArea()");
  Tags::windowFrame_windowButtonArea = TagIds::tagFromString("Framework::Gui::WindowFrame::windowButtonArea()");

  Tags::Canvas::textSize = TagIds::tagFromString("Tags::Canvas::textSize");
  Tags::Canvas::smallView = TagIds::tagFromString("Tags::Canvas::smallView");
  Tags::Canvas::largeEditor = TagIds::tagFromString("Tags::Canvas::largeEditor");

  Tags::Animations::succeeded = TagIds::tagFromString("Tags::Animations::succeeded");
  Tags::Animations::aborted = TagIds::tagFromString("Tags::Animations::aborted");
  Tags::Animations::show = TagIds::tagFromString("Tags::Animations::show");
  Tags::Animations::hide = TagIds::tagFromString("Tags::Animations::hide");
  Tags::Animations::suspended = TagIds::tagFromString("Tags::Animations::suspended");
  Tags::Animations::continued = TagIds::tagFromString("Tags::Animations::continued");
  Tags::Animations::key = TagIds::tagFromString("Tags::Animations::key");
  Tags::Animations::leftMouseButton = TagIds::tagFromString("Tags::Animations::leftMouseButton");
  Tags::Animations::middleMouseButton = TagIds::tagFromString("Tags::Animations::middleMouseButton");
  Tags::Animations::rightMouseButton = TagIds::tagFromString("Tags::Animations::rightMouseButton");
  Tags::Animations::startClick = TagIds::tagFromString("Tags::Animations::startClick");
  Tags::Animations::endClick = TagIds::tagFromString("Tags::Animations::endClick");
  Tags::Animations::startClickOffset = TagIds::tagFromString("Tags::Animations::startClickOffset");
  Tags::Animations::endClickOffset = TagIds::tagFromString("Tags::Animations::endClickOffset");

  Tags::Animations::mouseEnter = TagIds::tagFromString("Tags::Animations::mouseEnter");
  Tags::Animations::mouseLeave = TagIds::tagFromString("Tags::Animations::mouseLeave");
  Tags::Animations::keyFocusEnter = TagIds::tagFromString("Tags::Animations::keyFocusEnter");
  Tags::Animations::keyFocusLeave = TagIds::tagFromString("Tags::Animations::keyFocusLeave");
}


} // namespace Gui
} // namespace Framework
