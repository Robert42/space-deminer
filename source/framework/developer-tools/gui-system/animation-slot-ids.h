#ifndef FRAMEWORK_GUI_ANIMATIONS_H
#define FRAMEWORK_GUI_ANIMATIONS_H

#include "animation.h"

namespace Framework {
namespace Gui {
namespace AnimationSlots {
namespace RenderState {


extern Animation::SlotId alpha;
extern Animation::SlotId sensitive;
extern Animation::SlotId activated;
extern Animation::SlotId keyFocused;
extern Animation::SlotId mouseHover;
extern Animation::SlotId mousePressed;

extern Animation::SlotId xAllocationOffset;
extern Animation::SlotId yAllocationOffset;
extern Animation::SlotId zPositionOffset;


} // namespace RenderState

extern Animation::SlotId suppressMouseInput;

} // namespace AnimationSlots

void initAnimationSlots();

} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_ANIMATIONS_H
