#ifndef FRAMEWORK_GUI_FONTMANAGER_H
#define FRAMEWORK_GUI_FONTMANAGER_H

#include "font.h"

namespace Framework {
namespace Gui {

class FontManager
{
public:
  typedef std::shared_ptr<FontManager> Ptr;

public:
  FontManager();

  virtual Font defaultFont() = 0;

  static Ptr createFallbackTextureManager();
};

} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_FONTMANAGER_H
