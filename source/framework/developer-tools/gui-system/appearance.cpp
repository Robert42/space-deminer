#include "appearance.h"

namespace Framework {
namespace Gui {


Appearance::Appearance()
{
}

Appearance::~Appearance()
{
}

Appearance::Ptr Appearance::fallbackAppearance()
{
  class DummyAppearance : public Appearance
  {
  public:
    Renderer::Ptr renderer() override
    {
      return Renderer::fallbackRenderer();
    }

    Theme::Ptr rootTheme(const TagSet&) override
    {
      return Theme::fallbackTheme();
    }
  };

  static Ptr dummyAppearance = Ptr(new DummyAppearance);

  return dummyAppearance;
}


Signals::Signal<void()>& Appearance::signalRootThemeChanged()
{
  return _signalRootThemeChanged;
}


} // namespace Gui
} // namespace Framework
