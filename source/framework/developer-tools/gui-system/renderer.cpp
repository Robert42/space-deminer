#include "renderer.h"

namespace Framework {
namespace Gui {

Renderer::Renderer()
{
}

Renderer::Ptr Renderer::fallbackRenderer()
{
  class DummyRenderer : public Renderer
  {
  public:
    void beginRedrawing() override {}
    void endRedrawing() override {}
    void sheduleRedraw() override {}

    void setAntialiasingHint(AntialiasingHint) override {}

    void setWindowTransformation(const mat4&) override {}
    void setZPosition(real) override {}
    void drawFilledTriangle(const vec4&, const vec2&, const vec2&, const vec2&) override {}
    void drawFilledRectangle(const Rectangle<vec2>&, const vec4&) override {}
    void drawFilledRectangle(const Rectangle<vec2>&, const vec4&, const vec4&) override {}
    void drawRoundedRectangle(const RoundedRectangle&, const vec4&, real, real) override {}
    void drawText(const String&, const Font&, const vec2&, const vec4&) override {}
    void drawQuadStrip(const QVector<vec2>&, const QVector<vec2>&, const vec4&, bool) override {}

    LinePen startDrawingLine() override {return LinePen();}

    void setScissorRect(const Optional< Rectangle<vec2> >&) override {}
  };

  static Ptr dummyRenderer = Ptr(new DummyRenderer);

  return dummyRenderer;
}

} // namespace Gui
} // namespace Framework
