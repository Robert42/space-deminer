#include "layout.h"
#include "tags.h"
#include "window.h"

namespace Framework {
namespace Gui {


Layout::Layout()
  : _minimumSize(0),
    _optimalSize(0),
    _z(0.f)
{
  RTTI_CLASS_SET_TYPE(Framework::Gui::Layout);
}

Layout::~Layout()
{
  removeAllItems();
}

const Optional<Container*>& Layout::container() const
{
  return _container;
}

Optional<Container*> Layout::container()
{
  return _container;
}

void Layout::setContainer(Container* container)
{
  if(container != nullptr)
    setContainer(Optional<Container*>(container));
  else
    unsetContainer();
}

void Layout::unsetContainer()
{
  setContainer(nothing);
}

void Layout::setContainer(const Optional<Container*>& container)
{
  if(this->container() != container)
  {
    this->_container = container;

    for(const Item::Ptr& item : items())
    {
      item->onContainerChanged(container);
    }

    markEverythingDirty();
  }
}

Optional<Window*> Layout::window() const
{
  if(!_container)
    return nothing;
  return (*_container)->window();
}

const vec2& Layout::minimumSize() const
{
  return _minimumSize;
}

const vec2& Layout::optimalSize() const
{
  return _optimalSize;
}


real Layout::zPosition()
{
  return this->_z;
}


const Rectangle<vec2>& Layout::allocation()
{
  return this->_allocation;
}

void Layout::_allChildWidgets(const InOutput<QSet<Widget::Ptr>>& allChildWidgets)
{
  for(const Item::Ptr& item : items())
    item->allChildWidgets(allChildWidgets);
}

QSet<Widget::Ptr> Layout::allChildWidgets()
{
  QSet<Widget::Ptr> widgets;
  _allChildWidgets(inout(widgets));
  return widgets;
}


bool Layout::shouldRound() const
{
  if(this->window())
  {
    Window* window = *this->window();

    return window->roundAllocationHint();
  }else
  {
    return true;
  }
}


void Layout::markEverythingDirty()
{
  if(container())
  {
    Container* c = *container();

    c->markThemeDirty();
    c->markMinimumSizeDirty();
    c->markOptimalSizeDirty();
    c->markChildLayoutIndexDirty();
    c->markChildAllocationDirty();
    c->markChildZPositionDirty();
  }
}

void Layout::markEverythingLocationRelatedDirty()
{
  if(container())
  {
    Container* c = *container();

    c->markMinimumSizeDirty();
    c->markOptimalSizeDirty();
    c->markChildAllocationDirty();
  }
}

void Layout::markThemeDirty()
{
  if(container())
  {
    Container* c = *container();

    c->markThemeDirty();
  }
}

void Layout::markMinimumSizeDirty()
{
  if(container())
  {
    Container* c = *container();

    c->markMinimumSizeDirty();
  }
}

void Layout::markOptimalSizeDirty()
{
  if(container())
  {
    Container* c = *container();

    c->markOptimalSizeDirty();
  }
}

void Layout::markParentMinimumSizeDirty()
{
  if(container())
  {
    Container* c = *container();

    c->markParentMinimumSizeDirty();
  }
}

void Layout::markParentOptimalSizeDirty()
{
  if(container())
  {
    Container* c = *container();

    c->markParentOptimalSizeDirty();
  }
}

void Layout::markChildAllocationDirty()
{
  if(container())
  {
    Container* c = *container();

    c->markChildAllocationDirty();
  }
}

void Layout::markChildZPositionDirty()
{
  if(container())
  {
    Container* c = *container();

    c->markChildZPositionDirty();
  }
}


bool Layout::removeChildWidget(const Widget::Ptr& widget, bool recursive)
{
  return removeChildWidget(widget.get(), recursive);
}

bool Layout::removeChildWidget(const Widget* widget, bool recursive)
{
  for(const Item::Ptr& item : items())
  {
    if(item->isWidgetItem())
    {
      Layout::WidgetItem::Ptr widgetItem = std::static_pointer_cast<WidgetItem>(item);

      if(widgetItem->widget.get() == widget)
      {
        removeItem(widgetItem);
        return true;
      }
    }
    if(recursive && item->isSubLayoutItem())
    {
      Layout::SubLayoutItem::Ptr layoutItem = std::static_pointer_cast<SubLayoutItem>(item);

      if(layoutItem->subLayout->removeChildWidget(widget, recursive))
        return true;
    }
  }

  return false;
}

bool Layout::removeChildLayout(const LayoutPtr& layout, bool recursive)
{
  return removeChildLayout(layout.get(), recursive);
}

bool Layout::removeChildLayout(const Layout* layout, bool recursive)
{
  for(const Item::Ptr& item : items())
  {
    if(item->isSubLayoutItem())
    {
      Layout::SubLayoutItem::Ptr layoutItem = std::static_pointer_cast<SubLayoutItem>(item);

      if(layoutItem->subLayout.get() == layout)
      {
        removeItem(layoutItem);
        return true;
      }

      if(recursive && layoutItem->subLayout->removeChildLayout(layout, recursive))
        return true;
    }
  }

  return false;
}


void Layout::_updateMinimumSize()
{
  for(const Item::Ptr& item : items())
  {
    if(item->isSubLayoutItem())
    {
      Layout::SubLayoutItem::Ptr layoutItem = std::static_pointer_cast<SubLayoutItem>(item);

      layoutItem->subLayout->_updateMinimumSize();
    }
  }

  updateMinimumSize();
}

void Layout::_updateOptimalSize()
{
  for(const Item::Ptr& item : items())
  {
    if(item->isSubLayoutItem())
    {
      Layout::SubLayoutItem::Ptr layoutItem = std::static_pointer_cast<SubLayoutItem>(item);

      layoutItem->subLayout->_updateOptimalSize();
    }
  }

  updateOptimalSize();
}

void Layout::_updateChildAllocation()
{
  updateChildAllocation();

  for(const Item::Ptr& item : items())
  {
    if(item->isSubLayoutItem())
    {
      Layout::SubLayoutItem::Ptr layoutItem = std::static_pointer_cast<SubLayoutItem>(item);

      layoutItem->subLayout->_updateChildAllocation();
    }
  }
}

void Layout::_updateChildZPosition()
{
  updateChildZPosition();

  for(const Item::Ptr& item : items())
  {
    if(item->isSubLayoutItem())
    {
      Layout::SubLayoutItem::Ptr layoutItem = std::static_pointer_cast<SubLayoutItem>(item);

      layoutItem->subLayout->_updateChildZPosition();
    }
  }
}

void Layout::updateChildZPosition()
{
  for(const Item::Ptr& item : items())
    setItemZPosition(item, this->zPosition());
}

Optional<Theme::Ptr> Layout::theme()
{
  Optional<Container*> _container = container();
  if(_container)
  {
    Container* container = *_container;
    return container->theme();
  }else
  {
    return nothing;
  }
}

void Layout::applyMetricsFromTheme()
{
  Optional<Theme::Ptr> _theme = this->theme();
  if(_theme)
  {
    Theme::Ptr theme = *_theme;
    onApplyMetricsFromTheme(theme);
  }
}

void Layout::onItemRemoved(const std::shared_ptr<Item>& item)
{
  (void)item;
}

void Layout::onWindowChanged(const Optional<Window*>& window)
{
  for(const Item::Ptr& item : items())
    item->onWindowChanged(window);
}

void Layout::onThemeChanged()
{
  applyMetricsFromTheme();

  for(const Item::Ptr& item : items())
    item->onThemeChanged();
}

void Layout::setAllocation(Rectangle<vec2> allocation)
{
  allocation = Rectangle<vec2>(allocation.min(),
                              allocation.min() + max(this->minimumSize(), allocation.size()));

  if(allocation != this->allocation())
  {
    this->_allocation = allocation;
    markChildAllocationDirty();
  }
}

void Layout::setZPosition(real z)
{
  if(this->_z != z)
  {
    this->_z = z;
    markChildZPositionDirty();
  }
}

void Layout::setMinimumSize(const vec2& minimumSize)
{
  if(this->_minimumSize != minimumSize)
  {
    _minimumSize = minimumSize;
    _signalMinimumSizeChanged();
  }
}

void Layout::setOptimalSize(const vec2& optimalSize)
{
  if(this->_optimalSize != optimalSize)
  {
    _optimalSize = optimalSize;
    _signalOptimalSizeChanged();
  }
}

void Layout::setLocalItemAllocation(const std::shared_ptr<Item>& item, const Rectangle<vec2>& allocation)
{
  setItemAllocation(item, allocation + this->allocation().min());
}

void Layout::setItemAllocation(const std::shared_ptr<Item>& item, const Rectangle<vec2>& allocation)
{
  assert(items().contains(item));
  item->setBoundingRectangle(allocation);
}

void Layout::setLocalItemZPosition(const std::shared_ptr<Item>& item, real z)
{
  setItemZPosition(item, z + this->zPosition());
}

void Layout::setItemZPosition(const std::shared_ptr<Item>& item, real z)
{
  assert(items().contains(item));
  item->setZPosition(z);
}

void Layout::setItemIndex(const std::shared_ptr<Item>& item, const InOutput<uint16>& index)
{
  item->setItemIndex(index);
}

void Layout::insertItem(const std::shared_ptr<Item>& item)
{
  assert(&item->parentLayout == this);

  _items.insert(item);

  item->onContainerChanged(container());

  if(this->container())
  {
    Container* c = *this->container();

    c->markThemeDirty();
    c->markChildLayoutIndexDirty();
    c->markChildZPositionDirty();
    c->markChildAllocationDirty();
    c->markMinimumSizeDirty();
    c->markOptimalSizeDirty();
  }
}

void Layout::removeItem(const std::shared_ptr<Item>& item)
{
  _removeItem(item);

  if(this->container())
  {
    Container* c = *this->container();

    c->markChildLayoutIndexDirty();
    c->markChildZPositionDirty();
    c->markChildAllocationDirty();
    c->markMinimumSizeDirty();
    c->markOptimalSizeDirty();
  }
}

void Layout::_removeItem(const std::shared_ptr<Item>& item)
{
  item->onContainerChanged(nothing);
  _items.remove(item);
  onItemRemoved(item);
}

Optional<Layout::WidgetItem::Ptr> Layout::findItemWithWidget(const std::shared_ptr<Widget>& widget)
{
  for(const Item::Ptr& i : this->items())
  {
    if(i->isWidgetItem())
    {
      Layout::WidgetItem::Ptr w =  std::static_pointer_cast<Layout::WidgetItem>(i);

      if(w->widget == widget)
        return w;
    }
  }

  return nothing;
}

Optional<Layout::SubLayoutItem::Ptr> Layout::findItemWithLayout(const std::shared_ptr<Layout>& layout)
{
  return findItemWithLayout(layout.get());
}

Optional<Layout::SubLayoutItem::Ptr> Layout::findItemWithLayout(const Layout* layout)
{
  for(const Item::Ptr& i : this->items())
  {
    if(i->isSubLayoutItem())
    {
      Layout::SubLayoutItem::Ptr l =  std::static_pointer_cast<Layout::SubLayoutItem>(i);

      if(l->subLayout.get() == layout)
        return l;
    }
  }

  return nothing;
}

Optional< Widget::Ptr > Layout::navigateToNextWidget(const Widget::Ptr& widget, Navigation navigation)
{
  Optional<Layout*> optionalParentLayout = widget->layout();

  if(!optionalParentLayout)
    return nothing;

  Optional< std::shared_ptr<WidgetItem> >  widgetItem = optionalParentLayout.value()->findItemWithWidget(widget);
  if(!widgetItem)
    return nothing;
  Item::Ptr item = *widgetItem;

  return navigateToNextWidget(item, navigation);
}

// ISSUE-197: this function only allows navigation within the layout tree of a single  container. It should be possible to navigation through the whole widget tree
Optional< Widget::Ptr > Layout::navigateToNextWidget(Item::Ptr item, Navigation navigation)
{
  Optional<Layout*> optionalParentLayout = &item->parentLayout;

  // Go through all items of the current layout
  while(optionalParentLayout)
  {
    Layout* parentLayout = *optionalParentLayout;

    Optional<Item::Ptr> optionalNextItem = parentLayout->navigateToNextItem(item, navigation);

    // If theere's a next item, check the new one, otherwise continue with the parent layer.
    if(optionalNextItem)
    {
      Item::Ptr nextItem = *optionalNextItem;

      // If the found layer is a sublayout, go deeper, until it hits a leave item
      while(nextItem->isSubLayoutItem())
      {
        SubLayoutItem::Ptr subLayoutItem = std::static_pointer_cast<SubLayoutItem>(nextItem);

        Optional<Item::Ptr> optionalSubItem = subLayoutItem->subLayout->navigateToFirstItem(navigation);

        if(optionalSubItem)
        {
          nextItem = *optionalSubItem;
          optionalParentLayout = subLayoutItem->subLayout.get();
        }else
        {
          // the current sublayout item is a leave item
          break;
        }
      }

      // If the leave item we've found is a widget, we've found a new widget
      if(nextItem->isWidgetItem())
      {
        WidgetItem::Ptr widgetItem = std::static_pointer_cast<WidgetItem>(nextItem);
        return widgetItem->widget;
      }
    }else
    {
      optionalParentLayout = parentLayout->parentLayout();

      if(optionalParentLayout)
      {
        // As optionalParentLayout is returned by parentLayout->parentLayout(), we know, that
        // optionalParentLayout.value()->findItemWithLayout returns something valid.
        item = *optionalParentLayout.value()->findItemWithLayout(parentLayout);
      }
    }
  }

  return nothing;
}

void Layout::removeAllItems()
{
  while(!this->items().empty())
  {
    int n = this->items().size();
    _removeItem(*this->items().begin());

    assert(n == this->items().size()+1);
  }
}

const QSet<std::shared_ptr<Layout::Item> >& Layout::items()
{
 return _items;
}

Signals::Signal<void()>& Layout::signalMinimumSizeChanged()
{
  return _signalMinimumSizeChanged;
}

Signals::Signal<void()>& Layout::signalOptimalSizeChanged()
{
  return _signalOptimalSizeChanged;
}

const Optional<Layout*>& Layout::parentLayout()
{
  return _parentLayout;
}

void Layout::setParentLayout(const Optional<Layout*>& layout)
{
  _parentLayout = layout;

  if(layout)
    this->setContainer((*layout)->container());
  else
    this->setContainer(nothing);
}

// --------


Layout::Item::Item(Layout& parentLayout, real expandPriority)
  : parentLayout(parentLayout),
    _expandPriority(expandPriority)
{
}

Layout::Item::~Item()
{
}

real Layout::Item::expandPriority() const
{
  return _expandPriority;
}

void Layout::Item::setExpandPriority(real expandPriority)
{
  expandPriority = max(0.f, expandPriority);

  if(expandPriority != this->_expandPriority)
  {
    this->_expandPriority = expandPriority;
    this->parentLayout.markChildAllocationDirty();
  }
}

bool Layout::Item::isWidgetItem() const
{
  return false;
}

bool Layout::Item::isSubLayoutItem() const
{
  return false;
}

void Layout::Item::minimumSizeChanged()
{
  const vec2 currentSize = this->currentBoundingRectangle().size();
  const vec2& minimumSize = this->minimumSize();

  this->parentLayout.markMinimumSizeDirty();

  if(minimumSize.x > currentSize.x ||
     minimumSize.y > currentSize.y)
  {
    this->parentLayout.markChildAllocationDirty();
  }
}

void Layout::Item::optimalSizeChanged()
{
  const vec2 currentSize = this->currentBoundingRectangle().size();
  const vec2& optimalSize = this->minimumSize();

  this->parentLayout.markOptimalSizeDirty();

  if(optimalSize.x > currentSize.x ||
     optimalSize.y > currentSize.y ||
     this->expandPriority() > 0.f)
  {
    this->parentLayout.markChildAllocationDirty();
  }
}


// --------


Layout::WidgetItem::WidgetItem(const Widget::Ptr& widget, Layout& parentLayout, real expandPriority)
  : Item(parentLayout, expandPriority),
    widget(widget)
{
  // The widget should not be already added to another layout
  assert(!widget->layout());
  // The widget should not be already added to another container
  assert(!widget->parent());

  widgetSlot.setWidget(widget, &parentLayout);

  widget->signalMinimumSizeChanged().connect(std::bind(&WidgetItem::minimumSizeChanged, this)).track(this->trackable);
  widget->signalOptimalSizeChanged().connect(std::bind(&WidgetItem::optimalSizeChanged, this)).track(this->trackable);
}

Layout::WidgetItem::~WidgetItem()
{
  widgetSlot.unsetWidget();
}

Layout::WidgetItem::Ptr Layout::WidgetItem::create(const Widget::Ptr& widget, Layout& layout, real expandPriority)
{
  return Ptr(new WidgetItem(widget, layout, expandPriority));
}

const vec2& Layout::WidgetItem::minimumSize() const
{
  return widget->minimumSize();
}

const vec2& Layout::WidgetItem::optimalSize() const
{
  return widget->optimalSize();
}

const Rectangle<vec2>& Layout::WidgetItem::currentBoundingRectangle() const
{
  return widget->boundingRect();
}

void Layout::WidgetItem::setBoundingRectangle(const Rectangle<vec2>& boundingRect)
{
  widgetSlot.setWidgetBoundingRect(boundingRect);
}

void Layout::WidgetItem::setZPosition(real z)
{
  widgetSlot.setWidgetZPosition(z);
}

void Layout::WidgetItem::setItemIndex(const InOutput<uint16>& index)
{
  assert(index.value < 0xffff); // There are more widgets than allowed as a child for a container
  widgetSlot.setWindowLayoutIndex(index.value++);
}

void Layout::WidgetItem::onContainerChanged(const Optional<Container*>& container)
{
  widgetSlot.setContainer(container);
}

void Layout::WidgetItem::onWindowChanged(const Optional<Window*>& window)
{
  widgetSlot.setWindow(window);
}

void Layout::WidgetItem::onThemeChanged()
{
}

void Layout::WidgetItem::allChildWidgets(const InOutput<QSet<Widget::Ptr>>& allChildWidgets)
{
  allChildWidgets.value.insert(widget);
}

bool Layout::WidgetItem::isWidgetItem() const
{
  return true;
}


// --------


Layout::SpacingItem::SpacingItem(const vec2& minimumSize, const vec2& optimalSize, Layout& parentLayout, real expandPriority)
  : Item(parentLayout, expandPriority),
    _minimumSize(minimumSize),
    _optimalSize(optimalSize),
    _customSpacing(true)
{
}

Layout::SpacingItem::Ptr Layout::SpacingItem::create(const vec2& minimumSize, const vec2& optimalSize, Layout& layout, real expandPriority)
{
  return Ptr(new SpacingItem(minimumSize, optimalSize, layout, expandPriority));
}


Layout::SpacingItem::SpacingItem(Layout& parentLayout, real expandPriority)
  : SpacingItem(vec2(0), vec2(0), parentLayout, expandPriority)
{
  setThemeSize();
}

Layout::SpacingItem::Ptr Layout::SpacingItem::create(Layout& layout, real expandPriority)
{
  return Ptr(new SpacingItem(layout, expandPriority));
}

Layout::SpacingItem::~SpacingItem()
{
}

bool Layout::SpacingItem::customSpacing() const
{
  return _customSpacing;
}

const vec2& Layout::SpacingItem::minimumSize() const
{
  return _minimumSize;
}

const vec2& Layout::SpacingItem::optimalSize() const
{
  return _optimalSize;
}

void Layout::SpacingItem::setMinimumSize(const vec2& minimumSize)
{
  _customSpacing = true;
  _setMinimumSize(minimumSize);
}

void Layout::SpacingItem::setOptimalSize(const vec2& optimalSize)
{
  _customSpacing = true;
  _setOptimalSize(optimalSize);
}

void Layout::SpacingItem::setThemeSize()
{
  if(customSpacing() == true)
  {
    _customSpacing = false;
    onThemeChanged();
  }
}

void Layout::SpacingItem::_setMinimumSize(const vec2& minimumSize)
{
  if(this->_minimumSize != minimumSize)
  {
    _minimumSize = minimumSize;
    minimumSizeChanged();
  }
}

void Layout::SpacingItem::_setOptimalSize(const vec2& optimalSize)
{
  if(this->_optimalSize != optimalSize)
  {
    _optimalSize = optimalSize;
    optimalSizeChanged();
  }
}

const Rectangle<vec2>& Layout::SpacingItem::currentBoundingRectangle() const
{
  return _currentBoundingRectangle;
}

void Layout::SpacingItem::setBoundingRectangle(const Rectangle<vec2>& boundingRect)
{
  this->_currentBoundingRectangle = boundingRect;
}

void Layout::SpacingItem::setZPosition(real z)
{
  (void)z;
}

void Layout::SpacingItem::setItemIndex(const InOutput<uint16>& index)
{
  (void)index;
}

void Layout::SpacingItem::onContainerChanged(const Optional<Container*>&)
{
}

void Layout::SpacingItem::onWindowChanged(const Optional<Window*>&)
{
}

void Layout::SpacingItem::onThemeChanged()
{
  if(customSpacing())
    return;

  Optional<Theme::Ptr> _theme = parentLayout.theme();
  if(!_theme)
    return;
  Theme::Ptr theme = *_theme;

  const Metrics& metrics = *theme->layoutSpacingItemMetricsEnforced(&this->parentLayout);

  _setMinimumSize(metrics.minimumSizeHint);
  _setOptimalSize(metrics.optimalSizeHint);
}

void Layout::SpacingItem::allChildWidgets(const InOutput<QSet<Widget::Ptr>>&)
{
}

// --------


Layout::SubLayoutItem::SubLayoutItem(const Layout::Ptr& subLayout, Layout& parentLayout, real expandPriority)
  : Item(parentLayout, expandPriority),
    subLayout(subLayout)
{
  assert(subLayout.get() != &parentLayout);
  // The sublayout shouldn't be already a sublayout of another Layout
  assert(!subLayout->parentLayout());
  // The sublayout shouldn't be already the layout of a container
  assert(!subLayout->container());
  subLayout->setParentLayout(&parentLayout);

  subLayout->signalMinimumSizeChanged().connect(std::bind(&SubLayoutItem::minimumSizeChanged, this)).track(this->trackable);
  subLayout->signalOptimalSizeChanged().connect(std::bind(&SubLayoutItem::optimalSizeChanged, this)).track(this->trackable);
}

Layout::SubLayoutItem::~SubLayoutItem()
{
  subLayout->setParentLayout(nothing);
}

Layout::SubLayoutItem::Ptr Layout::SubLayoutItem::create(const Layout::Ptr& subLayout, Layout& layout, real expandPriority)
{
  return Ptr(new SubLayoutItem(subLayout, layout, expandPriority));
}

const vec2& Layout::SubLayoutItem::minimumSize() const
{
  return subLayout->minimumSize();
}

const vec2& Layout::SubLayoutItem::optimalSize() const
{
  return subLayout->optimalSize();
}

const Rectangle<vec2>& Layout::SubLayoutItem::currentBoundingRectangle() const
{
  return subLayout->allocation();
}

void Layout::SubLayoutItem::setBoundingRectangle(const Rectangle<vec2>& boundingRect)
{
  subLayout->setAllocation(boundingRect);
}

void Layout::SubLayoutItem::setZPosition(real z)
{
  subLayout->setZPosition(z);
}

void Layout::SubLayoutItem::setItemIndex(const InOutput<uint16>& index)
{
  subLayout->updateIndices(index);
}

bool Layout::SubLayoutItem::isSubLayoutItem() const
{
  return true;
}

void Layout::SubLayoutItem::onContainerChanged(const Optional<Container*>& container)
{
  subLayout->setContainer(container);
}

void Layout::SubLayoutItem::onWindowChanged(const Optional<Window*>& window)
{
  subLayout->onWindowChanged(window);
}

void Layout::SubLayoutItem::onThemeChanged()
{
  subLayout->onThemeChanged();
}

void Layout::SubLayoutItem::allChildWidgets(const InOutput<QSet<Widget::Ptr>>& allChildWidgets)
{
  subLayout->_allChildWidgets(allChildWidgets);
}


} // namespace Gui
} // namespace Framework
