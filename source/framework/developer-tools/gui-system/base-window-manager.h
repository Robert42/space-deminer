#ifndef FRAMEWORK_GUI_BASEWINDOWMANAGER_H
#define FRAMEWORK_GUI_BASEWINDOWMANAGER_H

#include "window.h"
#include "base-window-container.h"

#include "input-handlers/mouse-focus-input-handler.h"
#include "input-handlers/key-focus-input-handler.h"

namespace Framework {
namespace Gui {

class BaseWindowManager : public noncopyable
{
public:
  typedef std::shared_ptr<BaseWindowManager> Ptr;

private:
  class ConnectionSet
  {
  private:
    QSet<Signals::Connection> connections;

  public:
    void disconnectAll();
    void insert(const Signals::Connection& connection);
  };

  QHash<Window::Ptr, ConnectionSet> _connectionsToWindow;

private:
  Appearance::Ptr _appearance;

  bool _dirtyWindowOrder : 1;

  Signals::CallableSignal<void()> _signalAppereanceChanged;
  QVector<BaseWindowContainer::Ptr> _cachedWindowOrderToHandleUserInput;

  Signals::CallableSignal<void()> _signalInvalidatedMouseOverWidget;
  Signals::CallableSignal<void()> _signalInvalidatedKeyFocusedWidget;
  Signals::CallableSignal<void(const InputHandlers::MouseAction::Ptr&, BaseWindowContainer&)> _signalMouseActionStarted;
  Signals::CallableSignal<void(const Widget::Ptr&, Window& window)> _signalAskForKeyFocus;

protected:
  BaseWindowManager(const Appearance::Ptr& appearance);

public:
  virtual ~BaseWindowManager();

public:
  virtual void redraw();

  const Appearance::Ptr& appearance();

  void addMainWindow(const Window::Ptr& window);
  void addDialog(const Window::Ptr& window);
  void closeWindow(const Window::Ptr& window);

  Signals::Signal<void()>& signalAppereanceChanged();

  virtual InputHandler::Ptr createInputHandlerForWindow(BaseWindowContainer& windowContainer) = 0;

  const QVector<BaseWindowContainer::Ptr>& windowOrderToHandleUserInput();
  void invalidateWindowOrderToHandleUserInput();

  Optional<BaseWindowContainer::Ptr> windowContainerForWindow(const Window::Ptr& window);

  Signals::Signal<void()>& signalInvalidatedMouseOverWidget();
  Signals::Signal<void()>& signalInvalidatedKeyFocusedWidget();
  void invalidateMouseOverWidget();
  void invalidateKeyFocusedWidget();

  Signals::Signal<void(const InputHandlers::MouseAction::Ptr&, BaseWindowContainer&)>& signalMouseActionStarted();

  Signals::Signal<void(const Widget::Ptr&, Window& window)>& signalAskForKeyFocus();

protected:
  void _setAppereance(const Appearance::Ptr& appereance);

  /** Creates a mouse focus input handler distributing events between the windows of this window
   * manager, where always the window, with the mouse over it will get the events.
   *
   * @note Note that this is only usefull, if you gather the input events for the whole window manager at
   * once (and the windows manager has to decide all by itself , which window will handle input).
   *
   * @return Implementation of InputHandlers::MouseFocusInputHandler
   */
  InputHandlers::MouseFocusInputHandler::Ptr createMouseFocusInputHandler();

  /** Creates a mouse focus input handler distributing events between the widgets of the given
   * window, where always the widget, with the mouse over it will get the events.
   *
   * @note Note that this is only usefull, if you gather the input events for the whole window manager at
   * once (and the windows manager has to decide all by itself , which window will handle input).
   *
   * @return Implementation of InputHandlers::MouseFocusInputHandler
   */
  InputHandlers::MouseFocusInputHandler::Ptr createMouseFocusInputHandlerForWindow(BaseWindowContainer& windowContainer);

  /** Creates an input handler ensuring, that if an mouse-action is running, the window with this
   * mouse-action will get the input-handlers.
   *
   * @note Note that this is only usefull, if you gather the input events for the whole window manager at
   * once (and the windows manager has to decide all by itself , which window will handle input).
   *
   * @return Implementation of InputHandlers.
   */
  InputHandler::Ptr createMouseActionFocusWindowEnforcmenet();

  /** Creates a default button input handler forwarding certain keys to the default widgets
   *
   * @return The input-handler.
   */
  InputHandler::Ptr createDefaultButtonInputHandler(BaseWindowContainer& windowContainer,
                                                    const QSet<KeyCode>& defaultWidgetKeys = {KeyCode::RETURN, KeyCode::NUMPAD_ENTER, KeyCode::SPACE},
                                                    const QSet<KeyCode>& defaultEscapeWidgetKeys = {KeyCode::ESCAPE});

  /** Creates a key focus input handler distributing events between the windows of this window
   * manager, where always the window, with the mouse over it will get the events.
   *
   * @note Note that this is only usefull, if you gather the input events for the whole window manager at
   * once (and the windows manager has to decide all by itself , which window will handle input).
   *
   * @return Implementation of InputHandlers::MouseFocusInputHandler
   */
  InputHandlers::KeyFocusInputHandler::Ptr createKeyFocusInputHandler(bool forceHavingFocusedWindow);

  /** Creates a key focus input handler distributing events between the widgets of the given
   * window.
   *
   * @return Implementation of InputHandlers::MouseFocusInputHandler
   */
  InputHandlers::KeyFocusInputHandler::Ptr createKeyFocusInputHandlerForWindow(BaseWindowContainer& windowContainer, bool forceHavingFocusedWindow);

  /** Creates an input-handler allowing to change the focus of a window using shortcuts, for example
   * Strg+Tab to switch between windows
   *
   * @note Note that this is only usefull, if you gather the input events for the whole window manager at
   * once (and the windows manager has to decide all by itself , which window will handle input).
   *
   * @return
   */
  InputHandler::Ptr createKeyFocusNavigationInputHandler();

  /** Creates an input-handler allowing to change the focus of a widget using shortcuts, for example
   * Tab to switch between widgets, or arrow keys
   *
   * @return
   */
  InputHandler::Ptr createKeyFocusNavigationInputHandlerForWindow(BaseWindowContainer& windowContainer, bool gameUi);

  InputHandler::Ptr createInputHandler_BlenderStyle();
  InputHandler::Ptr createInputHandlerForWindow_BlenderStyle(BaseWindowContainer& windowContainer);
  InputHandler::Ptr createInputHandlerForWindow_GameUIStyle(BaseWindowContainer& windowContainer);

private:
  friend class BaseWindowContainer;

  void _askForKeyFocus(const Widget::Ptr& widget, const Widget::FocusHint& focusHint);

protected:
  virtual void _addMainWindow(const Window::Ptr& window) = 0;
  virtual void _addDialog(const Window::Ptr& window) = 0;
  virtual void _closeWindow(const BaseWindowContainer::Ptr& windowContainer) = 0;
  virtual QVector<BaseWindowContainer::Ptr> _windowOrderToHandleUserInput() = 0;

  virtual bool canBeFocused(Widget& widget, Window& window, const Widget::FocusHint& focusHint);

private:
  bool _addWindow(const Window::Ptr& window, const std::function<void(const Window::Ptr&)>& _insertWindwoIntoList);

  void _sendSignalMouseActionStarted(const InputHandlers::MouseAction::Ptr& mouseAction, const Window::Ptr& window);
};

} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_BASEWINDOWMANAGER_H
