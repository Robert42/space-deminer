#include "input-handler.h"
#include "input-handlers/fallback-input-handler.h"

namespace Framework {
namespace Gui {


InputHandler::InputHandler()
{
}

InputHandler::~InputHandler()
{
}

InputHandler::Ptr InputHandler::fallbackInputHandler()
{
  static Ptr fallback(InputHandlers::FallbackInputHandler::create());

  return fallback;
}


} // namespace Gui
} // namespace Framework
