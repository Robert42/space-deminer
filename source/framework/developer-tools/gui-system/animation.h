#ifndef FRAMEWORK_GUI_ANIMATION_H
#define FRAMEWORK_GUI_ANIMATION_H

#include <base/strings/string-id-manager.h>
#include <base/optional.h>
#include <base/output.h>
#include <base/signals.h>

namespace Framework {
using namespace Base;
namespace Gui {

class BaseAnimation;
class Animation
{
public:
  typedef std::shared_ptr<Animation> Ptr;
  typedef std::weak_ptr<Animation> WeakPtr;

  enum SlotId : std::underlying_type<StringId>::type
  {
  };

  BEGIN_ENUMERATION(FadeInterpolation,)
    LINEAR,
    EASY_IN,
    EASY_OUT,
    EASY_IN_AND_OUT
  ENUMERATION_BASIC_OPERATORS(FadeInterpolation)
  END_ENUMERATION;

  BEGIN_ENUMERATION(SnapInterpolation,)
    STICKY_TARGET,
    DAMPED,
    BOUNCING,
    RAPID
  ENUMERATION_BASIC_OPERATORS(SnapInterpolation)
  END_ENUMERATION;

private:
  class SetUpdatingFlag;
  class SetFinishingFlag;

private:
  real _currentValue;
  real _currentVelocity;
  real _currentAcceleration;
  bool _started : 1;
  bool _finished : 1;
  bool _finishedSignalSent : 1;
  bool _updateFunctionIsCalled : 1;
  bool _finishFunctionIsCalled : 1;
  bool _garbageCollectorRequested : 1;

  Signals::CallableSignal<void()> _signalStarted;
  Signals::CallableSignal<void()> _signalFinished;
  Signals::CallableSignal<void()> _signalUpdated;
  Signals::CallableSignal<void()> _signalGarbageCollectionRequested;

public:
  Animation();
  virtual ~Animation();

  static Ptr createFallback();

  static SlotId slotIdFromName(const char* name);
  static const Animation::Ptr& totalApplicationTimeAnimation();
  static Animation::Ptr timeAnimationStartingNow(real delay=0.f);

  static Ptr _createInterpolationAnimation(real duration,
                                           const FadeInterpolation& interpolation,
                                           real delay,
                                           const Animation::Ptr& timeAnimation);
  static Ptr _createSnapAnimation(const SnapInterpolation& interpolation,
                                  real targetValue,
                                  real duration,
                                  const Animation::Ptr& timeAnimation,
                                  real currentValue = 0.f,
                                  real currentVelocity = 0.f,
                                  real smoothness = 0.5f);

public:
  real currentValue() const;
  real currentVelocity() const;
  real currentAcceleration() const;

  void setFinished();
  bool finished() const;

  bool started() const;

  bool garbageCollectorRequested() const;

  void updateValues();

  Signals::Signal<void()>& signalStarted();
  Signals::Signal<void()>& signalUpdated();
  Signals::Signal<void()>& signalFinished();
  Signals::Signal<void()>& signalGarbageCollectionRequested();

  static Ptr garbageCollect(const Ptr& animation);

protected:
  virtual void _updateValues(const Output<real>& currentValue,
                             const Output<real>& currentVelocity,
                             const Output<real>& currentAcceleration) = 0;

  /**
   * @brief garbage collects the animation.
   *
   * For example, if a fading animation has successfully faded, the fade overload of this function will
   * return the target animation, so no memory or cpu time is wasted in keeping the fade-animation in the animation tree when it won't have any effect anymore.
   *
   * @note You have tu guarantee, that the values of an animation won't be changed by this function.
   *
   * @note This is the only function allowed to change the structure of the animation tree. Otherwise no function is allowed to change this, as
   * changing the tree itself might result in
   *
   * @param self A smartpointer pointing to the animation itself
   *
   * @return the garbage collection animation
   */
  virtual Ptr _garbageCollect(const Ptr& self);

private:
  void sendSignalUpdated();
  void sendSignalStarted();
  void sendSignalFinished();

protected:
  void sendSignalGarbageCollectionRequested();
};


class AnimationMap final
{
private:
  QHash<Animation::SlotId, real> _previousUnanimatedValues;
  QHash<Animation::SlotId, real> _currentUnanimatedValues;
  QHash<Animation::SlotId, Animation::Ptr> _animations;
  QHash<Animation::SlotId, Signals::Connection> _updateConnections;
  QHash<Animation::SlotId, Signals::Connection> _finishedConnections;

  Signals::CallableSignal<void()> _signalUpdated;
  Signals::CallableSignal<void(Animation::SlotId)> _signalRemovedAnimation;
  Signals::CallableSignal<void(Animation::SlotId)> _signalAddedAnimation;

public:
  Signals::Trackable trackable;

public:
  AnimationMap();

public:
  Signals::Signal<void()>& signalUpdated();
  Signals::Signal<void(Animation::SlotId)> signalRemovedAnimation();
  Signals::Signal<void(Animation::SlotId)> signalAddedAnimation();

  Optional<real> optionalValue(Animation::SlotId id) const;
  Optional<real> optionalVelocity(Animation::SlotId id) const;
  Optional<real> optionalAcceleration(Animation::SlotId id) const;

  real value(Animation::SlotId id) const;
  real value(Animation::SlotId id, real fallback) const;
  real velocity(Animation::SlotId id, real fallback=0.f) const;
  real acceleration(Animation::SlotId id, real fallback=0.f) const;

  real previousUnanimatedValue(Animation::SlotId id) const;
  real currentUnanimatedValue(Animation::SlotId id) const;
  void setCurrentUnanimatedValue(Animation::SlotId id, real value);


  void setAnimation(Animation::SlotId id, const Animation::Ptr& animation);
  void swapAnimation(Animation::SlotId id, const Animation::Ptr& animation);
  void removeAnimation(Animation::SlotId id);

  void snapToValue(Animation::SlotId id, real value);
  void snapToDefault(Animation::SlotId id);

  void fadeAnimation(Animation::SlotId id,
                     const Animation::Ptr& nextAnimation,
                     real interpolationDuration,
                     const Animation::FadeInterpolation& interpolation = Animation::FadeInterpolation::EASY_IN_AND_OUT,
                     real interpolationDelay = 0.f);
  void fadeAnimation(Animation::SlotId id,
                     const Animation::Ptr& nextAnimation,
                     real interpolationDuration,
                     const Animation::FadeInterpolation& interpolation,
                     real interpolationDelay,
                     const Animation::Ptr& timeAnimation,
                     bool finishAfterInterpolation = false,
                     bool enforcedSourceAnimation = false);
  void fadeAnimation(Animation::SlotId id,
                     const Animation::Ptr& nextAnimation,
                     const Animation::Ptr& interpolation,
                     bool finishAfterInterpolation = false,
                     bool enforcedSourceAnimation = false);

  void fadeToValue(Animation::SlotId id,
                   real value,
                   real interpolationDuration,
                   const Animation::FadeInterpolation& interpolation = Animation::FadeInterpolation::EASY_IN_AND_OUT,
                   real interpolationDelay = 0.f);
  void fadeToValue(Animation::SlotId id,
                   real value,
                   real interpolationDuration,
                   const Animation::FadeInterpolation& interpolation,
                   real interpolationDelay,
                   const Animation::Ptr& timeAnimation);
  void fadeToDefault(Animation::SlotId id,
                     real interpolationDuration,
                     const Animation::FadeInterpolation& interpolation = Animation::FadeInterpolation::EASY_IN_AND_OUT,
                     real interpolationDelay = 0.f);
  void fadeToDefault(Animation::SlotId id,
                     real interpolationDuration,
                     const Animation::FadeInterpolation& interpolation,
                     real interpolationDelay,
                     const Animation::Ptr& timeAnimation);
  void fadeToValue(Animation::SlotId id, real value, const Animation::Ptr& interpolation, bool finishAfterInterpolation = false);

  void snapToValue(Animation::SlotId id,
                   real value,
                   real interpolationDuration,
                   const Animation::SnapInterpolation& interpolation = Animation::SnapInterpolation::DAMPED,
                   real smooth = 1.f);
  void snapToValue(Animation::SlotId id,
                   real value,
                   real interpolationDuration,
                   const Animation::SnapInterpolation& interpolation,
                   real smooth,
                   const Animation::Ptr& timeAnimation,
                   bool finishAfterInterpolation = false);
  void snapToDefault(Animation::SlotId id,
                     real interpolationDuration,
                     const Animation::SnapInterpolation& interpolation = Animation::SnapInterpolation::DAMPED,
                     real smooth = 1.f);
  void snapToDefault(Animation::SlotId id,
                     real interpolationDuration,
                     const Animation::SnapInterpolation& interpolation,
                     real smooth,
                     const Animation::Ptr& timeAnimation);

private:
  void _handleFinishedAnimation(Animation::SlotId id, const Animation::Ptr& animation);
  Optional<Animation::Ptr> _getGarbageCollected(Animation::SlotId id) const;
  void _removeAnimation(Animation::SlotId id, bool sendSignals);
};


} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_ANIMATION_H
