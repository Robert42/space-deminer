#include "base-window-container.h"
#include "base-window-manager.h"

namespace Framework {
namespace Gui {

BaseWindowContainer::BaseWindowContainer(BaseWindowManager& windowManager, const Window::Ptr& window, bool roundAllocationHint)
  : windowManager(windowManager),
    window(window),
    inputHandler(InputHandlers::FocusableInputHandler::create(windowManager.createInputHandlerForWindow(*this)))
{
  assert("Window shouldn't ne already assigned to a window-container" && !window->_windowContainer);
  window->_windowContainer = this;

  window->_roundAllocationHint = roundAllocationHint;

  this->setWindowAppearance(windowManager.appearance());
}

BaseWindowContainer::~BaseWindowContainer()
{
  assert(window->_windowContainer == this);
  window->_windowContainer.reset();
}

void BaseWindowContainer::redraw(Painter& painter)
{
  QVector<Widget::Ptr> widgets = window->allWidgetsUpdatedAndSorted(true);

  beginDrawing(painter);
  for(const Widget::Ptr& w : widgets)
  {
    painter.setScissorRect(w->scissor());
    w->draw(painter);
  }
  endDrawing(painter);

  painter.setWindowTransformation(mat4(1.f));

  unmarkRedrawDirty();
}

void BaseWindowContainer::setWindowAppearance(const Appearance::Ptr& appearance)
{
  window->setAppearance(appearance);
}

void BaseWindowContainer::setWindowSize(const vec2& windowSize)
{
  window->setSize(windowSize);
}

void BaseWindowContainer::_handleMovedWindow()
{
  window->invalidateMouseOverWidget();
  windowManager.invalidateWindowOrderToHandleUserInput();
}

void BaseWindowContainer::unmarkRedrawDirty()
{
  window->_redrawSheduled = false;
}

QVector<Widget::Ptr> BaseWindowContainer::widgetsAtCurrentMousePosition() const
{
  return Widget::sortSetIntoVector(widgetsAtCurrentMousePositionImplementation());
}

void BaseWindowContainer::_askForKeyFocus(const Widget::Ptr& widget, const Widget::FocusHint& focusHint)
{
  windowManager._askForKeyFocus(widget, focusHint);
}


} // namespace Gui
} // namespace Framework
