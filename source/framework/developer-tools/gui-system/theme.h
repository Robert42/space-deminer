#ifndef FRAMEWORK_GUI_THEME_H
#define FRAMEWORK_GUI_THEME_H

#include <base/types/type.h>
#include <base/tag-set.h>
#include <base/color-tools.h>

#include "painter.h"
#include "metrics.h"
#include "animation.h"

#include <base/enum-macros.h>

namespace Framework {
namespace Gui {

class Widget;
class Layout;

class Theme : public noncopyable, public std::enable_shared_from_this<Theme>
{
public:
  typedef std::shared_ptr<Theme> Ptr;

public:
  class State
  {
  public:
    BEGIN_ENUMERATION(MouseState,)
      HOVER,
      PRESSED
    ENUMERATION_BASIC_OPERATORS(MouseState)
    END_ENUMERATION;

    real alpha;
    real sensitive;
    real keyFocused;
    real mouseStates[2];
    QHash<Animation::SlotId, real> customStates;

    State();

    template<typename T>
    T interpolateValues(const T& invisible,
                        const T& insensitive,
                        const T& normal, const T& hover, const T& pressed,
                        const T& keyFocused_normal, const T& keyFocused_hover, const T& keyFocused_pressed) const;
    template<typename T>
    T interpolateColors(const T& insensitive,
                        const T& normal, const T& hover, const T& pressed,
                        const T& keyFocused_normal, const T& keyFocused_hover, const T& keyFocused_pressed) const;
  };

  class RenderData
  {
  public:
    Rectangle<vec2> allocation;
    float zPosition;
    State state;
    TagSet tags;
  };

  class SimpleWidget : public noncopyable
  {
  public:
    typedef std::shared_ptr<SimpleWidget> Ptr;
    typedef Theme::State State;

  public:
    virtual ~SimpleWidget();

    static Ptr createFallback(const vec2& size=vec2(75, 25));

  public:
    virtual Optional<vec2> minimumSize() const;
    virtual Optional<vec2> optimalSize() const;
    virtual void draw(Painter& painter, const RenderData& renderData) = 0;
    virtual bool isWidgetAtPoint(const vec2& point, const RenderData& renderData) const = 0;
    void draw(Painter& painter,
              const Widget* widget);
    bool isWidgetAtPoint(const vec2& point,
                         const Widget* widget) const;
  };
  class SimpleTextWidget : public noncopyable
  {
  public:
    typedef std::shared_ptr<SimpleTextWidget> Ptr;
    typedef Theme::State State;

  public:
    SimpleTextWidget();
    virtual ~SimpleTextWidget();

    static Ptr createFallback();

    virtual vec2 minimumSize(const String& text) const = 0;
    virtual vec2 optimalSize(const String& text) const = 0;

    virtual void draw(const String& text,
                      Painter& painter,
                      const RenderData& renderData) = 0;
    void draw(const String& text,
              Painter& painter,
              const Widget* widget);
  };

  class AnimationData
  {
  public:
    TagSet& animationTags;
    AnimationMap& animationMap;
    bool& recursive;
    const ClassType& typeWidget;
    const TagSet& widgetTags;
    const Optional<ClassType>& parentLayoutType;
    const TagSet& parentLayoutTags;
    const RenderData& currentState;

    AnimationData(const InOutput<TagSet>& animationTags,
                  const InOutput<AnimationMap>& animationMap,
                  const Output<bool>& recursive,
                  const ClassType& typeWidget,
                  const TagSet& widgetTags,
                  const Optional<ClassType>& parentLayoutType,
                  const TagSet& parentLayoutTags,
                  const RenderData& currentState);
  };

public:
  Theme();
  virtual ~Theme();

  static Ptr fallbackTheme();

  virtual Theme::Ptr childTheme(const TagSet& tags);
  virtual Optional<SimpleWidget::Ptr> simpleWidget(const ClassType& widgetType,
                                                   const TagSet& tags=TagSet()) = 0;
  SimpleWidget::Ptr simpleWidgetEnforced(const ClassType& widgetType,
                                         const TagSet& tags=TagSet());
  SimpleWidget::Ptr simpleWidgetEnforced(Widget* widget);


  virtual Optional<SimpleTextWidget::Ptr> simpleTextWidget(const ClassType& widgetType,
                                                           const TagSet& tags=TagSet()) = 0;
  SimpleTextWidget::Ptr simpleTextWidgetEnforced(const ClassType& widgetType,
                                                 const TagSet& tags=TagSet());
  SimpleTextWidget::Ptr simpleTextWidgetEnforced(Widget* widget);


  virtual Optional<const Metrics*> layoutMetrics(const ClassType& typeLayout,
                                                 const ClassType& typeWidget,
                                                 const TagSet& tagsLayout=TagSet(),
                                                 const TagSet& tagsWidget=TagSet()) = 0;
  const Metrics* layoutMetricsEnforced(const ClassType& typeLayout,
                                       const Optional<ClassType>& typeWidget,
                                       const TagSet& tagsLayout=TagSet(),
                                       const TagSet& tagsWidget=TagSet());
  const Metrics* layoutMetricsEnforced(const ClassType& typeLayout,
                                       const ClassType& typeWidget,
                                       const TagSet& tagsLayout=TagSet(),
                                       const TagSet& tagsWidget=TagSet());
  const Metrics* layoutMetricsEnforced(Layout* layout);

  virtual Optional<const Metrics*> layoutSpacingItemMetrics(const ClassType& typeIdLayout,
                                                            const ClassType& typeIdWidget,
                                                            const TagSet& tagsLayout=TagSet(),
                                                            const TagSet& tagsWidget=TagSet()) = 0;
  const Metrics* layoutSpacingItemMetricsEnforced(const ClassType& typeLayout,
                                                  const ClassType& typeWidget,
                                                  const TagSet& tagsLayout=TagSet(),
                                                  const TagSet& tagsWidget=TagSet());
  const Metrics* layoutSpacingItemMetricsEnforced(const ClassType& typeLayout,
                                                  const Optional<ClassType>& typeWidget,
                                                  const TagSet& tagsLayout=TagSet(),
                                                  const TagSet& tagsWidget=TagSet());
  const Metrics* layoutSpacingItemMetricsEnforced(Layout* layout);

  virtual bool playAnimation(const InOutput<AnimationData>& animationData) = 0;
  bool playAnimation(const InOutput<TagSet>& animationTags,
                     Widget* widget,
                     const Output<bool>& recursive);
};

} // namespace Gui
} // namespace Framework

#include "theme.inl"

#endif // FRAMEWORK_GUI_THEME_H
