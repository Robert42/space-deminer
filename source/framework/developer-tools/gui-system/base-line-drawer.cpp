#include "base-line-drawer.h"
#include <base/io/log.h>

namespace Framework {
namespace Gui {

BaseLineDrawer::BaseLineDrawer()
  : loop(false),
    haveToSetFirstTangentIn(false),
    points(*this),
    uv(0),
    color(1),
    thickness(1),
    antialiasing(DEFAULT_ANTIALIASING_WIDTH),
    trailOffset(0),
    rightAntialiasing(true),
    leftAntialiasing(true)

{
}


BaseLineDrawer::~BaseLineDrawer()
{
  end();
}

void BaseLineDrawer::beginLoop()
{
  onBegin();
  end();
  loop = true;
}

void BaseLineDrawer::begin()
{
  onBegin();
  end();
  loop = false;
  haveToSetFirstTangentIn = true;
}

void BaseLineDrawer::beginLoop(const vec2& firstPoint)
{
  beginLoop();

  points.append(firstPoint);
}

void BaseLineDrawer::begin(const vec2& firstPoint, const vec2& tangent)
{
  begin(firstPoint);
  haveToSetFirstTangentIn = false;
  points[0].tangentIn = tangent;
}

void BaseLineDrawer::begin(const vec2& firstPoint)
{
  begin();
  points.append(firstPoint);
}

void BaseLineDrawer::drawTo(const vec2& p)
{
  appendPoint(p);
}

void BaseLineDrawer::appendPoint(const vec2& p)
{
  int prevPoint = points.size()-1;

  points.append(p);

  if(prevPoint>1)
    drawVertex(points[prevPoint-1], points[prevPoint]);
}

void BaseLineDrawer::end()
{
  if(points.empty())
    return;

  end(points[-1].position - points[-2].position);
}

void BaseLineDrawer::endFacingLastAndFirstTangent()
{
  if(points.empty())
    return;
  haveToSetFirstTangentIn = false;

  points[0].tangentIn = points[-1].position - points[-2].position;
  end(points[1].position - points[0].position);
}

void BaseLineDrawer::end(const vec2& outTangent)
{
  if(points.empty())
    return;

  if(points.size() == 1)
  {
    points.clear();
    onEnd();
    return;
  }
  if(points.size() == 2)
  {
    points[0].tangentIn = points[0].tangentOut = points[1].tangentIn = points[1].tangentOut = points[1].position-points[0].position;
    drawVertex(points[0], points[1]);
    points.clear();
    onEnd();
    return;
  }

  if(loop)
  {
    points[-1].tangentOut = points[0].position - points[-1].position;
    points[0].tangentIn = points[-1].tangentOut;
  }else
  {
    points[-1].tangentOut = outTangent;
    if(haveToSetFirstTangentIn)
      points[0].tangentIn = points[1].position - points[0].position;
  }

  drawVertex(points[-2], points[-1]);
  if(loop)
    drawVertex(points[-1], points[0]);
  drawVertex(points[0], points[1]);
  points.clear();
  onEnd();
}

void BaseLineDrawer::drawVertex(const Point& a, const Point& b)
{
  drawLineStripSegment(a.tangentIn,
                       a.position,
                       b.position,
                       b.tangentOut,
                       a.uv,
                       b.uv,
                       a.color,
                       b.color,
                       a.thickness,
                       b.thickness,
                       a.antialiasing,
                       b.antialiasing,
                       a.trailOffset,
                       b.trailOffset,
                       rightAntialiasing,
                       leftAntialiasing);
}

void BaseLineDrawer::drawLineStripSegment(const vec2& aTangent,
                                          vec2 a,
                                          vec2 b,
                                          const vec2& bTangent,
                                          const vec2& uvA,
                                          const vec2& uvB,
                                          const vec4& colorA,
                                          const vec4& colorB,
                                          real thicknessA,
                                          real thicknessB,
                                          real antialiasingA,
                                          real antialiasingB,
                                          real trailOffsetA,
                                          real trailOffsetB,
                                          bool rightAntialiasing,
                                          bool leftAntialiasing)
{
  assert(a!=b); // Otherwise normalize(mainDirection) will return nan

  const vec2 mainDirection = b-a;

  const vec2 mainPerpendicular = normalize(perpendicular90Degree(mainDirection));
  const vec2 aPerpendicular = normalize(perpendicular90Degree(aTangent));
  const vec2 bPerpendicular = normalize(perpendicular90Degree(bTangent));

  vec2 interpolatedAPerpendicular = normalize(mainPerpendicular+aPerpendicular);
  vec2 interpolatedBPerpendicular = normalize(mainPerpendicular+bPerpendicular);

  if(isnan(interpolatedAPerpendicular))
    interpolatedAPerpendicular = mainPerpendicular;
  if(isnan(interpolatedBPerpendicular))
    interpolatedBPerpendicular = mainPerpendicular;

  const real cosHalfAngleA = dot(interpolatedAPerpendicular, mainPerpendicular);
  const real cosHalfAngleB = dot(interpolatedBPerpendicular, mainPerpendicular);

  vec2 finalAPerpendicular;
  vec2 finalBPerpendicular;

  a += trailOffsetA * interpolatedAPerpendicular;
  b += trailOffsetB * interpolatedBPerpendicular;

  /* As the segments are angeled, the widths have to be corrected, so the line doesn't get thinner at edges
   * The idea behind the following formula:
   * Let the connection between two segments be the final thickness t. Then the perpendicular p to the tanget
   * form a rectanglular triangle at the point. The direction of the connection c is t
   * (t = finalThicknessA; p=thicknessA*mainPerpendicular; c = t * finalAPerpendicular)
   *
   *                   p |   / c
   *                     |  /
   *                     | /
   * ---------------------/
   *                       \
   *                         \
   *                           \ tangent
   *
   * cos(angle(between c and p)) = length(p) / length(c)
   * <=>
   * length(c) = length(p) / cos(angle(between c and p))
   * <=>
   * t = length(p) / cos(angle(between c and p))
   * <=>
   * finalThicknessA = thicknessA / cos(angle(between finalAPerpendicular and mainPerpendicular))
   * <=>
   * finalThicknessA = thicknessA / cos(acos(dot(finalAPerpendicular, mainPerpendicular) / (length(finalAPerpendicular) * length(mainPerpendicular))))
   * <=> (As finalAPerpendicular and mainPerpendicular are normalized)
   * finalThicknessA = thicknessA / cos(acos(dot(finalAPerpendicular, mainPerpendicular)))
   * <=>
   * finalThicknessA = thicknessA / dot(finalAPerpendicular, mainPerpendicular))
   *
   * If the angle gets too sharp
   */
  const real cosAngleLimit = 0.256f;
  if(cosHalfAngleA > cosAngleLimit)
  {
    real factor = 1.f / cosHalfAngleA;

    thicknessA *= factor;
    antialiasingA *= factor;
    finalAPerpendicular = interpolatedAPerpendicular;
  }else
  {
    finalAPerpendicular = mainPerpendicular;
  }
  if(cosHalfAngleB > cosAngleLimit)
  {
    real factor = 1.f / cosHalfAngleB;


    thicknessB *= factor;
    antialiasingB *= factor;
    finalBPerpendicular = interpolatedBPerpendicular;
  }else
  {
    finalBPerpendicular = mainPerpendicular;
  }

  if(antialiasingA)
  {
    antialiasingA *= antialiasingFactorForPoint(a, finalAPerpendicular);
    thicknessA = max<real>(0.f, thicknessA-antialiasingA);
  }
  if(antialiasingB)
  {
    antialiasingB *= antialiasingFactorForPoint(b, finalBPerpendicular);
    thicknessB = max<real>(0.f, thicknessB-antialiasingB);
  }

  vec2 halfThicknessAVector = finalAPerpendicular * (0.5f * thicknessA);
  vec2 halfThicknessBVector = finalBPerpendicular * (0.5f * thicknessB);

  vec4 colors[4] = {colorA, colorA, colorB, colorB};
  vec2 uvs[4] = {uvA, uvA, uvB, uvB};
  vec2 positions[4];

  if(thicknessA > 0.f || thicknessB > 0.f)
  {
    positions[0] = a-halfThicknessAVector;
    positions[1] = a+halfThicknessAVector;
    positions[2] = b+halfThicknessBVector;
    positions[3] = b-halfThicknessBVector;

    drawMainQuad(positions, colors, uvs);
  }

  if((antialiasingA||antialiasingB) && (rightAntialiasing||leftAntialiasing))
  {
    colors[1].a = 0.f;
    colors[2].a = 0.f;

    if(rightAntialiasing)
    {
      positions[0] = a+halfThicknessAVector;
      positions[1] = a+halfThicknessAVector+finalAPerpendicular*antialiasingA;
      positions[2] = b+halfThicknessBVector+finalBPerpendicular*antialiasingB;
      positions[3] = b+halfThicknessBVector;

      drawQuad(positions, colors, uvs);
    }

    if(leftAntialiasing)
    {
      positions[0] = a-halfThicknessAVector;
      positions[1] = a-halfThicknessAVector-finalAPerpendicular*antialiasingA;
      positions[2] = b-halfThicknessBVector-finalBPerpendicular*antialiasingB;
      positions[3] = b-halfThicknessBVector;

      drawQuad(positions, colors, uvs);
    }
  }
}

void BaseLineDrawer::drawMainQuad(const vec2* positions, const vec4* colors, const vec2* uvs)
{
  drawQuad(positions, colors, uvs);
}

void BaseLineDrawer::onBegin()
{
}

void BaseLineDrawer::onEnd()
{
}

real BaseLineDrawer::antialiasingFactorForPoint(const vec2& point, const vec2& antialiasingDirection)
{
  (void)point;
  (void)antialiasingDirection;
  return 1.f;
}

void BaseLineDrawer::drawArcBorder(const Circle& circle,
                                                       real fromAngle,
                                                       real toAngle,
                                                       real centerAntialiasing,
                                                       bool skipLastPoint)
{
  vec2 oldUv = this->uv;
  drawArcBorder(circle, [oldUv](real)->vec2{return oldUv;}, fromAngle, toAngle, centerAntialiasing, skipLastPoint);
}

void BaseLineDrawer::drawArcBorder(const Circle& circle,
                                                       const std::function<vec2(real)>& uvForAngle,
                                                       real fromAngle,
                                                       real toAngle,
                                                       real centerAntialiasing,
                                                       bool skipLastPoint)
{
  const real oldAntialiasing = this->antialiasing;
  const vec2 oldUv = this->uv;

  auto drawSinglePoint = [this,&uvForAngle,&circle, oldAntialiasing, centerAntialiasing](real angle){
    const vec2 v1 = circle.edgePointForAngle(angle);

    this->uv = uvForAngle(angle);
    this->antialiasing = _arcAntialiasing(angle, oldAntialiasing, centerAntialiasing);

    this->drawTo(v1);
  };

  this->_forEachEdgeOfCircle(circle, std::bind(drawSinglePoint, _1), fromAngle, toAngle);

  if(!skipLastPoint)
    drawSinglePoint(toAngle);

  this->antialiasing = oldAntialiasing;
  this->uv = oldUv;
}


BaseLineDrawer::PointList::PointList(const BaseLineDrawer& drawer)
  : drawer(drawer),
    _size(0)
{
}

int BaseLineDrawer::PointList::size() const
{
  return _size;
}

BaseLineDrawer::Point& BaseLineDrawer::PointList::operator[](int i)
{
  assert(size()>0);

  i = (size()+i)%size();

  if(i == 0)
    return _firstPoint;
  if(i == 1)
    return _secondPoint;

  assert(i>=_size-3);

  return _points[i%3];
}

void BaseLineDrawer::PointList::append(const vec2& pos)
{
  if(size()>1)
  {
    Point& pointBefore = (*this)[_size-1];
    if(pointBefore.position == pos)
    {
#ifndef NDEBUG
      std::cerr << "WARNING: BaseLineDrawer::PointList::append: Trying to draw the same point " << pos << " twice"  << std::endl;
#endif
      return;
    }
  }

  _size++;
  Point& p = (*this)[_size-1];

  p.antialiasing = drawer.antialiasing;
  p.uv = drawer.uv;
  p.color = drawer.color;
  p.thickness = drawer.thickness;
  p.trailOffset = drawer.trailOffset;
  p.position = pos;

  if(size()>1)
  {
    Point& pointBefore = (*this)[_size-2];

    p.tangentIn = p.position - pointBefore.position;
    pointBefore.tangentOut = p.tangentIn;
    p.tangentOut = p.tangentIn;
  }
}

void BaseLineDrawer::PointList::clear()
{
  _size = 0;
}

bool BaseLineDrawer::PointList::empty() const
{
  return _size == 0;
}

// ====


real BaseLineDrawer::_arcAntialiasing(real angle, real antialiasing, real centerAntialiasing)
{
  real antialiasingWeight = clamp(abs(glm::mod(angle, static_cast<real>(half_pi)) - quarter_pi) / quarter_pi, 0.f, 1.f);
  antialiasingWeight *= antialiasingWeight;
  antialiasingWeight = antialiasingWeight*0.75f + 0.25f;

  return interpolate(antialiasingWeight, centerAntialiasing, antialiasing);
}

void BaseLineDrawer::_forEachEdgeOfCircle(const Circle& circle, const std::function<void(real angle1,real angle2)>& function, real fromAngle, real toAngle)
{
  real angleRange = toAngle - fromAngle;

  real percentOfCircle = abs(angleRange) / two_pi;

  const int n=max(12, int(percentOfCircle * circle.radius()));
  for(int i=0; i<n; ++i)
  {
    const float a1 = fromAngle + angleRange * float(i)/n;
    const float a2 = fromAngle + angleRange * float(i+1)/n;

    function(a1, a2);
  }
}


// ==== VertexBufferLineDrawer ========


_VertexBufferLineDrawer::_VertexBufferLineDrawer()
{
  _VertexBufferLineDrawer::onBegin();
}

const QVector<vec2>& _VertexBufferLineDrawer::outlineVerticesLeft() const
{
  return this->_outlineVerticesLeft;
}

const QVector<vec2>& _VertexBufferLineDrawer::outlineVerticesRight() const
{
  return this->_outlineVerticesRight;
}

void _VertexBufferLineDrawer::onBegin()
{
  _outlineVerticesLeft.clear();
  _outlineVerticesLeft.append(vec2(0));
  _outlineVerticesLeft.append(vec2(0));

  _outlineVerticesRight.clear();
  _outlineVerticesRight.append(vec2(0));
  _outlineVerticesRight.append(vec2(0));
}

void _VertexBufferLineDrawer::onEnd()
{
  removeLast();
}


void _VertexBufferLineDrawer::drawMainQuad(const vec2* positions, const vec4* colors, const vec2* uvs)
{
  drawQuad(positions, colors, uvs);

  _outlineVerticesLeft[0] = positions[0];
  _outlineVerticesLeft[1] = positions[3];
  _outlineVerticesLeft.append(positions[3]);

  _outlineVerticesRight[0] = positions[1];
  _outlineVerticesRight[1] = positions[2];
  _outlineVerticesRight.append(positions[2]);
}


void _VertexBufferLineDrawer::removeLast()
{
  if(!outlineVerticesLeft().empty())
    _outlineVerticesLeft.removeLast();
  if(!outlineVerticesRight().empty())
    _outlineVerticesRight.removeLast();
}


// ==== OutlineLineDrawer ========


OutlinedLineDrawer::Outline::Outline(OutlinedLineDrawer& drawer)
  : drawer(drawer)
{
}

void OutlinedLineDrawer::Outline::drawQuad(const vec2* positions, const vec4* colors, const vec2* uvs)
{
  drawer.drawQuad(positions, colors, uvs);
}

real OutlinedLineDrawer::Outline::antialiasingFactorForPoint(const vec2& point, const vec2& antialiasingDirection)
{
  return drawer.antialiasingFactorForPoint(point, antialiasingDirection);
}


// ----


OutlinedLineDrawer::OutlinedLineDrawer()
  : outline(*this)
{
}

void OutlinedLineDrawer::drawOutline(bool innerAntialiasing, bool outerAntialiasing)
{
  drawOutline(outlineVerticesLeft(),
              outlineVerticesRight(),
              innerAntialiasing,
              outerAntialiasing);
}

void OutlinedLineDrawer::drawOutline(const QVector<vec2>& verticesLeft,
                                     const QVector<vec2>& verticesRight,
                                     bool innerAntialiasing,
                                     bool outerAntialiasing,
                                     bool implicitLoop)
{
  outline.leftAntialiasing = outerAntialiasing;
  outline.rightAntialiasing = innerAntialiasing;
  _drawOutline(verticesLeft, implicitLoop);
  outline.leftAntialiasing = innerAntialiasing;
  outline.rightAntialiasing = outerAntialiasing;
  _drawOutline(verticesRight, implicitLoop);
  outline.leftAntialiasing = true;
  outline.rightAntialiasing = true;
}

void OutlinedLineDrawer::_drawOutline(const QVector<vec2>& vertices, bool implicitLoop)
{
  if(vertices.size()<2)
    return;

  int n = vertices.size();

  if(implicitLoop)
  {
    outline.beginLoop();
  }else if(squareDistance(vertices.first(), vertices[n-1]) < outline.thickness*1.e-6f)
  {
    outline.beginLoop();
    n--;
  }else
  {
    outline.begin();
  }

  assert(n>0);

  // ignore the last vertex of the list, because, the first line segment is the last one drawn, so the
  // last vertex is the second point
  for(int i=0; i<n; ++i)
  {
    const vec2& v = vertices[i];
    outline.drawTo(v);
  }

  outline.end();
}


} // namespace Gui
} // namespace Framework
