#ifndef FRAMEWORK_GUI_PAINTER_H
#define FRAMEWORK_GUI_PAINTER_H

#include "font.h"

#include <base/geometry/rounded-rectangle.h>
#include <base/enum-macros.h>
#include <base/signals/trackable.h>
#include <base/tracking-value-with-fallback.h>

namespace Framework {
namespace Gui {


class Painter
{
public:
  typedef std::shared_ptr<Painter> Ptr;

  BEGIN_ENUMERATION(AntialiasingHint,)
    FORBID_ANTIALIASING,
    ASK_FOR_ANTIALIASING,
    AXIS_ALIGNED
  ENUMERATION_BASIC_OPERATORS(AntialiasingHint)
  END_ENUMERATION;

  class LinePen final
  {
  public:
    typedef std::shared_ptr<LinePen> Ptr;

    class Implementation : public noncopyable
    {
    public:
      typedef std::shared_ptr<Implementation> Ptr;

      Painter& painter;

      static Ptr createFallback();

      Implementation(Painter& painter);
      virtual ~Implementation();

      virtual void setColor(const vec4& color) = 0;
      virtual void setThickness(real thickness) = 0;

      virtual void setDashLength(Optional<real> dashLength) = 0;

      virtual void beginStroke(bool loop) = 0;
      virtual void drawVertex(const vec2& point) = 0;
      virtual void endStroke() = 0;
    };

  private:
    TrackingValueWithFallback<Implementation::Ptr> implementation;

  public:
    LinePen();
    explicit LinePen(const Implementation::Ptr& implementation);

    void setColor(const vec4& color);
    void setThickness(real thickness);

    /** Sets the length of shashes.
     *
     * Note, that the length is relative to the thickness.
     * So with 1, the dashes will be rendered quadratic, so matter which thickness is chosen.
     *
     * @param dashLength
     */
    void setDashLength(Optional<real> dashLength);

    void beginStroke(bool loop = false);
    void drawVertex(const vec2& point);
    void endStroke();
  };

public:
  Signals::Trackable trackable;

protected:
  real _hiDpiFactor;

public:
  Painter();
  virtual ~Painter();

  static Ptr fallbackPainter();

  virtual void setAntialiasingHint(AntialiasingHint hint) = 0;

  virtual void setWindowTransformation(const mat4& windowTransformation) = 0;
  virtual void setZPosition(real z) = 0;
  virtual void drawFilledTriangle(const vec4& color, const vec2& a, const vec2& b, const vec2& c) = 0;
  virtual void drawFilledRectangle(const Rectangle<vec2>& rectangle, const vec4& color) = 0;
  virtual void drawFilledRectangle(const Rectangle<vec2>& rectangle, const vec4& colorUpper, const vec4& colorLower) = 0;
  virtual void drawRoundedRectangle(const RoundedRectangle& roundedRectangle, const vec4& color, real border=0.f, real strokeOffset=0.f) = 0;

  virtual LinePen startDrawingLine() = 0;

  /** Draws a quad strip.
   *
   * @param verticesA
   * @param verticesB
   * @param loop If true, an implicit quad between connecting the first and the last quad is drawn.
   *        Prefer using this flag instead of drawing the last quad explicitly, as the renderer will
   *        be able to draw the antialiasing in a better way.
   *
   * @note One quad is rendered using the vertices `verticesA[i]`, `verticesB[i]`, `verticesB[i+1]`,
   *       `verticesA[i+1]`
   * @note This function expects `verticesA.size() == verticesB.size()`
   */
  virtual void drawQuadStrip(const QVector<vec2>& verticesLeft,
                             const QVector<vec2>& verticesRight,
                             const vec4& color,
                             bool loop) = 0;

  virtual void drawText(const String& text, const Font& font, const vec2& position, const vec4& color) = 0;


  virtual void setScissorRect(const Optional< Rectangle<vec2> >& scissorRect) = 0;

  real hiDpiFactor() const;
};

} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_PAINTER_H
