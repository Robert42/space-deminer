#include "widget.h"
#include "window.h"
#include "tags.h"

#include "animation-slot-ids.h"

#include "input-handlers/branch-off-input-handler.h"
#include "input-handlers/fallback-input-handler.h"
#include <base/tracking-pointer.h>

namespace Framework {
namespace Gui {


Widget::Widget()
  : _dirtyFlags(DirtyFlags::NONE),
    _includedToWidgetSlot(false),
    _sensitive(true),
    _parentIsSensitive(true),
    _visible(true),
    _shown(true),
    _parentIsVisible(true),
    _zPosition(0.f),
    _theme(Theme::fallbackTheme()),
    _window(nullptr),
    _parent(nullptr),
    _layout(nullptr),
    _minimumSize(0),
    _optimalSize(0)
{
  RTTI_CLASS_SET_TYPE(Framework::Gui::Widget);

  animationMap.signalUpdated().connect(std::bind(&Widget::sheduleRedraw, this)).track(this->trackable);
  animationMap.signalAddedAnimation().connect(std::bind(&Widget::_handleAddedOrRemovedAnimation, this, _1)).track(this->trackable);
  animationMap.signalRemovedAnimation().connect(std::bind(&Widget::_handleAddedOrRemovedAnimation, this, _1)).track(this->trackable);

  animationMap.setCurrentUnanimatedValue(AnimationSlots::RenderState::alpha, _visible);
  animationMap.setCurrentUnanimatedValue(AnimationSlots::RenderState::sensitive, _sensitive);
}

Widget::~Widget()
{
  this->trackable.clear();
}

const Widget::Index& Widget::index() const
{
  return _index;
}

real Widget::zPosition() const
{
  return _zPosition;
}

Optional<Window*> Widget::window()
{
  if(_window==nullptr)
    return nothing;

  return _window;
}

Optional<const Window*> Widget::window() const
{
  if(_window==nullptr)
    return nothing;

  return _window;
}

Optional<Container*> Widget::parent()
{
  if(_parent==nullptr)
    return nothing;

  return _parent;
}

Optional<const Container*> Widget::parent() const
{
  if(_parent==nullptr)
    return nothing;

  return _parent;
}

Optional<Layout*> Widget::layout()
{
  if(_layout==nullptr)
    return nothing;

  return _layout;
}

Optional<const Layout*> Widget::layout() const
{
  if(_layout==nullptr)
    return nothing;

  return _layout;
}

Optional<Container::Ptr> Widget::asContainer()
{
  if(!isContainer())
    return nothing;
  return std::static_pointer_cast<Container>(shared_from_this());
}

Optional< std::shared_ptr<const Container> > Widget::asContainer() const
{
  if(!isContainer())
    return nothing;
  return std::static_pointer_cast<const Container>(shared_from_this());
}

bool Widget::isContainer() const
{
  return this->rtti.type().isInheritingOrSame(Types::Container);
}


const Optional< Rectangle<vec2> >& Widget::scissor() const
{
  return _scissor;
}


bool Widget::canHaveKeyFocus() const
{
  if(!this->inputHandler())
    return false;
  InputHandlers::FocusableInputHandler::Ptr inputHandler = *this->inputHandler();

  return inputHandler->sensitive() && inputHandler->keySensitive() && this->sensitive() && visible();
}

bool Widget::canHaveMouseFocus() const
{
  if(!this->inputHandler())
    return false;
  InputHandlers::FocusableInputHandler::Ptr inputHandler = *this->inputHandler();

  return inputHandler->sensitive() && inputHandler->mouseSensitive() && this->sensitive() && visible();
}


bool Widget::hasKeyFocus() const
{
  if(!this->inputHandler())
    return false;
  InputHandlers::FocusableInputHandler::Ptr inputHandler = *this->inputHandler();

  return inputHandler->hasKeyFocus();
}

void Widget::askForKeyFocus(const FocusHint& focusHint)
{
  if(this->window().valid() && this->canHaveKeyFocus())
  {
    // This should work, as the widget is assigned to a window (and so to a parent widget)
    Widget::Ptr thisAsSmartPtr = *this->asSmartPtr();
    Window* window = *this->window();

    window->askForKeyFocus(thisAsSmartPtr, focusHint);
  }
}

void Widget::_tagAsKeyFocused()
{
  Optional<Window*> optionalWindow = this->window();
  if(optionalWindow)
  {
    Window* window = *optionalWindow;
    window->_tagWidgetAsKeyFocused(this);
  }
}

void Widget::_untagAsKeyFocused()
{
  Optional<Window*> optionalWindow = this->window();
  if(optionalWindow)
  {
    Window* window = *optionalWindow;
    window->_untagWidgetAsKeyFocused(this);
  }
}


bool Widget::visible() const
{
  return _visible;
}

void Widget::show()
{
  if(!_shown)
  {
    _shown = true;
    _updateVisibility();
    playAnimation({Tags::Animations::show});
  }
}

void Widget::hide()
{
  if(_shown)
  {
    _shown = false;
    _updateVisibility();
    playAnimation({Tags::Animations::hide});
  }
}

void Widget::_updateVisibility()
{
  bool visibleHint = this->_parentIsVisible && this->_shown;
  this->animationMap.setCurrentUnanimatedValue(AnimationSlots::RenderState::alpha, visibleHint);

  real animatedAlphaValue =  this->animationMap.value(AnimationSlots::RenderState::alpha);
  bool visibleByAnimation = (animatedAlphaValue>0.f);
  bool visible = visibleByAnimation;

  if(visible != this->visible())
  {
    this->_visible = visible;

    this->_markDirty(DirtyFlags::ALL);
    this->_markChildrenDirty(DirtyFlags::CHILD_Z_POSITION);

    _signalVisibilityChanged();

    invalidateMouseOverWidget();
    invalidateKeyFocusedWidget();
  }
}

void Widget::_updateParentVisibility()
{
  bool parentVisible = true;

  if(parent())
    parentVisible = (*parent())->visible();

  if(parentVisible != this->_parentIsVisible)
  {
    this->_parentIsVisible = parentVisible;
    _updateVisibility();
  }
}

void Widget::_updateSensitivity()
{
  bool sensitive = this->_parentIsSensitive && this->_localSensitive;

  if(sensitive != this->sensitive())
  {
    this->_sensitive = sensitive;

    animationMap.setCurrentUnanimatedValue(AnimationSlots::RenderState::sensitive, _sensitive);

    if(inputHandler())
    {
      (*inputHandler())->setSensitive(sensitive);
    }

    _signalSensitivityChanged();

    invalidateMouseOverWidget();
    invalidateKeyFocusedWidget();
  }
}

void Widget::_updateParentSensitivity()
{
  bool parentSensitive = true;

  if(parent())
    parentSensitive = (*parent())->sensitive();

  if(parentSensitive != this->_parentIsSensitive)
  {
    this->_parentIsSensitive = parentSensitive;
    _updateSensitivity();
  }
}

void Widget::_handleAddedOrRemovedAnimation(Animation::SlotId slotId)
{
  if(slotId == AnimationSlots::RenderState::alpha)
    _updateVisibility();
  if(slotId == AnimationSlots::suppressMouseInput)
    invalidateMouseOverWidget();
}


bool Widget::sensitive() const
{
  return _sensitive;
}

void Widget::setSensitive(bool sensitive)
{
  if(this->_localSensitive != sensitive)
  {
    this->_localSensitive = sensitive;

    _updateSensitivity();
  }
}

const Optional< InputHandlers::FocusableInputHandler::Ptr >& Widget::inputHandler() const
{
  return _inputHandler;
}

void Widget::setInputHandler(const Optional< InputHandlers::FocusableInputHandler::Ptr >& inputHandler)
{
  if(this->inputHandler() != inputHandler)
  {
    this->_inputHandler = inputHandler;

    (*inputHandler)->setSensitive(this->sensitive());

    invalidateMouseOverWidget();
    invalidateKeyFocusedWidget();
  }
}

typedef TrackingPtr<Widget*> TrackWidgetPtr;

class Widget::InputWidget_NextParentInHierarchy : public InputHandlers::DelegatingInputHandler
{
public:
  typedef std::shared_ptr<InputWidget_NextParentInHierarchy> Ptr;

public:
  const TrackWidgetPtr widget;

  InputWidget_NextParentInHierarchy(const TrackWidgetPtr& widget)
    : widget(widget)
  {
  }

  InputHandler::Ptr inputHandlerDelegatedTo() override
  {
    Optional<Widget*> optionalWidget = widget.value();

    if(optionalWidget)
    {
      // Don't use the widget itself, otherwise we are entering a stack-overflow, as the widget will
      // return this input-handler

      optionalWidget = (*optionalWidget)->parent();

      while(optionalWidget)
      {
        Widget* w = *optionalWidget;

        assert(w);

        Optional<InputHandler::Ptr> inputHandler = w->inputHandler();

        if(inputHandler)
          return *inputHandler;

        optionalWidget = w->parent();
      }
    }

    return InputHandler::fallbackInputHandler();
  }

  static Ptr create(const TrackWidgetPtr& widget)
  {
    return Ptr(new InputWidget_NextParentInHierarchy(widget));
  }
};

class Widget::InputWidget_DrawStateChanges : public InputHandlers::FallbackInputHandler
{
public:
  typedef std::shared_ptr<InputWidget_DrawStateChanges> Ptr;

public:
  const TrackWidgetPtr widget;

  InputWidget_DrawStateChanges(const TrackWidgetPtr& widget)
    : widget(widget)
  {
  }

  bool handleMouseEnter() override
  {
    Optional<Widget*> optionalWidget = widget.value();
    if(optionalWidget)
    {
      Widget& widget = **optionalWidget;

      widget.animationMap.setCurrentUnanimatedValue(AnimationSlots::RenderState::mouseHover, 1.f);
      widget.playAnimation({Tags::Animations::mouseEnter});
    }
    return false;
  }

  bool handleMouseLeave() override
  {
    Optional<Widget*> optionalWidget = widget.value();
    if(optionalWidget)
    {
      Widget& widget = **optionalWidget;

      widget.animationMap.setCurrentUnanimatedValue(AnimationSlots::RenderState::mouseHover, 0.f);
      widget.playAnimation({Tags::Animations::mouseLeave});
    }
    return false;
  }

  bool handleKeyFocusEnter() override
  {
    Optional<Widget*> optionalWidget = widget.value();
    if(optionalWidget)
    {
      Widget& widget = **optionalWidget;

      widget.animationMap.setCurrentUnanimatedValue(AnimationSlots::RenderState::keyFocused, 1.f);
      widget.playAnimation({Tags::Animations::keyFocusEnter});
    }
    return false;
  }

  bool handleKeyFocusLeave() override
  {
    Optional<Widget*> optionalWidget = widget.value();
    if(optionalWidget)
    {
      Widget& widget = **optionalWidget;

      widget.animationMap.setCurrentUnanimatedValue(AnimationSlots::RenderState::keyFocused, 0.f);
      widget.playAnimation({Tags::Animations::keyFocusLeave});
    }
    return false;
  }

  static Ptr create(const TrackWidgetPtr& widget)
  {
    return Ptr(new InputWidget_DrawStateChanges(widget));
  }
};

class Widget::InputWidget_UpdateFocus : public InputHandlers::FallbackInputHandler
{
public:
  typedef std::shared_ptr<InputWidget_UpdateFocus> Ptr;

public:
  const TrackWidgetPtr widget;

  InputWidget_UpdateFocus(const TrackWidgetPtr& widget)
    : widget(widget)
  {
  }

  bool handleKeyFocusEnter() override
  {
    Optional<Widget*> optionalWidget = widget.value();
    if(optionalWidget)
    {
      Widget& widget = **optionalWidget;

      widget._tagAsKeyFocused();
    }

    return false;
  }

  bool handleKeyFocusLeave() override
  {
    Optional<Widget*> optionalWidget = widget.value();
    if(optionalWidget)
    {
      Widget& widget = **optionalWidget;

      widget._untagAsKeyFocused();
    }

    return false;
  }

  static Ptr create(const TrackWidgetPtr& widget)
  {
    return Ptr(new InputWidget_UpdateFocus(widget));
  }

private:
  Optional<Window*> optionalWindow()
  {
    Optional<Widget*> optionalWidget = widget.value();

    if(!optionalWidget)
      return nothing;

    Widget& widget = **optionalWidget;

    return widget.window();
  }
};

void Widget::setInputHandlerWithWrapper(InputHandler::Ptr inputHandler, InputHandlerWrapperFlags flags)
{
  if((flags & InputHandlerWrapperFlags::CALL_PARENTS_AS_FALLBACK) != InputHandlerWrapperFlags::NONE)
    inputHandler = InputHandlers::BranchOffInputHandler::create(inputHandler,
                                                                InputWidget_NextParentInHierarchy::create(TrackWidgetPtr(this, this->trackable)));
  if((flags & InputHandlerWrapperFlags::DRAW_STATE_CHANGE) != InputHandlerWrapperFlags::NONE)
    inputHandler = InputHandlers::BranchOffInputHandler::create(InputWidget_DrawStateChanges::create(TrackWidgetPtr(this, this->trackable)),
                                                                inputHandler);
  if((flags & InputHandlerWrapperFlags::UPDATE_KEY_FOCUS) != InputHandlerWrapperFlags::NONE)
    inputHandler = InputHandlers::BranchOffInputHandler::create(InputWidget_UpdateFocus::create(TrackWidgetPtr(this, this->trackable)),
                                                                inputHandler);

  setInputHandler(InputHandlers::FocusableInputHandler::create(inputHandler));
}

const vec2& Widget::minimumSize() const
{
  return _minimumSize;
}

const vec2& Widget::optimalSize() const
{
  return _optimalSize;
}

void Widget::setMinimumSize(const vec2& minimumSize)
{
  if(this->_minimumSize != minimumSize)
  {
    _minimumSize = minimumSize;
    _signalMinimumSizeChanged();
  }
}

void Widget::setOptimalSize(const vec2& optimalSize)
{
  if(this->_optimalSize != optimalSize)
  {
    _optimalSize = optimalSize;
    _signalOptimalSizeChanged();
  }
}

const Theme::Ptr& Widget::theme()
{
  return _theme;
}


void Widget::playAnimation(TagSet animationTags)
{
  Theme::RenderData currentState;
  bool recursive = false;

  this->sheduleRedraw();
  this->getRenderData(out(currentState));

  theme()->playAnimation(inout(animationTags), this, out(recursive));

  if(recursive)
  {
    visitWholeHierarchy([&animationTags, this](const Widget::Ptr& widget) -> bool {
      widget->playAnimation(animationTags);
      return false;
    }, 1, 1);
  }
}

DirtyFlags Widget::dirtyFlags() const
{
  return _dirtyFlags;
}

bool Widget::dirtyFlagsChecked(DirtyFlags dirtyFlags) const
{
  return dirtyFlags == (dirtyFlags & this->_dirtyFlags);
}

void Widget::markZIndexDirty()
{
  sheduleReorderRenderOrder();
  _markDirty(DirtyFlags::Z_INDEX | DirtyFlags::SUBTREE_INDEX);
  _markChildrenDirty(DirtyFlags::Z_INDEX | DirtyFlags::SUBTREE_INDEX);
  _markParentsDirty(DirtyFlags::SUBTREE_INDEX);
}

void Widget::markThemeDirty()
{
  _markDirty(DirtyFlags::THEME);
}

void Widget::markMinimumSizeDirty()
{
  _markDirty(DirtyFlags::MINIMUM_SIZE);
  _markParentsDirty(DirtyFlags::MINIMUM_SIZE);
}

void Widget::markOptimalSizeDirty()
{
  _markDirty(DirtyFlags::OPTIMAL_SIZE);
  _markParentsDirty(DirtyFlags::OPTIMAL_SIZE);
}

void Widget::markParentMinimumSizeDirty()
{
  _markParentsDirty(DirtyFlags::MINIMUM_SIZE);
}

void Widget::markParentOptimalSizeDirty()
{
  _markParentsDirty(DirtyFlags::OPTIMAL_SIZE);
}

void Widget::markChildAllocationDirty()
{
  _markDirty(DirtyFlags::CHILD_ALLOCATION);
  _markChildrenDirty(DirtyFlags::CHILD_ALLOCATION);
}

void Widget::markChildZPositionDirty()
{
  sheduleReorderRenderOrder();
  _markDirty(DirtyFlags::CHILD_Z_POSITION);
  _markChildrenDirty(DirtyFlags::CHILD_Z_POSITION);
}

void Widget::_markDirty(DirtyFlags dirtyFlags)
{
  if(this->visible())
  {
    _dirtyFlags |= dirtyFlags;
    if(window())
      (*window())->widgetMarkedDirty(dirtyFlags);
  }
}

void Widget::_markParentsDirty(DirtyFlags dirtyFlags)
{
  if(!this->visible())
    return;

  Optional<Container*> optionalContainer = parent();

  while(optionalContainer)
  {
    Container* c = *optionalContainer;

    if(c->dirtyFlagsChecked(dirtyFlags))
      return;
    c->_markDirty(dirtyFlags);

    optionalContainer = c->parent();
  }
}

void Widget::_markChildrenDirty(DirtyFlags dirtyFlags)
{
  (void)dirtyFlags;
}

void Widget::_clearDirtyFlags(DirtyFlags dirtyFlags)
{
  this->_dirtyFlags = this->_dirtyFlags & (~dirtyFlags);
}


void Widget::markAppereanceDirty()
{
  markMinimumSizeDirty();
  markOptimalSizeDirty();
  sheduleRedraw();
}


bool Widget::visitWholeHierarchy(const std::function<bool(const Widget::Ptr& widget)>& handler, int minDepth, int maxDepth)
{
  if(minDepth>0 || maxDepth<0)
    return false;
  return handler(shared_from_this());
}


void Widget::getRenderData(const Output<Theme::RenderData>& renderData) const
{
  renderData.value.allocation = this->boundingRect();
  renderData.value.zPosition = this->zPosition() + animationMap.value(AnimationSlots::RenderState::zPositionOffset);
  renderData.value.tags = this->rtti.tags;

  renderData.value.state.customStates[AnimationSlots::RenderState::activated] = animationMap.value(AnimationSlots::RenderState::activated);
  renderData.value.state.alpha = animationMap.value(AnimationSlots::RenderState::alpha);
  renderData.value.state.sensitive = animationMap.value(AnimationSlots::RenderState::sensitive);
  renderData.value.state.keyFocused = animationMap.value(AnimationSlots::RenderState::keyFocused);
  renderData.value.state.mouseStates[Theme::State::MouseState::PRESSED] = animationMap.value(AnimationSlots::RenderState::mousePressed);
  renderData.value.state.mouseStates[Theme::State::MouseState::HOVER] = animationMap.value(AnimationSlots::RenderState::mouseHover);

  Optional<real> animatedXAllocationOffset= animationMap.optionalValue(AnimationSlots::RenderState::xAllocationOffset);
  Optional<real> animatedYAllocationOffset= animationMap.optionalValue(AnimationSlots::RenderState::yAllocationOffset);

  if(animatedXAllocationOffset || animatedYAllocationOffset)
  {
    vec2 offset(0);

    if(animatedXAllocationOffset)
      offset.x = *animatedXAllocationOffset;
    if(animatedXAllocationOffset)
      offset.y = *animatedYAllocationOffset;

    renderData.value.allocation.move(offset);
  }
}


std::shared_ptr<Widget> Widget::shared_from_this()
{
  return std::static_pointer_cast<Widget>(Object::shared_from_this());
}

std::shared_ptr<const Widget> Widget::shared_from_this() const
{
  return std::static_pointer_cast<const Widget>(Object::shared_from_this());
}


bool Widget::isWidgetAtPoint(const vec2& point) const
{
  if(!this->boundingRect().contains(point))
    return false;

  if(this->scissor() && !this->scissor()->contains(point))
    return false;

  return true;
}

vec2 Widget::currentMousePosition(bool localCoordinate) const
{
  Optional<const Window*> optionalWindow = window();

  vec2 coordinate(0);

  if(optionalWindow)
  {
    const Window* window = *optionalWindow;

    coordinate = window->currentMousePosition(this->zPosition());
  }

  if(localCoordinate)
    coordinate -= boundingRect().min();

  return coordinate;
}


void Widget::sheduleRedraw()
{
  if(this->visible())
  {
    Optional<Window*> w = window();
    if(w)
      w.value()->sheduleRedraw();
  }
}

void Widget::sheduleRedrawRecursive()
{
  sheduleRedraw();
}

void Widget::sheduleReorderRenderOrder()
{
  Optional<Window*> w = window();
  if(w)
    w.value()->sheduleReorderRenderOrder();
}

void Widget::invalidateMouseOverWidget()
{
  Optional<Window*> w = window();
  if(w)
    w.value()->invalidateMouseOverWidget();
}

void Widget::invalidateKeyFocusedWidget()
{
  Optional<Window*> w = window();
  if(w)
    w.value()->invalidateKeyFocusedWidget();
}

void Widget::onAddedToWindow()
{
  markThemeDirty();
}

void Widget::onRemovedFromWindow()
{
  _untagAsKeyFocused();
}

void Widget::onAddedToContainer()
{
  assert(this->parent());

  Container* parent = *this->parent();

  _updateParentVisibility();
  _parentConnections.insert(parent->signalVisibilityChanged().connect(std::bind(&Widget::_updateParentVisibility, this)).track(this->trackable));
  _parentConnections.insert(parent->signalSensitivityChanged().connect(std::bind(&Widget::_updateParentSensitivity, this)).track(this->trackable));

  markZIndexDirty();
  markThemeDirty();
}

void Widget::onAboutToBeRemovedFromContainer()
{
  for(Signals::Connection c : _parentConnections)
    c.disconnect();
  _parentConnections.clear();
  _updateParentVisibility();
}

void Widget::updateIndices(uint16 zOrder)
{
  if(!this->dirtyFlagsChecked(DirtyFlags::Z_INDEX))
    assert(this->index().zOrder() == zOrder);

  this->_index.setZOrder(zOrder);
}

void Widget::updateTheme()
{
  if(!dirtyFlagsChecked(DirtyFlags::THEME))
    return;

  Theme::Ptr theme = _theme;

  if(parent())
  {
    Container* p = *parent();
    theme = p->theme()->childTheme(this->rtti.tags);
  }else if(window() && !parent())
  {
    Window* w = *window();
    assert(w->rootWidget().get() == this);

    theme = w->appearance()->rootTheme(this->rtti.tags);
  }

  if(this->_theme != theme)
  {
    this->_theme = theme;

    markMinimumSizeDirty();
    markOptimalSizeDirty();
    onThemeChanged();
  }
}

void Widget::updateScissor()
{
  if(!this->parent())
    return;

  Container* parent = *this->parent();

  if(!this->dirtyFlagsChecked(DirtyFlags::SCISSOR))
  {
    assert(parent->finalScissorForChildren() == this->scissor());
    return;
  }

  this->_scissor = parent->finalScissorForChildren();
}

void Widget::updateMinimumSize()
{
  if(dirtyFlagsChecked(DirtyFlags::MINIMUM_SIZE))
    onUpdateMinimumSize();
}

void Widget::updateOptimalSize()
{
  if(dirtyFlagsChecked(DirtyFlags::MINIMUM_SIZE))
    onUpdateOptimalSize();
}

void Widget::updateChildAllocation()
{
  if(dirtyFlagsChecked(DirtyFlags::CHILD_ALLOCATION))
    onUpdateChildAllocation();
}

void Widget::updateChildZPosition()
{
  if(dirtyFlagsChecked(DirtyFlags::CHILD_Z_POSITION))
    onUpdateChildZPosition();
}

void Widget::onMoved()
{
  invalidateMouseOverWidget();

  _signalMoved();
}

void Widget::onMovedAlongZ()
{
  invalidateMouseOverWidget();

  this->sheduleReorderRenderOrder();
  _signalMovedAlongZ();
}

void Widget::onThemeChanged()
{
}

void Widget::onUpdateMinimumSize()
{
}

void Widget::onUpdateOptimalSize()
{
}

void Widget::onUpdateChildAllocation()
{
}

void Widget::onUpdateChildZPosition()
{
}

Signals::Signal<void()>& Widget::signalMoved()
{
  return _signalMoved;
}

Signals::Signal<void()>& Widget::signalMovedAlongZ()
{
  return _signalMovedAlongZ;
}

Signals::Signal<void()>& Widget::signalMinimumSizeChanged()
{
  return _signalMinimumSizeChanged;
}

Signals::Signal<void()>& Widget::signalOptimalSizeChanged()
{
  return _signalOptimalSizeChanged;
}

Signals::Signal<void()>& Widget::signalVisibilityChanged()
{
  return _signalVisibilityChanged;
}

Signals::Signal<void()>& Widget::signalSensitivityChanged()
{
  return _signalSensitivityChanged;
}


// ========


Widget::Index::Index()
  : Index(0, 0xffff)
{
}

Widget::Index::Index(uint16 zOrder, uint16 layoutOrder)
  : renderOrder(0)
{
  setZOrder(zOrder);
  setLayoutOrder(layoutOrder);
}

uint16 Widget::Index::zOrder() const
{
  return uint16(renderOrder >> 16);
}

uint16 Widget::Index::layoutOrder() const
{
  return uint16(renderOrder & 0xffff);
}

void Widget::Index::setZOrder(uint16 order)
{
  renderOrder = (uint32(order) << 16) | (renderOrder & 0x0000ffff);
}

void Widget::Index::setLayoutOrder(uint16 order)
{
  renderOrder = (renderOrder & 0xffff0000) | uint32(order);
}


bool Widget::Index::operator<(const Index& other) const
{
  return this->renderOrder < other.renderOrder;
}

bool Widget::Index::operator>(const Index& other) const
{
  return this->renderOrder > other.renderOrder;
}

bool Widget::Index::operator<=(const Index& other) const
{
  return this->renderOrder <= other.renderOrder;
}

bool Widget::Index::operator>=(const Index& other) const
{
  return this->renderOrder >= other.renderOrder;
}

bool Widget::Index::operator==(const Index& other) const
{
  return this->renderOrder == other.renderOrder;
}

bool Widget::Index::operator!=(const Index& other) const
{
  return this->renderOrder != other.renderOrder;
}



bool Widget::widgetOrderLessThan(const Widget::Ptr& a, const Widget::Ptr& b)
{
  real az = a->zPosition();
  real bz = b->zPosition();
  if(az < bz)
    return true;
  if(az > bz)
    return false;

  return a->index() < b->index();
}


Optional<Widget::Ptr> Widget::asSmartPtr()
{
  if(this->parent())
  {
    Container* parent = *this->parent();

    for(const Widget::Ptr& w : parent->allChildren())
    {
      if(w.get() == this)
        return w;
    }
  }

  if(this->window())
  {
    Window* window = *this->window();

    if(window->rootWidget().get() == this)
      return window->rootWidget();
  }

  return nothing;
}


void Widget::sortWidgets(const InOutput< QVector<Widget::Ptr> >& widgets)
{
  qStableSort(widgets.value.begin(),
              widgets.value.end(),
              Widget::widgetOrderLessThan);

}

QVector<Widget::Ptr> Widget::sortSetIntoVector(const QSet<Widget::Ptr>& widgetSet)
{
  QVector<Widget::Ptr> sortedWidgets;
  sortedWidgets.reserve(widgetSet.size());
  for(const Widget::Ptr& w : widgetSet)
  {
    if(w->visible())
      sortedWidgets.append(w);
  }

  sortWidgets(inout(sortedWidgets));

  return sortedWidgets;
}


} // namespace Gui
} // namespace Framework
