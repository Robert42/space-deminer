#ifndef FRAMEWORK_GUI_CONTAINER_H
#define FRAMEWORK_GUI_CONTAINER_H

#include "widget.h"

namespace Framework {
namespace Gui {

class Layout;
typedef std::shared_ptr<Layout> LayoutPtr;

class Container : public Widget
{
  friend class WidgetModel;
public:
  typedef std::shared_ptr<Container> Ptr;
  typedef std::weak_ptr<Container> WeakPtr;

private:
  LayoutPtr _rootLayout;
  Optional< Rectangle<vec2> > _scissorForChildren;

  Signals::Trackable trackLayout;

public:
  Container();
  Container(const LayoutPtr& rootLayout);
  virtual ~Container();

public:
  const LayoutPtr& rootLayout();
  QSet<Widget::Ptr> allChildren();

  void markChildLayoutIndexDirty();
  void markChildScissorDirty();

  Optional< Rectangle<vec2> > finalScissorForChildren() const;
  const Optional< Rectangle<vec2> >& scissorForChildren() const;
  void setScissorForChildren(const Optional< Rectangle<vec2> >& scissorForChildren);

  /**
   * @brief Removes a child widget from this container.
   *
   * @note This function doesn't go through the whole widget tree.
   *       Only the direct children of this container can be removed with this function
   *
   * @param widget
   * @return true, if removing the widget was successful. False, if the given widget
   *         is not a direct child of this container
   */
  bool removeChildWidget(const Widget::Ptr& widget);
  /**
   * @brief Removes a child widget from this container.
   *
   * @note This function doesn't go through the whole widget tree.
   *       Only the direct children of this container can be removed with this function
   *
   * @param widget
   * @return true, if removing the widget was successful. False, if the given widget
   *         is not a direct child of this container
   */
  bool removeChildWidget(const Widget* widget);
  /**
   * @brief Removes a child layout from this container.
   *
   * @note This function doesn't go through the whole widget tree.
   *       Only the layouts belonging to the rootLayout of this container can be removed
   *       with this function
   *
   * @param layout
   * @return true, if removing the layout was successful. False, if the given layout
   *         doesn't belong to this container
   */
  bool removeChildLayout(const LayoutPtr& layout);
  /**
   * @brief Removes a child layout from this container.
   *
   * @note This function doesn't go through the whole widget tree.
   *       Only the layouts belonging to the rootLayout of this container can be removed
   *       with this function
   *
   * @param layout
   * @return true, if removing the layout was successful. False, if the given layout
   *         doesn't belong to this container
   */
  bool removeChildLayout(const Layout* layout);

  void sheduleRedrawRecursive() final override;

  bool visitWholeHierarchy(const std::function<bool(const Widget::Ptr& widget)>& visitor, int minDepth=0, int maxDepth=std::numeric_limits<int>::max()) final override;

protected:
  void setRootLayout(const LayoutPtr& layout);
  void unsetRootLayout();

  void onMoved() override;
  void onMovedAlongZ() override;
  void onAddedToWindow() override;
  void onRemovedFromWindow() override;
  void updateIndices(uint16 zOrder) final override;
  void onThemeChanged() override;
  void onUpdateMinimumSize() override;
  void onUpdateOptimalSize() override;
  void onUpdateChildAllocation() final override;
  void onUpdateChildZPosition() final override;

  virtual void onChildAdded();
  virtual void onChildAboutToBeRemoved();

  virtual void onLayoutMinimumSizeChanged();
  virtual void onLayoutOptimalSizeChanged();

  void _markChildrenDirty(DirtyFlags dirtyFlags) final override;
};

RTTI_CLASS_DECLARE(Framework::Gui::Container)

} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_CONTAINER_H
