#include "label.h"

#include "../types.h"

#include <base/base-application.h>

namespace Framework {
namespace Gui {
namespace Widgets {


Label::Label(const Translation& label)
  : _label(label)
{
  RTTI_CLASS_SET_TYPE(Framework::Gui::Widgets::Label);

  _simpleTextWidget = Theme::SimpleTextWidget::createFallback();

  BaseApplication::signalLanguageChanged().connect(std::bind(&Widget::markAppereanceDirty, this)).track(this->trackable);
}

Label::Ptr Label::create(const Translation& label)
{
  return Ptr(new Label(label));
}


const Translation& Label::label() const
{
  return _label;
}

void Label::setLabel(const Translation& label)
{
  if(this->label() != label)
  {
    this->_label = label;
    markAppereanceDirty();
  }
}

void Label::onUpdateMinimumSize()
{
  setMinimumSize(_simpleTextWidget->minimumSize(this->label().translation()));
}


void Label::onUpdateOptimalSize()
{
  setOptimalSize(_simpleTextWidget->minimumSize(this->label().translation()));
}


void Label::onThemeChanged()
{
  _simpleTextWidget = this->theme()->simpleTextWidgetEnforced(this);

  markMinimumSizeDirty();
  markOptimalSizeDirty();
}

void Label::draw(Painter& painter)
{
  _simpleTextWidget->draw(this->label().translation(),
                          painter,
                          this);
}


} // namespace Widgets
} // namespace Gui
} // namespace Framework
