#ifndef FRAMEWORK_GUI_WIDGETS_CANVAS_H
#define FRAMEWORK_GUI_WIDGETS_CANVAS_H

#include "../widget.h"
#include "../simple-widget-template.h"
#include "../tags.h"

namespace Framework {
namespace Gui {
namespace Widgets {


class Canvas : public SimpleWidgetTemplate<Widget>
{
public:
  typedef std::shared_ptr<Canvas> Ptr;
  typedef SimpleWidgetTemplate<Widget> ParentClass;

  typedef std::function<void(Painter& painter,const Rectangle<vec2>&)> DrawFunction;

  BEGIN_ENUMERATION(SizeRequest,)
    NONE,
    TEXT_SIZE,
    SMALL_VIEW,
    LARGE_EDITOR
  ENUMERATION_BASIC_OPERATORS(SizeRequest)
  END_ENUMERATION;

public:
  Canvas(const SizeRequest& sizeRequest = SizeRequest::SMALL_VIEW);

  static Ptr create(const DrawFunction& drawFunction,
                    const SizeRequest& sizeRequest = SizeRequest::SMALL_VIEW);

  void draw(Painter& painter) override;
};

RTTI_CLASS_DECLARE(Framework::Gui::Widgets::Canvas)


} // namespace Widgets
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_WIDGETS_CANVAS_H
