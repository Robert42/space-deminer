#include "canvas.h"
#include "../types.h"
#include "../tags.h"

namespace Framework {
namespace Gui {
namespace Widgets {


Canvas::Canvas(const SizeRequest& sizeRequest)
{
  RTTI_CLASS_SET_TYPE(Framework::Gui::Widgets::Canvas);

  switch(sizeRequest.value)
  {
  case SizeRequest::TEXT_SIZE:
    rtti.tags.insert(Tags::Canvas::textSize);
    break;
  case SizeRequest::SMALL_VIEW:
    rtti.tags.insert(Tags::Canvas::smallView);
    break;
  case SizeRequest::LARGE_EDITOR:
    rtti.tags.insert(Tags::Canvas::largeEditor);
    break;
  default:
    break;
  }
}

void Canvas::draw(Painter& painter)
{
  ParentClass::draw(painter);
}


class FunctionalCanvas : public Canvas
{
public:
  typedef Canvas ParentClass;

  const DrawFunction drawFunction;

  FunctionalCanvas(const DrawFunction& drawFunction,
                   const SizeRequest& sizeRequest)
    : Canvas(sizeRequest),
      drawFunction(drawFunction)
  {
  }

  void draw(Painter& painter) override
  {
    ParentClass::draw(painter);

    drawFunction(painter, this->boundingRect());
  }
};


Canvas::Ptr Canvas::create(const DrawFunction& drawFunction,
                           const SizeRequest& sizeRequest)
{
  return Ptr(new FunctionalCanvas(drawFunction,
                                  sizeRequest));
}



} // namespace Widgets
} // namespace Gui
} // namespace Framework
