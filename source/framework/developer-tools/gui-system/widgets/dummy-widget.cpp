#include "dummy-widget.h"

namespace Framework {
namespace Gui {
namespace Widgets {

DummyWidget::DummyWidget(const vec4& color)
  : _color(color)
{
  RTTI_CLASS_SET_TYPE(Framework::Gui::Widgets::DummyWidget);

  this->setMinimumSize(vec2(22));
  this->setOptimalSize(vec2(48));
}


DummyWidget::Ptr DummyWidget::create(const vec4& color)
{
  return Ptr(new DummyWidget(color));
}


Signals::Signal<void(const vec4&)>& DummyWidget::signalColorChanged()
{
  return _signalColorChanged;
}


void DummyWidget::draw(Painter& painter)
{
  painter.setZPosition(this->zPosition());
  painter.drawFilledRectangle(this->boundingRect(),
                              this->color());
}


const vec4& DummyWidget::color()
{
  return this->_color;
}

void DummyWidget::setColor(const vec4& color)
{
  if(this->color() != color)
  {
    this->_color = color;
    this->_signalColorChanged(this->color());
    this->sheduleRedraw();
  }
}


} // namespace Widget
} // namespace Gui
} // namespace Framework
