#ifndef FRAMEWORK_GUI_WIDGETS_LABEL_H
#define FRAMEWORK_GUI_WIDGETS_LABEL_H

#include "../widget.h"

#include <base/i18n/translation.h>

namespace Framework {
namespace Gui {
namespace Widgets {

class Label : public Widget
{
public:
  typedef std::shared_ptr<Label> Ptr;
  typedef std::weak_ptr<Label> WeakPtr;

public:
  Signals::Trackable trackable;

private:
  Translation _label;
  Theme::SimpleTextWidget::Ptr _simpleTextWidget;

public:
  Label(const Translation& label=Translation());

  static Ptr create(const Translation& label=Translation());

public:
  const Translation& label() const;
  void setLabel(const Translation& label);

protected:
  void onUpdateMinimumSize() override;
  void onUpdateOptimalSize() override;
  void onThemeChanged() override;
  void draw(Painter& painter) override;

};

RTTI_CLASS_DECLARE(Framework::Gui::Widgets::Label)

} // namespace Widgets
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_WIDGETS_LABEL_H
