#ifndef FRAMEWORK_GUI_WIDGETS_DUMMYWIDGET_H
#define FRAMEWORK_GUI_WIDGETS_DUMMYWIDGET_H

#include "../widget.h"

namespace Framework {
namespace Gui {
namespace Widgets {

class DummyWidget final : public Widget
{
public:
  typedef std::shared_ptr<DummyWidget> Ptr;

private:
  vec4 _color;

  Signals::CallableSignal<void(const vec4&)> _signalColorChanged;

protected:
  DummyWidget(const vec4& color = vec4(1.f, 0.5f, 0.f, 1.f));

public:
  static Ptr create(const vec4& color = vec4(1.f, 0.5f, 0.f, 1.f));

public:
  const vec4& color();
  void setColor(const vec4& color);

  Signals::Signal<void(const vec4&)>& signalColorChanged();

public:
  void draw(Painter& painter) override;
};

RTTI_CLASS_DECLARE(Framework::Gui::Widgets::DummyWidget)

} // namespace Widgets
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_DUMMYWIDGET_H
