#include "painter.h"
#include "renderer.h"

namespace Framework {
namespace Gui {

Painter::Painter()
  : _hiDpiFactor(1.f)
{
}

Painter::~Painter()
{
}

Painter::Ptr Painter::fallbackPainter()
{
  return Renderer::fallbackRenderer();
}

real Painter::hiDpiFactor() const
{
  return _hiDpiFactor;
}

// ==== LinePen ========


Painter::LinePen::LinePen()
{
}

Painter::LinePen::LinePen(const Implementation::Ptr& implementation)
  : implementation(implementation)
{
  this->implementation.track(implementation->painter.trackable);
}


void Painter::LinePen::setColor(const vec4& color)
{
  this->implementation.value->setColor(color);
}

void Painter::LinePen::setThickness(real thickness)
{
  this->implementation.value->setThickness(thickness);
}

void Painter::LinePen::setDashLength(Optional<real> dashLength)
{
  this->implementation.value->setDashLength(dashLength);
}

void Painter::LinePen::beginStroke(bool loop)
{
  this->implementation.value->beginStroke(loop);
}

void Painter::LinePen::drawVertex(const vec2& point)
{
  this->implementation.value->drawVertex(point);
}

void Painter::LinePen::endStroke()
{
  this->implementation.value->endStroke();
}


// ==== LinePen::Implementation ========


Painter::LinePen::Implementation::Implementation(Painter& painter)
  : painter(painter)
{
}


Painter::LinePen::Implementation::Ptr Painter::LinePen::Implementation::createFallback()
{
  class FallbackImplementation final : public Painter::LinePen::Implementation
  {
  public:
    const Painter::Ptr fallbackPainter;

    FallbackImplementation(const Painter::Ptr& fallbackPainter)
      : Implementation(*fallbackPainter),
        fallbackPainter(fallbackPainter)
    {
    }

    void setColor(const vec4&) final override {}
    void setThickness(real) final override {}

    void setDashLength(Optional<real>) final override {}

    void beginStroke(bool) final override {}
    void drawVertex(const vec2&) override {}
    void endStroke() final override {}
  };

  static Ptr fallback(new FallbackImplementation(Painter::fallbackPainter()));

  return fallback;
}

Painter::LinePen::Implementation::~Implementation()
{
}


} // namespace Gui
} // namespace Framework
