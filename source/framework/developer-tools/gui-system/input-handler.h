#ifndef FRAMEWORK_GUI_INPUTHANDLER_H
#define FRAMEWORK_GUI_INPUTHANDLER_H

#include <framework/window/user-input/keyboard.h>
#include <framework/window/user-input/mouse.h>

namespace Framework {
namespace Gui {


/** An interface for all input handlers used by the gui system.
 */
class InputHandler : public noncopyable
{
public:
  typedef std::shared_ptr<InputHandler> Ptr;

protected:
  InputHandler();
  virtual ~InputHandler();

public:
  static Ptr fallbackInputHandler();

public:
  virtual bool handleKeyPressed(KeyCode keyCode) = 0;
  virtual bool handleKeyReleased(KeyCode keyCode) = 0;
  virtual bool handleUnicodeEvent(String::value_type unicodeValue) = 0;
  virtual bool handleMouseButtonPressed(MouseButton button) = 0;
  virtual bool handleMouseButtonReleased(MouseButton button) = 0;
  virtual bool handleMouseWheel(real wheelMovement) = 0;
  virtual bool handleMouseMove() = 0;
  virtual bool handleMouseEnter() = 0;
  virtual bool handleMouseLeave() = 0;
  virtual bool handleKeyFocusEnter() = 0;
  virtual bool handleKeyFocusLeave() = 0;
};

} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_INPUTHANDLER_H
