#ifndef FRAMEWORK_GUI_METRICS_H
#define FRAMEWORK_GUI_METRICS_H

#include "types.h"

namespace Framework {
namespace Gui {

class Widget;
struct Metrics
{
public:
  struct Padding
  {
    real top, left, bottom, right;

    Padding(real w=0.f);

    vec2 totalWidth() const;

    bool operator==(const Padding& other) const;
    bool operator!=(const Padding& other) const;
  };

public:
  Metrics();
  virtual ~Metrics();

  static const Metrics* fallback();

  real spacing;
  real zOffset;
  Padding margin;
  Padding padding;
  vec2 alignment;
  vec2 childExpansion;

  vec2 optimalSizeHint;
  vec2 minimumSizeHint;

  Optional<real> widthLimit;
  Optional<real> heightLimit;
};


} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_METRICS_H
