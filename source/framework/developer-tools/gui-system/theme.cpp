#include "theme.h"
#include "widget.h"
#include "layout.h"
#include "tags.h"

namespace Framework {
namespace Gui {

Theme::Theme()
{
}

Theme::~Theme()
{
}


Theme::Ptr Theme::fallbackTheme()
{
  class DummyTheme : public Theme
  {
  public:
    Optional<SimpleWidget::Ptr> simpleWidget(const ClassType&, const TagSet&) override
    {
      return nothing;
    }

    Optional<const Metrics*> layoutMetrics(const ClassType&,
                                           const ClassType&,
                                           const TagSet&,
                                           const TagSet&) override
    {
      return nothing;
    }



    Optional<const Metrics*> layoutSpacingItemMetrics(const ClassType&,
                                                      const ClassType&,
                                                      const TagSet&,
                                                      const TagSet&) override
    {
      return nothing;
    }


    Optional<SimpleTextWidget::Ptr> simpleTextWidget(const ClassType&,
                                                     const TagSet&) override
    {
      return nothing;
    }

    bool playAnimation(const InOutput<AnimationData>&) override
    {
      return false;
    }
  };

  static Theme::Ptr theme = Ptr(new DummyTheme);

  return theme;
}


Theme::Ptr Theme::childTheme(const TagSet&)
{
  return this->shared_from_this();
}

Theme::SimpleWidget::Ptr Theme::simpleWidgetEnforced(const ClassType& widgetType,
                                                     const TagSet& tags)
{
  return ClassType::calcValueForWholeHierarchy<SimpleWidget::Ptr>(widgetType,
                                                                  SimpleWidget::createFallback(),
                                                                  std::bind(&Theme::simpleWidget, this, _1, std::cref(tags)));
}

Theme::SimpleWidget::Ptr Theme::simpleWidgetEnforced(Widget* widget)
{
  return simpleWidgetEnforced(widget->rtti.type(), widget->rtti.tags);
}


Theme::SimpleTextWidget::Ptr Theme::simpleTextWidgetEnforced(const ClassType& widgetType,
                                                             const TagSet& tags)
{
  return ClassType::calcValueForWholeHierarchy<Theme::SimpleTextWidget::Ptr>(widgetType,
                                                                             SimpleTextWidget::createFallback(),
                                                                             std::bind(&Theme::simpleTextWidget, this, _1, std::cref(tags)));
}

Theme::SimpleTextWidget::Ptr Theme::simpleTextWidgetEnforced(Widget* widget)
{
  return simpleTextWidgetEnforced(widget->rtti.type(), widget->rtti.tags);
}


const Metrics* Theme::layoutMetricsEnforced(const ClassType& typeLayout,
                                            const ClassType& typeWidget,
                                            const TagSet& tagsLayout,
                                            const TagSet& tagsWidget)
{
  return ClassType::calcValueForTwoHierarchies<const Metrics*>(typeLayout,
                                                               typeWidget,
                                                               Metrics::fallback(),
                                                               std::bind(&Theme::layoutMetrics, this, _1, _2, std::cref(tagsLayout), std::cref(tagsWidget)));
}


const Metrics* Theme::layoutMetricsEnforced(const ClassType& typeLayout,
                                            const Optional<ClassType>& typeIdWidget,
                                            const TagSet& tagsLayout,
                                            const TagSet& tagsWidget)
{
  if(typeIdWidget)
  {
    return layoutMetricsEnforced(typeLayout,
                                 *typeIdWidget,
                                 tagsLayout,
                                 tagsWidget);
  }else
  {
    return Metrics::fallback();
  }
}

const Metrics* Theme::layoutMetricsEnforced(Layout* layout)
{
  if(layout->container())
  {
    Container* c = *layout->container();
    return layoutMetricsEnforced(layout->rtti.type(), c->rtti.type(), layout->rtti.tags, c->rtti.tags);
  }else
  {
    return layoutMetricsEnforced(layout->rtti.type(), nothing, layout->rtti.tags);
  }
}

const Metrics* Theme::layoutSpacingItemMetricsEnforced(const ClassType& typeLayout,
                                                       const ClassType& typeWidget,
                                                       const TagSet& tagsLayout,
                                                       const TagSet& tagsWidget)
{
  return ClassType::calcValueForTwoHierarchies<const Metrics*>(typeLayout,
                                                               typeWidget,
                                                               Metrics::fallback(),
                                                               std::bind(&Theme::layoutSpacingItemMetrics, this, _1, _2, std::cref(tagsLayout), std::cref(tagsWidget)));
}

const Metrics* Theme::layoutSpacingItemMetricsEnforced(const ClassType& typeLayout,
                                                       const Optional<ClassType>& typeWidget,
                                                       const TagSet& tagsLayout,
                                                       const TagSet& tagsWidget)
{
  if(typeWidget)
  {
    return layoutSpacingItemMetricsEnforced(typeLayout,
                                            *typeWidget,
                                            tagsLayout,
                                            tagsWidget);
  }else
  {
    return Metrics::fallback();
  }
}

const Metrics* Theme::layoutSpacingItemMetricsEnforced(Layout* layout)
{
  if(layout->container())
  {
    Container* c = *layout->container();
    return layoutSpacingItemMetricsEnforced(layout->rtti.type(), c->rtti.type(), layout->rtti.tags, c->rtti.tags);
  }else
  {
    return layoutSpacingItemMetricsEnforced(layout->rtti.type(), nothing, layout->rtti.tags);
  }
}

bool Theme::playAnimation(const InOutput<TagSet>& animationTags,
                          Widget* widget,
                          const Output<bool>& recursive)
{
  Optional<ClassType> layoutType;
  TagSet layoutTags;

  if(widget->layout())
  {
    Layout* layout = *widget->layout();

    layoutType = layout->rtti.type();
    layoutTags = layout->rtti.tags;
  }

  RenderData currentState;
  widget->getRenderData(out(currentState));

  AnimationData animationData(animationTags,
                              inout(widget->animationMap),
                              recursive,
                              widget->rtti.type(),
                              widget->rtti.tags,
                              layoutType,
                              layoutTags,
                              currentState);
  return playAnimation(inout(animationData));
}

// ====

Theme::SimpleWidget::~SimpleWidget()
{
}

Theme::SimpleWidget::Ptr Theme::SimpleWidget::createFallback(const vec2& size)
{
  class FallbackSimpleWidget : public SimpleWidget
  {
  private:
    vec2 size;

  public:
    FallbackSimpleWidget(const vec2& size)
      : size(size)
    {
    }

  public:
    void draw(Framework::Gui::Painter& painter, const Theme::RenderData& renderData) override
    {
      painter.setZPosition(renderData.zPosition);
      painter.drawFilledRectangle(renderData.allocation, vec4(1, 0, 1, 1));
    }

    bool isWidgetAtPoint(const vec2&, const Theme::RenderData&) const override
    {
      return true;
    }
  };

  static Ptr fallbackSimpleSidget = Ptr(new FallbackSimpleWidget(size));

  return fallbackSimpleSidget;
}


Optional<vec2> Theme::SimpleWidget::minimumSize() const
{
  return nothing;
}

Optional<vec2> Theme::SimpleWidget::optimalSize() const
{
  return nothing;
}


void Theme::SimpleWidget::draw(Painter& painter, const Widget* widget)
{
  RenderData renderData;

  widget->getRenderData(out(renderData));

  draw(painter, renderData);
}

bool Theme::SimpleWidget::isWidgetAtPoint(const vec2& point,
                                          const Widget* widget) const
{
  RenderData renderData;

  widget->getRenderData(out(renderData));

  return isWidgetAtPoint(point, renderData);
}


// ====

Theme::SimpleTextWidget::SimpleTextWidget()
{

}

Theme::SimpleTextWidget::~SimpleTextWidget()
{
}


Theme::SimpleTextWidget::Ptr Theme::SimpleTextWidget::createFallback()
{
  class DummySimpleTextWidget : public SimpleTextWidget
  {
    vec2 minimumSize(const String&) const override
    {
      return vec2(0);
    }
    vec2 optimalSize(const String&) const override
    {
      return vec2(0);
    }

    void draw(const Base::String&,
              Framework::Gui::Painter&,
              const Framework::Gui::Theme::RenderData&)
    {
    }
  };

  return Ptr(new DummySimpleTextWidget);
}


void Theme::SimpleTextWidget::draw(const String& text, Painter& painter, const Widget *widget)
{
  RenderData renderData;

  widget->getRenderData(out(renderData));

  draw(text, painter, renderData);
}


// ====

Theme::State::State()
  : alpha(1.f),
    sensitive(1.f),
    keyFocused(0.f),
    mouseStates{0.f, 0.f}
{
}

// ====

Theme::AnimationData::AnimationData(const InOutput<TagSet>& animationTags,
                                    const InOutput<AnimationMap>& animationMap,
                                    const Output<bool>& recursive,
                                    const ClassType& typeWidget,
                                    const TagSet& widgetTags,
                                    const Optional<ClassType>& parentLayoutType,
                                    const TagSet& parentLayoutTags,
                                    const RenderData& currentState)
  : animationTags(animationTags.value),
    animationMap(animationMap.value),
    recursive(recursive.value),
    typeWidget(typeWidget),
    widgetTags(widgetTags),
    parentLayoutType(parentLayoutType),
    parentLayoutTags(parentLayoutTags),
    currentState(currentState)
{
  this->recursive = false;
}


} // namespace Gui
} // namespace Framework
