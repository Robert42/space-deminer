#ifndef FRAMEWORK_GUI_BASELINEDRAWER_H
#define FRAMEWORK_GUI_BASELINEDRAWER_H

#include <base/geometry/circle.h>

namespace Framework {

using namespace Base;

namespace Gui {

/**
 * @note Note that the antialiasing has its limitations. For example when drastically changing the
 * thickness or trailOffset from one vertex to another, the antialiasing will be thinner than it
 * should be, as this would have made the Impementation more complicated.
 * If you want perfect antialiasing when changing the thickness extremely, consider using the
 * OutlineLineDrawer.
 */
class BaseLineDrawer
{
public:
  struct Point final
  {
    vec2 position, uv, tangentIn, tangentOut;
    vec4 color;
    real thickness, antialiasing, trailOffset;
  };

private:
  class PointList final
  {
  private:
    const BaseLineDrawer& drawer;
    int _size;
    Point _firstPoint;
    Point _secondPoint;
    Point _points[3];

  public:
    PointList(const BaseLineDrawer& drawer);

    int size() const;
    Point& operator[](int i);

    void append(const vec2& pos);
    void clear();
    bool empty() const;
  };

private:
  bool loop : 1;
  bool haveToSetFirstTangentIn : 1;
  PointList points;

public:
  vec2 uv;
  vec4 color;
  real thickness;
  real antialiasing;
  real trailOffset;
  bool rightAntialiasing, leftAntialiasing;

  BaseLineDrawer();
  virtual ~BaseLineDrawer();

  void beginLoop();
  void begin();
  void beginLoop(const vec2& firstPoint);
  void begin(const vec2& firstPoint, const vec2& tangent);
  void begin(const vec2& firstPoint);
  void drawTo(const vec2& p);
  void end();
  void endFacingLastAndFirstTangent();
  void end(const vec2& outTangent);

  void drawArcBorder(const Circle& circle,
                     real fromAngle = 0.f,
                     real toAngle = two_pi,
                     real centerAntialiasing = DEFAULT_ANTIALIASING_WIDTH,
                     bool skipLastPoint = false);
  void drawArcBorder(const Circle& circle,
                     const std::function<vec2(real)>& uvForAngle,
                     real fromAngle = 0.f,
                     real toAngle = two_pi,
                     real centerAntialiasing = DEFAULT_ANTIALIASING_WIDTH,
                     bool skipLastPoint = false);

private:
  void appendPoint(const vec2& p);

  void drawVertex(const Point& a, const Point& b);

  void drawLineStripSegment(const vec2& aTangent,
                            vec2 a,
                            vec2 b,
                            const vec2& bTangent,
                            const vec2& uvA,
                            const vec2& uvB,
                            const vec4& colorA,
                            const vec4& colorB,
                            real thicknessA,
                            real thicknessB,
                            real antialiasingA,
                            real antialiasingB,
                            real trailOffsetA = 0.f,
                            real trailOffsetB = 0.f,
                            bool rightAntialiasing = true,
                            bool leftAntialiasing = true);

protected:
  virtual void drawMainQuad(const vec2* positions, const vec4* colors, const vec2* uvs);
  virtual void drawQuad(const vec2* positions, const vec4* colors, const vec2* uvs) = 0;
  virtual void onBegin();
  virtual void onEnd();

  virtual real antialiasingFactorForPoint(const vec2& point, const vec2& antialiasingDirection);

public:
  static real _arcAntialiasing(real angle,
                               real antialiasing,
                               real centerAntialiasing);
  static void _forEachEdgeOfCircle(const Circle& circle,
                                  const std::function<void(real,real)>& function,
                                  real fromAngle=0.f,
                                  real toAngle=two_pi);
};


class _VertexBufferLineDrawer : public BaseLineDrawer
{
public:
  QVector<vec2> _outlineVerticesRight, _outlineVerticesLeft; // TOOD: refactor: this should be private

public:
  _VertexBufferLineDrawer();

public:
  const QVector<vec2>& outlineVerticesLeft() const;
  const QVector<vec2>& outlineVerticesRight() const;

protected:
  void onBegin() override;
  void onEnd() override;

  void drawMainQuad(const vec2* positions, const vec4* colors, const vec2* uvs) override;

public:
  void removeLast();
};

class VertexBufferLineDrawer final : public _VertexBufferLineDrawer
{
protected:
  void drawQuad(const vec2*, const vec4*, const vec2*) override {}
};


class OutlinedLineDrawer : public _VertexBufferLineDrawer
{
public:
  class Outline final : public BaseLineDrawer
  {
  public:
    OutlinedLineDrawer& drawer;

  public:
    Outline(OutlinedLineDrawer& drawer);

    void drawQuad(const vec2* positions, const vec4* colors, const vec2* uvs) override;
    real antialiasingFactorForPoint(const vec2& point, const vec2& antialiasingDirection) override;
  };

public:
  Outline outline;

public:
  OutlinedLineDrawer();

public:
  void drawOutline(bool innerAntialiasing=true, bool outerAntialiasing=true);
  void drawOutline(const QVector<vec2>& verticesLeft,
                   const QVector<vec2>& verticesRight,
                   bool innerAntialiasing=true,
                   bool outerAntialiasing=true,
                   bool implicitLoop=false);

private:
  void _drawOutline(const QVector<vec2>& vertices, bool implicitLoop=false);
};

} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_BASELINEDRAWER_H
