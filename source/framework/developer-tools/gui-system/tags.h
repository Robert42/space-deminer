#ifndef FRAMEWORK_GUI_TAGS_H
#define FRAMEWORK_GUI_TAGS_H

#include <base/types/type.h>

namespace Framework {

using namespace Base;

namespace Gui {
namespace Tags
{

extern Tag windowRoot;
extern Tag window_defaultWidget;
extern Tag window_defaultEscapeWidget;

extern Tag windowFrame_mainArea;
extern Tag windowFrame_windowButtonArea;

namespace Canvas
{
extern Tag textSize;
extern Tag smallView;
extern Tag largeEditor;
} // namespace Canvas

namespace Animations
{

extern Tag succeeded;
extern Tag aborted;

extern Tag show;
extern Tag hide;

extern Tag suspended;
extern Tag continued;

extern Tag key;
extern Tag leftMouseButton;
extern Tag middleMouseButton;
extern Tag rightMouseButton;

extern Tag startClick;
extern Tag endClick;
extern Tag startClickOffset;
extern Tag endClickOffset;

extern Tag mouseEnter;
extern Tag mouseLeave;
extern Tag keyFocusEnter;
extern Tag keyFocusLeave;

} // namespace Animations
} // namespace Tags

void initTags();

} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_TAGS_H
