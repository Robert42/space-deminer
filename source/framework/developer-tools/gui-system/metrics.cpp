#include "metrics.h"
#include "widget.h"

namespace Framework {
namespace Gui {

Metrics::Padding::Padding(real w)
  : top(w),
    left(w),
    bottom(w),
    right(w)
{
}

vec2 Metrics::Padding::totalWidth() const
{
  return vec2(left + right,
              top + bottom);
}

bool Metrics::Padding::operator==(const Padding& other) const
{
  return this->top==other.top
      && this->left==other.left
      && this->bottom==other.bottom
      && this->right==other.right;
}

bool Metrics::Padding::operator!=(const Padding& other) const
{
  return !(*this == other);
}


// ====


Metrics::Metrics()
  : spacing(0.f),
    zOffset(0.f),
    margin(0),
    padding(0),
    alignment(0),
    childExpansion(0),
    optimalSizeHint(0),
    minimumSizeHint(0)
{
}

Metrics::~Metrics()
{
}

const Metrics* Metrics::fallback()
{
  static Metrics fallback;

  return &fallback;
}


} // namespace Gui
} // namespace Framework
