#include "types.h"

#include <framework/developer-tools/gui-system/widget.h>
#include <framework/developer-tools/gui-system/container.h>
#include <framework/developer-tools/gui-system/layout.h>

#include <framework/developer-tools/gui-system/widgets/canvas.h>
#include <framework/developer-tools/gui-system/widgets/dummy-widget.h>
#include <framework/developer-tools/gui-system/widgets/label.h>

#include <framework/developer-tools/gui-system/containers/background.h>
#include <framework/developer-tools/gui-system/containers/bin.h>
#include <framework/developer-tools/gui-system/containers/button.h>
#include <framework/developer-tools/gui-system/containers/dummy-container.h>
#include <framework/developer-tools/gui-system/containers/window-frame.h>

#include <framework/developer-tools/gui-system/layouts/unordered-layout.h>
#include <framework/developer-tools/gui-system/layouts/box.h>
#include <framework/developer-tools/gui-system/layouts/button-box.h>

#include <framework/developer-tools/gui-system/font.h>

#include <framework/developer-tools/gui-system/ingame/gorilla-painter.h>

#include <base/types/type-manager.h>

namespace Framework {
namespace Gui {
namespace Types {

ClassType Widget;
ClassType Container;
ClassType Layout;
ClassType FontImplementation;

namespace Widgets
{
ClassType Canvas;
ClassType DummyWidget;
ClassType Label;
}

namespace Containers
{
ClassType Background;
ClassType Bin;
ClassType Button;
ClassType DummyContainer;
ClassType WindowFrame;
}

namespace Ingame
{
ClassType GorillaFont;
}

namespace Layouts
{
ClassType Alignment;
ClassType BaseBox;
ClassType BinaryLayout;
ClassType Border;
ClassType Box;
ClassType ButtonBox;
ClassType WholeArea;
ClassType LimitSize;
ClassType OrderedLayout;
ClassType StackedLayout;
ClassType UnorderedLayout;
ClassType ZOffset;
}

} // namespace Types


void registerTypes()
{
  Types::Widget = RTTI_CLASS_REGISTER_TYPE(Framework::Gui::Widget);
  Types::Container = RTTI_CLASS_REGISTER_TYPE_WITH_BASE(Framework::Gui::Container, Widget);
  Types::Layout = RTTI_CLASS_REGISTER_TYPE(Framework::Gui::Layout);
  Types::FontImplementation = RTTI_CLASS_REGISTER_TYPE(Framework::Gui::Font::Implementation);

  Types::Widgets::Canvas = RTTI_CLASS_REGISTER_TYPE_WITH_BASE(Framework::Gui::Widgets::Canvas, Widget);
  Types::Widgets::DummyWidget = RTTI_CLASS_REGISTER_TYPE_WITH_BASE(Framework::Gui::Widgets::DummyWidget, Widget);
  Types::Widgets::Label = RTTI_CLASS_REGISTER_TYPE_WITH_BASE(Framework::Gui::Widgets::Label, Widget);

  Types::Containers::Bin = RTTI_CLASS_REGISTER_TYPE_WITH_BASE(Framework::Gui::Containers::Bin, Container);
  Types::Containers::Background = RTTI_CLASS_REGISTER_TYPE_WITH_BASE(Framework::Gui::Containers::Background, Containers::Bin);
  Types::Containers::Button = RTTI_CLASS_REGISTER_TYPE_WITH_BASE(Framework::Gui::Containers::Button, Containers::Bin);
  Types::Containers::DummyContainer = RTTI_CLASS_REGISTER_TYPE_WITH_BASE(Framework::Gui::Containers::DummyContainer, Containers::Bin);
  Types::Containers::WindowFrame = RTTI_CLASS_REGISTER_TYPE_WITH_BASE(Framework::Gui::Containers::WindowFrame, Containers::Bin);

  Types::Layouts::BinaryLayout = RTTI_CLASS_REGISTER_TYPE_WITH_BASE(Framework::Gui::Layouts::BinaryLayout, Layout);
  Types::Layouts::OrderedLayout = RTTI_CLASS_REGISTER_TYPE_WITH_BASE(Framework::Gui::Layouts::OrderedLayout, Layout);
  Types::Layouts::UnorderedLayout = RTTI_CLASS_REGISTER_TYPE_WITH_BASE(Framework::Gui::Layouts::UnorderedLayout, Layout);
  Types::Layouts::Alignment = RTTI_CLASS_REGISTER_TYPE_WITH_BASE(Framework::Gui::Layouts::Alignment, Layouts::BinaryLayout);
  Types::Layouts::BaseBox = RTTI_CLASS_REGISTER_TYPE_WITH_BASE(Framework::Gui::Layouts::BaseBox, Layouts::OrderedLayout);
  Types::Layouts::Border = RTTI_CLASS_REGISTER_TYPE_WITH_BASE(Framework::Gui::Layouts::Border, Layouts::BinaryLayout);
  Types::Layouts::Box = RTTI_CLASS_REGISTER_TYPE_WITH_BASE(Framework::Gui::Layouts::Box, Layouts::BaseBox);
  Types::Layouts::ButtonBox = RTTI_CLASS_REGISTER_TYPE_WITH_BASE(Framework::Gui::Layouts::ButtonBox, Layouts::BaseBox);
  Types::Layouts::LimitSize = RTTI_CLASS_REGISTER_TYPE_WITH_BASE(Framework::Gui::Layouts::LimitSize, Layouts::BinaryLayout);
  Types::Layouts::StackedLayout = RTTI_CLASS_REGISTER_TYPE_WITH_BASE(Framework::Gui::Layouts::StackedLayout, Layouts::OrderedLayout);
  Types::Layouts::WholeArea = RTTI_CLASS_REGISTER_TYPE_WITH_BASE(Framework::Gui::Layouts::WholeArea, Layouts::BinaryLayout);
  Types::Layouts::ZOffset = RTTI_CLASS_REGISTER_TYPE_WITH_BASE(Framework::Gui::Layouts::ZOffset, Layouts::BinaryLayout);

  Types::Ingame::GorillaFont = RTTI_CLASS_REGISTER_TYPE_WITH_BASE(Framework::Gui::Ingame::GorillaPainter::FontImplementation, Font::Implementation);
}

} // namespace Gui
} // namespace Framework
