#ifndef FRAMEWORK_GUI_LAYOUT_H
#define FRAMEWORK_GUI_LAYOUT_H

#include "widgetmodel.h"
#include <base/types/class-type.h>

namespace Framework {
namespace Gui {

class Layout : public noncopyable
{
  RTTI_CLASS_MEMBER

  BEGIN_ENUMERATION(Navigation,)
    TAB_ORDER,
    TAB_ORDER_REVERSE,
    UP,
    DOWN,
    LEFT,
    RIGHT
  ENUMERATION_BASIC_OPERATORS(Navigation)
  END_ENUMERATION;

public:
  typedef std::shared_ptr<Layout> Ptr;
  typedef std::weak_ptr<Layout> WeakPtr;

  class Item;
  class WidgetItem;
  class SpacingItem;
  class SubLayoutItem;

private:
  Optional<Container*> _container;
  Optional<Layout*> _parentLayout;
  vec2 _minimumSize, _optimalSize;

  Rectangle<vec2> _allocation;
  real _z;

  Signals::CallableSignal<void()> _signalMinimumSizeChanged;
  Signals::CallableSignal<void()> _signalOptimalSizeChanged;

  QSet<std::shared_ptr<Item>> _items;

public:
  Layout();
  virtual ~Layout();

public:
  const Optional<Container*>& container() const;
  Optional<Container*> container();
  void setContainer(Container* container);
  void unsetContainer();
  void setContainer(const Optional<Container*>& container);
  Optional<Window*> window() const;
  Optional<Theme::Ptr> theme();

  const vec2& minimumSize() const;
  const vec2& optimalSize() const;

  real zPosition();
  const Rectangle<vec2>& allocation();

  QSet<Widget::Ptr> allChildWidgets();

  bool shouldRound() const;

  void markEverythingDirty();
  void markEverythingLocationRelatedDirty();
  void markThemeDirty();
  void markMinimumSizeDirty();
  void markOptimalSizeDirty();
  void markParentMinimumSizeDirty();
  void markParentOptimalSizeDirty();
  void markChildAllocationDirty();
  void markChildZPositionDirty();

  bool removeChildWidget(const Widget::Ptr& widget, bool recursive=true);
  bool removeChildWidget(const Widget* widget, bool recursive=true);
  bool removeChildLayout(const LayoutPtr& layout, bool recursive=true);
  bool removeChildLayout(const Layout* layout, bool recursive=true);

  static Optional< Widget::Ptr > navigateToNextWidget(const Widget::Ptr& widget, Navigation navigation);
  static Optional< Widget::Ptr > navigateToNextWidget(std::shared_ptr<Item> item, Navigation navigation);

private:
  friend class Container;

  void _updateMinimumSize();
  void _updateOptimalSize();
  void _updateChildAllocation();
  void _updateChildZPosition();

protected:
  virtual void updateMinimumSize() = 0;
  virtual void updateOptimalSize() = 0;
  virtual void updateChildAllocation() = 0;
  virtual void updateIndices(const InOutput<uint16>& index) = 0;
  virtual void updateChildZPosition();

  void applyMetricsFromTheme();
  virtual void onApplyMetricsFromTheme(const Theme::Ptr& theme) = 0;

  virtual Optional< std::shared_ptr<Item> > navigateToNextItem(const std::shared_ptr<Item>& item, Navigation navigation) = 0;
  virtual Optional< std::shared_ptr<Item> > navigateToFirstItem(Navigation navigation) = 0;

  virtual void onItemRemoved(const std::shared_ptr<Item>& item);

private:
  void onWindowChanged(const Optional<Window*>& window);
  void onThemeChanged();

  void setAllocation(Rectangle<vec2> allocation);
  void setZPosition(real z);

protected:
  void setMinimumSize(const vec2& minimumSize);
  void setOptimalSize(const vec2& optimalSize);

  void setLocalItemAllocation(const std::shared_ptr<Item>& item, const Rectangle<vec2>& allocation);
  void setItemAllocation(const std::shared_ptr<Item>& item, const Rectangle<vec2>& allocation);
  void setLocalItemZPosition(const std::shared_ptr<Item>& item, real z);
  void setItemZPosition(const std::shared_ptr<Item>& item, real z);
  void setItemIndex(const std::shared_ptr<Item>& item, const InOutput<uint16>& index);

  void insertItem(const std::shared_ptr<Item>& item);
  void removeItem(const std::shared_ptr<Item>& item);
  void removeAllItems();

  Optional< std::shared_ptr<WidgetItem> > findItemWithWidget(const std::shared_ptr<Widget>& widget);
  Optional< std::shared_ptr<SubLayoutItem> > findItemWithLayout(const std::shared_ptr<Layout>& layout);
  Optional< std::shared_ptr<SubLayoutItem> > findItemWithLayout(const Layout* layout);

  void _allChildWidgets(const InOutput<QSet<Widget::Ptr>>& allChildWidgets);

public:
  Signals::Signal<void()>& signalMinimumSizeChanged();
  Signals::Signal<void()>& signalOptimalSizeChanged();

  const QSet<std::shared_ptr<Item>>& items();

private:
  const Optional<Layout*>& parentLayout();
  void setParentLayout(const Optional<Layout*>& layout);

  void _removeItem(const std::shared_ptr<Item>& item);
};

RTTI_CLASS_DECLARE(Framework::Gui::Layout)


class Layout::Item
{
public:
  typedef std::shared_ptr<Item> Ptr;
  typedef std::weak_ptr<Item> WeakPtr;

  friend class Layout;

public:
  Layout& parentLayout;

private:
  real _expandPriority;

private:
  Item(Layout& parentLayout, real expandPriority=0.f);

public:
  virtual ~Item();

public:
  real expandPriority() const;
  void setExpandPriority(real expandPriority);

  virtual const vec2& minimumSize() const = 0;
  virtual const vec2& optimalSize() const = 0;

  virtual bool isWidgetItem() const;
  virtual bool isSubLayoutItem() const;

protected:
  void minimumSizeChanged();
  void optimalSizeChanged();

  virtual const Rectangle<vec2>& currentBoundingRectangle() const = 0;

  virtual void setBoundingRectangle(const Rectangle<vec2>& boundingRect) = 0;
  virtual void setZPosition(real z) = 0;
  virtual void setItemIndex(const InOutput<uint16>& index) = 0;

  virtual void onContainerChanged(const Optional<Container*>& container) = 0;
  virtual void onWindowChanged(const Optional<Window*>& window) = 0;
  virtual void onThemeChanged() = 0;

  virtual void allChildWidgets(const InOutput<QSet<Widget::Ptr>>& allChildWidgets) = 0;
};


class Layout::WidgetItem final : public Item
{
public:
  typedef std::shared_ptr<WidgetItem> Ptr;
  typedef std::weak_ptr<WidgetItem> WeakPtr;

public:
  Signals::Trackable trackable;
  const Widget::Ptr widget;
  WidgetModel::WidgetSlot widgetSlot;

public:
  WidgetItem(const Widget::Ptr& widget, Layout& parentLayout, real expandPriority=0.f);
  ~WidgetItem();

  static Ptr create(const Widget::Ptr& widget, Layout& parentLayout, real expandPriority=0.f);

public:
  const vec2& minimumSize() const override;
  const vec2& optimalSize() const override;

  bool isWidgetItem() const override;

private:
  const Rectangle<vec2>& currentBoundingRectangle() const override;

  void setBoundingRectangle(const Rectangle<vec2>& boundingRect) override;
  void setZPosition(real z) override;
  void setItemIndex(const InOutput<uint16>& index) override;

  void onContainerChanged(const Optional<Container*>& container) override;
  void onWindowChanged(const Optional<Window*>& window) override;
  void onThemeChanged() override;

  void allChildWidgets(const InOutput<QSet<Widget::Ptr>>& allChildWidgets) override;
};


class Layout::SpacingItem final : public Item
{
public:
  typedef std::shared_ptr<SpacingItem> Ptr;
  typedef std::weak_ptr<SpacingItem> WeakPtr;

private:
  vec2 _minimumSize, _optimalSize;
  Rectangle<vec2> _currentBoundingRectangle;
  bool _customSpacing;

public:
  SpacingItem(const vec2& minimumSize, const vec2& optimalSize, Layout& parentLayout, real expandPriority=0.f);
  SpacingItem(Layout& parentLayout, real expandPriority=0.f);
  ~SpacingItem();

  static Ptr create(const vec2& minimumSize, const vec2& optimalSize, Layout& parentLayout, real expandPriority=0.f);
  static Ptr create(Layout& parentLayout, real expandPriority=0.f);

public:
  const Rectangle<vec2>& currentBoundingRectangle() const override;

  const vec2& minimumSize() const override;
  const vec2& optimalSize() const override;

  void setMinimumSize(const vec2& minimumSize);
  void setOptimalSize(const vec2& optimalSize);
  void setThemeSize();

  bool customSpacing() const;

private:
  void _setMinimumSize(const vec2& minimumSize);
  void _setOptimalSize(const vec2& optimalSize);

  void setBoundingRectangle(const Rectangle<vec2>& boundingRect) override;
  void setZPosition(real z) override;
  void setItemIndex(const InOutput<uint16>& index) override;

  void onContainerChanged(const Optional<Container*>&) override;
  void onWindowChanged(const Optional<Window*>& window) override;
  void onThemeChanged() override;

  void allChildWidgets(const InOutput<QSet<Widget::Ptr>>& allChildWidgets) override;
};


class Layout::SubLayoutItem final : public Item
{
public:
  typedef std::shared_ptr<SubLayoutItem> Ptr;
  typedef std::weak_ptr<SubLayoutItem> WeakPtr;

public:
  const Layout::Ptr subLayout;
  Signals::Trackable trackable;

public:
  SubLayoutItem(const Layout::Ptr& subLayout, Layout& parentLayout, real expandPriority=0.f);
  ~SubLayoutItem();

  static Ptr create (const Layout::Ptr& subLayout, Layout& parentLayout, real expandPriority=0.f);

public:
  const vec2& minimumSize() const override;
  const vec2& optimalSize() const override;

  bool isSubLayoutItem() const override;

private:
  const Rectangle<vec2>& currentBoundingRectangle() const override;

  void setBoundingRectangle(const Rectangle<vec2>& boundingRect) override;
  void setZPosition(real z) override;
  void setItemIndex(const InOutput<uint16>& index) override;

  void onContainerChanged(const Optional<Container*>& container) override;
  void onWindowChanged(const Optional<Window*>& window) override;
  void onThemeChanged() override;

  void allChildWidgets(const InOutput<QSet<Widget::Ptr>>& allChildWidgets) override;
};


} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_LAYOUT_H
