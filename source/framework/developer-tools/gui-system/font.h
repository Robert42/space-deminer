#ifndef FRAMEWORK_GUI_FONT_H
#define FRAMEWORK_GUI_FONT_H

#include <base/geometry/rectangle.h>
#include <base/strings/string-id-manager.h>

#include "types.h"

namespace Framework {

using namespace Base;

namespace Gui {

class Font final
{
public:
  class Implementation : public noncopyable
  {
    RTTI_CLASS_MEMBER
  public:
    typedef std::shared_ptr<Implementation> Ptr;

  public:
    Implementation();
    virtual ~Implementation();

    static Ptr createFallbackImplementation();

    virtual vec2 textSize(const String& text) const = 0;
    virtual real lineSpacing() const = 0;
    virtual real lineHeight() const = 0;
  };

public:
  Implementation::Ptr _implementation;

public:
  Font();
  explicit Font(const Implementation::Ptr& implementation);

  vec2 textSize(const String& text) const;
  real lineSpacing() const;
  real lineHeight() const;

  const Implementation& implementation() const;
  Implementation& implementation();
};

RTTI_CLASS_DECLARE(Framework::Gui::Font::Implementation)

} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_FONT_H
