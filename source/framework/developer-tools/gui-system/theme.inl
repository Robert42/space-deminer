#ifndef FRAMEWORK_GUI_THEME_INL
#define FRAMEWORK_GUI_THEME_INL

#include "theme.h"

namespace Framework {
namespace Gui {


template<typename T>
T Theme::State::interpolateValues(const T& invisible,
                                  const T& insensitive,
                                  const T& normal, const T& hover, const T& pressed,
                                  const T& keyFocused_normal, const T& keyFocused_hover, const T& keyFocused_pressed) const
{
  T normalValue = interpolateWithFallback<T>(normal,
                                             this->mouseStates[MouseState::HOVER],
                                             hover,
                                             this->mouseStates[MouseState::PRESSED],
                                             pressed);
  T keyFocusedValue = interpolateWithFallback<T>(keyFocused_normal,
                                                 this->mouseStates[MouseState::HOVER],
                                                 keyFocused_hover,
                                                 this->mouseStates[MouseState::PRESSED],
                                                 keyFocused_pressed);

  T visibleValue  = interpolateWithFallback<T>(normalValue,
                                               1-this->sensitive,
                                               insensitive,
                                               this->keyFocused,
                                               keyFocusedValue);

  return interpolate<real, T>(this->alpha, invisible, visibleValue);
}

template<typename T>
T Theme::State::interpolateColors(const T& insensitive,
                                  const T& normal, const T& hover, const T& pressed,
                                  const T& keyFocused_normal, const T& keyFocused_hover, const T& keyFocused_pressed) const
{
  return unpremultiply(interpolateValues<T>(vec4(0),
                                            premultiply(insensitive),
                                            premultiply(normal), premultiply(hover), premultiply(pressed),
                                            premultiply(keyFocused_normal), premultiply(keyFocused_hover), premultiply(keyFocused_pressed)));
}


} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_THEME_INL
