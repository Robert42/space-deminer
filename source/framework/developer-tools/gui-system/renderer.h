#ifndef FRAMEWORK_GUI_RENDERER_H
#define FRAMEWORK_GUI_RENDERER_H

#include "painter.h"

namespace Framework {
namespace Gui {

class Renderer : public Painter
{
public:
  typedef std::shared_ptr<Renderer> Ptr;

public:
  Renderer();

  static Ptr fallbackRenderer();

  virtual void beginRedrawing() = 0;
  virtual void endRedrawing() = 0;
  virtual void sheduleRedraw() = 0;
};

} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_RENDERER_H
