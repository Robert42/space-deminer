#ifndef FRAMEWORK_GUI_SIMPLEWIDGETTEMPLATE_INL
#define FRAMEWORK_GUI_SIMPLEWIDGETTEMPLATE_INL

#include "simple-widget-template.h"
#include "theme.h"

namespace Framework {
namespace Gui {


template<class T_base>
SimpleWidgetTemplate<T_base>::SimpleWidgetTemplate()
{
  createSimpleWidget();
}

template<class T_base>
SimpleWidgetTemplate<T_base>::~SimpleWidgetTemplate()
{
}


template<class T_base>
const Theme::SimpleWidget::Ptr& SimpleWidgetTemplate<T_base>::simpleWidget()
{
  return _simpleWidget;
}


template<class T_base>
void SimpleWidgetTemplate<T_base>::onUpdateMinimumSize()
{
  Optional<vec2> size = this->simpleWidget()->minimumSize();
  if(size)
    this->setMinimumSize(*size);
  else
    ParentClass::onUpdateMinimumSize();
}

template<class T_base>
void SimpleWidgetTemplate<T_base>::onUpdateOptimalSize()
{
  Optional<vec2> size = this->simpleWidget()->optimalSize();
  if(size)
    this->setOptimalSize(*size);
  else
    ParentClass::onUpdateOptimalSize();
}


template<class T_base>
void SimpleWidgetTemplate<T_base>::onThemeChanged()
{
  ParentClass::onThemeChanged();
  createSimpleWidget();
}


template<class T_base>
void SimpleWidgetTemplate<T_base>::SimpleWidgetTemplate::draw(Painter& painter)
{
  simpleWidget()->draw(painter, this);
}


template<class T_base>
bool SimpleWidgetTemplate<T_base>::isWidgetAtPoint(const vec2& point) const
{
  if(!ParentClass::isWidgetAtPoint(point))
    return false;

  return _simpleWidget->isWidgetAtPoint(point, this);
}


template<class T_base>
void SimpleWidgetTemplate<T_base>::createSimpleWidget()
{
  _simpleWidget = this->theme()->simpleWidgetEnforced(this);
}


// ====


template<class T_base>
SimpleContainerTemplate<T_base>::SimpleContainerTemplate()
{
}

template<class T_base>
SimpleContainerTemplate<T_base>::~SimpleContainerTemplate()
{
}


template<class T_base>
void SimpleContainerTemplate<T_base>::onUpdateMinimumSize()
{
  T_base::onUpdateMinimumSize();
  resetMinimumSize();
}

template<class T_base>
void SimpleContainerTemplate<T_base>::onUpdateOptimalSize()
{
  T_base::onUpdateOptimalSize();
  resetOptimalSize();
}


template<class T_base>
void SimpleContainerTemplate<T_base>::onLayoutMinimumSizeChanged()
{
  resetMinimumSize();
}

template<class T_base>
void SimpleContainerTemplate<T_base>::onLayoutOptimalSizeChanged()
{
  resetOptimalSize();
}


template<class T_base>
void SimpleContainerTemplate<T_base>::resetMinimumSize()
{
  vec2 minimumSize = this->rootLayout()->minimumSize();

  Optional<vec2> size = this->simpleWidget()->minimumSize();
  if(size)
    minimumSize = max(*size, minimumSize);

  this->setMinimumSize(minimumSize);
}

template<class T_base>
void SimpleContainerTemplate<T_base>::resetOptimalSize()
{
  vec2 optimalSize = this->rootLayout()->optimalSize();

  Optional<vec2> size = this->simpleWidget()->optimalSize();
  if(size)
    optimalSize = max(*size, optimalSize);

  this->setOptimalSize(optimalSize);
}


} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_SIMPLEWIDGETTEMPLATE_INL
