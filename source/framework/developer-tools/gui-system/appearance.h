#ifndef FRAMEWORK_GUI_APPEARANCE_H
#define FRAMEWORK_GUI_APPEARANCE_H

#include "renderer.h"
#include "theme.h"
#include "input-handler.h"

#include <base/signals/signal.h>

namespace Framework {
namespace Gui {

class Window;

class Appearance : public noncopyable
{
public:
  typedef std::shared_ptr<Appearance> Ptr;

private:
  Signals::CallableSignal<void()> _signalRootThemeChanged;

public:
  Appearance();
  virtual ~Appearance();

  static Ptr fallbackAppearance();

  virtual Renderer::Ptr renderer() = 0;
  virtual Theme::Ptr rootTheme(const TagSet& tags = TagSet()) = 0;

public:
  Signals::Signal<void()>& signalRootThemeChanged();
};

} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_APPEARANCE_H
