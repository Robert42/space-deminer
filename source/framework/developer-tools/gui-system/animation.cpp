#include "animation.h"
#include "animations/concatenate-animations.h"
#include "animations/constant-animation.h"
#include "animations/delay-animation.h"
#include "animations/easy-in-interpolation.h"
#include "animations/easy-out-interpolation.h"
#include "animations/easy-in-and-out-interpolation.h"
#include "animations/fade-animation.h"
#include "animations/linear-interpolation-animation.h"
#include "animations/fall-animation.h"
#include "animations/finish-animation.h"

#include <framework/application.h>
#include <framework/frame-signals.h>
#include <base/set-value-in-lifetime.h>

namespace Framework {
namespace Gui {


class Animation::SetUpdatingFlag : public noncopyable
{
public:
  Animation& animation;

  SetUpdatingFlag(const InOutput<Animation>& animation)
    : animation(animation.value)
  {
    assert(this->animation._updateFunctionIsCalled==false);

    this->animation._updateFunctionIsCalled = true;
  }

  ~SetUpdatingFlag()
  {
    this->animation._updateFunctionIsCalled = false;
  }
};

class Animation::SetFinishingFlag : public noncopyable
{
public:
  Animation& animation;

  SetFinishingFlag(const InOutput<Animation>& animation)
    : animation(animation.value)
  {
    assert(this->animation._finishFunctionIsCalled==false);

    this->animation._finishFunctionIsCalled = true;
  }

  ~SetFinishingFlag()
  {
    this->animation._finishFunctionIsCalled = false;
  }
};


Animation::Animation()
  : _currentValue(-1.f),
    _currentVelocity(-1.f),
    _currentAcceleration(-1.f),
    _started(false),
    _finished(false),
    _finishedSignalSent(false),
    _updateFunctionIsCalled(false),
    _finishFunctionIsCalled(false),
    _garbageCollectorRequested(false)
{
}

Animation::~Animation()
{
  assert(_updateFunctionIsCalled == false);
  assert(_finishFunctionIsCalled == false);
}


Animation::Ptr Animation::createFallback()
{
  static Ptr fallbackAnimation = Animations::ConstantAnimation::create(42.f);

  return fallbackAnimation;
}


Animation::SlotId Animation::slotIdFromName(const char* name)
{
  return static_cast<SlotId>(StringIdManager::idFromString(name));
}

const Animation::Ptr& Animation::totalApplicationTimeAnimation()
{
  return Application::frametimeAnimation();
}

Animation::Ptr Animation::timeAnimationStartingNow(real delay)
{
  Animation::Ptr time = totalApplicationTimeAnimation();
  return Animations::DelayAnimation::create(time, time->currentValue()+delay);
}


Animation::Ptr Animation::_createInterpolationAnimation(real duration,
                                                        const FadeInterpolation& interpolation,
                                                        real delay,
                                                        const Animation::Ptr& timeAnimation)
{
  Animation::Ptr animation = timeAnimation;

  if(delay != 0.f)
    animation = Animations::DelayAnimation::create(animation, delay);

  switch(interpolation.value)
  {
  case FadeInterpolation::EASY_IN:
    animation = Animations::EasyInInterpolation::create(animation, duration);
    break;
  case FadeInterpolation::EASY_OUT:
    animation = Animations::EasyOutInterpolation::create(animation, duration);
    break;
  case FadeInterpolation::LINEAR:
    animation = Animations::LinearInterpolationAnimation::create(animation, duration);
    break;
  case FadeInterpolation::EASY_IN_AND_OUT:
  default:
    animation = Animations::EasyInAndOutInterpolation::create(animation, duration);
    break;
  }

  return animation;
}

Animation::Ptr Animation::_createSnapAnimation(const SnapInterpolation& interpolation,
                                               real targetValue,
                                               real duration,
                                               const Animation::Ptr& timeAnimation,
                                               real currentValue,
                                               real currentVelocity,
                                               real smoothness)
{
  if(interpolation == SnapInterpolation::RAPID)
    return Animations::FinishAnimation::create(Animations::ConstantAnimation::create(targetValue), 0.f);

  currentVelocity = interpolate(smoothness,
                                (targetValue-currentValue)/duration,
                                currentVelocity);

  Animations::FreeFallAnimation::PhysicDescription::InterpolationPreset interpolationPreset;

  switch(interpolation.value)
  {
  case SnapInterpolation::BOUNCING:
    interpolationPreset = Animations::FreeFallAnimation::PhysicDescription::InterpolationPreset::BOUNCE;
    break;
  case SnapInterpolation::DAMPED:
    interpolationPreset = Animations::FreeFallAnimation::PhysicDescription::InterpolationPreset::DAMPING;
    break;
  case SnapInterpolation::STICKY_TARGET:
  default:
    interpolationPreset = Animations::FreeFallAnimation::PhysicDescription::InterpolationPreset::STICK_TO_GROUND;
    break;
  }

  return  Animations::FreeFallAnimation::createByFallDuration(timeAnimation,
                                                              currentValue,
                                                              currentVelocity,
                                                              duration,
                                                              interpolationPreset);
}


real Animation::currentValue() const
{
  return _currentValue;
}

real Animation::currentVelocity() const
{
  return _currentVelocity;
}

real Animation::currentAcceleration() const
{
  return _currentAcceleration;
}


void Animation::setFinished()
{
  if(_finishFunctionIsCalled)
    return;

  if(!_finished)
  {
    SetFinishingFlag setFinishingFlag(inout(*this));

    // make sure the animation is updated a last time after it finished
    updateValues();
    _finished = true;
    sendSignalFinished();

    (void)setFinishingFlag;
  }
}

bool Animation::finished() const
{
  return _finished;
}

bool Animation::started() const
{
  return _started;
}

bool Animation::garbageCollectorRequested() const
{
  return _garbageCollectorRequested;
}


void Animation::updateValues()
{
  if(_updateFunctionIsCalled)
    return;

  if(!finished())
  {
    SetUpdatingFlag setUpdatingFlag(inout(*this));
    _updateValues(out(this->_currentValue),
                  out(this->_currentVelocity),
                  out(this->_currentAcceleration));
    sendSignalStarted();
    sendSignalUpdated();

    (void)setUpdatingFlag;
  }
  sendSignalFinished();

}

Signals::Signal<void()>& Animation::signalStarted()
{
  return _signalStarted;
}

Signals::Signal<void()>& Animation::signalUpdated()
{
  return _signalUpdated;
}

Signals::Signal<void()>& Animation::signalFinished()
{
  return _signalFinished;
}

Signals::Signal<void()>& Animation::signalGarbageCollectionRequested()
{
  return _signalGarbageCollectionRequested;
}

Animation::Ptr Animation::garbageCollect(const Ptr& animation)
{
  animation->_garbageCollectorRequested = false;
  return animation->_garbageCollect(animation);
}

Animation::Ptr Animation::_garbageCollect(const Ptr& self)
{
  return self;
}

void Animation::sendSignalUpdated()
{
  if(!finished())
    _signalUpdated();
}

void Animation::sendSignalStarted()
{
  if(!started())
  {
    _started = true;
    return _signalStarted();
  }
}

void Animation::sendSignalFinished()
{
  if(finished() && !_finishedSignalSent && !_updateFunctionIsCalled)
  {
    _finishedSignalSent = true;
    _signalFinished();
  }
}

void Animation::sendSignalGarbageCollectionRequested()
{
  if(garbageCollectorRequested())
    return;

  _garbageCollectorRequested = true;
  _signalGarbageCollectionRequested();
}


// ====


AnimationMap::AnimationMap()
{
}

inline Optional<Animation::Ptr> AnimationMap::_getGarbageCollected(Animation::SlotId id) const
{
  Optional<Animation::Ptr> optionalAnimation = optionalFromHashMap(_animations, id);

  if(optionalAnimation)
  {
    const Animation::Ptr& oldAnimation = *optionalAnimation;

    if(oldAnimation->garbageCollectorRequested())
    {
      const Animation::Ptr newAnimation = Animation::garbageCollect(oldAnimation);

      if(newAnimation != oldAnimation)
      {
        // The garbage collector function guarant not to change the value of the animation, but only the usage of memory
        const_cast<AnimationMap*>(this)->swapAnimation(id, newAnimation);
        return newAnimation;
      }
    }

    return oldAnimation;
  }else
  {
    return nothing;
  }
}

Signals::Signal<void()>& AnimationMap::signalUpdated()
{
  return _signalUpdated;
}

Signals::Signal<void(Animation::SlotId)> AnimationMap::signalRemovedAnimation()
{
  return _signalRemovedAnimation;
}

Signals::Signal<void(Animation::SlotId)> AnimationMap::signalAddedAnimation()
{
  return _signalAddedAnimation;
}

Optional<real> AnimationMap::optionalValue(Animation::SlotId id) const
{
  assert(id!=0);

  Optional<Animation::Ptr> optionalAnimation = _getGarbageCollected(id);

  if(!optionalAnimation)
    return nothing;

  return (*optionalAnimation)->currentValue();
}

Optional<real> AnimationMap::optionalVelocity(Animation::SlotId id) const
{
  assert(id!=0);

  Optional<Animation::Ptr> optionalAnimation = _getGarbageCollected(id);

  if(!optionalAnimation)
    return nothing;

  return (*optionalAnimation)->currentVelocity();
}


Optional<real> AnimationMap::optionalAcceleration(Animation::SlotId id) const
{
  assert(id!=0);

  Optional<Animation::Ptr> optionalAnimation = _getGarbageCollected(id);

  if(!optionalAnimation)
    return nothing;

  return (*optionalAnimation)->currentAcceleration();
}


real AnimationMap::value(Animation::SlotId id) const
{
  return value(id, currentUnanimatedValue(id));
}

real AnimationMap::value(Animation::SlotId id, real fallback) const
{
  Optional<real> v = optionalValue(id);

  if(v)
    return *v;
  else
    return fallback;
}

real AnimationMap::velocity(Animation::SlotId id, real fallback) const
{
  Optional<real> v = optionalVelocity(id);

  if(v)
    return *v;
  else
    return fallback;
}

real AnimationMap::acceleration(Animation::SlotId id, real fallback) const
{
  Optional<real> a = optionalAcceleration(id);

  if(a)
    return *a;
  else
    return fallback;
}


real AnimationMap::previousUnanimatedValue(Animation::SlotId id) const
{
  return hashValue(_previousUnanimatedValues, id, 0.f);
}

real AnimationMap::currentUnanimatedValue(Animation::SlotId id) const
{
  return hashValue(_currentUnanimatedValues, id, 0.f);
}


void AnimationMap::setCurrentUnanimatedValue(Animation::SlotId id, real value)
{
  real prev = currentUnanimatedValue(id);

  if(value == 0.f)
  {
    if(hashHasKey(_currentUnanimatedValues, id))
      _currentUnanimatedValues.remove(id);
  }else
  {
    _currentUnanimatedValues.insert(id, value);
  }

  if(prev == 0.f)
  {
    if(hashHasKey(_previousUnanimatedValues, id))
      _previousUnanimatedValues.remove(id);
  }else
  {
    _previousUnanimatedValues.insert(id, prev);
  }

  if(prev != currentUnanimatedValue(id) && !hashHasKey(_animations, id))
    _signalUpdated();
}


void AnimationMap::setAnimation(Animation::SlotId id, const Animation::Ptr& animation)
{
  assert(id!=0);

  removeAnimation(id);

  if(!animation->finished())
  {
    real previousValue = this->value(id);

    _animations[id] = animation;
    _updateConnections[id] = animation->signalUpdated().connect(_signalUpdated).track(this->trackable);
    _finishedConnections[id] = animation->signalFinished().connect(std::bind(&AnimationMap::_handleFinishedAnimation, this, id, animation)).track(this->trackable);

    animation->updateValues();

    _signalAddedAnimation(id);

    if(previousValue!=this->value(id))
      _signalUpdated();
  }
}

void AnimationMap::swapAnimation(Animation::SlotId id, const Animation::Ptr& animation)
{
  _removeAnimation(id, false);
  setAnimation(id, animation);
}

void AnimationMap::removeAnimation(Animation::SlotId id)
{
  _removeAnimation(id, true);
}

void AnimationMap::snapToValue(Animation::SlotId id, real value)
{
  setAnimation(id, Animations::ConstantAnimation::create(value));
}

void AnimationMap::snapToDefault(Animation::SlotId id)
{
  removeAnimation(id);
}

void AnimationMap::fadeAnimation(Animation::SlotId id,
                                 const Animation::Ptr& nextAnimation,
                                 real interpolationDuration,
                                 const Animation::FadeInterpolation& interpolation,
                                 real interpolationDelay)
{
  fadeAnimation(id,
                nextAnimation,
                interpolationDuration,
                interpolation,
                0.f, // the delay is already within the time Animation, so no extra delay necessary
                Animation::timeAnimationStartingNow(interpolationDelay));
}

void AnimationMap::fadeAnimation(Animation::SlotId id,
                                 const Animation::Ptr& nextAnimation,
                                 real interpolationDuration,
                                 const Animation::FadeInterpolation& interpolation,
                                 real interpolationDelay,
                                 const Animation::Ptr& timeAnimation,
                                 bool finishAfterInterpolation,
                                 bool enforcedSourceAnimation)
{
  fadeAnimation(id,
                nextAnimation,
                Animation::_createInterpolationAnimation(interpolationDuration,
                                                         interpolation,
                                                         interpolationDelay,
                                                         timeAnimation),
                finishAfterInterpolation,
                enforcedSourceAnimation);
}

void AnimationMap::fadeAnimation(Animation::SlotId id,
                                 const Animation::Ptr& nextAnimation,
                                 const Animation::Ptr& interpolation,
                                 bool finishAfterInterpolation,
                                 bool enforcedSourceAnimation)
{
  Optional<Animation::Ptr> optionalAnimation = optionalFromHashMap(_animations, id);

  Animation::Ptr sourceAnimation;
  if(optionalAnimation)
  {
    sourceAnimation = *optionalAnimation;
  }else if(enforcedSourceAnimation)
  {
    optionalAnimation = Animations::ConstantAnimation::create(this->previousUnanimatedValue(id));
  }else
  {
    setAnimation(id, nextAnimation);
    return;
  }


  Animations::FadeAnimation::Ptr fadeAnimation = Animations::FadeAnimation::create(*optionalAnimation,
                                                                                   nextAnimation,
                                                                                   interpolation,
                                                                                   finishAfterInterpolation);
  if(fadeAnimation->finished())
    removeAnimation(id);
  else
    swapAnimation(id, fadeAnimation);
}



void AnimationMap::fadeToValue(Animation::SlotId id,
                               real value,
                               real interpolationDuration,
                               const Animation::FadeInterpolation& interpolation,
                               real interpolationDelay)
{
  fadeToValue(id,
              value,
              interpolationDuration,
              interpolation,
              0.f, // the delay is already within the time Animation, so no extra delay necessary
              Animation::timeAnimationStartingNow(interpolationDelay));
}

void AnimationMap::fadeToValue(Animation::SlotId id,
                               real value,
                               real interpolationDuration,
                               const Animation::FadeInterpolation& interpolation,
                               real interpolationDelay,
                               const Animation::Ptr& timeAnimation)
{
  fadeToValue(id,
              value,
              Animation::_createInterpolationAnimation(interpolationDuration,
                                                       interpolation,
                                                       interpolationDelay,
                                                       timeAnimation));
}

void AnimationMap::fadeToDefault(Animation::SlotId id,
                                 real interpolationDuration,
                                 const Animation::FadeInterpolation& interpolation,
                                 real interpolationDelay)
{
  fadeToDefault(id,
                interpolationDuration,
                interpolation,
                0.f, // the delay is already within the time Animation, so no extra delay necessary
                Animation::timeAnimationStartingNow(interpolationDelay));
}

void AnimationMap::fadeToDefault(Animation::SlotId id,
                                 real interpolationDuration,
                                 const Animation::FadeInterpolation& interpolation,
                                 real interpolationDelay,
                                 const Animation::Ptr& timeAnimation)
{
  fadeToValue(id,
              this->currentUnanimatedValue(id),
              Animation::_createInterpolationAnimation(interpolationDuration,
                                                       interpolation,
                                                       interpolationDelay,
                                                       timeAnimation),
              true); // When the animation has faded to the default value, the animation should finish allowing the
}

void AnimationMap::fadeToValue(Animation::SlotId id, real value, const Animation::Ptr& interpolation, bool finishAfterInterpolation)
{
  fadeAnimation(id,
                Animations::ConstantAnimation::create(value),
                interpolation,
                finishAfterInterpolation,
                true);
}


void AnimationMap::snapToValue(Animation::SlotId id,
                               real value,
                               real interpolationDuration,
                               const Animation::SnapInterpolation& interpolation,
                               real smooth)
{
  snapToValue(id,
              value,
              interpolationDuration,
              interpolation,
              smooth,
              Animation::totalApplicationTimeAnimation());
}

void AnimationMap::snapToValue(Animation::SlotId id,
                               real value,
                               real interpolationDuration,
                               const Animation::SnapInterpolation& interpolation,
                               real smooth,
                               const Animation::Ptr& timeAnimation,
                               bool finishAfterInterpolation)
{
  real currentValue = this->value(id, this->previousUnanimatedValue(id));
  real currentVelocity = this->velocity(id);

  Animation::Ptr animation = Animation::_createSnapAnimation(interpolation,
                                                             value,
                                                             interpolationDuration,
                                                             timeAnimation,
                                                             currentValue,
                                                             currentVelocity,
                                                             smooth);

  if(!finishAfterInterpolation)
  {
    animation = Animations::ConcatenateAnimations::create({animation,
                                                           Animations::ConstantAnimation::create(value)});
  }

  swapAnimation(id, animation);
}

void AnimationMap::snapToDefault(Animation::SlotId id,
                                 real interpolationDuration,
                                 const Animation::SnapInterpolation& interpolation,
                                 real smooth)
{
  snapToDefault(id,
                interpolationDuration,
                interpolation,
                smooth,
                Animation::totalApplicationTimeAnimation());
}

void AnimationMap::snapToDefault(Animation::SlotId id,
                                 real interpolationDuration,
                                 const Animation::SnapInterpolation& interpolation,
                                 real smooth,
                                 const Animation::Ptr& timeAnimation)
{
  snapToValue(id,
              this->currentUnanimatedValue(id),
              interpolationDuration,
              interpolation,
              smooth,
              timeAnimation,
              true);
}

void AnimationMap::_removeAnimation(Animation::SlotId id, bool sendSignals)
{
  assert(id!=0);

  Optional<Animation::Ptr> optionalAnimation = optionalFromHashMap(_animations, id);

  if(optionalAnimation)
  {
    _updateConnections[id].disconnect();
    _finishedConnections[id].disconnect();

    _updateConnections.remove(id);
    _finishedConnections.remove(id);
    _animations.remove(id);

    if(sendSignals)
    {
      _signalRemovedAnimation(id);
      _signalUpdated();
    }
  }else
  {
    assert(!optionalFromHashMap(_updateConnections, id));
    assert(!optionalFromHashMap(_finishedConnections, id));
  }
}

void AnimationMap::_handleFinishedAnimation(Animation::SlotId id, const Animation::Ptr& animation)
{
  Optional<Animation::Ptr> a = optionalFromHashMap(_animations, id);

  if(a && animation.get() == a->get())
  {
    // the signalFinishedof an animation might be called dusing an updateValues call (or an updateValues call
    // of on of its children)
    // By giving a copy of the shared_ptr to the animation to a lambda, the animation object is guaranteed to
    // exist to the next frame.
    FrameSignals::callOnce([animation](real){});
    removeAnimation(id);
  }
}


} // namespace Gui
} // namespace Framework
