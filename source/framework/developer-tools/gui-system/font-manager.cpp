#include "font-manager.h"

namespace Framework {
namespace Gui {

FontManager::FontManager()
{
}


FontManager::Ptr FontManager::createFallbackTextureManager()
{
  class DummyFontManager : public FontManager
  {
  public:
    Font defaultFont() override
    {
      return Font();
    }
  };

  return Ptr(new DummyFontManager);
}


} // namespace Gui
} // namespace Framework
