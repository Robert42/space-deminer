#include "button.h"

#include "../widgets/label.h"
#include "../layouts/border.h"
#include "../tags.h"

namespace Framework {
namespace Gui {
namespace Containers {


Button::Button()
  : _raisedBorder(true)
{
  RTTI_CLASS_SET_TYPE(Framework::Gui::Containers::Button);

  _borderLayout = Layouts::Border::create();
  _alignmentLayout = Layouts::Alignment::create();
  _zOffsetLayout = Layouts::ZOffset::create();

  _borderLayout->addLayout(_alignmentLayout);
  _alignmentLayout->addLayout(_zOffsetLayout);

  setRootLayout(_borderLayout);

  this->setInputHandlerWithWrapper(clickHandler = InputHandlers::ClickHandler::create(*this));

  clickHandler->keys.insert(KeyCode::SPACE);
  clickHandler->keys.insert(KeyCode::RETURN);
  clickHandler->keys.insert(KeyCode::NUMPAD_ENTER);
}

Button::Button(const Widget::Ptr& child)
  : Button()
{
  addChildWidget(child);
}

Button::Button(const Translation& label)
  : Button(Widgets::Label::create(label))
{
}

Button::Ptr Button::create()
{
  return Ptr(new Button);
}

Button::Ptr Button::create(const Widget::Ptr& child)
{
  return Ptr(new Button(child));
}

Button::Ptr Button::create(const Translation& label)
{
  return Ptr(new Button(label));
}


bool Button::raisedBorder() const
{
  return _raisedBorder;
}

void Button::setRaisedBorder(bool raisedBorder)
{
  if(this->raisedBorder() != raisedBorder)
  {
    this->_raisedBorder = raisedBorder;
    sheduleRedraw();
  }
}

void Button::setAsDefaultButton()
{
  this->rtti.tags.insert(Tags::window_defaultWidget);
}

void Button::setAsDefaultEscapeButton()
{
  this->clickHandler->keys.insert(KeyCode::ESCAPE);
  this->rtti.tags.insert(Tags::window_defaultEscapeWidget);
}

Signals::Signal<void ()>& Button::signalClick()
{
  return clickHandler->signalLeftClicked();
}

Layouts::BinaryLayout& Button::binTargetParentLayout()
{
  return *_zOffsetLayout;
}


} // namespace Containers
} // namespace Gui
} // namespace Framework
