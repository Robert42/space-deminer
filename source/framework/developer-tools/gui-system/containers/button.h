#ifndef FRAMEWORK_GUI_CONTAINERS_BUTTON_H
#define FRAMEWORK_GUI_CONTAINERS_BUTTON_H

#include "bin.h"
#include "../layouts/border.h"
#include "../layouts/alignment.h"
#include "../layouts/z-offset.h"
#include "../simple-widget-template.h"
#include "../input-handlers/click-handler.h"

#include <base/strings/string.h>
#include <base/i18n/translation.h>

namespace Framework {
namespace Gui {
namespace Containers {

class Button : public SimpleContainerTemplate<Bin>
{
public:
  typedef std::shared_ptr<Button> Ptr;
  typedef std::weak_ptr<Button> WeakPtr;

  typedef SimpleContainerTemplate<Bin> ParentClass;

private:
  Layouts::Border::Ptr _borderLayout;
  Layouts::Alignment::Ptr _alignmentLayout;
  Layouts::ZOffset::Ptr _zOffsetLayout;
  bool _raisedBorder;

  InputHandlers::ClickHandler::Ptr clickHandler;

public:
  Button();
  Button(const Widget::Ptr& child);
  Button(const Translation& label);

  static Ptr create();
  static Ptr create(const Widget::Ptr& child);
  static Ptr create(const Translation& label);

public:
  bool raisedBorder() const;
  void setRaisedBorder(bool raisedBorder);

  void setAsDefaultButton();
  void setAsDefaultEscapeButton();

  Signals::Signal<void()>& signalClick();

private:
  Layouts::BinaryLayout& binTargetParentLayout() final override;
};

RTTI_CLASS_DECLARE(Framework::Gui::Containers::Button)

} // namespace Containers
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_CONTAINERS_BUTTON_H
