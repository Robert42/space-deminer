#include "dummy-container.h"

namespace Framework {
namespace Gui {
namespace Containers {

DummyContainer::DummyContainer(const vec4& color)
  : _color(color)
{
  RTTI_CLASS_SET_TYPE(Framework::Gui::Containers::DummyContainer);

  setRootLayout(_binaryLayout = Layouts::WholeArea::create());

  this->setMinimumSize(vec2(22));
  this->setOptimalSize(vec2(48));
}

DummyContainer::Ptr DummyContainer::create(const vec4& color)
{
  return Ptr(new DummyContainer(color));
}


const vec4& DummyContainer::color()
{
  return this->_color;
}

void DummyContainer::setColor(const vec4& color)
{
  if(this->color() != color)
  {
    this->_color = color;
    this->_signalColorChanged(this->color());
    this->sheduleRedraw();
  }
}

Signals::Signal<void(const vec4&)>& DummyContainer::signalColorChanged()
{
  return _signalColorChanged;
}


void DummyContainer::addLayout(const Layout::Ptr& layout)
{
  setRootLayout(layout);
}


void DummyContainer::draw(Painter& painter)
{
  painter.setZPosition(this->zPosition());
  painter.drawFilledRectangle(this->boundingRect(),
                              this->color());
}


} // namespace Containers
} // namespace Gui
} // namespace Framework
