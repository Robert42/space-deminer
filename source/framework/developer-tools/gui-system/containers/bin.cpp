#include "bin.h"
#include "../layouts/whole-area.h"

namespace Framework {
namespace Gui {
namespace Containers {

Bin::Bin()
{
  RTTI_CLASS_SET_TYPE(Framework::Gui::Containers::Bin);
}


void Bin::addLayout(const Layout::Ptr& layout)
{
  binTargetParentLayout().addLayout(layout);
}

void Bin::addChildWidget(const Widget::Ptr& widget)
{
  binTargetParentLayout().addWidget(widget);
}

void Bin::remove()
{
  binTargetParentLayout().remove();
}


} // namespace Containers
} // namespace Gui
} // namespace Framework
