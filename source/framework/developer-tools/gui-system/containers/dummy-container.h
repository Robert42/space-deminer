#ifndef FRAMEWORK_GUI_CONTAINERS_DUMMYCONTAINER_H
#define FRAMEWORK_GUI_CONTAINERS_DUMMYCONTAINER_H

#include "bin.h"
#include "../layouts/whole-area.h"

namespace Framework {
namespace Gui {
namespace Containers {

class DummyContainer : public Container
{
public:
  typedef std::shared_ptr<DummyContainer> Ptr;
  typedef std::weak_ptr<DummyContainer> WeakPtr;

private:
  Layouts::WholeArea::Ptr _binaryLayout;
  vec4 _color;

  Signals::CallableSignal<void(const vec4&)> _signalColorChanged;

protected:
  DummyContainer(const vec4& color = vec4(1.f, 0.5f, 0.f, 1.f));

public:
  static Ptr create(const vec4& color = vec4(1.f, 0.5f, 0.f, 1.f));

public:
  const vec4& color();
  void setColor(const vec4& color);

  Signals::Signal<void(const vec4&)>& signalColorChanged();

  void addLayout(const Layout::Ptr& layout);

public:
  void draw(Painter& painter) final override;
};

RTTI_CLASS_DECLARE(Framework::Gui::Containers::DummyContainer)

} // namespace Containers
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_CONTAINERS_DUMMYCONTAINER_H
