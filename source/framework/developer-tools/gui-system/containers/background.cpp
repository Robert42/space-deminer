#include "background.h"

namespace Framework {
namespace Gui {
namespace Containers {


Background::Background()
{
  RTTI_CLASS_SET_TYPE(Framework::Gui::Containers::Background);

  setRootLayout(_layout = Layouts::WholeArea::create());
}

Background::Ptr Background::create()
{
  return Ptr(new Background);
}


Layouts::BinaryLayout& Background::binTargetParentLayout()
{
  return *_layout;
}


} // namespace Containers
} // namespace Gui
} // namespace Framework
