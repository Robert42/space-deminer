#ifndef FRAMEWORK_GUI_CONTAINERS_BACKGROUND_H
#define FRAMEWORK_GUI_CONTAINERS_BACKGROUND_H

#include "bin.h"
#include "../layouts/whole-area.h"
#include "../simple-widget-template.h"

namespace Framework {
namespace Gui {
namespace Containers {

class Background : public SimpleContainerTemplate<Bin>
{
public:
  typedef std::shared_ptr<Background> Ptr;
  typedef std::weak_ptr<Background> WeakPtr;

private:
  Layouts::WholeArea::Ptr _layout;

public:
  Background();

  static Ptr create();

private:
  Layouts::BinaryLayout& binTargetParentLayout() final override;
};

RTTI_CLASS_DECLARE(Framework::Gui::Containers::Background)

} // namespace Containers
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_CONTAINERS_BACKGROUND_H
