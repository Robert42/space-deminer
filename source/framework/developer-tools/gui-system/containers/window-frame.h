#ifndef FRAMEWORK_GUI_CONTAINERS_WINDOWFRAME_H
#define FRAMEWORK_GUI_CONTAINERS_WINDOWFRAME_H

#include "bin.h"
#include "../layouts/stacked-layout.h"
#include "../layouts/border.h"
#include "../layouts/alignment.h"
#include "../layouts/limit-size.h"
#include "../layouts/z-offset.h"
#include "../simple-widget-template.h"

namespace Framework {
namespace Gui {
namespace Containers {

class WindowFrame : public SimpleContainerTemplate<Bin>
{
public:
  typedef std::shared_ptr<WindowFrame> Ptr;
  typedef std::weak_ptr<WindowFrame> WeakPtr;

private:
  Layouts::StackedLayout::Ptr _rootLayout;

  Layouts::Border::Ptr _mainAreaBorder;
  Layouts::Alignment::Ptr _mainAreaAlignment;
  Layouts::LimitSize::Ptr _mainAreaLimitSize;
  Layouts::ZOffset::Ptr _mainAreaZOffset;

  Layouts::Border::Ptr _windowButtonAreaBorder;
  Layouts::Alignment::Ptr _windowButtonAreaAlignment;
  Layouts::LimitSize::Ptr _windowButtonAreaLimitSize;
  Layouts::ZOffset::Ptr _windowButtonAreaZOffset;

public:
  WindowFrame();

  static Ptr create();

  Layouts::BinaryLayout& mainArea();
  Layouts::BinaryLayout& windowButtonArea();

private:
  Layouts::BinaryLayout& binTargetParentLayout() final override;
};

RTTI_CLASS_DECLARE(Framework::Gui::Containers::WindowFrame)

} // namespace Containers
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_CONTAINERS_WINDOWFRAME_H
