#include "window-frame.h"
#include "../tags.h"

namespace Framework {
namespace Gui {
namespace Containers {

WindowFrame::WindowFrame()
{
  RTTI_CLASS_SET_TYPE(Framework::Gui::Containers::WindowFrame);

  _rootLayout = Layouts::StackedLayout::create();

  _mainAreaBorder = Layouts::Border::create();
  _mainAreaAlignment = Layouts::Alignment::create();
  _mainAreaLimitSize = Layouts::LimitSize::create();
  _mainAreaZOffset = Layouts::ZOffset::create();
  _mainAreaBorder->rtti.tags.insert(Tags::windowFrame_mainArea);
  _mainAreaAlignment->rtti.tags.insert(Tags::windowFrame_mainArea);
  _mainAreaLimitSize->rtti.tags.insert(Tags::windowFrame_mainArea);
  _mainAreaZOffset->rtti.tags.insert(Tags::windowFrame_mainArea);
  _mainAreaBorder->addLayout(_mainAreaAlignment);
  _mainAreaAlignment->addLayout(_mainAreaLimitSize);
  _mainAreaLimitSize->addLayout(_mainAreaZOffset);

  _windowButtonAreaBorder = Layouts::Border::create();
  _windowButtonAreaAlignment = Layouts::Alignment::create();
  _windowButtonAreaLimitSize = Layouts::LimitSize::create();
  _windowButtonAreaZOffset = Layouts::ZOffset::create();
  _windowButtonAreaBorder->rtti.tags.insert(Tags::windowFrame_windowButtonArea);
  _windowButtonAreaAlignment->rtti.tags.insert(Tags::windowFrame_windowButtonArea);
  _windowButtonAreaLimitSize->rtti.tags.insert(Tags::windowFrame_windowButtonArea);
  _windowButtonAreaZOffset->rtti.tags.insert(Tags::windowFrame_windowButtonArea);
  _windowButtonAreaBorder->addLayout(_windowButtonAreaAlignment);
  _windowButtonAreaAlignment->addLayout(_windowButtonAreaLimitSize);
  _windowButtonAreaLimitSize->addLayout(_windowButtonAreaZOffset);

  _rootLayout->appendSubLayout(_mainAreaBorder);
  //_rootLayout->appendSubLayout(_windowButtonAreaBorder);

  setRootLayout(_rootLayout);
}

WindowFrame::Ptr WindowFrame::create()
{
  return Ptr(new WindowFrame);
}

Layouts::BinaryLayout& WindowFrame::mainArea()
{
  return *_mainAreaZOffset;
}

Layouts::BinaryLayout& WindowFrame::binTargetParentLayout()
{
  return mainArea();
}


} // namespace Containers
} // namespace Gui
} // namespace Framework
