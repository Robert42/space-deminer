#ifndef FRAMEWORK_GUI_CONTAINERS_BIN_H
#define FRAMEWORK_GUI_CONTAINERS_BIN_H

#include "../container.h"
#include "../layouts/binary-layout.h"

namespace Framework {
namespace Gui {
namespace Containers {

class Bin : public Container
{
public:
  typedef std::shared_ptr<Bin> Ptr;
  typedef std::weak_ptr<Bin> WeakPtr;

public:
  Bin();

public:
  void addLayout(const Layout::Ptr& layout);
  void addChildWidget(const Widget::Ptr& widget);
  void remove();

protected:
  virtual Layouts::BinaryLayout& binTargetParentLayout() = 0;
};

RTTI_CLASS_DECLARE(Framework::Gui::Containers::Bin)

} // namespace Containers
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_CONTAINERS_BIN_H
