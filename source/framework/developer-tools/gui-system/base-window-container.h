#ifndef FRAMEWORK_GUI_BASEWINDOWCONTAINER_H
#define FRAMEWORK_GUI_BASEWINDOWCONTAINER_H

#include "window.h"
#include "input-handlers/focusable-input-handler.h"

namespace Framework {
namespace Gui {

class BaseWindowManager;

class BaseWindowContainer
{
public:
  typedef std::shared_ptr<BaseWindowContainer> Ptr;

private:

public:
  BaseWindowManager& windowManager;
  const Window::Ptr window;
  const InputHandlers::FocusableInputHandler::Ptr inputHandler;

public:
  BaseWindowContainer(BaseWindowManager& windowManager,
                      const Window::Ptr& window,
                      bool roundAllocationHint);
  virtual ~BaseWindowContainer();

public:
  void redraw(Painter& painter);

  QVector<Widget::Ptr> widgetsAtCurrentMousePosition() const;
  virtual vec2 currentMousePosition(real zPosition) const = 0;
  virtual bool canHandleMouseInputAtCurrentMousePosition() const = 0;

protected:
  virtual void beginDrawing(Painter& painter) = 0;
  virtual void endDrawing(Painter& painter) = 0;

protected:
  void setWindowAppearance(const Appearance::Ptr& appearance);
  void setWindowSize(const vec2& windowSize);

  void _handleMovedWindow();

  void unmarkRedrawDirty();

private:
  virtual QSet<Widget::Ptr> widgetsAtCurrentMousePositionImplementation() const = 0;

private:
  friend class Window;

  void _askForKeyFocus(const Widget::Ptr& widget, const Widget::FocusHint& focusHint);
};

} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_BASEWINDOWCONTAINER_H
