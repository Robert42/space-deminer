#ifndef FRAMEWORK_GUI_SIMPLEWIDGETTEMPLATE_H
#define FRAMEWORK_GUI_SIMPLEWIDGETTEMPLATE_H

#include "theme.h"

namespace Framework {
namespace Gui {

class Container;


template<class T_base>
class SimpleWidgetTemplate : public T_base
{
  static_assert(std::is_base_of<Widget,T_base>::value || std::is_same<Widget,T_base>::value,
                "T_base must be the class Widget or inherit from Widget");
public:
  typedef std::shared_ptr< SimpleWidgetTemplate<T_base> > Ptr;
  typedef std::shared_ptr< SimpleWidgetTemplate<T_base> > WeakPtr;
  typedef T_base ParentClass;

private:
  Theme::SimpleWidget::Ptr _simpleWidget;

public:
  SimpleWidgetTemplate();
  virtual ~SimpleWidgetTemplate();

public:
  const Theme::SimpleWidget::Ptr& simpleWidget();

protected:
  void onUpdateMinimumSize() override;
  void onUpdateOptimalSize() override;

  void onThemeChanged() final override;
  void draw(Painter& painter) override;

  bool isWidgetAtPoint(const vec2& point) const override;

private:
  void createSimpleWidget();
};


template<class T_base>
class SimpleContainerTemplate : public SimpleWidgetTemplate<T_base>
{
  static_assert(std::is_base_of<Container,T_base>::value || std::is_same<Container,T_base>::value,
                "T_base must be the class Container or inherit from Container");
public:
  typedef std::shared_ptr< SimpleContainerTemplate<T_base> > Ptr;
  typedef std::shared_ptr< SimpleContainerTemplate<T_base> > WeakPtr;
  typedef SimpleWidgetTemplate<T_base> ParentClass;

public:
  SimpleContainerTemplate();
  virtual ~SimpleContainerTemplate();

protected:
  void onUpdateMinimumSize() final override;
  void onUpdateOptimalSize() final override;
  void onLayoutMinimumSizeChanged() final override;
  void onLayoutOptimalSizeChanged() final override;

private:
  void resetMinimumSize();
  void resetOptimalSize();
};


} // namespace Gui
} // namespace Framework

#include "simple-widget-template.inl"

#endif // FRAMEWORK_GUI_SIMPLEWIDGETTEMPLATE_H
