#ifndef FRAMEWORK_GUI_INPUTHANDLERS_DELEGATINGINPUTHANDLER_H
#define FRAMEWORK_GUI_INPUTHANDLERS_DELEGATINGINPUTHANDLER_H

#include "../input-handler.h"

namespace Framework {
namespace Gui {
namespace InputHandlers {


/** Helper class delegating the input-events to another input handler.
 */
class DelegatingInputHandler : public InputHandler
{
public:
  DelegatingInputHandler();

public:
  bool handleKeyPressed(KeyCode keyCode) override;
  bool handleKeyReleased(KeyCode keyCode) override;
  bool handleUnicodeEvent(String::value_type unicodeValue) override;
  bool handleMouseButtonPressed(MouseButton button) override;
  bool handleMouseButtonReleased(MouseButton button) override;
  bool handleMouseWheel(real wheelMovement) override;
  bool handleMouseMove() override;
  bool handleMouseEnter() override;
  bool handleMouseLeave() override;
  bool handleKeyFocusEnter() override;
  bool handleKeyFocusLeave() override;

  virtual InputHandler::Ptr inputHandlerDelegatedTo() = 0;
};

} // namespace InputHandlers
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_INPUTHANDLERS_DELEGATINGINPUTHANDLER_H
