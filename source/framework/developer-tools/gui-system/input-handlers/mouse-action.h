#ifndef FRAMEWORK_GUI_INPUTHANDLERS_MOUSEACTION_H
#define FRAMEWORK_GUI_INPUTHANDLERS_MOUSEACTION_H

#include "delegating-input-handler.h"
#include "signal-input-handler.h"
#include "../widget.h"

#include <base/enum-macros.h>
#include <base/geometry/region.h>

namespace Framework {
namespace Gui {
namespace InputHandlers {

class MouseAction final : public DelegatingInputHandler
{
public:
  typedef std::shared_ptr<MouseAction> Ptr;
  typedef std::weak_ptr<MouseAction> WeakPtr;

  class Event
  {
  private:
    friend class MouseAction;

    typedef std::shared_ptr<Event> Ptr;
    typedef std::weak_ptr<Event> WeakPtr;

  public:
    typedef bool CheckConditionSignalSignature(MouseAction&);
    typedef Signals::ResultHandler::LogicalAndResultHandler<CheckConditionSignalSignature> CheckCondictionResultHandler;
    typedef Signals::CallableSignal<CheckConditionSignalSignature, CheckCondictionResultHandler> CallableSignalCheckCondition;
    typedef Signals::Signal<CheckConditionSignalSignature, CheckCondictionResultHandler> SignalCheckCondition;

  public:
    Signals::Trackable trackable;
    MouseAction& mouseAction;

    bool eventShouldntBeHandeledByOtherHandlers : 1;
    bool eventShouldBeCalledOnlyOnce : 1;

  private:
    Signals::CallableSignal<void(MouseAction&)> _signalOccured;
    CallableSignalCheckCondition _signalCheckConditions;

  private:
    Event(MouseAction& mouseAction);

    static Ptr create(MouseAction& mouseAction);

  public:
    Signals::Signal<void(MouseAction&)>& signalOccured();
    SignalCheckCondition& signalCheckConditions();

    void setBehaviorSucceed();
    void setBehaviorAbort();
    void setBehaviorAnimation(const TagSet& tags);

    void setConditionMouseInsideWidget();
    void setConditionMouseOutsideWidget();
    void setConditionMouseInsideRegion(const Geometry::Region::Ptr& region);
    void setConditionMouseOutsideRegion(const Geometry::Region::Ptr& region);

    /** Returns a small circular area around the current mouse-position with the redius of the
     * mouse-movement threshold.
     *
     * If the mouse cursor is within this radius, the mouse can be seen as not moved by the user.
     *
     * @return a smart pointer to a region.
     */
    Geometry::Region::Ptr mouseNotMovedArea() const; // INPUT-174 : this function uses currentMousePosition and the mouseMovementTreshold from the theme/usability-settings

  private:
    bool handleEvent();
    std::function<bool()> handleEventSlot();

    vec2 currentMousePosition() const;

    bool isMouseInsideWidget();
    bool isMouseOutsideWidget();
    bool isMouseInsideRegion(const Geometry::Region::Ptr& region);
    bool isMouseOutsideRegion(const Geometry::Region::Ptr& region);
  };

  BEGIN_ENUMERATION(ButtonAction,)
    PRESS,
    RELEASE
  ENUMERATION_BASIC_OPERATORS(ButtonAction)
  END_ENUMERATION;

public:
  Signals::Trackable trackable;

  const SignalInputHandler::Ptr signalInputHandler;
  const InputHandler::Ptr inputHandler;

  /* A simple pointer is wnough, as the mouse action will be immediatly aborted, when the widget gets removed from the window
   */
  Widget& widget;
  bool succeedIfReplaced : 1;

private:
  Signals::CallableSignal<void()> _signalActionCanBeDeleted;
  Signals::CallableSignal<void()> _signalActionFinished;
  Signals::CallableSignal<void()> _signalActionSucceeded;
  Signals::CallableSignal<void()> _signalActionAborted;
  bool _finished : 1;

  Signals::CallableSignal<void()> _signalMouseLeftWidget;
  Signals::CallableSignal<void()> _signalMouseEnteredWidgetAgain;
  bool _isMouseInsideWidget : 1;

  QSet<Event::Ptr> _events;

private:
  MouseAction(Widget& widget, const Optional<InputHandler::Ptr>& inputHandler);

public:
  ~MouseAction();

  static Ptr create(Widget& widget, const Optional<InputHandler::Ptr>& inputHandler=nothing);

public:
  Signals::Signal<void()>& signalActionCanBeDeleted();
  Signals::Signal<void()>& signalActionFinished();
  Signals::Signal<void()>& signalActionSucceeded();
  Signals::Signal<void()>& signalActionAborted();

  Signals::Signal<void()>& signalMouseLeftWidget();
  Signals::Signal<void()>& signalMouseEnteredWidgetAgain();

  void succeed();
  void abort();
  void replace();

  Event* createKeyPressEvent(KeyCode keyCode);
  Event* createKeyReleaseEvent(KeyCode keyCode);
  Event* createKeyEvent(KeyCode keyCode, ButtonAction buttonAction);

  Event* createMouseButtonPressEvent(MouseButton mouseButton);
  Event* createMouseButtonReleaseEvent(MouseButton mouseButton);
  Event* createMouseButtonEvent(MouseButton mouseButton, ButtonAction buttonAction);

  Event* createMouseLeavesWidgetEvent();
  Event* createMouseEntersWidgetAgainEvent();

private:
  Event* _createEvent();

  static InputHandler::Ptr ensureInputHandler(Widget& widget,
                                              const Optional<InputHandler::Ptr>& inputHandler);

  InputHandler::Ptr inputHandlerDelegatedTo() final override;

  bool _checkWhetherMouseInWidget();

  static bool callIfButton(MouseButton realMouseButton, MouseButton expectedMouseButton, const std::function<bool()>& fn);
  static bool callIfKey(KeyCode key, KeyCode expectedKey, const std::function<bool()>& fn);
};

} // namespace InputHandlers
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_INPUTHANDLERS_MOUSEACTION_H
