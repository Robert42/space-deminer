#include "delegating-input-handler.h"

namespace Framework {
namespace Gui {
namespace InputHandlers {


DelegatingInputHandler::DelegatingInputHandler()
{
}


bool DelegatingInputHandler::handleKeyPressed(KeyCode keyCode)
{
  return inputHandlerDelegatedTo()->handleKeyPressed(keyCode);
}

bool DelegatingInputHandler::handleKeyReleased(KeyCode keyCode)
{
  return inputHandlerDelegatedTo()->handleKeyReleased(keyCode);
}

bool DelegatingInputHandler::handleUnicodeEvent(String::value_type unicodeValue)
{
  return inputHandlerDelegatedTo()->handleUnicodeEvent(unicodeValue);
}

bool DelegatingInputHandler::handleMouseButtonPressed(MouseButton button)
{
  return inputHandlerDelegatedTo()->handleMouseButtonPressed(button);
}

bool DelegatingInputHandler::handleMouseButtonReleased(MouseButton button)
{
  return inputHandlerDelegatedTo()->handleMouseButtonReleased(button);
}

bool DelegatingInputHandler::handleMouseWheel(real wheelMovement)
{
  return inputHandlerDelegatedTo()->handleMouseWheel(wheelMovement);
}

bool DelegatingInputHandler::handleMouseMove()
{
  return inputHandlerDelegatedTo()->handleMouseMove();
}

bool DelegatingInputHandler::handleMouseEnter()
{
  return inputHandlerDelegatedTo()->handleMouseEnter();
}

bool DelegatingInputHandler::handleMouseLeave()
{
  return inputHandlerDelegatedTo()->handleMouseLeave();
}

bool DelegatingInputHandler::handleKeyFocusEnter()
{
  return inputHandlerDelegatedTo()->handleKeyFocusEnter();
}

bool DelegatingInputHandler::handleKeyFocusLeave()
{
  return inputHandlerDelegatedTo()->handleKeyFocusLeave();
}


} // namespace InputHandlers
} // namespace Gui
} // namespace Framework
