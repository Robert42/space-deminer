#include "click-handler.h"
#include "mouse-action.h"

#include "../widget.h"
#include "../tags.h"

namespace Framework {
namespace Gui {
namespace InputHandlers {


ClickHandler::ClickHandler(Widget& widget)
  : widget(&widget, widget.trackable)
{
}

ClickHandler::Ptr ClickHandler::create(Widget& widget)
{
  return Ptr(new ClickHandler(widget));
}


Signals::Signal<void()>& ClickHandler::signalLeftClicked()
{
  return _signalLeftClicked;
}


bool ClickHandler::handleMouseButtonPressed(MouseButton button)
{
  Optional<Widget*> optionalWidget = this->widget.value();

  if(optionalWidget)
  {
    Widget& widget = **optionalWidget;

    if(button == MouseButton::LEFT)
    {
      Tag mouseButtonTag = Tags::Animations::leftMouseButton;

      widget.playAnimation({mouseButtonTag, Tags::Animations::startClick});

      MouseAction::Event* event;
      MouseAction::Ptr mouseAction = MouseAction::create(widget);

      mouseAction->signalActionSucceeded().connect(_signalLeftClicked).track(this->trackable);

      event = mouseAction->createMouseButtonReleaseEvent(button);
      event->setConditionMouseInsideWidget();
      event->setBehaviorSucceed();
      event->setBehaviorAnimation({mouseButtonTag, Tags::Animations::endClick, Tags::Animations::succeeded});

      event = mouseAction->createKeyReleaseEvent(KeyCode::ESCAPE);
      event->setBehaviorAbort();
      event->setBehaviorAnimation({mouseButtonTag, Tags::Animations::endClick, Tags::Animations::aborted});

      event = mouseAction->createMouseButtonReleaseEvent(button);
      event->setConditionMouseOutsideWidget();
      event->setBehaviorAbort();
      event->setBehaviorAnimation({mouseButtonTag, Tags::Animations::endClick, Tags::Animations::aborted});

      event = mouseAction->createMouseLeavesWidgetEvent();
      event->setBehaviorAnimation({mouseButtonTag, Tags::Animations::endClick, Tags::Animations::suspended});

      event = mouseAction->createMouseEntersWidgetAgainEvent();
      event->setBehaviorAnimation({mouseButtonTag, Tags::Animations::startClick, Tags::Animations::continued});

      return true;
    }
  }

  return false;
}


bool ClickHandler::handleKeyPressed(KeyCode keyCode)
{
  Optional<Widget*> optionalWidget = this->widget.value();

  if(optionalWidget)
  {
    Widget& widget = **optionalWidget;

    if(this->keys.contains(keyCode))
    {
      Tag mouseButtonTag = Tags::Animations::key;

      widget.playAnimation({mouseButtonTag, Tags::Animations::startClick});

      MouseAction::Event* event;
      MouseAction::Ptr mouseAction = MouseAction::create(widget);

      mouseAction->signalActionSucceeded().connect(_signalLeftClicked).track(this->trackable);

      event = mouseAction->createKeyReleaseEvent(keyCode);
      event->setBehaviorSucceed();
      event->setBehaviorAnimation({mouseButtonTag, Tags::Animations::endClick, Tags::Animations::succeeded});

      event = mouseAction->createKeyReleaseEvent(KeyCode::ESCAPE);
      event->setBehaviorAbort();
      event->setBehaviorAnimation({mouseButtonTag, Tags::Animations::endClick, Tags::Animations::aborted});

      return true;
    }
  }

  return false;
}



} // namespace InputHandlers
} // namespace Gui
} // namespace Framework
