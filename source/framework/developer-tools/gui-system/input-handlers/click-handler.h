#ifndef FRAMEWORK_GUI_INPUTHANDLERS_CLICKHANDLER_H
#define FRAMEWORK_GUI_INPUTHANDLERS_CLICKHANDLER_H

#include "fallback-input-handler.h"

#include <base/tracking-pointer.h>

namespace Framework {
namespace Gui {

class Widget;
namespace InputHandlers {

class ClickHandler final : public FallbackInputHandler
{
public:
  typedef std::shared_ptr<ClickHandler> Ptr;
  typedef std::weak_ptr<ClickHandler> WeakPtr;

public:
  Signals::Trackable trackable;

  const TrackingPtr<Widget*> widget;
  QSet<KeyCode> keys;

private:
  Signals::CallableSignal<void()> _signalLeftClicked;

public:
  ClickHandler(Widget& widget);

  static Ptr create(Widget& widget);

  Signals::Signal<void()>& signalLeftClicked();

  bool handleMouseButtonPressed(MouseButton button) override;
  bool handleKeyPressed(KeyCode keyCode) override;
};

} // namespace InputHandlers
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_INPUTHANDLERS_CLICKHANDLER_H
