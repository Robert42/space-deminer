#ifndef FRAMEWORK_GUI_INPUTHANDLERS_SIGNALINPUTHANDLER_H
#define FRAMEWORK_GUI_INPUTHANDLERS_SIGNALINPUTHANDLER_H

#include "../input-handler.h"

namespace Framework {
namespace Gui {
namespace InputHandlers {

class SignalInputHandler : public InputHandler
{
public:
  typedef std::shared_ptr<SignalInputHandler> Ptr;

private:
  Signals::CallableSignal<bool(KeyCode keyCode)> _signalKeyPressed;
  Signals::CallableSignal<bool(KeyCode keyCode)> _signalKeyReleased;
  Signals::CallableSignal<bool(String::value_type unicodeValue)> _signalUnicodeEvent;
  Signals::CallableSignal<bool(MouseButton button)> _signalMouseButtonPressed;
  Signals::CallableSignal<bool(MouseButton button)> _signalMouseButtonReleased;
  Signals::CallableSignal<bool(real wheelMovement)> _signalMouseWheel;
  Signals::CallableSignal<bool()> _signalMouseMove;
  Signals::CallableSignal<bool()> _signalMouseEnter;
  Signals::CallableSignal<bool()> _signalMouseLeave;
  Signals::CallableSignal<bool()> _signalKeyFocusEnter;
  Signals::CallableSignal<bool()> _signalKeyFocusLeave;

public:
  SignalInputHandler();

  static Ptr create();

public:
  bool handleKeyPressed(KeyCode keyCode) override;
  bool handleKeyReleased(KeyCode keyCode) override;
  bool handleUnicodeEvent(String::value_type unicodeValue) override;
  bool handleMouseButtonPressed(MouseButton button) override;
  bool handleMouseButtonReleased(MouseButton button) override;
  bool handleMouseWheel(real wheelMovement) override;
  bool handleMouseMove() override;
  bool handleMouseEnter() override;
  bool handleMouseLeave() override;
  bool handleKeyFocusEnter() override;
  bool handleKeyFocusLeave() override;

  Signals::Signal<bool(KeyCode keyCode)>& signalKeyPressed();
  Signals::Signal<bool(KeyCode keyCode)>& signalKeyReleased();
  Signals::Signal<bool(String::value_type unicodeValue)>& signalUnicodeEvent();
  Signals::Signal<bool(MouseButton button)>& signalMouseButtonPressed();
  Signals::Signal<bool(MouseButton button)>& signalMouseButtonReleased();
  Signals::Signal<bool(real wheelMovement)>& signalMouseWheel();
  Signals::Signal<bool()>& signalMouseMove();
  Signals::Signal<bool()>& signalMouseEnter();
  Signals::Signal<bool()>& signalMouseLeave();
  Signals::Signal<bool()>& signalKeyFocusEnter();
  Signals::Signal<bool()>& signalKeyFocusLeave();
};

} // namespace InputHandlers
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_INPUTHANDLERS_SIGNALINPUTHANDLER_H
