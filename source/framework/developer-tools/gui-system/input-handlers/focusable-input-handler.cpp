#include "focusable-input-handler.h"

namespace Framework {
namespace Gui {
namespace InputHandlers {


FocusableInputHandler::FocusableInputHandler(const InputHandler::Ptr& otherInputHandler)
  : _otherInputHandler(otherInputHandler),
    _mouseSensitive(true),
    _keySensitive(true),
    _sensitive(true),
    _hasKeyFocus(false),
    _hasMouseFocus(false)
{
}

FocusableInputHandler::Ptr FocusableInputHandler::create(const InputHandler::Ptr& otherInputHandler)
{
  return Ptr(new FocusableInputHandler(otherInputHandler));
}

bool FocusableInputHandler::sensitiveMode(Mode mode) const
{
  if(mode == Mode::MOUSE)
    return mouseSensitive();
  else
    return keySensitive();
}

bool FocusableInputHandler::sensitive() const
{
  return _sensitive;
}

bool FocusableInputHandler::mouseSensitive() const
{
  return _mouseSensitive;
}

bool FocusableInputHandler::keySensitive() const
{
  return _keySensitive;
}


bool FocusableInputHandler::hasKeyFocus() const
{
  return _hasKeyFocus;
}

bool FocusableInputHandler::hasMouseFocus() const
{
  return _hasMouseFocus;
}


void FocusableInputHandler::setMouseSensitive(bool mouseSensitive)
{
  if(this->mouseSensitive() != mouseSensitive)
  {
    this->_mouseSensitive = mouseSensitive;
    invalidateFocusedWidget();
  }
}

void FocusableInputHandler::setKeySensitive(bool keySensitive)
{
  if(this->keySensitive() != keySensitive)
  {
    this->_keySensitive = keySensitive;
    invalidateFocusedWidget();
  }
}

void FocusableInputHandler::setSensitive(bool sensitive)
{
  if(this->sensitive() != sensitive)
  {
    this->_sensitive = sensitive;
    invalidateFocusedWidget();
  }
}

void FocusableInputHandler::setSensitiveMode(Mode mode, bool sensitive)
{
  if(mode == Mode::MOUSE)
    return setMouseSensitive(sensitive);
  else
    return setKeySensitive(sensitive);
}


Signals::Signal<void()>& FocusableInputHandler::signalInvalidateFocusedWidget()
{
  return _signalInvalidateFocusedWidget;
}

InputHandler::Ptr FocusableInputHandler::inputHandlerDelegatedTo()
{
  return _otherInputHandler;
}

void FocusableInputHandler::invalidateFocusedWidget()
{
  _signalInvalidateFocusedWidget();
}


bool FocusableInputHandler::handleMouseEnter()
{
  _hasMouseFocus = true;
  return DelegatingInputHandler::handleMouseEnter();
}

bool FocusableInputHandler::handleMouseLeave()
{
  _hasMouseFocus = false;
  return DelegatingInputHandler::handleMouseLeave();
}


bool FocusableInputHandler::handleKeyFocusEnter()
{
  _hasKeyFocus = true;
  return DelegatingInputHandler::handleKeyFocusEnter();
}

bool FocusableInputHandler::handleKeyFocusLeave()
{
  _hasKeyFocus = false;
  return DelegatingInputHandler::handleKeyFocusLeave();
}


} // namespace InputHandlers
} // namespace Gui
} // namespace Framework
