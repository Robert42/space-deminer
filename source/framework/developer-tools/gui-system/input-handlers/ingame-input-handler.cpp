#include "ingame-input-handler.h"

namespace Framework {
namespace Gui {
namespace InputHandlers {

IngameInputHandler::IngameInputHandler(const InputHandler::Ptr& inputHandlerDelegatedTo,
                                       InputDevice::ActionPriority inputPriority)
  : _inputHandlerDelegatedTo(inputHandlerDelegatedTo)
{
  Keyboard::signalKeyPressed().connect(inputPriority, std::bind(&IngameInputHandler::handleKeyPressed, this, _1)).track(this->trackable);
  Keyboard::signalKeyReleased().connect(inputPriority, std::bind(&IngameInputHandler::handleKeyReleased, this, _1)).track(this->trackable);
  Keyboard::signalUnicodeEntered().connect(inputPriority, std::bind(&IngameInputHandler::handleUnicodeEvent, this, _1)).track(this->trackable);
  Mouse::signalButtonPressed().connect(inputPriority, std::bind(&IngameInputHandler::handleMouseButtonPressed, this, _1)).track(this->trackable);
  Mouse::signalButtonReleased().connect(inputPriority, std::bind(&IngameInputHandler::handleMouseButtonReleased, this, _1)).track(this->trackable);
  Mouse::signalMouseWheelMoved().connect(inputPriority, std::bind(&IngameInputHandler::_handleMouseWheel, this)).track(this->trackable);
  Mouse::signalMouseMoved().connect(inputPriority, std::bind(&IngameInputHandler::_handleMouseMove, this)).track(this->trackable);
}

IngameInputHandler::Ptr IngameInputHandler::create(const InputHandler::Ptr& inputHandlerDelegatedTo,
                                                   InputDevice::ActionPriority inputPriority)
{
  return Ptr(new IngameInputHandler(inputHandlerDelegatedTo,
                                    inputPriority));
}

InputHandler::Ptr IngameInputHandler::inputHandlerDelegatedTo()
{
  return _inputHandlerDelegatedTo;
}

bool IngameInputHandler::_handleMouseWheel()
{
  return handleMouseWheel(Mouse::wheelMovement());
}

bool IngameInputHandler::_handleMouseMove()
{
  return handleMouseMove();
}


} // namespace InputHandlers
} // namespace Gui
} // namespace Framework
