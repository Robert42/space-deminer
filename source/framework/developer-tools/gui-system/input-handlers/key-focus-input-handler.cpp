#include "key-focus-input-handler.h"
#include <base/increment-in-lifetime.h>


namespace Framework {
namespace Gui {
namespace InputHandlers {


KeyFocusInputHandler::KeyFocusInputHandler(bool forceHavingFocusedWidget)
  : FocusedInputHandler(FocusableInputHandler::Mode::KEY),
    forceHavingFocusedWidget(forceHavingFocusedWidget)
{
}


void KeyFocusInputHandler::onFocusLost(const FocusableInputHandler::Ptr& focusedInputHandler)
{
  FocusedInputHandler::onFocusLost(focusedInputHandler);
  focusedInputHandler->handleKeyFocusLeave();

  onInvalidateFocusedInputHandler();
}

void KeyFocusInputHandler::onFocusEntered()
{
  FocusedInputHandler::onFocusEntered();

  assert(this->focusedInputHandler());
  if(this->focusedInputHandler())
    (*this->focusedInputHandler())->handleKeyFocusEnter();
}

void KeyFocusInputHandler::onInvalidateFocusedInputHandler()
{
  if(forceHavingFocusedWidget &&
     !this->focusedInputHandler())
  {
    Optional<FocusableInputHandler::Ptr> handler = searchFallback();

    if(handler)
      setFocusedInputHandler(*handler);
    else
      unsetFocusedInputHandler();
  }
}

Optional<FocusableInputHandler::Ptr> KeyFocusInputHandler::searchFallback()
{
  return nothing;
}


} // namespace InputHandlers
} // namespace Gui
} // namespace Framework
