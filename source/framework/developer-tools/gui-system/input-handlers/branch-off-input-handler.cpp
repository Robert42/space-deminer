#include "branch-off-input-handler.h"

namespace Framework {
namespace Gui {
namespace InputHandlers {


BranchOffInputHandler::BranchOffInputHandler(const InputHandler::Ptr& preferredInputHandler,
                                             const InputHandler::Ptr& alternativeInputHandler)
  : preferredInputHandler(preferredInputHandler),
    alternativeInputHandler(alternativeInputHandler)
{
}

BranchOffInputHandler::Ptr BranchOffInputHandler::create(const InputHandler::Ptr& preferredInputHandler,
                                                         const InputHandler::Ptr& alternativeInputHandler)
{
  return Ptr(new BranchOffInputHandler(preferredInputHandler,
                                       alternativeInputHandler));
}


bool BranchOffInputHandler::handleKeyPressed(KeyCode keyCode)
{
  if(preferredInputHandler->handleKeyPressed(keyCode))
    return true;
  else
    return alternativeInputHandler->handleKeyPressed(keyCode);
}

bool BranchOffInputHandler::handleKeyReleased(KeyCode keyCode)
{
  if(preferredInputHandler->handleKeyReleased(keyCode))
    return true;
  else
    return alternativeInputHandler->handleKeyReleased(keyCode);
}

bool BranchOffInputHandler::handleUnicodeEvent(String::value_type unicodeValue)
{
  if(preferredInputHandler->handleUnicodeEvent(unicodeValue))
    return true;
  else
    return alternativeInputHandler->handleUnicodeEvent(unicodeValue);
}

bool BranchOffInputHandler::handleMouseButtonPressed(MouseButton button)
{
  if(preferredInputHandler->handleMouseButtonPressed(button))
    return true;
  else
    return alternativeInputHandler->handleMouseButtonPressed(button);
}

bool BranchOffInputHandler::handleMouseButtonReleased(MouseButton button)
{
  if(preferredInputHandler->handleMouseButtonReleased(button))
    return true;
  else
    return alternativeInputHandler->handleMouseButtonReleased(button);
}

bool BranchOffInputHandler::handleMouseWheel(real wheelMovement)
{
  if(preferredInputHandler->handleMouseWheel(wheelMovement))
    return true;
  else
    return alternativeInputHandler->handleMouseWheel(wheelMovement);
}

bool BranchOffInputHandler::handleMouseMove()
{
  if(preferredInputHandler->handleMouseMove())
    return true;
  else
    return alternativeInputHandler->handleMouseMove();
}

bool BranchOffInputHandler::handleMouseEnter()
{
  if(preferredInputHandler->handleMouseEnter())
    return true;
  else
    return alternativeInputHandler->handleMouseEnter();
}

bool BranchOffInputHandler::handleMouseLeave()
{
  if(preferredInputHandler->handleMouseLeave())
    return true;
  else
    return alternativeInputHandler->handleMouseLeave();
}

bool BranchOffInputHandler::handleKeyFocusEnter()
{
  if(preferredInputHandler->handleKeyFocusEnter())
    return true;
  else
    return alternativeInputHandler->handleKeyFocusEnter();
}

bool BranchOffInputHandler::handleKeyFocusLeave()
{
  if(preferredInputHandler->handleKeyFocusLeave())
    return true;
  else
    return alternativeInputHandler->handleKeyFocusLeave();
}


} // namespace InputHandlers
} // namespace Gui
} // namespace Framework
