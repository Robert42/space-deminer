#ifndef FRAMEWORK_GUI_INPUTHANDLERS_FALLBACKINPUTHANDLER_H
#define FRAMEWORK_GUI_INPUTHANDLERS_FALLBACKINPUTHANDLER_H

#include "../input-handler.h"

namespace Framework {
namespace Gui {
namespace InputHandlers {

class FallbackInputHandler : public InputHandler
{
public:
  typedef std::shared_ptr<FallbackInputHandler> Ptr;
  typedef std::weak_ptr<FallbackInputHandler> WeakPtr;

public:
  FallbackInputHandler();

  static Ptr create();

  bool handleKeyPressed(KeyCode keyCode) override;
  bool handleKeyReleased(KeyCode keyCode) override;
  bool handleUnicodeEvent(String::value_type unicodeValue) override;
  bool handleMouseButtonPressed(MouseButton button) override;
  bool handleMouseButtonReleased(MouseButton button) override;
  bool handleMouseWheel(real wheelMovement) override;
  bool handleMouseMove() override;
  bool handleMouseEnter() override;
  bool handleMouseLeave() override;
  bool handleKeyFocusEnter() override;
  bool handleKeyFocusLeave() override;
};

} // namespace InputHandlers
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_INPUTHANDLERS_FALLBACKINPUTHANDLER_H
