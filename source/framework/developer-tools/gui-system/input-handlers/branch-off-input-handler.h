#ifndef FRAMEWORK_GUI_INPUTHANDLERS_BRANCHOFFINPUTHANDLER_H
#define FRAMEWORK_GUI_INPUTHANDLERS_BRANCHOFFINPUTHANDLER_H

#include "../input-handler.h"

namespace Framework {
namespace Gui {
namespace InputHandlers {

class BranchOffInputHandler final : public InputHandler
{
public:
  typedef std::shared_ptr<BranchOffInputHandler> Ptr;

public:
  const InputHandler::Ptr preferredInputHandler;
  const InputHandler::Ptr alternativeInputHandler;

public:
  BranchOffInputHandler(const InputHandler::Ptr& preferredInputHandler,
                        const InputHandler::Ptr& alternativeInputHandler);

  static Ptr create(const InputHandler::Ptr& preferredInputHandler,
                    const InputHandler::Ptr& alternativeInputHandler);

  bool handleKeyPressed(KeyCode keyCode) override;
  bool handleKeyReleased(KeyCode keyCode) override;
  bool handleUnicodeEvent(String::value_type unicodeValue) override;
  bool handleMouseButtonPressed(MouseButton button) override;
  bool handleMouseButtonReleased(MouseButton button) override;
  bool handleMouseWheel(real wheelMovement) override;
  bool handleMouseMove() override;
  bool handleMouseEnter() override;
  bool handleMouseLeave() override;
  bool handleKeyFocusEnter() override;
  bool handleKeyFocusLeave() override;
};

} // namespace InputHandlers
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_INPUTHANDLERS_BRANCHOFFINPUTHANDLER_H
