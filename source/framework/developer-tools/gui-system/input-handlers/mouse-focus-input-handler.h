#ifndef FRAMEWORK_GUI_INPUTHANDLERS_MOUSEFOCUSINPUTHANDLER_H
#define FRAMEWORK_GUI_INPUTHANDLERS_MOUSEFOCUSINPUTHANDLER_H

#include "focused-input-handler.h"

namespace Framework {
namespace Gui {
namespace InputHandlers {


class MouseFocusInputHandler : public FocusedInputHandler
{
public:
  MouseFocusInputHandler();

protected:
  void onFocusLost(const FocusableInputHandler::Ptr& focusedInputHandler) override;
  void onFocusEntered() override;

  bool handleMouseMove() override;
  bool handleMouseEnter() override;
  bool handleMouseLeave() override;

  virtual QVector<FocusableInputHandler::Ptr> inputHandlersForCursorPosition() = 0;

protected:
  void onInvalidateFocusedInputHandler() override;
};


} // namespace InputHandlers
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_INPUTHANDLERS_MOUSEFOCUSINPUTHANDLER_H
