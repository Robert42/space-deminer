#ifndef FRAMEWORK_GUI_INPUTHANDLERS_MOUSEACTIONINPUTHANDLER_H
#define FRAMEWORK_GUI_INPUTHANDLERS_MOUSEACTIONINPUTHANDLER_H

#include "delegating-input-handler.h"
#include "mouse-action.h"
#include "../window.h"

namespace Framework {
namespace Gui {
namespace InputHandlers {

class MouseActionInputHandler final : public DelegatingInputHandler
{
public:
  typedef std::shared_ptr<MouseActionInputHandler> Ptr;

public:
  Signals::Trackable trackable;

private:
  Optional<MouseAction::Ptr> _mouseAction;

private:
  MouseActionInputHandler(const Window::Ptr& window);

public:
  ~MouseActionInputHandler();

  static Ptr create(const Window::Ptr& window);

public:
  const Optional<MouseAction::Ptr>& mouseAction();

private:
  InputHandler::Ptr inputHandlerDelegatedTo() final override;

  void startMouseAction(const MouseAction::Ptr& mouseAction);
  void unsetMouseAction(const MouseAction::Ptr& mouseAction);
  void abortMouseActionIfWidgetWasRemoved(const Widget::Ptr& widget);
};

} // namespace InputHandlers
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_INPUTHANDLERS_MOUSEACTIONINPUTHANDLER_H
