#include "mouse-action-input-handler.h"

namespace Framework {
namespace Gui {
namespace InputHandlers {

MouseActionInputHandler::MouseActionInputHandler(const Window::Ptr& window)
{
  window->signalMouseActionStarted().connect(std::bind(&MouseActionInputHandler::startMouseAction, this, _1)).track(this->trackable);
  window->widgetModel()->signalWidgetWasRemoved().connect(std::bind(&MouseActionInputHandler::abortMouseActionIfWidgetWasRemoved, this, _1)).track(this->trackable);
}

MouseActionInputHandler::~MouseActionInputHandler()
{
  this->trackable.clear();
}

MouseActionInputHandler::Ptr MouseActionInputHandler::create(const Window::Ptr& window)
{
  return Ptr(new MouseActionInputHandler(window));
}

const Optional<MouseAction::Ptr>& MouseActionInputHandler::mouseAction()
{
  return this->_mouseAction;
}

InputHandler::Ptr MouseActionInputHandler::inputHandlerDelegatedTo()
{
  if(this->_mouseAction)
    return *this->_mouseAction;
  else
    return InputHandler::fallbackInputHandler();
}

void MouseActionInputHandler::startMouseAction(const MouseAction::Ptr& mouseAction)
{
  if(this->_mouseAction)
  {
    MouseAction::Ptr prevMouseAction = *this->_mouseAction;

    assert(prevMouseAction != mouseAction);
    if(prevMouseAction == mouseAction)
      return;

    prevMouseAction->replace();

    assert(!this->_mouseAction);
  }

  this->_mouseAction = mouseAction;

  mouseAction->signalActionCanBeDeleted().connect(std::bind(&MouseActionInputHandler::unsetMouseAction, this, mouseAction)).track(this->trackable);
}

void MouseActionInputHandler::unsetMouseAction(const MouseAction::Ptr& mouseAction)
{
  if(this->_mouseAction)
  {
    MouseAction::Ptr currentMouseAction = *this->_mouseAction;

    assert(mouseAction == currentMouseAction);

    if(mouseAction == currentMouseAction)
      this->_mouseAction.reset();
  }
}

void MouseActionInputHandler::abortMouseActionIfWidgetWasRemoved(const Widget::Ptr& widget)
{
  if(this->_mouseAction)
  {
    MouseAction::Ptr mouseAction = *this->_mouseAction;

    if(&mouseAction->widget == widget.get())
      mouseAction->abort();
  }
}


} // namespace InputHandlers
} // namespace Gui
} // namespace Framework
