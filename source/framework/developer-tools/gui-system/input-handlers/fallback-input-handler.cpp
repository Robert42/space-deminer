#include "fallback-input-handler.h"

namespace Framework {
namespace Gui {
namespace InputHandlers {


FallbackInputHandler::FallbackInputHandler()
{
}

FallbackInputHandler::Ptr FallbackInputHandler::create()
{
  return Ptr(new FallbackInputHandler);
}


bool FallbackInputHandler::handleKeyPressed(KeyCode)
{
  return false;
}

bool FallbackInputHandler::handleKeyReleased(KeyCode)
{
  return false;
}

bool FallbackInputHandler::handleUnicodeEvent(String::value_type)
{
  return false;
}

bool FallbackInputHandler::handleMouseButtonPressed(MouseButton)
{
  return false;
}

bool FallbackInputHandler::handleMouseButtonReleased(MouseButton)
{
  return false;
}

bool FallbackInputHandler::handleMouseWheel(real)
{
  return false;
}

bool FallbackInputHandler::handleMouseMove()
{
  return false;
}

bool FallbackInputHandler::handleMouseEnter()
{
  return false;
}

bool FallbackInputHandler::handleMouseLeave()
{
  return false;
}

bool FallbackInputHandler::handleKeyFocusEnter()
{
  return false;
}

bool FallbackInputHandler::handleKeyFocusLeave()
{
  return false;
}


} // namespace InputHandlers
} // namespace Gui
} // namespace Framework
