#include "focused-input-handler.h"
#include <base/increment-in-lifetime.h>

namespace Framework {
namespace Gui {
namespace InputHandlers {


FocusedInputHandler::FocusedInputHandler(Mode mode)
  : _focusedSetterLocks(0),
    mode(mode)
{
}

FocusedInputHandler::~FocusedInputHandler()
{
  _signalConnection.disconnect();
}


int FocusedInputHandler::focusedSetterLocks() const
{
  return _focusedSetterLocks;
}


const Optional<FocusableInputHandler::Ptr>& FocusedInputHandler::focusedInputHandler() const
{
  return _focusedInputHandler;
}


void FocusedInputHandler::setFocusedInputHandler(const FocusableInputHandler::Ptr& focusedInputHandler)
{
  if(!focusedSetterLocks() &&
     this->_focusedInputHandler != focusedInputHandler &&
     focusedInputHandler->sensitiveMode(mode))
  {
    IncrementInLifeTime<int> lock(_focusedSetterLocks);

    this->unsetFocusedInputHandler();

    this->_focusedInputHandler = focusedInputHandler;
    this->onFocusEntered();

    _signalConnection = focusedInputHandler->signalInvalidateFocusedWidget().connect(std::bind(&FocusedInputHandler::onInvalidateFocusedInputHandler, this)).trackManually();

    (void)lock;
  }
}

void FocusedInputHandler::unsetFocusedInputHandler()
{
  if(this->_focusedInputHandler)
  {
    FocusableInputHandler::Ptr previousFocused = *this->_focusedInputHandler;

    this->_signalConnection.disconnect();
    this->_focusedInputHandler.reset();
    this->onFocusLost(previousFocused);
  }
}


void FocusedInputHandler::onFocusLost(const FocusableInputHandler::Ptr&)
{
}

void FocusedInputHandler::onFocusEntered()
{
}

InputHandler::Ptr FocusedInputHandler::inputHandlerDelegatedTo()
{
  if(_focusedInputHandler)
    return *_focusedInputHandler;
  else
    return InputHandler::fallbackInputHandler();
}


} // namespace InputHandlers
} // namespace Gui
} // namespace Framework
