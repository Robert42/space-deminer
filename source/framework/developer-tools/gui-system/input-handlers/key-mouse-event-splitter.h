#ifndef FRAMEWORK_GUI_INPUTHANDLERS_KEYMOUSEEVENTSPLITTER_H
#define FRAMEWORK_GUI_INPUTHANDLERS_KEYMOUSEEVENTSPLITTER_H

#include "../input-handler.h"

namespace Framework {
namespace Gui {
namespace InputHandlers {


class KeyMouseEventSplitter : public InputHandler
{
public:
  const InputHandler::Ptr keyInputHandler;
  const InputHandler::Ptr mouseInputHandler;

public:
  KeyMouseEventSplitter(const InputHandler::Ptr& keyInputHandler,
                        const InputHandler::Ptr& mouseInputHandler);

  static Ptr create(const InputHandler::Ptr& keyInputHandler,
                    const InputHandler::Ptr& mouseInputHandler,
                    bool keyFocusGetsMouseWheelEvents = false);

  bool handleKeyPressed(KeyCode keyCode) override;
  bool handleKeyReleased(KeyCode keyCode) override;
  bool handleUnicodeEvent(String::value_type unicodeValue) override;
  bool handleMouseButtonPressed(MouseButton button) override;
  bool handleMouseButtonReleased(MouseButton button) override;
  bool handleMouseWheel(real wheelMovement) override;
  bool handleMouseMove() override;
  bool handleMouseEnter() override;
  bool handleMouseLeave() override;
  bool handleKeyFocusEnter() override;
  bool handleKeyFocusLeave() override;
};


} // namespace InputHandlers
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_INPUTHANDLERS_KEYMOUSEEVENTSPLITTER_H
