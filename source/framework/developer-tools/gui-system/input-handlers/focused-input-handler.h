#ifndef FRAMEWORK_GUI_INPUTHANDLERS_FOCUSEDINPUTHANDLER_H
#define FRAMEWORK_GUI_INPUTHANDLERS_FOCUSEDINPUTHANDLER_H

#include "delegating-input-handler.h"
#include "focusable-input-handler.h"

namespace Framework {
namespace Gui {
namespace InputHandlers {

class FocusedInputHandler : public DelegatingInputHandler
{
public:
  typedef std::shared_ptr<FocusedInputHandler> Ptr;
  typedef std::weak_ptr<FocusedInputHandler> WeakPtr;

  typedef FocusableInputHandler::Mode Mode;

private:
  Optional<FocusableInputHandler::Ptr> _focusedInputHandler;
  Signals::Connection _signalConnection;
  int _focusedSetterLocks;

public:
  const Mode mode;

public:
  FocusedInputHandler(Mode mode);
  ~FocusedInputHandler();

public:
  int focusedSetterLocks() const;

  const Optional<FocusableInputHandler::Ptr>& focusedInputHandler() const;
  void setFocusedInputHandler(const FocusableInputHandler::Ptr& focusedInputHandler);
  void unsetFocusedInputHandler();

protected:
  virtual void onFocusLost(const FocusableInputHandler::Ptr& focusedInputHandler);
  virtual void onFocusEntered();

  virtual void onInvalidateFocusedInputHandler() = 0;

private:
  InputHandler::Ptr inputHandlerDelegatedTo() final override;
};

} // namespace InputHandlers
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_INPUTHANDLERS_FOCUSEDINPUTHANDLER_H
