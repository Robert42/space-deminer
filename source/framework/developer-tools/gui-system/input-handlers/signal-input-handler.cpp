#include "signal-input-handler.h"

namespace Framework {
namespace Gui {
namespace InputHandlers {

SignalInputHandler::SignalInputHandler()
{
}

SignalInputHandler::Ptr SignalInputHandler::create()
{
  return Ptr(new SignalInputHandler);
}


bool SignalInputHandler::handleKeyPressed(KeyCode keyCode)
{
  return _signalKeyPressed(keyCode);
}

bool SignalInputHandler::handleKeyReleased(KeyCode keyCode)
{
  return _signalKeyReleased(keyCode);
}

bool SignalInputHandler::handleUnicodeEvent(String::value_type unicodeValue)
{
  return _signalUnicodeEvent(unicodeValue);
}

bool SignalInputHandler::handleMouseButtonPressed(MouseButton button)
{
  return _signalMouseButtonPressed(button);
}

bool SignalInputHandler::handleMouseButtonReleased(MouseButton button)
{
  return _signalMouseButtonReleased(button);
}

bool SignalInputHandler::handleMouseWheel(real wheelMovement)
{
  return _signalMouseWheel(wheelMovement);
}

bool SignalInputHandler::handleMouseMove()
{
  return _signalMouseMove();
}

bool SignalInputHandler::handleMouseEnter()
{
  return _signalMouseEnter();
}

bool SignalInputHandler::handleMouseLeave()
{
  return _signalMouseLeave();
}

bool SignalInputHandler::handleKeyFocusEnter()
{
  return _signalKeyFocusEnter();
}

bool SignalInputHandler::handleKeyFocusLeave()
{
  return _signalKeyFocusLeave();
}


Signals::Signal<bool(KeyCode keyCode)>& SignalInputHandler::signalKeyPressed()
{
  return _signalKeyPressed;
}

Signals::Signal<bool(KeyCode keyCode)>& SignalInputHandler::signalKeyReleased()
{
  return _signalKeyReleased;
}

Signals::Signal<bool(String::value_type unicodeValue)>& SignalInputHandler::signalUnicodeEvent()
{
  return _signalUnicodeEvent;
}

Signals::Signal<bool(MouseButton button)>& SignalInputHandler::signalMouseButtonPressed()
{
  return _signalMouseButtonPressed;
}

Signals::Signal<bool(MouseButton button)>& SignalInputHandler::signalMouseButtonReleased()
{
  return _signalMouseButtonReleased;
}

Signals::Signal<bool(real wheelMovement)>& SignalInputHandler::signalMouseWheel()
{
  return _signalMouseWheel;
}

Signals::Signal<bool()>& SignalInputHandler::signalMouseMove()
{
  return _signalMouseMove;
}

Signals::Signal<bool()>& SignalInputHandler::signalMouseEnter()
{
  return _signalMouseEnter;
}

Signals::Signal<bool()>& SignalInputHandler::signalMouseLeave()
{
  return _signalMouseLeave;
}

Signals::Signal<bool()>& SignalInputHandler::signalKeyFocusEnter()
{
  return _signalKeyFocusEnter;
}

Signals::Signal<bool()>& SignalInputHandler::signalKeyFocusLeave()
{
  return _signalKeyFocusLeave;
}



} // namespace InputHandlers
} // namespace Gui
} // namespace Framework
