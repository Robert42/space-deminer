#include "key-mouse-event-splitter.h"

namespace Framework {
namespace Gui {
namespace InputHandlers {


class KeyMouseEventSplitter_KeyFocusedGetsMouseWheelEvent : public KeyMouseEventSplitter
{
public:
  KeyMouseEventSplitter_KeyFocusedGetsMouseWheelEvent(const InputHandler::Ptr& keyInputHandler,
                                                      const InputHandler::Ptr& mouseInputHandler)
    : KeyMouseEventSplitter(keyInputHandler,
                            mouseInputHandler)
  {
  }

  bool handleMouseWheel(real wheelMovement) override
  {
    return keyInputHandler->handleMouseWheel(wheelMovement);
  }
};


KeyMouseEventSplitter::KeyMouseEventSplitter(const InputHandler::Ptr& keyInputHandler,
                                             const InputHandler::Ptr& mouseInputHandler)
  : keyInputHandler(keyInputHandler),
    mouseInputHandler(mouseInputHandler)
{
}

KeyMouseEventSplitter::Ptr KeyMouseEventSplitter::create(const InputHandler::Ptr& keyInputHandler,
                                                         const InputHandler::Ptr& mouseInputHandler,
                                                         bool keyFocusGetsMouseWheelEvents)
{
  if(keyFocusGetsMouseWheelEvents)
    return Ptr(new KeyMouseEventSplitter_KeyFocusedGetsMouseWheelEvent(keyInputHandler,
                                                                       mouseInputHandler));
  else
    return Ptr(new KeyMouseEventSplitter(keyInputHandler,
                                         mouseInputHandler));
}


bool KeyMouseEventSplitter::handleKeyPressed(KeyCode keyCode)
{
  return keyInputHandler->handleKeyPressed(keyCode);
}

bool KeyMouseEventSplitter::handleKeyReleased(KeyCode keyCode)
{
  return keyInputHandler->handleKeyReleased(keyCode);
}

bool KeyMouseEventSplitter::handleUnicodeEvent(String::value_type unicodeValue)
{
  return keyInputHandler->handleUnicodeEvent(unicodeValue);
}

bool KeyMouseEventSplitter::handleMouseButtonPressed(MouseButton button)
{
  return mouseInputHandler->handleMouseButtonPressed(button);
}

bool KeyMouseEventSplitter::handleMouseButtonReleased(MouseButton button)
{
  return mouseInputHandler->handleMouseButtonReleased(button);
}

bool KeyMouseEventSplitter::handleMouseWheel(real wheelMovement)
{
  return mouseInputHandler->handleMouseWheel(wheelMovement);
}

bool KeyMouseEventSplitter::handleMouseMove()
{
  return mouseInputHandler->handleMouseMove();
}

bool KeyMouseEventSplitter::handleMouseEnter()
{
  return mouseInputHandler->handleMouseEnter();
}

bool KeyMouseEventSplitter::handleMouseLeave()
{
  return mouseInputHandler->handleMouseLeave();
}

bool KeyMouseEventSplitter::handleKeyFocusEnter()
{
  return keyInputHandler->handleKeyFocusEnter();
}

bool KeyMouseEventSplitter::handleKeyFocusLeave()
{
  return keyInputHandler->handleKeyFocusLeave();
}


} // namespace InputHandlers
} // namespace Gui
} // namespace Framework
