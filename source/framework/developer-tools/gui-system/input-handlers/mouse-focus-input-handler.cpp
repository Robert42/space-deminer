#include "mouse-focus-input-handler.h"
#include <base/increment-in-lifetime.h>

namespace Framework {
namespace Gui {
namespace InputHandlers {


MouseFocusInputHandler::MouseFocusInputHandler()
  : FocusedInputHandler(FocusableInputHandler::Mode::MOUSE)
{
}

void MouseFocusInputHandler::onFocusLost(const FocusableInputHandler::Ptr& focusedInputHandler)
{
  FocusedInputHandler::onFocusLost(focusedInputHandler);
  focusedInputHandler->handleMouseLeave();

  onInvalidateFocusedInputHandler();
}

void MouseFocusInputHandler::onFocusEntered()
{
  FocusedInputHandler::onFocusEntered();

  assert(this->focusedInputHandler());
  (*this->focusedInputHandler())->handleMouseEnter();
}

bool MouseFocusInputHandler::handleMouseMove()
{
  onInvalidateFocusedInputHandler();

  return FocusedInputHandler::handleMouseMove();
}

bool MouseFocusInputHandler::handleMouseEnter()
{
  onInvalidateFocusedInputHandler();

  return FocusedInputHandler::handleMouseEnter();
}

bool MouseFocusInputHandler::handleMouseLeave()
{
  this->unsetFocusedInputHandler();

  return FocusedInputHandler::handleMouseLeave();
}

void MouseFocusInputHandler::onInvalidateFocusedInputHandler()
{
  if(focusedSetterLocks())
    return;

  for(const FocusableInputHandler::Ptr& inputHandler : inputHandlersForCursorPosition())
  {
    if(inputHandler->mouseSensitive() && inputHandler->sensitive())
    {
      setFocusedInputHandler(inputHandler);
      return;
    }
  }

  unsetFocusedInputHandler();
}


} // namespace InputHandlers
} // namespace Gui
} // namespace Framework
