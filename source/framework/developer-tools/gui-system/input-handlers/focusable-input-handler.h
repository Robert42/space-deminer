#ifndef FRAMEWORK_GUI_INPUTHANDLERS_FOCUSABLEINPUTHANDLER_H
#define FRAMEWORK_GUI_INPUTHANDLERS_FOCUSABLEINPUTHANDLER_H

#include "delegating-input-handler.h"

namespace Framework {
namespace Gui {
namespace InputHandlers {

class FocusableInputHandler final : public DelegatingInputHandler
{
public:
  typedef std::shared_ptr<FocusableInputHandler> Ptr;
  typedef std::weak_ptr<FocusableInputHandler> WeakPtr;

  BEGIN_ENUMERATION(Mode,)
    MOUSE,
    KEY
  ENUMERATION_BASIC_OPERATORS(Mode)
  END_ENUMERATION;

private:
  const InputHandler::Ptr _otherInputHandler;
  bool _mouseSensitive : 1;
  bool _keySensitive : 1;
  bool _sensitive : 1;

  bool _hasKeyFocus : 1;
  bool _hasMouseFocus : 1;

  Signals::CallableSignal<void()> _signalInvalidateFocusedWidget;

public:
  FocusableInputHandler(const InputHandler::Ptr& otherInputHandler);

  static FocusableInputHandler::Ptr create(const InputHandler::Ptr& otherInputHandler);

public:
  bool sensitiveMode(Mode mode) const;
  bool sensitive() const;
  bool mouseSensitive() const;
  bool keySensitive() const;

  bool hasKeyFocus() const;
  bool hasMouseFocus() const;

  void setMouseSensitive(bool mouseSensitive);
  void setKeySensitive(bool keySensitive);
  void setSensitive(bool sensitiveMode);
  void setSensitiveMode(Mode mode, bool sensitiveMode);

  Signals::Signal<void()>& signalInvalidateFocusedWidget();

  void invalidateFocusedWidget();

  InputHandler::Ptr inputHandlerDelegatedTo() final override;

  bool handleMouseEnter() final override;
  bool handleMouseLeave() final override;
  bool handleKeyFocusEnter() final override;
  bool handleKeyFocusLeave() final override;
};

} // namespace InputHandlers
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_INPUTHANDLERS_FOCUSABLEINPUTHANDLER_H
