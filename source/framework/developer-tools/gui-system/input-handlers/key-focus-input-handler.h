#ifndef FRAMEWORK_GUI_INPUTHANDLERS_KEYFOCUSINPUTHANDLER_H
#define FRAMEWORK_GUI_INPUTHANDLERS_KEYFOCUSINPUTHANDLER_H

#include "focused-input-handler.h"

namespace Framework {
namespace Gui {
namespace InputHandlers {


class KeyFocusInputHandler : public FocusedInputHandler
{
public:
  typedef std::shared_ptr<KeyFocusInputHandler> Ptr;
  typedef std::weak_ptr<KeyFocusInputHandler> WeakPtr;

private:
  bool forceHavingFocusedWidget;

public:
  KeyFocusInputHandler(bool forceHavingFocusedWidget);

protected:
  void onFocusLost(const FocusableInputHandler::Ptr& focusedInputHandler) override;
  void onFocusEntered() final override;

  void onInvalidateFocusedInputHandler() final override;

  virtual Optional<FocusableInputHandler::Ptr> searchFallback();
};


} // namespace InputHandlers
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_INPUTHANDLERS_KEYFOCUSINPUTHANDLER_H
