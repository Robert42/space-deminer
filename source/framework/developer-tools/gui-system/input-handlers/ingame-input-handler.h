#ifndef FRAMEWORK_GUI_INPUTHANDLERS_INGAMEINPUTHANDLER_H
#define FRAMEWORK_GUI_INPUTHANDLERS_INGAMEINPUTHANDLER_H

#include "delegating-input-handler.h"

namespace Framework {
namespace Gui {
namespace InputHandlers {


/**
 * Helper input class connecting itself to the game input-devices.
 */
class IngameInputHandler : public DelegatingInputHandler
{
public:
  Signals::Trackable trackable;

private:
  InputHandler::Ptr _inputHandlerDelegatedTo;

public:
  IngameInputHandler(const InputHandler::Ptr& inputHandlerDelegatedTo,
                     InputDevice::ActionPriority inputPriority);

  static Ptr create(const InputHandler::Ptr& inputHandlerDelegatedTo,
                    InputDevice::ActionPriority inputPriority);

  InputHandler::Ptr inputHandlerDelegatedTo() override;

private:
  bool _handleMouseWheel();
  bool _handleMouseMove();
};

} // namespace InputHandlers
} // namespace Gui
} // namespace Framework

#endif // FRAMEWOTK_GUI_INPUTHANDLERS_INGAMEINPUTHANDLER_H
