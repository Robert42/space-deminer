#ifndef FRAMEWORK_GUI_INPUTHANDLERS_CONCATENATEBASEDINPUTHANDLER_H
#define FRAMEWORK_GUI_INPUTHANDLERS_CONCATENATEBASEDINPUTHANDLER_H

#include "../input-handler.h"

namespace Framework {
namespace Gui {


class BaseWindowManager;
class Window;


namespace InputHandlers {


/** An abstract base class allowing to handle input based on a priority.
 *
 */
class ConcatenatedBasedInputHandler : public InputHandler
{
public:
  typedef std::shared_ptr<ConcatenatedBasedInputHandler> Ptr;
  typedef std::shared_ptr<ConcatenatedBasedInputHandler> WeakPtr;

private:
  const QVector<InputHandler::Ptr> inputHandlers;

public:
  ConcatenatedBasedInputHandler(const QVector<InputHandler::Ptr>& inputHandlers);

  static Ptr create(const QVector<InputHandler::Ptr>& inputHandlers);

public:
  bool handleKeyPressed(KeyCode keyCode) override;
  bool handleKeyReleased(KeyCode keyCode) override;
  bool handleUnicodeEvent(String::value_type unicodeValue) override;
  bool handleMouseButtonPressed(MouseButton button) override;
  bool handleMouseButtonReleased(MouseButton button) override;
  bool handleMouseWheel(real wheelMovement) override;
  bool handleMouseMove() override;
  bool handleMouseEnter() override;
  bool handleMouseLeave() override;
  bool handleKeyFocusEnter() override;
  bool handleKeyFocusLeave() override;
};

} // namespace InputHandlers
} // namespace Gui
} // namespace Framework

#endif // FRAMEWORK_GUI_INPUTHANDLERS_CONCATENATEBASEDINPUTHANDLER_H
