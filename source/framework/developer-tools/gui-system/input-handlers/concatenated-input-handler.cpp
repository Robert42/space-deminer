#include "concatenated-input-handler.h"
#include "../base-window-manager.h"

namespace Framework {
namespace Gui {
namespace InputHandlers {


ConcatenatedBasedInputHandler::ConcatenatedBasedInputHandler(const QVector<InputHandler::Ptr>& inputHandlers)
  : inputHandlers(inputHandlers)
{
}

ConcatenatedBasedInputHandler::Ptr ConcatenatedBasedInputHandler::create(const QVector<InputHandler::Ptr>& inputHandlers)
{
  return Ptr(new ConcatenatedBasedInputHandler(inputHandlers));
}


bool ConcatenatedBasedInputHandler::handleKeyPressed(KeyCode keyCode)
{
  for(const InputHandler::Ptr& inputHandler : inputHandlers)
    if(inputHandler->handleKeyPressed(keyCode))
      return true;
  return false;
}

bool ConcatenatedBasedInputHandler::handleKeyReleased(KeyCode keyCode)
{
  for(const InputHandler::Ptr& inputHandler : inputHandlers)
    if(inputHandler->handleKeyReleased(keyCode))
      return true;
  return false;
}

bool ConcatenatedBasedInputHandler::handleUnicodeEvent(String::value_type unicodeValue)
{
  for(const InputHandler::Ptr& inputHandler : inputHandlers)
    if(inputHandler->handleUnicodeEvent(unicodeValue))
      return true;
  return false;
}

bool ConcatenatedBasedInputHandler::handleMouseButtonPressed(MouseButton button)
{
  for(const InputHandler::Ptr& inputHandler : inputHandlers)
    if(inputHandler->handleMouseButtonPressed(button))
      return true;
  return false;
}

bool ConcatenatedBasedInputHandler::handleMouseButtonReleased(MouseButton button)
{
  for(const InputHandler::Ptr& inputHandler : inputHandlers)
    if(inputHandler->handleMouseButtonReleased(button))
      return true;
  return false;
}

bool ConcatenatedBasedInputHandler::handleMouseWheel(real wheelMovement)
{
  for(const InputHandler::Ptr& inputHandler : inputHandlers)
    if(inputHandler->handleMouseWheel(wheelMovement))
      return true;
  return false;
}

bool ConcatenatedBasedInputHandler::handleMouseMove()
{
  for(const InputHandler::Ptr& inputHandler : inputHandlers)
    if(inputHandler->handleMouseMove())
      return true;
  return false;
}

bool ConcatenatedBasedInputHandler::handleMouseEnter()
{
  for(const InputHandler::Ptr& inputHandler : inputHandlers)
    if(inputHandler->handleMouseEnter())
      return true;
  return false;
}

bool ConcatenatedBasedInputHandler::handleMouseLeave()
{
  for(const InputHandler::Ptr& inputHandler : inputHandlers)
    if(inputHandler->handleMouseLeave())
      return true;
  return false;
}

bool ConcatenatedBasedInputHandler::handleKeyFocusEnter()
{
  for(const InputHandler::Ptr& inputHandler : inputHandlers)
    if(inputHandler->handleKeyFocusEnter())
      return true;
  return false;
}

bool ConcatenatedBasedInputHandler::handleKeyFocusLeave()
{
  for(const InputHandler::Ptr& inputHandler : inputHandlers)
    if(inputHandler->handleKeyFocusLeave())
      return true;
  return false;
}


} // namespace InputHandlers
} // namespace Gui
} // namespace Framework
