#include "optional-branch-off-input-handler.h"

namespace Framework {
namespace Gui {
namespace InputHandlers {

OptionalBranchOffInputHandler::OptionalBranchOffInputHandler(const InputHandler::Ptr& alternativeInputHandler)
  : alternativeInputHandler(alternativeInputHandler)
{
}


bool OptionalBranchOffInputHandler::handleKeyPressed(KeyCode keyCode)
{
  Optional<InputHandler::Ptr> preferredInputHandler = this->preferredInputHandler();

  if(preferredInputHandler && (*preferredInputHandler)->handleKeyPressed(keyCode))
    return true;
  else
    return alternativeInputHandler->handleKeyPressed(keyCode);
}

bool OptionalBranchOffInputHandler::handleKeyReleased(KeyCode keyCode)
{
  Optional<InputHandler::Ptr> preferredInputHandler = this->preferredInputHandler();

  if(preferredInputHandler && (*preferredInputHandler)->handleKeyPressed(keyCode))
    return true;
  else
    return alternativeInputHandler->handleKeyReleased(keyCode);
}

bool OptionalBranchOffInputHandler::handleUnicodeEvent(String::value_type unicodeValue)
{
  Optional<InputHandler::Ptr> preferredInputHandler = this->preferredInputHandler();

  if(preferredInputHandler && (*preferredInputHandler)->handleUnicodeEvent(unicodeValue))
    return true;
  else
    return alternativeInputHandler->handleUnicodeEvent(unicodeValue);
}

bool OptionalBranchOffInputHandler::handleMouseButtonPressed(MouseButton button)
{
  Optional<InputHandler::Ptr> preferredInputHandler = this->preferredInputHandler();

  if(preferredInputHandler && (*preferredInputHandler)->handleMouseButtonPressed(button))
    return true;
  else
    return alternativeInputHandler->handleMouseButtonPressed(button);
}

bool OptionalBranchOffInputHandler::handleMouseButtonReleased(MouseButton button)
{
  Optional<InputHandler::Ptr> preferredInputHandler = this->preferredInputHandler();

  if(preferredInputHandler && (*preferredInputHandler)->handleMouseButtonReleased(button))
    return true;
  else
    return alternativeInputHandler->handleMouseButtonReleased(button);
}

bool OptionalBranchOffInputHandler::handleMouseWheel(real wheelMovement)
{
  Optional<InputHandler::Ptr> preferredInputHandler = this->preferredInputHandler();

  if(preferredInputHandler && (*preferredInputHandler)->handleMouseWheel(wheelMovement))
    return true;
  else
    return alternativeInputHandler->handleMouseWheel(wheelMovement);
}

bool OptionalBranchOffInputHandler::handleMouseMove()
{
  Optional<InputHandler::Ptr> preferredInputHandler = this->preferredInputHandler();

  if(preferredInputHandler && (*preferredInputHandler)->handleMouseMove())
    return true;
  else
    return alternativeInputHandler->handleMouseMove();
}

bool OptionalBranchOffInputHandler::handleMouseEnter()
{
  Optional<InputHandler::Ptr> preferredInputHandler = this->preferredInputHandler();

  if(preferredInputHandler && (*preferredInputHandler)->handleMouseEnter())
    return true;
  else
    return alternativeInputHandler->handleMouseEnter();
}

bool OptionalBranchOffInputHandler::handleMouseLeave()
{
  Optional<InputHandler::Ptr> preferredInputHandler = this->preferredInputHandler();

  if(preferredInputHandler && (*preferredInputHandler)->handleMouseLeave())
    return true;
  else
    return alternativeInputHandler->handleMouseLeave();
}

bool OptionalBranchOffInputHandler::handleKeyFocusEnter()
{
  Optional<InputHandler::Ptr> preferredInputHandler = this->preferredInputHandler();

  if(preferredInputHandler && (*preferredInputHandler)->handleKeyFocusEnter())
    return true;
  else
    return alternativeInputHandler->handleKeyFocusEnter();
}

bool OptionalBranchOffInputHandler::handleKeyFocusLeave()
{
  Optional<InputHandler::Ptr> preferredInputHandler = this->preferredInputHandler();

  if(preferredInputHandler && (*preferredInputHandler)->handleKeyFocusLeave())
    return true;
  else
    return alternativeInputHandler->handleKeyFocusLeave();
}


} // namespace InputHandlers
} // namespace Gui
} // namespace Framework
