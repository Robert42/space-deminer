#include "mouse-action.h"
#include <framework/developer-tools/gui-system/window.h>

#include "branch-off-input-handler.h"

namespace Framework {
namespace Gui {
namespace InputHandlers {


MouseAction::MouseAction(Widget& widget, const Optional<InputHandler::Ptr>& inputHandler)
  : signalInputHandler(SignalInputHandler::create()),
    inputHandler(BranchOffInputHandler::create(signalInputHandler,
                                               ensureInputHandler(widget, inputHandler))),
    widget(widget),
    succeedIfReplaced(false),
    _finished(false)
{
  _isMouseInsideWidget = widget.isWidgetAtPoint(widget.currentMousePosition(false));

  signalInputHandler->signalMouseMove().connect(std::bind(&MouseAction::_checkWhetherMouseInWidget, this)).track(this->trackable);
}

MouseAction::~MouseAction()
{
  abort();
}


MouseAction::Ptr MouseAction::create(Widget& widget, const Optional<InputHandler::Ptr>& inputHandler)
{
  Ptr mouseAction = Ptr(new MouseAction(widget, inputHandler));

  if(widget.window())
    (*widget.window())->startMouseAction(mouseAction);

  return mouseAction;
}


Signals::Signal<void()>& MouseAction::signalActionFinished()
{
  return _signalActionFinished;
}

Signals::Signal<void()>& MouseAction::signalActionSucceeded()
{
  return _signalActionSucceeded;
}

Signals::Signal<void()>& MouseAction::signalActionAborted()
{
  return _signalActionAborted;
}

Signals::Signal<void()>& MouseAction::signalActionCanBeDeleted()
{
  return _signalActionCanBeDeleted;
}


Signals::Signal<void()>& MouseAction::signalMouseLeftWidget()
{
  return _signalMouseLeftWidget;
}

Signals::Signal<void()>& MouseAction::signalMouseEnteredWidgetAgain()
{
  return _signalMouseEnteredWidgetAgain;
}


void MouseAction::succeed()
{
  if(_finished)
    return;
  _finished = true;

  _signalActionSucceeded();
  _signalActionFinished();
  _signalActionCanBeDeleted();
}

void MouseAction::abort()
{
  if(_finished)
    return;
  _finished = true;

  _signalActionAborted();
  _signalActionFinished();
  _signalActionCanBeDeleted();
}

void MouseAction::replace()
{
  if(succeedIfReplaced)
    succeed();
  else
    abort();
}


MouseAction::Event* MouseAction::createKeyPressEvent(KeyCode keyCode)
{
  Event* event = _createEvent();

  signalInputHandler->signalKeyPressed().connect(std::bind(&MouseAction::callIfKey, _1, keyCode, event->handleEventSlot())).track(this->trackable);

  return event;
}

MouseAction::Event* MouseAction::createKeyReleaseEvent(KeyCode keyCode)
{
  Event* event = _createEvent();

  signalInputHandler->signalKeyReleased().connect(std::bind(&MouseAction::callIfKey, _1, keyCode, event->handleEventSlot())).track(this->trackable);

  return event;
}

MouseAction::Event* MouseAction::createKeyEvent(KeyCode keyCode, ButtonAction buttonAction)
{
  if(buttonAction == ButtonAction::PRESS)
    return createKeyPressEvent(keyCode);
  else
    return createKeyReleaseEvent(keyCode);
}


MouseAction::Event* MouseAction::createMouseButtonPressEvent(MouseButton mouseButton)
{
  Event* event = _createEvent();

  signalInputHandler->signalMouseButtonPressed().connect(std::bind(&MouseAction::callIfButton, _1, mouseButton, event->handleEventSlot())).track(this->trackable);

  return event;
}

MouseAction::Event* MouseAction::createMouseButtonReleaseEvent(MouseButton mouseButton)
{
  Event* event = _createEvent();

  signalInputHandler->signalMouseButtonReleased().connect(std::bind(&MouseAction::callIfButton, _1, mouseButton, event->handleEventSlot())).track(this->trackable);

  return event;
}

MouseAction::Event* MouseAction::createMouseButtonEvent(MouseButton mouseButton, ButtonAction buttonAction)
{
  if(buttonAction == ButtonAction::PRESS)
    return createMouseButtonPressEvent(mouseButton);
  else
    return createMouseButtonReleaseEvent(mouseButton);
}

MouseAction::Event* MouseAction::createMouseLeavesWidgetEvent()
{
  Event* event = _createEvent();

  event->eventShouldntBeHandeledByOtherHandlers = false;
  event->eventShouldBeCalledOnlyOnce = false;

  signalMouseLeftWidget().connect(event->handleEventSlot()).track(this->trackable);

  return event;
}

MouseAction::Event* MouseAction::createMouseEntersWidgetAgainEvent()
{
  Event* event = _createEvent();

  event->eventShouldntBeHandeledByOtherHandlers = false;
  event->eventShouldBeCalledOnlyOnce = false;

  signalMouseEnteredWidgetAgain().connect(event->handleEventSlot()).track(this->trackable);

  return event;
}


MouseAction::Event* MouseAction::_createEvent()
{
  Event::Ptr event = Event::create(*this);
  _events.insert(event);

  return event.get();
}


InputHandler::Ptr MouseAction::ensureInputHandler(Widget& widget,
                                                  const Optional<InputHandler::Ptr>& inputHandler)
{
  (void)widget;

  if(inputHandler)
    return *inputHandler;
  else
    return InputHandler::fallbackInputHandler();
}

InputHandler::Ptr MouseAction::inputHandlerDelegatedTo()
{
  return inputHandler;
}

bool MouseAction::_checkWhetherMouseInWidget()
{
  const vec2 mousePosition = widget.currentMousePosition(false);

  bool isMouseInsideWidget = widget.isWidgetAtPoint(mousePosition);

  if(this->_isMouseInsideWidget != isMouseInsideWidget)
  {
    this->_isMouseInsideWidget = isMouseInsideWidget;

    if(isMouseInsideWidget)
      _signalMouseEnteredWidgetAgain();
    else
      _signalMouseLeftWidget();
  }

  return false;
}


// ================

MouseAction::Event::Event(MouseAction& mouseAction)
  : mouseAction(mouseAction),
    eventShouldntBeHandeledByOtherHandlers(true),
    eventShouldBeCalledOnlyOnce(true)
{
}

MouseAction::Event::Ptr MouseAction::Event::create(MouseAction& mouseAction)
{
  return Ptr(new MouseAction::Event(mouseAction));
}

Signals::Signal<void(MouseAction&)>& MouseAction::Event::signalOccured()
{
  return _signalOccured;
}

MouseAction::Event::SignalCheckCondition& MouseAction::Event::signalCheckConditions()
{
  return _signalCheckConditions;
}

bool MouseAction::Event::handleEvent()
{
  if(!_signalCheckConditions(mouseAction))
    return false;

  _signalOccured(mouseAction);

  if(eventShouldBeCalledOnlyOnce)
    this->trackable.clear();

  return eventShouldntBeHandeledByOtherHandlers;
}


std::function<bool()> MouseAction::Event::handleEventSlot()
{
  return std::bind(&Event::handleEvent, this);
}


vec2 MouseAction::Event::currentMousePosition() const
{
  return mouseAction.widget.currentMousePosition(false);
}

bool MouseAction::Event::isMouseInsideWidget()
{
  return this->mouseAction.widget.isWidgetAtPoint(currentMousePosition());
}

bool MouseAction::Event::isMouseOutsideWidget()
{
  return !isMouseInsideWidget();
}

bool MouseAction::Event::isMouseInsideRegion(const Geometry::Region::Ptr& region)
{
  return region->contains(currentMousePosition());
}

bool MouseAction::Event::isMouseOutsideRegion(const Geometry::Region::Ptr& region)
{
  return !isMouseOutsideRegion(region);
}



void MouseAction::Event::setBehaviorSucceed()
{
  signalOccured().connect(std::bind(&MouseAction::succeed, &mouseAction)).track(this->trackable);
}

void MouseAction::Event::setBehaviorAbort()
{
  signalOccured().connect(std::bind(&MouseAction::abort, &mouseAction)).track(this->trackable);
}

void MouseAction::Event::setBehaviorAnimation(const TagSet& tags)
{
  signalOccured().connect(std::bind(&Widget::playAnimation, &mouseAction.widget, tags)).track(this->trackable);
}


void MouseAction::Event::setConditionMouseInsideWidget()
{
  signalCheckConditions().connect(std::bind(&MouseAction::Event::isMouseInsideWidget, this)).track(this->trackable);
}

void MouseAction::Event::setConditionMouseOutsideWidget()
{
  signalCheckConditions().connect(std::bind(&MouseAction::Event::isMouseOutsideWidget, this)).track(this->trackable);
}

void MouseAction::Event::setConditionMouseInsideRegion(const Geometry::Region::Ptr& region)
{
  signalCheckConditions().connect(std::bind(&MouseAction::Event::isMouseInsideRegion, this, region)).track(this->trackable);
}

void MouseAction::Event::setConditionMouseOutsideRegion(const Geometry::Region::Ptr& region)
{
  signalCheckConditions().connect(std::bind(&MouseAction::Event::isMouseOutsideRegion, this, region)).track(this->trackable);
}


Geometry::Region::Ptr MouseAction::Event::mouseNotMovedArea() const
{
  const real mouseMovementTreshold = 2;// INPUT-174 : this function uses currentMousePosition and the mouseMovementTreshold from the theme/usability-settings
  // INPUT-174 : ALso use the spatial information so it is still corrent for spatial windows

  return Geometry::Region::createCirclarRegion(Circle(currentMousePosition(), mouseMovementTreshold));
}


bool MouseAction::callIfButton(MouseButton realMouseButton, MouseButton expectedMouseButton, const std::function<bool()>& fn)
{
  if(realMouseButton == expectedMouseButton)
    return fn();
  return false;
}


bool MouseAction::callIfKey(KeyCode key, KeyCode expectedKey, const std::function<bool()>& fn)
{
  if(key == expectedKey)
    return fn();
  return false;
}


} // namespace InputHandlers
} // namespace Gui
} // namespace Framework
