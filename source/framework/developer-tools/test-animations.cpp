#include "test-animations.h"
#include <framework/developer-tools/gui-system/containers/button.h>
#include <framework/developer-tools/gui-system/containers/window-frame.h>
#include <framework/developer-tools/gui-system/layouts/box.h>
#include <framework/developer-tools/gui-system/layouts/border.h>
#include <framework/developer-tools/gui-system/layouts/button-box.h>
#include <framework/developer-tools/gui-system/widgets/canvas.h>
#include <framework/developer-tools/gui-system/widgets/label.h>
#include <framework/developer-tools/gui-system/widgets/canvas.h>
#include <framework/developer-tools/gui-system/tags.h>
#include <base/tango-colors.h>
#include <base/geometry/matrix3-transformations.h>

#include <framework/developer-tools/gui-system/animations/constant-animation.h>
#include <framework/developer-tools/gui-system/animations/fade-animation.h>


namespace Framework {


class TestAnimations::AnimationSlot final
{
public:
  typedef std::shared_ptr<AnimationSlot> Ptr;

public:
  Signals::Trackable trackable;
  const vec4 color;
  QVector<vec2> curve;
  Optional<vec2> finishPoint;

  const Gui::Animations::TimelineAnimation::Ptr& time;
  Gui::Animation::Ptr animation;

  Translation label;

  bool visible;

private:
  Signals::CallableSignal<void()> _signalReset;
  Signals::CallableSignal<void()> _signalConnected;
  Signals::CallableSignal<void()> _signalUpdated;

private:
  AnimationSlot(const Gui::Animations::TimelineAnimation::Ptr* timeAnimation, const vec4& color)
    : color(color),
      time(*timeAnimation)
  {
    this->animation = Gui::Animation::totalApplicationTimeAnimation();
    this->visible = false;
  }

public:
  static Ptr create(Gui::Animations::TimelineAnimation::Ptr* timeAnimation, const vec4& color)
  {
    return Ptr(new AnimationSlot(timeAnimation, color));
  }

  void reset()
  {
    if(animation)
    {
      curve.clear();
      trackable.clear();
      finishPoint.reset();
      animation = Gui::Animation::totalApplicationTimeAnimation();
      visible = false;

      label = Translation();

      _signalReset();
    }
  }

  void connectWith(const Translation& label, const Gui::Animation::Ptr& animation)
  {
    reset();

    this->animation = animation;
    this->label = label;
    this->visible = true;
    animation->signalUpdated().connect(std::bind(&AnimationSlot::handleSingleAnimationFrame, this)).track(this->trackable);
    animation->signalUpdated().connect(_signalUpdated).track(this->trackable);
    animation->signalFinished().connect(std::bind(&AnimationSlot::handleFinishedAnimation, this)).track(this->trackable);
    animation->signalFinished().connect(_signalUpdated).track(this->trackable);

    _signalConnected();
  }

  Signals::Signal<void()>& signalReset()
  {
    return _signalReset;
  }

  Signals::Signal<void()>& signalConnected()
  {
    return _signalConnected;
  }

  Signals::Signal<void()>& signalUpdated()
  {
    return _signalUpdated;
  }

private:
  void handleSingleAnimationFrame()
  {
    addPointToCurve();
  }

  void handleFinishedAnimation()
  {
    real time = this->time->currentValue();
    real value = this->animation->currentValue();

    finishPoint = vec2(time, value);

    signalUpdated();
  }

public:
  void addPointToCurve()
  {
    if(!visible || this->animation->finished())
      return;

    real time = this->time->currentValue();
    real value = this->animation->currentValue();

    bool pointAlreadyInCurve = !curve.empty() && curve.last().x==time;

    if(!pointAlreadyInCurve)
      curve.append(vec2(time, value));

    signalUpdated();
  }
};


class TestAnimations::AnimationSample final
{
public:
  typedef std::shared_ptr<AnimationSample> Ptr;

  typedef QPair<Translation, Gui::Animation::Ptr> AnimatinoLabelPair;

private:
  QList<AnimatinoLabelPair> _animations;

public:
  const Gui::Animations::TimelineAnimation::Ptr timeAnimation;

private:
  AnimationSample()
    : timeAnimation(Gui::Animations::TimelineAnimation::create())
  {
  }

public:
  static Ptr create()
  {
    return Ptr(new AnimationSample);
  }

  void addAnimation(const Translation& label, const Gui::Animation::Ptr& animation)
  {
    _animations.append(AnimatinoLabelPair(label, animation));
  }

  const QList<AnimatinoLabelPair>& animations()
  {
    return _animations;
  }
};




TestAnimations::AnimationSample::Ptr TestAnimations::createConstantSample()
{
  AnimationSample::Ptr sample = AnimationSample::create();

  sample->addAnimation(translate("Constant 1"), Gui::Animations::ConstantAnimation::create(1.f));
  sample->addAnimation(translate("Constant 0"), Gui::Animations::ConstantAnimation::create(0.f));

  return sample;
}

TestAnimations::AnimationSample::Ptr TestAnimations::createSimpleInterpolationSample(const FadeInterpolation& interpolation)
{
  AnimationSample::Ptr sample = AnimationSample::create();

  Gui::Animation::Ptr a, b;

  sample->addAnimation(translate("Constant 1"), a = Gui::Animations::ConstantAnimation::create(1.f));
  sample->addAnimation(translate("Constant 0"), b = Gui::Animations::ConstantAnimation::create(0.f));
  sample->addAnimation(translate("Interpolation"), Gui::Animations::FadeAnimation::create(a, b,
                                                                                          interpolation,
                                                                                          2.f,
                                                                                          4.f,
                                                                                          sample->timeAnimation,
                                                                                          true));

  return sample;
}

TestAnimations::AnimationSample::Ptr TestAnimations::createFreeFallInterpolationSample(const FreeFallPreset& freeFallPreset)
{
  AnimationSample::Ptr sample = AnimationSample::create();

  Gui::Animation::Ptr a, b;

  sample->addAnimation(translate("Constant 1"), a = Gui::Animations::ConstantAnimation::create(1.f));
  sample->addAnimation(translate("Constant 0"), b = Gui::Animations::ConstantAnimation::create(0.f));
  sample->addAnimation(translate("Interpolation"), Gui::Animations::FreeFallAnimation::createByFallDuration(sample->timeAnimation,
                                                                                                            0.f,
                                                                                                            0.f,
                                                                                                            4.f,
                                                                                                            freeFallPreset));

  return sample;
}






TestAnimations::TestAnimations()
{
  this->timeAnimation = Gui::Animations::TimelineAnimation::create();

  animationSlots.append(AnimationSlot::create(&this->timeAnimation, Tango::Orange_Middle));
  animationSlots.append(AnimationSlot::create(&this->timeAnimation, Tango::ScarletRed_Middle));
  animationSlots.append(AnimationSlot::create(&this->timeAnimation, Tango::Chameleon_Dark));
  animationSlots.append(AnimationSlot::create(&this->timeAnimation, Tango::SkyBlue_Middle));
  animationSlots.append(AnimationSlot::create(&this->timeAnimation, Tango::Plum_Middle));

  testWindow = Gui::Window::Ptr(new Gui::Window);

  windowManager.addMainWindow(testWindow);
  testWindow->setRootWidget(createGui());
}

TestAnimations::~TestAnimations()
{
}

void TestAnimations::reset()
{
  timeAnimation = Gui::Animations::TimelineAnimation::create();
  for(const AnimationSlot::Ptr& slot : animationSlots)
    slot->reset();
}

Gui::Widget::Ptr TestAnimations::createGui()
{
  Gui::Widgets::Canvas::Ptr testAnimationFunctionCanvas = Gui::Widgets::Canvas::create(std::bind(&TestAnimations::drawFunctions, this, _1, _2),
                                                                                       Gui::Widgets::Canvas::SizeRequest::SMALL_VIEW);
  Gui::Containers::WindowFrame::Ptr rootWidget = Gui::Containers::WindowFrame::create();

  Gui::Layouts::Border::Ptr mainBorder = Gui::Layouts::Border::create();
  Gui::Layouts::Box::Ptr hBoxLayout = Gui::Layouts::Box::create(Gui::Layouts::Box::Direction::HORIZONTAL);
  Gui::Layouts::Box::Ptr testVBoxLayout = Gui::Layouts::Box::create(Gui::Layouts::Box::Direction::VERTICAL);
  Gui::Layouts::Box::Ptr legendVBoxLayout = Gui::Layouts::Box::create(Gui::Layouts::Box::Direction::VERTICAL);
  Gui::Layouts::ButtonBox::Ptr vButtonBoxLayout = Gui::Layouts::ButtonBox::create(Gui::Layouts::Box::Direction::VERTICAL);

  hBoxLayout->appendSubLayout(testVBoxLayout, 1.f);
  hBoxLayout->appendSubLayout(vButtonBoxLayout);

  testVBoxLayout->appendWidget(testAnimationFunctionCanvas, 1);
  testVBoxLayout->appendSubLayout(legendVBoxLayout, 0.);

  for(const AnimationSlot::Ptr& slot : animationSlots)
    legendVBoxLayout->appendSubLayout(createLegendLabel(slot));

#define REGISTER_SAMPLE(label, name) vButtonBoxLayout->appendWidget(createSampleButton(label, std::bind(&TestAnimations::name, this)))
#define REGISTER_INTERPOLATION_SAMPLE(label, name, interpolation) vButtonBoxLayout->appendWidget(createSampleButton(label, std::bind(&TestAnimations::name, this, interpolation)))
#define REGISTER_FREEFALL_SAMPLE(label, name, preset) vButtonBoxLayout->appendWidget(createSampleButton(label, std::bind(&TestAnimations::name, this, preset)))
  REGISTER_SAMPLE(translate("Constants"), createConstantSample);
  REGISTER_INTERPOLATION_SAMPLE(translate("Linear Interpolation"), createSimpleInterpolationSample, FadeInterpolation::LINEAR);
  REGISTER_INTERPOLATION_SAMPLE(translate("Easy-In Interpolation"), createSimpleInterpolationSample, FadeInterpolation::EASY_IN);
  REGISTER_INTERPOLATION_SAMPLE(translate("Easy-Out Interpolation"), createSimpleInterpolationSample, FadeInterpolation::EASY_OUT);
  REGISTER_INTERPOLATION_SAMPLE(translate("Easy-In-And-Out Interpolation"), createSimpleInterpolationSample, FadeInterpolation::EASY_IN_AND_OUT);
  REGISTER_FREEFALL_SAMPLE(translate("Sticky Free-Fall Interpolation"), createFreeFallInterpolationSample, FreeFallPreset::STICK_TO_GROUND);
  REGISTER_FREEFALL_SAMPLE(translate("Bouncing Free-Fall Interpolation"), createFreeFallInterpolationSample, FreeFallPreset::BOUNCE);
  REGISTER_FREEFALL_SAMPLE(translate("Damping Free-Fall Interpolation"), createFreeFallInterpolationSample, FreeFallPreset::DAMPING);

  mainBorder->addLayout(hBoxLayout);

  rootWidget->addLayout(mainBorder);

  return rootWidget;
}

Gui::Layout::Ptr TestAnimations::createLegendLabel(const std::shared_ptr<AnimationSlot>& slot)
{
  vec4 color = slot->color;

  Gui::Layouts::Box::Ptr hBox = Gui::Layouts::Box::create(Gui::Layouts::Box::Direction::HORIZONTAL);

  /* ISSUE-198: Layouts should be able to handle hidden widgets
   * hBox->setVIsible(false);
   * slot->signalReset().connect(std::bind(&Gui::Layout::setVisible, hBox.get(), false)).track(hBox->trackable);
   * slot->signalConnected().connect(std::bind(&Gui::Layout::setVisible, hBox.get(), true)).track(hBox->trackable);
   */

  hBox->setThemeSpacing();
  hBox->appendWidget(Gui::Widgets::Canvas::create([color](Gui::Painter& p, const Rectangle<vec2>& b){p.drawFilledRectangle(b, color);},
                                                  Gui::Widgets::Canvas::SizeRequest::TEXT_SIZE));

  Gui::Widgets::Label::Ptr label = Gui::Widgets::Label::create();
  slot->signalConnected().connect([label, slot](){label->setLabel(slot->label);}).track(label->trackable).track(slot->trackable);
  slot->signalReset().connect(std::bind(&Gui::Widgets::Label::setLabel, label.get(), Translation())).track(label->trackable); // ISSUE-198 As long as it's not possible to hide layouts, we simply set the label to an empty string

  hBox->appendWidget(label);

  return hBox;
}

void TestAnimations::drawFunctions(Gui::Painter& painter, const Rectangle<vec2>& canvasArea)
{
  const Rectangle<vec2> drawnModelArea(vec2(0, -2), vec2(10, 2));
  const Rectangle<vec2> normalDataArea(vec2(0, -1), vec2(10, 1));

  mat3 m = drawnModelArea.mappingMatrixTo(canvasArea, true);

  painter.drawFilledRectangle(m * normalDataArea, vec4(vec3(1), 0.1f));
  painter.setScissorRect(canvasArea);


  Gui::Painter::LinePen pen = painter.startDrawingLine();

  for(const AnimationSlot::Ptr& slot : this->animationSlots)
  {
    if(!slot->visible)
      continue;

    pen.setColor(slot->color);
    pen.beginStroke();

    for(const vec2& v : slot->curve)
      pen.drawVertex(m * v);

    pen.endStroke();

    if(slot->finishPoint)
    {
      real r = painter.hiDpiFactor()*4;

      Rectangle<vec2> rect(vec2(-r), vec2(r));

      const vec2 finishPoint = *slot->finishPoint;

      rect.move(m * finishPoint);

      painter.drawFilledRectangle(rect, slot->color);
    }
  }
}


void TestAnimations::_setAnimationSample(const AnimationSample::Ptr& animationSample)
{
  reset();

  this->timeAnimation = animationSample->timeAnimation;

  this->timeAnimation->setTime(0.f);

  // If you ever into this assertion, you've created a sample needing more animations slots than avialable.
  // Just add more slots and you're fine.
  assert(animationSample->animations().size() <= this->animationSlots.size());
  int n = min(animationSample->animations().size(), this->animationSlots.size());
  for(int i : range(0, n-1))
  {
    const TestAnimations::AnimationSample::AnimatinoLabelPair& pair = animationSample->animations()[i];
    const Translation& label = pair.first;
    const Gui::Animation::Ptr& animation = pair.second;

    this->animationSlots[i]->connectWith(label, animation);
    this->animationSlots[i]->addPointToCurve();
  }

  for(real t : range(0.f, 10.f, 0.01f))
  {
    this->timeAnimation->setTime(t);
  }

  for(int i : range(0, n-1))
    this->animationSlots[i]->addPointToCurve();
}

Gui::Widget::Ptr TestAnimations::createSampleButton(const Translation& label, const std::function< std::shared_ptr<AnimationSample>() >& creator)
{
  Gui::Containers::Button::Ptr btn = Gui::Containers::Button::create(label);

  btn->signalClick().connect([creator, this](){_setAnimationSample(creator());}).track(this->trackable);

  return btn;
}


} // namespace Framework
