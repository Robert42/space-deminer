#include "terminal-history.h"

namespace Framework {
namespace DeveloperHudElements {

const size_t npos = std::numeric_limits<size_t>::max();

TerminalHistory::TerminalHistory()
{
  _maxNumberEntries = 16;
  currentHistoryIndex = npos;
}

String TerminalHistory::currentEntryToBeShown() const
{
  if(currentHistoryIndex >= recentCommands.size())
    return currentUserInput;

  StringList::const_reverse_iterator i=recentCommands.rbegin();
  size_t j=recentCommands.size()-1;

  while(j > currentHistoryIndex)
  {
    assert(i != recentCommands.rend());
    ++i;
    j--;
  }

  return *i;
}

void TerminalHistory::moveToPreviousCommand(const String& currentUserInput)
{
  if(currentHistoryIndex >= recentCommands.size())
  {
    this->currentUserInput = currentUserInput;
    currentHistoryIndex = recentCommands.size()-1;
  }else if(currentHistoryIndex>0)
  {
    currentHistoryIndex--;
  }
}

void TerminalHistory::moveToFollowingCommand()
{
  if(currentHistoryIndex >= recentCommands.size())
    return;

  if(currentHistoryIndex == recentCommands.size()-1)
  {
    currentHistoryIndex = npos;
  }else
  {
    currentHistoryIndex++;
  }
}

void TerminalHistory::appendCommand(const String& currentUserInput)
{
  currentHistoryIndex = npos;
  this->currentUserInput.clear();

  if(recentCommands.empty() || *recentCommands.rbegin()!=currentUserInput)
  {
    while(recentCommands.size()>=static_cast<size_t>(_maxNumberEntries))
      recentCommands.pop_front();
    recentCommands.push_back(currentUserInput);
  }
}

int TerminalHistory::maxNumberEntries() const
{
  return _maxNumberEntries;
}

void TerminalHistory::setMaxNumberEntries(int maxNumberEntries)
{
  this->_maxNumberEntries = clamp(maxNumberEntries, 4, 256);
}


}
}
