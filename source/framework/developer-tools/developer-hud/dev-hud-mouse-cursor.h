#ifndef FRAMEWORK_DEVELOPERHUDELEMENTS_MOUSECURSOR_H
#define FRAMEWORK_DEVELOPERHUDELEMENTS_MOUSECURSOR_H

#include <framework/developer-tools/developer-hud.h>
#include <framework/window/user-input.h>
#include <framework/window/user-input/mouse-mode-layers.h>
#include <framework/window/user-input/mouse-cursor.h>

namespace Framework {
namespace DeveloperHudElements {

class BaseMouseCursor : public DeveloperHud::Layer
{
public:
  typedef std::shared_ptr<BaseMouseCursor> Ptr;

protected:
  class Cursor
  {
  public:
    ivec2 hotspot;
    Gorilla::Sprite* sprite;

    Cursor(Gorilla::Screen& gorillaScreen, const std::string& name);
  };

  class CursorSet
  {
  private:
    friend class BaseMouseCursor;
    BaseMouseCursor* baseMouseCursor;

  public:
    typedef shared_ptr<CursorSet> Ptr;

    CursorSet();
    virtual ~CursorSet();

    virtual const Cursor& dummy() = 0;
    virtual const Cursor& defaultCursor()
    {
      return dummy();
    }
    virtual void init() = 0;

  protected:
    void initDefaultCursor(const DefaultMouseCursor::Ptr& cursor, const BaseMouseCursor::Cursor& c);
  };

public:
  Signals::Trackable trackable;

private:
  const CursorSet::Ptr cursorSet;

  const Cursor* currentCursor;
  Gorilla::Rectangle* cursor;

  real _scale;

public:
  BaseMouseCursor(Layer::ZLayer zlayer, Gorilla::Screen& gorillaScreen, const CursorSet::Ptr& cursorSet);
  virtual ~BaseMouseCursor();

private:
  void setCursor(const Cursor& cursor);
  void unsetCursor();

  bool updatePosition();

  void determineVisibility();

  real scale() const;
  void setScale(real scale);

  void updateCursorSize();

private:
  void setCursorPtr(const Cursor* cursor);
};

class DefaultMouseCursor : public BaseMouseCursor
{
private:
  class DefaultCursorSet : public CursorSet
  {
  public:
    Cursor _noCursor;
    Cursor _defaultCursor;
    Cursor _textCursor;
    Cursor _moveCursor;
    Cursor _moveNSCursor;
    Cursor _moveWECursor;
    Cursor _moveSwNeCursor;
    Cursor _moveNwSeCursor;

  private:
    DefaultCursorSet(Gorilla::Screen& gorillaScreen);

  public:
    static Ptr create(Gorilla::Screen& gorillaScreen);

  public:
    const Cursor& dummy() override;
    const Cursor& defaultCursor() override;
    void init() override;
  };

public:
  DefaultMouseCursor(DeveloperHud& hud);
};

} // namespace DeveloperHudElements
} // namespace Framework

#endif // FRAMEWORK_DEVELOPERHUDELEMENTS_MOUSECURSOR_H
