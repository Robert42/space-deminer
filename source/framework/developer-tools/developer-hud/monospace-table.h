#ifndef _FRAMEWORK_DEVELOPER_HUD_MONOSPACETABLE_H_
#define _FRAMEWORK_DEVELOPER_HUD_MONOSPACETABLE_H_

#include <dependencies.h>

#include <base/strings/string.h>

namespace Framework {

using namespace  Base;

class MonospaceTable
{
private:
  Gorilla::Layer& gorillaLayer;

  Gorilla::Caption* titleCaption;
  Gorilla::Rectangle* background;

  Gorilla::GlyphData* glyphData;

  const int glyphIndexDefault;
  const int glyphIndexEmphasized;
  const Ogre::ColourValue colorDefault;

  std::vector<int> columnPositions;
  std::vector<int> columnWidths;

  ivec2 position(int c, int r);
  ivec2 subPosition(int c, int r, int character);

  void adaptBackgroundSize(int c, int r);

  int numberColumns, numberRows;

  Gorilla::Caption& createCaption(int x, int y, const String& text, bool emphasize, Gorilla::TextAlignment align);

public:
  MonospaceTable(Gorilla::Layer& gorillaLayer, const String& title, int glyphIndexDefault, int glyphIndexEmphasized, const Ogre::ColourValue& colorDefault);
  MonospaceTable(Gorilla::Layer& gorillaLayer, const String& title);

public:
  void addColumn(int width);

  Gorilla::Caption& addText(int c, int r, const String& text, bool emphasize = false, Gorilla::TextAlignment align = Gorilla::TextAlign_Left);
  Gorilla::Caption& addValue(int c, int r, const String& unit, bool emphasize = false);
};


}


#endif
