#include "framerate-screen.h"

#include <base/strings/string.h>
#include <base/tango-colors.h>

#include <base/scripting/engine.h>

namespace Framework {


FramerateScreen::FramerateScreen(DeveloperHud& hud)
  : DeveloperHud::MonospaceLayer(ZLayer::FPS, hud),
    table(gorillaLayer(), "Framerate")
{
  FrameSignals::signalFrameStart().connect(std::bind(&FramerateScreen::update, this)).track(this->trackable);

  table.addColumn(8);
  table.addColumn(10);
  table.addColumn(10);
  table.addColumn(10);
  table.addColumn(10);

  table.addText(2, 0, "min", false, Gorilla::TextAlign_Centre);
  table.addText(3, 0, "max", false, Gorilla::TextAlign_Centre);

  table.addText(0, 1, "unlocked");
  table.addText(0, 2, "current");
  table.addText(0, 3, "smooth");

  potentialFpsCaption = &table.addValue(1, 1, "fps");
  currentFpsCaption = &table.addValue(1, 2, "fps");
  averageFpsCaption = &table.addValue(1, 3, "fps");

  minPotentialFpsCaption = &table.addValue(2, 1, "fps");
  minCurrentFpsCaption = &table.addValue(2, 2, "fps");
  minAverageFpsCaption = &table.addValue(2, 3, "fps");

  maxPotentialFpsCaption = &table.addValue(3, 1, "fps");
  maxCurrentFpsCaption = &table.addValue(3, 2, "fps");
  maxAverageFpsCaption = &table.addValue(3, 3, "fps");

  potentialMillisecondCaption = &table.addValue(4, 1, "ms");
  currentMillisecondCaption = &table.addValue(4, 2, "ms");
  averageMillisecondCaption = &table.addValue(4, 3, "ms");

  minMaxReadIndex = 0;
  minMaxWriteIndex = 1;

  resetMinMaxValues();
  swap(minMaxReadIndex, minMaxWriteIndex);
  resetMinMaxValues();
}

FramerateScreen::~FramerateScreen()
{
}

void FramerateScreen::update()
{
  if(!isVisible())
    return;

  int potentialFPS = round(FrameTimer::microsecondsToFramerate(FrameTimer::frameTimeWithoutLocking()));
  int currentFps = round(FrameTimer::microsecondsToFramerate(FrameTimer::frameTime()));
  int averageFps = round(1.f/FrameTimer::averageTime());

  if(simpleMinMaxTimer.tick(FrameTimer::averageTime()))
  {
    minPotentialFpsCaption->text(toString(minPotentialFps()).toOgreString());
    minCurrentFpsCaption->text(toString(minCurrentFps()).toOgreString());
    minAverageFpsCaption->text(toString(minAverageFps()).toOgreString());
    maxPotentialFpsCaption->text(toString(maxPotentialFps()).toOgreString());
    maxCurrentFpsCaption->text(toString(maxCurrentFps()).toOgreString());
    maxAverageFpsCaption->text(toString(maxAverageFps()).toOgreString());

    adaptColor(minPotentialFpsCaption, minPotentialFps());
    adaptColor(maxPotentialFpsCaption, maxPotentialFps());
    adaptColor(minCurrentFpsCaption, minCurrentFps());
    adaptColor(maxCurrentFpsCaption, maxCurrentFps());
    adaptColor(minAverageFpsCaption, minAverageFps());
    adaptColor(maxAverageFpsCaption, maxAverageFps());

    swap(minMaxReadIndex, minMaxWriteIndex);

    resetMinMaxValues();
  }

  potentialFpsCaption->text(toString(potentialFPS).toOgreString());
  currentFpsCaption->text(toString(currentFps).toOgreString());
  averageFpsCaption->text(toString(averageFps).toOgreString());

  potentialMillisecondCaption->text(toString(round(real(1.e-2)*FrameTimer::frameTimeWithoutLocking())*real(0.1)).toOgreString());
  currentMillisecondCaption->text(toString(round(real(1.e-2)*FrameTimer::frameTime())*real(0.1)).toOgreString());
  averageMillisecondCaption->text(toString(round(real(1.e4)*FrameTimer::averageTime())*real(0.1)).toOgreString());


  minPotentialFps() = min(minPotentialFps(), potentialFPS);
  minCurrentFps() = min(minCurrentFps(), currentFps);
  minAverageFps() = min(minAverageFps(), averageFps);
  maxPotentialFps() = max(maxPotentialFps(), potentialFPS);
  maxCurrentFps() = max(maxCurrentFps(), currentFps);
  maxAverageFps() = max(maxAverageFps(), averageFps);

  adaptColor(potentialFpsCaption, potentialFPS);
  adaptColor(potentialMillisecondCaption, potentialFPS);
  adaptColor(currentFpsCaption, currentFps);
  adaptColor(currentMillisecondCaption, currentFps);
  adaptColor(averageFpsCaption, averageFps);
  adaptColor(averageMillisecondCaption, averageFps);
}


void FramerateScreen::adaptColor(Gorilla::Caption* caption, int fps)
{
  if(fps < 16)
    caption->colour(Ogre::ColourValue_cast(Tango::ScarletRed_Light));
  else if(fps < 30)
    caption->colour(Ogre::ColourValue_cast(Tango::Butter_Light));
  else if(fps > 1000)
    caption->colour(Ogre::ColourValue_cast(Tango::SkyBlue_Light));
  else
    caption->colour(Ogre::ColourValue(1.0f, 1.0f, 1.0f));
}


void FramerateScreen::resetMinMaxValues()
{
  minPotentialFps() = std::numeric_limits<int>::max();
  minCurrentFps() = std::numeric_limits<int>::max();
  minAverageFps() = std::numeric_limits<int>::max();
  maxPotentialFps() = std::numeric_limits<int>::min();
  maxCurrentFps() = std::numeric_limits<int>::min();
  maxAverageFps() = std::numeric_limits<int>::min();
}

int& FramerateScreen::minPotentialFps()
{
  return minPotentialFps_[minMaxWriteIndex];
}

int& FramerateScreen::minCurrentFps()
{
  return minCurrentFps_[minMaxWriteIndex];
}

int& FramerateScreen::minAverageFps()
{
  return minAverageFps_[minMaxWriteIndex];
}

int& FramerateScreen::maxPotentialFps()
{
  return maxPotentialFps_[minMaxWriteIndex];
}

int& FramerateScreen::maxCurrentFps()
{
  return maxCurrentFps_[minMaxWriteIndex];
}

int& FramerateScreen::maxAverageFps()
{
  return maxAverageFps_[minMaxWriteIndex];
}

void FramerateScreen::registerTerminalCommands(const std::shared_ptr<Scripting::Index>&)
{
  int r = 0;
  AngelScript::asIScriptEngine* scriptEngine = Scripting::Engine::angelScriptEngine();

  // ==== ::
  r = scriptEngine->RegisterGlobalFunction("void set_showFramerate(bool visible)",
                                           AngelScript::asMETHOD(FramerateScreen, setVisible),
                                           AngelScript::asCALL_THISCALL_ASGLOBAL,
                                           this);
  Scripting::AngelScriptCheck(r);
  r = scriptEngine->RegisterGlobalFunction("bool get_showFramerate()",
                                           AngelScript::asMETHOD(FramerateScreen, isVisible),
                                           AngelScript::asCALL_THISCALL_ASGLOBAL,
                                           this);
  Scripting::AngelScriptCheck(r);
}



}
