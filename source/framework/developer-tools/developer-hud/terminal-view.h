#ifndef _FRAMEWORK_DEVELOPER_HUD_TERMINAL_VIEW_H_
#define _FRAMEWORK_DEVELOPER_HUD_TERMINAL_VIEW_H_

#include <dependencies.h>

#include <framework/developer-tools/terminal/buffer/terminal-format.h>
#include <framework/developer-tools/terminal/terminal-content.h>
#include <framework/window/user-input/keyboard.h>
#include <framework/window/user-input/mouse.h>
#include <framework/developer-tools/developer-hud.h>
#include <framework/developer-tools/gorilla-helper/primitive-renderer.h>

#include <base/containers/lifetime-controlled-stack.h>

namespace Framework {
using namespace  Base;

class RenderWindow;

namespace DeveloperHudElements {
typedef Containers::LifetimeControlledStack<Terminal::Buffer::Ptr> BufferStack;
typedef BufferStack::StackElement::Ptr BufferStackElementPtr;

class PrivateTerminalView
{
public:
  typedef shared_ptr<PrivateTerminalView> Ptr;

private:
  BufferStackElementPtr stackElement;

  Terminal::Buffer::Ptr _buffer;
  const Terminal::Viewport& viewport;

private:
  friend class TerminalView;

  PrivateTerminalView(const BufferStackElementPtr& stackElement,
                      Terminal::Viewport& viewport)
    : stackElement(stackElement),
      _buffer(stackElement->value),
      viewport(viewport)
  {
  }

public:
  int widthInChars() const
  {
    return viewport.width();
  }

  int heightInChars() const
  {
    return viewport.height();
  }

  const Terminal::Buffer::Ptr& buffer() const
  {
    return _buffer;
  }
};


class TerminalView final
{
private:
  typedef QMap<Terminal::Format, vec4> FormatColors;
  typedef DeveloperHud::MonospaceLayer::Font Font;

public:
  Signals::Trackable trackable;

private:
  bool active;

  Terminal::Buffer::Ptr buffer;
  std::shared_ptr<Terminal::Viewport> terminalViewport;
  std::shared_ptr<Terminal::Content> terminalContent;

  int x, y, width, height;
  real mouseScrollingPosition;
  real mouseScrollDirection;
  real mouseGrabDirection;

  Font& fontSmall, &fontLarge;
  const Font* font;
  int fontFactor;

  Gorilla::Layer& gorillaLayer;

  FormatColors terminalColors;

  class TextRenderer : public GorillaHelper::PrimitiveRenderer
  {
  public:
    TerminalView& terminalView;

  public:
    TextRenderer(TerminalView& terminalView);

    void _redrawImplementation() override;

    void makeDirty();
  };

  TextRenderer* textRenderer;

  shared_ptr<Terminal::Cursor> userInputCursor;
  Gorilla::Rectangle* gorillaCursor;

  BufferStackElementPtr mainBuffer;
  BufferStack bufferStack;

public:
  TerminalView(Gorilla::Layer& gorillaLayer,
               Font& fontSmall,
               Font& fontLarge,
               const shared_ptr<Terminal::Cursor>& userInputCursor);
  ~TerminalView();

  void setAllocation(int x, int y, int width, int height);

  int lineHeight() const;
  int lineSpacing() const;

  int widthInChars() const;
  int heightInChars() const;

  void setFontSize(int fontSize);
  int fontSize() const;

  void setInvertScrollDirection(bool invert);
  bool invertScrollDirection() const;

  void setInvertGrabDirection(bool invert);
  bool invertGrabDirection() const;

  PrivateTerminalView::Ptr createPrivateTerminalView();

  bool isActive() const;
  void setActive(bool active);

private:
  void markDirty();
  void redrawNow();

  void updateTerminalViewportSize();

  void recalculateCursor(bool sizeWasChanged);

private:
  bool handleKey(KeyCode key);
  bool handleMouseMovement();
  bool handleMouseWheel();

  void initFormatColor(Terminal::Format::Color color);
  void initFormat(const Terminal::Format& format);
  static vec4 asRgbaColor(const Terminal::Format& format);

  void setBuffer();
};


}
}

#endif
