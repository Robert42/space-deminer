#ifndef _FRAMEWORK_DEVELOPER_HUD_TERMINAL_HISTORY_H_
#define _FRAMEWORK_DEVELOPER_HUD_TERMINAL_HISTORY_H_

#include <base/strings/string.h>

namespace Framework {
using namespace Base;

namespace DeveloperHudElements {


class TerminalHistory
{
private:
  typedef std::list<String> StringList;

  StringList recentCommands;
  size_t currentHistoryIndex;
  int _maxNumberEntries;

public:
  String currentUserInput;

public:
  TerminalHistory();

  String currentEntryToBeShown()const;

  void moveToPreviousCommand(const String& currentUserInput);
  void moveToFollowingCommand();
  void appendCommand(const String& currentUserInput);

  int maxNumberEntries() const;
  void setMaxNumberEntries(int _maxNumberEntries);
};


}
}

#endif
