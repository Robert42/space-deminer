#include "terminal-input.h"

#include <framework/developer-tools/terminal.h>
#include <framework/developer-tools/terminal/terminal-cursor.h>
#include <framework/developer-tools/terminal/terminal-manager.h>
#include <framework/scripting/terminal-syntax-highlighter.h>

#include <base/scripting/script-autocompletion.h>

namespace Framework {
namespace DeveloperHudElements {

TerminalUserInput::TerminalUserInput()
  : interactiveArea(Terminal::InteractiveArea::Order::TERMINAL_USER_INPUT)
{
  active = false;

  scriptIndex = Scripting::Index::create();

  dummyBuffer = Terminal::Buffer::create();
  _cursor.reset(new Terminal::Cursor(dummyBuffer));

  greenPrefix = "space-deminer ";
  bluePrefix = "dev-terminal $";
  normalPrefix = " ";
  prefixLength = greenPrefix.length() + bluePrefix.length() + normalPrefix.length();

  Keyboard::signalKeyPressed().connect(InputDevice::ActionPriority::TERMINAL,
                                       std::bind(&TerminalUserInput::handleKey, this, _1)).track(this->trackable);
  Keyboard::signalUnicodeEntered().connect(InputDevice::ActionPriority::TERMINAL,
                                           std::bind(&TerminalUserInput::handleUnicodeInput, this, _1)).track(this->trackable);

  signalCommandEdited().connect(std::bind(&TerminalUserInput::highlightSyntax, this)).trackManually();
}

bool TerminalUserInput::isActive() const
{
  return active;
}

void TerminalUserInput::setActive(bool active)
{
  active = callableSignalFilterActive(active);

  if(isActive() == active)
    return;

  this->active = active;

  if(active)
  {
    interactiveArea.writeOut(Terminal::Format(Terminal::Format::Color::GREEN, Terminal::Format::Light::BRIGHTER), greenPrefix);
    interactiveArea.writeOut(Terminal::Format(Terminal::Format::Color::BLUE, Terminal::Format::Light::BRIGHTER), bluePrefix);
    interactiveArea.writeOut(normalPrefix);
    interactiveArea.writeOut(" ");

    Terminal::Cursor& start = *cursor();
    Terminal::Cursor end(start);
    interactiveArea.tryGetCursors(out(start), out(end));
    cursor()->moveToPositionInLine(firstPossibleCursorPosition());
  }else
  {
    cursor()->reset(dummyBuffer);
    interactiveArea.clear();
  }
}

String TerminalUserInput::typedInText() const
{
  String fullText = interactiveArea.asPlainText();

  const String::value_type* begin, *end;

  begin = &fullText[firstPossibleCursorPosition()];
  end = &fullText[lastPossibleCursorPosition()];

  return String(begin, end);
}


TerminalUserInput::CommandSignal& TerminalUserInput::signalCommandExecuted()
{
  return callableSignalCommandExecuted;
}


TerminalUserInput::EditingSignal& TerminalUserInput::signalCommandEdited()
{
  return callableSignalCommandEdited;
}


TerminalUserInput::FilterActiveSignal& TerminalUserInput::signalFilterActive()
{
  return callableSignalFilterActive;
}


size_t TerminalUserInput::firstPossibleCursorPosition() const
{
  return prefixLength;
}

size_t TerminalUserInput::lastPossibleCursorPosition() const
{
  return cursor()->line()->length()-1;
}

const shared_ptr<Terminal::Cursor>& TerminalUserInput::cursor() const
{
  return _cursor;
}

size_t TerminalUserInput::clampCursorPosition(size_t position) const
{
  if(!isActive())
    return 0;

  return clamp<size_t>(position,
                       firstPossibleCursorPosition(),
                       lastPossibleCursorPosition());
}

void TerminalUserInput::moveCursorRelative(int relativePosition)
{
  moveCursorAbsolute(cursor()->positionInLine()+relativePosition);
}

void TerminalUserInput::moveCursorAbsolute(int absolutePosition)
{
  cursor()->moveToPositionInLine(clampCursorPosition(absolutePosition));
}

void TerminalUserInput::moveCursorToNextToken(int direction)
{
  if(direction == 0)
    return;

  moveCursorAbsolute(absoluteIndexOfNextToken(direction));
}

void TerminalUserInput::deleteToNextToken(int direction)
{
  if(direction == 0)
    return;

  cursor()->deleteCharactersTo(absoluteIndexOfNextToken(direction));
}

bool TerminalUserInput::handleKey(KeyCode key)
{
  if(!isActive())
    return false;

  bool onlyControl = Keyboard::areOnlyModifierDown(KeyModifier::CONTROL);
  bool noModifier = !Keyboard::isAnyModifierDown();

  switch(key.value)
  {
  case KeyCode::RETURN:
    if(noModifier)
    {
      String typedInText = this->typedInText();
      writeTypedTextOut(typedInText);
      callableSignalCommandExecuted(typedInText);
      return true;
    }
    return false;
  case KeyCode::C:
    if(onlyControl)
    {
      writeTypedTextOut();
      setActive(false);
      setActive(true);
      return true;
    }
    return false;
  case KeyCode::ARROW_LEFT:
    if(noModifier)
    {
      moveCursorRelative(-1);
      return true;
    }else if(onlyControl)
    {
      moveCursorToNextToken(-1);
      return true;
    }
    return false;
  case KeyCode::ARROW_RIGHT:
    if(noModifier)
    {
      moveCursorRelative(1);
      return true;
    }else if(onlyControl)
    {
      moveCursorToNextToken(1);
      return true;
    }
    return false;
  case KeyCode::HOME:
    if(noModifier)
    {
      moveCursorAbsolute(0);
      return true;
    }
    return false;
  case KeyCode::TAB:
    if(noModifier || onlyControl)
    {
      autocomplete();
      return true;
    }
  case KeyCode::END:
    if(noModifier)
    {
      moveCursorAbsolute(lastPossibleCursorPosition());
      return true;
    }
    return false;
  case KeyCode::BACKSPACE:
    if(noModifier)
    {
      if(cursor()->positionInLine() > firstPossibleCursorPosition())
      {
        cursor()->deleteLeadingCharacter();
        callableSignalCommandEdited();
      }
      return true;
    }else if(onlyControl)
    {
      deleteToNextToken(-1);
      return true;
    }
    return false;
  case KeyCode::DELETE:
    if(noModifier)
    {
      if(cursor()->positionInLine() < lastPossibleCursorPosition())
      {
        cursor()->deleteFollowingCharacter();
        callableSignalCommandEdited();
      }
      return true;
    }else if(onlyControl)
    {
      deleteToNextToken(1);
      return true;
    }
    return false;
  default:
    return false;
  }
}

void TerminalUserInput::clearUserInput()
{
  moveCursorAbsolute(0);
  while(firstPossibleCursorPosition() != lastPossibleCursorPosition())
    cursor()->deleteFollowingCharacter();
}

void TerminalUserInput::resetUserInput(const String& newInput)
{
  clearUserInput();
  cursor()->insertString(newInput);
  callableSignalCommandEdited();
}

void TerminalUserInput::writeTypedTextOut(const String& typedInText) const
{
  Terminal::ensureBeginningOfLine();
  Terminal::writeOut(Terminal::Format(Terminal::Format::Color::GREEN, Terminal::Format::Light::BRIGHTER), greenPrefix);
  Terminal::writeOut(Terminal::Format(Terminal::Format::Color::BLUE, Terminal::Format::Light::BRIGHTER), bluePrefix);
  Terminal::writeOut(normalPrefix);
  Scripting::SyntaxHightlighting::writeOut(Terminal::Manager::defaultOutput(), typedInText);
  Terminal::writeOut("\n");
}

void TerminalUserInput::writeTypedTextOut() const
{
  writeTypedTextOut(typedInText());
}

bool TerminalUserInput::handleUnicodeInput(String::value_type unicode)
{
  if(!isActive() || unicode < 32)
    return false;

  /* There's a bug, that if the terminal gets closed using the command `Terminal::visible=false` and shown again by pressing [^]
   * A '^' character gets misstakenly printed to the terminal.
   * Debugging would take some time, so I decided to add this hack, which simply prevents any '^' sign to be typed into the terminal.
   * When switching to CEGUI, I assume the terminal will be completely replaced anyway, so debugging might be a waste of time.
   */
  if(unicode == '^')
    return false;

  cursor()->insertCharacter(unicode);
  callableSignalCommandEdited();

  return true;
}

void TerminalUserInput::highlightSyntax()
{
  Scripting::SyntaxHightlighting::reformat(cursor()->line(),
                                           firstPossibleCursorPosition(),
                                           lastPossibleCursorPosition());
}

void TerminalUserInput::autocomplete()
{
  Base::Scripting::TokenizedScript tokenizedScript(typedInText());
  const size_t currentPosition = cursor()->positionInLine() - firstPossibleCursorPosition();

  Scripting::Autocompletion autocompletion(scriptIndex);

  auto insertCharactersIfPossible = [this](std::set<std::string>& result, const std::string &identifierToBeCompleted){
    if(!result.empty())
    {
      std::string commonStringToInsert = result.begin()->substr(identifierToBeCompleted.length());

      for(const std::string& possibleCompletion : result)
      {
        std::string completionForCurrent = possibleCompletion.substr(identifierToBeCompleted.length());

        size_t common;
        size_t maxNumCommon = min(commonStringToInsert.length(), completionForCurrent.length());
        for(common=0; common<maxNumCommon; ++common)
          if(completionForCurrent[common]!=commonStringToInsert[common])
            break;

        commonStringToInsert = commonStringToInsert.substr(0, common);

        if(commonStringToInsert.empty())
          return;
      }

      cursor()->insertString(String::fromUtf8(commonStringToInsert));
    }
  };

  std::set<std::string> possibleCompletions = autocompletion.autoComplete(tokenizedScript, currentPosition, insertCharactersIfPossible);

  if(possibleCompletions.size() > 1)
  {
    writeTypedTextOut();

    for(const std::string& possibleCompletion : possibleCompletions)
      Terminal::writeOut("%1\n", String::fromUtf8(possibleCompletion));
  }
}

size_t TerminalUserInput::indexOfNextToken(int direction) const
{
  Base::Scripting::TokenizedScript tokenizedScript(typedInText());
  const size_t currentPosition = cursor()->positionInLine() - firstPossibleCursorPosition();

  if(direction == 0)
    return currentPosition;

  size_t prevToken = 0;

  for(const Base::Scripting::TokenizedScript::Token& token : tokenizedScript.tokens())
  {
    const size_t indexBegin = token.indexBegin;
    const size_t indexEnd = indexBegin + token.length();

    bool atBegin = indexBegin == currentPosition;
    bool withinToken = indexBegin < currentPosition && indexEnd > currentPosition;

    if(direction < 0)
    {

      if(atBegin)
        return prevToken;
      else if(withinToken)
        return indexBegin;

    }else
    {

      if(atBegin || withinToken)
        return indexEnd;

    }

    prevToken = indexBegin;

  }

  if(direction < 0)
    return prevToken;

  // If no token was found, this means we are at the last possibl position
  return currentPosition;
}

size_t TerminalUserInput::absoluteIndexOfNextToken(int direction) const
{
  return clampCursorPosition(indexOfNextToken(direction) + firstPossibleCursorPosition());
}

}
}
