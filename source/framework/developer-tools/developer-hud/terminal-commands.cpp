#include "terminal-commands.h"
#include "terminal-hud.h"
#include "terminal-input.h"

#include <framework/application.h>
#include <framework/window/user-input/mouse-mode-layers.h>
#include <framework/scripting/initialization.h>
#include <framework/frame-timer.h>
#include <framework/private-rendering-loop.h>
#include <framework/developer-tools/terminal.h>
#include <framework/developer-tools/developer-tools.h>
#include <framework/window/render-window.h>
#include <framework/window/user-input/mouse.h>
#include <framework/window/user-input/mouse-cursor.h>
#include <framework/window/user-input/mouse-cursor-layers.h>

#include <base/scripting/script-index.h>
#include <base/i18n/language.h>

namespace Framework {

namespace ProceduralAssets {
namespace GuiThemes {

void testExportingDarkTheme(const std::string& filename, bool largeWidgets, bool fillBackground);
void testDarkThemeTextureAtlas(const std::string& directory, bool largeWidgets);
void testDarkThemeXml(const std::string& xmlFilename);
void testExportingMouseCursor(const std::string& filename, const ivec2& size);
void testMouseCursorTextureAtlas(const std::string& directory, const ivec2& size);

} // namespace ProceduralAssets
} // namespace GuiThemes

void testExportCeguiLooknfeel(const std::string& prefix, const std::string& filename);
void runUnitTests(PrivateRenderLoop& privateRenderLoop, bool fast);

namespace DeveloperHudElements {

void createProblematicTexture();
void testLanguageChange();
void exit();

const uint32 terminalAccessMask = Scripting::AccessMasks::TERMINAL_COMMANDS | Scripting::AccessMasks::MATH;

TerminalCommands::TerminalCommands()
{
  int r;
  terminalHud = nullptr;

  AngelScript::asIScriptEngine* engine = Scripting::Engine::angelScriptEngine();

  scriptModule = engine->GetModule("terminal-commands", AngelScript::asGM_CREATE_IF_NOT_EXISTS);
  scriptModule->SetAccessMask(terminalAccessMask);

  r = scriptModule->Build();
  Scripting::AngelScriptCheck(r);
}

void TerminalCommands::registerTerminalCommands(TerminalHud& hud)
{
  int r;
  this->terminalHud = &hud;

  DeveloperTools* developerTools = &Application::developerTools();

  AngelScript::asIScriptEngine* scriptEngine = scriptModule->GetEngine();

  scriptModule->AddScriptSection("temp-test",
R"script(
namespace Terminal
{
void showAllFormats()
{
string s = "Hello World 123 +-/();";

Terminal::print(s, Terminal::Color::Normal, Terminal::Lightness::Darker);
Terminal::print(s, Terminal::Color::Normal, Terminal::Lightness::Normal);
Terminal::print(s, Terminal::Color::Normal, Terminal::Lightness::Brighter);
Terminal::print("\n");
Terminal::print(s, Terminal::Color::Red, Terminal::Lightness::Darker);
Terminal::print(s, Terminal::Color::Red, Terminal::Lightness::Normal);
Terminal::print(s, Terminal::Color::Red, Terminal::Lightness::Brighter);
Terminal::print("\n");
Terminal::print(s, Terminal::Color::Green, Terminal::Lightness::Darker);
Terminal::print(s, Terminal::Color::Green, Terminal::Lightness::Normal);
Terminal::print(s, Terminal::Color::Green, Terminal::Lightness::Brighter);
Terminal::print("\n");
Terminal::print(s, Terminal::Color::Blue, Terminal::Lightness::Darker);
Terminal::print(s, Terminal::Color::Blue, Terminal::Lightness::Normal);
Terminal::print(s, Terminal::Color::Blue, Terminal::Lightness::Brighter);
Terminal::print("\n");
Terminal::print(s, Terminal::Color::Yellow, Terminal::Lightness::Darker);
Terminal::print(s, Terminal::Color::Yellow, Terminal::Lightness::Normal);
Terminal::print(s, Terminal::Color::Yellow, Terminal::Lightness::Brighter);
Terminal::print("\n");
Terminal::print(s, Terminal::Color::Orange, Terminal::Lightness::Darker);
Terminal::print(s, Terminal::Color::Orange, Terminal::Lightness::Normal);
Terminal::print(s, Terminal::Color::Orange, Terminal::Lightness::Brighter);
Terminal::print("\n");
Terminal::print(s, Terminal::Color::Magenta, Terminal::Lightness::Darker);
Terminal::print(s, Terminal::Color::Magenta, Terminal::Lightness::Normal);
Terminal::print(s, Terminal::Color::Magenta, Terminal::Lightness::Brighter);
Terminal::print("\n");
Terminal::print(s, Terminal::Color::Cyan, Terminal::Lightness::Darker);
Terminal::print(s, Terminal::Color::Cyan, Terminal::Lightness::Normal);
Terminal::print(s, Terminal::Color::Cyan, Terminal::Lightness::Brighter);
Terminal::print("\n");
}
}
)script");

  uint32 oldMask = scriptEngine->SetDefaultAccessMask(Scripting::AccessMasks::TERMINAL_COMMANDS);

  scriptEngine->RegisterGlobalFunction("void runTests(bool fast=false)",
                                       AngelScript::asMETHOD(TerminalCommands, runTests),
                                       AngelScript::asCALL_THISCALL_ASGLOBAL,
                                       this);

  scriptEngine->RegisterGlobalFunction("void parseAngelScriptToken(const string &in token)",
                                       AngelScript::asMETHOD(TerminalCommands, parseAngelScriptToken),
                                       AngelScript::asCALL_THISCALL_ASGLOBAL,
                                       this);

  scriptEngine->RegisterGlobalFunction("void help()",
                                       AngelScript::asMETHOD(TerminalCommands, help),
                                       AngelScript::asCALL_THISCALL_ASGLOBAL,
                                       this);

  // ==== Terminal::
  r = scriptEngine->SetDefaultNamespace("Terminal");
  Scripting::AngelScriptCheck(r);

  scriptEngine->RegisterGlobalFunction("void set_fontSize(int large)",
                                       AngelScript::asMETHOD(TerminalHud, setFontSize),
                                       AngelScript::asCALL_THISCALL_ASGLOBAL,
                                       terminalHud);
  scriptEngine->RegisterGlobalFunction("int get_fontSize()",
                                       AngelScript::asMETHOD(TerminalHud, fontSize),
                                       AngelScript::asCALL_THISCALL_ASGLOBAL,
                                       terminalHud);

  scriptEngine->RegisterGlobalFunction("void set_invertScrollDirection(bool invert)",
                                       AngelScript::asMETHOD(TerminalView, setInvertScrollDirection),
                                       AngelScript::asCALL_THISCALL_ASGLOBAL,
                                       terminalHud->terminalView.get());
  scriptEngine->RegisterGlobalFunction("bool get_invertScrollDirection()",
                                       AngelScript::asMETHOD(TerminalView, invertScrollDirection),
                                       AngelScript::asCALL_THISCALL_ASGLOBAL,
                                       terminalHud->terminalView.get());

  scriptEngine->RegisterGlobalFunction("void set_invertGrabDirection(bool invert)",
                                       AngelScript::asMETHOD(TerminalView, setInvertGrabDirection),
                                       AngelScript::asCALL_THISCALL_ASGLOBAL,
                                       terminalHud->terminalView.get());
  scriptEngine->RegisterGlobalFunction("bool get_invertGrabDirection()",
                                       AngelScript::asMETHOD(TerminalView, invertGrabDirection),
                                       AngelScript::asCALL_THISCALL_ASGLOBAL,
                                       terminalHud->terminalView.get());

  scriptEngine->RegisterGlobalFunction("bool clear()",
                                       AngelScript::asFUNCTION(&Terminal::clear),
                                       AngelScript::asCALL_CDECL);

  scriptEngine->RegisterGlobalFunction("void set_visible(bool visible)",
                                       AngelScript::asMETHOD(TerminalHud, setVisible),
                                       AngelScript::asCALL_THISCALL_ASGLOBAL,
                                       terminalHud);
  scriptEngine->RegisterGlobalFunction("bool get_visible()",
                                       AngelScript::asMETHOD(TerminalHud, isVisible),
                                       AngelScript::asCALL_THISCALL_ASGLOBAL,
                                       terminalHud);

  // ==== Mouse::
  r = scriptEngine->SetDefaultNamespace("Mouse");

  r = scriptEngine->RegisterEnum("Mode");
  Scripting::AngelScriptCheck(r);

  r = scriptEngine->RegisterEnumValue("Mode", "Relative", MouseMode::RELATIVE);
  Scripting::AngelScriptCheck(r);
  r = scriptEngine->RegisterEnumValue("Mode", "Ingame", MouseMode::INGAME);
  Scripting::AngelScriptCheck(r);
  r = scriptEngine->RegisterEnumValue("Mode", "System", MouseMode::SYSTEM);
  Scripting::AngelScriptCheck(r);

  r = scriptEngine->RegisterGlobalFunction("void set_mode(Mode mode)",
                                           AngelScript::asMETHOD(MouseModeLayers::Layer, setMouseMode),
                                           AngelScript::asCALL_THISCALL_ASGLOBAL,
                                           Mouse::mouseModeLayers().terminalLayer.get());
  Scripting::AngelScriptCheck(r);
  r = scriptEngine->RegisterGlobalFunction("Mode get_mode()",
                                           AngelScript::asMETHOD(MouseModeLayers::Layer, mouseMode),
                                           AngelScript::asCALL_THISCALL_ASGLOBAL,
                                           Mouse::mouseModeLayers().terminalLayer.get());
  Scripting::AngelScriptCheck(r);

  r = scriptEngine->RegisterEnum("Cursor");
  Scripting::AngelScriptCheck(r);

  r = scriptEngine->RegisterEnumValue("Cursor", "None", 0);
  Scripting::AngelScriptCheck(r);
  r = scriptEngine->RegisterEnumValue("Cursor", "Default", 1);
  Scripting::AngelScriptCheck(r);
  r = scriptEngine->RegisterEnumValue("Cursor", "Text", 2);
  Scripting::AngelScriptCheck(r);
  r = scriptEngine->RegisterEnumValue("Cursor", "Move", 3);
  Scripting::AngelScriptCheck(r);
  r = scriptEngine->RegisterEnumValue("Cursor", "MoveNS", 4);
  Scripting::AngelScriptCheck(r);
  r = scriptEngine->RegisterEnumValue("Cursor", "MoveWE", 5);
  Scripting::AngelScriptCheck(r);
  r = scriptEngine->RegisterEnumValue("Cursor", "MoveSwNe", 6);
  Scripting::AngelScriptCheck(r);
  r = scriptEngine->RegisterEnumValue("Cursor", "MoveNwSe", 7);
  Scripting::AngelScriptCheck(r);
  r = scriptEngine->RegisterEnumValue("Cursor", "TweakUIHover", 8);
  Scripting::AngelScriptCheck(r);
  r = scriptEngine->RegisterEnumValue("Cursor", "TweakUIIdle", 9);
  Scripting::AngelScriptCheck(r);
  r = scriptEngine->RegisterEnumValue("Cursor", "TweakUIRotate", 10);
  Scripting::AngelScriptCheck(r);

  r = scriptEngine->RegisterGlobalFunction("void set_cursor(Cursor cursor)",
                                           AngelScript::asMETHOD(TerminalCommands, setMouseCursor),
                                           AngelScript::asCALL_THISCALL_ASGLOBAL,
                                           this);

  r = scriptEngine->RegisterGlobalFunction("void set_cursorScale(float scale)",
                                           AngelScript::asFUNCTION(Mouse::setMouseCursorScale),
                                           AngelScript::asCALL_CDECL);
  Scripting::AngelScriptCheck(r);
  r = scriptEngine->RegisterGlobalFunction("float get_cursorScale()",
                                           AngelScript::asFUNCTION(Mouse::mouseCursorScale),
                                           AngelScript::asCALL_CDECL);
  Scripting::AngelScriptCheck(r);

  // ==== Samples::Browser::
  r = scriptEngine->SetDefaultNamespace("Samples::Browser");

  r = scriptEngine->RegisterGlobalFunction("void start()",
                                           AngelScript::asFUNCTION(Application::startSampleBrowser),
                                           AngelScript::asCALL_CDECL);

  r = scriptEngine->RegisterGlobalFunction("void quit()",
                                           AngelScript::asFUNCTION(Application::startMainApplet),
                                           AngelScript::asCALL_CDECL);

  // ==== ::
  r = scriptEngine->SetDefaultNamespace("");
  Scripting::AngelScriptCheck(r);

  r = scriptEngine->RegisterGlobalFunction("void startSampleBrowser()",
                                           AngelScript::asFUNCTION(Application::startSampleBrowser),
                                           AngelScript::asCALL_CDECL);

  r = scriptEngine->RegisterGlobalFunction("void quitSampleBrowser()",
                                           AngelScript::asFUNCTION(Application::startMainApplet),
                                           AngelScript::asCALL_CDECL);

  r = scriptEngine->RegisterGlobalFunction("void startTextureBrowser()",
                                           AngelScript::asMETHOD(DeveloperTools, startTextureBrowser),
                                           AngelScript::asCALL_THISCALL_ASGLOBAL,
                                           developerTools);

  r = scriptEngine->RegisterGlobalFunction("void quitTextureBrowser()",
                                           AngelScript::asMETHOD(DeveloperTools, quitTextureBrowser),
                                           AngelScript::asCALL_THISCALL_ASGLOBAL,
                                           developerTools);

  r = scriptEngine->RegisterGlobalFunction("void createProblematicTexture()",
                                           AngelScript::asFUNCTION(createProblematicTexture),
                                           AngelScript::asCALL_CDECL);

  r = scriptEngine->RegisterGlobalFunction("void startCodeGenerator()",
                                           AngelScript::asMETHOD(DeveloperTools, startCodeGenerator),
                                           AngelScript::asCALL_THISCALL_ASGLOBAL,
                                           developerTools);

  r = scriptEngine->RegisterGlobalFunction("void quitCodeGenerator()",
                                           AngelScript::asMETHOD(DeveloperTools, quitCodeGenerator),
                                           AngelScript::asCALL_THISCALL_ASGLOBAL,
                                           developerTools);

  r = scriptEngine->RegisterGlobalFunction("void startIngameEditor()",
                                           AngelScript::asMETHOD(DeveloperTools, startIngameEditor),
                                           AngelScript::asCALL_THISCALL_ASGLOBAL,
                                           developerTools);

  r = scriptEngine->RegisterGlobalFunction("void quitIngameEditor()",
                                           AngelScript::asMETHOD(DeveloperTools, quitIngameEditor),
                                           AngelScript::asCALL_THISCALL_ASGLOBAL,
                                           developerTools);

  r = scriptEngine->RegisterGlobalFunction("void startSpatialGui()",
                                           AngelScript::asMETHOD(DeveloperTools, startSpatialGui),
                                           AngelScript::asCALL_THISCALL_ASGLOBAL,
                                           developerTools);

  r = scriptEngine->RegisterGlobalFunction("void quitSpatialGui()",
                                           AngelScript::asMETHOD(DeveloperTools, quitSpatialGui),
                                           AngelScript::asCALL_THISCALL_ASGLOBAL,
                                           developerTools);

  r = scriptEngine->RegisterGlobalFunction("void startTestAnimations()",
                                           AngelScript::asMETHOD(DeveloperTools, startTestAnimations),
                                           AngelScript::asCALL_THISCALL_ASGLOBAL,
                                           developerTools);

  r = scriptEngine->RegisterGlobalFunction("void quitTestAnimations()",
                                           AngelScript::asMETHOD(DeveloperTools, quitTestAnimations),
                                           AngelScript::asCALL_THISCALL_ASGLOBAL,
                                           developerTools);

  r = scriptEngine->RegisterGlobalFunction("void startCEGUI()",
                                           AngelScript::asMETHOD(DeveloperTools, startCEGUI),
                                           AngelScript::asCALL_THISCALL_ASGLOBAL,
                                           developerTools);

  r = scriptEngine->RegisterGlobalFunction("void quitCEGUI()",
                                           AngelScript::asMETHOD(DeveloperTools, quitCEGUI),
                                           AngelScript::asCALL_THISCALL_ASGLOBAL,
                                           developerTools);

  r = scriptEngine->RegisterGlobalFunction("void testLanguageChange()",
                                           AngelScript::asFUNCTION(testLanguageChange),
                                           AngelScript::asCALL_CDECL);

  r = scriptEngine->RegisterGlobalFunction("void testExportingDarkTheme(const string &in filename, bool largeWidgets=false, bool fillBackground=false)",
                                           AngelScript::asFUNCTION(ProceduralAssets::GuiThemes::testExportingDarkTheme),
                                           AngelScript::asCALL_CDECL);

  r = scriptEngine->RegisterGlobalFunction("void testDarkThemeTextureAtlas(const string &in directory, bool largeWidgets=false)",
                                           AngelScript::asFUNCTION(ProceduralAssets::GuiThemes::testDarkThemeTextureAtlas),
                                           AngelScript::asCALL_CDECL);

  r = scriptEngine->RegisterGlobalFunction("void testDarkThemeXml(const string &in xmlFilename)",
                                           AngelScript::asFUNCTION(ProceduralAssets::GuiThemes::testDarkThemeXml),
                                           AngelScript::asCALL_CDECL);

  r = scriptEngine->RegisterGlobalFunction("void testExportingMouseCursor(const string &in filename, const ivec2 &in size=ivec2(128, 64), bool fillBackground=false)",
                                           AngelScript::asFUNCTION(ProceduralAssets::GuiThemes::testExportingMouseCursor),
                                           AngelScript::asCALL_CDECL);

  r = scriptEngine->RegisterGlobalFunction("void testMouseCursorTextureAtlas(const string &in directory, const ivec2 &in size=ivec2(128, 64))",
                                           AngelScript::asFUNCTION(ProceduralAssets::GuiThemes::testMouseCursorTextureAtlas),
                                           AngelScript::asCALL_CDECL);

  r = scriptEngine->RegisterGlobalFunction("void testExportCeguiLooknfeel(const string &in widgetLookPrefix, const string &in filename)",
                                           AngelScript::asFUNCTION(testExportCeguiLooknfeel),
                                           AngelScript::asCALL_CDECL);

  r = scriptEngine->RegisterGlobalFunction("void exit()",
                                           AngelScript::asFUNCTION(exit),
                                           AngelScript::asCALL_CDECL);

  // ==== Build

  scriptEngine->SetDefaultAccessMask(oldMask);

  r = scriptModule->Build();
  Scripting::AngelScriptCheck(r);

  // ==== init index
  const Scripting::Index::Ptr &scriptIndex = hud.scriptIndex();
  scriptIndex->insertGlobal(scriptEngine, terminalAccessMask);
  scriptIndex->insertModule(scriptModule);
}


void TerminalCommands::execute(const String &command)
{
  assert(scriptModule);

  FrameTimer::ignoreNextFrame = true;
  int r;

  terminalHud->terminalUserInput->setActive(false);

  AngelScript::asIScriptEngine *engine = Scripting::Engine::angelScriptEngine();

  AngelScript::asIScriptFunction *func;
  r = scriptModule->CompileFunction("",
                                     String::compose("void executeCommand(){%1\n}", command).toUtf8String().c_str(),
                                     0,
                                     0,
                                     &func);

  if(func == nullptr)
  {
    terminalHud->terminalUserInput->setActive(true);
    return;
  }

  AngelScript::asIScriptContext *scriptContext = engine->CreateContext();
  r = scriptContext->Prepare(func);
  Scripting::AngelScriptCheck(r);

  r = scriptContext->Execute();
  if(AngelScript::asEXECUTION_FINISHED != r)
  {
    assert(AngelScript::asEXECUTION_FINISHED == r);
    // ISSUE-140
  }

  func->Release();
  scriptContext->Release();

  terminalHud->terminalUserInput->setActive(true);
}

void TerminalCommands::parseAngelScriptToken(const std::string &token)
{
  AngelScript::asIScriptEngine *engine = Scripting::Engine::angelScriptEngine();

  int tokenLength;
  AngelScript::asETokenClass tokenClass = engine->ParseToken(token.c_str(), 0, &tokenLength);

  String tokenClassStr;
  switch(tokenClass)
  {
  case AngelScript::asTC_WHITESPACE:
    tokenClassStr = "asTC_WHITESPACE";
    break;
  case AngelScript::asTC_COMMENT:
    tokenClassStr = "asTC_COMMENT";
    break;
  case AngelScript::asTC_IDENTIFIER:
    tokenClassStr = "asTC_IDENTIFIER";
    break;
  case AngelScript::asTC_VALUE:
    tokenClassStr = "asTC_VALUE";
    break;
  case AngelScript::asTC_KEYWORD:
    tokenClassStr = "asTC_KEYWORD";
    break;
  case AngelScript::asTC_UNKNOWN:
    tokenClassStr = "asTC_UNKNOWN";
    break;
  default:
    tokenClassStr = "Unknown token class enum value";
  }

  Terminal::writeOut("The token \"");
  Terminal::writeOut(Terminal::Format(Terminal::Format::Color::ORANGE),
                     String::fromUtf8(std::string(token.c_str(), token.c_str()+tokenLength)));
  Terminal::writeOut("\" has the class ");
  Terminal::writeOut(Terminal::Format(Terminal::Format::Color::ORANGE),
                     tokenClassStr+"\n");
}

void TerminalCommands::runTests(bool fast)
{
  try
  {
    auto signalBlocker = UserInput::createSignalBlocker(InputDevice::ActionPriority::BLOCK_ABSOLUTELY_EVERY_INPUT);
    PrivateRenderLoop privateRenderLoop;
    runUnitTests(privateRenderLoop, fast);
    (void)signalBlocker;
  }catch(const std::exception &e)
  {
    Terminal::writeOut(Terminal::Format(Terminal::Format::Color::RED, Terminal::Format::Light::BRIGHTER),
                       "runTests(): Uncatched exception: `%0`",
                       e.what());
  }catch(...)
  {
    Terminal::writeOut(Terminal::Format(Terminal::Format::Color::RED, Terminal::Format::Light::BRIGHTER),
                       "runTests(): Uncatched unknown exception");
  }
}

void TerminalCommands::setMouseCursor(int cursor)
{
  DefaultMouseCursor::Ptr c;

  switch(cursor)
  {
  case 0:
    c = DefaultMouseCursors::noCursor;
    break;
  case 2:
    c = DefaultMouseCursors::textCursor;
    break;
  case 3:
    c = DefaultMouseCursors::moveCursor;
    break;
  case 4:
    c = DefaultMouseCursors::moveNSCursor;
    break;
  case 5:
    c = DefaultMouseCursors::moveWECursor;
    break;
  case 6:
    c = DefaultMouseCursors::moveSwNeCursor;
    break;
  case 7:
    c = DefaultMouseCursors::moveNwSeCursor;
    break;
  case 8:
    c = DefaultMouseCursors::tweakUIHover;
    break;
  case 9:
    c = DefaultMouseCursors::tweakUIIdle;
    break;
  case 10:
    c = DefaultMouseCursors::tweakUIRotate;
    break;
  case 1:
  default:
    c = DefaultMouseCursors::defaultCursor;
  }

  Mouse::mouseCursorLayers().terminalLayer->setCursor(c);
}

void TerminalCommands::help()
{
  class HelpVisitor : public Scripting::Index::NameVisitor
  {
  public:
    void visitFunction(const std::string &name, const AngelScript::asIScriptFunction *) override
    {
      Terminal::writeOut("%1\n", name);
    }

    void visitEnumValue(const std::string &name, int) override
    {
      Terminal::writeOut("%1\n", name);
    }
  }visitor;

  terminalHud->scriptIndex()->visitAllWith(visitor);
}


void createProblematicTexture()
{
  int w = 356;
  int h = 64;
  Ogre::PixelFormat pixelFormat = Ogre::PF_FLOAT32_RGBA;

  Ogre::TexturePtr texture = Ogre::TextureManager::getSingleton().createManual("problematic-texture",
                                                                               Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
                                                                               Ogre::TEX_TYPE_2D,
                                                                               w,
                                                                               h,
                                                                               0,
                                                                               pixelFormat,
                                                                               Ogre::TU_STATIC);
  std::vector<vec4> colors;
  colors.resize(w*h);
  for(int x=0; x<w; ++x)
  {
    for(int y=0; y<h; ++y)
    {
      colors[x+w*y] = vec4(vec3((x-50)/255.f), 1);
    }
  }

  colors[100 + w*8].y = NAN;
  colors[150 + w*8].y =  INFINITY;
  colors[200 + w*8].y = -INFINITY;
  colors[100 + w*32].a = NAN;
  colors[150 + w*32].a =  INFINITY;
  colors[200 + w*32].a = -INFINITY;

  Ogre::DataStreamPtr stream(new Ogre::MemoryDataStream(colors.data(), colors.size()*sizeof(vec4)));
  texture->loadRawData(stream, w, h, pixelFormat);
}


void testLanguageChange() // ISSUE-184 remove this hardcoded language
{
  I18n::Language english;

  I18n::Language german;
  german.simpleTranslations["Cancel"] = "Abbrechen";
  german.simpleTranslations["Help"] = "Hilfe";

  if(Application::language().simpleTranslations.empty())
    Application::setLanguage(german);
  else
    Application::setLanguage(english);
}

void exit()
{
  Application::quit();
}


} // namespace DeveloperHudElements
} // namespace Framework
