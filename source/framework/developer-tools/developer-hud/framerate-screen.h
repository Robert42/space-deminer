#ifndef _FRAMEWORK_DEVELOPER_HUD_FRAMERATE_H_
#define _FRAMEWORK_DEVELOPER_HUD_FRAMERATE_H_

#include <dependencies.h>

#include <framework/frame-timer.h>
#include <framework/frame-signals.h>
#include <framework/timer.h>
#include <framework/developer-tools/developer-hud.h>

#include "monospace-table.h"

namespace Framework {

using namespace  Base;


class FramerateScreen final : public DeveloperHud::MonospaceLayer
{
public:
  Signals::Trackable trackable;

private:
  MonospaceTable table;
  PassiveTimer simpleMinMaxTimer;

  int minPotentialFps_[2];
  int minCurrentFps_[2];
  int minAverageFps_[2];
  int maxPotentialFps_[2];
  int maxCurrentFps_[2];
  int maxAverageFps_[2];

  int minMaxReadIndex;
  int minMaxWriteIndex;

  int& minPotentialFps();
  int& minCurrentFps();
  int& minAverageFps();
  int& maxPotentialFps();
  int& maxCurrentFps();
  int& maxAverageFps();

public:
  FramerateScreen(DeveloperHud& hud);
  ~FramerateScreen();


  Gorilla::Caption* potentialFpsCaption;
  Gorilla::Caption* currentFpsCaption;
  Gorilla::Caption* averageFpsCaption;

  Gorilla::Caption* minPotentialFpsCaption;
  Gorilla::Caption* minCurrentFpsCaption;
  Gorilla::Caption* minAverageFpsCaption;

  Gorilla::Caption* maxPotentialFpsCaption;
  Gorilla::Caption* maxCurrentFpsCaption;
  Gorilla::Caption* maxAverageFpsCaption;

  Gorilla::Caption* potentialMillisecondCaption;
  Gorilla::Caption* currentMillisecondCaption;
  Gorilla::Caption* averageMillisecondCaption;

private:
  void update();

  void resetMinMaxValues();

  void adaptColor(Gorilla::Caption* caption, int fps);

  void registerTerminalCommands(const std::shared_ptr<Scripting::Index>& scriptIndex) override;
};



}


#endif
