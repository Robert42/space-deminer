#ifndef _FRAMEWORK_DEVELOPER_HUD_TERMINAL_H_
#define _FRAMEWORK_DEVELOPER_HUD_TERMINAL_H_

#include <dependencies.h>

#include <framework/window/user-input.h>
#include <framework/window/user-input/mouse-mode-layers.h>
#include <framework/window/user-input/mouse-cursor-layers.h>
#include <framework/developer-tools/developer-hud.h>

#include <base/output.h>

#include "terminal-view.h"
#include "terminal-commands.h"

namespace Framework {
using namespace  Base;

class RenderWindow;

namespace DeveloperHudElements {
class TerminalUserInput;
class TerminalView;
class TerminalHistory;

class TerminalHud final : public DeveloperHud::MonospaceLayer
{
public:
  Signals::Trackable trackable;

  TerminalCommands terminalCommands;

  shared_ptr<TerminalUserInput> terminalUserInput;
  shared_ptr<TerminalHistory> terminalHistory;
  shared_ptr<TerminalView> terminalView;

private:
  Gorilla::Rectangle* terminalBackground;

  shared_ptr<UserInput::SignalBlocker> signalBlocker;
  shared_ptr<MouseModeLayers::Layer> mouseModeLayer;
  shared_ptr<MouseCursorLayers::Layer> mouseCursorLayer;

  bool alwaysUseTerminalMouseMode;

  int posX, posY;

  PrivateTerminalView::Ptr showPossibleFormatsView;

public:
  TerminalHud(DeveloperHud& hud);
  ~TerminalHud();

  static shared_ptr<TerminalHud> create(DeveloperHud& hud);

  void show() override;
  void hide() override;

  const shared_ptr<Scripting::Index>& scriptIndex();

public:
  void setFontSize(int fontSize);
  int fontSize() const;

private:
  bool handleKey(KeyCode key);

  void move(int x, int y);
  void recalcPosition();

  static void relativePositionForPos(const Output<int>& first, const Output<int>& second, int pos, int maxValue);

  void handleCommand(const String& command);

  void registerTerminalCommands(const std::shared_ptr<Scripting::Index>&) override;
};


}
}

#endif
