#include "monospace-table.h"

#include <framework/developer-tools/developer-hud.h>

namespace Framework {
typedef DeveloperHud::MonospaceLayer MonospaceLayer;

MonospaceTable::MonospaceTable(Gorilla::Layer& gorillaLayer, const String& title, int glyphIndexDefault, int glyphIndexEmphasized, const Ogre::ColourValue& colorDefault)
  : gorillaLayer(gorillaLayer),
    glyphIndexDefault(glyphIndexDefault),
    glyphIndexEmphasized(glyphIndexEmphasized),
    colorDefault(colorDefault)
{
  numberRows = 0;
  numberColumns = 0;

  glyphData = gorillaLayer._getGlyphData(glyphIndexDefault);

  background = gorillaLayer.createRectangle(0, 0, 16, 16);
  background->background_colour(Ogre::ColourValue(0.0f, 0.0f, 0.0f, 0.5f));

  titleCaption = gorillaLayer.createCaption(glyphIndexDefault, 0, glyphData->mLineHeight, title.toOgreString());
  titleCaption->fixedWidth(true);
  titleCaption->colour(colorDefault);
}

MonospaceTable::MonospaceTable(Gorilla::Layer& gorillaLayer, const String& title)
  : MonospaceTable(gorillaLayer, title, MonospaceLayer::freemono_14_normal, MonospaceLayer::freemono_14_bright, Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.75f))
{
}

void MonospaceTable::addColumn(int width)
{
  int position = 0;
  if(!columnPositions.empty())
    position = *columnPositions.rbegin() + 1+*columnWidths.rbegin();

  this->columnWidths.push_back(width);
  this->columnPositions.push_back(position);
}

ivec2 MonospaceTable::position(int c, int r)
{
  while(c >= int(columnPositions.size()))
    addColumn(16);

  ++r;

  ivec2 pos(columnPositions[c]*glyphData->mMonoWidth, r*glyphData->mLineSpacing + (r+1)*glyphData->mLineHeight);

  return pos;
}

ivec2 MonospaceTable::subPosition(int c, int r, int character)
{
  ivec2 pos = position(c, r);

  pos.x += character*glyphData->mMonoWidth;

  return pos;
}

Gorilla::Caption& MonospaceTable::createCaption(int x, int y, const String& text, bool emphasize, Gorilla::TextAlignment align)
{
  Gorilla::Caption* caption = gorillaLayer.createCaption(emphasize?glyphIndexEmphasized:glyphIndexDefault,
                                                         x,
                                                         y,
                                                         text.toOgreString());
  caption->fixedWidth(true);
  caption->colour(colorDefault);
  caption->align(align);

  return *caption;
}

Gorilla::Caption& MonospaceTable::addText(int c, int r, const String& text, bool emphasize, Gorilla::TextAlignment align)
{
  ivec2 pos = position(c, r);

  Gorilla::Caption& caption = createCaption(pos.x, pos.y, text, emphasize, align);

  caption.width(glyphData->mMonoWidth * columnWidths[c]);

  adaptBackgroundSize(c, r);

  return caption;
}

Gorilla::Caption& MonospaceTable::addValue(int c, int r, const String& unit, bool emphasize)
{
  ivec2 pos = position(c, r);
  int nAviableCharacters = columnWidths[c];

  int unitX = pos.x + (nAviableCharacters-unit.length()) * glyphData->mMonoWidth;
  int valueWidth = (nAviableCharacters-unit.length()) * glyphData->mMonoWidth - glyphData->mMonoWidth/2;

  createCaption(unitX, pos.y, unit, false, Gorilla::TextAlignment::TextAlign_Left);

  Gorilla::Caption& valueCaption = createCaption(pos.x, pos.y, "", emphasize, Gorilla::TextAlignment::TextAlign_Right);

  valueCaption.width(valueWidth);

  adaptBackgroundSize(c, r);

  return valueCaption;
}

void MonospaceTable::adaptBackgroundSize(int c, int r)
{
  position(c, r);
  numberColumns = max(c+1, numberColumns);
  numberRows = max(r+1, numberRows);

  ivec2 size = position(numberColumns, numberRows);
  background->width(size.x);
  background->height(size.y);
}


}
