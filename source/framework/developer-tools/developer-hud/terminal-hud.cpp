#include <dependencies.h>

#include <framework/window.h>
#include <framework/application.h>
#include <framework/window/user-input/mouse.h>
#include <framework/developer-tools/terminal.h>
#include <framework/developer-tools/terminal/terminal-manager.h>
#include <framework/frame-signals.h>
#include <framework/scripting/initialization.h>
#include <framework/developer-tools/developer-settings.h>

#include "terminal-hud.h"
#include "terminal-input.h"
#include "terminal-history.h"

namespace Framework {
namespace DeveloperHudElements {

TerminalHud::TerminalHud(DeveloperHud& hud)
  : MonospaceLayer(ZLayer::TERMINAL, hud)
{
  posX = 0;
  posY = 2;

  Keyboard::signalKeyPressed().connect(InputDevice::ActionPriority::TERMINAL,
                                       std::bind(&TerminalHud::handleKey, this, _1)).track(this->trackable);
  Keyboard::signalKeyPressed().connect(InputDevice::ActionPriority::TERMINAL,
                                       std::bind(&TerminalHud::handleKey, this, _1)).track(this->trackable);

  gorillaLayer().hide();
  terminalBackground = gorillaLayer().createRectangle(0, 0);
  terminalBackground->border_colour(Ogre::ColourValue(1.0f, 1.0f, 1.0f, 0.5f));
  terminalBackground->background_colour(Ogre::ColourValue(0.0f, 0.0f, 0.0f, 0.8f));
  terminalBackground->border_width(2.0f);

  terminalUserInput = shared_ptr<TerminalUserInput>(new TerminalUserInput());
  terminalUserInput->signalFilterActive().connect([this](bool active){return active && this->isVisible();}).track(this->trackable);
  terminalUserInput->signalCommandExecuted().connect(std::bind(&TerminalHud::handleCommand, this, _1)).track(this->trackable);

  terminalView = shared_ptr<TerminalView>(new TerminalView(gorillaLayer(),
                                                           fontSmall,
                                                           fontLarge,
                                                           terminalUserInput->cursor()));

  terminalHistory = shared_ptr<TerminalHistory>(new TerminalHistory);

  recalcPosition();

  RenderWindow::signalWindowResized().connect(std::bind(&TerminalHud::recalcPosition, this)).track(this->trackable);

  alwaysUseTerminalMouseMode = Application::developerSettings().terminal.alwaysUseTerminalMouseMode;
  mouseModeLayer = Mouse::mouseModeLayers().terminalLayer;
  if(alwaysUseTerminalMouseMode)
    mouseModeLayer->setVisible(true);
  mouseCursorLayer = Mouse::mouseCursorLayers().terminalLayer;
}


TerminalHud::~TerminalHud()
{
  showPossibleFormatsView.reset();
  terminalUserInput.reset();
}


shared_ptr<TerminalHud> TerminalHud::create(DeveloperHud& hud)
{
  return shared_ptr<TerminalHud>(new TerminalHud(hud));
}


void TerminalHud::registerTerminalCommands(const std::shared_ptr<Scripting::Index>&)
{
  terminalCommands.registerTerminalCommands(*this);
}


void TerminalHud::show()
{
  if(isVisible())
    return;

  this->signalBlocker = UserInput::createSignalBlocker(InputDevice::ActionPriority::TERMINAL_BLOCKER);

  MonospaceLayer::show();

  terminalUserInput->setActive(true);
  terminalView->setActive(true);
  mouseModeLayer->setVisible(true);
  mouseCursorLayer->setVisible(true);

  Mouse::unsetMouseAction();
}


void TerminalHud::hide()
{
  this->signalBlocker.reset();

  MonospaceLayer::hide();

  terminalUserInput->setActive(false);
  terminalView->setActive(false);
  if(!alwaysUseTerminalMouseMode)
    mouseModeLayer->setVisible(false);
  mouseCursorLayer->setVisible(false);
}


void TerminalHud::move(int x, int y)
{
  posX = clamp<int>(posX+x, -2, 2);
  posY = clamp<int>(posY+y, -2, 2);

  recalcPosition();
}


bool TerminalHud::handleKey(KeyCode key)
{
  if(!isVisible())
  {
    if(key == KeyCode::OPENTERMINAL)
    {
      FrameSignals::callOnce(std::bind(&TerminalHud::show, this));
      return true;
    }

    return false;
  }

  bool onlyAlt = Keyboard::areOnlyModifierDown(KeyModifier::ALT);
  bool noModifier = !Keyboard::isAnyModifierDown();

  if(key == KeyCode::ESCAPE)
  {
    hide();
    return true;
  }else if(key == KeyCode::ARROW_LEFT && onlyAlt)
  {
    move(-1, 0);
    return true;
  }else if(key == KeyCode::ARROW_RIGHT && onlyAlt)
  {
    move(1, 0);
    return true;
  }else if(key == KeyCode::ARROW_UP && onlyAlt)
  {
    move(0, -1);
    return true;
  }else if(key == KeyCode::ARROW_DOWN && onlyAlt)
  {
    move(0, 1);
    return true;
  }else if(key == KeyCode::ARROW_UP && noModifier)
  {
    terminalHistory->moveToPreviousCommand(terminalUserInput->typedInText());
    terminalUserInput->resetUserInput(terminalHistory->currentEntryToBeShown());
    return true;
  }else if(key == KeyCode::ARROW_DOWN && noModifier)
  {
    terminalHistory->moveToFollowingCommand();
    terminalUserInput->resetUserInput(terminalHistory->currentEntryToBeShown());
    return true;
  }

  return false;
}

void TerminalHud::handleCommand(const String& command)
{
  String trimmedCommand = command.trimWhitespace();

  if(trimmedCommand == "u")
    terminalCommands.execute("runTests(true);");
  else if(trimmedCommand == "U")
    terminalCommands.execute("runTests();");
  else
    terminalCommands.execute(command);

  terminalHistory->appendCommand(command);

  // ISSUE-77 add terminal tool showing which key events are received
}

void TerminalHud::setFontSize(int fontSize)
{
  terminalView->setFontSize(fontSize);
  recalcPosition();
}

int TerminalHud::fontSize() const
{
  return terminalView->fontSize();
}

void TerminalHud::recalcPosition()
{
  int top, left, right, bottom;

  real minSize = 128;
  int border = 8;

  ivec2 windowSize = RenderWindow::size();

  int maxRight = max<int>(minSize, windowSize.x)-1;
  int maxBottom = max<int>(minSize, windowSize.y)-1;

  relativePositionForPos(out(left), out(right), posX, maxRight);
  relativePositionForPos(out(top), out(bottom), posY, maxBottom);

  int lineHeight = terminalView->lineHeight();
  int lineSpacing = terminalView->lineSpacing();
  int currentContentAreaHeight = bottom-top-border*2;
  currentContentAreaHeight -= currentContentAreaHeight % lineHeight;
  currentContentAreaHeight -= lineSpacing;

  // adapt the height to the lineheightso the terminal will look better
  if(top != 0)
    top = bottom - currentContentAreaHeight - border*2;
  else
    bottom = top + currentContentAreaHeight + border*2;

  terminalBackground->left(left);
  terminalBackground->top(top);
  terminalBackground->width(right-left);
  terminalBackground->height(bottom-top);

  terminalView->setAllocation(left + border,
                              top + border,
                              right - left - border*2,
                              currentContentAreaHeight + lineSpacing);
}

void TerminalHud::relativePositionForPos(const Output<int>& first,
                                         const Output<int>& second,
                                         int pos,
                                         int maxValue)
{
  int a = 0;
  int b = maxValue / 3;
  int d = (maxValue * 2) / 3;
  int e = maxValue;

  switch(pos)
  {
  case -2:
    first.value = a;
    second.value = b;
    break;
  case -1:
    first.value = a;
    second.value = d;
    break;
  case 0:
    first.value = a;
    second.value = e;
    break;
  case 1:
    first.value =  b;
    second.value = e;
    break;
  case 2:
    first.value =  d;
    second.value = e;
    break;
  default:
    first.value =  a;
    second.value = e;
  }
}


const shared_ptr<Scripting::Index>& TerminalHud::scriptIndex()
{
  return terminalUserInput->scriptIndex;
}


}
}
