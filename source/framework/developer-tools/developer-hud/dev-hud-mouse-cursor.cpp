#include <framework/application.h>
#include <framework/developer-tools/developer-settings.h>
#include <framework/window/user-input/mouse-cursor.h>

#include "dev-hud-mouse-cursor.h"

namespace Framework {
namespace DeveloperHudElements {


BaseMouseCursor::Cursor::Cursor(Gorilla::Screen& gorillaScreen,
                                const std::string& name)
{
  Gorilla::Sprite* hotspotSprite = gorillaScreen.getAtlas()->getSprite(name + "-hotspot");

  hotspot = ivec2(hotspotSprite->spriteWidth, hotspotSprite->spriteHeight);
  sprite = gorillaScreen.getAtlas()->getSprite(name);
}


BaseMouseCursor::CursorSet::CursorSet()
{
  baseMouseCursor = nullptr;
}


BaseMouseCursor::CursorSet::~CursorSet()
{
  assert(baseMouseCursor == nullptr);
}


void BaseMouseCursor::CursorSet::initDefaultCursor(const Framework::DefaultMouseCursor::Ptr& cursor, const BaseMouseCursor::Cursor& c)
{
  cursor->signalActivate().connect(std::bind(&BaseMouseCursor::setCursorPtr, baseMouseCursor, &c)).track(baseMouseCursor->trackable);
  cursor->signalDeactivate().connect(std::bind(&BaseMouseCursor::unsetCursor, baseMouseCursor)).track(baseMouseCursor->trackable);
}


BaseMouseCursor::BaseMouseCursor(ZLayer zlayer, Gorilla::Screen& gorillaScreen, const CursorSet::Ptr& cursorSet)
  : Layer(zlayer, gorillaScreen),
    cursorSet(cursorSet)
{
  this->currentCursor = &cursorSet->defaultCursor();
  this->cursor = gorillaLayer().createRectangle(0, 0);

  this->_scale = Mouse::mouseCursorScale();

  this->setCursor(cursorSet->defaultCursor());

  cursorSet->baseMouseCursor = this;
  cursorSet->init();

  updatePosition();
  determineVisibility();
  Mouse::signalMouseMoved().connect(Mouse::ActionPriority::MOUSE_CURSOR,
                                    std::bind(&BaseMouseCursor::updatePosition, this)).track(this->trackable);
  Mouse::signalModeChanged().connect(std::bind(&BaseMouseCursor::determineVisibility, this)).track(this->trackable);
  Mouse::signalCursorScaleChanged().connect(std::bind(&BaseMouseCursor::setScale, this, _1)).track(this->trackable);
  RenderWindow::signalFocusChanged().connect(std::bind(&BaseMouseCursor::determineVisibility, this)).track(this->trackable);
}


BaseMouseCursor::~BaseMouseCursor()
{
  cursorSet->baseMouseCursor = nullptr;
}


void BaseMouseCursor::setCursor(const Cursor& cursor)
{
  this->currentCursor = &cursor;
  this->cursor->background_image(cursor.sprite);

  updateCursorSize();
  determineVisibility();
}


void BaseMouseCursor::setCursorPtr(const Cursor* cursor)
{
  setCursor(*cursor);
}


void BaseMouseCursor::updateCursorSize()
{
  vec2 spriteSize(currentCursor->sprite->spriteWidth, currentCursor->sprite->spriteHeight);

  this->cursor->width(spriteSize.x * scale());
  this->cursor->height(spriteSize.y * scale());

  updatePosition();
}


void BaseMouseCursor::unsetCursor()
{
  setCursor(cursorSet->dummy());
}


bool BaseMouseCursor::updatePosition()
{
  const ivec2& hotspot = currentCursor->hotspot;

  const ivec2 cursorPositionImage = round(vec2(Mouse::absolutePosition()) - vec2(hotspot)*scale());

  this->cursor->left(cursorPositionImage.x);
  this->cursor->top(cursorPositionImage.y);

  return false;
}


void BaseMouseCursor::determineVisibility()
{
  bool visible = Mouse::mouseMode() == MouseMode::INGAME;
  visible &= currentCursor!=&cursorSet->dummy();
  visible &= RenderWindow::gotFocusAlreadyOnce();

  if(Application::developerSettings().input.alwaysShowIngameMouseCursor)
  {
    Layer::setVisible(true);

    Ogre::ColourValue color = Ogre::ColourValue(1.f, 1.f, 1.f, visible ? 1.f : 0.25f);

    this->cursor->background_colour(color);
  }else
  {
    this->cursor->background_colour(Ogre::ColourValue::White);
    Layer::setVisible(visible);
  }
}


void BaseMouseCursor::setScale(real scale)
{
  if(this->scale() != scale)
  {
    this->_scale = scale;

    updateCursorSize();
  }
}


real BaseMouseCursor::scale() const
{
  return _scale;
}


// ====


DefaultMouseCursor::DefaultCursorSet::DefaultCursorSet(Gorilla::Screen& gorillaScreen)
  : _noCursor(gorillaScreen, "default"),
    _defaultCursor(gorillaScreen, "default"),
    _textCursor(gorillaScreen, "text"),
    _moveCursor(gorillaScreen, "move"),
    _moveNSCursor(gorillaScreen, "move-n-s"),
    _moveWECursor(gorillaScreen, "move-w-e"),
    _moveSwNeCursor(gorillaScreen, "move-ws-ne"),
    _moveNwSeCursor(gorillaScreen, "move-nw-se")
{
}

const DefaultMouseCursor::Cursor& DefaultMouseCursor::DefaultCursorSet::dummy()
{
  return _noCursor;
}

const DefaultMouseCursor::Cursor& DefaultMouseCursor::DefaultCursorSet::defaultCursor()
{
  return _defaultCursor;
}

void DefaultMouseCursor::DefaultCursorSet::init()
{
  initDefaultCursor(DefaultMouseCursors::noCursor, this->_noCursor);
  initDefaultCursor(DefaultMouseCursors::defaultCursor, this->_defaultCursor);
  initDefaultCursor(DefaultMouseCursors::textCursor, this->_textCursor);
  initDefaultCursor(DefaultMouseCursors::moveCursor, this->_moveCursor);
  initDefaultCursor(DefaultMouseCursors::moveNSCursor, this->_moveNSCursor);
  initDefaultCursor(DefaultMouseCursors::moveWECursor, this->_moveWECursor);
  initDefaultCursor(DefaultMouseCursors::moveSwNeCursor, this->_moveSwNeCursor);
  initDefaultCursor(DefaultMouseCursors::moveNwSeCursor, this->_moveNwSeCursor);
}


DefaultMouseCursor::DefaultCursorSet::Ptr DefaultMouseCursor::DefaultCursorSet::create(Gorilla::Screen& gorillaScreen)
{
  return Ptr(new DefaultCursorSet(gorillaScreen));
}


DefaultMouseCursor::DefaultMouseCursor(DeveloperHud& hud)
  : BaseMouseCursor(DeveloperHud::Layer::ZLayer::MOUSECURSOR,
                    hud.gorillaScreenMouseCursor(),
                    DefaultCursorSet::create(hud.gorillaScreenMouseCursor()))
{
}


} // namespace DeveloperHudElements
} // namespace Framework
