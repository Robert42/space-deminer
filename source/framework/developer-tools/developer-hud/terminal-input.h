#ifndef _FRAMEWORK_DEVELOPER_HUD_TERMINAL_INPUT_H_
#define _FRAMEWORK_DEVELOPER_HUD_TERMINAL_INPUT_H_

#include <dependencies.h>

#include <framework/window/user-input/keyboard.h>
#include <framework/window/user-input/mouse.h>

#include <framework/developer-tools/terminal/interactive-area.h>
#include <framework/developer-tools/terminal/buffer/terminal-buffer.h>

namespace Base {
namespace Scripting {
class Index;
}
}

namespace Framework {
namespace DeveloperHudElements {

class TerminalUserInput final
{
public:
  typedef Signals::Signal<void(const String&)> CommandSignal;
  typedef Signals::Signal<void()> EditingSignal;
  typedef Signals::Signal< bool(bool), Signals::ResultHandler::LogicalAndResultHandler<bool(bool)> > FilterActiveSignal;

public:
  Signals::Trackable trackable;

private:
  typedef Signals::CallableSignal<void(const String&)> CallableCommandSignal;
  typedef Signals::CallableSignal<void()> CallableEditingSignal;
  typedef Signals::CallableSignal< bool(bool), Signals::ResultHandler::LogicalAndResultHandler<bool(bool)> > CallableFilterActiveSignal;

  Terminal::InteractiveArea interactiveArea;
  bool active;
  Terminal::Buffer::Ptr dummyBuffer;

  String greenPrefix, bluePrefix, normalPrefix;
  size_t prefixLength;

  shared_ptr<Terminal::Cursor> _cursor;

  CallableCommandSignal callableSignalCommandExecuted;
  CallableEditingSignal callableSignalCommandEdited;
  CallableFilterActiveSignal callableSignalFilterActive;

public:
  shared_ptr<Scripting::Index> scriptIndex;

public:
  TerminalUserInput();

  bool isActive() const;
  void setActive(bool active);

  String typedInText() const;

  CommandSignal& signalCommandExecuted();
  EditingSignal& signalCommandEdited();
  FilterActiveSignal& signalFilterActive();

  const shared_ptr<Terminal::Cursor>& cursor() const;

  void clearUserInput();
  void resetUserInput(const String& newInput="");

  void autocomplete();

private:
  bool handleKey(KeyCode key);
  bool handleUnicodeInput(String::value_type unicode);

  size_t firstPossibleCursorPosition() const;
  size_t lastPossibleCursorPosition() const;

  size_t clampCursorPosition(size_t position) const;
  void moveCursorRelative(int relativePosition);
  void moveCursorAbsolute(int absolutePosition);

  void moveCursorToNextToken(int direction);
  void deleteToNextToken(int direction);

  void writeTypedTextOut(const String& typedInText) const;
  void writeTypedTextOut() const;

  void highlightSyntax();

  size_t indexOfNextToken(int direction) const;
  size_t absoluteIndexOfNextToken(int direction) const;
};

}
}

#endif
