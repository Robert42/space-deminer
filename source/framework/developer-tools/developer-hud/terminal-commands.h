#ifndef _FRAMEWORK_DEVELOPER_HUD_TERMINA_COMMANDSL_H_
#define _FRAMEWORK_DEVELOPER_HUD_TERMINA_COMMANDSL_H_

#include <base/scripting/engine.h>

namespace Base {
namespace Scripting {
class Index;
}
}
namespace Framework {
using namespace Base;

namespace DeveloperHudElements {

class TerminalHud;

class TerminalCommands
{
private:
  AngelScript::asIScriptModule* scriptModule;
  TerminalHud* terminalHud;

public:
  TerminalCommands();

  void registerTerminalCommands(TerminalHud& hud);
  void execute(const String& command);

private:
  void runTests(bool fast);
  void parseAngelScriptToken(const std::string& token);

  void setMouseCursor(int cursor);

  void help();
};



}
}

#endif
