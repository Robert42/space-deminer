#ifndef FRAMEWORK_TWEAKUI_TWEAKUIMOUSECURSOR_H
#define FRAMEWORK_TWEAKUI_TWEAKUIMOUSECURSOR_H

#include <framework/developer-tools/developer-hud/dev-hud-mouse-cursor.h>

namespace Framework {
namespace TweakUI {

class MouseCursor : public DeveloperHudElements::BaseMouseCursor
{
private:

  class CursorSet : public DeveloperHudElements::BaseMouseCursor::CursorSet
  {
  public:
    typedef DeveloperHudElements::BaseMouseCursor::Cursor Cursor;

    Cursor noCursor;
    Cursor hoverCursor;
    Cursor idleCursor;
    Cursor rotatingCursor;

  private:
    CursorSet(::Gorilla::Screen& gorillaScreen);

  public:
    static Ptr create(::Gorilla::Screen& gorillaScreen);

  public:
    const Cursor& dummy() override;
    void init() override;
  };

public:
  MouseCursor(DeveloperHud& hud);
};

} // namespace TweakUI
} // namespace Framework

#endif // FRAMEWORK_TWEAKUI_TWEAKUIMOUSECURSOR_H
