#ifndef FRAMEWORK_TWEAKUI_TWEAKUIHUDLAYER_H
#define FRAMEWORK_TWEAKUI_TWEAKUIHUDLAYER_H

#include <framework/developer-tools/developer-hud.h>
#include <framework/window/user-input/mouse-mode-layers.h>
#include <framework/window/user-input/mouse-cursor-layers.h>
#include <framework/developer-tools/tweak-ui/tweak-ui-root.h>

namespace Framework {
namespace TweakUI {

class TweakUIHudLayer final : public DeveloperHud::Layer
{
public:
  typedef std::shared_ptr<TweakUIHudLayer> Ptr;

private:
  Root::Ptr tweakUiRoot;

  MouseModeLayers::Layer::Ptr mouseModeLayer;
  MouseCursorLayers::Layer::Ptr mouseCursorLayer;

public:
  TweakUIHudLayer(DeveloperHud& hud, TweakUI::Gorilla::TextureAtlas* textureAtlas);

private:
  void show() override;
  void hide() override;

  void registerTerminalCommands(const std::shared_ptr<Base::Scripting::Index>& scriptIndex) override;
};

} // namespace TweakUI
} // namespace Framework

#endif // FRAMEWORK_TWEAKUI_TWEAKUIHUDLAYER_H
