#include "tweak-ui-mouse-cursor.h"

namespace Framework {
namespace TweakUI {

MouseCursor::CursorSet::CursorSet(::Gorilla::Screen& gorillaScreen)
  : noCursor(gorillaScreen, "invisible"),
    hoverCursor(gorillaScreen, "hover"),
    idleCursor(gorillaScreen, "invisible"),
    rotatingCursor(gorillaScreen, "invisible")
{
}


MouseCursor::CursorSet::Ptr MouseCursor::CursorSet::create(::Gorilla::Screen& gorillaScreen)
{
  return Ptr(new CursorSet(gorillaScreen));
}


const MouseCursor::Cursor& MouseCursor::CursorSet::dummy()
{
  return noCursor;
}


void MouseCursor::CursorSet::init()
{
  initDefaultCursor(DefaultMouseCursors::tweakUIHover, this->hoverCursor);
  initDefaultCursor(DefaultMouseCursors::tweakUIIdle, this->idleCursor);
  initDefaultCursor(DefaultMouseCursors::tweakUIRotate, this->rotatingCursor);
}


// ====


MouseCursor::MouseCursor(DeveloperHud& hud)
  : BaseMouseCursor(DeveloperHud::Layer::ZLayer::MOUSECURSOR,
                    hud.gorillaScreenTweakUIMouseCursor(),
                    CursorSet::create(hud.gorillaScreenTweakUIMouseCursor()))
{
}

} // namespace TweakUI
} // namespace Framework
