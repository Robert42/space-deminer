#ifndef FRAMEWORK_TWEAKUI_ROTOCURSORRESETTER_H
#define FRAMEWORK_TWEAKUI_ROTOCURSORRESETTER_H

#include <framework/window/user-input/mouse.h>
#include <framework/window/user-input/mouse-cursor.h>
#include <base/state-machines/function-pointer-based/final-state-machine.h>

namespace Framework {
namespace TweakUI {

class RotoCursorResetter final
{
public:
  typedef shared_ptr<RotoCursorResetter> Ptr;

private:
  typedef StateMachines::FunctionPointerBased::FinalStateMachine<> FinalStateMachine;
  typedef FinalStateMachine::StateTemplate<RotoCursorResetter> State;

public:
  Signals::Trackable trackable;

private:
  FinalStateMachine stateMachine;
  State idleState, rotoState;

  ivec2 cursorPositionBeforeUsingTheRotoSlider;

private:
  RotoCursorResetter();

public:
  static Ptr create();

  void afterHandlingMouseButton(bool pressed, MouseButton button);
  void afterHandlingMouseMovement();

private:
  void ignore();

  void enterRotoState();
  void leaveRotoState();

private:
  void activatedRotationCursor();
  void deactivatedRotationCursor(const Framework::MouseCursor::Ptr& nextCursor);

  void setMousePositionClamping(bool activated);
};

} // namespace TweakUI
} // namespace Framework

#endif // FRAMEWORK_TWEAKUI_ROTOCURSORRESETTER_H
