#include "tweak-ui-hud-layer.h"

#include "gorilla/tweak-ui-renderer.h"

#include <framework/window/user-input/mouse.h>

namespace Framework {
namespace TweakUI {


TweakUIHudLayer::TweakUIHudLayer(DeveloperHud& hud, Gorilla::TextureAtlas* textureAtlas)
  : Layer(DeveloperHud::Layer::ZLayer::TWEAK_UI,
          hud.gorillaScreenAntTweakBar())
{
  Gorilla::TweakUiRenderer* renderer = OGRE_NEW Gorilla::TweakUiRenderer(textureAtlas);

  tweakUiRoot = Root::create(renderer, this);

  gorillaLayer().appendCustomRenderer(renderer);

  mouseModeLayer = Mouse::mouseModeLayers().tweakUILayer;
  mouseCursorLayer = Mouse::mouseCursorLayers().tweakUILayer;
}

void TweakUIHudLayer::show()
{
  DeveloperHud::Layer::show();
  mouseModeLayer->show();
  mouseCursorLayer->show();
  tweakUiRoot->startHandlingInput();
}

void TweakUIHudLayer::hide()
{
  DeveloperHud::Layer::hide();
  mouseModeLayer->hide();
  mouseCursorLayer->hide();
  tweakUiRoot->stopHandlingInput();
}

void TweakUIHudLayer::registerTerminalCommands(const std::shared_ptr<Base::Scripting::Index>&)
{
  int r = 0;
  AngelScript::asIScriptEngine* scriptEngine = Scripting::Engine::angelScriptEngine();

  // ==== ::TweakUi
  r = scriptEngine->SetDefaultNamespace("TweakUi");
  Scripting::AngelScriptCheck(r);

  r = scriptEngine->RegisterGlobalFunction("void set_visible(bool visible)",
                                           AngelScript::asMETHOD(TweakUIHudLayer, setVisible),
                                           AngelScript::asCALL_THISCALL_ASGLOBAL,
                                           this);
  Scripting::AngelScriptCheck(r);
  r = scriptEngine->RegisterGlobalFunction("bool get_visible()",
                                           AngelScript::asMETHOD(TweakUIHudLayer, isVisible),
                                           AngelScript::asCALL_THISCALL_ASGLOBAL,
                                           this);
  Scripting::AngelScriptCheck(r);
}


} // namespace TweakUI
} // namespace Framework
