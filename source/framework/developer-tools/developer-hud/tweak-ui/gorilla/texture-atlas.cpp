#include "texture-atlas.h"

namespace Framework {
namespace TweakUI {
namespace Gorilla {

TextureAtlas::TextureAtlas() : ::Gorilla::TextureAtlas("tweak-ui-font.gorilla", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME)
{
}

void TextureAtlas::exchangeTexture(const ivec2& textureSize, const std::vector<unsigned char>& textureData, const ivec2& whitePoint)
{
  Ogre::DataStreamPtr stream(new Ogre::MemoryDataStream(const_cast<unsigned char*>(textureData.data()), textureData.size(), false, true));

  this->mTexture->unload();
  this->mTexture->loadRawData(stream, textureSize.x, textureSize.y, Ogre::PF_A4L4);
  this->mTexture->_dirtyState();
  this->mInverseTextureSize = Ogre::Vector2_cast(1.f / vec2(textureSize));
  this->mWhitePixel = Ogre::Vector2_cast(vec2(whitePoint)) * mInverseTextureSize;
}

} // namespace Gorilla
} // namespace TweakUI
} // namespace Framework
