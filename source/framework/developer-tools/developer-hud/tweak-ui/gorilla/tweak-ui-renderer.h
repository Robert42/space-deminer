#ifndef FRAMEWORK_TWEAKUI_GORILLA_TWEAKUIRENDERER_H
#define FRAMEWORK_TWEAKUI_GORILLA_TWEAKUIRENDERER_H

#include <framework/developer-tools/gorilla-helper/viewport-renderer.h>
#include <framework/window/user-input/mouse-cursor-layers.h>

namespace Framework {
namespace TweakUI {
namespace Gorilla {

class TextureAtlas;

class TweakUiRenderer : public GorillaHelper::ViewportRenderer
{
private:
  TextureAtlas* _textureAtlas;
  MouseCursorLayers::Layer::Ptr mouseCursorLayer;

public:
  TweakUiRenderer(TextureAtlas* textureAtlas);

  void _redraw() override;
  void _redrawImplementation() override;

  TextureAtlas* textureAtlas();

private:
  void updateCursor();
};

} // namespace Gorilla
} // namespace TweakUI
} // namespace Framework

#endif // FRAMEWORK_TWEAKUI_GORILLA_TWEAKUIRENDERER_H
