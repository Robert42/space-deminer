#ifndef FRAMEWORK_TWEAKUI_GORILLA_TEXTUREATLAS_H
#define FRAMEWORK_TWEAKUI_GORILLA_TEXTUREATLAS_H

#include <dependencies.h>

namespace Framework {

using namespace Base;

namespace TweakUI {
namespace Gorilla {


class TextureAtlas : public ::Gorilla::TextureAtlas
{
public:
  TextureAtlas();

  void exchangeTexture(const ivec2& textureSize, const std::vector<unsigned char>& textureData, const ivec2& whitePoint);
};


} // namespace Gorilla
} // namespace TweakUI
} // namespace Framework

#endif // FRAMEWORK_TWEAKUI_GORILLA_TEXTUREATLAS_H
