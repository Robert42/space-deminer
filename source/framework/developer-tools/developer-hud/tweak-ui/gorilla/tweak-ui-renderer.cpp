#include "tweak-ui-renderer.h"

#include <TweakUI.h>

#include <framework/window/user-input/mouse.h>

using namespace ::TweakUI;

namespace Framework {
namespace TweakUI {
namespace Gorilla {

TweakUiRenderer::TweakUiRenderer(TextureAtlas* textureAtlas)
  : _textureAtlas(textureAtlas)
{
  mouseCursorLayer = Mouse::mouseCursorLayers().tweakUILayer;
}

void TweakUiRenderer::_redraw()
{
  ViewportRenderer::_redraw();
  makeDirty();
}

void TweakUiRenderer::_redrawImplementation()
{
  mVertices.remove_all();
  TwDraw();

  updateCursor();
}

TextureAtlas* TweakUiRenderer::textureAtlas()
{
  return _textureAtlas;
}

void TweakUiRenderer::updateCursor()
{
  switch(TwCurrentCursor)
  {
  case TW_CURSOR_BottomLeft:
  case TW_CURSOR_TopRight:
    mouseCursorLayer->setCursor(DefaultMouseCursors::moveSwNeCursor);
    break;
  case TW_CURSOR_BottomRight:
  case TW_CURSOR_TopLeft:
    mouseCursorLayer->setCursor(DefaultMouseCursors::moveNwSeCursor);
    break;
  case TW_CURSOR_RotoHover:
    mouseCursorLayer->setCursor(DefaultMouseCursors::tweakUIHover);
    break;
  case TW_CURSOR_Rotation:
    mouseCursorLayer->setCursor(DefaultMouseCursors::tweakUIRotate);
    break;
  case TW_CURSOR_RotoIdle:
    mouseCursorLayer->setCursor(DefaultMouseCursors::tweakUIIdle);
    break;
  case TW_CURSOR_IBeam:
    mouseCursorLayer->setCursor(DefaultMouseCursors::textCursor);
    break;
  case TW_CURSOR_Move:
    mouseCursorLayer->setCursor(DefaultMouseCursors::moveCursor);
    break;
  case TW_CURSOR_WE:
    mouseCursorLayer->setCursor(DefaultMouseCursors::moveWECursor);
    break;
  case TW_CURSOR_NS:
    mouseCursorLayer->setCursor(DefaultMouseCursors::moveNSCursor);
    break;
  case TW_CURSOR_Arrow:
  default:
    mouseCursorLayer->setCursor(DefaultMouseCursors::defaultCursor);
  }
}

} // namespace Gorilla
} // namespace TweakUI
} // namespace Framework
