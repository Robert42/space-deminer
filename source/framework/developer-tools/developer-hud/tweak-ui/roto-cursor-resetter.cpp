#include "roto-cursor-resetter.h"

#include <framework/window/user-input/mouse.h>
#include <framework/window/user-input/mouse-position-constraint-layers.h>

namespace Framework {
namespace TweakUI {


RotoCursorResetter::RotoCursorResetter()
  : stateMachine(&idleState)
{
  idleState.set(this,
                &RotoCursorResetter::ignore,
                &RotoCursorResetter::ignore,
                &RotoCursorResetter::ignore);
  rotoState.set(this,
                &RotoCursorResetter::enterRotoState,
                &RotoCursorResetter::ignore,
                &RotoCursorResetter::leaveRotoState);

  DefaultMouseCursors::tweakUIIdle->signalActivate().connect(std::bind(&RotoCursorResetter::activatedRotationCursor, this)).track(this->trackable);
  DefaultMouseCursors::tweakUIIdle->signalDeactivate().connect(std::bind(&RotoCursorResetter::deactivatedRotationCursor, this, _1)).track(this->trackable);
  DefaultMouseCursors::tweakUIRotate->signalActivate().connect(std::bind(&RotoCursorResetter::activatedRotationCursor, this)).track(this->trackable);
  DefaultMouseCursors::tweakUIRotate->signalDeactivate().connect(std::bind(&RotoCursorResetter::deactivatedRotationCursor, this, _1)).track(this->trackable);
}


RotoCursorResetter::Ptr RotoCursorResetter::create()
{
  return Ptr(new RotoCursorResetter);
}


void RotoCursorResetter::enterRotoState()
{
  cursorPositionBeforeUsingTheRotoSlider = Mouse::absolutePosition();

  setMousePositionClamping(false);
}


void RotoCursorResetter::leaveRotoState()
{
  Mouse::setAbsolutePosition(cursorPositionBeforeUsingTheRotoSlider, false);

  setMousePositionClamping(true);
}


void RotoCursorResetter::afterHandlingMouseButton(bool pressed, MouseButton button)
{
  if(!pressed && button==MouseButton::LEFT)
  {
    stateMachine.gotoState(idleState);
    stateMachine.updateState();
  }
}


void RotoCursorResetter::setMousePositionClamping(bool activated)
{
  MousePositionConstraintLayers& mousePositionConstraintLayers = Mouse::mousePositionConstraintLayers();

  mousePositionConstraintLayers.rotoCursorLayer->setVisible(!activated);
}


void RotoCursorResetter::afterHandlingMouseMovement()
{
}


void RotoCursorResetter::activatedRotationCursor()
{
  stateMachine.gotoState(rotoState);
  stateMachine.updateState();
}


inline bool isRotationCursor(const Framework::MouseCursor::Ptr& cursor)
{
  return cursor==DefaultMouseCursors::tweakUIIdle || cursor==DefaultMouseCursors::tweakUIRotate;
}


void RotoCursorResetter::deactivatedRotationCursor(const Framework::MouseCursor::Ptr& nextCursor)
{
  if(!isRotationCursor(nextCursor))
  {
    stateMachine.gotoState(idleState);
    stateMachine.updateState();
  }
}


void RotoCursorResetter::ignore()
{
}


} // namespace TweakUI
} // namespace Framework
