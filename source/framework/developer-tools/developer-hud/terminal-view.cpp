#include "terminal-view.h"

#include <framework/developer-tools/terminal/terminal-manager.h>

#include <base/tango-colors.h>

namespace Framework {
namespace DeveloperHudElements {


TerminalView::TerminalView(Gorilla::Layer& gorillaLayer, Font& fontSmall, Font& fontLarge,
                           const shared_ptr<Terminal::Cursor>& userInputCursor)
  : fontSmall(fontSmall),
    fontLarge(fontLarge),
    gorillaLayer(gorillaLayer),
    userInputCursor(userInputCursor)
{
  active = false;
  mouseScrollingPosition = 0.0f;
  x = y = width = height = 0;
  mouseScrollDirection = 1.f;
  mouseGrabDirection = 1.f;

  font = &fontSmall;
  fontFactor = 1;

  textRenderer = new TextRenderer(*this);
  gorillaLayer.appendCustomRenderer(textRenderer);

  initFormatColor(Terminal::Format::Color::NORMAL);
  initFormatColor(Terminal::Format::Color::BLUE);
  initFormatColor(Terminal::Format::Color::CYAN);
  initFormatColor(Terminal::Format::Color::GREEN);
  initFormatColor(Terminal::Format::Color::MAGENTA);
  initFormatColor(Terminal::Format::Color::NORMAL);
  initFormatColor(Terminal::Format::Color::ORANGE);
  initFormatColor(Terminal::Format::Color::RED);
  initFormatColor(Terminal::Format::Color::YELLOW);

  buffer = Framework::Terminal::Manager::activate();

  terminalViewport.reset(new Terminal::Viewport(buffer));
  terminalContent.reset(new Terminal::Content(*terminalViewport));

  terminalViewport->signalRedrawNeeded().connect(std::bind(&TerminalView::markDirty, this)).track(this->trackable);

  mainBuffer = bufferStack.push(buffer);
  bufferStack.signalTopValueChanged().connect(std::bind(&TerminalView::setBuffer, this)).track(this->trackable);

  userInputCursor->signalCursorWasMoved().connect(std::bind(&TerminalView::recalculateCursor, this, false)).track(this->trackable);

  gorillaCursor = nullptr;

  Keyboard::signalKeyPressed().connect(InputDevice::ActionPriority::TERMINAL,
                                       std::bind(&TerminalView::handleKey, this, _1)).track(this->trackable);
  Mouse::signalMouseMoved().connect(InputDevice::ActionPriority::TERMINAL,
                                    std::bind(&TerminalView::handleMouseMovement, this)).track(this->trackable);
  Mouse::signalMouseWheelMoved().connect(InputDevice::ActionPriority::TERMINAL,
                                         std::bind(&TerminalView::handleMouseWheel, this)).track(this->trackable);
}

TerminalView::~TerminalView()
{
  mainBuffer.reset();

  terminalContent.reset();
  terminalViewport.reset();
}

int TerminalView::lineHeight() const
{
  return font->lineHeight();
}

int TerminalView::lineSpacing() const
{
  return font->lineSpacing();
}

int TerminalView::widthInChars() const
{
  return terminalViewport->width();
}

int TerminalView::heightInChars() const
{
  return terminalViewport->height();
}

void TerminalView::setBuffer()
{
  if(!bufferStack.empty())
    terminalViewport->setBuffer(bufferStack.topValue());
}

PrivateTerminalView::Ptr TerminalView::createPrivateTerminalView()
{
  return PrivateTerminalView::Ptr(
        new PrivateTerminalView(bufferStack.push(Terminal::Buffer::create()),
                                *terminalViewport));
}

void TerminalView::initFormatColor(Terminal::Format::Color color)
{
  initFormat(Terminal::Format(color, Terminal::Format::Light::DARKER));
  initFormat(Terminal::Format(color, Terminal::Format::Light::NORMAL));
  initFormat(Terminal::Format(color, Terminal::Format::Light::BRIGHTER));
}

void TerminalView::initFormat(const Terminal::Format& format)
{
  terminalColors.insert(format, asRgbaColor(format));
}

void TerminalView::setAllocation(int x, int y, int width, int height)
{
  if(this->x == x && this->y==y && this->width == width && this->height==height)
    return;

  this->x = x;
  this->y = y;
  this->width = width;
  this->height = height;

  updateTerminalViewportSize();
}

void TerminalView::updateTerminalViewportSize()
{
  this->terminalViewport->setSize(this->width / (this->font->characterWidth()),
                                  this->height / (this->font->lineHeight()));
}

void TerminalView::setFontSize(int fontSize)
{
  fontSize = clamp(fontSize, 0, 4);

  if(this->fontSize() == fontSize)
    return;

  if(fontSize>0)
    font = &fontLarge;
  else
    font = &fontSmall;

  if(fontSize>=1)
    fontFactor = fontSize;

  fontLarge.scaleFactor = fontFactor;
  fontSmall.scaleFactor = fontFactor;

  updateTerminalViewportSize();
  recalculateCursor(true);
}

int TerminalView::fontSize() const
{
  if(font == &fontSmall)
    return 0;
  return fontFactor;
}

void TerminalView::setInvertScrollDirection(bool invert)
{
  mouseScrollDirection = invert ? -1.f : 1.f;
}

bool TerminalView::invertScrollDirection() const
{
  return mouseScrollDirection < 0.f;
}

void TerminalView::setInvertGrabDirection(bool invert)
{
  mouseGrabDirection = invert ? -1.f : 1.f;
}

bool TerminalView::invertGrabDirection() const
{
  return mouseGrabDirection < 0.f;
}

void TerminalView::recalculateCursor(bool sizeWasChanged)
{
  Optional<ivec2> _position = terminalViewport->optionalPositionWithinViewport(*userInputCursor);

  if(sizeWasChanged || !_position)
  {
    if(gorillaCursor != nullptr)
    {
      gorillaLayer.destroyRectangle(gorillaCursor);
      gorillaCursor = nullptr;
    }
  }

  if(_position)
  {
    ivec2 position = *_position;
    Ogre::Real left, top;

    left = position.x * font->characterWidth() + x - 1;
    top = position.y * font->lineHeight() + y;

    if(gorillaCursor == nullptr)
    {
      gorillaCursor = gorillaLayer.createRectangle(left, top, 2, font->lineHeight());
    }else
    {
      gorillaCursor->left(left);
      gorillaCursor->top(top);
    }
  }
}

void TerminalView::markDirty()
{
  textRenderer->makeDirty();
  recalculateCursor(false);
}

void TerminalView::redrawNow()
{
  for(FormatColors::const_iterator i=terminalColors.begin(); i!=terminalColors.end(); ++i)
  {
    const Terminal::Format& format = i.key();

    Optional<String> content = terminalContent->contentAsPlainString(format);
    if(!content)
      continue;

    const vec4& color = i.value();

    int fontId = this->font->gorillaIndex(format.light);

    GorillaHelper::PrimitiveRenderer::Font font(*this->textRenderer, fontId, true);
    font.color = color;
    font.roundPositition = true;
    font.verticalGlyphOffset = font.baseline();
    font.setScale(this->fontFactor);

    vec2 cursor(this->x, this->y);

    std::list<String> lines = content->split('\n', false);

    for(const String& line : lines)
    {
      if(!line.empty())
      {

        font.cursor = cursor;
        font.drawTextLine(line);
      }

      cursor.y += this->font->lineHeight();
    }
  }
}

vec4 TerminalView::asRgbaColor(const Terminal::Format& format)
{
  if(format.light == Terminal::Format::Light::DARKER)
  {
    return asRgbaColor(Terminal::Format(format.color, Terminal::Format::Light::NORMAL));
  }

  switch(format.light)
  {
  case Terminal::Format::Light::BRIGHTER:
    switch(format.color)
    {
    case Terminal::Format::Color::BLUE:
      return Tango::SkyBlue_Light;
    case Terminal::Format::Color::CYAN:
      return Tango::Turquoise_Light;
    case Terminal::Format::Color::GREEN:
      return Tango::Chameleon_Light;
    case Terminal::Format::Color::MAGENTA:
      return vec4(1, 0, 1, 1);
    case Terminal::Format::Color::ORANGE:
      return Tango::Orange_Light;
    case Terminal::Format::Color::RED:
      return Tango::ScarletRed_Light;
    case Terminal::Format::Color::YELLOW:
      return Tango::Butter_Light;
    case Terminal::Format::Color::NORMAL:
    default:
      return vec4(1);
    }
  case Terminal::Format::Light::NORMAL:
  default:
    switch(format.color)
    {
    case Terminal::Format::Color::BLUE:
      return Tango::SkyBlue_Middle;
    case Terminal::Format::Color::CYAN:
      return Tango::Turquoise_Middle;
    case Terminal::Format::Color::GREEN:
      return Tango::Chameleon_Dark;
    case Terminal::Format::Color::MAGENTA:
      return vec4(0.7f, 0.1f, 0.7f, 1.0f);
    case Terminal::Format::Color::ORANGE:
      return Tango::Orange_Middle;
    case Terminal::Format::Color::RED:
      return Tango::ScarletRed_Middle;
    case Terminal::Format::Color::YELLOW:
      return Tango::Butter_Middle;
    case Terminal::Format::Color::NORMAL:
    default:
      return Tango::GreyLight_Light;
    }
  }
}

bool TerminalView::handleKey(KeyCode key)
{
  if(!isActive())
    return false;

  switch(key.value)
  {
  case KeyCode::PAGEDOWN:
    terminalViewport->moveDown(terminalViewport->height()-1);
    return true;
  case KeyCode::PAGEUP:
    terminalViewport->moveUp(terminalViewport->height()-1);
    return true;
  default:
    return false;
  }
}

bool TerminalView::handleMouseMovement()
{
  if(!isActive())
    return false;

  if(Mouse::isButtonPressed(MouseButton::MIDDLE))
  {
    bool control = Keyboard::isModifierDown(KeyModifier::CONTROL);
    bool shift = Keyboard::isModifierDown(KeyModifier::SHIFT);
    real factor;

    if(shift)
      factor = 0.75f;
    else if(control)
      factor = 0.1f;
    else
      factor = 0.3f;

    real movement = Mouse::relativeMovement().y * factor * mouseGrabDirection;

    mouseScrollingPosition += movement;

    if(abs(mouseScrollingPosition) >= 1.0f)
    {
      bool negative = mouseScrollingPosition < 0.0f;
      mouseScrollingPosition = abs(mouseScrollingPosition);

      int linesToMove = floor(mouseScrollingPosition);
      mouseScrollingPosition -= linesToMove;

      if(negative)
      {
        linesToMove = -linesToMove;
        mouseScrollingPosition = -mouseScrollingPosition;
      }

      terminalViewport->move(linesToMove);
    }
    return true;
  }else
    return false;
}

bool TerminalView::handleMouseWheel()
{
  if(!isActive())
    return false;

  bool control = Keyboard::isModifierDown(KeyModifier::CONTROL);
  bool shift = Keyboard::isModifierDown(KeyModifier::SHIFT);

  float wheel = Mouse::wheelMovement() * mouseScrollDirection;

  if(control)
    setFontSize(fontSize() + 2*(wheel > 0) - 1);
  else
    terminalViewport->move(wheel * (shift ? 3.0f : 1.0f));

  return true;
}

bool TerminalView::isActive() const
{
  return active;
}

void TerminalView::setActive(bool active)
{
  if(isActive() == active)
    return;

  this->active = active;
}

// ====

TerminalView::TextRenderer::TextRenderer(TerminalView& terminalView)
  : terminalView(terminalView)
{
}

void TerminalView::TextRenderer::_redrawImplementation()
{
  mVertices.remove_all();

  terminalView.redrawNow();
}

void TerminalView::TextRenderer::makeDirty()
{
  PrimitiveRenderer::makeDirty();
}


}
}
