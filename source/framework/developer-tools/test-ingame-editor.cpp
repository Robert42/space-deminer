#include "test-ingame-editor.h"
#include "test-spatial-gui.h"

namespace Framework {

TestIngameEditor::TestIngameEditor()
{
  testWindow = Gui::Window::Ptr(new Gui::Window);
  testWindow->setRootWidget(TestSpatialGui::createTestGui());

  windowManager.addMainWindow(testWindow);
}

TestIngameEditor::~TestIngameEditor()
{
}

} // namespace Framework
