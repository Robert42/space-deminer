#ifndef FRAMEWORK_TEXTUREVIEWER_H
#define FRAMEWORK_TEXTUREVIEWER_H

#include "developer-hud.h"

#include <framework/developer-tools/editor/base/editor-2d/view.h>
#include <framework/developer-tools/editor/base/editor-2d/controller.h>
#include <framework/developer-tools/editor/base/editor-2d/renderer.h>
#include <framework/developer-tools/editor/blender-style-input/editor-2d/input-handler.h>
#include <framework/developer-tools/gorilla-helper/viewport-renderer.h>
#include <framework/timer.h>

namespace Framework {

class TextureViewer : public DeveloperHud::Layer
{
private:
  BEGIN_ENUMERATION(Checkerboard,)
    NONE,
    DARK,
    MIDDLE,
    LIGHT,
    BLACK,
    GREY,
    WHITE,
  ENUMERATION_BASIC_OPERATORS(Checkerboard)
  END_ENUMERATION;

  BEGIN_ENUMERATION(DisplayMode,)
    NORMAL,
    IGNORE_ALPHA,
    RED_CHANNEL,
    GREEN_CHANNEL,
    BLUE_CHANNEL,
    ALPHA_CHANNEL
  ENUMERATION_BASIC_OPERATORS(DisplayMode)
  END_ENUMERATION;

  BEGIN_ENUMERATION(DisplayFilter,)
    NONE,
    PREMULTIPLY,
    UNDO_PREMULTIPLY,
    FALSE_COLOR
  ENUMERATION_BASIC_OPERATORS(DisplayFilter)
  END_ENUMERATION;

  BEGIN_ENUMERATION(DisplayHighlight,)
    NONE,
    ALL,
    LOWER_0,
    GREATER_1,
    OUTSIDE_RANGE,
    NOT_A_NUMBER,
    INFINITE_VALUE,
    NUMERIC_ERRORS
  ENUMERATION_BASIC_OPERATORS(DisplayHighlight)
  END_ENUMERATION;

public:
  BEGIN_ENUMERATION(Usage,)
    TEXTURE_BROWSER
  ENUMERATION_BASIC_OPERATORS(Usage)
  END_ENUMERATION;

  static QMap<Usage::Value, Gorilla::Screen*> _gorillaScreens;

private:
  class TextureModel;

  class Renderer : public GorillaHelper::ViewportRenderer
  {
  public:
    const Editor::Base::Editor2D::View::Ptr view;
    Editor::Base::Editor2D::Renderer renderer;

    vec4 lightTile, darkTile;
    bool grayBackground;

    /**
     * - red: encodes the displaymode
     * - green: encodes the displayfilter
     * - blue: encodes the start of the interesting range
     * - alpha: encodes the size of the interesting range
     */
    vec4 displayParameter;

    /**
     * - red: nan
     * - green: inf
     * - blue: <0
     * - alpha: >1
     */
    vec4 highlightParameter;

    void setDisplayMode(DisplayMode mode);
    void setDisplayFilter(DisplayFilter filter);
    void setDisplayHighlight(DisplayHighlight highlight);
    void setDisplayRange(real begin, real end);

  public:
    Renderer(const Editor::Base::Editor2D::View::Ptr& view);

    void makeDirty();

  public:
    void  _redrawImplementation() override;

  private:
    ActiveTimer timer;
    bool noHighlight;
    bool temporaryInvisible;

    void blink();
    vec4 encodeTileColors(const vec4& lightTile, const vec4& darkTile);
  };

  Signals::CallableSignal<void(const Ogre::TexturePtr& texture)> _signalTextureChanged;

  std::shared_ptr<TextureModel> model;
  std::shared_ptr<Editor::Base::Editor2D::Controller> controller;
  std::shared_ptr<Editor::BlenderStyleInput::Editor2D::InputHandler> inputHandler;
  Renderer* renderer;
  const Usage usage;

  Checkerboard _checkerboard;
  DisplayMode _displayMode;
  DisplayFilter _displayFilter;
  DisplayHighlight _displayHighlight;
  real rangeStart, rangeEnd;

public:
  TextureViewer(Usage usage);
  ~TextureViewer();

  void setTexture(const Ogre::String& name);
  const Editor::Base::Editor2D::View::Ptr& view();

  void setCheckerboard(Checkerboard checkerboard);
  void setCheckerboardByIndex(int index);
  Checkerboard checkerboard() const;

  void setGrayBackground(bool grayBackground);
  bool grayBackground() const;

  void setDisplayMode(DisplayMode mode);
  void setDisplayModeByIndex(int index);
  DisplayMode displayMode() const;

  void setDisplayFilter(DisplayFilter filter);
  void setDisplayFilterByIndex(int index);
  DisplayFilter displayFilter() const;

  void setDisplayRangeStart(real displayRangeStart);
  real displayRangeStart() const;

  void setDisplayRangeEnd(real displayRangeEnd);
  real displayRangeEnd() const;

  void setDisplayHighlight(DisplayHighlight highlight);
  void setDisplayHighlightByIndex(int index);
  DisplayHighlight displayHighlight() const;

  void setInvertY(bool invertY);
  bool invertY() const;

  Signals::Signal<void(const Ogre::TexturePtr& texture)>& signalTextureChanged();

private:
  Ogre::String materialName() const;
};

} // namespace Framework

#endif // FRAMEWORK_TEXTUREVIEWER_H
