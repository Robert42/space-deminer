#ifndef FRAMEWORK_DEVELOPERTOOLS_H
#define FRAMEWORK_DEVELOPERTOOLS_H

#include <dependencies.h>

namespace Framework {

class TextureBrowser;
class CodeGeneratorTool;
class TestIngameEditor;
class TestSpatialGui;
class TestAnimations;
class TestCEGUI;

class DeveloperTools
{
private:
  std::shared_ptr<TextureBrowser> textureBrowser;
  std::shared_ptr<CodeGeneratorTool> codeGenerator;
  std::shared_ptr<TestIngameEditor> testIngameEditor;
  std::shared_ptr<TestSpatialGui> testSpatialGui;
  std::shared_ptr<TestAnimations> testAnimations;
  std::shared_ptr<TestCEGUI> testCEGUI;

public:
  DeveloperTools();

  void startTextureBrowser();
  void quitTextureBrowser();

  void startCodeGenerator();
  void quitCodeGenerator();

  void startIngameEditor();
  void quitIngameEditor();

  void startSpatialGui();
  void quitSpatialGui();

  void startTestAnimations();
  void quitTestAnimations();

  void startCEGUI();
  void quitCEGUI();
};

} // namespace Framework

#endif // FRAMEWORK_DEVELOPERTOOLS_H
