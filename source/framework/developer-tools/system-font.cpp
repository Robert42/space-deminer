#include "system-font.h"

#include <base/io/directory.h>

#include <CEGUI/FontManager.h>


namespace Framework {


SystemFont systemFont;


SystemFont::SystemFont()
{
  style[Flags::NORMAL].fontName = "system-font";
  style[Flags::BOLD].fontName = "system-font.bold";
  style[Flags::MONOSPACE].fontName = "system-font.monospace";
  style[Flags::BOLD|Flags::MONOSPACE].fontName = "system-font.monospace.bold";
}


String SystemFont::getFontName(float size, Flags flags)
{
  init();

  return String::fromCeguiString(style[flags.value].loadSize(size));
}


void SystemFont::init()
{
  for(int i=0; i<4; ++i)
  {
    style[i].init(style[0], findFontFile(Flags::Value(i)));
  }
}


IO::File SystemFont::findFontFile(Flags flags)
{
  return findFontFile((flags & Flags::BOLD) != Flags::NORMAL,
                      (flags & Flags::MONOSPACE) != Flags::NORMAL);
}


// ====

bool SystemFont::Style::isInitialized() const
{
  return !this->resourceGroup.empty();
}

void SystemFont::Style::init(Style& defaultStyle, const IO::File& fontFile)
{
  if(this->isInitialized())
    return;

  assert(!this->fontName.empty());

  this->fontFile = fontFile;
  this->resourceGroup = this->fontName;

  if(defaultStyle.fontFile.parentDirectory().pathAsString() == this->fontFile.parentDirectory().pathAsString())
    resourceGroup = "system-fonts";
}

CEGUI::String SystemFont::Style::loadSize(float size)
{
  CEGUI::FontManager& fontManager = CEGUI::FontManager::getSingleton();

  CEGUI::String fontName = String::compose("%0-%1", String::fromCeguiString(this->fontName), size).toCeguiString();

  if(!fontManager.isDefined(fontName))
    fontManager.createFreeTypeFont(fontName, size, true, this->fontFile.pathAsCeguiString(), this->resourceGroup);

  return fontName;
}


} // namespace Framework
