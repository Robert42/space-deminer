#ifndef _FRAMEWORK_TERMINAL_H_
#define _FRAMEWORK_TERMINAL_H_

#include <dependencies.h>

#include "terminal/buffer/terminal-format.h"

#include <base/strings/string.h>

namespace Framework {
using namespace Base;

namespace Terminal {

void ensureBeginningOfLine();

void writeOut(const Format& format, const String& text);
void clear();

template<typename... argument_types>
inline void writeOut(const Format& format, const String& text, const argument_types& ... arguments)
{
  writeOut(format, String::compose(text, arguments...));
}

template<typename... argument_types>
inline void writeOut(const String& text, const argument_types& ... arguments)
{
  writeOut(Format(), String::compose(text, arguments...));
}

}
}


#endif
