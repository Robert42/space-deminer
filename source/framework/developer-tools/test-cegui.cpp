#include "test-cegui.h"
#include "../cegui-system.h"

#include <CEGUI/WindowManager.h>
#include <CEGUI/falagard/WidgetLookManager.h>

#include <CEGUI/widgets/FrameWindow.h>
#include <CEGUI/widgets/PushButton.h>
#include <CEGUI/widgets/ToggleButton.h>
#include <CEGUI/widgets/RadioButton.h>
#include <CEGUI/widgets/Editbox.h>
#include <CEGUI/widgets/Spinner.h>
#include <CEGUI/widgets/Scrollbar.h>

namespace Framework {


TestCEGUI::TestCEGUI()
{
  this->mouseModeLayer = Mouse::mouseModeLayers().ingameUILayer;
  this->mouseCursorLayer = Mouse::mouseCursorLayers().ingameUILayer;

  this->mouseModeLayer->setMouseMode(MouseMode::INGAME);
  this->mouseModeLayer->setVisible(true);
  this->mouseCursorLayer->setCursor(DefaultMouseCursors::defaultCursor);
  this->mouseCursorLayer->setCursor(DefaultMouseCursors::noCursor); // use the no Cursor as dummy in order not to show both, the goriila and the cegui cursor
  this->mouseCursorLayer->setVisible(true);

  CeguiSystem::updateMousePosition();

  createTestGUI();
}

TestCEGUI::~TestCEGUI()
{
  myRoot->destroy();

  this->mouseModeLayer->setVisible(false);
  this->mouseCursorLayer->setVisible(false);
}


// Source: http://static.cegui.org.uk/docs/0.8.4/window_tutorial.html
void TestCEGUI::createTestGUI()
{
  CEGUI::WindowManager& wmgr = CEGUI::WindowManager::getSingleton();

  myRoot = wmgr.createWindow("DefaultWindow", "root");
  CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(myRoot);

  int spacing = 8;
  int i=1;

  CEGUI::FrameWindow* wnd = reinterpret_cast<CEGUI::FrameWindow*>(wmgr.createWindow("dark-theme.window-frame", "testWindow"));
  wnd->setPosition(CEGUI::UVector2( CEGUI::UDim( 0.25f, 0.0f ), CEGUI::UDim( 0.25f, 0.0f ) ) );
  wnd->setSize(CEGUI::USize(CEGUI::UDim( 0.5f, 0.0f ), CEGUI::UDim(0.5f, 0.0f)));
  wnd->setText("Hello World!");
  myRoot->addChild(wnd);
  i++;

  CEGUI::ToggleButton* checkBox = reinterpret_cast<CEGUI::ToggleButton*>(wmgr.createWindow("dark-theme.check-button", "testCheckBox"));
  checkBox->setPosition(CEGUI::UVector2( CEGUI::UDim( 0.f, spacing), CEGUI::UDim( 0.f, 0.0f + spacing*i ) ) );
  checkBox->setSize(CEGUI::USize(CEGUI::UDim( 0.0f, 75.0f ), CEGUI::UDim(0.0f, 22.f)));
  checkBox->setText("Checkbox");
  wnd->addChild(checkBox);
  i++;

  CEGUI::RadioButton* radioButton = reinterpret_cast<CEGUI::RadioButton*>(wmgr.createWindow("dark-theme.radio-button", "testRadioButton"));
  radioButton->setPosition(CEGUI::UVector2( CEGUI::UDim( 0.f, spacing), CEGUI::UDim( 0.f, 22.0f + spacing*i ) ) );
  radioButton->setSize(CEGUI::USize(CEGUI::UDim( 0.0f, 75.0f ), CEGUI::UDim(0.0f, 22.f)));
  radioButton->setText("RadioButton");
  wnd->addChild(radioButton);
  i++;

  CEGUI::Window* label = wmgr.createWindow("dark-theme.label", "testLabel");
  label->setPosition(CEGUI::UVector2( CEGUI::UDim( 0.f, spacing), CEGUI::UDim( 0.f, 44.0f + spacing*i ) ) );
  label->setSize(CEGUI::USize(CEGUI::UDim( 0.0f, 75.0f ), CEGUI::UDim(0.0f, 22.f)));
  label->setText("Label");
  wnd->addChild(label);
  i++;

  CEGUI::Window* insensitiveLabel = wmgr.createWindow("dark-theme.label", "testLabelInsensitive");
  insensitiveLabel->setPosition(CEGUI::UVector2( CEGUI::UDim( 0.f, spacing), CEGUI::UDim( 0.f, 66.0f + spacing*i ) ) );
  insensitiveLabel->setSize(CEGUI::USize(CEGUI::UDim( 0.0f, 75.0f ), CEGUI::UDim(0.0f, 22.f)));
  insensitiveLabel->setText("Insensitive");
  insensitiveLabel->disable();
  wnd->addChild(insensitiveLabel);
  i++;

  CEGUI::Editbox* textBox = reinterpret_cast<CEGUI::Editbox*>(wmgr.createWindow("dark-theme.text-box", "testTextBox"));
  textBox->setPosition(CEGUI::UVector2( CEGUI::UDim( 0.f, spacing), CEGUI::UDim( 0.f, 88.0f + spacing*i ) ) );
  textBox->setSize(CEGUI::USize(CEGUI::UDim( 0.0f, 100.0f ), CEGUI::UDim(0.0f, 22.f)));
  textBox->setText("TextBox g_j^°");
  wnd->addChild(textBox);
  i++;

  CEGUI::Window* joinedArea = wmgr.createWindow("DefaultWindow", "joinedWidgetsBackground");
  joinedArea->setPosition(CEGUI::UVector2( CEGUI::UDim( 0.f, spacing), CEGUI::UDim( 0.f, 110.0f + spacing*i ) ) );
  joinedArea->setSize(CEGUI::USize(CEGUI::UDim( 0.0f, 66.0f ), CEGUI::UDim(0.0f, 64.f)));
  wnd->addChild(joinedArea);
  CEGUI::ToggleButton* toggleButton1 = reinterpret_cast<CEGUI::ToggleButton*>(wmgr.createWindow("dark-theme.toggle-button.joined-border-nwse", "testToggleButton1"));
  toggleButton1->setPosition(CEGUI::UVector2( CEGUI::UDim( 0.f, 0), CEGUI::UDim( 0.f, 0.f ) ) );
  toggleButton1->setSize(CEGUI::USize(CEGUI::UDim( 0.0f, 33.0f ), CEGUI::UDim(0.0f, 22.f)));
  toggleButton1->setText("A");
  joinedArea->addChild(toggleButton1);
  CEGUI::ToggleButton* toggleButton2 = reinterpret_cast<CEGUI::ToggleButton*>(wmgr.createWindow("dark-theme.toggle-button.joined-border-nwse", "testToggleButton2"));
  toggleButton2->setPosition(CEGUI::UVector2( CEGUI::UDim( 0.f, 32.0f ), CEGUI::UDim( 0.f, 0.f ) ) );
  toggleButton2->setSize(CEGUI::USize(CEGUI::UDim( 0.0f, 34.0f ), CEGUI::UDim(0.0f, 22.f)));
  toggleButton2->setText("B");
  joinedArea->addChild(toggleButton2);
  CEGUI::ToggleButton* toggleButton3 = reinterpret_cast<CEGUI::ToggleButton*>(wmgr.createWindow("dark-theme.toggle-button.joined-border-nwse", "testToggleButton3"));
  toggleButton3->setPosition(CEGUI::UVector2( CEGUI::UDim( 0.f, 0 ), CEGUI::UDim( 0.f, 21.0f) ) );
  toggleButton3->setSize(CEGUI::USize(CEGUI::UDim( 0.0f, 66.0f ), CEGUI::UDim(0.0f, 22.f)));
  toggleButton3->setText("C");
  joinedArea->addChild(toggleButton3);
  CEGUI::ToggleButton* toggleButton4 = reinterpret_cast<CEGUI::ToggleButton*>(wmgr.createWindow("dark-theme.toggle-button.joined-border-nwse", "testToggleButton4"));
  toggleButton4->setPosition(CEGUI::UVector2( CEGUI::UDim( 0.f, 0 ), CEGUI::UDim( 0.f, 42.0f) ) );
  toggleButton4->setSize(CEGUI::USize(CEGUI::UDim( 0.0f, 22.0f ), CEGUI::UDim(0.0f, 22.f)));
  toggleButton4->setText("D");
  joinedArea->addChild(toggleButton4);
  CEGUI::ToggleButton* toggleButton5 = reinterpret_cast<CEGUI::ToggleButton*>(wmgr.createWindow("dark-theme.toggle-button.joined-border-nwse", "testToggleButton5"));
  toggleButton5->setPosition(CEGUI::UVector2( CEGUI::UDim( 0.f, 21 ), CEGUI::UDim( 0.f, 42.0f ) ) );
  toggleButton5->setSize(CEGUI::USize(CEGUI::UDim( 0.0f, 23.0f ), CEGUI::UDim(0.0f, 22.f)));
  toggleButton5->setText("E");
  joinedArea->addChild(toggleButton5);
  CEGUI::ToggleButton* toggleButton6 = reinterpret_cast<CEGUI::ToggleButton*>(wmgr.createWindow("dark-theme.toggle-button.joined-border-nwse", "testToggleButton6"));
  toggleButton6->setPosition(CEGUI::UVector2( CEGUI::UDim( 0.f, 43 ), CEGUI::UDim( 0.f, 42.0f) ) );
  toggleButton6->setSize(CEGUI::USize(CEGUI::UDim( 0.0f, 23.0f ), CEGUI::UDim(0.0f, 22.f)));
  toggleButton6->setText("F");
  joinedArea->addChild(toggleButton6);
  CEGUI::Window* joinedButtonsFrame = wmgr.createWindow("dark-theme.joined-border", "testJoinedWidgets1");
  joinedButtonsFrame->setPosition(CEGUI::UVector2( CEGUI::UDim( 0.f, 0 ), CEGUI::UDim( 0.f, 0.0f ) ) );
  joinedButtonsFrame->setSize(CEGUI::USize(CEGUI::UDim( 0.0f, 66.0f ), CEGUI::UDim(0.0f, 64.f)));
  joinedButtonsFrame->setMousePassThroughEnabled(true);
  joinedButtonsFrame->setAlwaysOnTop(true);
  joinedArea->addChild(joinedButtonsFrame);



  i=1;
  CEGUI::Window* shadowFrame = wmgr.createWindow("dark-theme.shadow-frame.in", "testShadowFrame1");
  shadowFrame->setPosition(CEGUI::UVector2( CEGUI::UDim( 0.f, 100.f +spacing*2), CEGUI::UDim( 0.f, 0.0f + spacing*i) ) );
  shadowFrame->setSize(CEGUI::USize(CEGUI::UDim( 0.0f, 32.f), CEGUI::UDim(0.0f, 32.f)));
  wnd->addChild(shadowFrame);
  i++;

  shadowFrame = wmgr.createWindow("dark-theme.shadow-frame.out", "testShadowFrame2");
  shadowFrame->setPosition(CEGUI::UVector2( CEGUI::UDim( 0.f, 100.f +spacing*2), CEGUI::UDim( 0.f, 32.0f + spacing*i) ) );
  shadowFrame->setSize(CEGUI::USize(CEGUI::UDim( 0.0f, 32.f), CEGUI::UDim(0.0f, 32.f)));
  wnd->addChild(shadowFrame);
  i++;

  CEGUI::Spinner* spinner = reinterpret_cast<CEGUI::Spinner*>(wmgr.createWindow("dark-theme.number-box", "testNumberBox"));
  spinner->setPosition(CEGUI::UVector2( CEGUI::UDim( 0.f, 100.f +spacing*2), CEGUI::UDim( 0.f, 64.0f + spacing*i) ) );
  spinner->setSize(CEGUI::USize(CEGUI::UDim( 0.0f, 100.f), CEGUI::UDim(0.0f, 22.f)));
  spinner->setCurrentValue(42);
  wnd->addChild(spinner);
  i++;

  CEGUI::Scrollbar* scrollbar = reinterpret_cast<CEGUI::Scrollbar*>(wmgr.createWindow("dark-theme.scrollbar.horizontal", "testScrollbarH"));
  scrollbar->setPosition(CEGUI::UVector2( CEGUI::UDim( 0.f, 100.f +spacing*2), CEGUI::UDim( 0.f, 86.0f + spacing*i) ) );
  scrollbar->setSize(CEGUI::USize(CEGUI::UDim( 0.0f, 100.f), CEGUI::UDim(0.0f, 22.f)));
  scrollbar->setDocumentSize(100.f);
  scrollbar->setPageSize(10.f);
  scrollbar->setOverlapSize(0.f);
  scrollbar->setScrollPosition(42.f);
  wnd->addChild(scrollbar);
  i++;



  i=1;
  scrollbar = reinterpret_cast<CEGUI::Scrollbar*>(wmgr.createWindow("dark-theme.scrollbar.vertical", "testScrollbarV"));
  scrollbar->setPosition(CEGUI::UVector2( CEGUI::UDim( 0.f, 200.f +spacing*3), CEGUI::UDim( 0.f, 0.0f + spacing*i) ) );
  scrollbar->setSize(CEGUI::USize(CEGUI::UDim( 0.0f, 17.f), CEGUI::UDim(0.0f, 100.f)));
  scrollbar->setDocumentSize(100.f);
  scrollbar->setPageSize(10.f);
  scrollbar->setOverlapSize(0.f);
  scrollbar->setScrollPosition(42.f);
  wnd->addChild(scrollbar);
  i++;



  CEGUI::PushButton* defaultbtn = reinterpret_cast<CEGUI::PushButton*>(wmgr.createWindow("dark-theme.default-button", "testDefaultButton"));
  defaultbtn->setPosition(CEGUI::UVector2( CEGUI::UDim( 1.f, -75.0f*2-2*spacing), CEGUI::UDim( 1.f, -25.0f-spacing ) ) );
  defaultbtn->setSize(CEGUI::USize(CEGUI::UDim( 0.0f, 75.0f ), CEGUI::UDim(0.0f, 25.f)));
  defaultbtn->setText("Default");
  wnd->addChild(defaultbtn);

  CEGUI::PushButton* btn = reinterpret_cast<CEGUI::PushButton*>(wmgr.createWindow("dark-theme.button", "testPushButton"));
  btn->setPosition(CEGUI::UVector2( CEGUI::UDim( 1.f, -75.0f-spacing ), CEGUI::UDim( 1.f, -25.0f-spacing ) ) );
  btn->setSize(CEGUI::USize(CEGUI::UDim( 0.0f, 75.0f ), CEGUI::UDim(0.0f, 25.f)));
  btn->setText("Ok");
  wnd->addChild(btn);
}


void testExportCeguiLooknfeel(const std::string& prefix, const std::string& filename)
{
  std::ofstream s;
  s.open(filename);
  CEGUI::WidgetLookManager::getSingleton().writeWidgetLookSeriesToStream(prefix, s);
}



} // namespace Framework
