#ifndef FRAMEWORK_TESTINGAMEEDITOR_H
#define FRAMEWORK_TESTINGAMEEDITOR_H

#include <dependencies.h>

#include <framework/developer-tools/gui-system/ingame/flat/window-manager.h>
#include <framework/developer-tools/gui-system/window.h>

namespace Framework {

class TestIngameEditor final : noncopyable
{
public:
  Signals::Trackable trackable;

private:
  Gui::Ingame::Flat::WindowManager windowManager;
  Gui::Window::Ptr testWindow;

public:
  TestIngameEditor();
  ~TestIngameEditor();
};

} // namespace Framework

#endif // FRAMEWORK_TESTINGAMEEDITOR_H
