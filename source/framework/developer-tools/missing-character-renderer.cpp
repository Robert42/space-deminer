#include "missing-character-renderer.h"
#include <base/geometry/matrix3-transformations.h>
#include <base/interpolation.h>

namespace Framework {

MissingCharacterRenderer::Metrics::Metrics()
{
  setToSmallestWithFullShape(4, 2);
}

void MissingCharacterRenderer::Metrics::setToOnePixel(int nDigits, int nColumns)
{
  this->nColumns = nColumns;
  this->nDigits = nDigits;
  w = 0.f;
  h = 0.f;
  tipLength = 0.f;
  spacing = 0;
  padding = vec2(0.0f);
  borderWidth = 0.5f;
}

void MissingCharacterRenderer::Metrics::setToSmallestPossible(int nDigits, int nColumns)
{
  this->nColumns = nColumns;
  this->nDigits = nDigits;
  w = 0.f;
  h = 0.f;
  tipLength = 0.f;
  spacing = 0;
  padding = vec2(0.5f);
  borderWidth = 1.f;
}

void MissingCharacterRenderer::Metrics::setToSmallestWithDotInTheCenter(int nDigits, int nColumns)
{
  this->nColumns = nColumns;
  this->nDigits = nDigits;
  w = 0.5f;
  h = 0.5f;
  tipLength = 0.f * w;
  spacing = -1;
  padding = vec2(1.f);
  borderWidth = 1.f;
}

void MissingCharacterRenderer::Metrics::setToSmallestWithMultipleDots(int nDigits, int nColumns)
{
  this->nColumns = nColumns;
  this->nDigits = nDigits;
  w = 0.75f;
  h = 0.75f;
  tipLength = 0.f * w;
  spacing = w;
  padding = vec2(1.f);
  borderWidth = 1.f;
}

void MissingCharacterRenderer::Metrics::setToSmallestWithSymbolsRecognizableAsDigits(int nDigits, int nColumns)
{
  this->nColumns = nColumns;
  this->nDigits = nDigits;
  w = 1.f;
  h = 1.f;
  tipLength = 0.f * w;
  spacing = w;
  padding = vec2(1.f);
  borderWidth = 1.f;
}

void MissingCharacterRenderer::Metrics::setToSmallestWithAlmostReadableSymbols(int nDigits, int nColumns)
{
  this->nColumns = nColumns;
  this->nDigits = nDigits;
  w = 1.f;
  h = 1.5f;
  tipLength = 0.f * w;
  spacing = w;
  padding = vec2(1.f);
  borderWidth = 1.f;
}

void MissingCharacterRenderer::Metrics::setToSmallestWithReadableSymbols(int nDigits, int nColumns)
{
  this->nColumns = nColumns;
  this->nDigits = nDigits;
  w = 1.f;
  h = 2.f;
  tipLength = 0.125f * w;
  spacing = w;
  padding = vec2(1.f);
  borderWidth = 1.f;
}

void MissingCharacterRenderer::Metrics::setToSmallestWithFullShape(int nDigits, int nColumns)
{
  this->nColumns = nColumns;
  this->nDigits = nDigits;
  w = 1.f;
  h = 4.f;
  tipLength = 0.5f * w;
  spacing = 2*w;
  padding = vec2(spacing);
  borderWidth = 1.f;
}

MissingCharacterRenderer::Metrics MissingCharacterRenderer::Metrics::onePixel(int nDigits, int nColumns)
{
  Metrics m;
  m.setToOnePixel(nDigits, nColumns);
  return m;
}

MissingCharacterRenderer::Metrics MissingCharacterRenderer::Metrics::smallestPossible(int nDigits, int nColumns)
{
  Metrics m;
  m.setToSmallestPossible(nDigits, nColumns);
  return m;
}

MissingCharacterRenderer::Metrics MissingCharacterRenderer::Metrics::smallestWithDotInTheCenter(int nDigits, int nColumns)
{
  Metrics m;
  m.setToSmallestWithDotInTheCenter(nDigits, nColumns);
  return m;
}

MissingCharacterRenderer::Metrics MissingCharacterRenderer::Metrics::smallestWithMultipleDots(int nDigits, int nColumns)
{
  Metrics m;
  m.setToSmallestWithMultipleDots(nDigits, nColumns);
  return m;
}

MissingCharacterRenderer::Metrics MissingCharacterRenderer::Metrics::smallestWithSymbolsRecognizableAsDigits(int nDigits, int nColumns)
{
  Metrics m;
  m.setToSmallestWithSymbolsRecognizableAsDigits(nDigits, nColumns);
  return m;
}

MissingCharacterRenderer::Metrics MissingCharacterRenderer::Metrics::smallestWithAlmostReadableSymbols(int nDigits, int nColumns)
{
  Metrics m;
  m.setToSmallestWithAlmostReadableSymbols(nDigits, nColumns);
  return m;
}

MissingCharacterRenderer::Metrics MissingCharacterRenderer::Metrics::smallestWithReadableSymbols(int nDigits, int nColumns)
{
  Metrics m;
  m.setToSmallestWithReadableSymbols(nDigits, nColumns);
  return m;
}

MissingCharacterRenderer::Metrics MissingCharacterRenderer::Metrics::smallestWithFullShape(int nDigits, int nColumns)
{
  Metrics m;
  m.setToSmallestWithFullShape(nDigits, nColumns);
  return m;
}

bool MissingCharacterRenderer::Metrics::setToPreset(int i, int nDigits, int nColumns)
{
  switch(i)
  {
  case 0:
    setToOnePixel(nDigits, nColumns);
    return true;
  case 1:
    setToSmallestPossible(nDigits, nColumns);
    return true;
  case 2:
    setToSmallestWithDotInTheCenter(nDigits, nColumns);
    return true;
  case 3:
    setToSmallestWithMultipleDots(nDigits, nColumns);
    return true;
  case 4:
    setToSmallestWithSymbolsRecognizableAsDigits(nDigits, nColumns);
    return true;
  case 5:
    setToSmallestWithAlmostReadableSymbols(nDigits, nColumns);
    return true;
  case 6:
    setToSmallestWithReadableSymbols(nDigits, nColumns);
    return true;
  case 7:
    setToSmallestWithFullShape(nDigits, nColumns);
    return true;
  default:
    return false;
  }
}

void MissingCharacterRenderer::Metrics::initForDimension(int dimension, real size, int nDigits, int nColumns)
{
  if(size <= 1.f)
  {
    setToOnePixel(nDigits, nColumns);
    return;
  }

  Metrics smaller;
  smaller.setToOnePixel();
  real smallerSize = smaller.frameSize()[dimension];

  for(int i=1; i<1000; ++i)
  {
    Metrics larger;
    if(!larger.setToPreset(i, nDigits, nColumns))
      break;
    real largerSize = larger.frameSize()[dimension];

    if(largerSize == size)
    {
      *this = larger;
      return;
    }
    if(largerSize > size)
    {
      real weight = interpolationWeightOf(size, smallerSize, largerSize);

      *this = interpolate(weight, smaller, larger);
      return;
    }

    smallerSize = largerSize;
    smaller = larger;
  }

  // smaller is now set to setToSmallestWithFullShape, we just need to scale it
  *this = smaller * (size / smallerSize);
}

void MissingCharacterRenderer::Metrics::initEnforcingSize(const vec2& size, int nDigits)
{
  Metrics m[4];
  vec2 frameSize[4];
  real error[4] = {INFINITY, INFINITY, INFINITY, INFINITY};
  int nColumns[4] = {2, 2, 3, 3};
  int nExperiments = nDigits>4 ? 4 : 2;

  for(int i=0; i<nExperiments; ++i)
  {
    int d = i%2;
    int otherDimension = 1-d;

    m[i].initForDimension(d, size[d], nDigits, nColumns[i]);
    frameSize[i] = m[i].frameSize();

    error[i] = size[otherDimension] - frameSize[i][otherDimension];

    if(error[i] < 0.f)
      error[i] = INFINITY;
  }

  int bestIndex = 0;
  real bestError = INFINITY;

  for(int i=0; i<nExperiments; ++i)
  {
    if(error[i] < bestError)
    {
      bestIndex = i;
      bestError = error[i];
    }
  }

  *this = m[bestIndex];
  if(bestError!=INFINITY)
    this->padding[1-(bestIndex%2)] += bestError*0.5f;
}

MissingCharacterRenderer::Metrics& MissingCharacterRenderer::Metrics::operator*=(real x)
{
  w *= x;
  h *= x;
  tipLength *= x;
  spacing *= x;
  borderWidth *= x;
  padding *= x;

  return *this;
}

MissingCharacterRenderer::Metrics MissingCharacterRenderer::Metrics::operator*(real x) const
{
  Metrics m = *this;
  return m *= x;
}

MissingCharacterRenderer::Metrics& MissingCharacterRenderer::Metrics::operator+=(const Metrics& other)
{
  w += other.w;
  h += other.h;
  tipLength += other.tipLength;
  spacing += other.spacing;
  borderWidth += other.borderWidth;
  padding += other.padding;

  return *this;
}

MissingCharacterRenderer::Metrics MissingCharacterRenderer::Metrics::operator+(const Metrics& other) const
{
  Metrics m = *this;
  return m += other;
}

vec2 MissingCharacterRenderer::Metrics::digitSize() const
{
  return vec2(  h+w,
              2*h+w);
}

vec2 MissingCharacterRenderer::Metrics::frameSize() const
{
  const vec2 digitSize = this->digitSize();

  const int nRows = nDigits/nColumns;

  const vec2 contentSize = vec2(nColumns * digitSize.x + (nColumns-1)*spacing,
                                nRows * digitSize.y + (nRows-1)*spacing);
  const vec2 frameSize = contentSize + padding*2.f + vec2(borderWidth*2);

  return frameSize;
}


// ====


MissingCharacterRenderer::NumberDependentMetrics::NumberDependentMetrics()
{
  metrics6.nColumns = 3;
  metrics6.nDigits = 6;
}


const MissingCharacterRenderer::Metrics& MissingCharacterRenderer::NumberDependentMetrics::metrics(uint number) const
{
  if(number > 0xffff)
    return metrics6;
  else
    return metrics4;
}

void MissingCharacterRenderer::NumberDependentMetrics::initForFontHeight(real height, int nColumnsFor6Digits)
{
  metrics4.initForDimension(1, height, 4, 2);
  metrics6.initForDimension(1, height, 6, nColumnsFor6Digits);
}

void MissingCharacterRenderer::NumberDependentMetrics::initForMonoFont(const vec2& size)
{
  metrics4.initEnforcingSize(size, 4);
  metrics6.initEnforcingSize(size, 6);
}


// ====


MissingCharacterRenderer::MissingCharacterRenderer()
  : matrix(1)
{
  drawTriangle = _fallbackTriangleDrawer;
}

void MissingCharacterRenderer::drawMissingCharacterWithFrame(uint character) const
{
  const Metrics& metrics = numberDependentMetrics.metrics(character);

  drawFrame(metrics.frameSize(), metrics.borderWidth);
  drawNumber(character, metrics, vec2(metrics.borderWidth + metrics.padding));
}

void MissingCharacterRenderer::drawNumber(uint number, const Metrics& metrics, const vec2& offset) const
{
  int digits[6];

  const int nDigits = metrics.nDigits;

  for(int i=0; i<nDigits; ++i)
  {
    int offset = (nDigits-1-i)*4;

    const int digit = (number & (0xf << offset)) >> offset;
    digits[i] = digit;
  }

  drawNumber(digits, metrics, offset);
}

void MissingCharacterRenderer::drawNumber(const int* digits, const Metrics& metrics, const vec2& offset) const
{
  vec2 p(0);
  const vec2 s = metrics.digitSize();
  const real spacing = metrics.spacing;
  const int nDigits = metrics.nDigits;
  const int nColumns = metrics.nColumns;

  const real xStep = s.x+spacing;
  const real yStep = s.y+spacing;

  for(int i=0; i<nDigits; ++i)
  {
    p.x = xStep * (i%nColumns);
    p.y = yStep * (i/nColumns);

    drawDigit(digits[i], metrics, p+offset);
  }
}

void MissingCharacterRenderer::drawDigit(int digit, const Metrics& metrics, const vec2& offset) const
{
  switch(digit)
  {
  case 0:
    drawSegments(MASK_0, metrics, offset);
    break;
  case 1:
    drawSegments(MASK_1, metrics, offset);
    break;
  case 2:
    drawSegments(MASK_2, metrics, offset);
    break;
  case 3:
    drawSegments(MASK_3, metrics, offset);
    break;
  case 4:
    drawSegments(MASK_4, metrics, offset);
    break;
  case 5:
    drawSegments(MASK_5, metrics, offset);
    break;
  case 6:
    drawSegments(MASK_6, metrics, offset);
    break;
  case 7:
    drawSegments(MASK_7, metrics, offset);
    break;
  case 8:
    drawSegments(MASK_8, metrics, offset);
    break;
  case 9:
    drawSegments(MASK_9, metrics, offset);
    break;
  case 10:
    drawSegments(MASK_A, metrics, offset);
    break;
  case 11:
    drawSegments(MASK_B, metrics, offset);
    break;
  case 12:
    drawSegments(MASK_C, metrics, offset);
    break;
  case 13:
    drawSegments(MASK_D, metrics, offset);
    break;
  case 14:
    drawSegments(MASK_E, metrics, offset);
    break;
  case 15:
    drawSegments(MASK_F, metrics, offset);
    break;
  }
}

void MissingCharacterRenderer::drawSegments(Mask mask, const Metrics& metrics, const vec2& offset) const
{
  for(int i=0; i<7; ++i)
  {
    if(mask & (1<<i))
      drawSegment(i, metrics, offset);
  }
}

void MissingCharacterRenderer::drawSegment(int i, const Metrics& metrics, const vec2& offset) const
{
  const real w = metrics.w;
  const real h = metrics.h;
  vec2 position = vec2(w*0.5f) + offset;
  bool horizontal = false;

  switch(i)
  {
  case 0:
    position += vec2(0, h);
    horizontal = true;
    break;
  case 1:
    position += vec2(h, 0);
    break;
  case 2:
    horizontal = true;
    break;
  case 3:
    break;
  case 4:
    position += vec2(0, h);
    break;
  case 5:
    position += vec2(0, 2*h);
    horizontal = true;
    break;
  case 6:
    position += vec2(h, h);
    break;
  }

  drawSegment(position, metrics, horizontal);
}

void MissingCharacterRenderer::drawFrame(const vec2& frameSize, real borderWidth) const
{
  Rectangle<vec2> horizontal(vec2(0), vec2(frameSize.x, borderWidth));
  Rectangle<vec2> vertical(vec2(0, borderWidth), vec2(borderWidth, frameSize.y - borderWidth));

  drawRectangle(horizontal);
  drawRectangle(horizontal + vec2(0, frameSize.y-borderWidth));
  drawRectangle(vertical);
  drawRectangle(vertical + vec2(frameSize.x-borderWidth, 0));
}

void MissingCharacterRenderer::drawSegment(const vec2& position, const Metrics& metrics, bool horizontal) const
{
  mat3 m = this->matrix * Geometry::translate3(position);

  if(horizontal)
    m *= Geometry::rotateDiscrete3(3);

  drawSegment(m, metrics);
}

void MissingCharacterRenderer::drawSegment(const mat3& matrix, const Metrics& metrics) const
{

  /*    c
   *   / \
   *  /   \
   * a     b
   * |     |
   * |     |
   * |     |
   * |     |
   * |     |
   * d     f
   *  \   /
   *   \ /
   *    e
   */

  auto applyMatrix = [&matrix](real x, real y) {
    return (matrix * vec3(x, y, 1.f)).xy();
  };

  const real w = metrics.w;
  const real h = metrics.h;
  const real tipLength = metrics.tipLength;
  const real half_w = w*0.5f;

  const vec2 a = applyMatrix(-half_w, tipLength);
  const vec2 b = applyMatrix(half_w, tipLength);
  const vec2 c = applyMatrix(0.f, 0.f);
  const vec2 d = applyMatrix(-half_w, h-tipLength);
  const vec2 e = applyMatrix(0.f, h);
  const vec2 f = applyMatrix(half_w, h-tipLength);

  if(tipLength != 0.f)
  {
    drawTriangle(a, b, c);
    drawTriangle(d, e, f);
  }
  drawTriangle(a, d, f);
  drawTriangle(a, f, b);
}

void MissingCharacterRenderer::drawRectangle(const Rectangle<vec2>& rect) const
{
  /*
   * b----a
   * |    |
   * |    |
   * c----d
   */

  auto applyMatrix = [this](real x, real y) {
    return (matrix * vec3(x, y, 1.f)).xy();
  };

  const vec2 a = applyMatrix(rect.max().x, rect.min().y);
  const vec2 b = applyMatrix(rect.min().x, rect.min().y);
  const vec2 c = applyMatrix(rect.min().x, rect.max().y);
  const vec2 d = applyMatrix(rect.max().x, rect.max().y);

  drawTriangle(a, b, c);
  drawTriangle(a, c, d);
}

void MissingCharacterRenderer::_fallbackTriangleDrawer(const vec2& a, const vec2& b, const vec2& c)
{
  (void)a;
  (void)b;
  (void)c;
}


} // namespace Framework
