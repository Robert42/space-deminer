#ifndef _FRAMEWORK_DEBUG_CAMERA_CONTROLLER_H_
#define _FRAMEWORK_DEBUG_CAMERA_CONTROLLER_H_

#include <base/geometry/coordinate.h>
#include <base/signals.h>

#include <framework/window/user-input/keyboard.h>
#include <framework/window/user-input/mouse.h>

namespace Framework {


class DebugCameraController final
{
private:
  bool active;
  Keyboard::KeyState keyMoveForward, keyMoveBackward;
  Keyboard::KeyState keyStraveLeft, keyStraveRight;
  Keyboard::KeyState keyStraveUp, keyStraveDown;
  Keyboard::KeyState keyIncreaseSpeed, keyDecreaseSpeed;

public:
  Signals::Trackable trackable;

  Coordinate coordinate;
  real adjustableSpeedFactor;

public:
  DebugCameraController(InputDevice::ActionPriority actionPriority,
                        bool active = true);

public:
  void onWait(real time);

  bool handleMouseMovement();
  bool handleMouseWheelMovement();

  bool isActive() const;
  void setActive(bool active);
};


}

#endif
