#ifndef FRAMEWORK_MISSINGCHARACTERRENDERER_H
#define FRAMEWORK_MISSINGCHARACTERRENDERER_H

#include <base/geometry/rectangle.h>

namespace Framework {

using namespace Base;

class MissingCharacterRenderer final
{
public:
  /*
   *   -2-
   *  |   |
   *  3   1
   *  |   |
   *   -0-
   *  |   |
   *  4   6
   *  |   |
   *   -5-
   *
   */
  enum Mask
  {
    MASK_0 = 0x7e,
    MASK_1 = 0x42,
    MASK_2 = 0x37,
    MASK_3 = 0x67,
    MASK_4 = 0x4b,
    MASK_5 = 0x6d,
    MASK_6 = 0x7d,
    MASK_7 = 0x46,
    MASK_8 = 0x7f,
    MASK_9 = 0x6f,
    MASK_A = 0x5f,
    MASK_B = 0x79,
    MASK_C = 0x31,
    MASK_D = 0x73,
    MASK_E = 0x3d,
    MASK_F = 0x1d
  };

public:
  mat3 matrix;

public:
  class Metrics final
  {
  public:
    real w, h, tipLength, spacing, borderWidth;
    vec2 padding;

    int nColumns;
    int nDigits;

    Metrics();

    void setToOnePixel(int nDigits=4, int nColumns=2);
    void setToSmallestPossible(int nDigits=4, int nColumns=2);
    void setToSmallestWithDotInTheCenter(int nDigits=4, int nColumns=2);
    void setToSmallestWithMultipleDots(int nDigits=4, int nColumns=2);
    void setToSmallestWithSymbolsRecognizableAsDigits(int nDigits=4, int nColumns=2);
    void setToSmallestWithAlmostReadableSymbols(int nDigits=4, int nColumns=2);
    void setToSmallestWithReadableSymbols(int nDigits=4, int nColumns=2);
    void setToSmallestWithFullShape(int nDigits=4, int nColumns=2);

    static Metrics onePixel(int nDigits=4, int nColumns=2);
    static Metrics smallestPossible(int nDigits=4, int nColumns=2);
    static Metrics smallestWithDotInTheCenter(int nDigits=4, int nColumns=2);
    static Metrics smallestWithMultipleDots(int nDigits=4, int nColumns=2);
    static Metrics smallestWithSymbolsRecognizableAsDigits(int nDigits=4, int nColumns=2);
    static Metrics smallestWithAlmostReadableSymbols(int nDigits=4, int nColumns=2);
    static Metrics smallestWithReadableSymbols(int nDigits=4, int nColumns=2);
    static Metrics smallestWithFullShape(int nDigits=4, int nColumns=2);

    bool setToPreset(int i, int nDigits, int nColumns);

    void initForDimension(int dimension, real size, int nDigits, int nColumns);
    void initEnforcingSize(const vec2& size, int nDigits);

    Metrics& operator*=(real x);
    Metrics operator*(real x) const;
    Metrics& operator+=(const Metrics& other);
    Metrics operator+(const Metrics& other) const;

    vec2 digitSize() const;
    vec2 frameSize() const;
  };

  class NumberDependentMetrics final
  {
  private:
    Metrics metrics4, metrics6;

  public:
    NumberDependentMetrics();

    const Metrics& metrics(uint number) const;

    void initForFontHeight(real height, int nColumnsFor6Digits=3);
    void initForMonoFont(const vec2& size);

  }numberDependentMetrics;

  std::function<void(const vec2& a, const vec2& b, const vec2& c)> drawTriangle;

public:
  MissingCharacterRenderer();

  void drawMissingCharacterWithFrame(uint character) const;
  void drawNumber(uint number, const Metrics& metrics, const vec2& offset=vec2(0)) const;
  void drawNumber(const int* digits, const Metrics& metrics, const vec2& offset=vec2(0)) const;
  void drawDigit(int digit, const Metrics& metrics, const vec2& offset=vec2(0)) const;
  void drawSegments(Mask mask, const Metrics& metrics, const vec2& offset=vec2(0)) const;
  void drawSegment(int i, const Metrics& metrics, const vec2& offset=vec2(0)) const;

private:
  void drawFrame(const vec2& frameSize, real borderWidth) const;

  void drawSegment(const vec2& position, const Metrics& metrics, bool horizontal=false) const;
  void drawSegment(const mat3& matrix, const Metrics& metrics) const;

  void drawRectangle(const Rectangle<vec2>& rect) const;

private:
  static void _fallbackTriangleDrawer(const vec2& a, const vec2& b, const vec2& c);
};

} // namespace Framework

#endif // FRAMEWORK_MISSINGCHARACTERRENDERER_H
