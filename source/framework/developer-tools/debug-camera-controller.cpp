#include <framework/developer-tools/debug-camera-controller.h>
#include <framework/window/user-input.h>
#include <framework/frame-timer.h>
#include <framework/frame-signals.h>

namespace Framework {

const KeyCode KEY_CAMERA_FORWARD = KeyCode::W;
const KeyCode KEY_CAMERA_BACKWARD = KeyCode::S;
const KeyCode KEY_CAMERA_STRAVE_LEFT = KeyCode::A;
const KeyCode KEY_CAMERA_STRAVE_RIGHT = KeyCode::D;
const KeyCode KEY_CAMERA_STRAVE_UP = KeyCode::E;
const KeyCode KEY_CAMERA_STRAVE_DOWN = KeyCode::Q;

const KeyCode KEY_CAMERA_INCREASE_SPEED = KeyCode::SHIFT_LEFT;
const KeyCode KEY_CAMERA_DECREASE_SPEED = KeyCode::ALT_LEFT;


DebugCameraController::DebugCameraController(InputDevice::ActionPriority actionPriority, bool active)
  : active(active),
    keyMoveForward(KEY_CAMERA_FORWARD,
                   actionPriority,
                   active),
    keyMoveBackward(KEY_CAMERA_BACKWARD,
                    actionPriority,
                    active),
    keyStraveLeft(KEY_CAMERA_STRAVE_LEFT,
                  actionPriority,
                  active),
    keyStraveRight(KEY_CAMERA_STRAVE_RIGHT,
                   actionPriority,
                   active),
    keyStraveUp(KEY_CAMERA_STRAVE_UP,
                actionPriority,
                active),
    keyStraveDown(KEY_CAMERA_STRAVE_DOWN,
                  actionPriority,
                  active),
    keyIncreaseSpeed(KEY_CAMERA_INCREASE_SPEED,
                     actionPriority,
                     active),
    keyDecreaseSpeed(KEY_CAMERA_DECREASE_SPEED,
                     actionPriority,
                     active)
{
  FrameSignals::signalRenderingQueued().connect(std::bind(&DebugCameraController::onWait, this, _1)).track(this->trackable);

  Mouse::signalMouseMoved().connect(InputDevice::ActionPriority::INGAME_DEBUG,
                                    std::bind(&DebugCameraController::handleMouseMovement, this)).track(this->trackable);
  Mouse::signalMouseWheelMoved().connect(InputDevice::ActionPriority::INGAME_DEBUG,
                                         std::bind(&DebugCameraController::handleMouseWheelMovement, this)).track(this->trackable);

  adjustableSpeedFactor = 1.0f;
}

void DebugCameraController::onWait(real time)
{
  if(!active)
    return;

  vec3 movementDirection(0.f);

  if(keyMoveForward.isPressed())
    movementDirection.y += 1.0f;
  if(keyMoveBackward.isPressed())
    movementDirection.y -= 1.0f;

  if(keyStraveRight.isPressed())
    movementDirection.x += 1.0f;
  if(keyStraveLeft.isPressed())
    movementDirection.x -= 1.0f;

  if(keyStraveUp.isPressed())
    movementDirection.z += 1.0f;
  if(keyStraveDown.isPressed())
    movementDirection.z -= 1.0f;

  if(keyDecreaseSpeed.isPressed())
    movementDirection *= 0.1f;
  if(keyIncreaseSpeed.isPressed())
    movementDirection *= 5.0f;
  movementDirection *= adjustableSpeedFactor;

  coordinate.moveLocal(movementDirection * time);
}

bool DebugCameraController::handleMouseMovement()
{
  if(!active)
    return false;

  const vec2 mouseMovement = Mouse::currentSpeed();
  const real rotationSpeed = 90.0f;
  const real time = FrameTimer::averageTime();

  coordinate.rotateZ(radians(-time*rotationSpeed*mouseMovement.x));
  coordinate.rotateLocalX(radians(-time*rotationSpeed*mouseMovement.y));

  return true;
}

bool DebugCameraController::handleMouseWheelMovement()
{
  if(!active)
    return false;

  adjustableSpeedFactor = clamp<real>(adjustableSpeedFactor * (1+0.2f*Mouse::wheelMovement()),
                                      0.01,
                                      200.f);

  return true;
}

bool DebugCameraController::isActive() const
{
  return active;
}

void DebugCameraController::setActive(bool active)
{
  keyMoveForward.blockLowerPriorities = active;
  keyMoveBackward.blockLowerPriorities = active;
  keyStraveLeft.blockLowerPriorities = active;
  keyStraveRight.blockLowerPriorities = active;
  keyStraveUp.blockLowerPriorities = active;
  keyStraveDown.blockLowerPriorities = active;
  keyDecreaseSpeed.blockLowerPriorities = active;
  keyIncreaseSpeed.blockLowerPriorities = active;
}


}
