#include "test-spatial-gui.h"

#include <framework/window/user-input/mouse.h>
#include <framework/window/render-window.h>
#include <framework/developer-tools/gui-system/widgets/dummy-widget.h>
#include <framework/developer-tools/gui-system/containers/dummy-container.h>
#include <framework/developer-tools/gui-system/containers/button.h>
#include <framework/developer-tools/gui-system/layouts/z-offset.h>
#include <framework/developer-tools/gui-system/layouts/border.h>
#include <framework/developer-tools/gui-system/layouts/box.h>
#include <framework/developer-tools/gui-system/layouts/button-box.h>
#include <framework/developer-tools/gui-system/containers/window-frame.h>

namespace Framework {


TestSpatialGui::TestSpatialGui()
{
  testWindow = Gui::Window::Ptr(new Gui::Window);

  windowManager.addMainWindow(testWindow);

  cameraCoordinate = windowManager.camera.coordinate;

  updateCameraCoordinate();

  Mouse::signalMouseMoved().connect(InputDevice::ActionPriority::GUI_SPECIAL_EFFECT, std::bind(&TestSpatialGui::updateCameraCoordinate, this)).track(this->trackable);

  testWindow->setRootWidget(createTestGui());
}


TestSpatialGui::~TestSpatialGui()
{
}


bool TestSpatialGui::updateCameraCoordinate()
{
  vec2 mouse = Mouse::absolutePosition();
  mouse /= vec2(RenderWindow::size());
  mouse *= 2.f;
  mouse -= 1.f;

  mouse *= radians(30.f);

  Coordinate c;

  c.rotateLocalY(mouse.x);
  c.rotateLocalX(mouse.y);

  windowManager.camera.coordinate = c * cameraCoordinate;
  windowManager.sheduleRedraw();

  return false;
}

Gui::Widget::Ptr TestSpatialGui::createTestGui()
{
  Gui::Containers::WindowFrame::Ptr rootWidget = Gui::Containers::WindowFrame::create();
  Gui::Layouts::ZOffset::Ptr zOffsetLayout = Gui::Layouts::ZOffset::create(10.f);
  Gui::Layouts::Border::Ptr mainBorder = Gui::Layouts::Border::create();
  Gui::Layouts::Box::Ptr vBoxLayout = Gui::Layouts::Box::create(Gui::Layouts::Box::Direction::VERTICAL);
  Gui::Layouts::Box::Ptr hBoxLayout = Gui::Layouts::Box::create(Gui::Layouts::Box::Direction::HORIZONTAL);

  Gui::Layouts::ButtonBox::Ptr vButtonBoxLayout = Gui::Layouts::ButtonBox::create(Gui::Layouts::Box::Direction::VERTICAL);
  Gui::Layouts::ButtonBox::Ptr hButtonBoxLayout = Gui::Layouts::ButtonBox::create(Gui::Layouts::Box::Direction::HORIZONTAL);

  Gui::Containers::Button::Ptr btnHelp = Gui::Containers::Button::create(translate("Help"));
  Gui::Containers::Button::Ptr btnOk = Gui::Containers::Button::create(translate("Ok"));
  Gui::Containers::Button::Ptr btnCancel = Gui::Containers::Button::create(translate("Cancel"));

  btnOk->setAsDefaultButton();
  btnCancel->setAsDefaultEscapeButton();

  Gui::Widgets::DummyWidget::Ptr dw;

  hBoxLayout->appendWidget(dw = Gui::Widgets::DummyWidget::create(vec4(0.5, 0, 0, 1)));
  hBoxLayout->appendSubLayout(vButtonBoxLayout, 1.f);
  hBoxLayout->appendWidget(Gui::Widgets::DummyWidget::create(vec4(0, 0, 0.5, 1)));

  dw->setOptimalSize(vec2(75, dw->optimalSize().y));

  vButtonBoxLayout->appendWidget(dw = Gui::Widgets::DummyWidget::create(vec4(1, 0, 0, 1)));
  vButtonBoxLayout->appendWidget(Gui::Widgets::DummyWidget::create(vec4(0, 1, 0, 1)));
  vButtonBoxLayout->appendWidget(Gui::Widgets::DummyWidget::create(vec4(0, 0, 1, 1)));
  vButtonBoxLayout->setPrependedSpaceStretching(1);
  vButtonBoxLayout->setInterSpaceStretching(1);
  vButtonBoxLayout->setAppendedSpaceStretching(1);

  dw->setOptimalSize(vec2(75, dw->optimalSize().y));

  vBoxLayout->appendSubLayout(hBoxLayout, 1.f);
  vBoxLayout->appendSubLayout(hButtonBoxLayout);
  hButtonBoxLayout->appendWidget(btnHelp);
  hButtonBoxLayout->appendSpace(vec2(0), 1.f);
  hButtonBoxLayout->appendWidget(btnOk);
  hButtonBoxLayout->appendWidget(btnCancel);

  mainBorder->addLayout(vBoxLayout);
  zOffsetLayout->addLayout(mainBorder);

  rootWidget->addLayout(zOffsetLayout);
  rootWidget->markChildZPositionDirty();

  return rootWidget;
}


} // namespace Framework
