#ifndef FRAMEWORK_EDITOR_BASE_EDITOR2D_VIEW_H
#define FRAMEWORK_EDITOR_BASE_EDITOR2D_VIEW_H

#include "model.h"
#include <base/output.h>

namespace Framework {
namespace Editor {
namespace Base {
namespace Editor2D {

class View
{
public:
  typedef std::shared_ptr<View> Ptr;

public:
  Signals::Trackable trackable;

private:
  bool dirty;
  int blockedSignals, blockedUpdate, blockedConstraints;

  vec2 _position;
  vec2 _viewportSize;
  real _zoomFactor;
  real _modelUnitsPerPixel;
  bool _invertY;
  mat3x2 _screenToModelMatrix, _modelToScreenMatrix;

  Signals::CallableSignal<void()> _signalViewChanged;
  Signals::CallableSignal<vec2(const vec2&), Signals::ResultHandler::PassResultAsArgumentResultHandler<vec2(const vec2&)>> _signalConstrainPosition;
  Signals::CallableSignal<void(const vec2&)> _signalViewportSizeChanged;

public:
  const Model::Ptr model;

public:
  View(const Model::Ptr& model);
  virtual ~View();

  static Ptr create(const Model::Ptr& model);

  void setPosition(const vec2& _position);
  const vec2& position() const;

  void moveToCenter();
  void moveInViewSpace(const ivec2& offset);

  void setViewportSize(const vec2& _viewportSize);
  const vec2& viewportSize() const;

  void setZoomFactor(real _zoomFactor);
  real zoomFactor() const;
  void zoom(float direction);
  void zoomWithFixedScreenPosition(float direction, const vec2& fixedScreenPosition);

  void setPixelsPerModelUnit(float pixelsPerModelUnit);
  void setModelUnitsPerPixel(float _modelUnitsPerPixel);
  float pixelsPerModelUnit() const;
  float modelUnitsPerPixel() const;

  Rectangle<vec2> viewportInModelSpace() const;

  void fitToModel(bool crop=false, bool dontEnlarge=true);
  void resetView(float _zoomFactor=1.f);
  real calcBestfittingZoomForModel(bool crop=false)const;

  void setInvertY(bool _invertY);
  bool invertY() const;

  const mat3x2& screenToModelMatrix() const;
  const mat3x2& modelToScreenMatrix() const;

  vec2 transformFromScreenToModelSpace(const vec2& screenSpaceCoordinate) const;
  vec2 transformFromModelToScreenSpace(const vec2& modelSpaceCoordinate) const;
  vec2 transformFromViewToModelSpace(const vec2& viewSpaceCoordinate) const;
  vec2 transformFromModelToViewSpace(const vec2& modelSpaceCoordinate) const;
  vec2 transformFromViewToScreenSpace(const vec2& viewSpaceCoordinate) const;
  vec2 transformFromScreenToViewSpace(const vec2& screenSpaceCoordinate) const;

  vec2 transformDirectionFromScreenToModelSpace(const vec2& screenSpaceDirection) const;
  vec2 transformDirectionFromModelToScreenSpace(const vec2& modelSpaceDirection) const;
  vec2 transformDirectionFromViewToModelSpace(const vec2& viewSpaceDirection) const;
  vec2 transformDirectionFromModelToViewSpace(const vec2& modelSpaceDirection) const;
  vec2 transformDirectionFromViewToScreenSpace(const vec2& viewSpaceDirection) const;
  vec2 transformDirectionFromScreenToViewSpace(const vec2& screenSpaceDirection) const;

  Circle transformFromScreenToModelSpace(const Circle& screenSpaceCircle) const;
  Circle transformFromModelToScreenSpace(const Circle& modelSpaceCircle) const;
  Circle transformFromViewToModelSpace(const Circle& viewSpaceCircle) const;
  Circle transformFromModelToViewSpace(const Circle& modelSpaceCircle) const;

  Rectangle<vec2> transformFromScreenToModelSpace(const Rectangle<vec2>& screenSpaceRectangle) const;
  Rectangle<vec2> transformFromModelToScreenSpace(const Rectangle<vec2>& modelSpaceRectangle) const;
  Rectangle<vec2> transformFromViewToModelSpace(const Rectangle<vec2>& viewSpaceRectangle) const;
  Rectangle<vec2> transformFromModelToViewSpace(const Rectangle<vec2>& modelSpaceRectangle) const;

  void applyPositionConstraints();
  void addConstraint_ClampPositionToModelBoundingBox();

  Signals::Signal<void()>& signalViewChanged();
  Signals::Signal<vec2(const vec2&), Signals::ResultHandler::PassResultAsArgumentResultHandler<vec2(const vec2&)>>& signalConstrainPosition();
  Signals::Signal<void(const vec2&)>& signalViewportSizeChanged();

private:
  void markDirtyAndUpdate();
  void update();

  mat3x2 createScreenToModelMatrix() const;
  mat3x2 createModelToScreenMatrix() const;

  vec2 calcScaleFactor() const;

  vec2 constrainPositionToModelBoundingBox(const vec2& pos);
};

} // namespace Editor2D
} // namespace Base
} // namespace Editor
} // namespace Framework

#endif // FRAMEWORK_EDITOR_BASE_EDITOR2D_VIEW_H
