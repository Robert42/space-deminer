#include "view.h"

namespace Framework {
namespace Editor {
namespace Base {
namespace Editor2D {


class TestModel : public Model
{
public:
  typedef std::shared_ptr<TestModel> Ptr;

public:
  void setBoundingRectangle(const Rectangle<vec2>& rectangle)
  {
    _boundingRect.set(rectangle);
  }

  static Ptr create()
  {
    return Ptr(new TestModel);
  }
};


TEST(framework_editor_base_editor2d_View, coordinateSpaceConversion)
{
  TestModel::Ptr model = TestModel::create();

  model->setBoundingRectangle(Rectangle<vec2>(vec2(-10), vec2(10)));

  View view(model);

  view.setViewportSize(vec2(1000));

  EXPECT_EQ(vec2(500-10, 500+10), view.transformFromModelToScreenSpace(vec2(-10, 10)));
  EXPECT_EQ(vec2(500+10, 500-10), view.transformFromModelToScreenSpace(vec2(10, -10)));
  EXPECT_EQ(vec2(-10, 10), view.transformFromScreenToModelSpace(vec2(500-10, 500+10)));
  EXPECT_EQ(vec2(10, -10), view.transformFromScreenToModelSpace(vec2(500+10, 500-10)));

  view.setInvertY(true);

  EXPECT_EQ(vec2(500-10, 500-10), view.transformFromModelToScreenSpace(vec2(-10, 10)));
  EXPECT_EQ(vec2(500+10, 500+10), view.transformFromModelToScreenSpace(vec2(10, -10)));
  EXPECT_EQ(vec2(-10, 10), view.transformFromScreenToModelSpace(vec2(500-10, 500-10)));
  EXPECT_EQ(vec2(10, -10), view.transformFromScreenToModelSpace(vec2(500+10, 500+10)));

  view.fitToModel(false, false);

  EXPECT_EQ(vec2(0), view.transformFromModelToScreenSpace(vec2(-10, 10)));
  EXPECT_EQ(vec2(1000), view.transformFromModelToScreenSpace(vec2(10, -10)));
  EXPECT_EQ(vec2(-10, 10), view.transformFromScreenToModelSpace(vec2(0)));
  EXPECT_EQ(vec2(10, -10), view.transformFromScreenToModelSpace(vec2(1000)));
}


TEST(framework_editor_base_editor2d_View, fitToModel)
{
  TestModel::Ptr model = TestModel::create();

  model->setBoundingRectangle(Rectangle<vec2>(vec2(0), vec2(10, 5)));

  View view(model);

  view.setViewportSize(vec2(200, 90));
  view.fitToModel(true, false);
  EXPECT_EQ(20.f, view.zoomFactor());
  view.fitToModel(false, false);
  EXPECT_EQ(18.f, view.zoomFactor());

  view.setViewportSize(vec2(60, 50));
  view.fitToModel(true, false);
  EXPECT_EQ(10.f, view.zoomFactor());
  view.fitToModel(false, false);
  EXPECT_EQ(6.f, view.zoomFactor());

  view.setViewportSize(vec2(90, 200));
  view.fitToModel(true, false);
  EXPECT_EQ(40.f, view.zoomFactor());
  view.fitToModel(false, false);
  EXPECT_EQ(9.f, view.zoomFactor());

  view.setViewportSize(vec2(50, 60));
  view.fitToModel(true, false);
  EXPECT_EQ(12.f, view.zoomFactor());
  view.fitToModel(false, false);
  EXPECT_EQ(5.f, view.zoomFactor());

  model->setBoundingRectangle(Rectangle<vec2>(vec2(0), vec2(5, 10)));

  view.setViewportSize(vec2(200, 90));
  view.fitToModel(true, false);
  EXPECT_EQ(40.f, view.zoomFactor());
  view.fitToModel(false, false);
  EXPECT_EQ(9.f, view.zoomFactor());

  view.setViewportSize(vec2(60, 50));
  view.fitToModel(true, false);
  EXPECT_EQ(12.f, view.zoomFactor());
  view.fitToModel(false, false);
  EXPECT_EQ(5.f, view.zoomFactor());

  view.setViewportSize(vec2(90, 200));
  view.fitToModel(true, false);
  EXPECT_EQ(20.f, view.zoomFactor());
  view.fitToModel(false, false);
  EXPECT_EQ(18.f, view.zoomFactor());

  view.setViewportSize(vec2(50, 60));
  view.fitToModel(true, false);
  EXPECT_EQ(10.f, view.zoomFactor());
  view.fitToModel(false, false);
  EXPECT_EQ(6.f, view.zoomFactor());

  model->setBoundingRectangle(Rectangle<vec2>(vec2(0), vec2(1)));
  view.setViewportSize(vec2(10));
  view.fitToModel(true, false);
  EXPECT_TRUE(view.zoomFactor() > 1);
}


TEST(framework_editor_base_editor2d_View, pixelsPerModelUnit)
{
  TestModel::Ptr model = TestModel::create();

  model->setBoundingRectangle(Rectangle<vec2>(vec2(0), vec2(10)));

  View view(model);

  view.setViewportSize(vec2(500));
  view.setPixelsPerModelUnit(25);

  view.fitToModel(false, false);
  EXPECT_EQ(vec2(0), view.transformFromModelToScreenSpace(vec2(0)));
  EXPECT_EQ(vec2(500), view.transformFromModelToScreenSpace(vec2(10)));
  EXPECT_EQ(2.f, view.zoomFactor());
}


TEST(framework_editor_base_editor2d_View, moveInViewSpace)
{
  ivec2 fullHD(1920, 1080);
  vec2 modelSize(256);
  vec2 center = modelSize/2.f;

  TestModel::Ptr model = TestModel::create();

  model->setBoundingRectangle(Rectangle<vec2>(vec2(0), modelSize));

  View view(model);

  view.setViewportSize(fullHD);
  view.resetView();

  EXPECT_EQ(center, view.position());
  view.moveInViewSpace(vec2(1, 0));
  EXPECT_EQ(center+vec2(1, 0), view.position());
  view.moveInViewSpace(vec2(-1, 1));
  EXPECT_EQ(center+vec2(0, 1), view.position());
}


TEST(framework_editor_base_editor2d_View, zoomWithFixedScreenPosition)
{
  ivec2 fullHD(1920, 1080);
  vec2 modelSize(256);

  TestModel::Ptr model = TestModel::create();

  model->setBoundingRectangle(Rectangle<vec2>(vec2(0), modelSize));

  View view(model);

  view.setViewportSize(fullHD);
  view.resetView();

  vec2 fixedScreenPosition = view.transformFromModelToScreenSpace(vec2(0));

  view.zoomWithFixedScreenPosition(10, fixedScreenPosition);

  EXPECT_FLOAT_EQ(fixedScreenPosition.x, view.transformFromModelToScreenSpace(vec2(0)).x);
  EXPECT_FLOAT_EQ(fixedScreenPosition.y, view.transformFromModelToScreenSpace(vec2(0)).y);
}


} // namespace Editor2D
} // namespace Base
} // namespace Editor
} // namespace Framework
