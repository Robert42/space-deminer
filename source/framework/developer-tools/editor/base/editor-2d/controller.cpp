#include "controller.h"

namespace Framework {
namespace Editor {
namespace Base {
namespace Editor2D {


Controller::Action::Action(Controller& controller)
  : _finished(false),
    _controller(controller),
    constraint(MouseConstraint::WHATEVER)
{
}


Controller::Action::~Action()
{
  abort();
}


void Controller::Action::abort()
{
  _finish(false);
}


void Controller::Action::succeed()
{
  _finish(true);
}

void Controller::Action::_finish(bool succeeded)
{
  if(_finished)
    return;

  Action::Ptr lock = controller()._currentAction;

  _finished = true;

  if(succeeded)
    _signalSucceeded();
  else
    _signalAborted();
  _signalFinished();
  controller()._signalActionFinished(lock);

  if(controller()._currentAction.get() == this)
    controller()._resetCurrentAction();

  (void)lock;
}

bool Controller::Action::finished() const
{
  return _finished;
}

Controller& Controller::Action::controller()
{
  return _controller;
}

Signals::Signal<void()>& Controller::Action::signalSucceeded()
{
  return _signalSucceeded;
}

Signals::Signal<void()>& Controller::Action::signalAborted()
{
  return _signalAborted;
}

Signals::Signal<void()>& Controller::Action::signalFinished()
{
  return _signalFinished;
}

bool Controller::Action::handlePointerMovement()
{
  return false;
}

Controller::Controller(const View::Ptr& view)
  : _view(view),
    _currentMousePosition(0),
    _currentMouseMovement(0),
    _clampedMousePosition(0),
    _clampedMouseMovement(0),
    _focusMode(InputFocusMode::FOCUSED_IF_MOSUE_INSIDE_VIEWPORT)
{
  _view->signalViewportSizeChanged().connect([this](const vec2&){_signalViewportChanged(viewport());}).track(this->trackable);
}


Controller::~Controller()
{
  _resetCurrentAction();
}


Controller::Ptr Controller::create(const View::Ptr& view)
{
  return Ptr(new Controller(view));
}


std::string Controller::shortcutHelpText()
{
  return
    "Ctrl+Home - Fit View\n"
    "1 Zoom to 100%\n"
    "2 Zoom to 50%\n"
    "4 Zoom to 25%\n"
    "8 Zoom to 12.5%\n"
    "Ctrl+2 Zoom to 200%\n"
    "Ctrl+4 Zoom to 400%\n"
    "Ctrl+8 Zoom to 800%\n"
    "+ Zoom in\n"
    "- Zoom out\n"
    "\n"
    "Use the mouse wheel to zoom\n"
    "Press the middle mouse button to navigate through the image\n";
}


bool Controller::zoomToMouse(float direction)
{
  if(!hasInputFocus())
    return false;

  view()->zoomWithFixedScreenPosition(direction, currentMousePosition());

  return true;
}


bool Controller::handlePointerMovement(const vec2& newMousePosition, const vec2& relativeMovement)
{
  const vec2 oldMousePosition = newMousePosition - relativeMovement;

  initPointerPosition(newMousePosition);

  _currentMouseMovement = relativeMovement;
  _clampedMouseMovement = _clampedMousePosition - oldMousePosition;

  if(_currentAction!=nullptr)
    return _currentAction->handlePointerMovement();

  return false;
}


void Controller::initPointerPosition(const vec2 &pos)
{
  _currentMousePosition = pos;
  _clampedMousePosition = viewport().clamp(_currentMousePosition);
}


const vec2& Controller::currentMousePosition() const
{
  return _currentMousePosition;
}


const vec2& Controller::currentMouseMovement() const
{
  return _currentMouseMovement;
}


const vec2& Controller::clampedMousePosition() const
{
  return _clampedMousePosition;
}


const vec2& Controller::clampedMouseMovement() const
{
  return _clampedMouseMovement;
}


vec2 Controller::currentModelSpaceMousePosition() const
{
  return _view->transformFromScreenToModelSpace(currentMousePosition());
}


vec2 Controller::currentModelSpaceMouseMovement() const
{
  return _view->transformDirectionFromScreenToModelSpace(currentMouseMovement());
}


vec2 Controller::clampedModelSpaceMousePosition() const
{
  return _view->transformFromScreenToModelSpace(clampedModelSpaceMousePosition());
}


vec2 Controller::clampedModelSpaceMouseMovement() const
{
  return _view->transformDirectionFromScreenToModelSpace(clampedModelSpaceMouseMovement());
}


Rectangle<vec2> Controller::viewport() const
{
  return Rectangle<vec2>(vec2(0), _view->viewportSize());
}


Controller::InputFocusMode Controller::focusMode() const
{
  return _focusMode;
}


bool Controller::hasInputFocus() const
{
  switch(focusMode().value)
  {
  case InputFocusMode::INPUT_FOCUSED:
    return true;
  case InputFocusMode::NO_INPUT_FOCUS:
    return false;
  case InputFocusMode::FOCUSED_IF_MOSUE_INSIDE_VIEWPORT:
  default:
    return viewport().contains(currentMousePosition());
  }
}


void Controller::setFocusMode(InputFocusMode focusMode)
{
  _focusMode = focusMode;
}


const View::Ptr& Controller::view()
{
  return _view;
}


bool Controller::fitWholeModel()
{
  if(!hasInputFocus())
    return false;

  view()->fitToModel();

  return true;
}


bool Controller::setZoomFactor(real zoomFactor)
{
  if(!hasInputFocus())
    return false;

  view()->resetView(zoomFactor);

  return true;
}


Signals::Signal<void(const Controller::Action::Ptr&)>& Controller::signalActionActivated()
{
  return _signalActionActivated;
}


Signals::Signal<void(const Controller::Action::Ptr&)>& Controller::signalActionFinished()
{
  return _signalActionFinished;
}


Signals::Signal<void(const Rectangle<vec2>&)>& Controller::signalViewportChanged()
{
  return _signalViewportChanged;
}


Controller::Action::Ptr Controller::startMovingViewAction()
{
  class MovingViewAction : public Action
  {
  public:
    MovingViewAction(Controller& controller)
      : Action(controller)
    {
      this->constraint = MouseConstraint::WRAPAROUND_VIEWPORT;
    }

    bool handlePointerMovement() final override
    {
      if(finished())
        return false;

      controller().view()->moveInViewSpace(-controller().currentMouseMovement());
      return true;
    }
  };

  Controller::Action::Ptr action = Controller::Action::Ptr(new MovingViewAction(*this));

  _startAction(action);

  return action;
}


void Controller::_startAction(const Action::Ptr& action)
{
  if(_currentAction != action)
  {
    if(_currentAction)
      _currentAction->abort();

    assert(_currentAction == nullptr);

    _currentAction = action;

    if(!hasInputFocus())
      action->abort();
  }
}


void Controller::_resetCurrentAction()
{
  _currentAction.reset();
}


} // namespace Editor2D
} // namespace Base
} // namespace Editor
} // namespace Framework
