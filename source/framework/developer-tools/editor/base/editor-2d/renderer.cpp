#include "renderer.h"

namespace Framework {
namespace Editor {
namespace Base {
namespace Editor2D {


Renderer::Renderer(const View::Ptr& view)
{
  view->model->signalBoundingRectChanged().connect(_signalMarkedAsDirty).track(this->trackable);
  view->signalViewChanged().connect(_signalMarkedAsDirty).track(this->trackable);
}


Renderer::~Renderer()
{
}


void Renderer::markAsDirty()
{
  _signalMarkedAsDirty();
}


Signals::Signal<void()>& Renderer::signalMarkedAsDirty()
{
  return this->_signalMarkedAsDirty;
}


} // namespace Editor2D
} // namespace Base
} // namespace Editor
} // namespace Framework
