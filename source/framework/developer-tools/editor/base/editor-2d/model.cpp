#include "model.h"

namespace Framework {
namespace Editor {
namespace Base {
namespace Editor2D {

Model::Model(const BoundingRect& boundingRectangle)
  : _boundingRect(boundingRectangle)
{
}


const BoundingRect& Model::boundingRect() const
{
  return _boundingRect;
}


Signals::Signal<void()>& Model::signalBoundingRectChanged()
{
  return _boundingRect.signalChanged();
}


} // namespace Editor2D
} // namespace Base
} // namespace Editor
} // namespace Framework
