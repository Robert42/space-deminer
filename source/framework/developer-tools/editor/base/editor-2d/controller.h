#ifndef FRAMEWORK_EDITOR_BASE_EDITOR2D_CONTROLLER_H
#define FRAMEWORK_EDITOR_BASE_EDITOR2D_CONTROLLER_H

#include "view.h"

namespace Framework {
namespace Editor {
namespace Base {
namespace Editor2D {

class Controller
{
public:
  typedef std::shared_ptr<Controller> Ptr;

  BEGIN_ENUMERATION(InputFocusMode,)
    INPUT_FOCUSED,
    NO_INPUT_FOCUS,
    FOCUSED_IF_MOSUE_INSIDE_VIEWPORT
  ENUMERATION_BASIC_OPERATORS(InputFocusMode)
  END_ENUMERATION;

  class Action
  {
  public:
    friend class Controller;

    typedef std::shared_ptr<Action> Ptr;
    typedef std::weak_ptr<Action> WeakPtr;

    BEGIN_ENUMERATION(MouseConstraint,)
      WHATEVER,
      NONE,
      FULLSCREEN,
      VIEWPORT,
      WRAPAROUND_VIEWPORT
    ENUMERATION_BASIC_OPERATORS(MouseConstraint)
    END_ENUMERATION;

  private:
    bool _finished;
    Controller& _controller;
    Signals::CallableSignal<void()> _signalSucceeded;
    Signals::CallableSignal<void()> _signalAborted;
    Signals::CallableSignal<void()> _signalFinished;

  public:
    MouseConstraint constraint;

    Action(Controller& controller);
    virtual ~Action();

    bool finished() const;
    Controller& controller();

    void abort();
    void succeed();

    Signals::Signal<void()>& signalSucceeded();
    Signals::Signal<void()>& signalAborted();
    Signals::Signal<void()>& signalFinished();

    virtual bool handlePointerMovement();

  private:
    void _finish(bool succeeded);
  };

public:
  Signals::Trackable trackable;

private:
  const View::Ptr _view;

  Action::Ptr _currentAction;

private:
  vec2 _currentMousePosition;
  vec2 _currentMouseMovement;
  vec2 _clampedMousePosition;
  vec2 _clampedMouseMovement;
  InputFocusMode _focusMode;

  Signals::CallableSignal<void(const Action::Ptr&)> _signalActionActivated;
  Signals::CallableSignal<void(const Action::Ptr&)> _signalActionFinished;
  Signals::CallableSignal<void(const Rectangle<vec2>&)> _signalViewportChanged;

public:
  Controller(const View::Ptr& view);
  virtual ~Controller();

  static Ptr create(const View::Ptr& view);
  static std::string shortcutHelpText();

  InputFocusMode focusMode() const;
  bool hasInputFocus() const;
  void setFocusMode(InputFocusMode focusMode);

  const View::Ptr& view();

  bool fitWholeModel();
  bool setZoomFactor(real zoomFactor);
  bool zoomToMouse(float direction);

  virtual bool handlePointerMovement(const vec2& newMousePosition, const vec2& relativeMovement);
  virtual void initPointerPosition(const vec2& pos);

  Signals::Signal<void(const Action::Ptr&)>& signalActionActivated();
  Signals::Signal<void(const Action::Ptr&)>& signalActionFinished();
  Signals::Signal<void(const Rectangle<vec2>&)>& signalViewportChanged();

  Action::Ptr startMovingViewAction();

protected:
  void _startAction(const Action::Ptr& action);
  void _resetCurrentAction();

public:
  const vec2& currentMousePosition() const;
  const vec2& currentMouseMovement() const;
  const vec2& clampedMousePosition() const;
  const vec2& clampedMouseMovement() const;

  vec2 currentModelSpaceMousePosition() const;
  vec2 currentModelSpaceMouseMovement() const;
  vec2 clampedModelSpaceMousePosition() const;
  vec2 clampedModelSpaceMouseMovement() const;

  Rectangle<vec2> viewport() const;
};

} // namespace Editor2D
} // namespace Base
} // namespace Editor
} // namespace Framework

#endif // FRAMEWORK_EDITOR_BASE_EDITOR2D_CONTROLLER_H
