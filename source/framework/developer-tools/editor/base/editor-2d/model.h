#ifndef FRAMEWORK_EDITOR_BASE_EDITOR2D_MODEL_H
#define FRAMEWORK_EDITOR_BASE_EDITOR2D_MODEL_H

#include <base/geometry/bounding-rect.h>

namespace Framework {

using namespace Base;

namespace Editor {
namespace Base {
namespace Editor2D {

using ::Base::Geometry::BoundingRect;

class Model : public noncopyable
{
public:
  typedef std::shared_ptr<Model> Ptr;
  typedef std::weak_ptr<Model> WeakPtr;

protected:
  BoundingRect _boundingRect;

public:
  Model(const BoundingRect& boundingRectangle = BoundingRect());

public:
  const BoundingRect& boundingRect() const;

  Signals::Signal<void()>& signalBoundingRectChanged();
};

} // namespace Editor2D
} // namespace Base
} // namespace Editor
} // namespace Framework

#endif // FRAMEWORK_EDITOR_BASE_EDITOR2D_MODEL_H
