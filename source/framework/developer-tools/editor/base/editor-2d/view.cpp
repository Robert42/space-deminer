#include "view.h"

#include <base/increment-in-lifetime.h>
#include <base/geometry/matrix3-transformations.h>

namespace Framework {
namespace Editor {
namespace Base {
namespace Editor2D {

using ::Base::Geometry::mat3x3ToMat3x2;
using ::Base::Geometry::scale3;
using ::Base::Geometry::translate3;

View::View(const Model::Ptr& model)
  : dirty(true),
    blockedSignals(0),
    blockedUpdate(0),
    blockedConstraints(0),
    _viewportSize(model->boundingRect().empty() ? vec2(1) : model->boundingRect().rectangle().size()),
    _zoomFactor(1.f),
    _modelUnitsPerPixel(1.f),
    _invertY(false),
    model(model)
{
  moveToCenter();

  model->signalBoundingRectChanged().connect(std::bind(&View::applyPositionConstraints, this)).track(this->trackable);
}


View::~View()
{
}


View::Ptr View::create(const Model::Ptr& model)
{
  return Ptr(new View(model));
}


void View::setPosition(const vec2& position_)
{
  vec2 position = position_;

  if(blockedConstraints==0)
    position = _signalConstrainPosition(position);

  if(this->_position != position)
  {
    this->_position = position;
    this->markDirtyAndUpdate();
  }
}


const vec2& View::position() const
{
  return this->_position;
}


void View::moveToCenter()
{
  const BoundingRect& modelBoundingRect = model->boundingRect();

  vec2 center;

  if(modelBoundingRect.empty())
    center = vec2(0);
  else
    center = modelBoundingRect.rectangle().centerPoint();

  this->setPosition(center);
}


void View::moveInViewSpace(const ivec2& offset)
{
  vec2 modelSpacePosition = transformFromViewToModelSpace(offset);

  setPosition(modelSpacePosition);
}


void View::setViewportSize(const vec2& viewportSize)
{
  if(this->_viewportSize != viewportSize)
  {
    this->_viewportSize = viewportSize;
    this->markDirtyAndUpdate();
    this->_signalViewportSizeChanged(this->_viewportSize);
  }
}


const vec2& View::viewportSize() const
{
  return this->_viewportSize;
}


void View::setZoomFactor(real zoomFactor)
{
  zoomFactor = clamp<real>(zoomFactor, 0.001, 3200.f);

  if(this->_zoomFactor != zoomFactor)
  {
    this->_zoomFactor = zoomFactor;
    this->markDirtyAndUpdate();
  }
}


void View::zoom(float direction)
{
  setZoomFactor(zoomFactor() * pow(1.4, direction));
}


void  View::zoomWithFixedScreenPosition(float direction, const vec2& fixedScreenPosition)
{
  vec2 screenSpaceDifference = transformFromViewToScreenSpace(vec2(0)) - fixedScreenPosition;

  {
    IncrementInLifeTime<int> blocker(blockedSignals);
    IncrementInLifeTime<int> constraintblocker(blockedConstraints);

    {
      IncrementInLifeTime<int> updateBlocker(blockedUpdate);

      setPosition(transformFromScreenToModelSpace(fixedScreenPosition));

      (void)updateBlocker;
    }

    zoom(direction);

    (void)blocker;
    (void)constraintblocker;
  }

  moveInViewSpace(screenSpaceDifference);
}


real View::zoomFactor() const
{
  return this->_zoomFactor;
}


void View::setPixelsPerModelUnit(float pixelsPerModelUnit)
{
  assert(pixelsPerModelUnit > 0.f);

  setModelUnitsPerPixel(1.f/pixelsPerModelUnit);
}


void View::setModelUnitsPerPixel(float modelUnitsPerPixel)
{
  assert(modelUnitsPerPixel > 0.f);

  this->_modelUnitsPerPixel = modelUnitsPerPixel;
}


float View::pixelsPerModelUnit() const
{
  return 1.f / _modelUnitsPerPixel;
}


float View::modelUnitsPerPixel() const
{
  return _modelUnitsPerPixel;
}


Rectangle<vec2> View::viewportInModelSpace() const
{
  return Rectangle<vec2>(vec2(0),
                         transformFromScreenToModelSpace(viewportSize()));
}


real View::calcBestfittingZoomForModel(bool crop) const
{
  const BoundingRect& modelBoundingRect = model->boundingRect();

  if(modelBoundingRect.empty())
    return 1.f;

  const vec2& viewportSize = this->viewportSize();
  const vec2 modelSize = modelBoundingRect.rectangle().size();

  assert(modelSize.y>0.f);
  assert(viewportSize.y>0.f);

  const real viewportAspectRatio = viewportSize.x / viewportSize.y;
  const real modelAspectRatio = modelSize.x / modelSize.y;

  bool alignToWidth = viewportAspectRatio < modelAspectRatio;

  if(crop)
    alignToWidth = !alignToWidth;

  if(alignToWidth)
    return _modelUnitsPerPixel * viewportSize.x / modelSize.x;
  else
    return _modelUnitsPerPixel * viewportSize.y / modelSize.y;
}


void View::fitToModel(bool crop, bool dontEnlarge)
{
  {
    IncrementInLifeTime<int> blocker(blockedUpdate);

    moveToCenter();

    if(dontEnlarge)
    {
      // If the model is larger than the view, it gets smaller, otherwise it will be shown with zoom 100%
      setZoomFactor(std::min<real>(1.f, calcBestfittingZoomForModel(crop)));
    }else
    {
      setZoomFactor(calcBestfittingZoomForModel(crop));
    }

    (void)blocker;
  }

  update();
}


void View::resetView(float zoomFactor)
{
  {
    IncrementInLifeTime<int> blocker(blockedUpdate);

    moveToCenter();
    setZoomFactor(zoomFactor);

    (void)blocker;
  }

  update();
}


void View::setInvertY(bool invertY)
{
  if(this->_invertY != invertY)
  {
    this->_invertY = invertY;
    this->markDirtyAndUpdate();
  }
}


bool View::invertY() const
{
  return this->_invertY;
}


const mat3x2& View::screenToModelMatrix() const
{
  return _screenToModelMatrix;
}


const mat3x2& View::modelToScreenMatrix() const
{
  return _modelToScreenMatrix;
}


vec2 View::transformFromScreenToModelSpace(const vec2& screenSpaceCoordinate) const
{
  return screenToModelMatrix()*vec3(screenSpaceCoordinate, 1);
}


vec2 View::transformFromModelToScreenSpace(const vec2& modelSpaceCoordinate) const
{
  return modelToScreenMatrix()*vec3(modelSpaceCoordinate, 1);
}


vec2 View::transformFromViewToModelSpace(const vec2& viewSpaceCoordinate) const
{
  return transformFromScreenToModelSpace(transformFromViewToScreenSpace(viewSpaceCoordinate));
}


vec2 View::transformFromModelToViewSpace(const vec2& modelSpaceCoordinate) const
{
  return transformFromScreenToViewSpace(transformFromModelToScreenSpace(modelSpaceCoordinate));
}


vec2 View::transformFromViewToScreenSpace(const vec2& viewSpaceCoordinate) const
{
  return viewSpaceCoordinate + viewportSize()*0.5f;
}


vec2 View::transformFromScreenToViewSpace(const vec2& screenSpaceCoordinate) const
{
  return screenSpaceCoordinate - viewportSize()*0.5f;
}


vec2 View::transformDirectionFromScreenToModelSpace(const vec2& screenSpaceDirection) const
{
  return screenToModelMatrix()*vec3(screenSpaceDirection, 0);
}


vec2 View::transformDirectionFromModelToScreenSpace(const vec2& modelSpaceDirection) const
{
  return modelToScreenMatrix()*vec3(modelSpaceDirection, 0);
}


vec2 View::transformDirectionFromViewToModelSpace(const vec2& viewSpaceDirection) const
{
  return transformDirectionFromScreenToModelSpace(transformDirectionFromViewToScreenSpace(viewSpaceDirection));
}


vec2 View::transformDirectionFromModelToViewSpace(const vec2& modelSpaceDirection) const
{
  return transformDirectionFromScreenToViewSpace(transformDirectionFromModelToScreenSpace(modelSpaceDirection));
}


vec2 View::transformDirectionFromViewToScreenSpace(const vec2& viewSpaceDirection) const
{
  return viewSpaceDirection;
}


vec2 View::transformDirectionFromScreenToViewSpace(const vec2& screenSpaceDirection) const
{
  return screenSpaceDirection;
}


Circle View::transformFromScreenToModelSpace(const Circle& screenSpaceCircle) const
{
  return Circle(transformFromScreenToModelSpace(screenSpaceCircle.centerPosition()),
                screenSpaceCircle.radius() / zoomFactor());
}


Circle View::transformFromModelToScreenSpace(const Circle& modelSpaceCircle) const
{
  return Circle(transformFromModelToScreenSpace(modelSpaceCircle.centerPosition()),
                modelSpaceCircle.radius() * zoomFactor());
}


Circle View::transformFromViewToModelSpace(const Circle& viewSpaceCircle) const
{
  return Circle(transformFromViewToModelSpace(viewSpaceCircle.centerPosition()),
                viewSpaceCircle.radius() / zoomFactor());
}


Circle View::transformFromModelToViewSpace(const Circle& modelSpaceCircle) const
{
  return Circle(transformFromModelToViewSpace(modelSpaceCircle.centerPosition()),
                modelSpaceCircle.radius() * zoomFactor());
}


Rectangle<vec2> View::transformFromScreenToModelSpace(const Rectangle<vec2>& screenSpaceRectangle) const
{
  return Rectangle<vec2>(transformFromScreenToModelSpace(screenSpaceRectangle.min()),
                         transformFromScreenToModelSpace(screenSpaceRectangle.max()));
}


Rectangle<vec2> View::transformFromModelToScreenSpace(const Rectangle<vec2>& modelSpaceRectangle) const
{
  return Rectangle<vec2>(transformFromModelToScreenSpace(modelSpaceRectangle.min()),
                         transformFromModelToScreenSpace(modelSpaceRectangle.max()));
}


Rectangle<vec2> View::transformFromViewToModelSpace(const Rectangle<vec2>& viewSpaceRectangle) const
{
  return Rectangle<vec2>(transformFromViewToModelSpace(viewSpaceRectangle.min()),
                         transformFromViewToModelSpace(viewSpaceRectangle.max()));
}


Rectangle<vec2> View::transformFromModelToViewSpace(const Rectangle<vec2>& modelSpaceRectangle) const
{
  return Rectangle<vec2>(transformFromModelToViewSpace(modelSpaceRectangle.min()),
                         transformFromModelToViewSpace(modelSpaceRectangle.max()));
}


void View::markDirtyAndUpdate()
{
  dirty = true;
  update();
}


void View::update()
{
  if(dirty)
  {
    if(blockedUpdate > 0)
      return;

    _screenToModelMatrix = createScreenToModelMatrix();
    _modelToScreenMatrix = createModelToScreenMatrix();

    if(blockedSignals > 0)
      return;

    dirty = false;

    _signalViewChanged();
  }
}


vec2 View::calcScaleFactor() const
{
  return vec2(_zoomFactor/_modelUnitsPerPixel) * vec2(1.f, _invertY ? -1.f : 1.f);
}


mat3x2 View::createScreenToModelMatrix() const
{
  return mat3x3ToMat3x2(
          translate3(_position) *
          scale3(1.f/calcScaleFactor()) *
          translate3(-_viewportSize*0.5f)
        );
}


mat3x2 View::createModelToScreenMatrix() const
{
  return mat3x3ToMat3x2(
          translate3(_viewportSize*0.5f) *
          scale3(calcScaleFactor()) *
          translate3(-_position)
        );
}


void View::applyPositionConstraints()
{
  setPosition(position());
}


void View::addConstraint_ClampPositionToModelBoundingBox()
{
  signalConstrainPosition().connect(std::bind(&View::constrainPositionToModelBoundingBox, this, _1)).trackManually();

  applyPositionConstraints();
}


Signals::Signal<void()>& View::signalViewChanged()
{
  return _signalViewChanged;
}


Signals::Signal<vec2(const vec2&), Signals::ResultHandler::PassResultAsArgumentResultHandler<vec2(const vec2&)>>& View::signalConstrainPosition()
{
  return _signalConstrainPosition;
}


Signals::Signal<void (const vec2&)>& View::signalViewportSizeChanged()
{
  return _signalViewportSizeChanged;
}


vec2 View::constrainPositionToModelBoundingBox(const vec2& pos)
{
  const BoundingRect& modelBoundingRect = model->boundingRect();

  if(modelBoundingRect.empty())
    return pos;

  return model->boundingRect().rectangle().clamp(pos);
}


} // namespace Editor2D
} // namespace Base
} // namespace Editor
} // namespace Framework
