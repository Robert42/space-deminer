#ifndef FRAMEWORK_EDITOR_BASE_EDITOR2D_RENDERER_H
#define FRAMEWORK_EDITOR_BASE_EDITOR2D_RENDERER_H

#include "view.h"

namespace Framework {
namespace Editor {
namespace Base {
namespace Editor2D {

class Renderer
{
public:
  typedef std::shared_ptr<Renderer> Ptr;

public:
  Signals::Trackable trackable;

private:
  Signals::CallableSignal<void()> _signalMarkedAsDirty;

public:
  Renderer(const View::Ptr& view);
  virtual ~Renderer();

protected:
  void markAsDirty();

public:
  Signals::Signal<void()>& signalMarkedAsDirty();
};

} // namespace Editor2D
} // namespace Base
} // namespace Editor
} // namespace Framework

#endif // FRAMEWORK_EDITOR_BASE_EDITOR2D_RENDERER_H
