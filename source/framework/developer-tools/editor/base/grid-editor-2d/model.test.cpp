#include "model.h"

namespace Framework {
namespace Editor {
namespace Base {
namespace GridEditor2D {

class TestCell
{
public:
  typedef std::shared_ptr<TestCell> Ptr;

  const int id;

  TestCell(int id)
    : id(id)
  {
  }
};

class TestModel : public Model<TestCell>
{
private:
  int nextCellId;

public:
  TestModel(const vec2& gridSize)
    : Model(gridSize)
  {
    nextCellId = 0;
  }

  Cell::Ptr createCell()
  {
    return Cell::Ptr(new TestCell(nextCellId++));
  }

  FRIEND_TEST(framework_editor_base_grideditor2d_Model, cellIndexForPosition);

  Optional<TestCell::Ptr> cellForIndex(const ivec2& index)
  {
    Optional<Model::Cell::Ptr> baseCell = Model::cellForIndex(index);

    if(!baseCell)
      return nothing;

    return std::static_pointer_cast<TestCell>(*baseCell);
  }

  TestCell::Ptr findOrCreateCell(const ivec2& index)
  {
    return std::static_pointer_cast<TestCell>(Model::findOrCreateCell(index));
  }

  QList<TestCell::Ptr> cellsInRectangle(const Rectangle<vec2>& rect)
  {
    QList<TestCell::Ptr> cells;

    for(const Cell::Ptr& c : Model::cellsInRectangle(rect))
      cells.append(std::static_pointer_cast<TestCell>(c));

    return cells;
  }
};


TEST(framework_editor_base_grideditor2d_Model, cellIndexForPosition)
{
  TestModel model(vec2(100));

  EXPECT_EQ(ivec2(0, 0), model.cellIndexForPosition(vec2(0, 0)));
  EXPECT_EQ(ivec2(0, -1), model.cellIndexForPosition(vec2(0, -10)));
  EXPECT_EQ(ivec2(1, 1), model.cellIndexForPosition(vec2(100, 199.999)));
}


TEST(framework_editor_base_grideditor2d_Model, cellForIndex)
{
  TestModel model(vec2(100));

  EXPECT_FALSE(model.cellForIndex(ivec2(0, 0)));
  EXPECT_FALSE(model.cellForIndex(ivec2(1, 0)));

  TestCell::Ptr createdCell = model.findOrCreateCell(ivec2(0, 0));

  Optional<TestCell::Ptr> cell;
  ASSERT_TRUE(cell = model.cellForIndex(ivec2(0, 0)));
  EXPECT_EQ(createdCell, *cell);
  EXPECT_EQ(0, (*cell)->id);
  EXPECT_FALSE(model.cellForIndex(ivec2(1, 0)));
}


TEST(framework_editor_base_grideditor2d_Model, cellsInRectangle)
{
  TestModel model(vec2(100));

  EXPECT_TRUE(model.cellsInRectangle(Rectangle<vec2>(vec2(120, 210), vec2(499, 250))).empty());

  for(int y=-1; y<10; ++y)
  {
    for(int x=0; x<10; ++x)
    {
      model.findOrCreateCell(ivec2(x, y));
    }
  }

  QList<TestCell::Ptr> cells = model.cellsInRectangle(Rectangle<vec2>(vec2(120, 210), vec2(499, 250)));

  ASSERT_EQ(4, cells.size());
  EXPECT_EQ(31, cells.first()->id);
  cells.pop_front();
  EXPECT_EQ(32, cells.first()->id);
  cells.pop_front();
  EXPECT_EQ(33, cells.first()->id);
  cells.pop_front();
  EXPECT_EQ(34, cells.first()->id);
  cells.pop_front();
}


} // namespace GridEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework
