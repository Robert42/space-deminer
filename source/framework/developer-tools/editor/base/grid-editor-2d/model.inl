#ifndef FRAMEWORK_EDITOR_BASE_GRIDEDITOR2D_MODEL_INL
#define FRAMEWORK_EDITOR_BASE_GRIDEDITOR2D_MODEL_INL

#include "model.h"

namespace Framework {
namespace Editor {
namespace Base {
namespace GridEditor2D {


template<typename T>
Model<T>::Model(const vec2& gridSize, const Editor2D::BoundingRect& boundingRectangle)
  : Editor2D::Model(boundingRectangle),
    gridSize(gridSize)
{
}


template<typename T>
ivec2 Model<T>::cellIndexForPosition(const vec2& position) const
{
  return ivec2(floor(position / gridSize));
}


template<typename T>
Optional<typename Model<T>::CellPtr> Model<T>::cellForIndex(const ivec2& index)
{
  GridIterator iter = iteratorWithIndex(index);

  if(iter == _gridMap.end())
    return nothing;

  return *iter;
}


template<typename T>
Optional<typename Model<T>::CellPtr> Model<T>::cellForPosition(const vec2& position)
{
  return cellForIndex(cellIndexForPosition(position));
}


template<typename T>
QList<std::shared_ptr<T>> Model<T>::cellsInRectangle(const Rectangle<vec2>& rect)
{
  return cells(Rectangle<ivec2>(cellIndexForPosition(rect.min()),
                                cellIndexForPosition(rect.max())));
}


template<typename T>
QList<std::shared_ptr<T>> Model<T>::cells(const Rectangle<ivec2>& indices)
{
  QList<CellPtr> cells;

  const ivec2 from = indices.min();
  const ivec2 to = indices.max();

  for(int x=from.x; x<=to.x; ++x)
  {
    for(int y=from.y; y<=to.y; ++y)
    {
      Optional<Model<T>::CellPtr> cell;
      if(cell = cellForIndex(ivec2(x, y)))
        cells.append(*cell);
    }
  }

  return cells;
}


template<typename T>
std::shared_ptr<T> Model<T>::findOrCreateCell(const ivec2& index)
{
  Optional<CellPtr> c = cellForIndex(index);

  if(c)
    return *c;

  CellPtr cell = createCell();

  _gridMap.insert(keyFromIndex(index), cell);

  return cell;
}


template<typename T>
QList<std::shared_ptr<T>> Model<T>::findOrCreateCellsInRectangle(const Rectangle<vec2>& rect)
{
  return findOrCreateCells(Rectangle<ivec2>(cellIndexForPosition(rect.min()),
                                            cellIndexForPosition(rect.max())));
}


template<typename T>
QList<std::shared_ptr<T>> Model<T>::findOrCreateCells(const Rectangle<ivec2>& indices)
{
  QList<Model<T>::CellPtr> cells;

  const ivec2 from = indices.min();
  const ivec2 to = indices.max();

  for(int x=from.x; x<=to.x; ++x)
  {
    for(int y=from.y; y<=to.y; ++y)
    {
      cells.append(findOrCreateCell(ivec2(x, y)));
    }
  }

  return cells;
}


template<typename T>
void Model<T>::removeCell(const ivec2& index)
{
  GridIterator iter = iteratorWithIndex(index);

  if(iter != _gridMap.end())
    _gridMap.erase(iter);
}


template<typename T>
void Model<T>::removeCell(const CellPtr& cell)
{
  for(auto i=_gridMap.begin(); i!=_gridMap.end();)
  {
    if(*i == cell)
      i = _gridMap.erase(i);
    else
      ++i;
  }
}


template<typename T>
const typename Model<T>::GridMap& Model<T>::gridMap()
{
  return _gridMap;
}


template<typename T>
typename Model<T>::GridIterator Model<T>::iteratorWithIndex(const ivec2& index)
{
  return _gridMap.find(keyFromIndex(index));
}


template<typename T>
QPair<int, int> Model<T>::keyFromIndex(const ivec2& index)
{
  return QPair<int, int>(index.x, index.y);
}


} // namespace GridEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework

#endif
