#ifndef FRAMEWORK_EDITOR_BASE_GRIDEDITOR2D_MODEL_H
#define FRAMEWORK_EDITOR_BASE_GRIDEDITOR2D_MODEL_H

#include <framework/developer-tools/editor/base/editor-2d/model.h>
#include <base/optional.h>

namespace Framework {
namespace Editor {
namespace Base {
namespace GridEditor2D {

template<typename T_Cell>
class Model : public Editor2D::Model
{
public:
  typedef T_Cell Cell;
  typedef std::shared_ptr<Cell> CellPtr;

private:
  typedef QMap<QPair<int, int>, CellPtr> GridMap;
  typedef typename GridMap::iterator GridIterator;

  const vec2 gridSize;
  GridMap _gridMap;

public:
  Model(const vec2& gridSize,
        const Editor2D::BoundingRect& boundingRectangle = Editor2D::BoundingRect());

protected:
  ivec2 cellIndexForPosition(const vec2& position) const;

  Optional<Model::CellPtr> cellForIndex(const ivec2& index);
  Optional<Model::CellPtr> cellForPosition(const vec2& position);

  QList<CellPtr> cellsInRectangle(const Rectangle<vec2>& rect);
  QList<CellPtr> cells(const Rectangle<ivec2>& indices);

protected:
  virtual CellPtr createCell() = 0;

  CellPtr findOrCreateCell(const ivec2& index);
  QList<CellPtr> findOrCreateCells(const Rectangle<ivec2>& indices);
  QList<CellPtr> findOrCreateCellsInRectangle(const Rectangle<vec2>& rect);

  void removeCell(const ivec2& index);
  void removeCell(const CellPtr& cell);

  const GridMap& gridMap();

private:
  GridIterator iteratorWithIndex(const ivec2& index);
  GridIterator findCell(const CellPtr& cell);

  static QPair<int, int> keyFromIndex(const ivec2& index);
};


} // namespace GridEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework

#include "model.inl"

#endif // FRAMEWORK_EDITOR_BASE_GRIDEDITOR2D_MODEL_H
