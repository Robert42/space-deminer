#ifndef FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_VIEW_H
#define FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_VIEW_H

#include "model.h"

#include <framework/developer-tools/editor/base/editor-2d/view.h>

namespace Framework {
namespace Editor {
namespace Base {
namespace ObjectEditor2D {

class View : public Editor2D::View
{
public:
  typedef std::shared_ptr<View> Ptr;

public:
  View(const Model::Ptr& objectModel);

  static Ptr create(const Model::Ptr& objectModel);

  QSet<Object::Ptr> findObjectsInView() const;
  QSet<Object::Ptr> findObjectsInViewWithExtension(const vec2& extension) const;
  QSet<Object::Ptr> findObjectsInScreenspaceRectangle(const Rectangle<vec2>& rect) const;

public:
  Model::Ptr model() const;
};

} // namespace ObjectEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework

#endif // FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_VIEW_H
