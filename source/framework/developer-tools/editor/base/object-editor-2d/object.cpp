#include "object.h"
#include "model.h"

namespace Framework {
namespace Editor {
namespace Base {
namespace ObjectEditor2D {

Object::Object()
  : _model(nullptr),
    _boundingRect(vec2(0), vec2(1))
{
}

Object::~Object()
{
}


const Model& Object::model() const
{
  return *_model;
}


Model& Object::model()
{
  return *_model;
}


const Rectangle<vec2>& Object::boundingRect() const
{
  return _boundingRect;
}


vec2 Object::originPosition() const
{
  return model().objectOriginPosition(*this);
}


} // namespace ObjectEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework
