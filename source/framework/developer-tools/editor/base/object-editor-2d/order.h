#ifndef FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_ORDER_H
#define FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_ORDER_H

#include "object.h"

namespace Framework {
namespace Editor {
namespace Base {
namespace ObjectEditor2D {

class Order
{
public:
  typedef std::shared_ptr<Order> Ptr;

public:
  class Iterator
  {
  public:
    typedef std::shared_ptr<Iterator> Ptr;

  public:
    Iterator();
    virtual ~Iterator();

  public:
    virtual bool hasNext() const = 0;
    Object::Ptr next();

  protected:
    virtual Object::Ptr nextImpl() = 0;
  };

public:
  Order();
  virtual ~Order();

  static Ptr createSimpleOrder();

  virtual Iterator::Ptr upToDownIterator(const QSet<Object::Ptr>& objects) = 0;
  virtual Iterator::Ptr downToUpIterator(const QSet<Object::Ptr>& objects) = 0;
};


} // namespace ObjectEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework

#endif // FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_ORDER_H
