#include "selection-based-order.h"

namespace Framework {
namespace Editor {
namespace Base {
namespace ObjectEditor2D {


class SelectionBasedOrderIterator : public Order::Iterator
{
protected:
  QVector<Object::Ptr> objects;
  const int end;
  int i;

public:
  SelectionBasedOrderIterator(const QVector<Object::Ptr>& orderedObjects, const QSet<Object::Ptr> &objectSet, int begin, int end)
    : objects(filter(orderedObjects, objectSet)),
      end(end),
      i(begin)
  {
  }

  bool hasNext() const override
  {
    return i != end;
  }

private:
  static QVector<Object::Ptr> filter(const QVector<Object::Ptr>& orderedObjects, const QSet<Object::Ptr>& objectSet)
  {
    QVector<Object::Ptr> v;
    v.reserve(objectSet.size());

    for(const Object::Ptr& o : orderedObjects)
      if(objectSet.contains(o))
        v.append(o);

    // The objectset should be a subset of all object of the model
    // Also all objects of the model should be part of this class
    assert(v.size() == objectSet.size());

    return v;
  }
};

class SelectionBasedForwardOrderIterator : public SelectionBasedOrderIterator
{
public:
  SelectionBasedForwardOrderIterator(const QVector<Object::Ptr>& orderedObjects, const QSet<Object::Ptr>& objectSet)
    : SelectionBasedOrderIterator(orderedObjects, objectSet, 0, objectSet.size())
  {
  }

  Object::Ptr nextImpl() override
  {
    return objects[i++];
  }
};

class SelectionBasedBackwardOrderIterator : public SelectionBasedOrderIterator
{
public:
  SelectionBasedBackwardOrderIterator(const QVector<Object::Ptr>& orderedObjects, const QSet<Object::Ptr>& objectSet)
    : SelectionBasedOrderIterator(orderedObjects, objectSet, objectSet.size()-1, -1)
  {
  }

  Object::Ptr nextImpl() override
  {
    return objects[i--];
  }
};


SelectionBasedOrder::SelectionBasedOrder(const Selection::Ptr& selection)
  : selection(selection),
    _dirty(true)
{
  const Model::Ptr& model = selection->model();

  selection->signalActiveObjectChanged().connect(std::bind(&SelectionBasedOrder::markDirty, this)).track(this->trackable);
  model->signalObjectWasAdded().connect(std::bind(&SelectionBasedOrder::addObject, this, _1)).track(this->trackable);
  model->signalObjectWasRemoved().connect(std::bind(&SelectionBasedOrder::removeObject, this, _1)).track(this->trackable);

  for(const Object::Ptr& o : model->allObjects())
    objects.append(o);
}


SelectionBasedOrder::Ptr SelectionBasedOrder::create(const Selection::Ptr& selection)
{
  return SelectionBasedOrder::Ptr(new SelectionBasedOrder(selection));
}


Order::Iterator::Ptr SelectionBasedOrder::upToDownIterator(const QSet<Object::Ptr>& objects)
{
  update();
  return Iterator::Ptr(new SelectionBasedBackwardOrderIterator(this->objects,
                                                               objects));
}


Order::Iterator::Ptr SelectionBasedOrder::downToUpIterator(const QSet<Object::Ptr>& objects)
{
  update();
  return Iterator::Ptr(new SelectionBasedForwardOrderIterator(this->objects,
                                                              objects));
}


void SelectionBasedOrder::markDirty()
{
  _dirty = true;
}


bool SelectionBasedOrder::enforeLessThan(const Object::Ptr&, const Object::Ptr&)
{
  return false;
}


void SelectionBasedOrder::update()
{
  if(!_dirty)
    return;
  _dirty = false;

  std::function<bool(const Object::Ptr& a, const Object::Ptr& b)> less = [this](const Object::Ptr& a, const Object::Ptr& b){
    if(enforeLessThan(a, b))
      return true;
    else if(enforeLessThan(b, a))
      return false;
    return false;
  };

  Optional<Object::Ptr> optionalActiveObject;
  if(optionalActiveObject = selection->activeObject())
  {
    Object::Ptr activeObject = *optionalActiveObject;

    less = [&activeObject,this](const Object::Ptr& a, const Object::Ptr& b){
      if(enforeLessThan(a, b))
        return true;
      else if(enforeLessThan(b, a))
        return false;
      else if(activeObject == b)
        return true;
      else if(activeObject == a)
        return false;
      return false;
    };
  }

  qStableSort(objects.begin(),
              objects.end(),
              less);
}


void SelectionBasedOrder::addObject(const Object::Ptr& object)
{
  objects.append(object);
  markDirty();
}


void SelectionBasedOrder::removeObject(const Object::Ptr& object)
{
  int i = objects.indexOf(object);

  if(i>=0)
  {
    objects.remove(i);
    markDirty();
  }
}


} // namespace ObjectEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework
