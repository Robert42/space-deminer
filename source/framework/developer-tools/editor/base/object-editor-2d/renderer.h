#ifndef FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_RENDERER_H
#define FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_RENDERER_H

#include "view.h"
#include "order.h"
#include "shape.h"
#include "selection.h"

#include <framework/developer-tools/editor/base/editor-2d/renderer.h>

namespace Framework {
namespace Editor {
namespace Base {
namespace ObjectEditor2D {


class Renderer : public Editor2D::Renderer
{
private:
  const View::Ptr _view;
  const Order::Ptr order;
  const Shape::Ptr shape;

public:
  Renderer(const View::Ptr& view,
           const Order::Ptr& order,
           const Shape::Ptr& shape,
           const Selection::Ptr& selection);

  Order::Iterator::Ptr objectsToRender();

  const View::Ptr& view();

protected:
  void markAsDirty();
};


} // namespace ObjectEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework

#endif // FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_RENDERER_H
