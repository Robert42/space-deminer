#include "controller.h"

#include <base/geometry/bounding-rect.h>
#include <framework/window/render-window.h>

#include <base/io/log.h>

namespace Framework {
namespace Editor {
namespace Base {
namespace ObjectEditor2D {

Controller::Controller(const View::Ptr& view, const Order::Ptr& order, const Shape::Ptr& shape, const Selection::Ptr& selection, const SnappingPosition::Ptr& snappingPosition)
  : Editor2D::Controller(view),
    _order(order),
    _shape(shape),
    _selection(selection),
    _snappingPosition(snappingPosition),
    _selectionCircleRadius(32.f)
{
  if(view->model() != selection->model())
    IO::Log::logError("Framework::Editor::Base::ObjectEditor2D::Controller: the view and the selection mus both use the same model");
  assert(view->model() == selection->model());
}


Controller::~Controller()
{
  _resetCurrentAction();
}


Controller::Ptr Controller::create(const View::Ptr& view, const Order::Ptr& order, const Shape::Ptr& shape, const Selection::Ptr& selection, const SnappingPosition::Ptr& snappingPosition)
{
  return Ptr(new Controller(view, order, shape, selection, snappingPosition));
}


std::string Controller::shortcutHelpText()
{
  return Editor2D::Controller::shortcutHelpText() +
      "\n"
      "Press A to select/deselect all objects\n"
      "Press I to invert the selection\n"
      "You can select single objects by clicking on them with the left/right mouse button (hold shift to add it to the selection).\n"
      "When starting dragging from an empty area holding the right mouse-button, you can select multiple objects at once.\n"
      "Press B to select multiple objects using a selection rectangle. Hold Shift, to deselect instead.\n"
      "Press C to select multiple objects fast using a selection brush. Hold Shift, to deselect instead.\n"
      "Press G to move objects. Using X/Y, you can constrain the moving direction.\n"
      "Press S to move objects by scaling the objects. Using X/Y, you can constrain the scaling direction.\n"
      "Press R to move objects by rotate the objects.\n";
}


Optional<Object::Ptr> Controller::objectAtScreenPoint(const vec2& screenPoint)
{
  const vec2 modelPoint = view()->transformFromScreenToModelSpace(screenPoint);

  Order::Iterator::Ptr iterator = order()->upToDownIterator(model()->objectsAtPoint(modelPoint));

  while(iterator->hasNext())
  {
    Object::Ptr object = iterator->next();

    if(object->boundingRect().contains(modelPoint))
    {
      if(shape()->isWithinShape(modelPoint, object))
      {
        return object;
      }
    }
  }

  return nothing;
}


View::Ptr Controller::view()
{
  return std::static_pointer_cast<View>(Editor2D::Controller::view());
}


const Model::Ptr& Controller::model()
{
  return this->_selection->model();
}


const Order::Ptr& Controller::order()
{
  return this->_order;
}


const Shape::Ptr& Controller::shape()
{
  return this->_shape;
}


const Selection::Ptr& Controller::selection()
{
  return this->_selection;
}


void  Controller::changeSelectionCircleRadius(real direction)
{
  setSelectionCircleRadius(selectionCircleScreenspaceRadius() * pow(2.f, -0.125f * direction));
}


void Controller::setSelectionCircleRadius(real radius)
{
  radius = clamp<real>(radius, 4.f, RenderWindow::size().x*0.75f);

  if(this->_selectionCircleRadius != radius)
  {
    this->_selectionCircleRadius = radius;
    _signalSelectionCircleRadiusChanged(this->_selectionCircleRadius);
  }
}


real Controller::selectionCircleScreenspaceRadius() const
{
  return _selectionCircleRadius;
}


Signals::Signal<void(const vec2&)>& Controller::signalShowSelectionRectangle()
{
  return _signalShowSelectionRectangle;
}


Signals::Signal<void()>& Controller::signalHideSelectionRectangle()
{
  return _signalHideSelectionRectangle;
}


Signals::Signal<void()>& Controller::signalShowSelectionCircle()
{
  return _signalShowSelectionCircle;
}


Signals::Signal<void()>& Controller::signalHideSelectionCircle()
{
  return _signalHideSelectionCircle;
}


Signals::Signal<void()>& Controller::signalShowSelectionRectangleCrosslines()
{
  return _signalShowSelectionRectangleCrosslines;
}


Signals::Signal<void()>& Controller::signalHideSelectionRectangleCrosslines()
{
  return _signalHideSelectionRectangleCrosslines;
}


Signals::Signal<void(Controller::ScaleObjectsPositionAction&)>& Controller::signalShowScalingLine()
{
  return _signalShowScalingLine;
}


Signals::Signal<void(Controller::RotateObjectsPositionAction&)>& Controller::signalShowRotationLine()
{
  return _signalShowRotationLine;
}


Signals::Signal<void(real)>& Controller::signalSelectionCircleRadiusChanged()
{
  return _signalSelectionCircleRadiusChanged;
}


real Controller::slowDownFactor() const
{
  return 0.125f;
}


bool Controller::selectOrDeselectAll()
{
  if(!hasInputFocus())
    return false;

  selection()->selectOrDeselectAll();
  return true;
}


bool Controller::selectAllObjects()
{
  if(!hasInputFocus())
    return false;

  selection()->selectAll();
  return true;
}


bool Controller::unselectAllObjects()
{
  if(!hasInputFocus())
    return false;

  selection()->unselectAll();
  return true;
}


bool Controller::invertSelection()
{
  if(!hasInputFocus())
    return false;

  selection()->invertSelection();
  return true;
}


Optional<Object::Ptr> Controller::selectAndActivateObjectAtMouseCursor(bool deselectAllOther)
{
  Optional<Object::Ptr> o = objectAtScreenPoint(currentMousePosition());
  if(o)
  {
    Object::Ptr object = *o;
    if(deselectAllOther)
      selection()->setSelection({object});
    else
      selection()->selectObject(object);
    selection()->activateObject(object);
  }

  return o;
}


Controller::CircleSelectionAction::CircleSelectionAction(Controller& controller)
  : Action(controller),
    isSelecting(false),
    selectMode(true)
{
  controller._signalShowSelectionCircle();
  this->signalFinished().connect(controller._signalHideSelectionCircle).track(controller.trackable);

  this->constraint = Action::MouseConstraint::VIEWPORT;
}


bool Controller::CircleSelectionAction::handlePointerMovement()
{
  if(finished())
    return false;

  if(isSelecting)
    controller().selectObjectsInCircle(selectMode);

  return false;
}


void Controller::CircleSelectionAction::startSelectingObjectInSelectionCircle(bool selectMode)
{
  if(finished())
  {
    stopSelectingObjectInSelectionCircle();
    return;
  }

  this->isSelecting = true;
  this->selectMode = selectMode;

  controller().selectObjectsInCircle(this->selectMode);
}


void Controller::CircleSelectionAction::stopSelectingObjectInSelectionCircle()
{
  this->isSelecting = false;
}


Controller::RectangleSelectionCrossAction::RectangleSelectionCrossAction(Controller& controller)
  : Action(controller)
{
  controller._signalShowSelectionRectangleCrosslines();
  signalFinished().connect(controller._signalHideSelectionRectangleCrosslines).track(controller.trackable);

  this->constraint = Action::MouseConstraint::VIEWPORT;
}


Controller::RectangleSelectionAction::RectangleSelectionAction(Controller& controller, bool selectMode)
  : Action(controller)
{
  this->constraint = Action::MouseConstraint::VIEWPORT;

  vec2 selectionRectangleFrom = controller.currentMousePosition();
  signalSucceeded().connect(std::bind(&Controller::selectObjectInRectangleToMousePosition, &controller, selectionRectangleFrom, selectMode)).track(controller.trackable);
  signalFinished().connect(controller._signalHideSelectionRectangle).track(controller.trackable);

  controller._signalShowSelectionRectangle(selectionRectangleFrom);
}

// ====


Controller::TransformObjectPositionAction::Transformation::Transformation()
  : Transformation(nullptr, Object::Ptr())
{
}


Controller::TransformObjectPositionAction::Transformation::Transformation(TransformObjectPositionAction* action, const Object::Ptr& object)
  : _action(action),
    _object(object),
    _originalPosition(object->originPosition())
{
}


Controller::TransformObjectPositionAction& Controller::TransformObjectPositionAction::Transformation::action()
{
  return *_action;
}


const Object::Ptr& Controller::TransformObjectPositionAction::Transformation::object()
{
  return _object;
}


const vec2& Controller::TransformObjectPositionAction::Transformation::originalPosition() const
{
  return _originalPosition;
}


void Controller::TransformObjectPositionAction::Transformation::reset()
{
  object()->model().moveObjectTo(object(), originalPosition());
}


void Controller::TransformObjectPositionAction::Transformation::update()
{
  action().applyTransformation(*this);
}

// ----

Controller::TransformObjectPositionAction::TransformObjectPositionAction(const QSet<Object::Ptr>& objects, Controller& controller)
  : Action(controller),
    _slowMode(false),
    _mousePosition(0),
    originalMousePosition(controller.currentModelSpaceMousePosition()),
    originalCenterOfObjects(centerOfObjects(objects))
{
  this->constraint = Action::MouseConstraint::WRAPAROUND_VIEWPORT;

  this->transformations.reserve(objects.size());

  for(const Object::Ptr& o : objects)
    transformations.append(Transformation(this, o));

  signalAborted().connect(std::bind(&MoveObjectsAction::resetObjectsBeingMoved, this)).track(controller.trackable).track(this->trackable);
}


void Controller::TransformObjectPositionAction::resetObjectsBeingMoved()
{
  for(Transformation& t : transformations)
    t.reset();
}


bool Controller::TransformObjectPositionAction::slowMode() const
{
  return _slowMode;
}


vec2 Controller::TransformObjectPositionAction::snap(const vec2& originalPosition, const vec2& newPosition, const Object::Ptr& object)
{
  const SnappingPosition::Ptr& snapper = controller()._snappingPosition;

  return snapper->snap(originalPosition, newPosition, object);
}

void Controller::TransformObjectPositionAction::applyTransformation(Transformation& t)
{
  const Object::Ptr& object = t.object();
  const vec2& originalPosition = t.originalPosition();
  vec2 newPosition = calculatePosition(t);

  newPosition = snap(originalPosition, newPosition, object);

  object->model().moveObjectTo(object, newPosition);
}


void Controller::TransformObjectPositionAction::reapplyMovement()
{
  for(Transformation& t : transformations)
    t.update();
}


void Controller::TransformObjectPositionAction::slowObjectMovement()
{
  _slowMode = true;

  onSlow();
}

void Controller::TransformObjectPositionAction::unslowObjectMovement()
{
  _slowMode = false;

  onUnslow();

  reapplyMovement();
}


vec2 Controller::TransformObjectPositionAction::centerOfObjects(const QSet<Object::Ptr>& objects)
{
  Geometry::BoundingRect boundinRect;

  for(const Object::Ptr& o : objects)
    boundinRect |= o->originPosition();

  return boundinRect.rectangle().centerPoint();
}


const vec2& Controller::TransformObjectPositionAction::mousePosition() const
{
  return _mousePosition;
}


bool Controller::TransformObjectPositionAction::handlePointerMovement()
{
  const vec2 relativeMovement = controller().view()->transformDirectionFromScreenToModelSpace(controller().currentMouseMovement());
  _mousePosition += relativeMovement;

  mouseMoved(relativeMovement);

  return true;
}


// ====


Controller::MoveObjectsAction::MoveObjectsAction(const QSet<Object::Ptr>& objects, Controller& controller)
  : TransformObjectPositionAction(objects, controller),
    _axisSnapping(AxisSnapping::NONE)
{
}


Controller::MoveObjectsAction::AxisSnapping Controller::MoveObjectsAction::axisSnapping() const
{
  return this->_axisSnapping;
}


void Controller::MoveObjectsAction::setAxisSnapping(AxisSnapping axisSnapping)
{
  if(this->_axisSnapping != axisSnapping)
  {
    this->_axisSnapping = axisSnapping;

    reapplyMovement();
  }
}


void Controller::MoveObjectsAction::mouseMoved(const vec2& relativeMovement)
{
  const vec2 slowedDownOffset = slowMode() ? relativeMovement*controller().slowDownFactor()
                                           : relativeMovement;

  mousePositionSlowedDown += slowedDownOffset;

  reapplyMovement();
}

vec2 Controller::MoveObjectsAction::transform(const Transformation& t, const vec2& mousePosition)
{
  return t.originalPosition() + mousePosition;
}


vec2 Controller::MoveObjectsAction::calculatePosition(Transformation& t)
{
  return transform(t, mousePositionSlowedDown);
}

vec2 Controller::MoveObjectsAction::snap(const vec2& originalPosition, const vec2& newPosition, const Object::Ptr& object)
{
  vec2 snappedPosition = TransformObjectPositionAction::snap(originalPosition, newPosition, object);

  snappedPosition = (snappedPosition-originalPosition) * axisSnappingFactor() + originalPosition;

  return snappedPosition;
}


void Controller::MoveObjectsAction::onUnslow()
{
  mousePositionSlowedDown = mousePosition();
}


vec2 Controller::MoveObjectsAction::axisSnappingFactor() const
{
  switch(axisSnapping().value)
  {
  case AxisSnapping::X:
    return vec2(1.f, 0.f);
  case AxisSnapping::Y:
    return vec2(0.f, 1.f);
  default:
    return vec2(1.f);
  }
}


Controller::ScaleObjectsPositionAction::ScaleObjectsPositionAction(const QSet<Object::Ptr> &objects, Controller &controller)
  : MoveObjectsAction(objects, controller)
{
  controller._signalShowScalingLine(*this);
}


vec2 Controller::ScaleObjectsPositionAction::transform(const Transformation& t, const vec2& mousePosition)
{
  const vec2 originalOffset = t.originalPosition() - originalCenterOfObjects;
  const real originalMouseDistance = length(originalMousePosition - originalCenterOfObjects);
  const real currentMouseDistance = length(mousePosition+originalMousePosition - originalCenterOfObjects);

  if(originalMouseDistance == 0.f)
    return t.originalPosition();

  real factor = currentMouseDistance / originalMouseDistance;

  return originalOffset*factor + originalCenterOfObjects;
}


Controller::RotateObjectsPositionAction::RotateObjectsPositionAction(const QSet<Object::Ptr>& objects, Controller& controller)
  : TransformObjectPositionAction(objects, controller),
    angle(0.f)
{
  controller._signalShowRotationLine(*this);

  previousMouseAngle = directionToAngle(originalMousePosition-originalCenterOfObjects, 0.f);
}


void Controller::RotateObjectsPositionAction::mouseMoved(const vec2&)
{
  real currentMouseAngle = directionToAngle(mousePosition()+originalMousePosition-originalCenterOfObjects, previousMouseAngle);
  real angleDifference = currentMouseAngle - previousMouseAngle;

  while(angleDifference > half_pi)
    angleDifference -= pi;
  while(angleDifference < -half_pi)
    angleDifference += pi;

  if(slowMode())
    angleDifference *= controller().slowDownFactor();

  angle += angleDifference;

  previousMouseAngle = currentMouseAngle;

  reapplyMovement();
}


vec2 Controller::RotateObjectsPositionAction::calculatePosition(Transformation& t)
{
  return glm::rotateZ(vec3(t.originalPosition() - originalCenterOfObjects, 0.f), angle).xy() + originalCenterOfObjects;
}


real Controller::RotateObjectsPositionAction::directionToAngle(const vec2& direction, real fallback)
{
  vec2 v = normalize(direction);

  if(dot(v,v) == 0.f)
    return fallback;

  return atan(v.y, v.x);
}


Controller::ResizeObjectByHandle::ResizeObjectByHandle(const Object::Ptr& object, Shape::HandleType handle, Controller& controller)
  : Action(controller),
    object(object),
    originalBoundingRect(object->boundingRect()),
    originalCursorPosition(controller.currentModelSpaceMousePosition()),
    wnMask(handle==Shape::HandleType::RESIZE_NW||handle==Shape::HandleType::RESIZE_SW||handle==Shape::HandleType::RESIZE_W,
           handle==Shape::HandleType::RESIZE_NW||handle==Shape::HandleType::RESIZE_NE||handle==Shape::HandleType::RESIZE_N),
    esMask(handle==Shape::HandleType::RESIZE_SE||handle==Shape::HandleType::RESIZE_NE||handle==Shape::HandleType::RESIZE_E,
           handle==Shape::HandleType::RESIZE_SE||handle==Shape::HandleType::RESIZE_SW||handle==Shape::HandleType::RESIZE_S),
    totalOffset(0)
{
  this->constraint = Action::MouseConstraint::WRAPAROUND_VIEWPORT;

  signalAborted().connect(std::bind(&ResizeObjectByHandle::reset, this)).track(controller.trackable).track(this->trackable);
}


void Controller::ResizeObjectByHandle::reset()
{
  setBoundingRect(originalBoundingRect);
}


void Controller::ResizeObjectByHandle::setBoundingRect(const Rectangle<vec2>& boundingRect)
{
  Model& model = object->model();

  model.moveObject(object, boundingRect);
}


bool Controller::ResizeObjectByHandle::handlePointerMovement()
{
  totalOffset += controller().currentModelSpaceMouseMovement();

  setBoundingRect(Rectangle<vec2>(wnMask*totalOffset + originalBoundingRect.min(),
                                  esMask*totalOffset + originalBoundingRect.max()));

  return true;
}


Controller::CircleSelectionAction::Ptr Controller::startSelectingObjectsWithCircle()
{
  Controller::CircleSelectionAction::Ptr action = Controller::CircleSelectionAction::Ptr(new CircleSelectionAction(*this));

  _startAction(action);

  return action;
}


std::shared_ptr<Controller::RectangleSelectionCrossAction> Controller::startRectangleSelectionCross()
{
  Controller::RectangleSelectionCrossAction::Ptr action = Controller::RectangleSelectionCrossAction::Ptr(new RectangleSelectionCrossAction(*this));

  _startAction(action);

  return action;
}


std::shared_ptr<Controller::RectangleSelectionAction> Controller::startRectangleSelection(bool selectMode)
{
  Controller::RectangleSelectionAction::Ptr action = Controller::RectangleSelectionAction::Ptr(new RectangleSelectionAction(*this, selectMode));

  _startAction(action);

  return action;
}


Controller::MoveObjectsAction::Ptr Controller::startMovingObjects(const QSet<Object::Ptr>& objects)
{
  Controller::MoveObjectsAction::Ptr action = Controller::MoveObjectsAction::Ptr(new Controller::MoveObjectsAction(objects, *this));

  _startAction(action);

  if(objects.empty())
    action->abort();

  return action;
}


Controller::ScaleObjectsPositionAction::Ptr Controller::startScaleObjectsPositionAction(const QSet<Object::Ptr>& objects)
{
  Controller::ScaleObjectsPositionAction::Ptr action = Controller::ScaleObjectsPositionAction::Ptr(new Controller::ScaleObjectsPositionAction(objects, *this));

  _startAction(action);

  if(objects.empty())
    action->abort();

  return action;
}


Controller::RotateObjectsPositionAction::Ptr Controller::startRotateObjectsPositionAction(const QSet<Object::Ptr>& objects)
{
  Controller::RotateObjectsPositionAction::Ptr action = Controller::RotateObjectsPositionAction::Ptr(new Controller::RotateObjectsPositionAction(objects, *this));

  _startAction(action);

  if(objects.empty())
    action->abort();

  return action;
}


Controller::ResizeObjectByHandle::Ptr Controller::startResizingObjectByHandle(bool deselectAllOther, const Object::Ptr& object, Shape::HandleType handle)
{
  if(deselectAllOther)
    selection()->setSelection({object});
  else
    selection()->selectObject(object);

  Controller::ResizeObjectByHandle::Ptr action = Controller::ResizeObjectByHandle::Ptr(new Controller::ResizeObjectByHandle(object, handle, *this));

  _startAction(action);

  return action;
}


void Controller::selectObjectInRectangleToMousePosition(const vec2& from, bool selectMode)
{
  const Rectangle<vec2> rect(from, currentMousePosition());
  const vec2 rectSize = rect.size();

  if(rectSize.x==0.f || rectSize.y==0.f)
    return;

  QSet<Object::Ptr> objects = view()->findObjectsInScreenspaceRectangle(rect);

  if(selectMode)
    selection()->selectObjects(objects);
  else
    selection()->unselectObjects(objects);
}


void Controller::selectObjectsInCircle(bool selectMode)
{
  const Circle circle(view()->transformFromScreenToModelSpace(Circle(currentMousePosition(), selectionCircleScreenspaceRadius())));

  QSet<Object::Ptr> objects = model()->objectsInRectangle(circle.boundingRect());

  // We iterate over a copy of objects
  for(Object::Ptr o : QSet<Object::Ptr>(objects))
  {
    if(!shape()->isIntersectingWith(circle, o))
      objects.remove(o);
  }

  if(selectMode)
    selection()->selectObjects(objects);
  else
    selection()->unselectObjects(objects);
}


Controller::Action::Action(Controller& controller)
  : Editor2D::Controller::Action(controller)
{
}


Controller& Controller::Action::controller()
{
  return static_cast<Controller&>(Editor2D::Controller::Action::controller());
}


} // namespace ObjectEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework
