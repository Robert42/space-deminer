#include "shape.h"
#include "test-model.h"

namespace Framework {
namespace Editor {
namespace Base {
namespace ObjectEditor2D {


TEST(framework_editor_base_objecteditor2d_Shape_SimpleResizableRectangularShape, areas)
{
  TestModel testModel(vec2(10));
  Shape::Ptr shape = Shape::createSimpleResizableRectangularShape(10);
  Object::Ptr object(new Object);

  testModel.addObject(object, Rectangle<vec2>(vec2(0), vec2(100, 50)));

  EXPECT_EQ(Shape::HandleType::MOVE, shape->handleType(vec2(50, 25), object));
  EXPECT_EQ(Shape::HandleType::MOVE, shape->handleType(vec2(11, 11), object));

  EXPECT_EQ(Shape::HandleType::RESIZE_E, shape->handleType(vec2(99, 25), object));
  EXPECT_EQ(Shape::HandleType::RESIZE_W, shape->handleType(vec2(1, 25), object));
  EXPECT_EQ(Shape::HandleType::RESIZE_N, shape->handleType(vec2(50, 1), object));
  EXPECT_EQ(Shape::HandleType::RESIZE_S, shape->handleType(vec2(50, 49), object));

  EXPECT_EQ(Shape::HandleType::RESIZE_NW, shape->handleType(vec2(1, 1), object));
  EXPECT_EQ(Shape::HandleType::RESIZE_SE, shape->handleType(vec2(99, 49), object));
  EXPECT_EQ(Shape::HandleType::RESIZE_SW, shape->handleType(vec2(1, 49), object));
  EXPECT_EQ(Shape::HandleType::RESIZE_NE, shape->handleType(vec2(99, 1), object));

}


} // namespace ObjectEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework
