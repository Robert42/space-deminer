#include "order.h"

namespace Framework {
namespace Editor {
namespace Base {
namespace ObjectEditor2D {


Order::Order()
{
}


Order::~Order()
{
}


class SimpleOrder : public Order
{
  class SimpleIterator : public Order::Iterator
  {
  public:
    const QList<Object::Ptr> objects;
    const int end;
    int i;

    SimpleIterator(const QSet<Object::Ptr>& objects, int begin, int end)
      : objects(sortObjects(objects)),
        end(end),
        i(begin)
    {
    }

    bool hasNext() const override
    {
      return i != end;
    }

  private:
    static bool comparator(const Object::Ptr& a, const Object::Ptr& b)
    {
      vec2 pointA = a->boundingRect().centerPoint();
      vec2 pointB = b->boundingRect().centerPoint();

      QPair<real, real> pairA(pointA.y, pointA.x);
      QPair<real, real> pairB(pointB.y, pointB.x);

      return pairA < pairB;
    }

    static QList<Object::Ptr> sortObjects(const QSet<Object::Ptr>& objects)
    {
      QList<Object::Ptr> o = objects.values();
      qStableSort(o.begin(), o.end(), comparator);
      return o;
    }

  };
  class SimpleForwardIterator : public SimpleIterator
  {
  public:
    SimpleForwardIterator(const QSet<Object::Ptr>& objects)
      : SimpleIterator(objects, 0, objects.size())
    {
    }

    Object::Ptr nextImpl() override
    {
      return objects[i++];
    }
  };
  class SimpleBackwardIterator : public SimpleIterator
  {
  public:
    SimpleBackwardIterator(const QSet<Object::Ptr>& objects)
      : SimpleIterator(objects, objects.size()-1, -1)
    {
    }

    Object::Ptr nextImpl() override
    {
      return objects[i--];
    }
  };

  Iterator::Ptr upToDownIterator(const QSet<Object::Ptr>& objects)
  {
    return Iterator::Ptr(new SimpleBackwardIterator(objects));
  }

  Iterator::Ptr downToUpIterator(const QSet<Object::Ptr>& objects)
  {
    return Iterator::Ptr(new SimpleForwardIterator(objects));
  }
};


Order::Ptr Order::createSimpleOrder()
{
  return Ptr(new SimpleOrder);
}


Order::Iterator::Iterator()
{
}


Order::Iterator::~Iterator()
{
}


Object::Ptr Order::Iterator::next()
{
  if(hasNext())
    return nextImpl();

  assert(false);
  throw std::logic_error("Order::Iterator::next() called although hasNext() is false");
}


} // namespace ObjectEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework
