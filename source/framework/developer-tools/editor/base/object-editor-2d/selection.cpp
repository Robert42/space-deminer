#include "selection.h"

namespace Framework {
namespace Editor {
namespace Base {
namespace ObjectEditor2D {


Selection::Selection(const Model::Ptr& model)
  : _model(model)
{
  model->signalObjectWasRemoved().connect(std::bind(&Selection::handleRemovedObject, this, _1)).track(this->trackable);
}


Selection::~Selection()
{
}


Selection::Ptr Selection::create(const Model::Ptr& model)
{
  return Ptr(new Selection(model));
}


const Model::Ptr& Selection::model()
{
  return _model;
}


bool Selection::hasActiveObject() const
{
  return _activeObject!=nullptr;
}


Optional<Object::Ptr> Selection::activeObject()
{
  if(!hasActiveObject())
    return nothing;

  return _activeObject;
}


const QSet<Object::Ptr>& Selection::selectedObjects()
{
  return _selectedObjects;
}


void Selection::activateObject(const Object::Ptr& object)
{
  if(!canActivate(object))
    return;

  if(this->_activeObject != object)
  {
    this->_activeObject = object;
    _signalActiveObjectChanged();
  }
}


void Selection::selectObject(const Object::Ptr& object)
{
  assert(this->model()->allObjects().contains(object));

  if(!canSelect(object))
    return;

  if(!this->selectedObjects().contains(object))
  {
    this->_selectedObjects.insert(object);
    _signalSelectionChanged();
  }
}


void Selection::selectObjects(const QSet<Object::Ptr>& objects)
{
  int previousNumberSelected = this->selectedObjects().size();

  for(const Object::Ptr& o : objects)
  {
    assert(this->model()->allObjects().contains(o));

    if(canSelect(o))
      this->_selectedObjects.insert(o);
  }

  if(previousNumberSelected != this->selectedObjects().size())
    _signalSelectionChanged();
}


void Selection::setSelection(const QSet<Object::Ptr>& objects)
{
  int numberCommonObjects = 0;
  QSet<Object::Ptr> previousSelection;

  this->_selectedObjects.swap(previousSelection);

  for(const Object::Ptr& o : objects)
  {
    assert(this->model()->allObjects().contains(o));

    if(canSelect(o))
    {
      this->_selectedObjects.insert(o);
      if(previousSelection.contains(o))
        ++numberCommonObjects;
    }
  }

  if(numberCommonObjects != this->selectedObjects().size() || numberCommonObjects!=previousSelection.size())
    _signalSelectionChanged();
}


void Selection::unselectObject(const Object::Ptr& object)
{
  if(this->_selectedObjects.contains(object))
  {
    this->_selectedObjects.remove(object);

    _signalSelectionChanged();
  }
}


void Selection::unselectObjects(const QSet<Object::Ptr>& objects)
{
  int previousNumberSelected = this->selectedObjects().size();

  this->_selectedObjects -= objects;

  if(previousNumberSelected != this->selectedObjects().size())
    _signalSelectionChanged();
}


void Selection::deactivateObject()
{
  if(this->hasActiveObject())
  {
    this->_activeObject.reset();
    _signalActiveObjectChanged();
  }
}


void Selection::selectAll()
{
  selectObjects(model()->allObjects());
}


void Selection::unselectAll()
{
  if(!this->selectedObjects().empty())
  {
    this->_selectedObjects.clear();

    _signalSelectionChanged();
  }
}


void Selection::selectOrDeselectAll()
{
  if(selectedObjects().empty())
    selectAll();
  else
    unselectAll();
}


void Selection::invertSelection()
{
  if(model()->allObjects().empty())
  {
    assert(this->selectedObjects().empty());
    return;
  }

  QSet<Object::Ptr> invertedSelection = model()->allObjects() - this->selectedObjects();
  setSelection(invertedSelection);
}


bool Selection::active(const Object::Ptr& object) const
{
  return object == _activeObject;
}


bool Selection::contains(const Object::Ptr& object) const
{
  return _selectedObjects.contains(object);
}


Signals::Signal<void()>& Selection::signalActiveObjectChanged()
{
  return this->_signalActiveObjectChanged;
}


Signals::Signal<void()>& Selection::signalSelectionChanged()
{
  return this->_signalSelectionChanged;
}


void Selection::handleRemovedObject(const Object::Ptr& object)
{
  if(this->_activeObject == object)
    this->deactivateObject();

  if(this->selectedObjects().contains(object))
    this->unselectObject(object);
}


bool Selection::canSelect(const Object::Ptr& object)
{
  (void)object;
  return true;
}


bool Selection::canActivate(const Object::Ptr& object)
{
  (void)object;
  return true;
}


} // namespace ObjectEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework
