#ifndef FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_CONTROLLER_H
#define FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_CONTROLLER_H

#include "model.h"
#include "view.h"
#include "order.h"
#include "shape.h"
#include "selection.h"
#include "snapping-position.h"

#include <framework/developer-tools/editor/base/editor-2d/controller.h>

namespace Framework {
namespace Editor {
namespace Base {
namespace ObjectEditor2D {

class Controller : public Editor2D::Controller
{
public:
  typedef std::shared_ptr<Controller> Ptr;

  class Action : public Editor2D::Controller::Action
  {
  public:
    Signals::Trackable trackable;

    Action(Controller& controller);

    Controller& controller();
  };

  class CircleSelectionAction;
  class RectangleSelectionAction;
  class RectangleSelectionCrossAction;
  class TransformObjectPositionAction;
  class MoveObjectsAction;
  class ScaleObjectsPositionAction;
  class RotateObjectsPositionAction;
  class ResizeObjectByHandle;

private:
  const Order::Ptr _order;
  const Shape::Ptr _shape;
  const Selection::Ptr _selection;
  const SnappingPosition::Ptr _snappingPosition;

  real _selectionCircleRadius;

  Signals::CallableSignal<void(const vec2&)> _signalShowSelectionRectangle;
  Signals::CallableSignal<void()> _signalHideSelectionRectangle;
  Signals::CallableSignal<void()> _signalShowSelectionCircle;
  Signals::CallableSignal<void()> _signalHideSelectionCircle;
  Signals::CallableSignal<void()> _signalShowSelectionRectangleCrosslines;
  Signals::CallableSignal<void()> _signalHideSelectionRectangleCrosslines;
  Signals::CallableSignal<void(ScaleObjectsPositionAction&)> _signalShowScalingLine;
  Signals::CallableSignal<void(RotateObjectsPositionAction&)> _signalShowRotationLine;
  Signals::CallableSignal<void(real)> _signalSelectionCircleRadiusChanged;

public:
  Controller(const View::Ptr& view, const Order::Ptr& order, const Shape::Ptr& shape, const Selection::Ptr& selection, const SnappingPosition::Ptr& snappingPosition=SnappingPosition::createNoSnapping());
  ~Controller();

  static Ptr create(const View::Ptr& view, const Order::Ptr& order, const Shape::Ptr& shape, const Selection::Ptr& selection, const SnappingPosition::Ptr& snappingPosition=SnappingPosition::createNoSnapping());

  static std::string shortcutHelpText();

  Optional<Object::Ptr> objectAtScreenPoint(const vec2& screenPoint);

public:
  View::Ptr view();
  const Model::Ptr& model();
  const Order::Ptr& order();
  const Shape::Ptr& shape();
  const Selection::Ptr& selection();

  void changeSelectionCircleRadius(real direction);
  void setSelectionCircleRadius(real radius);
  real selectionCircleScreenspaceRadius() const;

  Signals::Signal<void(const vec2&)>& signalShowSelectionRectangle();
  Signals::Signal<void()>& signalHideSelectionRectangle();

  Signals::Signal<void()>& signalShowSelectionCircle();
  Signals::Signal<void()>& signalHideSelectionCircle();

  Signals::Signal<void()>& signalShowSelectionRectangleCrosslines();
  Signals::Signal<void()>& signalHideSelectionRectangleCrosslines();

  Signals::Signal<void(ScaleObjectsPositionAction&)>& signalShowScalingLine();
  Signals::Signal<void(RotateObjectsPositionAction&)>& signalShowRotationLine();

  Signals::Signal<void(real)>& signalSelectionCircleRadiusChanged();

  virtual real slowDownFactor() const;

  bool selectOrDeselectAll();
  bool selectAllObjects();
  bool unselectAllObjects();
  bool invertSelection();

  Optional<Object::Ptr> selectAndActivateObjectAtMouseCursor(bool deselectAllOther);

  std::shared_ptr<CircleSelectionAction> startSelectingObjectsWithCircle();
  std::shared_ptr<RectangleSelectionCrossAction> startRectangleSelectionCross();
  std::shared_ptr<RectangleSelectionAction> startRectangleSelection(bool selectMode);
  std::shared_ptr<MoveObjectsAction> startMovingObjects(const QSet<Object::Ptr>& objects);
  std::shared_ptr<ScaleObjectsPositionAction> startScaleObjectsPositionAction(const QSet<Object::Ptr>& objects);
  std::shared_ptr<RotateObjectsPositionAction> startRotateObjectsPositionAction(const QSet<Object::Ptr>& objects);
  std::shared_ptr<ResizeObjectByHandle> startResizingObjectByHandle(bool deselectAllOther, const Object::Ptr& object, Shape::HandleType handle);

private:
  void selectObjectInRectangleToMousePosition(const vec2& from, bool selectMode);
  void selectObjectsInCircle(bool selectMode);
};


class Controller::CircleSelectionAction : public Controller::Action
{
public:
  typedef std::shared_ptr<CircleSelectionAction> Ptr;

private:
  bool isSelecting, selectMode;

public:
  CircleSelectionAction(Controller& controller);

public:
  void startSelectingObjectInSelectionCircle(bool selectMode);
  void stopSelectingObjectInSelectionCircle();

private:
  bool handlePointerMovement() final override;
};

class Controller::RectangleSelectionAction : public Controller::Action
{
public:
  typedef std::shared_ptr<RectangleSelectionAction> Ptr;

public:
  RectangleSelectionAction(Controller& controller, bool selectMode);
};


class Controller::RectangleSelectionCrossAction : public Controller::Action
{
public:
  typedef std::shared_ptr<RectangleSelectionCrossAction> Ptr;

public:
  RectangleSelectionCrossAction(Controller& controller);
};


class Controller::TransformObjectPositionAction : public Controller::Action
{
public:
  typedef std::shared_ptr<TransformObjectPositionAction> Ptr;

protected:
  class Transformation
  {
  private:
    TransformObjectPositionAction* _action;
    Object::Ptr _object;
    vec2 _originalPosition;

  public:
    Transformation();
    Transformation(TransformObjectPositionAction* action, const Object::Ptr& object);

    const Object::Ptr& object();
    const vec2& originalPosition() const;

    void reset();
    void update();

  private:
    TransformObjectPositionAction& action();
  };

private:
  QVector<Transformation> transformations;
  bool _slowMode;
  vec2 _mousePosition;

public:
  const vec2 originalMousePosition;
  const vec2 originalCenterOfObjects;

public:
  TransformObjectPositionAction(const QSet<Object::Ptr>& objects, Controller& controller);

  void slowObjectMovement();
  void unslowObjectMovement();
  void resetObjectsBeingMoved();

  bool slowMode() const;

  const vec2& mousePosition() const;

protected:
  void reapplyMovement();

  static vec2 centerOfObjects(const QSet<Object::Ptr>& objects);

  virtual vec2 calculatePosition(Transformation& t) = 0;
  virtual vec2 snap(const vec2& originalPosition, const vec2& newPosition, const Object::Ptr& object);
  virtual void onSlow(){}
  virtual void onUnslow(){}

  virtual void mouseMoved(const vec2& relativeMovement) = 0;

private:
  void applyTransformation(Transformation& t);

  bool handlePointerMovement() final override;
};

class Controller::MoveObjectsAction : public Controller::TransformObjectPositionAction
{
public:
  typedef std::shared_ptr<MoveObjectsAction> Ptr;

  BEGIN_ENUMERATION(AxisSnapping,)
    NONE,
    X,
    Y
  ENUMERATION_BASIC_OPERATORS(AxisSnapping)
  END_ENUMERATION;

private:
  AxisSnapping _axisSnapping;
  vec2 mousePositionSlowedDown;

public:
  MoveObjectsAction(const QSet<Object::Ptr>& objects, Controller& controller);

  AxisSnapping axisSnapping() const;
  virtual void setAxisSnapping(AxisSnapping axisSnapping);

protected:
  void mouseMoved(const vec2& relativeMovement) final override;

  virtual vec2 transform(const Transformation& t, const vec2& mousePosition);

private:
  void onUnslow() final override;

  vec2 calculatePosition(Transformation& t) final override;

  vec2 axisSnappingFactor() const;

  vec2 snap(const vec2& originalPosition, const vec2& newPosition, const Object::Ptr& object) override;
};


class Controller::ScaleObjectsPositionAction : public Controller::MoveObjectsAction
{
public:
  typedef std::shared_ptr<ScaleObjectsPositionAction> Ptr;

public:
  ScaleObjectsPositionAction(const QSet<Object::Ptr>& objects, Controller& controller);

private:
  vec2 transform(const Transformation& t, const vec2& mousePosition) final override;
};


class Controller::RotateObjectsPositionAction : public Controller::TransformObjectPositionAction
{
public:
  typedef std::shared_ptr<RotateObjectsPositionAction> Ptr;

private:
  real angle, previousMouseAngle;

public:
  RotateObjectsPositionAction(const QSet<Object::Ptr>& objects, Controller& controller);

private:
  void mouseMoved(const vec2& relativeMovement) final override;

  vec2 calculatePosition(Transformation& t) final override;

  real directionToAngle(const vec2& direction, real fallback);
};


class Controller::ResizeObjectByHandle : public Controller::Action
{
public:
  typedef std::shared_ptr<ResizeObjectByHandle> Ptr;

private:
  const Object::Ptr object;
  const Rectangle<vec2> originalBoundingRect;
  const vec2 originalCursorPosition;
  const vec2 wnMask, esMask;

  vec2 totalOffset;

public:
  ResizeObjectByHandle(const Object::Ptr& object, Shape::HandleType handle, Controller& controller);

private:
  bool handlePointerMovement() final override;

  void reset();
  void setBoundingRect(const Rectangle<vec2>& boundingRect);
};



} // namespace ObjectEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework

#endif // FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_CONTROLLER_H
