#include "test-model.h"

namespace Framework {
namespace Editor {
namespace Base {
namespace ObjectEditor2D {


TestModel::TestModel(const vec2& gridSize)
  : Model(gridSize)
{
}

TestModel::Ptr TestModel::create(const vec2& gridSize)
{
  return Ptr(new TestModel(gridSize));
}

void TestModel::addObject(const Object::Ptr& object, const Rectangle<vec2>& boundingRectangle)
{
  Model::addObject(object, boundingRectangle);
}

void TestModel::removeObject(const Object::Ptr& object)
{
  Model::removeObject(object);
}

void TestModel::moveObject(const Object::Ptr& object, const Rectangle<vec2>& boundingRectangle)
{
  Model::moveObject(object, boundingRectangle);
}


} // namespace ObjectEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework
