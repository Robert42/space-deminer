#ifndef FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_SELECTION_H
#define FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_SELECTION_H

#include "model.h"

namespace Framework {
namespace Editor {
namespace Base {
namespace ObjectEditor2D {

class Selection
{
public:
  typedef std::shared_ptr<Selection> Ptr;

public:
  Signals::Trackable trackable;

private:
  Model::Ptr _model;

  Signals::CallableSignal<void()> _signalActiveObjectChanged;
  Signals::CallableSignal<void()> _signalSelectionChanged;

  QSet<Object::Ptr> _selectedObjects;
  Object::Ptr _activeObject;

public:
  Selection(const Model::Ptr& model);
  virtual ~Selection();

  static Ptr create(const Model::Ptr& model);

  const Model::Ptr& model();

  bool hasActiveObject() const;
  Optional<Object::Ptr> activeObject();

  const QSet<Object::Ptr>& selectedObjects();

  void activateObject(const Object::Ptr& object);
  void selectObject(const Object::Ptr& object);
  void selectObjects(const QSet<Object::Ptr>& objects);
  void setSelection(const QSet<Object::Ptr>& objects);

  void unselectObject(const Object::Ptr& object);
  void unselectObjects(const QSet<Object::Ptr>& object);
  void deactivateObject();

  void selectAll();
  void unselectAll();
  void selectOrDeselectAll();
  void invertSelection();

  bool active(const Object::Ptr& object) const;
  bool contains(const Object::Ptr& object) const;

  Signals::Signal<void()>& signalActiveObjectChanged();
  Signals::Signal<void()>& signalSelectionChanged();

private:
  void handleRemovedObject(const Object::Ptr& object);

  virtual bool canSelect(const Object::Ptr& object);
  virtual bool canActivate(const Object::Ptr& object);
};

} // namespace ObjectEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework

#endif // FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_SELECTION_H
