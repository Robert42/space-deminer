#include "model.h"

#include <base/io/log.h>

namespace Framework {
namespace Editor {
namespace Base {
namespace ObjectEditor2D {
namespace Private {
class ObjectModelGridCell
{
public:
  typedef std::shared_ptr<ObjectModelGridCell> Ptr;
  typedef QSet<Object::Ptr> ObjectSet;

  ObjectSet objects;
};
}

using namespace Private;

using IO::Log;


Model::Model(const vec2& gridSize)
  : GridEditor2D::Model<ObjectModelGridCell>(gridSize)
{
}


void Model::removeObject(const Object::Ptr& object)
{
  assertInvariant();

  if(object->_model!=this)
  {
    Log::logError("Model::removeObject: Trying to remove an object which is not handeled by this model");
    return;
  }

  QList<CellPtr> cells = cellsInRectangle(object->boundingRect());

  for(const CellPtr& cell : cells)
  {
    ObjectSet& objects = cell->objects;
    objects.remove(object);

    if(objects.isEmpty())
      removeCell(cell);
  }

  object->_model = nullptr;
  _allObjects.remove(object);

  if(isBoundingBoxChangingModelBoundingRect(object->boundingRect()))
    updateBoundingRect();

  assertInvariant();

  _signalObjectWasRemoved(object);
}


void Model::moveObject(const Object::Ptr& object, Rectangle<vec2> boundingRectangle)
{
  assertInvariant();

  if(object->_model!=this)
  {
    Log::logError("Model::removeObject: Trying to remove an object which is not handeled by this model");
    return;
  }

  boundingRectangle = constrainObjectMovement(boundingRectangle, object);

  if(boundingRectangle == object->boundingRect())
    return;

  QList<CellPtr> cells = cellsInRectangle(object->boundingRect());

  for(const CellPtr& cell : cells)
    cell->objects.remove(object);

  bool needFullBoundingRectUpdate = isBoundingBoxChangingModelBoundingRect(object->boundingRect());

  object->_boundingRect = boundingRectangle;

  for(const CellPtr& cell : findOrCreateCellsInRectangle(boundingRectangle))
    cell->objects.insert(object);

  for(const CellPtr& cell : cells)
  {
    if(cell->objects.isEmpty())
      removeCell(cell);
  }

  if(needFullBoundingRectUpdate)
    updateBoundingRect();
  else
    this->_boundingRect |= object->boundingRect();

  assertInvariant();

  _signalObjectWasMoved(object);
}


void Model::moveObjectByOffset(const Object::Ptr& object, const vec2& offset)
{
  moveObject(object, object->boundingRect() + offset);
}


void Model::moveObjectTo(const Object::Ptr& object, const vec2& position)
{
  moveObjectByOffset(object, position - object->originPosition());
}


vec2 Model::objectOriginPosition(const Object::Ptr& object) const
{
  return objectOriginPosition(*object);
}


vec2 Model::objectOriginPosition(const Object& object) const
{
  return object.boundingRect().min();
}


Rectangle<vec2> Model::constrainObjectMovement(const Rectangle<vec2>& newBoundingRect, const Object::Ptr& object)
{
  (void)object;
  return newBoundingRect;
}


Rectangle<vec2> Model::constrainNewObjectPlacement(const Rectangle<vec2>& newBoundingRect, const Object::Ptr& object)
{
  return constrainObjectMovement(newBoundingRect, object);
}


Signals::Signal<void(const Object::Ptr&)>& Model::signalObjectWasAdded()
{
  return _signalObjectWasAdded;
}


Signals::Signal<void(const Object::Ptr&)>& Model::signalObjectWasRemoved()
{
  return _signalObjectWasRemoved;
}


Signals::Signal<void(const Object::Ptr&)>& Model::signalObjectWasMoved()
{
  return _signalObjectWasMoved;
}


void Model::addObject(const Object::Ptr& object, const Rectangle<vec2>& boundingRectangle)
{
  assertInvariant();

  if(object->_model==nullptr)
  {
    object->_model = this;
    _allObjects.insert(object);
  }else if(object->_model!=this)
  {
    Log::logError("Model::addObject: Trying to add an object which is already handeled by another model");
    return;
  }else if(object->_model == this)
  {
    moveObject(object, boundingRectangle);
    return;
  }

  object->_boundingRect = constrainNewObjectPlacement(boundingRectangle, object);

  QList<CellPtr> cells = findOrCreateCellsInRectangle(boundingRectangle);

  for(const CellPtr& cell : cells)
  {
    ObjectSet& objects = cell->objects;
    objects.insert(object);
  }

  _boundingRect |= object->boundingRect();

  assertInvariant();

  _signalObjectWasAdded(object);
}


Model::ObjectSet Model::objectsInRectangle(const Rectangle<vec2>& rect)
{
  ObjectSet objects;
  QList<CellPtr> cells = cellsInRectangle(rect);

  for(const CellPtr& cell : cells)
  {
    for(const Object::Ptr& o : cell->objects)
    {
      if(rect.intersects(o->boundingRect()))
        objects.insert(o);
    }
  }

  return objects;
}


Model::ObjectSet Model::objectsAtPoint(const vec2& point)
{
  ObjectSet objects;
  Optional<CellPtr> cell = cellForPosition(point);

  if(cell)
  {
    for(const Object::Ptr& o : cell.value()->objects)
    {
      if(o->boundingRect().contains(point))
        objects.insert(o);
    }
  }

  return objects;
}


const Model::ObjectSet& Model::allObjects() const
{
  return _allObjects;
}


void Model::assertInvariant()
{
#ifndef NDEBUG
  assert(_boundingRect == calcBoundingRect());

  for(const CellPtr& cell : gridMap().values())
  {
    assert(!cell->objects.empty());
    for(const Object::Ptr& object : cell->objects)
    {
      allObjects().contains(object);
      assert(cellsInRectangle(object->boundingRect()).contains(cell));
    }
  }

  for(const Object::Ptr& object : allObjects())
  {
    for(const CellPtr& cell : cellsInRectangle(object->boundingRect()))
    {
      cell->objects.contains(object);
    }
  }
#endif
}


void Model::updateBoundingRect()
{
  _boundingRect = calcBoundingRect();
}


Editor2D::BoundingRect Model::calcBoundingRect() const
{
  Editor2D::BoundingRect boundingRect;

  for(const Object::Ptr& object : allObjects())
    boundingRect |= object->boundingRect();

  return boundingRect;
}


bool Model::isBoundingBoxChangingModelBoundingRect(const Rectangle<vec2>& rectangle) const
{
  if(this->boundingRect().empty())
    return true;

  real diagonal = rectangle.diagonalLength();

  if(diagonal == 0.f)
  {
    diagonal = this->boundingRect().rectangle().diagonalLength();
    if(diagonal == 0.f)
      return true;
  }

  return !this->boundingRect().rectangle().contains(rectangle,
                                                    diagonal*0.01f);
}


Model::CellPtr Model::createCell()
{
  return CellPtr(new Cell);
}


} // namespace ObjectEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework
