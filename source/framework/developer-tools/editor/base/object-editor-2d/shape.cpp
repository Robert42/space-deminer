#include "shape.h"

namespace Framework {
namespace Editor {
namespace Base {
namespace ObjectEditor2D {


Shape::Shape()
{
}


Shape::~Shape()
{
}


class SimpleRectangularShape : public Shape
{
public:
  bool isWithinShape(const vec2& point, const Object::Ptr& object) const override
  {
    return object->boundingRect().contains(point);
  }

  bool isIntersectingWith(const Circle& circle, const Object::Ptr& object) const override
  {
    return object->boundingRect().intersects(circle);
  }


  HandleType handleType(const vec2& point, const Object::Ptr& object) const override
  {
    (void)point;
    (void)object;
    return HandleType::NONE;
  }


  vec2 largestPossibleAdditionalObjectDecoration() const override
  {
    return vec2(0);
  }
};


class SimpleMovableRectangularShape : public SimpleRectangularShape
{
  HandleType handleType(const vec2& point, const Object::Ptr& object) const override
  {
    (void)point;
    (void)object;
    return HandleType::MOVE;
  }
};


class SimpleResizableRectangularShape : public SimpleRectangularShape
{
  const real borderWidth;

public:
  SimpleResizableRectangularShape(real borderWidth)
    : borderWidth(borderWidth)
  {
  }

private:
  HandleType handleType(const vec2& point, const Object::Ptr& object) const override
  {
    const Rectangle<vec2> boundingRect = object->boundingRect();
    const vec2 borderWidth = min(boundingRect.size()*0.5f, vec2(this->borderWidth));

    bool n = point.y <= boundingRect.min().y+borderWidth.y;
    bool w = point.x <= boundingRect.min().x+borderWidth.x;
    bool s = point.y >= boundingRect.max().y-borderWidth.y;
    bool e = point.x >= boundingRect.max().x-borderWidth.x;

    if(s && e)
      return HandleType::RESIZE_SE;
    else if(s && w)
      return HandleType::RESIZE_SW;
    else if(n && w)
      return HandleType::RESIZE_NW;
    else if(n && e)
      return HandleType::RESIZE_NE;
    else if(e)
      return HandleType::RESIZE_E;
    else if(s)
      return HandleType::RESIZE_S;
    else if(w)
      return HandleType::RESIZE_W;
    else if(n)
      return HandleType::RESIZE_N;

    return HandleType::MOVE;
  }
};


Shape::Ptr Shape::createSimpleRectangularShape()
{
  return Ptr(new SimpleRectangularShape);
}


Shape::Ptr Shape::createSimpleMovableRectangularShape()
{
  return Ptr(new SimpleMovableRectangularShape);
}


Shape::Ptr Shape::createSimpleResizableRectangularShape(real borderWidth)
{
  return Ptr(new SimpleResizableRectangularShape(borderWidth));
}


#define CASE(x) case Shape::x:return o << #x;
std::ostream& operator<<(std::ostream& o, Shape::HandleType handle)
{
  switch(handle.value)
  {
  CASE(HandleType::NONE)
  CASE(HandleType::SELECTION)
  CASE(HandleType::MOVE)
  CASE(HandleType::RESIZE_N)
  CASE(HandleType::RESIZE_W)
  CASE(HandleType::RESIZE_S)
  CASE(HandleType::RESIZE_E)
  CASE(HandleType::RESIZE_NE)
  CASE(HandleType::RESIZE_NW)
  CASE(HandleType::RESIZE_SW)
  CASE(HandleType::RESIZE_SE)
  }
  assert(false);
  return o;
}

void PrintTo(Shape::HandleType handle, std::ostream* o)
{
  *o << handle;
}


} // namespace ObjectEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework
