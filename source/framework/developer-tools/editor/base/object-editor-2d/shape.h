#ifndef FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_OUTLINESHAPE_H
#define FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_OUTLINESHAPE_H

#include "object.h"

#include <base/enum-macros.h>

namespace Framework {
namespace Editor {
namespace Base {
namespace ObjectEditor2D {

class Shape
{
public:
  typedef std::shared_ptr<Shape> Ptr;

  BEGIN_ENUMERATION(HandleType,)
    NONE,
    SELECTION,
    MOVE,
    RESIZE_N,
    RESIZE_W,
    RESIZE_S,
    RESIZE_E,
    RESIZE_NE,
    RESIZE_NW,
    RESIZE_SW,
    RESIZE_SE
  ENUMERATION_BASIC_OPERATORS(HandleType)
  END_ENUMERATION;

public:
  Shape();
  virtual ~Shape();

  static Ptr createSimpleRectangularShape();
  static Ptr createSimpleMovableRectangularShape();
  static Ptr createSimpleResizableRectangularShape(real borderWidth);

  virtual bool isWithinShape(const vec2& point, const Object::Ptr& object) const = 0;
  virtual bool isIntersectingWith(const Circle& circle, const Object::Ptr& object) const = 0;
  virtual HandleType handleType(const vec2& point, const Object::Ptr& object) const = 0;
  virtual vec2 largestPossibleAdditionalObjectDecoration() const = 0;
};


std::ostream& operator<<(std::ostream& o, Shape::HandleType handle);

void PrintTo(Shape::HandleType handle, std::ostream* o);


} // namespace ObjectEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework

#endif // FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_OUTLINESHAPE_H
