#include "snapping-position.h"

namespace Framework {
namespace Editor {
namespace Base {
namespace ObjectEditor2D {

SnappingPosition::SnappingPosition()
{
}

SnappingPosition::~SnappingPosition()
{
}

SnappingPosition::Ptr SnappingPosition::createSimple(const std::function<vec2(const vec2&, const vec2&, const Object::Ptr&)>& function)
{
  class SimpleSnappingPosition : public SnappingPosition
  {
  public:
    typedef std::function<vec2(const vec2&, const vec2&, const Object::Ptr&)> function_type;

  private:
    const function_type function;

  public:
    SimpleSnappingPosition(const function_type& function)
      : function(function)
    {
    }

    vec2 snap(const vec2& originalPosition, const vec2& newPosition, const Object::Ptr& object) override
    {
      return function(originalPosition, newPosition, object);
    }
  };

  return Ptr(new SimpleSnappingPosition(function));
}

SnappingPosition::Ptr SnappingPosition::createVerySimple(const std::function<vec2(const vec2&)>& function)
{
  return createSimple([function](const vec2&, const vec2& position, const Object::Ptr&){
    return function(position);
  });
}

SnappingPosition::Ptr SnappingPosition::createNoSnapping()
{
  class NoSnappingPosition : public SnappingPosition
  {
  public:
    NoSnappingPosition()
    {
    }

    vec2 snap(const vec2& originalPosition, const vec2& newPosition, const Object::Ptr& object) override
    {
      (void)originalPosition;
      (void)object;
      return newPosition;
    }
  };

  return Ptr(new NoSnappingPosition);
}

} // namespace ObjectEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework
