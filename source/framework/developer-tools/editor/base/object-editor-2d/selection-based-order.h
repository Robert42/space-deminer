#ifndef FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_SELECTIONBASEDORDER_H
#define FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_SELECTIONBASEDORDER_H

#include "order.h"
#include "selection.h"

namespace Framework {
namespace Editor {
namespace Base {
namespace ObjectEditor2D {

class SelectionBasedOrder : public Order
{
public:
  typedef std::shared_ptr<SelectionBasedOrder> Ptr;

private:
  Signals::Trackable trackable;
  Selection::Ptr selection;

  bool _dirty;

  QVector<Object::Ptr> objects;

public:
  SelectionBasedOrder(const Selection::Ptr& selection);

public:
  static Ptr create(const Selection::Ptr& selection);

protected:
  virtual bool enforeLessThan(const Object::Ptr& a, const Object::Ptr& b);

  Iterator::Ptr upToDownIterator(const QSet<Object::Ptr>& objects) override;
  Iterator::Ptr downToUpIterator(const QSet<Object::Ptr>& objects) override;

private:
  void update();
  void markDirty();

  void addObject(const Object::Ptr& object);
  void removeObject(const Object::Ptr& object);
};

} // namespace ObjectEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework

#endif // FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_SELECTIONBASEDORDER_H
