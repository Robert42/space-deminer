#ifndef FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_OBJECT_H
#define FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_OBJECT_H

#include <dependencies.h>

#include <base/geometry/rectangle.h>

namespace Framework {

using namespace Base;

namespace Editor {
namespace Base {
namespace ObjectEditor2D {

class Model;

class Object : public std::enable_shared_from_this<Object>, public noncopyable
{
  friend class Model;
public:
  typedef std::shared_ptr<Object> Ptr;

private:
  Model* _model;

  Rectangle<vec2> _boundingRect;

public:
  Object();
  virtual ~Object();

public:
  const Model& model() const;
  Model& model();

  const Rectangle<vec2>& boundingRect() const;

  vec2 originPosition() const;
};

} // namespace ObjectEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework

#endif // FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_OBJECT_H
