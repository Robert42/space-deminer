#include "model.h"
#include "test-model.h"

namespace Framework {
namespace Editor {
namespace Base {
namespace ObjectEditor2D {

TEST(framework_editor_base_objecteditor2d_Model, objectsInRectangle)
{
  TestModel model(vec2(100));

  EXPECT_TRUE(model.objectsInRectangle(Rectangle<vec2>(vec2(-1000), vec2(1000))).empty());

  Object::Ptr o(new Object());

  model.addObject(o, Rectangle<vec2>(vec2(120, 210), vec2(599, 250)));

  QSet<Object::Ptr> objects = model.objectsInRectangle(Rectangle<vec2>(vec2(-1000), vec2(1000)));
  EXPECT_EQ(1, objects.size());
  EXPECT_TRUE(objects.contains(o));

  objects = model.objectsInRectangle(Rectangle<vec2>(vec2(0), vec2(119, 209)));
  EXPECT_TRUE(objects.empty());

  objects = model.objectsInRectangle(Rectangle<vec2>(vec2(0), vec2(120.1, 210.1)));
  EXPECT_FALSE(objects.empty());

  model.removeObject(o);
}

TEST(framework_editor_base_objecteditor2d_Model, objectsAtPoint)
{
  TestModel model(vec2(100));

  EXPECT_TRUE(model.objectsAtPoint(vec2(200,240)).empty());

  Object::Ptr o(new Object());

  model.addObject(o, Rectangle<vec2>(vec2(120, 210), vec2(599, 250)));

  QSet<Object::Ptr> objects = model.objectsAtPoint(vec2(200, 240));
  EXPECT_EQ(1, objects.size());
  EXPECT_TRUE(objects.contains(o));

  objects = model.objectsAtPoint(vec2(119, 209));
  EXPECT_TRUE(objects.empty());

  model.removeObject(o);
}

TEST(framework_editor_base_objecteditor2d_Model, addObject)
{
  TestModel model(vec2(100));

  EXPECT_TRUE(model.boundingRect().empty());

  Object::Ptr o1(new Object());

  model.addObject(o1, Rectangle<vec2>(vec2(120, 210), vec2(599, 250)));

  EXPECT_EQ(Rectangle<vec2>(vec2(120, 210), vec2(599, 250)), model.boundingRect().rectangle());

  Object::Ptr o2(new Object());
  model.addObject(o2, Rectangle<vec2>(vec2(301, 245), vec2(905, 401)));

  EXPECT_FALSE(model.boundingRect().empty());
  EXPECT_EQ(Rectangle<vec2>(vec2(120, 210), vec2(905, 401)), model.boundingRect().rectangle());

  QSet<Object::Ptr> objects = model.objectsAtPoint(vec2(200, 240));
  EXPECT_EQ(1, objects.size());
  EXPECT_TRUE(objects.contains(o1));
  EXPECT_FALSE(objects.contains(o2));

  objects = model.objectsAtPoint(vec2(302, 249));
  EXPECT_EQ(2, objects.size());
  EXPECT_TRUE(objects.contains(o1));
  EXPECT_TRUE(objects.contains(o2));

  objects = model.objectsAtPoint(vec2(302, 251));
  EXPECT_EQ(1, objects.size());
  EXPECT_FALSE(objects.contains(o1));
  EXPECT_TRUE(objects.contains(o2));

  EXPECT_FALSE(model.boundingRect().empty());
}

TEST(framework_editor_base_objecteditor2d_Model, removeObject)
{
  TestModel model(vec2(100));

  Object::Ptr o1(new Object());
  Object::Ptr o2(new Object());

  model.addObject(o1, Rectangle<vec2>(vec2(120, 210), vec2(599, 250)));
  model.addObject(o2, Rectangle<vec2>(vec2(301, 245), vec2(905, 401)));

  EXPECT_FALSE(model.boundingRect().empty());
  EXPECT_EQ(Rectangle<vec2>(vec2(120, 210), vec2(905, 401)), model.boundingRect().rectangle());

  QSet<Object::Ptr> objects = model.objectsAtPoint(vec2(302, 249));
  EXPECT_EQ(2, objects.size());
  EXPECT_TRUE(objects.contains(o1));
  EXPECT_TRUE(objects.contains(o2));

  model.removeObject(o1);

  objects = model.objectsAtPoint(vec2(302, 249));
  EXPECT_EQ(1, objects.size());
  EXPECT_FALSE(objects.contains(o1));
  EXPECT_TRUE(objects.contains(o2));

  EXPECT_FALSE(model.boundingRect().empty());
  EXPECT_EQ(Rectangle<vec2>(vec2(301, 245), vec2(905, 401)), model.boundingRect().rectangle());

  model.removeObject(o2);

  objects = model.objectsAtPoint(vec2(302, 249));
  EXPECT_TRUE(objects.empty());

  EXPECT_TRUE(model.boundingRect().empty());
}

TEST(framework_editor_base_objecteditor2d_Model, moveObject)
{
  TestModel model(vec2(1));

  Object::Ptr o1(new Object());
  Object::Ptr o2(new Object());
  Object::Ptr o3(new Object());

  model.addObject(o1, Rectangle<vec2>(vec2(1, 2), vec2(3, 3)));
  model.addObject(o2, Rectangle<vec2>(vec2(2, 1), vec2(4, 4)));
  model.addObject(o3, Rectangle<vec2>(vec2(-1), vec2(-2)));

  EXPECT_FALSE(model.boundingRect().empty());
  EXPECT_EQ(Rectangle<vec2>(vec2(-2), vec2(4)), model.boundingRect().rectangle());

  model.moveObject(o3, Rectangle<vec2>(vec2(2), vec2(3)));

  EXPECT_EQ(Rectangle<vec2>(vec2(2), vec2(3)), o3->boundingRect());
  EXPECT_FALSE(model.boundingRect().empty());
  EXPECT_EQ(Rectangle<vec2>(vec2(1), vec2(4)), model.boundingRect().rectangle());

  model.moveObject(o2, Rectangle<vec2>(vec2(1, 2), vec2(2, 3)));

  EXPECT_EQ(Rectangle<vec2>(vec2(1, 2), vec2(2, 3)), o2->boundingRect());
  EXPECT_FALSE(model.boundingRect().empty());
  EXPECT_EQ(Rectangle<vec2>(vec2(1, 2), vec2(3, 3)), model.boundingRect().rectangle());
}


} // namespace ObjectEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework
