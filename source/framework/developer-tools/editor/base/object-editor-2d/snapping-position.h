#ifndef FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_SNAPPINGPOSITION_H
#define FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_SNAPPINGPOSITION_H

#include <dependencies.h>

#include "object.h"

namespace Framework {

using namespace Base;

namespace Editor {
namespace Base {
namespace ObjectEditor2D {

class SnappingPosition
{
public:
  typedef std::shared_ptr<SnappingPosition> Ptr;

  SnappingPosition();
  virtual ~SnappingPosition();

  /**
   * @param originalPosition the position the object had before starting the transformation.
   *        Because of snapping/some constraints and also, because a part of transformation is already done,
   *        the current position of the object probably will be completly different to this value.
   * @param newPosition the next position the object would be given, if there was no snapping.
   * @param object
   *
   * @return the snapped new position
   */
  virtual vec2 snap(const vec2& originalPosition, const vec2& newPosition, const Object::Ptr& object) = 0;

  static Ptr createSimple(const std::function<vec2(const vec2&, const vec2&, const Object::Ptr&)>& function);
  static Ptr createVerySimple(const std::function<vec2(const vec2&)>& function);
  static Ptr createNoSnapping();
};

} // namespace ObjectEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework

#endif // FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_SNAPPINGPOSITION_H
