#include "view.h"

namespace Framework {
namespace Editor {
namespace Base {
namespace ObjectEditor2D {

View::View(const Model::Ptr& objectModel)
  : Editor2D::View(objectModel)
{
}


View::Ptr View::create(const Model::Ptr& objectModel)
{
  return View::Ptr(new View(objectModel));
}


QSet<Object::Ptr> View::findObjectsInView() const
{
  return model()->objectsInRectangle(viewportInModelSpace());
}


QSet<Object::Ptr> View::findObjectsInViewWithExtension(const vec2& extension) const
{
  vec2 e = abs(extension);
  return model()->objectsInRectangle(transformFromScreenToModelSpace(Rectangle<vec2>(-e, viewportSize()+e)));
}


QSet<Object::Ptr> View::findObjectsInScreenspaceRectangle(const Rectangle<vec2>& rect) const
{
  return model()->objectsInRectangle(transformFromScreenToModelSpace(rect));
}


Model::Ptr View::model() const
{
  return std::static_pointer_cast<Model>(Editor2D::View::model);
}


} // namespace ObjectEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework
