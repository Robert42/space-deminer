#ifndef FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_MODEL_H
#define FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_MODEL_H

#include "object.h"

#include <framework/developer-tools/editor/base/grid-editor-2d/model.h>

namespace Framework {
namespace Editor {
namespace Base {
namespace ObjectEditor2D {
namespace Private {
class ObjectModelGridCell;
}


class Model : public GridEditor2D::Model<Private::ObjectModelGridCell>
{
public:
  typedef std::shared_ptr<Model> Ptr;
  typedef QSet<Object::Ptr> ObjectSet;

private:
  typedef Private::ObjectModelGridCell Cell;
  typedef std::shared_ptr<Cell> CellPtr;

private:
  ObjectSet _allObjects;

  Signals::CallableSignal<void(const Object::Ptr&)> _signalObjectWasAdded;
  Signals::CallableSignal<void(const Object::Ptr&)> _signalObjectWasRemoved;
  Signals::CallableSignal<void(const Object::Ptr&)> _signalObjectWasMoved;

public:
  Model(const vec2& gridSize);

  ObjectSet objectsInRectangle(const Rectangle<vec2>& rect);
  ObjectSet objectsAtPoint(const vec2& point);

  const ObjectSet& allObjects() const;

public:
  Signals::Signal<void(const Object::Ptr&)>& signalObjectWasAdded();
  Signals::Signal<void(const Object::Ptr&)>& signalObjectWasRemoved();
  Signals::Signal<void(const Object::Ptr&)>& signalObjectWasMoved();

protected:
  friend class Controller;
  friend vec2 Object::originPosition()const;

  void addObject(const Object::Ptr& object, const Rectangle<vec2>& boundingRectangle);
  void removeObject(const Object::Ptr& object);
  void moveObject(const Object::Ptr& object, Rectangle<vec2> boundingRectangle);
  void moveObjectByOffset(const Object::Ptr& object, const vec2& offset);
  void moveObjectTo(const Object::Ptr& object, const vec2& position);

  vec2 objectOriginPosition(const Object::Ptr& object) const;
  virtual vec2 objectOriginPosition(const Object& object) const;
  virtual Rectangle<vec2> constrainObjectMovement(const Rectangle<vec2>& newBoundingRect, const Object::Ptr& object);
  virtual Rectangle<vec2> constrainNewObjectPlacement(const Rectangle<vec2>& newBoundingRect, const Object::Ptr& object);

private:
  void assertInvariant();

  void updateBoundingRect();
  Editor2D::BoundingRect calcBoundingRect() const;

  bool isBoundingBoxChangingModelBoundingRect(const Rectangle<vec2>& rectangle) const;

private:
  CellPtr createCell() override;
};

} // namespace ObjectEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework

#endif // FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_MODEL_H
