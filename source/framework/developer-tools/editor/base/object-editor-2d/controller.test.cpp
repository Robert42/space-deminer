#include "controller.h"
#include "test-model.h"
#include "snapping-position.h"

#include <framework/window/user-input/test-input-devices/test-mouse.h>
#include <framework/window/user-input/test-input-devices/test-keyboard.h>

#include <framework/developer-tools/editor/blender-style-input/object-editor-2d/input-handler.h>

namespace Framework {
namespace Editor {
namespace Base {
namespace ObjectEditor2D {

class ControllerTestFrame
{
public:
  TestMouse mouse;
  TestKeyboard keyboard;

  TestModel::Ptr model;
  View::Ptr view;
  Shape::Ptr shape;
  Order::Ptr order;
  Selection::Ptr selection;
  SnappingPosition::Ptr snappingPosition;
  Controller::Ptr controller;
  BlenderStyleInput::ObjectEditor2D::InputHandler::Ptr inputHandler;

  ControllerTestFrame(bool initManually=false)
  {
    if(!initManually)
      init();
  }

  void setShape_Movement()
  {
    shape = Shape::createSimpleMovableRectangularShape();
  }

  void setShape_Resizable(real borderSize=8)
  {
    shape = Shape::createSimpleResizableRectangularShape(borderSize);
  }

  void setSnapping_ForceY(real y)
  {
    snappingPosition = SnappingPosition::createVerySimple([y](const vec2& v){return vec2(v.x, y);});
  }

  void init()
  {
    if(!model)
      model = TestModel::create(vec2(100));
    if(!view)
      view = View::create(model);
    if(!shape)
      shape = Shape::createSimpleRectangularShape();
    if(!order)
      order = Order::createSimpleOrder();
    if(!selection)
      selection = Selection::create(model);
    if(!snappingPosition)
      snappingPosition = SnappingPosition::createNoSnapping();
    if(!controller)
      controller = Controller::create(view, order, shape, selection, snappingPosition);
    inputHandler = BlenderStyleInput::ObjectEditor2D::InputHandler::create(controller, InputDevice::ActionPriority::GUI, InputDevice::ActionPriority::GUI_BLOCKER);

    view->setViewportSize(vec2(1024));
    view->fitToModel();

    // To get the test more likely to fail, try an exotic offset
    inputHandler->setViewportOffset(vec2(-10000, 1000));
    // The mouse has to be within the viewport, otherwise all shortcuts are ignored
    moveMouseToModel(vec2(16));
  }

  QList<Object::Ptr> createSomeObjects(bool skip2and3=false)
  {
    /**
     *           1024x1024
     * +-----------+----------------------------+
     * |   0       |                            |
     * |           |                            |
     * +-----------+                            |
     * |                                        |
     * |      +----------------------+          |
     * |      |                      |          |
     * |      |      1               |          |
     * |      |                      |          |
     * |      |                      |          |
     * |  +--------+                 |          |
     * |  |   |    |           +--------+       |
     * |  |   +----|-----------|-----+  |       |
     * |  | 2      |           |  3     |       |
     * |  +--------+           +--------+ +-----+
     * |                                  |  4  |
     * +----------------------------------+-----+
     *
     */

    QList<Object::Ptr> objects;

    Object::Ptr o(new Object);
    objects.append(o);
    this->model->addObject(o, Rectangle<vec2>(vec2(0), vec2(210, 70)));

    o = Object::Ptr(new Object);
    objects.append(o);
    this->model->addObject(o, Rectangle<vec2>(vec2(160, 256), vec2(160, 256)+vec2(640, 480)));

    if(!skip2and3)
    {
      o = Object::Ptr(new Object);
      objects.append(o);
      this->model->addObject(o, Rectangle<vec2>(vec2(96, 640), vec2(96, 640)+vec2(320, 160)));

      o = Object::Ptr(new Object);
      objects.append(o);
      this->model->addObject(o, Rectangle<vec2>(vec2(560, 590), vec2(560, 590)+vec2(320, 160)));
    }

    o = Object::Ptr(new Object);
    objects.append(o);
    this->model->addObject(o, Rectangle<vec2>(vec2(1000), vec2(1000)+vec2(24)));

    this->view->setViewportSize(vec2(512, 512));
    this->view->fitToModel();

    moveMouseToModel(vec2(16));

    return objects;
  }

  ~ControllerTestFrame()
  {
    controller.reset();
    selection.reset();
    order.reset();
    shape.reset();
    view.reset();
    model.reset();
  }

  void moveMouseToModel(const vec2& modelPosition)
  {
    ivec2 screenPosition = ivec2(round(view->transformFromModelToScreenSpace(modelPosition) + inputHandler->viewportOffset()));

    mouse.moveTo(screenPosition);
  }

  void moveMouseInModelSpace(const vec2& modelSpaceDistance)
  {
    ivec2 modelspaceDistance = ivec2(round(view->transformDirectionFromModelToScreenSpace(modelSpaceDistance)));

    mouse.move(modelspaceDistance);
  }
};

TEST(framework_editor_base_objecteditor2d_Controller, selectAllUsingKeyShortcuts)
{
  ControllerTestFrame frame;
  QList<Object::Ptr> objects = frame.createSomeObjects();

  EXPECT_EQ(0, frame.selection->selectedObjects().size());

  frame.selection->selectObject(objects.first());

  EXPECT_EQ(1, frame.selection->selectedObjects().size());

  frame.keyboard.press(KeyCode::A);
  frame.keyboard.release(KeyCode::A);

  EXPECT_EQ(0, frame.selection->selectedObjects().size());

  frame.keyboard.press(KeyCode::A);
  frame.keyboard.release(KeyCode::A);

  EXPECT_EQ(5, frame.selection->selectedObjects().size());

  frame.keyboard.press(KeyCode::A);
  frame.keyboard.release(KeyCode::A);

  EXPECT_EQ(0, frame.selection->selectedObjects().size());
}

TEST(framework_editor_base_objecteditor2d_Controller, invertSelection)
{
  ControllerTestFrame frame;

  EXPECT_EQ(0, frame.selection->selectedObjects().size());
  frame.selection->invertSelection();
  EXPECT_EQ(0, frame.selection->selectedObjects().size());

  QList<Object::Ptr> objects = frame.createSomeObjects();

  EXPECT_EQ(0, frame.selection->selectedObjects().size());

  frame.selection->selectObject(objects.first());

  EXPECT_EQ(1, frame.selection->selectedObjects().size());
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects.first()));

  frame.keyboard.press(KeyCode::CONTROL_LEFT);
  frame.keyboard.press(KeyCode::I);
  frame.keyboard.release(KeyCode::I);
  frame.keyboard.release(KeyCode::CONTROL_LEFT);

  EXPECT_EQ(4, frame.selection->selectedObjects().size());
  EXPECT_FALSE(frame.selection->selectedObjects().contains(objects.first()));

  frame.keyboard.press(KeyCode::CONTROL_LEFT);
  frame.keyboard.press(KeyCode::I);
  frame.keyboard.release(KeyCode::I);
  frame.keyboard.release(KeyCode::CONTROL_LEFT);

  EXPECT_EQ(1, frame.selection->selectedObjects().size());
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects.first()));

  frame.selection->unselectAll();
  frame.selection->invertSelection();
  EXPECT_EQ(5, frame.selection->selectedObjects().size());
  frame.selection->invertSelection();
  EXPECT_EQ(0, frame.selection->selectedObjects().size());
}

TEST(framework_editor_base_objecteditor2d_Controller, selectByClick)
{
  Optional<Object::Ptr> optionalActiveObject;
  Object::Ptr activeObject;
  ControllerTestFrame frame;
  QList<Object::Ptr> objects = frame.createSomeObjects();

  frame.moveMouseToModel(objects[1]->boundingRect().min()-vec2(2, 2));
  frame.mouse.press(MouseButton::LEFT);
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_EQ(0, frame.selection->selectedObjects().size());
  EXPECT_FALSE(frame.selection->hasActiveObject());

  frame.moveMouseToModel(objects[1]->boundingRect().min()+vec2(2, 2));
  frame.mouse.press(MouseButton::LEFT);
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_EQ(1, frame.selection->selectedObjects().size());
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[1]));
  ASSERT_TRUE(optionalActiveObject = frame.selection->activeObject());
  activeObject = *optionalActiveObject;
  EXPECT_EQ(objects[1], activeObject);

  frame.moveMouseToModel(objects[0]->boundingRect().min()+vec2(2, 2));
  frame.mouse.press(MouseButton::RIGHT);
  frame.mouse.release(MouseButton::RIGHT);

  EXPECT_EQ(1, frame.selection->selectedObjects().size());
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[0]));
  ASSERT_TRUE(optionalActiveObject = frame.selection->activeObject());
  activeObject = *optionalActiveObject;
  EXPECT_EQ(objects[0], activeObject);

  frame.moveMouseToModel(objects[1]->boundingRect().min()-vec2(2, 2));
  frame.mouse.press(MouseButton::LEFT);
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_EQ(1, frame.selection->selectedObjects().size());
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[0]));
  ASSERT_TRUE(optionalActiveObject = frame.selection->activeObject());
  activeObject = *optionalActiveObject;
  EXPECT_EQ(objects[0], activeObject);

  frame.keyboard.press(KeyCode::A);
  frame.keyboard.release(KeyCode::A);

  EXPECT_EQ(0, frame.selection->selectedObjects().size());

  frame.keyboard.press(KeyCode::A);
  frame.keyboard.release(KeyCode::A);

  EXPECT_EQ(5, frame.selection->selectedObjects().size());

  frame.moveMouseToModel(objects[1]->boundingRect().min()+vec2(2, 2));
  frame.mouse.press(MouseButton::LEFT);
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_EQ(1, frame.selection->selectedObjects().size());
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[1]));
  ASSERT_TRUE(optionalActiveObject = frame.selection->activeObject());
  activeObject = *optionalActiveObject;
  EXPECT_EQ(objects[1], activeObject);
}

TEST(framework_editor_base_objecteditor2d_Controller, selectMultipleByClick)
{
  Optional<Object::Ptr> optionalActiveObject;
  Object::Ptr activeObject;
  ControllerTestFrame frame;
  QList<Object::Ptr> objects = frame.createSomeObjects();

  frame.keyboard.press(KeyCode::SHIFT_LEFT);

  frame.moveMouseToModel(objects[1]->boundingRect().min()+vec2(2, 2));
  frame.mouse.press(MouseButton::LEFT);
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_EQ(1, frame.selection->selectedObjects().size());
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[1]));
  ASSERT_TRUE(optionalActiveObject = frame.selection->activeObject());
  activeObject = *optionalActiveObject;
  EXPECT_EQ(objects[1], activeObject);

  frame.moveMouseToModel(objects[0]->boundingRect().min()+vec2(2, 2));
  frame.mouse.press(MouseButton::LEFT);
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_EQ(2, frame.selection->selectedObjects().size());
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[0]));
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[1]));
  ASSERT_TRUE(optionalActiveObject = frame.selection->activeObject());
  activeObject = *optionalActiveObject;
  EXPECT_EQ(objects[0], activeObject);

  frame.moveMouseToModel(objects[2]->boundingRect().min()+vec2(2, 2));
  frame.mouse.press(MouseButton::RIGHT);
  frame.mouse.release(MouseButton::RIGHT);

  EXPECT_EQ(3, frame.selection->selectedObjects().size());
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[0]));
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[1]));
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[2]));
  ASSERT_TRUE(optionalActiveObject = frame.selection->activeObject());
  activeObject = *optionalActiveObject;
  EXPECT_EQ(objects[2], activeObject);
}

TEST(framework_editor_base_objecteditor2d_Controller, selectRMBRectangle)
{
  ControllerTestFrame frame;
  QList<Object::Ptr> objects = frame.createSomeObjects();

  frame.moveMouseToModel(objects[1]->boundingRect().min()-vec2(2, 2));
  frame.mouse.press(MouseButton::RIGHT);
  frame.mouse.release(MouseButton::RIGHT);

  EXPECT_TRUE(frame.selection->selectedObjects().empty());

  frame.moveMouseToModel(objects[1]->boundingRect().min()-vec2(2, 2));
  frame.mouse.press(MouseButton::RIGHT);
  frame.moveMouseToModel(objects[1]->boundingRect().max());
  frame.keyboard.press(KeyCode::ESCAPE);
  frame.keyboard.release(KeyCode::ESCAPE);
  frame.mouse.release(MouseButton::RIGHT);

  EXPECT_TRUE(frame.selection->selectedObjects().empty());

  frame.moveMouseToModel(objects[1]->boundingRect().min()-vec2(2, 2));
  frame.mouse.press(MouseButton::RIGHT);
  frame.moveMouseToModel(objects[1]->boundingRect().max());
  frame.mouse.press(MouseButton::LEFT);
  frame.mouse.release(MouseButton::LEFT);
  frame.mouse.release(MouseButton::RIGHT);

  EXPECT_TRUE(frame.selection->selectedObjects().empty());

  frame.moveMouseToModel(objects[1]->boundingRect().min()-vec2(2, 2));
  frame.mouse.press(MouseButton::RIGHT);
  frame.moveMouseToModel(objects[1]->boundingRect().max());
  frame.mouse.press(MouseButton::MIDDLE);
  frame.mouse.release(MouseButton::MIDDLE);
  frame.mouse.release(MouseButton::RIGHT);

  EXPECT_TRUE(frame.selection->selectedObjects().empty());

  frame.moveMouseToModel(objects[1]->boundingRect().min()-vec2(2, 2));
  frame.mouse.press(MouseButton::RIGHT);
  frame.moveMouseToModel(objects[1]->boundingRect().max());
  frame.mouse.release(MouseButton::RIGHT);

  EXPECT_EQ(3, frame.selection->selectedObjects().size());
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[1]));
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[2]));
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[3]));
}

TEST(framework_editor_base_objecteditor2d_Controller, selectRMBRectangle_CoordinateSystem)
{
  ControllerTestFrame frame;
  QList<Object::Ptr> objects = frame.createSomeObjects();

  frame.moveMouseToModel(objects[0]->boundingRect().max()+vec2(2));
  frame.mouse.press(MouseButton::RIGHT);
  frame.moveMouseToModel(objects[1]->boundingRect().min()-vec2(2));
  frame.mouse.release(MouseButton::RIGHT);

  EXPECT_TRUE(frame.selection->selectedObjects().empty());
}

TEST(framework_editor_base_objecteditor2d_Controller, selectWithCircle)
{
  ControllerTestFrame frame;
  QList<Object::Ptr> objects = frame.createSomeObjects();

  // With the current zoom-factor, the radius in the model-space will be 10
  frame.controller->setSelectionCircleRadius(5.f);

  frame.keyboard.press(KeyCode::C);

  // No Object should be selected at this position, as there's no intersection with the circle
  // (although the bounding rectangle of the circle is intersecting)
  frame.moveMouseToModel(objects[1]->boundingRect().min()-vec2(8, 8));

  EXPECT_TRUE(frame.selection->selectedObjects().empty());

  frame.mouse.press(MouseButton::LEFT);
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_TRUE(frame.selection->selectedObjects().empty());

  // The Object should be selected, the almost no change to the previous position
  frame.moveMouseToModel(objects[1]->boundingRect().min()-vec2(6, 6));

  EXPECT_TRUE(frame.selection->selectedObjects().empty());

  frame.mouse.press(MouseButton::LEFT);
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_FALSE(frame.selection->selectedObjects().empty());
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[1]));

  frame.mouse.press(MouseButton::MIDDLE);
  frame.mouse.release(MouseButton::MIDDLE);

  EXPECT_TRUE(frame.selection->selectedObjects().empty());

  frame.mouse.press(MouseButton::RIGHT);
  frame.mouse.release(MouseButton::RIGHT);

  frame.mouse.press(MouseButton::LEFT);
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_TRUE(frame.selection->selectedObjects().empty());
}

TEST(framework_editor_base_objecteditor2d_Controller, selectWithCircle_MultipleWithOneStroke)
{
  ControllerTestFrame frame;
  QList<Object::Ptr> objects = frame.createSomeObjects();

  frame.controller->setSelectionCircleRadius(2.f);

  frame.keyboard.press(KeyCode::C);
  frame.moveMouseToModel(objects[1]->boundingRect().min());

  EXPECT_TRUE(frame.selection->selectedObjects().empty());

  frame.mouse.press(MouseButton::LEFT);

  EXPECT_EQ(1, frame.selection->selectedObjects().size());
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[1]));

  frame.moveMouseToModel(objects[0]->boundingRect().min());

  EXPECT_EQ(2, frame.selection->selectedObjects().size());
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[0]));
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[1]));

  frame.moveMouseToModel(objects[2]->boundingRect().min());

  EXPECT_EQ(3, frame.selection->selectedObjects().size());
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[0]));
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[1]));
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[2]));

  frame.mouse.release(MouseButton::LEFT);

  frame.moveMouseToModel(objects[3]->boundingRect().max());

  EXPECT_EQ(3, frame.selection->selectedObjects().size());
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[0]));
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[1]));
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[2]));

  frame.mouse.press(MouseButton::MIDDLE);

  EXPECT_EQ(3, frame.selection->selectedObjects().size());
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[0]));
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[1]));
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[2]));

  frame.moveMouseToModel(objects[2]->boundingRect().min());

  EXPECT_EQ(2, frame.selection->selectedObjects().size());
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[0]));
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[1]));

  frame.moveMouseToModel(objects[0]->boundingRect().min());

  EXPECT_EQ(1, frame.selection->selectedObjects().size());
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[1]));
}

TEST(framework_editor_base_objecteditor2d_Controller, zoomAndSelectWithCircle)
{
  ControllerTestFrame frame;

  auto zoomFactor = [&](){return frame.controller->view()->zoomFactor();};

  // Zoom with the mouse
  real lastZoomFactor = zoomFactor();
  frame.mouse.useWheel(1.f);
  EXPECT_NE(lastZoomFactor, zoomFactor());

  // start and quit selecting with the circle selection tool
  frame.keyboard.press(KeyCode::C);
  frame.keyboard.release(KeyCode::C);
  lastZoomFactor = zoomFactor();
  frame.mouse.useWheel(1.f);
  EXPECT_EQ(lastZoomFactor, zoomFactor());
  frame.keyboard.press(KeyCode::ESCAPE);
  frame.keyboard.release(KeyCode::ESCAPE);

  // Zoom with the mouse
  lastZoomFactor = zoomFactor();
  frame.mouse.useWheel(1.f);
  EXPECT_NE(lastZoomFactor, zoomFactor());
}

TEST(framework_editor_base_objecteditor2d_Controller, selectRectangle)
{
  ControllerTestFrame frame;
  QList<Object::Ptr> objects = frame.createSomeObjects();

  frame.moveMouseToModel(objects[1]->boundingRect().min()-vec2(2, 2));

  // Just Pressing B and the left mouse-button shouldn't have an effect
  frame.keyboard.press(KeyCode::B);
  frame.keyboard.release(KeyCode::B);
  frame.mouse.press(MouseButton::LEFT);
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_TRUE(frame.selection->selectedObjects().empty());

  // Start selecting, but abort with esc
  frame.keyboard.press(KeyCode::B);
  frame.keyboard.release(KeyCode::B);
  frame.moveMouseToModel(objects[1]->boundingRect().min()-vec2(2, 2));
  frame.mouse.press(MouseButton::LEFT);
  frame.moveMouseToModel(objects[1]->boundingRect().max());
  frame.keyboard.press(KeyCode::ESCAPE);
  frame.keyboard.release(KeyCode::ESCAPE);
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_TRUE(frame.selection->selectedObjects().empty());

  // Start selecting, but abort with the right mouse-button
  frame.keyboard.press(KeyCode::B);
  frame.keyboard.release(KeyCode::B);
  frame.moveMouseToModel(objects[1]->boundingRect().min()-vec2(2, 2));
  frame.mouse.press(MouseButton::LEFT);
  frame.moveMouseToModel(objects[1]->boundingRect().max());
  frame.mouse.press(MouseButton::RIGHT);
  frame.mouse.release(MouseButton::RIGHT);
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_TRUE(frame.selection->selectedObjects().empty());

  // Start selecting, but abort with the middle mouse-button
  frame.keyboard.press(KeyCode::B);
  frame.keyboard.release(KeyCode::B);
  frame.moveMouseToModel(objects[1]->boundingRect().min()-vec2(2, 2));
  frame.mouse.press(MouseButton::LEFT);
  frame.moveMouseToModel(objects[1]->boundingRect().max());
  frame.mouse.press(MouseButton::MIDDLE);
  frame.mouse.release(MouseButton::MIDDLE);
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_TRUE(frame.selection->selectedObjects().empty());

  // select thee objects
  frame.keyboard.press(KeyCode::B);
  frame.keyboard.release(KeyCode::B);
  frame.moveMouseToModel(objects[1]->boundingRect().min()-vec2(2, 2));
  frame.mouse.press(MouseButton::LEFT);
  frame.moveMouseToModel(objects[1]->boundingRect().max());
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_EQ(3, frame.selection->selectedObjects().size());
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[1]));
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[2]));
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[3]));
}

TEST(framework_editor_base_objecteditor2d_Controller, selectRectangle_deselect)
{
  ControllerTestFrame frame;
  QList<Object::Ptr> objects = frame.createSomeObjects();

  // Drag a selection rectangle with the left mouse-button
  frame.keyboard.press(KeyCode::B);
  frame.keyboard.release(KeyCode::B);
  frame.moveMouseToModel(objects[1]->boundingRect().min());
  frame.mouse.press(MouseButton::LEFT);
  frame.moveMouseToModel(objects[1]->boundingRect().max());
  frame.mouse.release(MouseButton::LEFT);

  // something should be selected now
  EXPECT_FALSE(frame.selection->selectedObjects().empty());

  // Drag a selection rectangle with the middle mouse-button to deselect everything again
  frame.keyboard.press(KeyCode::B);
  frame.keyboard.release(KeyCode::B);
  frame.mouse.press(MouseButton::MIDDLE);
  frame.moveMouseToModel(objects[1]->boundingRect().min());
  frame.mouse.release(MouseButton::MIDDLE);

  // nothing should be selected now
  EXPECT_TRUE(frame.selection->selectedObjects().empty());

  frame.keyboard.press(KeyCode::A);
  frame.keyboard.release(KeyCode::A);

  // something should be selected again
  EXPECT_FALSE(frame.selection->selectedObjects().empty());

  // start deselecting everything ...
  frame.keyboard.press(KeyCode::B);
  frame.keyboard.release(KeyCode::B);
  frame.moveMouseToModel(objects[0]->boundingRect().min());
  frame.mouse.press(MouseButton::MIDDLE);
  frame.moveMouseToModel(objects[4]->boundingRect().max());
  // but abort it with the left mouse button
  frame.mouse.press(MouseButton::LEFT);
  frame.mouse.release(MouseButton::LEFT);
  frame.mouse.release(MouseButton::MIDDLE);

  // so deselecting shoud fail, because aborted
  EXPECT_FALSE(frame.selection->selectedObjects().empty());

  // start deselecting everything ...
  frame.keyboard.press(KeyCode::B);
  frame.keyboard.release(KeyCode::B);
  frame.moveMouseToModel(objects[0]->boundingRect().min());
  frame.mouse.press(MouseButton::MIDDLE);
  frame.moveMouseToModel(objects[4]->boundingRect().max());
  // but abort it with the right mouse button
  frame.mouse.press(MouseButton::RIGHT);
  frame.mouse.release(MouseButton::RIGHT);
  frame.mouse.release(MouseButton::MIDDLE);

  // so deselecting shoud fail, because aborted
  EXPECT_FALSE(frame.selection->selectedObjects().empty());

  // start deselecting everything ...
  frame.keyboard.press(KeyCode::B);
  frame.keyboard.release(KeyCode::B);
  frame.moveMouseToModel(objects[0]->boundingRect().min());
  frame.mouse.press(MouseButton::MIDDLE);
  frame.moveMouseToModel(objects[4]->boundingRect().max());
  // but abort it with the escape key
  frame.keyboard.press(KeyCode::ESCAPE);
  frame.keyboard.release(KeyCode::ESCAPE);
  frame.mouse.release(MouseButton::MIDDLE);

  // so deselecting shoud fail, because aborted
  EXPECT_FALSE(frame.selection->selectedObjects().empty());

  // now try, whether deselecting everything works if not aborted
  frame.keyboard.press(KeyCode::B);
  frame.keyboard.release(KeyCode::B);
  frame.moveMouseToModel(objects[0]->boundingRect().min());
  frame.mouse.press(MouseButton::MIDDLE);
  frame.moveMouseToModel(objects[4]->boundingRect().max());
  frame.mouse.release(MouseButton::MIDDLE);

  // deselecting should succeed
  EXPECT_TRUE(frame.selection->selectedObjects().empty());
}

TEST(framework_editor_base_objecteditor2d_Controller, selectRectangle_ClickWithoutMoving)
{
  ControllerTestFrame frame;
  QList<Object::Ptr> objects = frame.createSomeObjects();

  // When simply clicking and releasing without dragging a rectangle, no object should be selected (to keep the usability blender-like)
  frame.keyboard.press(KeyCode::B);
  frame.moveMouseToModel(objects[1]->boundingRect().centerPoint());
  frame.mouse.press(MouseButton::LEFT);
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_TRUE(frame.selection->selectedObjects().empty());
}

TEST(framework_editor_base_objecteditor2d_Controller, selectRectangle_pressBwhileDraggingRectangle)
{
  ControllerTestFrame frame;
  QList<Object::Ptr> objects = frame.createSomeObjects();

  // Press B
  frame.keyboard.press(KeyCode::B);
  frame.keyboard.release(KeyCode::B);
  // Select Objects[1] by dragging a rectangle with the left mouse button over the object ...
  frame.moveMouseToModel(objects[1]->boundingRect().min());
  frame.mouse.press(MouseButton::LEFT);
  frame.moveMouseToModel(objects[1]->boundingRect().max());
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_FALSE(frame.selection->selectedObjects().empty());

  // .. so a deselection rectangle will be dragged with the middle mouse button
  frame.keyboard.press(KeyCode::B);
  frame.keyboard.release(KeyCode::B);
  frame.mouse.press(MouseButton::MIDDLE);
  frame.moveMouseToModel(objects[1]->boundingRect().min());
  frame.mouse.release(MouseButton::MIDDLE);

  EXPECT_TRUE(frame.selection->selectedObjects().empty());
}


TEST(framework_editor_base_objecteditor2d_Controller, selectRectangle_CoordinateSystem)
{
  ControllerTestFrame frame;
  QList<Object::Ptr> objects = frame.createSomeObjects();

  frame.moveMouseToModel(objects[0]->boundingRect().max()+vec2(2));
  frame.keyboard.press(KeyCode::B);
  frame.keyboard.release(KeyCode::B);
  frame.mouse.press(MouseButton::LEFT);
  frame.moveMouseToModel(objects[1]->boundingRect().min()-vec2(2));
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_TRUE(frame.selection->selectedObjects().empty());
}

TEST(framework_editor_base_objecteditor2d_Controller, simpleMovement)
{
  ControllerTestFrame frame(true);
  frame.setShape_Movement();
  frame.init();
  QList<Object::Ptr> objects = frame.createSomeObjects();

  Object::Ptr o = objects[1];

  Rectangle<vec2> rect = o->boundingRect();
  const vec2 offset(-10, 200);

  frame.moveMouseToModel(rect.centerPoint());
  frame.mouse.press(MouseButton::LEFT);
  frame.moveMouseInModelSpace(offset);
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_EQ(rect+offset, o->boundingRect());
}

TEST(framework_editor_base_objecteditor2d_Controller, simpleMovement_Slow)
{
  ControllerTestFrame frame(true);
  frame.setShape_Movement();
  frame.init();
  QList<Object::Ptr> objects = frame.createSomeObjects();

  Object::Ptr o = objects[1];

  Rectangle<vec2> rect = o->boundingRect();
  const vec2 offset(-10, 200);

  frame.moveMouseToModel(rect.centerPoint());
  frame.mouse.press(MouseButton::LEFT);
  frame.keyboard.press(KeyCode::SHIFT_LEFT);
  frame.moveMouseInModelSpace(offset);
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_EQ(rect+offset*frame.controller->slowDownFactor(), o->boundingRect());
}

TEST(framework_editor_base_objecteditor2d_Controller, simpleMovement_SlowAndNormal)
{
  ControllerTestFrame frame(true);
  frame.setShape_Movement();
  frame.init();
  QList<Object::Ptr> objects = frame.createSomeObjects();

  Object::Ptr o = objects[1];

  Rectangle<vec2> rect = o->boundingRect();
  const vec2 offset(-10, 200);

  frame.moveMouseToModel(rect.centerPoint());
  frame.mouse.press(MouseButton::LEFT);
  frame.moveMouseInModelSpace(offset);
  frame.keyboard.press(KeyCode::SHIFT_LEFT);
  EXPECT_EQ(rect+offset, o->boundingRect());
  frame.moveMouseInModelSpace(offset);
  EXPECT_EQ(rect+offset*(1.f+frame.controller->slowDownFactor()), o->boundingRect());
  frame.keyboard.release(KeyCode::SHIFT_LEFT);
  EXPECT_EQ(rect+offset*2.f, o->boundingRect());
  frame.moveMouseInModelSpace(offset);
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_EQ(rect+offset*3.f, o->boundingRect());
}

TEST(framework_editor_base_objecteditor2d_Controller, simpleMovement_Abort)
{
  ControllerTestFrame frame(true);
  frame.setShape_Movement();
  frame.init();
  QList<Object::Ptr> objects = frame.createSomeObjects();

  Object::Ptr o = objects[1];

  Rectangle<vec2> rect = o->boundingRect();
  const vec2 offset(-10, 200);

  frame.moveMouseToModel(rect.centerPoint());
  frame.mouse.press(MouseButton::LEFT);
  frame.moveMouseInModelSpace(offset);
  frame.mouse.press(MouseButton::MIDDLE);
  frame.mouse.release(MouseButton::MIDDLE);
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_EQ(rect, o->boundingRect());

  frame.moveMouseToModel(rect.centerPoint());
  frame.mouse.press(MouseButton::LEFT);
  frame.moveMouseInModelSpace(offset);
  frame.mouse.press(MouseButton::RIGHT);
  frame.mouse.release(MouseButton::RIGHT);
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_EQ(rect, o->boundingRect());

  frame.moveMouseToModel(rect.centerPoint());
  frame.mouse.press(MouseButton::LEFT);
  frame.moveMouseInModelSpace(offset);
  frame.keyboard.press(KeyCode::ESCAPE);
  frame.keyboard.release(KeyCode::ESCAPE);
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_EQ(rect, o->boundingRect());
}

TEST(framework_editor_base_objecteditor2d_Controller, simpleMovement_Snap)
{
  ControllerTestFrame frame(true);
  frame.setShape_Movement();
  frame.setSnapping_ForceY(-42);
  frame.init();
  QList<Object::Ptr> objects = frame.createSomeObjects();

  Object::Ptr o = objects[1];

  Rectangle<vec2> rect = o->boundingRect();
  const vec2 offset(-10, 200);

  frame.moveMouseToModel(rect.centerPoint());
  frame.mouse.press(MouseButton::LEFT);
  frame.moveMouseInModelSpace(offset);
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_EQ(Rectangle<vec2>(vec2(rect.min().x+offset.x, -42), vec2(rect.max().x+offset.x, -42+rect.size().y)), o->boundingRect());
}

TEST(framework_editor_base_objecteditor2d_Controller, simpleMovement_Contrained_X)
{
  ControllerTestFrame frame(true);
  frame.setShape_Movement();
  frame.init();
  QList<Object::Ptr> objects = frame.createSomeObjects();

  Object::Ptr o = objects[1];

  Rectangle<vec2> rect = o->boundingRect();
  const vec2 offset(-10, 200);

  frame.moveMouseToModel(rect.centerPoint());
  frame.mouse.press(MouseButton::LEFT);
  frame.moveMouseInModelSpace(offset);
  EXPECT_EQ(rect+offset, o->boundingRect());
  frame.keyboard.press(KeyCode::X);
  frame.keyboard.release(KeyCode::X);
  EXPECT_EQ(rect+vec2(offset.x, 0), o->boundingRect());
  frame.keyboard.press(KeyCode::X);
  frame.keyboard.release(KeyCode::X);
  EXPECT_EQ(rect+offset, o->boundingRect());
  frame.mouse.release(MouseButton::LEFT);

}

TEST(framework_editor_base_objecteditor2d_Controller, simpleMovement_Contrained_Y)
{
  ControllerTestFrame frame(true);
  frame.setShape_Movement();
  frame.init();
  QList<Object::Ptr> objects = frame.createSomeObjects();

  Object::Ptr o = objects[1];

  Rectangle<vec2> rect = o->boundingRect();
  const vec2 offset(-10, 200);

  frame.moveMouseToModel(rect.centerPoint());
  frame.mouse.press(MouseButton::LEFT);
  frame.moveMouseInModelSpace(offset);
  EXPECT_EQ(rect+offset, o->boundingRect());
  frame.keyboard.press(KeyCode::Y);
  frame.keyboard.release(KeyCode::Y);
  EXPECT_EQ(rect+vec2(0, offset.y), o->boundingRect());
  frame.keyboard.press(KeyCode::Y);
  frame.keyboard.release(KeyCode::Y);
  EXPECT_EQ(rect+offset, o->boundingRect());
  frame.mouse.release(MouseButton::LEFT);

}

TEST(framework_editor_base_objecteditor2d_Controller, simpleMovement_selectionChange)
{
  ControllerTestFrame frame(true);
  frame.setShape_Movement();
  frame.init();
  QList<Object::Ptr> objects = frame.createSomeObjects();

  frame.selection->selectAll();

  EXPECT_EQ(5, frame.selection->selectedObjects().size());

  Object::Ptr o = objects[1];

  frame.selection->unselectObject(o);

  EXPECT_EQ(4, frame.selection->selectedObjects().size());
  EXPECT_FALSE(frame.selection->selectedObjects().contains(o));

  Rectangle<vec2> rect = o->boundingRect();
  const vec2 offset(-10, 200);

  frame.moveMouseToModel(rect.centerPoint());
  frame.mouse.press(MouseButton::LEFT);
  frame.moveMouseInModelSpace(offset);
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_EQ(1, frame.selection->selectedObjects().size());
  EXPECT_TRUE(frame.selection->selectedObjects().contains(o));
}

TEST(framework_editor_base_objecteditor2d_Controller, simpleMovement_OutsideClick_noSelectionChange)
{
  ControllerTestFrame frame(true);
  frame.setShape_Movement();
  frame.init();
  QList<Object::Ptr> objects = frame.createSomeObjects();

  frame.selection->selectObjects({objects[1], objects[2]});

  EXPECT_EQ(2, frame.selection->selectedObjects().size());
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[1]));
  EXPECT_TRUE(frame.selection->selectedObjects().contains(objects[2]));

  Object::Ptr o1 = objects[1];
  Object::Ptr o2 = objects[2];

  Rectangle<vec2> rect1 = o1->boundingRect();
  Rectangle<vec2> rect2 = o2->boundingRect();
  const vec2 offset(-10, 200);
  const vec2 startPoint(1000, 0);

  frame.moveMouseToModel(startPoint);
  frame.mouse.press(MouseButton::LEFT);
  frame.moveMouseInModelSpace(offset);
  frame.mouse.release(MouseButton::LEFT);

  EXPECT_EQ(rect1+offset, o1->boundingRect());
  EXPECT_EQ(rect2+offset, o2->boundingRect());
}

TEST(framework_editor_base_objecteditor2d_Controller, move_with_G)
{
  ControllerTestFrame frame;
  QList<Object::Ptr> objects = frame.createSomeObjects();

  frame.selection->selectObjects({objects[1], objects[2]});

  Object::Ptr o1 = objects[1];
  Object::Ptr o2 = objects[2];

  Rectangle<vec2> rect1 = o1->boundingRect();
  Rectangle<vec2> rect2 = o2->boundingRect();

  vec2 offset(-10, 200);

  frame.keyboard.press(KeyCode::G);
  frame.keyboard.release(KeyCode::G);
  frame.moveMouseInModelSpace(offset);

  EXPECT_EQ(rect1+offset, o1->boundingRect());
  EXPECT_EQ(rect2+offset, o2->boundingRect());

  frame.keyboard.press(KeyCode::X);
  frame.keyboard.release(KeyCode::X);

  EXPECT_EQ(rect1+vec2(offset.x, 0), o1->boundingRect());
  EXPECT_EQ(rect2+vec2(offset.x, 0), o2->boundingRect());

  frame.keyboard.press(KeyCode::Y);
  frame.keyboard.release(KeyCode::Y);

  EXPECT_EQ(rect1+vec2(0, offset.y), o1->boundingRect());
  EXPECT_EQ(rect2+vec2(0, offset.y), o2->boundingRect());

  frame.keyboard.press(KeyCode::Y);
  frame.keyboard.release(KeyCode::Y);

  EXPECT_EQ(rect1+offset, o1->boundingRect());
  EXPECT_EQ(rect2+offset, o2->boundingRect());
}

TEST(framework_editor_base_objecteditor2d_Controller, trying_to_move_no_object)
{
  ControllerTestFrame frame;
  QList<Object::Ptr> objects = frame.createSomeObjects();

  vec2 offset(-10, 200);

  frame.keyboard.press(KeyCode::G);
  frame.keyboard.release(KeyCode::G);
  frame.moveMouseInModelSpace(offset);

  EXPECT_TRUE(frame.selection->selectedObjects().empty());

  // There are no selected objects, so pressing G shouldn't have started an move action.
  // So pressing A should have an effect.

  frame.keyboard.press(KeyCode::A);
  frame.keyboard.release(KeyCode::A);
  EXPECT_FALSE(frame.selection->selectedObjects().empty());
}

TEST(framework_editor_base_objecteditor2d_Controller, resize_by_dragging_shape_NW)
{
  ControllerTestFrame frame(true);
  frame.setShape_Resizable();
  frame.init();
  QList<Object::Ptr> objects = frame.createSomeObjects(true);

  Object::Ptr o = objects[1];
  Rectangle<vec2> rect = o->boundingRect();

  vec2 offset(-10, 30);

  frame.moveMouseToModel(rect.min() + vec2(4));
  frame.mouse.press(MouseButton::LEFT);
  frame.moveMouseInModelSpace(offset);

  EXPECT_EQ(Rectangle<vec2>(rect.min()+offset, rect.max()), o->boundingRect());
}

TEST(framework_editor_base_objecteditor2d_Controller, resize_by_dragging_shape_NE)
{
  ControllerTestFrame frame(true);
  frame.setShape_Resizable();
  frame.init();
  QList<Object::Ptr> objects = frame.createSomeObjects(true);

  Object::Ptr o = objects[1];
  Rectangle<vec2> rect = o->boundingRect();

  vec2 offset(-10, 30);

  frame.moveMouseToModel(vec2(rect.max().x, rect.min().y)+vec2(-4, 4));
  frame.mouse.press(MouseButton::LEFT);
  frame.moveMouseInModelSpace(offset);

  EXPECT_EQ(Rectangle<vec2>(rect.min()+vec2(0, offset.y), rect.max()+vec2(offset.x, 0)), o->boundingRect());
}

TEST(framework_editor_base_objecteditor2d_Controller, resize_by_dragging_shape_SW)
{
  ControllerTestFrame frame(true);
  frame.setShape_Resizable();
  frame.init();
  QList<Object::Ptr> objects = frame.createSomeObjects(true);

  Object::Ptr o = objects[1];
  Rectangle<vec2> rect = o->boundingRect();

  vec2 offset(-10, 30);

  frame.moveMouseToModel(vec2(rect.min().x, rect.max().y)+vec2(4, -4));
  frame.mouse.press(MouseButton::LEFT);
  frame.moveMouseInModelSpace(offset);

  EXPECT_EQ(Rectangle<vec2>(rect.min()+vec2(offset.x, 0), rect.max()+vec2(0, offset.y)), o->boundingRect());
}

TEST(framework_editor_base_objecteditor2d_Controller, resize_by_dragging_shape_SE)
{
  ControllerTestFrame frame(true);
  frame.setShape_Resizable();
  frame.init();
  QList<Object::Ptr> objects = frame.createSomeObjects(true);

  Object::Ptr o = objects[1];
  Rectangle<vec2> rect = o->boundingRect();

  vec2 offset(-10, 30);

  frame.moveMouseToModel(rect.max()+vec2(-4));
  frame.mouse.press(MouseButton::LEFT);
  frame.moveMouseInModelSpace(offset);

  EXPECT_EQ(Rectangle<vec2>(rect.min(), rect.max()+offset), o->boundingRect());
}

TEST(framework_editor_base_objecteditor2d_Controller, resize_by_dragging_shape_N)
{
  ControllerTestFrame frame(true);
  frame.setShape_Resizable();
  frame.init();
  QList<Object::Ptr> objects = frame.createSomeObjects(true);

  Object::Ptr o = objects[1];
  Rectangle<vec2> rect = o->boundingRect();

  vec2 offset(-10, 30);

  frame.moveMouseToModel(rect.min()+vec2(30, 4));
  frame.mouse.press(MouseButton::LEFT);
  frame.moveMouseInModelSpace(offset);

  EXPECT_EQ(Rectangle<vec2>(rect.min()+vec2(0,offset.y), rect.max()), o->boundingRect());
}

TEST(framework_editor_base_objecteditor2d_Controller, resize_by_dragging_shape_W)
{
  ControllerTestFrame frame(true);
  frame.setShape_Resizable();
  frame.init();
  QList<Object::Ptr> objects = frame.createSomeObjects(true);

  Object::Ptr o = objects[1];
  Rectangle<vec2> rect = o->boundingRect();

  vec2 offset(-10, 30);

  frame.moveMouseToModel(rect.min()+vec2(4, 30));
  frame.mouse.press(MouseButton::LEFT);
  frame.moveMouseInModelSpace(offset);

  EXPECT_EQ(Rectangle<vec2>(rect.min()+vec2(offset.x, 0), rect.max()), o->boundingRect());
}

TEST(framework_editor_base_objecteditor2d_Controller, resize_by_dragging_shape_E)
{
  ControllerTestFrame frame(true);
  frame.setShape_Resizable();
  frame.init();
  QList<Object::Ptr> objects = frame.createSomeObjects(true);

  Object::Ptr o = objects[1];
  Rectangle<vec2> rect = o->boundingRect();

  vec2 offset(-10, 30);

  frame.moveMouseToModel(rect.max()+vec2(-4, -30));
  frame.mouse.press(MouseButton::LEFT);
  frame.moveMouseInModelSpace(offset);

  EXPECT_EQ(Rectangle<vec2>(rect.min(), rect.max()+vec2(offset.x, 0)), o->boundingRect());
}

TEST(framework_editor_base_objecteditor2d_Controller, resize_by_dragging_shape_S)
{
  ControllerTestFrame frame(true);
  frame.setShape_Resizable();
  frame.init();
  QList<Object::Ptr> objects = frame.createSomeObjects(true);

  Object::Ptr o = objects[1];
  Rectangle<vec2> rect = o->boundingRect();

  vec2 offset(-10, 30);

  frame.moveMouseToModel(rect.max()+vec2(-30, -4));
  frame.mouse.press(MouseButton::LEFT);
  frame.moveMouseInModelSpace(offset);

  EXPECT_EQ(Rectangle<vec2>(rect.min(), rect.max()+vec2(0, offset.y)), o->boundingRect());
}


} // namespace ObjectEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework
