#ifndef FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_TESTMODEL_H
#define FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_TESTMODEL_H


#include "model.h"


namespace Framework {
namespace Editor {
namespace Base {
namespace ObjectEditor2D {


class TestModel : public Model
{
public:
  typedef std::shared_ptr<TestModel> Ptr;

public:
  TestModel(const vec2& gridSize);

  static Ptr create(const vec2& gridSize);

  void addObject(const Object::Ptr& object, const Rectangle<vec2>& boundingRectangle);
  void removeObject(const Object::Ptr& object);
  void moveObject(const Object::Ptr& object, const Rectangle<vec2>& boundingRectangle);
};


} // namespace ObjectEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework

#endif // FRAMEWORK_EDITOR_BASE_OBJECTEDITOR2D_TESTMODEL_H
