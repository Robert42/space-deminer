#include "renderer.h"

namespace Framework {
namespace Editor {
namespace Base {
namespace ObjectEditor2D {


Renderer::Renderer(const View::Ptr& view, const Order::Ptr& order, const Shape::Ptr& shape, const Selection::Ptr& selection)
  : Editor2D::Renderer(view),
    _view(view),
    order(order),
    shape(shape)
{
  auto markDirty = std::bind(&Renderer::markAsDirty, this);

  view->model()->signalObjectWasAdded().connect(markDirty).track(this->trackable);
  view->model()->signalObjectWasMoved().connect(markDirty).track(this->trackable);
  view->model()->signalObjectWasRemoved().connect(markDirty).track(this->trackable);
  selection->signalActiveObjectChanged().connect(markDirty).track(this->trackable);
  selection->signalSelectionChanged().connect(markDirty).track(this->trackable);
}


Order::Iterator::Ptr Renderer::objectsToRender()
{
  return this->order->downToUpIterator(view()->findObjectsInViewWithExtension(shape->largestPossibleAdditionalObjectDecoration()));
}


const View::Ptr& Renderer::view()
{
  return _view;
}


void Renderer::markAsDirty()
{
  Editor2D::Renderer::markAsDirty();
}


} // namespace ObjectEditor2D
} // namespace Base
} // namespace Editor
} // namespace Framework
