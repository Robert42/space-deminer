#ifndef FRAMEWORK_EDITOR_BLENDERSTYLEINPUT_EDITOR2D_INPUTHANDLER_H
#define FRAMEWORK_EDITOR_BLENDERSTYLEINPUT_EDITOR2D_INPUTHANDLER_H

#include <framework/developer-tools/editor/base/editor-2d/controller.h>

#include <framework/window/user-input/keyboard.h>
#include <framework/window/user-input/mouse.h>
#include <framework/window/user-input/mouse-action.h>
#include <framework/window/user-input/mouse-cursor-layers.h>


namespace Framework {
namespace Editor {
namespace BlenderStyleInput {
namespace Editor2D {

class InputHandler
{
public:
  typedef std::shared_ptr<InputHandler> Ptr;

public:
  Signals::Trackable trackable;

private:
  Base::Editor2D::Controller::Ptr _controller;

  std::shared_ptr<UserInput::SignalBlocker> signalBlocker;

  vec2 _viewportOffset;

  const MouseCursorLayers::Layer::Ptr _mouseCursorLayer;

  Signals::CallableSignal<void(const Rectangle<vec2>&)> _signalViewportChanged;

public:
  /**
   * @param controller
   * @param priority
   * @param blockerPriority
   * @param mouseCursorLayer If null is given as an argument, a fallback mouselayer will be used
   */
  InputHandler(const Base::Editor2D::Controller::Ptr& controller, InputDevice::ActionPriority priority, InputDevice::ActionPriority blockerPriority, const MouseCursorLayers::Layer::Ptr& mouseCursorLayer=MouseCursorLayers::Layer::Ptr());
  virtual ~InputHandler();

  /**
   * @param controller
   * @param priority
   * @param blockerPriority
   * @param mouseCursorLayer If null is given as an argument, a fallback mouselayer will be used
   *
   * @return A new instance
   */
  static Ptr create(const Base::Editor2D::Controller::Ptr& controller, InputDevice::ActionPriority priority, InputDevice::ActionPriority blockerPriority, const MouseCursorLayers::Layer::Ptr& mouseCursorLayer=MouseCursorLayers::Layer::Ptr());

  void setViewportOffset(const vec2& viewportOffset);
  const vec2& viewportOffset() const;

  Rectangle<vec2> viewport() const;

  Signals::Signal<void(const Rectangle<vec2>&)> signalViewportChanged();


protected:
  Base::Editor2D::Controller::Ptr& controller();
  const MouseCursorLayers::Layer::Ptr& mouseCursorLayer();

  MouseAction::Ptr createMouseActionForControllerAction(Base::Editor2D::Controller::Action::Ptr action);

private:
  bool handleKey(KeyCode keyCode);

  bool handleMouseButtonPressed(MouseButton button);
  bool handleMouseWheel();
  bool handleMouseMove();
  void applyMousePosition();

  vec2 currentMousePosition() const;
};

} // namespace Editor2D
} // namespace BlenderStyleInput
} // namespace Editor
} // namespace Framework

#endif // FRAMEWORK_EDITOR_BLENDERSTYLEINPUT_EDITOR2D_INPUTHANDLER_H
