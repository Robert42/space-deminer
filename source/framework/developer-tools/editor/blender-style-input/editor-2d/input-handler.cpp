#include "input-handler.h"
#include <framework/window/user-input/mouse.h>

namespace Framework {
namespace Editor {
namespace BlenderStyleInput {
namespace Editor2D {

InputHandler::InputHandler(const Base::Editor2D::Controller::Ptr &controller, InputDevice::ActionPriority priority, InputDevice::ActionPriority blockerPriority, const MouseCursorLayers::Layer::Ptr& mouseCursorLayer)
  : _controller(controller),
    _viewportOffset(0),
    _mouseCursorLayer(mouseCursorLayer ? mouseCursorLayer : Mouse::mouseCursorLayers().unused)
{
  Keyboard::signalKeyPressed().connect(priority, std::bind(&InputHandler::handleKey, this, _1)).track(this->trackable);
  Mouse::signalButtonPressed().connect(priority, std::bind(&InputHandler::handleMouseButtonPressed, this, _1)).track(this->trackable);
  Mouse::signalMouseWheelMoved().connect(priority, std::bind(&InputHandler::handleMouseWheel, this)).track(this->trackable);
  Mouse::signalMouseMoved().connect(priority, std::bind(&InputHandler::handleMouseMove, this)).track(this->trackable);

  signalBlocker = UserInput::createSignalBlocker(blockerPriority, true);

  controller->signalViewportChanged().connect([this](const Rectangle<vec2>&){_signalViewportChanged(viewport());}).track(this->trackable).track(controller->trackable);

  applyMousePosition();

  _mouseCursorLayer->setVisible(true);
  _mouseCursorLayer->setCursor(DefaultMouseCursors::defaultCursor);
}


InputHandler::Ptr InputHandler::create(const Base::Editor2D::Controller::Ptr& controller, InputDevice::ActionPriority priority, InputDevice::ActionPriority blockerPriority, const MouseCursorLayers::Layer::Ptr& mouseCursorLayer)
{
  return Ptr(new InputHandler(controller, priority, blockerPriority, mouseCursorLayer));
}


InputHandler::~InputHandler()
{
  mouseCursorLayer()->setVisible(false);
}


void InputHandler::setViewportOffset(const vec2& viewportOffset)
{
  if(this->_viewportOffset != viewportOffset)
  {
    this->_viewportOffset = viewportOffset;
    _signalViewportChanged(viewport());
    applyMousePosition();
  }
}


const vec2& InputHandler::viewportOffset() const
{
  return _viewportOffset;
}


Rectangle<vec2> InputHandler::viewport() const
{
  return _controller->viewport() + viewportOffset();
}


bool InputHandler::handleKey(KeyCode keyCode)
{
  auto invertZoomButton = [](float factor)
  {
    if(!Keyboard::isModifierDown(KeyModifier::CONTROL))
      factor = 1.f/factor;

    return factor;
  };

  switch(keyCode.value)
  {
  case KeyCode::HOME:
    return controller()->fitWholeModel();
  case KeyCode::KEYPAD_1:
  case KeyCode::NUMPAD_1:
    return controller()->setZoomFactor(1.f);
  case KeyCode::KEYPAD_2:
  case KeyCode::NUMPAD_2:
    return controller()->setZoomFactor(invertZoomButton(2.f));
  case KeyCode::KEYPAD_4:
  case KeyCode::NUMPAD_4:
    return controller()->setZoomFactor(invertZoomButton(4.f));
  case KeyCode::KEYPAD_8:
  case KeyCode::NUMPAD_8:
    return controller()->setZoomFactor(invertZoomButton(8.f));
  case KeyCode::NUMPAD_PLUS:
    controller()->zoomToMouse(1.f);
    return true;
  case KeyCode::NUMPAD_MINUS:
    return controller()->zoomToMouse(-1.f);
  default:
    return false;
  }
}


bool InputHandler::handleMouseWheel()
{
  return controller()->zoomToMouse(Mouse::wheelMovement());
}


bool InputHandler::handleMouseMove()
{
  return controller()->handlePointerMovement(currentMousePosition(),
                                             Mouse::relativeMovement());
}


void InputHandler::applyMousePosition()
{
  return controller()->initPointerPosition(currentMousePosition());
}

vec2 InputHandler::currentMousePosition() const
{
  return vec2(Mouse::absolutePosition())-viewportOffset();
}


bool InputHandler::handleMouseButtonPressed(MouseButton button)
{
  if(!controller()->hasInputFocus())
    return false;

  if(button == MouseButton::MIDDLE)
  {
    Base::Editor2D::Controller::Action::Ptr action = controller()->startMovingViewAction();
    MouseAction::Ptr mouseAction = createMouseActionForControllerAction(action);

    mouseAction->succeedWhenReleasingButton(MouseButton::MIDDLE);
    mouseAction->abortWhenReleasingKeyboardKey(KeyCode::ESCAPE);
    mouseAction->setCursor(DefaultMouseCursors::moveCursor);
  }

  return false;
}


Base::Editor2D::Controller::Ptr& InputHandler::controller()
{
  return _controller;
}


const MouseCursorLayers::Layer::Ptr& InputHandler::mouseCursorLayer()
{
  return _mouseCursorLayer;
}


MouseAction::Ptr InputHandler::createMouseActionForControllerAction(Base::Editor2D::Controller::Action::Ptr action)
{
  MouseAction::Ptr mouseAction = MouseAction::create();

  action->signalSucceeded().connect(&MouseAction::succeed).track(mouseAction);
  action->signalAborted().connect(&MouseAction::abort).track(mouseAction);
  mouseAction->signalActionSucceeded().connect(std::bind(&Base::Editor2D::Controller::Action::succeed, action.get())).track(action);
  mouseAction->signalActionAborted().connect(std::bind(&Base::Editor2D::Controller::Action::abort, action.get())).track(action);
  mouseAction->blockOtherSignals();
  mouseAction->setCursor(DefaultMouseCursors::defaultCursor);
  Mouse::signalMouseMoved().connect(InputDevice::ActionPriority::MOUSE_ACTION, std::bind(&InputHandler::handleMouseMove, this)).track(mouseAction);

  MousePositionConstraint::Ptr mousePositionConstraint;
  switch(action->constraint.value)
  {
  case Base::Editor2D::Controller::Action::MouseConstraint::NONE:
    mousePositionConstraint = MousePositionConstraint::createNoConstraint();
    break;
  case Base::Editor2D::Controller::Action::MouseConstraint::FULLSCREEN:
    mousePositionConstraint = MousePositionConstraint::createFullscreenConstraint(false);
    break;
  case Base::Editor2D::Controller::Action::MouseConstraint::VIEWPORT:
  case Base::Editor2D::Controller::Action::MouseConstraint::WRAPAROUND_VIEWPORT:
  {
    const bool wraparound = action->constraint==Base::Editor2D::Controller::Action::MouseConstraint::WRAPAROUND_VIEWPORT;
    MousePositionConstraint::RectangleConstraint::Ptr rectangleConstraint = MousePositionConstraint::createRectangleConstraint(viewport().cast<ivec2>(), wraparound);
    signalViewportChanged().connect([rectangleConstraint](const Rectangle<vec2>& r){rectangleConstraint->setRectangle(r.cast<ivec2>());}).track(mouseAction).track(this->trackable).track(action);
    mousePositionConstraint = rectangleConstraint;
    break;
  }
  case Base::Editor2D::Controller::Action::MouseConstraint::WHATEVER:
    break;
  }
  if(mousePositionConstraint)
    mouseAction->setPositionConstraint(mousePositionConstraint);

  if(action->finished())
    mouseAction->abort();

  return mouseAction;
}

Signals::Signal<void(const Rectangle<vec2>&)> InputHandler::signalViewportChanged()
{
  return _signalViewportChanged;
}

} // namespace Editor2D
} // namespace BlenderStyleInput
} // namespace Editor
} // namespace Framework
