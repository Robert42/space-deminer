#include "input-handler.h"

namespace Framework {
namespace Editor {
namespace BlenderStyleInput {
namespace ObjectEditor2D {

InputHandler::InputHandler(const Base::ObjectEditor2D::Controller::Ptr& controller, InputDevice::ActionPriority priority, InputDevice::ActionPriority blockerPriority, const MouseCursorLayers::Layer::Ptr& mouseCursorLayer)
  : parent_class(controller, priority, blockerPriority, mouseCursorLayer)
{
  Keyboard::signalKeyPressed().connect(priority, std::bind(&InputHandler::handleKey, this, _1)).track(this->trackable);
  Mouse::signalButtonPressed().connect(priority, std::bind(&InputHandler::handleMouseButtonPressed, this, _1)).track(this->trackable);
  Mouse::signalMouseMoved().connect(priority, std::bind(&InputHandler::chooseRightMouseCursor, this), Signals::PREPEND_SLOT).track(this->trackable);

  controller->view()->signalViewChanged().connect(std::bind(&InputHandler::chooseRightMouseCursor, this)).track(this->trackable);
  controller->signalViewportChanged().connect(std::bind(&InputHandler::chooseRightMouseCursor, this)).track(this->trackable);
  controller->model()->signalObjectWasAdded().connect(std::bind(&InputHandler::chooseRightMouseCursor, this)).track(this->trackable);
  controller->model()->signalObjectWasMoved().connect(std::bind(&InputHandler::chooseRightMouseCursor, this)).track(this->trackable);
  controller->model()->signalObjectWasRemoved().connect(std::bind(&InputHandler::chooseRightMouseCursor, this)).track(this->trackable);

  chooseRightMouseCursor();
}

InputHandler::~InputHandler()
{
}

InputHandler::Ptr InputHandler::create(const Base::ObjectEditor2D::Controller::Ptr& controller, InputDevice::ActionPriority priority, InputDevice::ActionPriority blockerPriority, const MouseCursorLayers::Layer::Ptr& mouseCursorLayer)
{
  return Ptr(new InputHandler(controller, priority, blockerPriority, mouseCursorLayer));
}

Base::ObjectEditor2D::Controller::Ptr InputHandler::controller()
{
  return std::static_pointer_cast<Base::ObjectEditor2D::Controller>(Editor2D::InputHandler::controller());
}

bool InputHandler::handleKey(KeyCode keyCode)
{
  switch(keyCode.value)
  {
  case KeyCode::A:
    if(!Keyboard::isAnyModifierDown())
      return controller()->selectOrDeselectAll();
    return false;
  case KeyCode::I:
    if(Keyboard::areOnlyModifierDown(KeyModifier::CONTROL))
      return controller()->invertSelection();
    return false;
  case KeyCode::C:
    if(!Keyboard::isAnyModifierDown())
    {
      Base::ObjectEditor2D::Controller::CircleSelectionAction::Ptr action = controller()->startSelectingObjectsWithCircle();
      MouseAction::Ptr mouseAction = createMouseActionForControllerAction(action);

      mouseAction->succeedWhenReleasingButton(MouseButton::RIGHT);
      mouseAction->abortWhenReleasingKeyboardKey(KeyCode::ESCAPE);
      mouseAction->setCursor(MouseCursor::Ptr());

      Mouse::signalMouseWheelMoved().connect(InputDevice::ActionPriority::MOUSE_ACTION, std::bind(&InputHandler::changeMouseSelectionSizeByMouseWheel, this, action.get())).track(action).track(this->trackable);
      Mouse::signalButtonPressed().connect(InputDevice::ActionPriority::MOUSE_ACTION, std::bind(&InputHandler::selectionCircleModeButtonPressed, this, action.get(), _1)).track(action).track(this->trackable);
      Mouse::signalButtonReleased().connect(InputDevice::ActionPriority::MOUSE_ACTION, std::bind(&InputHandler::selectionCircleModeButtonReleased, this, action.get(), _1)).track(action).track(this->trackable);
      return true;
    }
  case KeyCode::B:
    if(!Keyboard::isAnyModifierDown())
    {
      Base::ObjectEditor2D::Controller::RectangleSelectionCrossAction::Ptr action = controller()->startRectangleSelectionCross();
      MouseAction::Ptr mouseAction = createMouseActionForControllerAction(action);

      mouseAction->succeedWhenReleasingButton(MouseButton::RIGHT);
      mouseAction->abortWhenReleasingKeyboardKey(KeyCode::ESCAPE);

      Mouse::signalButtonPressed().connect(InputDevice::ActionPriority::MOUSE_ACTION, std::bind(&InputHandler::mouseButtonPressedDuringSelectionRectangleCrosslines, this, _1, action.get())).track(action);
      return true;
    }
  case KeyCode::G:
    if(!Keyboard::isAnyModifierDown())
    {
      startMovingSelectedObjects(MouseButton::LEFT);
      return true;
    }
    return false;
  case KeyCode::S:
    if(!Keyboard::isAnyModifierDown())
    {
      startScalingPositionOfSelectedObjects(MouseButton::LEFT);
      return true;
    }
    return false;
  case KeyCode::R:
    if(!Keyboard::isAnyModifierDown())
    {
      startRotatingPositionOfSelectedObjects(MouseButton::LEFT);
      return true;
    }
    return false;
  default:
    return false;
  }
}


bool InputHandler::handleMouseButtonPressed(MouseButton button)
{
  if(!controller()->hasInputFocus())
    return false;

  switch(button.value)
  {
  case MouseButton::LEFT:
  case MouseButton::RIGHT:
  {
    if(Keyboard::areOnlyModifierDown(KeyModifier::SHIFT))
    {
      controller()->selectAndActivateObjectAtMouseCursor(false);
    }else
    {
      Optional<Object::Ptr> selectedObject = controller()->selectAndActivateObjectAtMouseCursor(true);

      if(button == MouseButton::LEFT)
      {
        if(selectedObject)
        {
          Object::Ptr object = *selectedObject;

          ObjectShape::HandleType handle = controller()->shape()->handleType(controller()->currentModelSpaceMousePosition(), object);
          switch(handle.value)
          {
          case ObjectShape::HandleType::RESIZE_E:
          case ObjectShape::HandleType::RESIZE_N:
          case ObjectShape::HandleType::RESIZE_NE:
          case ObjectShape::HandleType::RESIZE_NW:
          case ObjectShape::HandleType::RESIZE_S:
          case ObjectShape::HandleType::RESIZE_SE:
          case ObjectShape::HandleType::RESIZE_SW:
          case ObjectShape::HandleType::RESIZE_W:
            startResizingSelectedObjects(button, handle, cursorForHandle(handle), object);
            break;
          case ObjectShape::HandleType::MOVE:
            startMovingSelectedObjects(button);
            break;
          default:
            break;
          }
        }else
        {
          startMovingSelectedObjects(button);
        }
      }else if(!Keyboard::isAnyModifierDown() && button == MouseButton::RIGHT)
      {
        if(selectedObject)
          startMovingSelectedObjects(button);
        else
          startSelectingObjectInRectangle(MouseButton::RIGHT, true);
      }
    }
    return true;
  }
  default:
    return false;
  }
}


bool InputHandler::mouseButtonPressedDuringSelectionRectangleCrosslines(MouseButton button, Base::ObjectEditor2D::Controller::RectangleSelectionCrossAction* action)
{
  bool selectMode;

  switch(button.value)
  {
  case MouseButton::LEFT:
    selectMode = true;
    break;
  case MouseButton::MIDDLE:
    selectMode = false;
    break;
  default:
    return false;
  }

  action->succeed();
  startSelectingObjectInRectangle(button, selectMode);
  return true;
}


void InputHandler::startSelectingObjectInRectangle(MouseButton button, bool selectMode)
{
  Base::ObjectEditor2D::Controller::RectangleSelectionAction::Ptr action = controller()->startRectangleSelection(selectMode);
  startGenericAction(button, action);
}

void InputHandler::startMovingSelectedObjects(MouseButton button)
{
  Base::ObjectEditor2D::Controller::MoveObjectsAction::Ptr action = controller()->startMovingObjects(controller()->selection()->selectedObjects());

  startGenericMoveAction(button, action);
}


void InputHandler::startScalingPositionOfSelectedObjects(MouseButton button)
{
  Base::ObjectEditor2D::Controller::ScaleObjectsPositionAction::Ptr action = controller()->startScaleObjectsPositionAction(controller()->selection()->selectedObjects());

  startGenericMoveAction(button, action);
}


void InputHandler::startRotatingPositionOfSelectedObjects(MouseButton button)
{
  Base::ObjectEditor2D::Controller::RotateObjectsPositionAction::Ptr action = controller()->startRotateObjectsPositionAction(controller()->selection()->selectedObjects());

  startGenericPositionTransformAction(button, action);
}



MouseAction::Ptr InputHandler::startGenericPositionTransformAction(MouseButton button, Base::ObjectEditor2D::Controller::TransformObjectPositionAction::Ptr action)
{
  MouseAction::Ptr mouseAction = startGenericAction(button, action);

  Keyboard::signalKeyPressed().connect(InputDevice::ActionPriority::MOUSE_ACTION, std::bind(&InputHandler::keyPressedDuringObjectPositionTransformation, this, _1, action.get())).track(action).track(this->trackable);
  Keyboard::signalKeyReleased().connect(InputDevice::ActionPriority::MOUSE_ACTION, std::bind(&InputHandler::keyReleasedDuringObjectPositionTransformation, this, _1, action.get())).track(action).track(this->trackable);

  if(Keyboard::isModifierDown(KeyModifier::SHIFT))
    action->slowObjectMovement();

  return mouseAction;
}

MouseAction::Ptr InputHandler::startGenericMoveAction(MouseButton button, Base::ObjectEditor2D::Controller::MoveObjectsAction::Ptr action)
{
  MouseAction::Ptr mouseAction = startGenericPositionTransformAction(button, action);

  Keyboard::signalKeyPressed().connect(InputDevice::ActionPriority::MOUSE_ACTION, std::bind(&InputHandler::keyPressedDuringObjectMovement, this, _1, action.get())).track(action).track(this->trackable);
  Keyboard::signalKeyReleased().connect(InputDevice::ActionPriority::MOUSE_ACTION, std::bind(&InputHandler::keyReleasedDuringObjectMovement, this, _1, action.get())).track(action).track(this->trackable);

  return mouseAction;
}

MouseAction::Ptr InputHandler::startGenericAction(MouseButton button, Base::ObjectEditor2D::Controller::Action::Ptr action)
{
  MouseAction::Ptr mouseAction = createMouseActionForControllerAction(action);
  mouseAction->succeedWhenReleasingButton(button);
  for(MouseButton b : {MouseButton::LEFT, MouseButton::MIDDLE, MouseButton::RIGHT})
  {
    if(b!=button)
      mouseAction->abortWhenReleasingButton(b);
  }
  mouseAction->abortWhenReleasingKeyboardKey(KeyCode::ESCAPE);

  return mouseAction;
}

void InputHandler::startResizingSelectedObjects(MouseButton button, ObjectShape::HandleType handle, const DefaultMouseCursor::Ptr& cursor, const Object::Ptr& object)
{
  Base::ObjectEditor2D::Controller::ResizeObjectByHandle::Ptr action = controller()->startResizingObjectByHandle(true, object, handle);
  MouseAction::Ptr mouseAction = startGenericAction(button, action);

  mouseAction->setCursor(cursor);
}

bool InputHandler::changeMouseSelectionSizeByMouseWheel(Base::ObjectEditor2D::Controller::CircleSelectionAction*)
{
  controller()->changeSelectionCircleRadius(Mouse::wheelMovement());
  return true;
}


bool InputHandler::selectionCircleModeButtonPressed(Base::ObjectEditor2D::Controller::CircleSelectionAction* action, MouseButton button)
{
  bool selectMode;

  switch(button.value)
  {
  case MouseButton::LEFT:
    selectMode = true;
    break;
  case MouseButton::MIDDLE:
    selectMode = false;
    break;
  default:
    return false;
  }

  action->startSelectingObjectInSelectionCircle(selectMode);
  currentlyUsedSelectionButton = button;
  return true;
}


bool InputHandler::selectionCircleModeButtonReleased(Base::ObjectEditor2D::Controller::CircleSelectionAction* action, MouseButton button)
{
  if(button == currentlyUsedSelectionButton)
  {
    action->stopSelectingObjectInSelectionCircle();
    return true;
  }
  return false;
}


bool InputHandler::keyPressedDuringObjectPositionTransformation(KeyCode key, Base::ObjectEditor2D::Controller::TransformObjectPositionAction* action)
{
  switch(key.value)
  {
  case KeyCode::SHIFT_LEFT:
  case KeyCode::SHIFT_RIGHT:
  {
    action->slowObjectMovement();
    return true;
  }
  default:
    return false;
  }
}


bool InputHandler::keyReleasedDuringObjectPositionTransformation(KeyCode key, Base::ObjectEditor2D::Controller::TransformObjectPositionAction* action)
{
  switch(key.value)
  {
  case KeyCode::SHIFT_LEFT:
  case KeyCode::SHIFT_RIGHT:
  {
    action->unslowObjectMovement();
    return true;
  }
  default:
    return false;
  }
}


bool InputHandler::keyPressedDuringObjectMovement(KeyCode key, Base::ObjectEditor2D::Controller::MoveObjectsAction* action)
{
  switch(key.value)
  {
  case KeyCode::X:
  {
    if(action->axisSnapping()!=Base::ObjectEditor2D::Controller::MoveObjectsAction::AxisSnapping::AxisSnapping::X)
      action->setAxisSnapping(Base::ObjectEditor2D::Controller::MoveObjectsAction::AxisSnapping::AxisSnapping::X);
    else
      action->setAxisSnapping(Base::ObjectEditor2D::Controller::MoveObjectsAction::AxisSnapping::AxisSnapping::NONE);
    return true;
  }
  case KeyCode::Y:
  {
    if(action->axisSnapping()!=Base::ObjectEditor2D::Controller::MoveObjectsAction::AxisSnapping::AxisSnapping::Y)
      action->setAxisSnapping(Base::ObjectEditor2D::Controller::MoveObjectsAction::AxisSnapping::AxisSnapping::Y);
    else
      action->setAxisSnapping(Base::ObjectEditor2D::Controller::MoveObjectsAction::AxisSnapping::AxisSnapping::NONE);
    return true;
  }
  default:
    return false;
  }
}


bool InputHandler::keyReleasedDuringObjectMovement(KeyCode, Base::ObjectEditor2D::Controller::MoveObjectsAction*)
{
  return false;
}


DefaultMouseCursor::Ptr InputHandler::cursorForHandle(ObjectShape::HandleType handle)
{
  switch(handle.value)
  {
  case ObjectShape::HandleType::RESIZE_E:
    return DefaultMouseCursors::moveWECursor;
  case ObjectShape::HandleType::RESIZE_N:
    return DefaultMouseCursors::moveNSCursor;
  case ObjectShape::HandleType::RESIZE_NE:
    return DefaultMouseCursors::moveSwNeCursor;
  case ObjectShape::HandleType::RESIZE_NW:
    return DefaultMouseCursors::moveNwSeCursor;
  case ObjectShape::HandleType::RESIZE_S:
    return DefaultMouseCursors::moveNSCursor;
  case ObjectShape::HandleType::RESIZE_SE:
    return DefaultMouseCursors::moveNwSeCursor;
  case ObjectShape::HandleType::RESIZE_SW:
    return DefaultMouseCursors::moveSwNeCursor;
  case ObjectShape::HandleType::RESIZE_W:
    return DefaultMouseCursors::moveWECursor;
  default:
    return DefaultMouseCursors::defaultCursor;
  }
}


bool InputHandler::chooseRightMouseCursor()
{
  const vec2 mousePosition = controller()->currentMousePosition();
  Optional<Object::Ptr> object;

  DefaultMouseCursor::Ptr cursor;

  if(object = controller()->objectAtScreenPoint(mousePosition))
  {
    const vec2 modelSpaceMousePosition = controller()->currentModelSpaceMousePosition();

    ObjectShape::HandleType handle = controller()->shape()->handleType(modelSpaceMousePosition, *object);
    cursor = cursorForHandle(handle);
  }else
  {
    cursor = DefaultMouseCursors::defaultCursor;
  }

  mouseCursorLayer()->setCursor(cursor);

  return false;
}


} // namespace ObjectEditor2D
} // namespace BlenderStyleInput
} // namespace Editor
} // namespace Framework
