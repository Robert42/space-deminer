#ifndef FRAMEWORK_EDITOR_BLENDERSTYLEINPUT_OBJECTEDITOR2D_INPUTHANDLER_H
#define FRAMEWORK_EDITOR_BLENDERSTYLEINPUT_OBJECTEDITOR2D_INPUTHANDLER_H

#include <framework/developer-tools/editor/blender-style-input/editor-2d/input-handler.h>
#include <framework/developer-tools/editor/base/object-editor-2d/controller.h>

namespace Framework {
namespace Editor {
namespace BlenderStyleInput {
namespace ObjectEditor2D {

class InputHandler : public Editor2D::InputHandler
{
public:
  typedef std::shared_ptr<InputHandler> Ptr;
  typedef Editor2D::InputHandler parent_class;
  typedef Base::ObjectEditor2D::Shape ObjectShape;
  typedef Base::ObjectEditor2D::Object Object;

private:
  Signals::Trackable trackable;

public:
  /**
   * @param controller
   * @param priority
   * @param blockerPriority
   * @param mouseCursorLayer If null is given as an argument, a fallback mouselayer will be used
   */
  InputHandler(const Base::ObjectEditor2D::Controller::Ptr& controller, InputDevice::ActionPriority priority, InputDevice::ActionPriority blockerPriority, const MouseCursorLayers::Layer::Ptr& mouseCursorLayer=MouseCursorLayers::Layer::Ptr());
  ~InputHandler();

  /**
   * @param controller
   * @param priority
   * @param blockerPriority
   * @param mouseCursorLayer If null is given as an argument, a fallback mouselayer will be used
   *
   * @return A new instance
   */
  static Ptr create(const Base::ObjectEditor2D::Controller::Ptr& controller, InputDevice::ActionPriority priority, InputDevice::ActionPriority blockerPriority, const MouseCursorLayers::Layer::Ptr& mouseCursorLayer=MouseCursorLayers::Layer::Ptr());

protected:
  Base::ObjectEditor2D::Controller::Ptr controller();

private:
  bool handleKey(KeyCode keyCode);
  bool handleMouseButtonPressed(MouseButton button);

  bool chooseRightMouseCursor();

  MouseButton currentlyUsedSelectionButton;

  bool changeMouseSelectionSizeByMouseWheel(Base::ObjectEditor2D::Controller::CircleSelectionAction* action);
  bool selectionCircleModeButtonPressed(Base::ObjectEditor2D::Controller::CircleSelectionAction* action, MouseButton button);
  bool selectionCircleModeButtonReleased(Base::ObjectEditor2D::Controller::CircleSelectionAction* action, MouseButton button);

  void startSelectingObjectInRectangle(MouseButton button, bool selectMode);
  void startMovingSelectedObjects(MouseButton button);
  void startScalingPositionOfSelectedObjects(MouseButton button);
  void startRotatingPositionOfSelectedObjects(MouseButton button);
  void startResizingSelectedObjects(MouseButton button, ObjectShape::HandleType handle, const DefaultMouseCursor::Ptr& cursor, const Object::Ptr& object);

  bool mouseButtonPressedDuringSelectionRectangleCrosslines(MouseButton button, Base::ObjectEditor2D::Controller::RectangleSelectionCrossAction* action);
  bool keyPressedDuringObjectPositionTransformation(KeyCode key, Base::ObjectEditor2D::Controller::TransformObjectPositionAction* action);
  bool keyReleasedDuringObjectPositionTransformation(KeyCode key, Base::ObjectEditor2D::Controller::TransformObjectPositionAction* action);
  bool keyPressedDuringObjectMovement(KeyCode key, Base::ObjectEditor2D::Controller::MoveObjectsAction* action);
  bool keyReleasedDuringObjectMovement(KeyCode key, Base::ObjectEditor2D::Controller::MoveObjectsAction* action);

  static DefaultMouseCursor::Ptr cursorForHandle(ObjectShape::HandleType handle);

  MouseAction::Ptr startGenericMoveAction(MouseButton button, Base::ObjectEditor2D::Controller::MoveObjectsAction::Ptr action);
  MouseAction::Ptr startGenericPositionTransformAction(MouseButton button, Base::ObjectEditor2D::Controller::TransformObjectPositionAction::Ptr action);
  MouseAction::Ptr startGenericAction(MouseButton button, Base::ObjectEditor2D::Controller::Action::Ptr action);
};

} // namespace ObjectEditor2D
} // namespace BlenderStyleInput
} // namespace Editor
} // namespace Framework

#endif // FRAMEWORK_EDITOR_BLENDERSTYLEINPUT_OBJECTEDITOR2D_INPUTHANDLER_H
