#ifndef FRAMEWORK_EDITOR_GORILLA_BLENDERSTYLETOOLLAYER_H
#define FRAMEWORK_EDITOR_GORILLA_BLENDERSTYLETOOLLAYER_H

#include <base/geometry/rectangle.h>
#include <framework/developer-tools/developer-hud.h>

namespace Framework {
namespace Editor {
namespace Gorilla {

class BlenderStyleToolLayer final : public DeveloperHud::Layer
{
private:
  class GorillaCustomRenderer;

public:
  class Renderer
  {
  public:
    Signals::Trackable trackable;

  private:
    BlenderStyleToolLayer& layer;
    GorillaCustomRenderer* const customRenderer;

  protected:
    const Rectangle<vec2> viewport;

  protected:
    Renderer(BlenderStyleToolLayer& layer, const Rectangle<vec2>& viewport);
    virtual ~Renderer();

    vec2 currentMousePosition() const;

  public:
    virtual void draw(GorillaCustomRenderer& renderer) = 0;

    void makeDirty();
  };

  class SelectionRectangleRenderer final : public Renderer
  {
    friend class BlenderStyleToolLayer;

  private:
    vec2 from;

    SelectionRectangleRenderer(BlenderStyleToolLayer& layer, const Rectangle<vec2>& viewport, const vec2& from);

  private:
    void draw(GorillaCustomRenderer& renderer);
  };

  class SelectionCircleRenderer final : public Renderer
  {
    friend class BlenderStyleToolLayer;

  private:
    real radius;

    SelectionCircleRenderer(BlenderStyleToolLayer& layer, const Rectangle<vec2>& viewport, real radius);

  public:
    void setRadius(real radius);

  private:
    void draw(GorillaCustomRenderer& renderer);

  };

  class SelectionRectangleCrosslinesRenderer final : public Renderer
  {
    friend class BlenderStyleToolLayer;

  private:
    SelectionRectangleCrosslinesRenderer(BlenderStyleToolLayer& layer, const Rectangle<vec2>& viewport);

  private:
    void draw(GorillaCustomRenderer& renderer);
  };

  class TransformationLineRenderer final : public Renderer
  {
    friend class BlenderStyleToolLayer;

  private:
    const vec2 from;
    const bool rotateArrow;
    const std::function<vec2()> mousePosition;

  private:
    TransformationLineRenderer(BlenderStyleToolLayer& layer, const Rectangle<vec2>& viewport, const vec2& from, const std::function<vec2()>& mousePosition, bool rotateArrow);

  private:
    void draw(GorillaCustomRenderer& renderer);
  };

private:
  friend class Renderer;

  void incrNumberRenderer();
  void decrNumberRenderer();

  int numRenderer;

public:
  BlenderStyleToolLayer(DeveloperHud& hud);
  ~BlenderStyleToolLayer();

  static std::shared_ptr<BlenderStyleToolLayer> create(DeveloperHud& hud);

  std::shared_ptr<SelectionRectangleRenderer> createSelectionRectangleRenderer(const Rectangle<vec2>& viewport, const vec2& from);
  std::shared_ptr<SelectionCircleRenderer> createSelectionCircleRenderer(const Rectangle<vec2>& viewport, real radius);
  std::shared_ptr<SelectionRectangleCrosslinesRenderer> createSelectionRectangleCrosslinesRenderer(const Rectangle<vec2>& viewport);
  std::shared_ptr<TransformationLineRenderer> createTransformationLine(const Rectangle<vec2>& viewport, const vec2& from, const std::function<vec2()>& mousePosition, bool rotateArrow);
};

} // namespace Gorilla
} // namespace Editor
} // namespace Framework

#endif // FRAMEWORK_EDITOR_GORILLA_BLENDERSTYLETOOLLAYER_H
