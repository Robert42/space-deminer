#include "blender-style-tool-layer.h"

#include <framework/developer-tools/gorilla-helper/viewport-renderer.h>
#include <framework/window/user-input/mouse.h>
#include <base/geometry/matrix3-transformations.h>

namespace Framework {
namespace Editor {
namespace Gorilla {


BlenderStyleToolLayer::BlenderStyleToolLayer(DeveloperHud& hud)
  : Layer(ZLayer::Value(0), hud.gorillaScreenBlenderStyleToolLayer()),
    numRenderer(0)
{
}


BlenderStyleToolLayer::~BlenderStyleToolLayer()
{
  assert(numRenderer == 0);
}


void BlenderStyleToolLayer::incrNumberRenderer()
{
  ++numRenderer;

  show();
}


void BlenderStyleToolLayer::decrNumberRenderer()
{
  --numRenderer;

  if(numRenderer == 0)
    hide();
}


std::shared_ptr<BlenderStyleToolLayer> BlenderStyleToolLayer::create(DeveloperHud& hud)
{
  return std::shared_ptr<BlenderStyleToolLayer>(new BlenderStyleToolLayer(hud));
}


std::shared_ptr<BlenderStyleToolLayer::SelectionRectangleRenderer> BlenderStyleToolLayer::createSelectionRectangleRenderer(const Rectangle<vec2>& viewport, const vec2& from)
{
  return std::shared_ptr<BlenderStyleToolLayer::SelectionRectangleRenderer>(new BlenderStyleToolLayer::SelectionRectangleRenderer(*this, viewport, from));
}


std::shared_ptr<BlenderStyleToolLayer::SelectionCircleRenderer> BlenderStyleToolLayer::createSelectionCircleRenderer(const Rectangle<vec2> &viewport, real radius)
{
  return std::shared_ptr<BlenderStyleToolLayer::SelectionCircleRenderer>(new BlenderStyleToolLayer::SelectionCircleRenderer(*this, viewport, radius));
}


std::shared_ptr<BlenderStyleToolLayer::SelectionRectangleCrosslinesRenderer> BlenderStyleToolLayer::createSelectionRectangleCrosslinesRenderer(const Rectangle<vec2>& viewport)
{
  return std::shared_ptr<BlenderStyleToolLayer::SelectionRectangleCrosslinesRenderer>(new BlenderStyleToolLayer::SelectionRectangleCrosslinesRenderer(*this, viewport));
}


std::shared_ptr<BlenderStyleToolLayer::TransformationLineRenderer> BlenderStyleToolLayer::createTransformationLine(const Rectangle<vec2>& viewport, const vec2& from, const std::function<vec2()>& mousePosition, bool rotateArrow)
{
  return std::shared_ptr<BlenderStyleToolLayer::TransformationLineRenderer>(new BlenderStyleToolLayer::TransformationLineRenderer(*this, viewport, from, mousePosition, rotateArrow));
}


// ====

class BlenderStyleToolLayer::GorillaCustomRenderer : public GorillaHelper::ViewportRenderer
{
private:
  BlenderStyleToolLayer::Renderer& renderer;

public:
  GorillaCustomRenderer(BlenderStyleToolLayer::Renderer& renderer)
    : renderer(renderer)
  {
  }

  void _redrawImplementation() override
  {
    mVertices.remove_all();

    renderer.draw(*this);
  }

  void makeDirty()
  {
    GorillaHelper::ViewportRenderer::makeDirty();
  }
};


BlenderStyleToolLayer::Renderer::Renderer(BlenderStyleToolLayer &layer, const Rectangle<vec2>& viewport)
  : layer(layer),
    customRenderer(new GorillaCustomRenderer(*this)),
    viewport(viewport)
{
  layer.incrNumberRenderer();

  layer.gorillaLayer().appendCustomRenderer(customRenderer);

  customRenderer->setScissor(viewport);

  Mouse::signalMouseMoved().connect(InputDevice::ActionPriority::MOUSE_CURSOR, [this](){makeDirty();return false;}).track(this->trackable);
}

BlenderStyleToolLayer::Renderer::~Renderer()
{
  layer.gorillaLayer().destroyCustomRenderer(customRenderer);
  layer.decrNumberRenderer();
}

vec2 BlenderStyleToolLayer::Renderer::currentMousePosition() const
{
  return vec2(Mouse::absolutePosition())-viewport.min();
}

void BlenderStyleToolLayer::Renderer::makeDirty()
{
  return customRenderer->makeDirty();
}

inline vec4 selectionBackGroundColor()
{
  return vec4(vec3(1.f), 0.125f);
}


BlenderStyleToolLayer::SelectionRectangleRenderer::SelectionRectangleRenderer(BlenderStyleToolLayer &layer, const Rectangle<vec2>& viewport, const vec2 &from)
  : Renderer(layer, viewport),
    from(from)
{
}


void BlenderStyleToolLayer::SelectionRectangleRenderer::draw(GorillaCustomRenderer& renderer)
{
  vec2 mouse = currentMousePosition();
  Rectangle<vec2> rect(from, mouse);

  const real pixelwidth = 4.f;

  renderer.zPosition = -1.f; // No Dashing
  renderer.drawRectangle(rect,
                         selectionBackGroundColor());


  renderer.zPosition = 0.f; // No antialiasing
  renderer.drawRectangle(rect,
                         rect-from,
                         vec4(1, pixelwidth, 0, 1),
                         1.f);
}


BlenderStyleToolLayer::SelectionCircleRenderer::SelectionCircleRenderer(BlenderStyleToolLayer &layer, const Rectangle<vec2>& viewport, real radius)
  : Renderer(layer, viewport),
    radius(radius)
{
}


void BlenderStyleToolLayer::SelectionCircleRenderer::setRadius(real radius)
{
  this->radius = radius;
  this->makeDirty();
}


void BlenderStyleToolLayer::SelectionCircleRenderer::draw(GorillaCustomRenderer& renderer)
{
  const vec2 mouse = currentMousePosition();
  const Circle circle(mouse, this->radius);

  const real radius = circle.radius();
  const real circumference = circle.circumference();

  renderer.drawCircle(circle, selectionBackGroundColor());

  const real aspiredPixelWidth = 5.f;
  const real pixelwidth = circumference/round(circumference/aspiredPixelWidth);
  const vec4 color(1, pixelwidth, 0, 1);

  renderer.zPosition = DEFAULT_ANTIALIASING_WIDTH; // The Width of the antialiasing

  renderer.drawCircleBorder(circle, [radius](real angle){return vec2(radius*angle, 0);}, color);
}


BlenderStyleToolLayer::SelectionRectangleCrosslinesRenderer::SelectionRectangleCrosslinesRenderer(BlenderStyleToolLayer& layer, const Rectangle<vec2>& viewport)
  : Renderer(layer, viewport)
{
}


void BlenderStyleToolLayer::SelectionRectangleCrosslinesRenderer::draw(GorillaCustomRenderer& renderer)
{
  const vec2 mouse = currentMousePosition();

  vec2 vertical0 = viewport.min();
  vec2 vertical1 = viewport.max();
  vec2 horizontal0 = viewport.min();
  vec2 horizontal1 = viewport.max();

  vec2 viewportSize = viewport.size();

  vertical0.x = vertical1.x = mouse.x;
  horizontal0.y = horizontal1.y = mouse.y;

  const real pixelwidth = 4.f;
  const vec4 color(1, pixelwidth, 0, 1);
  renderer.zPosition = 0.f; // No antialiasing
  renderer.drawLine(horizontal0, horizontal1, vec2(-mouse.x, 0), vec2(viewportSize.x-mouse.x, 0), color, color, 1.f, 0.f);
  renderer.drawLine(vertical0, vertical1, vec2(-mouse.y, 0), vec2(viewportSize.y-mouse.y, 0), color, color, 1.f, 0.f);
}


BlenderStyleToolLayer::TransformationLineRenderer::TransformationLineRenderer(BlenderStyleToolLayer& layer, const Rectangle<vec2>& viewport, const vec2& from, const std::function<vec2()>& mousePosition, bool rotateArrow)
  : Renderer(layer, viewport),
    from(from),
    rotateArrow(rotateArrow),
    mousePosition(mousePosition)
{
}


void BlenderStyleToolLayer::TransformationLineRenderer::draw(GorillaCustomRenderer &renderer)
{
  const vec2 mouse = mousePosition();

  real dashWidth = 4.f;
  vec4 color(0, // black
             dashWidth*2.f, // desh length
             0, // alpha between
             1); // total aplha
  renderer.zPosition = DEFAULT_ANTIALIASING_WIDTH; // The Width of the antialiasing
  renderer.drawLine(from, mouse, vec2(dashWidth, 0), vec2(dashWidth+length(mouse-from), 0), color, color);

  vec2 lineDirection = normalize(mouse-from);
  real arrowAngle = atan(lineDirection.y, lineDirection.x);
  if(this->rotateArrow)
    arrowAngle += radians(90.f);
  mat3 arrowMatrix = Geometry::translate3(currentMousePosition()) * Geometry::rotate3(arrowAngle) * Geometry::scale3(vec2(8.f));

  real w = 0.3625f;
  QVector<vec2> arrowShape = {vec2(-1.f, w),
                              vec2(-1.f, 1.0f),
                              vec2(-2.f, 0.0f),
                              vec2(-1.f,-1.0f),
                              vec2(-1.f,-w),
                              vec2( 1.f,-w),
                              vec2( 1.f,-1.0f),
                              vec2( 2.f, 0.0f),
                              vec2( 1.f, 1.0f),
                              vec2( 1.f, w)};
  for(vec2& v : arrowShape)
    v = (arrowMatrix * vec3(v, 1)).xy();

  color = vec4(0, //black
               0, // no dashing
               1,
               1);
  renderer.drawLineLoop(arrowShape, color);
}


} // namespace Gorilla
} // namespace Editor
} // namespace Framework
