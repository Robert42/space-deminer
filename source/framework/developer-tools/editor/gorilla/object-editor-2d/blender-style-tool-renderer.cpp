#include "blender-style-tool-renderer.h"

#include <framework/application.h>

namespace Framework {
namespace Editor {
namespace Gorilla {
namespace ObjectEditor2D {


BlenderStyleToolRenderer::BlenderStyleToolRenderer(const Base::ObjectEditor2D::Controller::Ptr& controller)
  : controller(controller)
{
  controller->signalShowSelectionRectangle().connect(std::bind(&BlenderStyleToolRenderer::showSelectionRectangle, this, _1)).track(this->trackable);
  controller->signalHideSelectionRectangle().connect(std::bind(&BlenderStyleToolRenderer::hideSelectionRectangle, this)).track(this->trackable);
  controller->signalShowSelectionCircle().connect(std::bind(&BlenderStyleToolRenderer::showSelectionCircle, this)).track(this->trackable);
  controller->signalHideSelectionCircle().connect(std::bind(&BlenderStyleToolRenderer::hideSelectionCircle, this)).track(this->trackable);
  controller->signalShowSelectionRectangleCrosslines().connect(std::bind(&BlenderStyleToolRenderer::showSelectionRectangleCrosslines, this)).track(this->trackable);
  controller->signalHideSelectionRectangleCrosslines().connect(std::bind(&BlenderStyleToolRenderer::hideSelectionRectangleCrosslines, this)).track(this->trackable);
  controller->signalShowScalingLine().connect(std::bind(&BlenderStyleToolRenderer::showTransformationLine, this, _1, false)).track(this->trackable);
  controller->signalShowRotationLine().connect(std::bind(&BlenderStyleToolRenderer::showTransformationLine, this, _1, true)).track(this->trackable);
}

BlenderStyleToolRenderer::~BlenderStyleToolRenderer()
{
}

BlenderStyleToolRenderer::Ptr BlenderStyleToolRenderer::create(const Base::ObjectEditor2D::Controller::Ptr& controller)
{
  return Ptr(new BlenderStyleToolRenderer(controller));
}

void BlenderStyleToolRenderer::showSelectionRectangle(const vec2& from)
{
  selectionRectangle = Application::developerHud().blenderStyleToolLayer()->createSelectionRectangleRenderer(controller->viewport().cast<vec2>(), from);
}

void BlenderStyleToolRenderer::hideSelectionRectangle()
{
  selectionRectangle.reset();
}

void BlenderStyleToolRenderer::showSelectionCircle()
{
  selectionCircle = Application::developerHud().blenderStyleToolLayer()->createSelectionCircleRenderer(controller->viewport().cast<vec2>(), controller->selectionCircleScreenspaceRadius());
  controller->signalSelectionCircleRadiusChanged().connect(std::bind(&BlenderStyleToolLayer::SelectionCircleRenderer::setRadius, selectionCircle.get(), _1)).track(selectionCircle);
}

void BlenderStyleToolRenderer::hideSelectionCircle()
{
  selectionCircle.reset();
}


void BlenderStyleToolRenderer::showSelectionRectangleCrosslines()
{
  selectionRectangleCrosslines = Application::developerHud().blenderStyleToolLayer()->createSelectionRectangleCrosslinesRenderer(controller->viewport().cast<vec2>());
}


void BlenderStyleToolRenderer::hideSelectionRectangleCrosslines()
{
  selectionRectangleCrosslines.reset();
}


void BlenderStyleToolRenderer::showTransformationLine(Base::ObjectEditor2D::Controller::TransformObjectPositionAction& action, bool rotateArrow)
{
  transformationLineRenderer = Application::developerHud().blenderStyleToolLayer()->createTransformationLine(controller->viewport().cast<vec2>(),
                                                                                                             controller->view()->transformFromModelToScreenSpace(action.originalCenterOfObjects),
                                                                                                             [this,&action](){return controller->view()->transformFromModelToScreenSpace(action.mousePosition()+action.originalMousePosition);},
                                                                                                             rotateArrow);

  action.signalFinished().connect(std::bind(&BlenderStyleToolRenderer::hideTransformationLine, this)).track(this->trackable);
}


void BlenderStyleToolRenderer::hideTransformationLine()
{
  transformationLineRenderer.reset();
}


} // namespace ObjectEditor2D
} // namespace Gorilla
} // namespace Editor
} // namespace Framework
