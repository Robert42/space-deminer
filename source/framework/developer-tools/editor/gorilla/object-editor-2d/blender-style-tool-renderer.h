#ifndef FRAMEWORK_EDITOR_GORILLA_BLENDERSTYLETOOLRENDERER_H
#define FRAMEWORK_EDITOR_GORILLA_BLENDERSTYLETOOLRENDERER_H

#include "../blender-style-tool-layer.h"

#include <framework/developer-tools/editor/base/object-editor-2d/controller.h>

namespace Framework {
namespace Editor {
namespace Gorilla {
namespace ObjectEditor2D {

class BlenderStyleToolRenderer final
{
public:
  typedef std::shared_ptr<BlenderStyleToolRenderer> Ptr;

public:
  Signals::Trackable trackable;

private:
  const Base::ObjectEditor2D::Controller::Ptr controller;

  std::shared_ptr<BlenderStyleToolLayer::SelectionRectangleRenderer> selectionRectangle;
  std::shared_ptr<BlenderStyleToolLayer::SelectionCircleRenderer> selectionCircle;
  std::shared_ptr<BlenderStyleToolLayer::SelectionRectangleCrosslinesRenderer> selectionRectangleCrosslines;
  std::shared_ptr<BlenderStyleToolLayer::TransformationLineRenderer> transformationLineRenderer;

public:
  BlenderStyleToolRenderer(const Base::ObjectEditor2D::Controller::Ptr& controller);
  ~BlenderStyleToolRenderer();

  static Ptr create(const Base::ObjectEditor2D::Controller::Ptr& controller);

private:
  void showSelectionRectangle(const vec2& from);
  void hideSelectionRectangle();
  void showSelectionCircle();
  void hideSelectionCircle();
  void showSelectionRectangleCrosslines();
  void hideSelectionRectangleCrosslines();
  void showTransformationLine(Base::ObjectEditor2D::Controller::TransformObjectPositionAction& action, bool rotateArrow);
  void hideTransformationLine();
};

} // namespace ObjectEditor2D
} // namespace Gorilla
} // namespace Editor
} // namespace Framework

#endif // FRAMEWORK_EDITOR_GORILLA_BLENDERSTYLETOOLRENDERER_H
