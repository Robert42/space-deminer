#include "developer-tools.h"

#include <framework/developer-tools/texture-browser.h>
#include <framework/developer-tools/code-generator-tool.h>
#include <framework/developer-tools/test-ingame-editor.h>
#include <framework/developer-tools/test-spatial-gui.h>
#include <framework/developer-tools/test-animations.h>
#include <framework/developer-tools/test-cegui.h>

namespace Framework {

DeveloperTools::DeveloperTools()
{
}

void DeveloperTools::startTextureBrowser()
{
  quitTextureBrowser();
  textureBrowser = std::shared_ptr<TextureBrowser>(new TextureBrowser);
}

void DeveloperTools::quitTextureBrowser()
{
  textureBrowser.reset();
}


void DeveloperTools::startCodeGenerator()
{
  quitCodeGenerator();
  codeGenerator = std::shared_ptr<CodeGeneratorTool>(new CodeGeneratorTool);
}

void DeveloperTools::quitCodeGenerator()
{
  codeGenerator.reset();
}


void DeveloperTools::startIngameEditor()
{
  quitIngameEditor();
  testIngameEditor = std::shared_ptr<TestIngameEditor>(new TestIngameEditor);
}

void DeveloperTools::quitIngameEditor()
{
  testIngameEditor.reset();
}


void DeveloperTools::startSpatialGui()
{
  quitSpatialGui();
  testSpatialGui = std::shared_ptr<TestSpatialGui>(new TestSpatialGui);
}

void DeveloperTools::quitSpatialGui()
{
  testSpatialGui.reset();
}


void DeveloperTools::startTestAnimations()
{
  quitTestAnimations();
  testAnimations = std::shared_ptr<TestAnimations>(new TestAnimations);
}

void DeveloperTools::quitTestAnimations()
{
  testAnimations.reset();
}

void DeveloperTools::startCEGUI()
{
  quitCEGUI();
  testCEGUI = std::shared_ptr<TestCEGUI>(new TestCEGUI);
}

void DeveloperTools::quitCEGUI()
{
  testCEGUI.reset();
}


} // namespace Framework
