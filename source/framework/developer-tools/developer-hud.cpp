#include <dependencies.h>
#include <framework/developer-tools/developer-hud.h>
#include <framework/window.h>
#include <framework/application.h>
#include <framework/developer-tools/developer-settings.h>
#include <framework/scripting/initialization.h>
#include <framework/developer-tools/editor/gorilla/blender-style-tool-layer.h>

#include "developer-hud/tweak-ui/tweak-ui-hud-layer.h"
#include "developer-hud/tweak-ui/gorilla/texture-atlas.h"
#include "developer-hud/tweak-ui/tweak-ui-mouse-cursor.h"
#include "developer-hud/dev-hud-mouse-cursor.h"
#include "developer-hud/terminal-hud.h"
#include "developer-hud/framerate-screen.h"

#include <framework/developer-tools/texture-viewer.h>

namespace Framework {
namespace Gorilla_TextureAtlas {
const char* freemono = "freemono";
const char* nodeEditor = freemono;
const char* gorillaBlenderStyleTool = "blender-style-tools";
const char* dejavu = "dejavu";
const char* TextureViewerBrowser = "texture-viewer-browser";
const char* IngameEditor = dejavu;
const char* TweakUI = "tweak-ui-font";
const char* tweakUICursors = "tweak-ui-cursors";
const char* mouseCursor = "mouse-pointer";
}

DeveloperHud::DeveloperHud()
{
  Ogre::Root& ogreRoot = Ogre::Root::getSingleton();

  gorillaSilverback_ = new OwnSilverback;
  tweakUITextureAtlas = OGRE_NEW TweakUI::Gorilla::TextureAtlas;

  gorillaSilverback().loadAtlas(Gorilla_TextureAtlas::dejavu);
  gorillaSilverback().loadAtlas(Gorilla_TextureAtlas::freemono);
  gorillaSilverback().loadAtlas(Gorilla_TextureAtlas::tweakUICursors);
  gorillaSilverback().loadAtlas(Gorilla_TextureAtlas::mouseCursor);
  gorillaSilverback().loadAtlas(Gorilla_TextureAtlas::TextureViewerBrowser);
  gorillaSilverback().loadAtlas(Gorilla_TextureAtlas::gorillaBlenderStyleTool);
  gorillaSilverback_->registerOwnTextureAtlas(Gorilla_TextureAtlas::TweakUI, tweakUITextureAtlas);

  sceneManager = ogreRoot.createSceneManager(Ogre::ST_GENERIC, "developer-hud");
  camera = sceneManager->createCamera("developer-hud-camera");
  viewport = RenderWindow::ogreRenderWindow()->addViewport(camera, std::numeric_limits<int>::max());

  viewport->setClearEveryFrame(false);

  gorillaScreenNodeEditor_ = gorillaSilverback().createScreen(viewport, Gorilla_TextureAtlas::nodeEditor);
  TextureViewer::_gorillaScreens[TextureViewer::Usage::TEXTURE_BROWSER] = gorillaSilverback().createScreen(viewport, Gorilla_TextureAtlas::TextureViewerBrowser);
  gorillaScreenBlenderStyleToolLayer_ = gorillaSilverback().createScreen(viewport, Gorilla_TextureAtlas::gorillaBlenderStyleTool);
  gorillaScreenAntTweakBar_ = gorillaSilverback().createScreen(viewport, Gorilla_TextureAtlas::TweakUI);
  gorillaScreenIngameEditor_ = gorillaSilverback().createScreen(viewport, Gorilla_TextureAtlas::IngameEditor);
  gorillaScreenMonospace_ = gorillaSilverback().createScreen(viewport, Gorilla_TextureAtlas::freemono);
  gorillaScreenTweakUIMouseCursor_ = gorillaSilverback().createScreen(viewport, Gorilla_TextureAtlas::tweakUICursors);
  gorillaScreenMouseCursor_ = gorillaSilverback().createScreen(viewport, Gorilla_TextureAtlas::mouseCursor);
}

DeveloperHud::~DeveloperHud()
{
  _blenderStyleToolLayer.reset();
  terminalHud.reset();

  allLayer.clear();

  Ogre::Root& ogreRoot = Ogre::Root::getSingleton();

  delete gorillaSilverback_;

  ogreRoot.destroySceneManager(sceneManager);
}

Gorilla::Silverback& DeveloperHud::gorillaSilverback()
{
  return *gorillaSilverback_;
}

Gorilla::Screen& DeveloperHud::gorillaScreenNodeEditor()
{
  return *gorillaScreenNodeEditor_;
}

Gorilla::Screen& DeveloperHud::gorillaScreenBlenderStyleToolLayer()
{
  return *gorillaScreenBlenderStyleToolLayer_;
}

Gorilla::Screen& DeveloperHud::gorillaScreenMonospace()
{
  return *gorillaScreenMonospace_;
}

Gorilla::Screen& DeveloperHud::gorillaScreenAntTweakBar()
{
  return *gorillaScreenAntTweakBar_;
}

Gorilla::Screen& DeveloperHud::gorillaScreenIngameEditor()
{
  return *gorillaScreenIngameEditor_;
}

Gorilla::Screen& DeveloperHud::gorillaScreenMouseCursor()
{
  return *gorillaScreenMouseCursor_;
}

Gorilla::Screen& DeveloperHud::gorillaScreenTweakUIMouseCursor()
{
  return *gorillaScreenTweakUIMouseCursor_;
}

void DeveloperHud::addLayer(const std::shared_ptr<Layer>& layer)
{
  allLayer.push_back(layer);
}

shared_ptr<DeveloperHud> DeveloperHud::create()
{
  bool withTerminal = Application::developerSettings().terminal.enable;

  typedef Framework::DeveloperHudElements::DefaultMouseCursor MouseCursor;
  typedef Framework::DeveloperHudElements::TerminalHud TerminalHud;

  std::shared_ptr<DeveloperHud> hud(new DeveloperHud());

  hud->addLayer(Layer::Ptr(new MouseCursor(*hud)));
  hud->addLayer(Layer::Ptr(new TweakUI::TweakUIHudLayer(*hud, hud->tweakUITextureAtlas)));
  hud->addLayer(Layer::Ptr(new TweakUI::MouseCursor(*hud)));
  if(withTerminal)
  {
    hud->terminalHud = TerminalHud::create(*hud);
    hud->_blenderStyleToolLayer = Editor::Gorilla::BlenderStyleToolLayer::create(*hud);

    hud->addLayer(hud->terminalHud);
    hud->addLayer(Layer::Ptr(new FramerateScreen(*hud)));
  }

  return hud;
}

void DeveloperHud::registerTerminalCommands()
{
  if(!terminalHud)
    return;

  const std::shared_ptr<Scripting::Index>& scriptIndex = terminalHud->scriptIndex();

  int r = 0;
  AngelScript::asIScriptEngine* scriptEngine = Scripting::Engine::angelScriptEngine();

  for(const Layer::Ptr& layer : allLayer)
  {
    // Each layer should be able to assue to have the global namepsace as active namespace
    r = scriptEngine->SetDefaultNamespace("");
    Scripting::AngelScriptCheck(r);

    // Each layer should be able to assue to have TERMINAL_COMMANDS as access mask
    uint32 oldMask = scriptEngine->SetDefaultAccessMask(Scripting::AccessMasks::TERMINAL_COMMANDS);

    try
    {
      layer->registerTerminalCommands(scriptIndex);
    }catch(...)
    {
      // In case of an exception, reset namespace ...
      r = scriptEngine->SetDefaultNamespace("");
      Scripting::AngelScriptCheck(r);

      // ... reset default access mask ...
      scriptEngine->SetDefaultAccessMask(oldMask);

      // ... rethrow
      throw;
    }

    // reset default access mas
    scriptEngine->SetDefaultAccessMask(oldMask);
  }

  r = scriptEngine->SetDefaultNamespace("");
  Scripting::AngelScriptCheck(r);
}


std::shared_ptr<Editor::Gorilla::BlenderStyleToolLayer>& DeveloperHud::blenderStyleToolLayer()
{
  return _blenderStyleToolLayer;
}


// ==== OwnSilverback ====

void DeveloperHud::OwnSilverback::registerOwnTextureAtlas(const Ogre::String& name, Gorilla::TextureAtlas* atlas)
{
  mAtlases[name] = atlas;
}


// ==== Screen ====

DeveloperHud::Layer::Layer(ZLayer z, Gorilla::Screen& gorillaScreen)
  : gorillaScreen(gorillaScreen),
    gorillaLayer_(gorillaScreen.createLayer(static_cast<uint>(z.value)))
{
  gorillaLayer().hide();
}

DeveloperHud::Layer::~Layer()
{
  gorillaScreen.destroy(&gorillaLayer());
}

Gorilla::Layer& DeveloperHud::Layer::gorillaLayer()
{
  return *gorillaLayer_;
}

void DeveloperHud::Layer::show()
{
  gorillaLayer().show();
}

void DeveloperHud::Layer::hide()
{
  gorillaLayer().hide();
}

void DeveloperHud::Layer::setVisible(bool visible)
{
  if(visible)
    show();
  else
    hide();
}

bool DeveloperHud::Layer::isVisible()
{
  return gorillaLayer().isVisible();
}

void DeveloperHud::Layer::registerTerminalCommands(const std::shared_ptr<Scripting::Index>&)
{
}



// ==== MonospaceLayer ====

DeveloperHud::MonospaceLayer::MonospaceLayer(ZLayer z, DeveloperHud& hud)
  : Layer(z, hud.gorillaScreenMonospace()),
    fontSmall(gorillaLayer(), 14),
    fontLarge(gorillaLayer(), 16)
{
}


DeveloperHud::MonospaceLayer::Font::Font(Gorilla::Layer& gorillaLayer, int lineHeight)
  : _lineHeight(lineHeight),
    gorillaLayer(gorillaLayer)
{
  scaleFactor = 1;

  Gorilla::GlyphData* glyphData = gorillaGlyphData(Terminal::Format::Light::NORMAL);
  assert(glyphData != nullptr);
  assert(_lineHeight == glyphData->mLineHeight);
  _lineSpacing = glyphData->mLineSpacing;
  _characterWidth = glyphData->mMonoWidth;
}

int DeveloperHud::MonospaceLayer::Font::gorillaIndex(Terminal::Format::Light light) const
{
  int weight = light==Terminal::Format::Light::BRIGHTER ? 2 : light==Terminal::Format::Light::NORMAL ? 1 : 0;

  return weight + 10*_lineHeight;
}

int DeveloperHud::MonospaceLayer::Font::characterWidth() const
{
  return _characterWidth * scaleFactor;
}

int DeveloperHud::MonospaceLayer::Font::lineSpacing() const
{
  return _lineSpacing * scaleFactor;
}

int DeveloperHud::MonospaceLayer::Font::lineHeight() const
{
  return _lineHeight * scaleFactor;
}

Gorilla::GlyphData* DeveloperHud::MonospaceLayer::Font::gorillaGlyphData(Terminal::Format::Light light) const
{
  return gorillaLayer._getGlyphData(gorillaIndex(light));
}


}
