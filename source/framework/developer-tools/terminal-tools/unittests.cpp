#include <framework/developer-tools/terminal.h>
#include <framework/private-rendering-loop.h>
#include <base/strings/application-argument-faker.h>
#include <base/strings/to-string.h>

namespace Base
{
  bool _fastTest = true;
  bool fastTest()
  {
    return _fastTest;
  }
}

namespace Framework {
inline void destroyResourceGroup()
{
  if(Ogre::ResourceGroupManager::getSingleton().resourceGroupExists(UnitTest_ResourceGroup))
    Ogre::ResourceGroupManager::getSingleton().destroyResourceGroup(UnitTest_ResourceGroup);
}

inline void createResourceGroup()
{
  destroyResourceGroup();
  Ogre::ResourceGroupManager::getSingleton().createResourceGroup(UnitTest_ResourceGroup);
}


// See http://code.google.com/p/googletest/wiki/V1_6_AdvancedGuide#Extending_Google_Test_by_Handling_Test_Events
class InternalTerminalTestPrinter : public ::testing::EmptyTestEventListener
{
private:
  typedef ::testing::UnitTest UnitTest;
  typedef ::testing::TestInfo TestInfo;
  typedef ::testing::TestCase TestCase;
  typedef ::testing::TestPartResult TestPartResult;
  typedef ::testing::TestResult TestResult;

  PrivateRenderLoop& privateRenderLoop;

public:
  InternalTerminalTestPrinter(PrivateRenderLoop& privateRenderLoop)
    : privateRenderLoop(privateRenderLoop)
  {
    // If this assertion gets hiz, most probably the source file has changed its location.
    // Thats ok, just update the InternalTerminalTestPrinter::interestingPartOfPath() method
    assert(getCurrentFile().find(interestingPartOfPath()) != String::npos);
    assert(getLocalPath(__FILE__) == interestingPartOfPath());
  }

private:
// Note: The formating code is based on the default formating code (see class PrettyUnitTestResultPrinter in gtest.cc from the gtest framework)
  void OnTestIterationStart(const UnitTest& unitTest, int iteration) override
  {
    int repeat = ::testing::GTEST_FLAG(repeat);
    std::string filter = ::testing::GTEST_FLAG(filter);
    bool shuffle = ::testing::GTEST_FLAG(shuffle);

    if(repeat != 1)
      Terminal::writeOut(brightWhite(),
                         "\nRepeating all tests (iteration %0) ...\n\n",
                         iteration + 1);

    if(filter != "*")
    {
      Terminal::writeOut(darkYellow(),
                         "Note: %0 filter = %1\n",
                         GTEST_NAME_,
                         filter);
    }

    if(shuffle)
    {
      Terminal::writeOut(darkYellow(),
                         "Note: Randomizing tests' orders with a seed of %0 .\n",
                         unitTest.random_seed());
    }

    writeLeftAlignedBock(darkGreen(),
                         "",
                         '=');
    Terminal::writeOut("Running %0 from %1.\n",
                       tests(unitTest.test_to_run_count()),
                       cases(unitTest.test_case_to_run_count()));

    suspend();
  }

  void OnEnvironmentsSetUpStart(const UnitTest&) override
  {
    writeLeftAlignedBock(darkGreen(),
                         "",
                         '-');
    Terminal::writeOut(darkWhite(),
                       "Global test environment set-up.\n");

    suspend();
  }

  void OnTestCaseStart(const TestCase& testCase) override
  {
    writeLeftAlignedBock(darkGreen(),
                         "",
                         '-');
    Terminal::writeOut(darkWhite(),
                       "%0 from %1\n",
                       tests(testCase.test_to_run_count()),
                       testCase.name());

    createResourceGroup();

    suspend();
  }

  void OnTestStart(const TestInfo& testInfo) override
  {
    writeLeftAlignedBock(darkGreen(), "run", ' ');
    Terminal::writeOut(darkWhite(),
                       "%0.%1\n",
                       testInfo.test_case_name(),
                       testInfo.name());

    suspend();
  }

  void OnTestPartResult(const TestPartResult& testPartResult)
  {
    if(testPartResult.failed())
    {
      Terminal::writeOut(normalRed(), "FAILURE");
      Terminal::writeOut(normalRed(), " in %0:%1\n",
                         getLocalPath(testPartResult.file_name()),
                         testPartResult.line_number());
      Terminal::writeOut("%0\n",
                         testPartResult.message());

      suspend();
    }
  }

  void OnTestEnd(const TestInfo& testInfo) override
  {
    if(testInfo.result()->Passed())
      writeRightAlignedBock(darkGreen(), "ok", ' ');
    else
      writeCenterAlignedBock(brightRed(), "failed", ' ');
    Terminal::writeOut(testInfo.result()->Passed() ? darkWhite() : normalWhite(),
                       "%0.%1 (%2 ms)\n",
                       testInfo.test_case_name(),
                       testInfo.name(),
                       testInfo.result()->elapsed_time());

    suspend();
  }

  void OnTestCaseEnd(const TestCase& testCase) override
  {
    writeLeftAlignedBock(testCase.Failed() ? darkRed() : darkGreen(),
                         "",
                         '-');
    Terminal::writeOut(darkWhite(),
                       "%0 from %1\n\n",
                       tests(testCase.test_to_run_count()),
                       testCase.name());

    destroyResourceGroup();

    suspend();
  }

  void OnEnvironmentsTearDownStart(const UnitTest&) override
  {
    writeLeftAlignedBock(darkGreen(),
                         "",
                         '-');
    Terminal::writeOut(darkWhite(),
                       "Global test environment tear-down.\n");

    suspend();
  }

  void OnTestIterationEnd(const UnitTest& unitTest, int /*iteration*/) override
  {
    writeLeftAlignedBock(darkGreen(),
                         "",
                         '=');
    Terminal::writeOut(darkWhite(),
                       "%0 from %1 ran. (%2 ms total)\n",
                       tests(unitTest.test_to_run_count()),
                       cases(unitTest.test_case_to_run_count()),
                       unitTest.elapsed_time());

    writeLeftAlignedBock(unitTest.successful_test_count()==0 ?
                         normalRed() :
                         normalGreen(),
                         "passed",
                         ' ');
    Terminal::writeOut(unitTest.successful_test_count()==0 ? darkRed() : darkGreen(),
                       "%0.\n",
                       tests(unitTest.successful_test_count()));

    if(unitTest.Failed())
    {
      writeLeftAlignedBock(normalRed(),
                           "failed",
                           ' ');
      Terminal::writeOut(normalRed(),
                         "%0, Listed below:\n",
                         tests(unitTest.failed_test_count()));

      for(int i=0; i<unitTest.total_test_case_count(); ++i)
      {
        const TestCase& testCase = *unitTest.GetTestCase(i);

        if(!testCase.should_run() || testCase.failed_test_count()==0)
          continue;

        for(int j=0; j<testCase.total_test_count(); ++j)
        {
          const TestInfo& testInfo = *testCase.GetTestInfo(j);

          if(!testInfo.should_run() || testInfo.result()->Passed())
            continue;

          writeLeftAlignedBock(normalRed(),
                               "failed",
                               ' ');
          Terminal::writeOut(normalRed(),
                             "%0.%1\n\n",
                             testInfo.test_case_name(),
                             testInfo.name());
        }
      }

      Terminal::writeOut(normalRed(),
                         "%0 FAILED\n",
                         tests(unitTest.failed_test_count()).upperCase());

      suspend();
    }
  }

private:
  void suspend()
  {
    privateRenderLoop.update();
  }

  static String getLocalPath(String path)
  {
    String currentFile = getCurrentFile();

    size_t commonPrefix = currentFile.find(interestingPartOfPath());
    assert(commonPrefix != String::npos);

    return path.trimFromLeft(commonPrefix);
  }

  static String getCurrentFile()
  {
    String thisFile = __FILE__;
    thisFile.replace('\\', '/');

    return thisFile;
  }

  static String interestingPartOfPath()
  {
    return "/source/framework/developer-tools/terminal-tools/unittests.cpp";
  }

  static String tests(int i)
  {
    return String::compose("%0 %1",
                           i,
                           i==1 ? "test" : "tests");
  }

  static String cases(int i)
  {
    return String::compose("%0 test %1",
                           i,
                           i==1 ? "case" : "cases");
  }

  static Terminal::Format darkWhite()
  {
    return Terminal::Format(Terminal::Format::Color::NORMAL, Terminal::Format::Light::DARKER);
  }

  static Terminal::Format brightWhite()
  {
    return Terminal::Format(Terminal::Format::Color::NORMAL, Terminal::Format::Light::BRIGHTER);
  }

  static Terminal::Format darkYellow()
  {
    return Terminal::Format(Terminal::Format::Color::YELLOW, Terminal::Format::Light::DARKER);
  }

  static Terminal::Format normalWhite()
  {
    return Terminal::Format(Terminal::Format::Color::NORMAL, Terminal::Format::Light::NORMAL);
  }

  static Terminal::Format darkGreen()
  {
    return Terminal::Format(Terminal::Format::Color::GREEN, Terminal::Format::Light::DARKER);
  }

  static Terminal::Format normalGreen()
  {
    return Terminal::Format(Terminal::Format::Color::GREEN, Terminal::Format::Light::NORMAL);
  }

  static Terminal::Format darkRed()
  {
    return Terminal::Format(Terminal::Format::Color::RED, Terminal::Format::Light::DARKER);
  }

  static Terminal::Format normalRed()
  {
    return Terminal::Format(Terminal::Format::Color::RED, Terminal::Format::Light::NORMAL);
  }

  static Terminal::Format brightRed()
  {
    return Terminal::Format(Terminal::Format::Color::RED, Terminal::Format::Light::BRIGHTER);
  }

  static void writeCenterAlignedBock(const Terminal::Format& format,
                                     const String& str,
                                     String::value_type fillWith)
  {
    Terminal::writeOut(format, getBlockCenterAligned(str, fillWith));
  }

  static void writeRightAlignedBock(const Terminal::Format& format,
                                    const String& str,
                                    String::value_type fillWith)
  {
    Terminal::writeOut(format, getBlockRightAligned(str, fillWith));
  }

  static void writeLeftAlignedBock(const Terminal::Format& format,
                                   const String& str,
                                   String::value_type fillWith)
  {
    Terminal::writeOut(format, getBlockLeftAligned(str, fillWith));
  }

  static String getBlockCenterAligned(const String& str, String::value_type fillWith)
  {
    int nFillCharactersNeeded = max<int>(0, nFillCharacters() - str.length());

    return getBlock(nFillCharactersNeeded/2, str, (nFillCharactersNeeded+1)/2, fillWith);
  }

  static String getBlockRightAligned(const String& str, String::value_type fillWith)
  {
    int nFillCharactersNeeded = max<int>(0, nFillCharacters() - str.length());
    int spacing = 0;

    if(nFillCharactersNeeded > 2)
    {
      spacing = 1;
      nFillCharactersNeeded--;
    }

    return getBlock(nFillCharactersNeeded, str, spacing, fillWith);
  }

  static String getBlockLeftAligned(const String& str, String::value_type fillWith)
  {
    int nFillCharactersNeeded = max<int>(0, nFillCharacters() - str.length());
    int spacing = 0;

    if(nFillCharactersNeeded > 2)
    {
      spacing = 1;
      nFillCharactersNeeded--;
    }

    return getBlock(spacing, str, nFillCharactersNeeded, fillWith);
  }

  static String getBlock(int leftFill, const String& str, int rightFill, String::value_type fillWith)
  {
    return String::compose("[%0%1%2] ",
                           String::fromUnivodeValue(fillWith, leftFill),
                           str.upperCase(),
                           String::fromUnivodeValue(fillWith, rightFill));
  }

  static int nFillCharacters()
  {
    return 10;
  }
};

void runUnitTests(PrivateRenderLoop& privateRenderLoop, bool fast)
{
  static bool firstRun = true;

  _fastTest = fast;

  ApplicationArgumentFaker applicationArgumentFaker({});

  ::testing::InitGoogleTest(applicationArgumentFaker.argc(), applicationArgumentFaker.argv());

  if(firstRun)
  {
    ::testing::TestEventListeners& listeners = ::testing::UnitTest::GetInstance()->listeners();
    listeners.Append(new InternalTerminalTestPrinter(privateRenderLoop));

    firstRun = false;
  }

  bool successful = RUN_ALL_TESTS() == 0;
  (void)successful;

  destroyResourceGroup();
}
}
