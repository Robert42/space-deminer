#ifndef FRAMEWORK_TESTCEGUI_H
#define FRAMEWORK_TESTCEGUI_H

#include <framework/window/user-input/mouse.h>
#include <framework/window/user-input/mouse-cursor-layers.h>
#include <framework/window/user-input/mouse-mode-layers.h>

#include <CEGUI/GUIContext.h>

namespace Framework {


class TestCEGUI final
{
  MouseModeLayers::Layer::Ptr mouseModeLayer;
  MouseCursorLayers::Layer::Ptr mouseCursorLayer;

  CEGUI::Window* myRoot;

public:
  TestCEGUI();
  ~TestCEGUI();

  void createTestGUI();
};


} // namespace Framework

#endif // FRAMEWORK_TESTCEGUI_H
