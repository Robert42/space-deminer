#include "code-node-editor.h"

#include <framework/developer-tools/gorilla-helper/viewport-renderer.h>
#include <framework/developer-tools/editor/base/object-editor-2d/selection-based-order.h>
#include <framework/application.h>

namespace Framework {

class CodeNodeEditor::NodeModel : public  Editor::Base::ObjectEditor2D::Model
{
public:
  typedef std::shared_ptr<NodeModel> Ptr;

  NodeModel()
    : Model(vec2(800))
  {
    addObject(Editor::Base::ObjectEditor2D::Object::Ptr(new Editor::Base::ObjectEditor2D::Object), Rectangle<vec2>(vec2(0), vec2(16)));
    addObject(Editor::Base::ObjectEditor2D::Object::Ptr(new Editor::Base::ObjectEditor2D::Object), Rectangle<vec2>(vec2(1000), vec2(1024)));
    addObject(Editor::Base::ObjectEditor2D::Object::Ptr(new Editor::Base::ObjectEditor2D::Object), Rectangle<vec2>(vec2(256), vec2(320)));
    addObject(Editor::Base::ObjectEditor2D::Object::Ptr(new Editor::Base::ObjectEditor2D::Object), Rectangle<vec2>(vec2(64), vec2(200)));
    addObject(Editor::Base::ObjectEditor2D::Object::Ptr(new Editor::Base::ObjectEditor2D::Object), Rectangle<vec2>(vec2(500), vec2(800)));
  }
};

class CodeNodeEditor::Renderer : public GorillaHelper::ViewportRenderer
{
private:
  Editor::Base::ObjectEditor2D::Renderer renderer;
  Editor::Base::ObjectEditor2D::Selection::Ptr selection;

public:
  Renderer(const Editor::Base::ObjectEditor2D::View::Ptr& view,
           const Editor::Base::ObjectEditor2D::Order::Ptr& order,
           const Editor::Base::ObjectEditor2D::Shape::Ptr& shape,
           const Editor::Base::ObjectEditor2D::Selection::Ptr& selection)
    : renderer(view, order, shape, selection),
      selection(selection)
  {
    renderer.signalMarkedAsDirty().connect(std::bind(&Renderer::makeDirty, this)).trackManually();
  }

  void  _redrawImplementation() override
  {
    this->mVertices.remove_all();

    Editor::Base::ObjectEditor2D::Order::Iterator::Ptr objectsToDraw = renderer.objectsToRender();

    while(objectsToDraw->hasNext())
    {
      Editor::Base::ObjectEditor2D::Object::Ptr object = objectsToDraw->next();

      vec4 color(vec3(0.5f), 1.f);

      if(selection->contains(object))
      {
        if(selection->active(object))
          color = vec4(vec3(1, 1, 0), 1);
        else
          color = vec4(vec3(1, 0.5f, 0), 1);
      }

      this->drawRectangle(renderer.view()->transformFromModelToScreenSpace(object->boundingRect()),
                          color);
    }
  }
};

CodeNodeEditor::CodeNodeEditor()
  : DeveloperHud::Layer(ZLayer::Value(0), Application::developerHud().gorillaScreenNodeEditor())
{
  model = NodeModel::Ptr(new NodeModel());
  Editor::Base::ObjectEditor2D::View::Ptr view = Editor::Base::ObjectEditor2D::View::create(model);
  selection = Editor::Base::ObjectEditor2D::Selection::create(model);
  Editor::Base::ObjectEditor2D::Order::Ptr order = Editor::Base::ObjectEditor2D::SelectionBasedOrder::create(selection);
  Editor::Base::ObjectEditor2D::Shape::Ptr shape = Editor::Base::ObjectEditor2D::Shape::createSimpleResizableRectangularShape(8.f);
  controller = Editor::Base::ObjectEditor2D::Controller::create(view,
                                                                order,
                                                                shape,
                                                                selection);
  inputHandler = Editor::BlenderStyleInput::ObjectEditor2D::InputHandler::create(controller,
                                                                                 InputDevice::ActionPriority::NODE_EDITOR,
                                                                                 InputDevice::ActionPriority::NODE_EDITOR_BLOCKER,
                                                                                 Mouse::mouseCursorLayers().codeNodeEditorLayer);
  blenderStyleToolRenderer = Editor::Gorilla::ObjectEditor2D::BlenderStyleToolRenderer::create(controller);
  renderer = new Renderer(view, order, shape, selection);

  gorillaLayer().appendCustomRenderer(renderer);

  show();
}

CodeNodeEditor::~CodeNodeEditor()
{
}

std::shared_ptr<Editor::Base::ObjectEditor2D::View> CodeNodeEditor::view()
{
  return controller->view();
}

} // namespace Framework
