#ifndef _FRAMEWORK_TERMINAL_CURSOR_H_
#define _FRAMEWORK_TERMINAL_CURSOR_H_

#include <dependencies.h>

#include <framework/developer-tools/terminal/buffer/terminal-buffer.h>
#include <base/signals/signal.h>
#include <base/optional.h>

namespace Framework {
namespace Terminal {
class Cursor final
{
public:
  typedef Buffer::Line Line;

public:
  Signals::Trackable trackable;

private:
  Buffer::Ptr _buffer;

  Signals::Connection bufferSignalConnection_handleClearedBuffer;
  Signals::Connection bufferSignalConnection_handleModifiedLine;
  Signals::Connection bufferSignalConnection_handleLineGoingToBeRemoved;

  Line::Ptr _line;
  size_t _positionInLine;

public:
  Cursor(const Buffer::Ptr& buffer, const Line::Ptr& line, size_t positionInLine);
  Cursor(const Buffer::Ptr& buffer);
  ~Cursor();

  Cursor(const Cursor& other);
  Cursor& operator=(const Cursor& other) = delete;
  Cursor& operator=(Cursor& other);

  void reset(Buffer::Ptr buffer);
  void reset(const Cursor& other);

  Buffer::ConstPtr buffer() const;
  const Buffer::Line::Ptr& line();
  Buffer::Line::ConstPtr line() const;
  size_t positionInLine() const;

  Optional<Line::Ptr> nextLine() const;

  void moveToFirstPositionInLine();
  void moveToFirstPositionInBuffer();
  void moveToLastPositionInLine();
  void moveToLastPositionInBuffer();

  void moveToPositionInLine(size_t position);
  void moveToPositionInBuffer(const Line::Ptr& line, size_t position);

  bool moveToNextLine();

  bool deleteFollowingCharacter();
  bool deleteLeadingCharacter();
  void deleteCharactersTo(size_t i);
  bool insertCharacter(String::value_type character);
  bool insertString(const String& str);
  void pushAtEndOfLine(const String& text, const Format& format = Format());
  void clearLine();
  void removeLine();

  bool operator<(const Cursor& other) const;
  bool operator>(const Cursor& other) const;
  bool operator<=(const Cursor& other) const;
  bool operator>=(const Cursor& other) const;
  bool operator==(const Cursor& other) const;
  bool operator!=(const Cursor& other) const;

private:
  Signals::CallableSignal<void()> _signalCursorWasMoved;

  void handleClearedBuffer();
  void handleModifiedLine(const Line::ConstPtr& modifiedLine);
  void handleLineGoingToBeRemoved(const Line::ConstPtr& lineGoingToBeRemoved);

  void connectToBufferSignals();
  void disconnectFromBufferSignals();

public:
  Signals::Signal<void()>& signalCursorWasMoved();
};

}
}

#endif
