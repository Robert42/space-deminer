#include <framework/developer-tools/terminal/terminal-manager.h>
#include <framework/developer-tools/terminal/private-interactive-area.h>
#include <framework/developer-tools/terminal/buffer/terminal-buffer.h>

namespace Framework {
namespace Terminal {


class WorkingTerminalImplementation : public Manager::Implementation
{
private:
  const Buffer::Ptr buffer;

  Private::InteractiveAreaManager interactiveAreaManager;

public:
  WorkingTerminalImplementation(const Buffer::Ptr& buffer)
    : buffer(buffer),
      interactiveAreaManager(buffer)
  {
  }

  shared_ptr<Private::InteractiveArea> createInteractiveArea(InteractiveArea::Order order) final override
  {
    shared_ptr<Private::InteractiveArea> area;

    interactiveAreaManager.tryCreateInteractiveArea(order, out(area));

    return area;
  }
};

class DummyTerminalImplementation : public Manager::Implementation
{
public:
  shared_ptr<Private::InteractiveArea> createInteractiveArea(InteractiveArea::Order) final override
  {
    return shared_ptr<Private::InteractiveArea>();
  }
};

Manager::Manager()
  : implementation(new DummyTerminalImplementation),
    _defaultOutput(InteractiveArea::Order::DEFAULT_OUTPUT)
{
}


Manager::Ptr Manager::create()
{
  return singleton();
}


const shared_ptr<Buffer>& Manager::activate()
{
  Manager::Ptr manager = singleton();

  if(manager->buffer)
    return manager->buffer;

  manager->buffer = Buffer::create();

  manager->implementation.reset(new WorkingTerminalImplementation(manager->buffer));

  return manager->buffer;
}


InteractiveArea& Manager::defaultOutput()
{
  return singleton()->_defaultOutput;
}


}
}
