#include <dependencies.h>
#include <framework/developer-tools/terminal/terminal-cursor.h>

namespace Framework {
namespace Terminal {
typedef Buffer::Line Line;


TEST(framework_terminal_TerminalCursor, disconnectFromBufferSignals)
{
  Buffer::Ptr bufferA = Buffer::create();
  Buffer::Ptr bufferB = Buffer::create();

  bufferA->push("a\nb\nc");
  bufferB->push("a\nb\nc");

  Cursor c(bufferA);

  c.reset(bufferB);
  c.moveToFirstPositionInBuffer();

  EXPECT_EQ(bufferB->firstLine(), c.line());

  bufferA->clear();

  EXPECT_EQ(bufferB->firstLine(), c.line());
}

TEST(framework_terminal_TerminalCursor, cursor_at_empty_buffer)
{
  Buffer::Ptr buffer = Buffer::create();
  Cursor cursor(buffer);

  ASSERT_EQ(buffer->firstLine(), cursor.line());
}

TEST(framework_terminal_TerminalCursor, ensure_cursor_is_not_getting_invalid_when_line_is_removed)
{
  Buffer::Ptr buffer = Buffer::create();
  Cursor cursor(buffer);

  buffer->push("a\nb\nc\nd");

  EXPECT_EQ("a", cursor.line()->content());

  buffer->removeLine(buffer->firstLine());

  EXPECT_EQ("b", cursor.line()->content());
}

TEST(framework_terminal_TerminalCursor, ensure_cursor_is_not_getting_invalid_when_buffer_is_cleared)
{
  Buffer::Ptr buffer = Buffer::create();
  Cursor cursor(buffer);

  buffer->push("a\nb\nc\nd");

  EXPECT_EQ("a", cursor.line()->content());

  buffer->clear();

  EXPECT_EQ(buffer->firstLine(), cursor.line());
}


}
}
