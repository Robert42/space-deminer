#include <framework/developer-tools/terminal/buffer/terminal-buffer.h>


namespace Framework {
namespace Terminal {


Buffer::Line::Part::Part(String::size_type indexBegin,
                         String::size_type indexEnd,
                         const Format& format,
                         const Line& line)
  : line(line),
    indexBegin(indexBegin),
    indexEnd(indexEnd),
    format(format)
{
  classInvariant();
}


Buffer::Ptr Buffer::create()
{
  return Ptr(new Buffer());
}


String Buffer::Line::Part::asString() const
{
  return String(this->begin(), this->end());
}


Buffer::Line::Part::CharacterRange Buffer::Line::Part::characerRange() const
{
  return CharacterRange(this->indexBegin, this->indexEnd);
}

bool Buffer::Line::Part::operator==(const Part& other) const
{
  return this->indexBegin==other.indexBegin &&
         this->indexEnd==other.indexEnd &&
         this->format==other.format &&
         &this->line == &other.line;
}

bool Buffer::Line::Part::operator!=(const Part& other) const
{
  return !(*this == other);
}


const String::value_type* Buffer::Line::Part::begin() const
{
  return pointerFromIndex(this->indexBegin);
}


const String::value_type* Buffer::Line::Part::end() const
{
  return pointerFromIndex(this->indexEnd);
}


Buffer::Line::Part& Buffer::Line::Part::operator=(const Part& other)
{
  if(&other.line == &this->line)
  {
    this->indexBegin = other.indexBegin;
    this->indexEnd = other.indexEnd;
    this->format = other.format;
  }else
  {
    assert("Buffer::Line::Part::operator=(): assigning a part from a different line is not allowed" && false);
  }

  classInvariant();

  return *this;
}


const String::value_type* Buffer::Line::Part::pointerFromIndex(String::size_type index) const
{
  return line.content().toUtf32String().c_str() + index;
}


void Buffer::Line::Part::classInvariant() const
{
  assert(this->indexBegin <= this->indexEnd);
}


Buffer::Line::Line(const String& content, const Format& format)
{
  clear(content, format);
}

Buffer::Line::Line(const Line& other)
{
  *this = other;
}

Buffer::Line::Ptr Buffer::Line::create(const String& content, const Format& format)
{
  return Ptr(new Line(content, format));
}


Buffer::Line& Buffer::Line::operator=(const Line& other)
{
  this->_content = other.content();
  this->_parts.clear();

  for(const Part& part : other.parts())
  {
    this->_parts.push_back(Part(part.indexBegin,
                                part.indexEnd,
                                part.format,
                                *this));
  }

  return *this;
}


const String& Buffer::Line::content() const
{
  return _content;
}


size_t Buffer::Line::length() const
{
  return content().length();
}


const std::list<Buffer::Line::Part>& Buffer::Line::parts() const
{
  return this->_parts;
}


void Buffer::Line::clear(const String& content, const Format& format)
{
  this->_parts.clear();
  this->_content = content;

  _parts.push_back(Part(0,
                        content.length(),
                        format,
                        *this));
}


void Buffer::Line::append(const String& text, const Format& format)
{
  if(text.empty())
    return;

  this->_content += text;

  Part& lastPart = *this->_parts.rbegin();

  if(lastPart.format == format || lastPart.begin() == lastPart.end())
  {
    lastPart.indexEnd = content().length();
    lastPart.format = format;
  }else
  {
    this->_parts.push_back(Part(lastPart.indexEnd,
                                content().length(),
                                format,
                                *this));
  }
}


void Buffer::Line::deleteCharacter(size_t position)
{
  this->_content = this->content().subString(0, position) + this->content().subString(position+1);

  for(std::list<Part>::iterator i=_parts.begin(); i!=_parts.end();)
  {
    Part& p = *i;

    if(p.indexBegin > position)
      p.indexBegin--;
    if(p.indexEnd > position)
      p.indexEnd--;

    p.classInvariant();

    if(p.indexBegin == p.indexEnd)
      i = _parts.erase(i);
    else
      ++i;
  }

  if(_parts.empty())
    clear("");
}


void Buffer::Line::insertCharacter(size_t position, String::value_type character)
{
  if(character == '\n')
    return;

  position = min(this->length(), position);

  this->_content = this->content().subString(0, position) + String::fromUnivodeValue(character) + this->content().subString(position);

  for(Part& p : _parts)
  {
    if(p.indexBegin >= position && position != 0)
      p.indexBegin++;
    if(p.indexEnd >= position)
      p.indexEnd++;

    p.classInvariant();
  }
}

bool Buffer::Line::reformat(const Format& newFormat,
                            size_t begin,
                            size_t end)
{
  size_t length = this->length();

  end = min(end, length);

  Part newPart(begin, end, newFormat, *this);

  Part::CharacterRange newPartRange = newPart.characerRange();

  for(const Part& oldPart : _parts)
  {
    if(oldPart == newPart)
      return false;
    if(newFormat==oldPart.format && newPartRange.isSubsetOf(oldPart.characerRange()))
      return false;
  }

  if(!newPartRange.isValid())
    return false;
  if(newPartRange.empty())
    return false;
  std::list<Part> oldParts;
  oldParts.swap(_parts);

  bool newPartAdded = false;

  for(Part oldPart : oldParts)
  {
    assert(!oldPart.characerRange().empty());

    const Part::CharacterRange oldPartRange = oldPart.characerRange();

    assert(oldPartRange.isValid());

    if(oldPartRange.overlap(newPartRange))
    {
      if(oldPartRange.begin < newPartRange.begin)
      {
        oldPart.indexEnd = newPart.indexBegin;

        _parts.push_back(oldPart);

        // reset in case it will be added a second time after the new  part
        oldPart.indexEnd = oldPartRange.end;
      }

      if(!newPartAdded)
      {
        _parts.push_back(newPart);
        newPartAdded = true;
      }

      if(oldPartRange.end > newPartRange.end)
      {
        oldPart.indexBegin = newPart.indexEnd;
        _parts.push_back(oldPart);
      }
    }else
    {
      _parts.push_back(oldPart);
    }
  }

  mergePartsIfPossible();
  return true;
}


void Buffer::Line::mergePartsIfPossible()
{
  std::list<Part>::iterator next = _parts.begin();
  for(std::list<Part>::iterator current=_parts.begin(); next!=_parts.end(); current = next)
  {
    ++next;
    if(next!=_parts.end() && current->format == next->format)
    {
      next->indexBegin = current->indexBegin;

      _parts.erase(current);
    }
  }
}




Buffer::Buffer()
{
  clear();
}


Buffer::~Buffer()
{
}


Buffer::Line::ConstPtr Buffer::firstLine() const
{
  return *this->lines.begin();
}


Buffer::Line::ConstPtr Buffer::lastLine() const
{
  return *this->lines.rbegin();
}


Buffer::Line::Ptr Buffer::firstLine()
{
  return *this->lines.begin();
}


Buffer::Line::Ptr Buffer::lastLine()
{
  return *this->lines.rbegin();
}


Buffer::Line::Ptr Buffer::push(const Line::Ptr& line, const String& text, const Format& format)
{
  std::list<String> textLines = text.split('\n', false);

  assert(textLines.size() > 0);
  assert(this->lines.size() > 0);

  // append the first line to the current line
  String firstTextLine = *textLines.begin();
  textLines.pop_front();
  if(!firstTextLine.empty())
  {
    line->append(firstTextLine, format);
    _signalLineModified(lastLine());
  }

  iterator line_iterator = find(line);

  // create a new Line for the rest
  for(const String& l : textLines)
  {
    ++line_iterator;
    line_iterator = this->lines.insert(line_iterator, Line::Ptr(new Line(l, format)));
    _signalLineInserted(*line_iterator);
  }

  return *line_iterator;
}


Buffer::Line::Ptr Buffer::push(const String& text, const Format& format)
{
  return push(lastLine(), text, format);
}


Buffer::Line::Ptr Buffer::prependEmptyLine()
{
  Line::Ptr l(new Line());

  this->lines.push_front(l);

  return l;
}



void Buffer::clear()
{
  this->lines.clear();
  this->lines.push_back(Line::Ptr(new Line()));

  _signalCleared();
}


size_t Buffer::numberLines() const
{
  return this->lines.size();
}


Buffer::iterator Buffer::begin()
{
  return this->lines.begin();
}


Buffer::iterator Buffer::end()
{
  return this->lines.end();
}


Buffer::reverse_iterator Buffer::rbegin()
{
  return this->lines.rbegin();
}


Buffer::reverse_iterator Buffer::rend()
{
  return this->lines.rend();
}


Buffer::const_iterator Buffer::begin() const
{
  return this->lines.begin();
}


Buffer::const_iterator Buffer::end() const
{
  return this->lines.end();
}


Buffer::const_reverse_iterator Buffer::rbegin() const
{
  return this->lines.rbegin();
}


Buffer::const_reverse_iterator Buffer::rend() const
{
  return this->lines.rend();
}


bool Buffer::areThisLinesConsecutive(const Line::ConstPtr& a, const Line::ConstPtr& b) const
{
  for(const_reverse_iterator i = rbegin(); i!=rend(); ++i)
  {
    if(*i == b)
      return true;
    if(*i == a)
      return false;
  }

  throw std::logic_error("called TerminalBuffer::areThisLinesConsecutive with two buffers not beeing contained in the buffer");
}


bool Buffer::containsLine(const Line::ConstPtr& line) const
{
  return rfind(line) != rend();
}


Signals::Signal<void(const Buffer::Line::ConstPtr&)>& Buffer::signalLineModified()
{
  return _signalLineModified;
}


Signals::Signal<void(const Buffer::Line::ConstPtr&)>& Buffer::signalLineInserted()
{
  return _signalLineInserted;
}


Signals::Signal<void(const Buffer::Line::ConstPtr&)>& Buffer::signalGoingToRemoveLine()
{
  return _signalGoingToRemoveLine;
}


Signals::Signal<void(const Buffer::Line::ConstPtr&)>& Buffer::signalLineRemoved()
{
  return _signalLineRemoved;
}


Signals::Signal<void()>& Buffer::signalCleared()
{
  return _signalCleared;
}


bool Buffer::areThisLinesConsecutive(const std::initializer_list<Line::ConstPtr>& lines) const
{
  Line::ConstPtr prevLine;

  for(const Line::ConstPtr& line : lines)
  {
    if(prevLine)
    {
      if(!areThisLinesConsecutive(prevLine, line))
        return false;
    }

    prevLine = line;
  }

  return true;
}


void Buffer::deleteCharacterInLine(const Line::Ptr& line, size_t position)
{
  if(position >= line->length())
    return;

  line->deleteCharacter(position);

  _signalLineModified(line);
}


void Buffer::insertCharacterIntoLine(const Line::Ptr& line, size_t position, String::value_type character)
{
  line->insertCharacter(position, character);

  _signalLineModified(line);
}

void Buffer::reformat(const Line::Ptr& line,
                      const Format& format,
                      size_t begin,
                      size_t end)
{
  if(line->reformat(format, begin, end))
    _signalLineModified(line);
}

void Buffer::clearLine(const Line::Ptr& line)
{
  if(line->length() == 0)
    return;

  line->clear();
  _signalLineModified(line);
}

void Buffer::removeLine(Line::Ptr line)
{
  if(lines.size() == 1)
  {
    clearLine(line);
    return;
  }

  _signalGoingToRemoveLine(line);
  lines.remove(line);
  _signalLineRemoved(line);
}

Buffer::iterator Buffer::find(const Line::ConstPtr& line)
{
  reverse_iterator i = rfind(line);

  if(i == rend())
    return end();
  iterator j = i.base();
  --j;
  return j;
}

Buffer::reverse_iterator Buffer::rfind(const Line::ConstPtr& line)
{
  return std::find(lines.rbegin(),
                   lines.rend(),
                   line);
}

Buffer::const_iterator Buffer::find(const Line::ConstPtr& line) const
{
  return const_cast<Buffer*>(this)->find(line);
}

Buffer::const_reverse_iterator Buffer::rfind(const Line::ConstPtr& line) const
{
  return const_cast<Buffer*>(this)->rfind(line);
}

Optional<Buffer::Line::Ptr> Buffer::nextLineOf(const Line::ConstPtr& line)
{
  if(line == lastLine())
    return nothing;

  const_reverse_iterator i = rfind(line);

  if(i == rend())
    return nothing;

  --i;

  return *i;
}


Optional<Buffer::Line::Ptr> Buffer::prevLineOf(const Line::ConstPtr& line)
{
  if(line == firstLine())
    return nothing;

  const_reverse_iterator i = rfind(line);

  if(i == rend())
    return nothing;

  ++i;

  return *i;
}


String Buffer::asPlainText() const
{
  String plainText;

  for(const Line::Ptr& line : *this)
    plainText += line->content() + "\n";

  return plainText.trimFromRight(1);
}


}
}
