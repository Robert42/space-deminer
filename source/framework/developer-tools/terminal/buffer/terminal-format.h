#ifndef _FRAMEWORK_TERMINAL_FORMAT_H_
#define _FRAMEWORK_TERMINAL_FORMAT_H_


#include <dependencies.h>

#include <base/enum-macros.h>


namespace Framework {
using namespace Base;


namespace Terminal {

class Format
{
public:
  BEGIN_ENUMERATION(Color,)
    NORMAL,
    RED,
    GREEN,
    BLUE,
    YELLOW,
    ORANGE,
    MAGENTA,
    CYAN
  ENUMERATION_BASIC_OPERATORS(Color)
  END_ENUMERATION;

  BEGIN_ENUMERATION(Light,)
    DARKER,
    NORMAL,
    BRIGHTER
  ENUMERATION_BASIC_OPERATORS(Light)
  END_ENUMERATION;

public:
  Light::Value light : 2;
  Color::Value color : 6;

public:
  Format(Color color=Color::NORMAL, Light light=Light::NORMAL);
  Format(Light light);

  bool operator==(const Format& other)const;
  bool operator!=(const Format& other)const;
  bool operator<(const Format& other)const;
  bool operator<=(const Format& other)const;
  bool operator>(const Format& other)const;
  bool operator>=(const Format& other)const;

private:
  int asInteger() const;
};

}
}

#endif
