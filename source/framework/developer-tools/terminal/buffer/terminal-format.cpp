#include <framework/developer-tools/terminal/buffer/terminal-format.h>


namespace Framework {
namespace Terminal {
Format::Format(Color color, Light light)
{
  this->color = color.value;
  this->light = light.value;
}

Format::Format(Light light) : Format(Color::NORMAL, light)
{
}

bool Format::operator==(const Format& other)const
{
  return this->asInteger() == other.asInteger();
}

bool Format::operator!=(const Format& other)const
{
  return this->asInteger() != other.asInteger();
}

bool Format::operator<(const Format& other)const
{
  return this->asInteger() < other.asInteger();
}

bool Format::operator<=(const Format& other)const
{
  return this->asInteger() <= other.asInteger();
}

bool Format::operator>(const Format& other)const
{
  return this->asInteger() > other.asInteger();
}

bool Format::operator>=(const Format& other)const
{
  return this->asInteger() >= other.asInteger();
}

int Format::asInteger() const
{
  return (static_cast<unsigned int>(this->color)<<8) | static_cast<unsigned int>(this->light);
}


}
}
