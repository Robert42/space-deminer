#include <framework/developer-tools/terminal/buffer/terminal-buffer.h>
#include <base/container-utilities.h>


namespace Framework {
namespace Terminal {


TEST(framework_terminal_TerminalBuffer_Line, assign_operator)
{
  Buffer::Line a, b;

  a = b;

  EXPECT_EQ(&a, &a.parts().begin()->line);
}

TEST(framework_terminal_TerminalBuffer_Line, clear)
{
  Buffer::Line line("ABCD", Format(Format::Light::BRIGHTER));

  EXPECT_EQ("ABCD", line.content());
  EXPECT_EQ("ABCD", line.parts().begin()->asString());
  EXPECT_EQ(Format(Format::Light::BRIGHTER), line.parts().begin()->format);

  line.clear("");

  EXPECT_EQ("", line.content());
  EXPECT_EQ("", line.parts().begin()->asString());
  EXPECT_EQ(Format(), line.parts().begin()->format);
}

TEST(framework_terminal_TerminalBuffer_Line, clear_simple)
{
  Buffer::Line line("Echo");

  line.clear("Blub", Format(Format::Light::BRIGHTER));

  EXPECT_EQ("Blub", line.content());
  EXPECT_EQ("Blub", line.parts().begin()->asString());
  EXPECT_EQ(Format(Format::Light::BRIGHTER), line.parts().begin()->format);
}

TEST(framework_terminal_TerminalBuffer_Line, append_empty)
{
  Buffer::Line line("abcd");

  line.append("");
  line.append("");
  line.append("");
  line.append("");
  line.append("");
  line.append("");

  ASSERT_EQ(static_cast<size_t>(1), line.parts().size());

  EXPECT_EQ("abcd", line.content());
  EXPECT_EQ("abcd", line.parts().begin()->asString());
}

TEST(framework_terminal_TerminalBuffer_Line, append_merging_same_format)
{
  Buffer::Line line("abcd");

  line.append("");
  line.append("");
  line.append("");
  line.append("");
  line.append("");
  line.append("e");

  ASSERT_EQ(static_cast<size_t>(1), line.parts().size());

  EXPECT_EQ("abcde", line.content());
  EXPECT_EQ("abcde", line.parts().begin()->asString());
}

TEST(framework_terminal_TerminalBuffer_Line, append_different_format)
{
  Buffer::Line line("abcd");

  line.append("");
  line.append("", Format(Format::Light::BRIGHTER));
  line.append("");
  line.append("e", Format(Format::Light::BRIGHTER));

  ASSERT_EQ(static_cast<size_t>(2), line.parts().size());

  EXPECT_EQ("abcde", line.content());
  EXPECT_EQ("abcd", line.parts().begin()->asString());
  EXPECT_EQ(Format(), line.parts().begin()->format);
  EXPECT_EQ("e", line.parts().rbegin()->asString());
  EXPECT_EQ(Format(Format::Light::BRIGHTER), line.parts().rbegin()->format);
}

TEST(framework_terminal_TerminalBuffer_Line, append_same_format)
{
  Buffer::Line line("abcd");

  line.append("");
  line.append("");
  line.append("", Format(Format::Light::BRIGHTER));
  line.append("");
  line.append("");
  line.append("e");

  ASSERT_EQ(static_cast<size_t>(1), line.parts().size());

  EXPECT_EQ("abcde", line.content());
  EXPECT_EQ("abcde", line.parts().begin()->asString());
  EXPECT_EQ(Format(), line.parts().begin()->format);
}

TEST(framework_terminal_TerminalBuffer_Line, append_different_format_to_empty_line)
{
  Buffer::Line line("");

  line.append("a", Format(Format::Light::BRIGHTER));

  ASSERT_EQ(static_cast<size_t>(1), line.parts().size());

  EXPECT_EQ("a", line.content());
  EXPECT_EQ(Format(Format::Light::BRIGHTER), line.parts().begin()->format);
}

TEST(framework_terminal_TerminalBuffer_Line, append_different_format_to_empty_part)
{
  Buffer::Line line("");

  line.append("a", Format(Format::Light::BRIGHTER));
  line.append("b", Format());
  line.append("", Format(Format::Light::BRIGHTER));
  line.append("", Format(Format::Light::BRIGHTER));
  line.append("", Format(Format::Light::BRIGHTER));
  line.append("c", Format());
  line.append("d", Format(Format::Light::BRIGHTER));

  EXPECT_EQ("abcd", line.content());

  ASSERT_EQ(static_cast<size_t>(3), line.parts().size());

  std::list<Buffer::Line::Part>::const_iterator iter = line.parts().begin();

  EXPECT_EQ(Format(Format::Light::BRIGHTER), iter->format);
  EXPECT_EQ("a", iter->asString());
  EXPECT_EQ(Format(), (++iter)->format);
  EXPECT_EQ("bc", iter->asString());
  EXPECT_EQ(Format(Format::Light::BRIGHTER), (++iter)->format);
  EXPECT_EQ("d", iter->asString());
}

TEST(framework_terminal_TerminalBuffer_Line, deleteCharacter_delete_random_character)
{
  Buffer::Line line("abXcd");

  line.deleteCharacter(2);

  EXPECT_EQ("abcd", line.content());
  EXPECT_EQ("abcd", line.parts().begin()->asString());
}

TEST(framework_terminal_TerminalBuffer_Line, deleteCharacter_delete_first_character)
{
  Buffer::Line line("Xabcd");

  line.deleteCharacter(0);

  EXPECT_EQ("abcd", line.content());
  EXPECT_EQ("abcd", line.parts().begin()->asString());
}

TEST(framework_terminal_TerminalBuffer_Line, deleteCharacter_delete_last_character)
{
  Buffer::Line line("abcdX");

  line.deleteCharacter(4);

  EXPECT_EQ("abcd", line.content());
  EXPECT_EQ("abcd", line.parts().begin()->asString());
}

TEST(framework_terminal_TerminalBuffer_Line, deleteCharacter_delete_invalid)
{
  Buffer::Line line("abcd");

  line.deleteCharacter(4);

  EXPECT_EQ("abcd", line.content());
  EXPECT_EQ("abcd", line.parts().begin()->asString());
}

TEST(framework_terminal_TerminalBuffer_Line, deleteCharacter_delete_first_of_format)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Buffer::Line line("ab");

  line.append("XCD", Format(Format::Light::BRIGHTER));
  line.append("ef", Format());

  line.deleteCharacter(2);

  std::list<Part>::const_iterator i = line.parts().begin();
  const Part& p0 = *(i++);
  const Part& p1 = *(i++);
  const Part& p2 = *(i++);
  EXPECT_EQ(line.parts().end(), i);

  EXPECT_EQ("abCDef", line.content());
  EXPECT_EQ("ab", p0.asString());
  EXPECT_EQ("CD", p1.asString());
  EXPECT_EQ("ef", p2.asString());
}

TEST(framework_terminal_TerminalBuffer_Line, deleteCharacter_delete_last_of_format)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line("ab");

  line.append("CDX", Format(Format::Light::BRIGHTER));
  line.append("ef", Format());

  line.deleteCharacter(4);

  std::list<Part>::const_iterator i = line.parts().begin();
  const Part& p0 = *(i++);
  const Part& p1 = *(i++);
  const Part& p2 = *(i++);
  EXPECT_EQ(line.parts().end(), i);

  EXPECT_EQ("abCDef", line.content());
  EXPECT_EQ("ab", p0.asString());
  EXPECT_EQ("CD", p1.asString());
  EXPECT_EQ("ef", p2.asString());
}

TEST(framework_terminal_TerminalBuffer_Line, deleteCharacter_delete_only_one_of_format)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line("ab");

  line.append("X", Format(Format::Light::BRIGHTER));
  line.append("cd", Format());

  line.deleteCharacter(2);

  std::list<Part>::const_iterator i = line.parts().begin();
  const Part& p0 = *(i++);
  const Part& p1 = *(i++);
  EXPECT_EQ(line.parts().end(), i);

  EXPECT_EQ("abcd", line.content());
  EXPECT_EQ("ab", p0.asString());
  EXPECT_EQ("cd", p1.asString());
}

TEST(framework_terminal_TerminalBuffer_Line, deleteCharacter_delete_whole_line)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line("a", Format(Format::Light::BRIGHTER));

  line.deleteCharacter(0);

  ASSERT_FALSE(line.parts().empty());

  std::list<Part>::const_iterator i = line.parts().begin();
  const Part& p0 = *(i++);
  EXPECT_EQ(line.parts().end(), i);

  EXPECT_EQ("", line.content());
  EXPECT_EQ("", p0.asString());
  EXPECT_EQ(Format(), p0.format);
}

TEST(framework_terminal_TerminalBuffer_Line, insertCharacter_insert_random_character)
{
  Buffer::Line line("abcd");

  line.insertCharacter(2, 'X');

  EXPECT_EQ("abXcd", line.content());
  EXPECT_EQ("abXcd", line.parts().begin()->asString());
}

TEST(framework_terminal_TerminalBuffer_Line, insertCharacter_insert_first_character)
{
  Buffer::Line line("abcd");

  line.insertCharacter(0, 'X');

  EXPECT_EQ("Xabcd", line.content());
  EXPECT_EQ("Xabcd", line.parts().begin()->asString());
}

TEST(framework_terminal_TerminalBuffer_Line, insertCharacter_insert_last_character)
{
  Buffer::Line line("abcd");

  line.insertCharacter(4, 'X');

  EXPECT_EQ("abcdX", line.content());
  EXPECT_EQ("abcdX", line.parts().begin()->asString());
}

TEST(framework_terminal_TerminalBuffer_Line, deleteCharacter_insert_invalid)
{
  Buffer::Line line("abcd");

  line.insertCharacter(5, 'X');

  EXPECT_EQ("abcdX", line.content());
  EXPECT_EQ("abcdX", line.parts().begin()->asString());
}

TEST(framework_terminal_TerminalBuffer_Line, insertCharacter_insert_first_of_format)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line("ab");

  line.append("CD", Format(Format::Light::BRIGHTER));
  line.append("ef", Format());

  line.insertCharacter(2, 'X');

  std::list<Part>::const_iterator i = line.parts().begin();
  const Part& p0 = *(i++);
  const Part& p1 = *(i++);
  const Part& p2 = *(i++);
  EXPECT_EQ(line.parts().end(), i);

  EXPECT_EQ("abXCDef", line.content());
  EXPECT_EQ("abX", p0.asString());
  EXPECT_EQ("CD", p1.asString());
  EXPECT_EQ("ef", p2.asString());
}

TEST(framework_terminal_TerminalBuffer_Line, insertCharacter_insert_last_of_format)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line("ab");

  line.append("CD", Format(Format::Light::BRIGHTER));
  line.append("ef", Format());

  line.insertCharacter(4, 'X');

  std::list<Part>::const_iterator i = line.parts().begin();
  const Part& p0 = *(i++);
  const Part& p1 = *(i++);
  const Part& p2 = *(i++);
  EXPECT_EQ(line.parts().end(), i);

  EXPECT_EQ("abCDXef", line.content());
  EXPECT_EQ("ab", p0.asString());
  EXPECT_EQ("CDX", p1.asString());
  EXPECT_EQ("ef", p2.asString());
}

TEST(framework_terminal_TerminalBuffer_Line, insertCharacter_insert_before_only_one_of_format)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line("a");

  line.append("b", Format(Format::Light::BRIGHTER));
  line.append("c", Format());

  line.insertCharacter(1, 'X');

  std::list<Part>::const_iterator i = line.parts().begin();
  const Part& p0 = *(i++);
  const Part& p1 = *(i++);
  const Part& p2 = *(i++);
  EXPECT_EQ(line.parts().end(), i);

  EXPECT_EQ("aXbc", line.content());
  EXPECT_EQ("aX", p0.asString());
  EXPECT_EQ("b", p1.asString());
  EXPECT_EQ("c", p2.asString());
}

TEST(framework_terminal_TerminalBuffer_Line, insertCharacter_insert_after_only_one_of_format)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line("a");

  line.append("b", Format(Format::Light::BRIGHTER));
  line.append("c", Format());

  line.insertCharacter(2, 'X');

  std::list<Part>::const_iterator i = line.parts().begin();
  const Part& p0 = *(i++);
  const Part& p1 = *(i++);
  const Part& p2 = *(i++);
  EXPECT_EQ(line.parts().end(), i);

  EXPECT_EQ("abXc", line.content());
  EXPECT_EQ("a", p0.asString());
  EXPECT_EQ("bX", p1.asString());
  EXPECT_EQ("c", p2.asString());
}

TEST(framework_terminal_TerminalBuffer_Line, insertCharacter_insert_to_empty_line)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line("", Format(Format::Light::BRIGHTER));

  line.insertCharacter(0, 'X');

  std::list<Part>::const_iterator i = line.parts().begin();
  const Part& p0 = *(i++);
  EXPECT_EQ(line.parts().end(), i);

  EXPECT_EQ("X", line.content());
  EXPECT_EQ("X", p0.asString());
  EXPECT_EQ(Format(Format::Light::BRIGHTER), p0.format);
}

TEST(framework_terminal_TerminalBuffer_Line, reformat_replace_whole)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line;
  line.append("abc");
  line.append("def", Format(Format::Light::BRIGHTER));
  line.append("ghi", Format());

  EXPECT_TRUE(line.reformat(Format(Format::Light::DARKER), 0, 9));

  std::list<Part> parts = line.parts();

  EXPECT_EQ(Part(0, 9, Format(Format::Light::DARKER), line), pop(parts));
  EXPECT_TRUE(parts.empty());
}

TEST(framework_terminal_TerminalBuffer_Line, reformat_replace_part)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line;
  line.append("abc");
  line.append("def", Format(Format::Light::BRIGHTER));
  line.append("ghi", Format());

  EXPECT_TRUE(line.reformat(Format(Format::Light::DARKER), 3, 6));

  std::list<Part> parts = line.parts();

  EXPECT_EQ(Format::Light::NORMAL, pop(parts).format.light);
  EXPECT_EQ(Format::Light::DARKER, pop(parts).format.light);
  EXPECT_EQ(Format::Light::NORMAL, pop(parts).format.light);
  EXPECT_TRUE(parts.empty());
}

TEST(framework_terminal_TerminalBuffer_Line, reformat_replace_subpart_beginning)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line;
  line.append("abc");
  line.append("def", Format(Format::Light::BRIGHTER));
  line.append("ghi", Format());

  EXPECT_TRUE(line.reformat(Format(Format::Light::DARKER), 3, 4));

  std::list<Part> parts = line.parts();

  Part p = pop(parts);
  EXPECT_EQ(static_cast<size_t>(0), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(3), p.indexEnd);
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(static_cast<size_t>(3), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(4), p.indexEnd);
  EXPECT_EQ(Format::Light::DARKER, p.format.light);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(static_cast<size_t>(4), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(6), p.indexEnd);
  EXPECT_EQ(Format::Light::BRIGHTER, p.format.light);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(static_cast<size_t>(6), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(9), p.indexEnd);
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);

  EXPECT_TRUE(parts.empty());
}

TEST(framework_terminal_TerminalBuffer_Line, reformat_replace_subpart_end)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line;
  line.append("abc");
  line.append("def", Format(Format::Light::BRIGHTER));
  line.append("ghi", Format());

  EXPECT_TRUE(line.reformat(Format(Format::Light::DARKER), 5, 6));

  std::list<Part> parts = line.parts();

  Part p = pop(parts);
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(0), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(3), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::BRIGHTER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(3), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(5), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::DARKER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(5), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(6), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(6), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(9), p.indexEnd);

  EXPECT_TRUE(parts.empty());
}

TEST(framework_terminal_TerminalBuffer_Line, reformat_replace_subpart_middle)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line;
  line.append("abc");
  line.append("def", Format(Format::Light::BRIGHTER));
  line.append("ghi", Format());

  EXPECT_TRUE(line.reformat(Format(Format::Light::DARKER), 4, 5));

  std::list<Part> parts = line.parts();

  Part p = pop(parts);
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(0), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(3), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::BRIGHTER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(3), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(4), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::DARKER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(4), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(5), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::BRIGHTER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(5), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(6), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(6), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(9), p.indexEnd);

  EXPECT_TRUE(parts.empty());
}

TEST(framework_terminal_TerminalBuffer_Line, reformat_merge_whole)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line;
  line.append("abc");
  line.append("def", Format(Format::Light::BRIGHTER));
  line.append("ghi", Format());

  EXPECT_TRUE(line.reformat(Format(Format::Light::NORMAL), 3, 6));

  std::list<Part> parts = line.parts();

  Part p = pop(parts);
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(0), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(9), p.indexEnd);

  EXPECT_TRUE(parts.empty());
}

TEST(framework_terminal_TerminalBuffer_Line, reformat_merge_whole_overlapping)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line;
  line.append("abc");
  line.append("def", Format(Format::Light::BRIGHTER));
  line.append("ghi", Format());

  EXPECT_TRUE(line.reformat(Format(Format::Light::NORMAL), 2, 7));

  std::list<Part> parts = line.parts();

  Part p = pop(parts);
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(0), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(9), p.indexEnd);

  EXPECT_TRUE(parts.empty());
}

TEST(framework_terminal_TerminalBuffer_Line, reformat_merge_with_previous)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line;
  line.append("abc");
  line.append("def", Format(Format::Light::BRIGHTER));
  line.append("ghi", Format());

  EXPECT_TRUE(line.reformat(Format(Format::Light::NORMAL), 3, 4));

  std::list<Part> parts = line.parts();

  Part p = pop(parts);
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(0), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(4), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::BRIGHTER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(4), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(6), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(6), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(9), p.indexEnd);

  EXPECT_TRUE(parts.empty());
}

TEST(framework_terminal_TerminalBuffer_Line, reformat_merge_with_next)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line;
  line.append("abc");
  line.append("def", Format(Format::Light::BRIGHTER));
  line.append("ghi", Format());

  EXPECT_TRUE(line.reformat(Format(Format::Light::NORMAL), 5, 6));

  std::list<Part> parts = line.parts();

  Part p = pop(parts);
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(0), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(3), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::BRIGHTER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(3), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(5), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(5), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(9), p.indexEnd);

  EXPECT_TRUE(parts.empty());
}

TEST(framework_terminal_TerminalBuffer_Line, reformat_overlap)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line;
  line.append("abc");
  line.append("def", Format(Format::Light::BRIGHTER));
  line.append("ghi", Format());

  EXPECT_TRUE(line.reformat(Format(Format::Light::DARKER), 2, 7));

  std::list<Part> parts = line.parts();

  Part p = pop(parts);
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(0), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(2), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::DARKER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(2), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(7), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(7), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(9), p.indexEnd);

  EXPECT_TRUE(parts.empty());
}

TEST(framework_terminal_TerminalBuffer_Line, reformat_overlap_with_previous)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line;
  line.append("abc");
  line.append("def", Format(Format::Light::BRIGHTER));
  line.append("ghi", Format());

  EXPECT_TRUE(line.reformat(Format(Format::Light::DARKER), 2, 4));

  std::list<Part> parts = line.parts();

  Part p = pop(parts);
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(0), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(2), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::DARKER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(2), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(4), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::BRIGHTER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(4), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(6), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(6), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(9), p.indexEnd);

  EXPECT_TRUE(parts.empty());
}

TEST(framework_terminal_TerminalBuffer_Line, reformat_overlap_with_next)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line;
  line.append("abc");
  line.append("def", Format(Format::Light::BRIGHTER));
  line.append("ghi", Format());

  EXPECT_TRUE(line.reformat(Format(Format::Light::DARKER), 5, 7));

  std::list<Part> parts = line.parts();

  Part p = pop(parts);
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(0), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(3), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::BRIGHTER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(3), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(5), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::DARKER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(5), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(7), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(7), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(9), p.indexEnd);

  EXPECT_TRUE(parts.empty());
}

TEST(framework_terminal_TerminalBuffer_Line, reformat_first_part_of_first)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line;
  line.append("abc");
  line.append("def", Format(Format::Light::BRIGHTER));
  line.append("ghi", Format());

  EXPECT_TRUE(line.reformat(Format(Format::Light::DARKER), 0, 1));

  std::list<Part> parts = line.parts();

  Part p = pop(parts);
  EXPECT_EQ(Format::Light::DARKER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(0), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(1), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(1), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(3), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::BRIGHTER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(3), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(6), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(6), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(9), p.indexEnd);

  EXPECT_TRUE(parts.empty());
}

TEST(framework_terminal_TerminalBuffer_Line, reformat_middle_part_of_first)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line;
  line.append("abc");
  line.append("def", Format(Format::Light::BRIGHTER));
  line.append("ghi", Format());

  EXPECT_TRUE(line.reformat(Format(Format::Light::DARKER), 1, 2));

  std::list<Part> parts = line.parts();

  Part p = pop(parts);
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(0), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(1), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::DARKER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(1), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(2), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(2), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(3), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::BRIGHTER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(3), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(6), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(6), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(9), p.indexEnd);

  EXPECT_TRUE(parts.empty());
}

TEST(framework_terminal_TerminalBuffer_Line, reformat_last_part_of_first)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line;
  line.append("abc");
  line.append("def", Format(Format::Light::BRIGHTER));
  line.append("ghi", Format());

  EXPECT_TRUE(line.reformat(Format(Format::Light::DARKER), 2, 3));

  std::list<Part> parts = line.parts();

  Part p = pop(parts);
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(0), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(2), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::DARKER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(2), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(3), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::BRIGHTER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(3), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(6), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(6), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(9), p.indexEnd);

  EXPECT_TRUE(parts.empty());
}

TEST(framework_terminal_TerminalBuffer_Line, reformat_complete_first)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line;
  line.append("abc");
  line.append("def", Format(Format::Light::BRIGHTER));
  line.append("ghi", Format());

  EXPECT_TRUE(line.reformat(Format(Format::Light::DARKER), 0, 3));

  std::list<Part> parts = line.parts();

  Part p = pop(parts);
  EXPECT_EQ(Format::Light::DARKER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(0), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(3), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::BRIGHTER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(3), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(6), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(6), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(9), p.indexEnd);

  EXPECT_TRUE(parts.empty());
}

TEST(framework_terminal_TerminalBuffer_Line, reformat_first_part_of_last)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line;
  line.append("abc");
  line.append("def", Format(Format::Light::BRIGHTER));
  line.append("ghi", Format());

  EXPECT_TRUE(line.reformat(Format(Format::Light::DARKER), 6, 7));

  std::list<Part> parts = line.parts();

  Part p = pop(parts);
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(0), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(3), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::BRIGHTER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(3), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(6), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::DARKER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(6), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(7), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(7), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(9), p.indexEnd);

  EXPECT_TRUE(parts.empty());
}

TEST(framework_terminal_TerminalBuffer_Line, reformat_middle_part_of_last)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line;
  line.append("abc");
  line.append("def", Format(Format::Light::BRIGHTER));
  line.append("ghi", Format());

  EXPECT_TRUE(line.reformat(Format(Format::Light::DARKER), 7, 8));

  std::list<Part> parts = line.parts();

  Part p = pop(parts);
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(0), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(3), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::BRIGHTER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(3), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(6), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(6), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(7), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::DARKER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(7), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(8), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(8), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(9), p.indexEnd);

  EXPECT_TRUE(parts.empty());
}

TEST(framework_terminal_TerminalBuffer_Line, reformat_last_part_of_last)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line;
  line.append("abc");
  line.append("def", Format(Format::Light::BRIGHTER));
  line.append("ghi", Format());

  EXPECT_TRUE(line.reformat(Format(Format::Light::DARKER), 8, 9));

  std::list<Part> parts = line.parts();

  Part p = pop(parts);
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(0), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(3), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::BRIGHTER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(3), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(6), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(6), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(8), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::DARKER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(8), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(9), p.indexEnd);

  EXPECT_TRUE(parts.empty());
}

TEST(framework_terminal_TerminalBuffer_Line, reformat_complete_last)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line;
  line.append("abc");
  line.append("def", Format(Format::Light::BRIGHTER));
  line.append("ghi", Format());

  EXPECT_TRUE(line.reformat(Format(Format::Light::DARKER), 6, 9));

  std::list<Part> parts = line.parts();

  Part p = pop(parts);
  EXPECT_EQ(Format::Light::NORMAL, p.format.light);
  EXPECT_EQ(static_cast<size_t>(0), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(3), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::BRIGHTER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(3), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(6), p.indexEnd);

  ASSERT_NO_THROW(p = pop(parts));
  EXPECT_EQ(Format::Light::DARKER, p.format.light);
  EXPECT_EQ(static_cast<size_t>(6), p.indexBegin);
  EXPECT_EQ(static_cast<size_t>(9), p.indexEnd);

  EXPECT_TRUE(parts.empty());
}

TEST(framework_terminal_TerminalBuffer_Line, reformat_dont_change_anything)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  std::list<Part> newFormat;

  Line line;
  line.append("abc");
  line.append("def", Format(Format::Light::BRIGHTER));
  line.append("ghi", Format());

  EXPECT_FALSE(line.reformat(Format(Format::Light::BRIGHTER), 3, 6));
  EXPECT_FALSE(line.reformat(Format(Format::Light::BRIGHTER), 3, 4));
  EXPECT_FALSE(line.reformat(Format(Format::Light::BRIGHTER), 4, 5));
  EXPECT_FALSE(line.reformat(Format(Format::Light::BRIGHTER), 5, 6));
}

TEST(framework_terminal_TerminalBuffer_Line, mergePartsIfPossible)
{
  typedef Buffer::Line Line;
  typedef Line::Part Part;

  Line line;

  line.mergePartsIfPossible();

  std::list<Part> parts = line.parts();

  EXPECT_EQ(static_cast<size_t>(0), line.length());
  EXPECT_EQ(static_cast<size_t>(1), parts.size());
  EXPECT_EQ(static_cast<size_t>(0), parts.begin()->indexBegin);
  EXPECT_EQ(static_cast<size_t>(0), parts.begin()->indexEnd);

  line.append("abc");
  line.mergePartsIfPossible();

  parts = line.parts();
  EXPECT_EQ(Part(0, 3, Format(Format::Light::NORMAL), line), pop(parts));
  EXPECT_TRUE(parts.empty());

  line.append("def", Format(Format::Light::BRIGHTER));
  line.mergePartsIfPossible();

  parts = line.parts();
  EXPECT_EQ(Part(0, 3, Format(Format::Light::NORMAL), line), pop(parts));
  EXPECT_EQ(Part(3, 6, Format(Format::Light::BRIGHTER), line), pop(parts));
  EXPECT_TRUE(parts.empty());

  line._parts.rbegin()->format = Format(Format::Light::NORMAL);

  parts = line.parts();
  EXPECT_EQ(Part(0, 3, Format(Format::Light::NORMAL), line), pop(parts));
  EXPECT_EQ(Part(3, 6, Format(Format::Light::NORMAL), line), pop(parts));
  EXPECT_TRUE(parts.empty());

  line.mergePartsIfPossible();

  parts = line.parts();
  EXPECT_EQ(Part(0, 6, Format(Format::Light::NORMAL), line), pop(parts));
  EXPECT_TRUE(parts.empty());

  line.append("ghi", Format(Format::Light::BRIGHTER));
  line.append("jkl", Format(Format::Light::NORMAL));
  line.append("mno", Format(Format::Light::BRIGHTER));

  parts = line.parts();
  EXPECT_EQ(Part(0, 6, Format(Format::Light::NORMAL), line), pop(parts));
  EXPECT_EQ(Part(6, 9, Format(Format::Light::BRIGHTER), line), pop(parts));
  EXPECT_EQ(Part(9, 12, Format(Format::Light::NORMAL), line), pop(parts));
  EXPECT_EQ(Part(12, 15, Format(Format::Light::BRIGHTER), line), pop(parts));
  EXPECT_TRUE(parts.empty());

  auto iter = line._parts.rbegin();
  ++iter;
  ++iter;
  iter->format = Format(Format::Light::NORMAL);

  parts = line.parts();
  EXPECT_EQ(Part(0, 6, Format(Format::Light::NORMAL), line), pop(parts));
  EXPECT_EQ(Part(6, 9, Format(Format::Light::NORMAL), line), pop(parts));
  EXPECT_EQ(Part(9, 12, Format(Format::Light::NORMAL), line), pop(parts));
  EXPECT_EQ(Part(12, 15, Format(Format::Light::BRIGHTER), line), pop(parts));
  EXPECT_TRUE(parts.empty());

  line.mergePartsIfPossible();

  parts = line.parts();
  EXPECT_EQ(Part(0, 12, Format(Format::Light::NORMAL), line), pop(parts));
  EXPECT_EQ(Part(12, 15, Format(Format::Light::BRIGHTER), line), pop(parts));
  EXPECT_TRUE(parts.empty());
}

TEST(framework_terminal_TerminalBuffer, push_empty)
{
  Buffer::Ptr buffer = Buffer::create();

  buffer->push("");

  EXPECT_NE(buffer->end(), buffer->begin());
  EXPECT_EQ("", buffer->firstLine()->content());
}

TEST(framework_terminal_TerminalBuffer, push_simple)
{
  Buffer::Ptr buffer = Buffer::create();

  buffer->push("a");

  EXPECT_NE(buffer->end(), buffer->begin());
  EXPECT_EQ("a", buffer->firstLine()->content());
}

TEST(framework_terminal_TerminalBuffer, push_simple_twice)
{
  Buffer::Ptr buffer = Buffer::create();

  buffer->push("a");

  EXPECT_NE(buffer->end(), buffer->begin());
  EXPECT_EQ("a", buffer->firstLine()->content());
  EXPECT_EQ("a", buffer->lastLine()->content());

  buffer->push("b");

  EXPECT_NE(buffer->end(), buffer->begin());
  EXPECT_EQ("ab", buffer->firstLine()->content());
  EXPECT_EQ("ab", buffer->lastLine()->content());
}

TEST(framework_terminal_TerminalBuffer, push_simple_newline)
{
  Buffer::Ptr buffer = Buffer::create();

  buffer->push("a");

  EXPECT_NE(buffer->end(), buffer->begin());
  EXPECT_EQ("a", buffer->firstLine()->content());
  EXPECT_EQ("a", buffer->lastLine()->content());

  buffer->push("\n");

  EXPECT_NE(buffer->end(), buffer->begin());
  EXPECT_EQ("a", buffer->firstLine()->content());
  EXPECT_EQ("", buffer->lastLine()->content());

  buffer->push("b");

  EXPECT_NE(buffer->end(), buffer->begin());
  EXPECT_EQ("a", buffer->firstLine()->content());
  EXPECT_EQ("b", buffer->lastLine()->content());
}

TEST(framework_terminal_TerminalBuffer, push_newline)
{
  Buffer::Ptr buffer = Buffer::create();

  buffer->push("a\nb");

  EXPECT_NE(buffer->end(), buffer->begin());
  EXPECT_EQ("a", buffer->firstLine()->content());
  EXPECT_EQ("b", buffer->lastLine()->content());
}

TEST(framework_terminal_TerminalBuffer, push_formating)
{
  Buffer::Ptr buffer = Buffer::create();

  ASSERT_EQ(static_cast<size_t>(1), buffer->firstLine()->parts().size());
  ASSERT_EQ(buffer->firstLine().get(), &buffer->firstLine()->parts().begin()->line);

  buffer->push("a\nb", Format(Format::Light::BRIGHTER));

  EXPECT_NE(buffer->end(), buffer->begin());
  EXPECT_EQ(Format(Format::Light::BRIGHTER), buffer->firstLine()->parts().begin()->format);
  EXPECT_EQ(Format(Format::Light::BRIGHTER), buffer->lastLine()->parts().begin()->format);
}

TEST(framework_terminal_TerminalBuffer, push_multiple_newlines)
{
  Buffer::Ptr buffer = Buffer::create();

  buffer->push("a\nb\nc\nhaha");
  buffer->push("d", Format(Format::Light::BRIGHTER));
  buffer->push("e");
  buffer->push("f\n", Format(Format::Light::BRIGHTER));
  buffer->push("g\n\nh\ngaga");
  buffer->push("i\n", Format(Format::Light::BRIGHTER));

  Buffer::const_iterator i = buffer->begin();

  EXPECT_EQ("a", (*i)->content());
  ++i;
  EXPECT_EQ("b", (*i)->content());
  ++i;
  EXPECT_EQ("c", (*i)->content());
  ++i;
  EXPECT_EQ("hahadef", (*i)->content());
  auto p = (*i)->parts().begin();
  EXPECT_EQ("haha", p->asString());
  EXPECT_EQ(Format(), p->format);
  ++p;
  EXPECT_EQ("d", p->asString());
  EXPECT_EQ(Format(Format::Light::BRIGHTER), p->format);
  ++p;
  EXPECT_EQ("e", p->asString());
  EXPECT_EQ(Format(), p->format);
  ++p;
  EXPECT_EQ("f", p->asString());
  EXPECT_EQ(Format(Format::Light::BRIGHTER), p->format);
  ++p;
  EXPECT_EQ((*i)->parts().end(), p);

  ++i;
  EXPECT_EQ("g", (*i)->content());
  ++i;
  EXPECT_EQ("", (*i)->content());
  ++i;
  EXPECT_EQ("h", (*i)->content());
  ++i;
  EXPECT_EQ("gagai", (*i)->content());
  ++i;
  EXPECT_EQ("", (*i)->content());
  ++i;
  EXPECT_EQ(buffer->end(), i);
}

TEST(framework_terminal_TerminalBuffer, clear_signal)
{
  int nCaughtSignals = 0;

  Buffer::Ptr buffer = Buffer::create();

  Signals::Connection c = buffer->signalCleared().connect([&nCaughtSignals](){++nCaughtSignals;});
  c.addTrackingType(Signals::Connection::TrackingType::SUPPRESS_WARNING);

  EXPECT_EQ(0, nCaughtSignals);
  buffer->clear();
  EXPECT_EQ(1, nCaughtSignals);
}

TEST(framework_terminal_TerminalBuffer, areThisLinesConsecutive_reflexivity)
{
  Buffer::Ptr buffer = Buffer::create();

  buffer->push("0\n1\n2\n3\n4\n5\n6\n8\n9");

  EXPECT_TRUE(buffer->areThisLinesConsecutive(buffer->firstLine(), buffer->firstLine()));
  EXPECT_TRUE(buffer->areThisLinesConsecutive(buffer->lastLine(), buffer->lastLine()));
}

TEST(framework_terminal_TerminalBuffer, areThisLinesConsecutive)
{
  Buffer::Ptr buffer = Buffer::create();

  buffer->push("0\n1\n2\n3\n4\n5\n6\n8\n9");

  EXPECT_TRUE(buffer->areThisLinesConsecutive(buffer->firstLine(), buffer->lastLine()));
  EXPECT_FALSE(buffer->areThisLinesConsecutive(buffer->lastLine(), buffer->firstLine()));

  Buffer::Line::ConstPtr prevLine;
  for(Buffer::Line::ConstPtr line : *buffer)
  {
    EXPECT_TRUE(buffer->areThisLinesConsecutive(buffer->firstLine(),line));
    EXPECT_TRUE(buffer->areThisLinesConsecutive(line,buffer->lastLine()));
    EXPECT_EQ(buffer->lastLine()==line, buffer->areThisLinesConsecutive(buffer->lastLine(),line));
    EXPECT_EQ(buffer->firstLine()==line, buffer->areThisLinesConsecutive(line, buffer->firstLine()));

    if(prevLine)
    {
      EXPECT_TRUE(buffer->areThisLinesConsecutive(prevLine,line));
      EXPECT_FALSE(buffer->areThisLinesConsecutive(line,prevLine));
    }

    prevLine = line;
  }
}

TEST(framework_terminal_TerminalBuffer, areThisLinesConsecutive_arbitrary_number_of_lines_small_list)
{
  Buffer::Ptr buffer = Buffer::create();

  buffer->push("0\n1\n2\n3\n4\n5\n6\n8\n9");

  EXPECT_TRUE(buffer->areThisLinesConsecutive({}));
  EXPECT_TRUE(buffer->areThisLinesConsecutive({buffer->firstLine()}));
}

TEST(framework_terminal_TerminalBuffer, areThisLinesConsecutive_arbitrary_number_of_lines)
{
  Buffer::Ptr buffer = Buffer::create();

  buffer->push("0\n1\n2\n3\n4\n5\n6\n8\n9");

  EXPECT_TRUE(buffer->areThisLinesConsecutive({buffer->firstLine(), buffer->firstLine(), buffer->lastLine()}));
  EXPECT_FALSE(buffer->areThisLinesConsecutive({buffer->lastLine(), buffer->firstLine()}));
}


TEST(framework_terminal_TerminalBuffer, deleteCharacterInLastLine)
{
  typedef Buffer::Line Line;

  int nSignalsSent = 0;
  Buffer::Ptr buffer = Buffer::create();

  buffer->push("abcd");

  Signals::Connection c = buffer->signalLineModified().connect([&nSignalsSent](const Line::ConstPtr&){++nSignalsSent;});
  c.addTrackingType(Signals::Connection::TrackingType::SUPPRESS_WARNING);

  EXPECT_EQ("abcd", buffer->lastLine()->content());
  EXPECT_EQ(0, nSignalsSent);

  buffer->deleteCharacterInLine(buffer->lastLine(), 3);

  EXPECT_EQ("abc", buffer->lastLine()->content());
  EXPECT_EQ(1, nSignalsSent);

  buffer->deleteCharacterInLine(buffer->lastLine(), 3);

  EXPECT_EQ("abc", buffer->lastLine()->content());
  EXPECT_EQ(1, nSignalsSent);
}

TEST(framework_terminal_TerminalBuffer, find)
{
  Buffer::Ptr buffer = Buffer::create();

  buffer->push("a\nb");

  EXPECT_EQ(buffer->firstLine(), *buffer->find(buffer->firstLine()));
  EXPECT_EQ(buffer->lastLine(), *buffer->find(buffer->lastLine()));
}

TEST(Buffer, getNextLineOf)
{
  Buffer::Ptr buffer = Buffer::create();
  Optional<Buffer::Line::Ptr> l;
  Buffer::Line::Ptr line;

  buffer->push("a\nb\nc\nd");

  ASSERT_FALSE(buffer->nextLineOf(buffer->lastLine()));

  ASSERT_TRUE(l = buffer->nextLineOf(buffer->firstLine()));
  line = *l;
  EXPECT_EQ("b", line->content());

  ASSERT_TRUE(l = buffer->nextLineOf(line));
  line = *l;
  EXPECT_EQ("c", line->content());

  ASSERT_TRUE(l = buffer->nextLineOf(line));
  line = *l;
  EXPECT_EQ("d", line->content());

  EXPECT_FALSE(buffer->nextLineOf(line));
}

TEST(Buffer, getPrevLineOf)
{
  Buffer::Ptr buffer = Buffer::create();
  Optional<Buffer::Line::Ptr> l;
  Buffer::Line::Ptr line;

  buffer->push("d\nc\nb\na");

  ASSERT_FALSE(buffer->prevLineOf(buffer->firstLine()));

  ASSERT_TRUE(l = buffer->prevLineOf(buffer->lastLine()));
  line = *l;
  EXPECT_EQ("b", line->content());

  ASSERT_TRUE(l = buffer->prevLineOf(line));
  line = *l;
  EXPECT_EQ("c", line->content());

  ASSERT_TRUE(l = buffer->prevLineOf(line));
  line = *l;
  EXPECT_EQ("d", line->content());

  EXPECT_FALSE(buffer->prevLineOf(line));
}


}
}
