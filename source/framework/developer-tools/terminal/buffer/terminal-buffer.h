#ifndef _FRAMEWORK_TERMINAL_BUFFER_H_
#define _FRAMEWORK_TERMINAL_BUFFER_H_


#include <dependencies.h>

#include "terminal-format.h"

#include <base/strings/string.h>
#include <base/optional.h>
#include <base/signals/signal.h>
#include <base/range.h>


namespace Framework {
using namespace Base;


namespace Terminal {


/** Stores the lines written to the terminal.
 *
 * Each line is an instance of Line stored in a list.
 */
class Buffer
{
public:
  typedef shared_ptr<Buffer> Ptr;
  typedef weak_ptr<Buffer> WeakPtr;
  typedef shared_ptr<const Buffer> ConstPtr;
  typedef weak_ptr<const Buffer> WeakConstPtr;

  class Line
  {
  public:
    typedef shared_ptr<Line> Ptr;
    typedef weak_ptr<Line> WeakPtr;
    typedef shared_ptr<const Line> ConstPtr;
    typedef weak_ptr<const Line> WeakConstPtr;

    class Part
    {
    public:
      typedef Range<size_t> CharacterRange;

      const Line& line;

      String::size_type indexBegin;
      String::size_type indexEnd;

      Format format;

    public:
      Part(String::size_type indexBegin,
           String::size_type indexEnd,
           const Format& format,
           const Line& line);

    public:
      const String::value_type* begin() const;
      const String::value_type* end() const;

      String asString() const;

      CharacterRange characerRange() const;

      bool operator==(const Part& other) const;
      bool operator!=(const Part& other) const;

      Part& operator=(const Part& other);

    public:
      const String::value_type* pointerFromIndex(String::size_type index) const;

      void classInvariant() const;
    };

  private:
    String _content;
    std::list<Part> _parts;

  public:
    Line(const String& content = String(), const Format& format=Format());
    Line(const Line& other);
    Line& operator=(const Line& other);

    static Ptr create(const String& content = String(), const Format& format=Format());

  public:
    const String& content() const;
    size_t length() const;
    void clear(const String& content = String(), const Format& format=Format());

    const std::list<Part>& parts() const;

    void append(const String& text, const Format& format=Format());

    void deleteCharacter(size_t position);
    void insertCharacter(size_t position, String::value_type character);

    bool reformat(const Format& newFormat,
                  size_t begin=0,
                  size_t end = std::numeric_limits<size_t>::max());

  private:
    void mergePartsIfPossible();

    FRIEND_TEST(framework_terminal_TerminalBuffer_Line, mergePartsIfPossible);
  };

public:
  typedef std::list<Line::Ptr> LineList;
  typedef LineList::const_iterator const_iterator;
  typedef LineList::const_reverse_iterator const_reverse_iterator;
  typedef LineList::iterator iterator;
  typedef LineList::reverse_iterator reverse_iterator;

private:
  LineList lines;

  Buffer();

public:
  static Ptr create();

  ~Buffer();

public:
  Line::Ptr push(const Line::Ptr& line, const String& text, const Format& format=Format());
  Line::Ptr push(const String& text, const Format& format=Format());
  Line::Ptr prependEmptyLine();
  void clear();

  void deleteCharacterInLine(const Line::Ptr& line, size_t position);
  void insertCharacterIntoLine(const Line::Ptr& line, size_t position, String::value_type character);

  void reformat(const Line::Ptr& line,
                const Format& format,
                size_t begin,
                size_t end);

  void clearLine(const Line::Ptr& line);
  void removeLine(Line::Ptr line);

public:
  size_t numberLines() const;

  iterator find(const Line::ConstPtr& line);
  reverse_iterator rfind(const Line::ConstPtr& line);

  const_iterator find(const Line::ConstPtr& line) const;
  const_reverse_iterator rfind(const Line::ConstPtr& line) const;

  Line::Ptr firstLine();
  Line::Ptr lastLine();
  Line::ConstPtr firstLine() const;
  Line::ConstPtr lastLine() const;

  String asPlainText() const;

  iterator begin();
  iterator end();
  reverse_iterator rbegin();
  reverse_iterator rend();

  const_iterator begin() const;
  const_iterator end() const;
  const_reverse_iterator rbegin() const;
  const_reverse_iterator rend() const;

  Optional<Line::Ptr> nextLineOf(const Line::ConstPtr& line);
  Optional<Line::Ptr> prevLineOf(const Line::ConstPtr& line);

  bool areThisLinesConsecutive(const Line::ConstPtr& a, const Line::ConstPtr& b) const;
  bool areThisLinesConsecutive(const std::initializer_list<Line::ConstPtr>& lines) const;

  bool containsLine(const Line::ConstPtr& line) const;

public:
  Signals::Signal<void(const Line::ConstPtr&)>& signalLineModified();
  Signals::Signal<void(const Line::ConstPtr&)>& signalLineInserted();
  Signals::Signal<void(const Line::ConstPtr&)>& signalGoingToRemoveLine();
  Signals::Signal<void(const Line::ConstPtr&)>& signalLineRemoved();
  Signals::Signal<void()>& signalCleared();

private:
  Signals::CallableSignal<void(const Line::ConstPtr&)> _signalLineModified;
  Signals::CallableSignal<void(const Line::ConstPtr&)> _signalLineInserted;
  Signals::CallableSignal<void(const Line::ConstPtr&)> _signalGoingToRemoveLine;
  Signals::CallableSignal<void(const Line::ConstPtr&)> _signalLineRemoved;
  Signals::CallableSignal<void()> _signalCleared;
};


}
}


#endif
