#include <framework/developer-tools/terminal/private-interactive-area.h>

namespace Framework {
namespace Terminal {
namespace Private {

const InteractiveArea::Order ORDER_ABC = static_cast<InteractiveArea::Order::Value>(-1);
const InteractiveArea::Order ORDER_UVW = static_cast<InteractiveArea::Order::Value>(0);
const InteractiveArea::Order ORDER_XYZ = static_cast<InteractiveArea::Order::Value>(1);


TEST(framework_terminal_InteractiveAreaManager, findRightCursorForNewInteractiveAreaWithOrder)
{
  InteractiveArea::Ptr uvw, abc, xyz;
  Buffer::Ptr buffer = Buffer::create();
  InteractiveAreaManager interactiveAreaManager(buffer);

  Cursor c = interactiveAreaManager.findRightCursorForNewInteractiveAreaWithOrder(ORDER_UVW);

  EXPECT_EQ(buffer->firstLine(), c.line());
  EXPECT_EQ(buffer->lastLine(), c.line());

  interactiveAreaManager.tryCreateInteractiveArea(ORDER_UVW, out(uvw));

  EXPECT_EQ("", uvw->asPlainText());

  uvw->writeOut("u\nv\n");

  EXPECT_EQ("u\nv", uvw->asPlainText());

  uvw->writeOut("w\n");

  EXPECT_EQ("u\nv\nw", uvw->asPlainText());

  interactiveAreaManager.tryCreateInteractiveArea(ORDER_ABC, out(abc));

  abc->writeOut("a\nb\nc\n");

  EXPECT_EQ("a\nb\nc", abc->asPlainText());

  interactiveAreaManager.tryCreateInteractiveArea(ORDER_XYZ, out(xyz));

  xyz->writeOut("x\ny\nz\n");

  EXPECT_EQ("a\nb\nc\nu\nv\nw\nx\ny\nz", buffer->asPlainText());

  EXPECT_EQ("a\nb\nc", abc->asPlainText());
  EXPECT_EQ("u\nv\nw", uvw->asPlainText());
  EXPECT_EQ("x\ny\nz", xyz->asPlainText());

  xyz.reset();

  EXPECT_EQ("a\nb\nc", abc->asPlainText());
  EXPECT_EQ("u\nv\nw", uvw->asPlainText());

  uvw.reset();

  EXPECT_EQ("a\nb\nc", abc->asPlainText());
}

TEST(framework_terminal_InteractiveAreaManager, destroying_areas)
{
  InteractiveArea::Ptr uvw, abc, xyz;
  Buffer::Ptr buffer = Buffer::create();
  InteractiveAreaManager interactiveAreaManager(buffer);

  interactiveAreaManager.tryCreateInteractiveArea(ORDER_UVW, out(uvw));

  uvw->writeOut("u\nv\nw\n");

  interactiveAreaManager.tryCreateInteractiveArea(ORDER_ABC, out(abc));

  abc->writeOut("a\nb\nc\n");

  interactiveAreaManager.tryCreateInteractiveArea(ORDER_XYZ, out(xyz));

  abc->writeOut("x\ny\nz\n");

  EXPECT_EQ("a\nb\nc\nx\ny\nz\nu\nv\nw\n", buffer->asPlainText());
}

TEST(framework_terminal_InteractiveAreaManager, clear_areas)
{
  InteractiveArea::Ptr uvw, abc, xyz;
  Buffer::Ptr buffer = Buffer::create();
  InteractiveAreaManager interactiveAreaManager(buffer);

  interactiveAreaManager.tryCreateInteractiveArea(ORDER_UVW, out(uvw));

  uvw->writeOut("u\nv\nw\n");

  interactiveAreaManager.tryCreateInteractiveArea(ORDER_ABC, out(abc));

  abc->writeOut("a\nb\nc\n");

  interactiveAreaManager.tryCreateInteractiveArea(ORDER_XYZ, out(xyz));

  xyz->writeOut("x\ny\nz\n");

  EXPECT_EQ("a\nb\nc\nu\nv\nw\nx\ny\nz", buffer->asPlainText());

  uvw->clearWholeArea();

  EXPECT_EQ("a\nb\nc\n\nx\ny\nz", buffer->asPlainText());

  interactiveAreaManager.tryCreateInteractiveArea(ORDER_UVW, out(uvw));

  uvw->writeOut("u\nv\nw\n");
  abc->clearWholeArea();

  EXPECT_EQ("\nu\nv\nw\nx\ny\nz", buffer->asPlainText());

  abc->writeOut("a\nb\nc\n");
  xyz->clearWholeArea();

  EXPECT_EQ("a\nb\nc\nu\nv\nw\n", buffer->asPlainText());

  xyz->writeOut("x\ny\nz\n");

  EXPECT_EQ("a\nb\nc\nu\nv\nw\nx\ny\nz", buffer->asPlainText());
}

TEST(framework_terminal_InteractiveAreaManager, destroy_and_recreate_areas)
{
  InteractiveArea::Ptr uvw, abc, xyz;
  Buffer::Ptr buffer = Buffer::create();
  InteractiveAreaManager interactiveAreaManager(buffer);

  interactiveAreaManager.tryCreateInteractiveArea(ORDER_UVW, out(uvw));

  uvw->writeOut("u\nv\nw\n");

  interactiveAreaManager.tryCreateInteractiveArea(ORDER_ABC, out(abc));

  abc->writeOut("a\nb\nc\n");

  interactiveAreaManager.tryCreateInteractiveArea(ORDER_XYZ, out(xyz));

  xyz->writeOut("x\ny\nz\n");

  EXPECT_EQ("a\nb\nc\nu\nv\nw\nx\ny\nz", buffer->asPlainText());

  uvw.reset();

  EXPECT_EQ("a\nb\nc\nx\ny\nz", buffer->asPlainText());

  interactiveAreaManager.tryCreateInteractiveArea(ORDER_UVW, out(uvw));
  uvw->writeOut("u\nv\nw\n");

  abc.reset();

  EXPECT_EQ("u\nv\nw\nx\ny\nz", buffer->asPlainText());

  interactiveAreaManager.tryCreateInteractiveArea(ORDER_ABC, out(abc));
  abc->writeOut("a\nb\nc\n");

  xyz.reset();

  EXPECT_EQ("a\nb\nc\nu\nv\nw", buffer->asPlainText());

  interactiveAreaManager.tryCreateInteractiveArea(ORDER_XYZ, out(xyz));
  xyz->writeOut("x\ny\nz\n");

  EXPECT_EQ("a\nb\nc\nu\nv\nw\nx\ny\nz", buffer->asPlainText());
}


}
}
}
