#include <framework/developer-tools/terminal/private-interactive-area.h>

namespace Framework {
namespace Terminal {
namespace Private {

InteractiveArea::InteractiveArea(Cursor startCursor) : _startCursor(startCursor), _endCursor(startCursor)
{
  this->avoidToEndWithEmptyLine = true;
  this->_endingWithEmptyLine = false;
  this->_endCursor = this->_startCursor;
}

InteractiveArea::~InteractiveArea()
{
  this->clearWholeArea();
  this->_startCursor.removeLine();
}

void InteractiveArea::clearWholeArea()
{
  assert(this->_startCursor <= this->_endCursor);

  this->_endingWithEmptyLine = false;

  while(this->_startCursor.line() != this->_endCursor.line())
    this->_startCursor.removeLine();

  this->_endCursor.clearLine();
}

void InteractiveArea::ensureBeginningOfLine()
{
  if(this->_endCursor.line()->length() != 0 && !_endingWithEmptyLine)
    writeOut("\n");
}

void InteractiveArea::writeOut(const Format& format, const String& text)
{
  if(text.empty())
    return;

  String textReallyWritten = text;

  if(avoidToEndWithEmptyLine)
  {
    if(_endingWithEmptyLine)
    {
      _endingWithEmptyLine = false;
      textReallyWritten = String::fromUnivodeValue('\n') + textReallyWritten;
    }

    if(*textReallyWritten.rbegin()=='\n')
    {
      textReallyWritten = textReallyWritten.trimFromRight(1);
      _endingWithEmptyLine = true;
    }
  }

  this->_endCursor.pushAtEndOfLine(textReallyWritten, format);
}

void InteractiveArea::writeOut(const String& text)
{
  writeOut(Format(), text);
}

const Cursor& InteractiveArea::startCursor() const
{
  return this->_startCursor;
}

const Cursor& InteractiveArea::endCursor() const
{
  return this->_endCursor;
}

String InteractiveArea::asPlainText() const
{
  assert(this->_startCursor <= this->_endCursor);

  String asPlainText;

  Cursor c = this->_startCursor;

  assert(c <= this->_endCursor);

  while(c.line() != this->_endCursor.line())
  {
    asPlainText += c.line()->content() + "\n";

    c.moveToNextLine();
  }
  asPlainText += c.line()->content();

  assert(this->_startCursor <= this->_endCursor);

  return asPlainText;
}


InteractiveAreaManager::InteractiveAreaManager(const Buffer::Ptr& buffer)
  : buffer(buffer)
{
}

bool InteractiveAreaManager::tryCreateInteractiveArea(InteractiveArea::Order order, const Output<InteractiveArea::Ptr>& interactiveArea)
{
  InteractiveArea::WeakPtr& interactiveAreaWithOrder = interactiveAreas[order.value];

  interactiveArea.value = interactiveAreaWithOrder.lock();

  if(interactiveArea.value)
    return false;

  interactiveArea.value = InteractiveArea::Ptr(new InteractiveArea(findRightCursorForNewInteractiveAreaWithOrder(order)));
  interactiveAreaWithOrder = interactiveArea.value;

  return true;
}

Cursor InteractiveAreaManager::findRightCursorForNewInteractiveAreaWithOrder(InteractiveArea::Order order)
{
  // findRightCursorForNewInteractiveAreaWithOrder is called by tryCreateInteractiveArea after std::map<>::operator[]() has been called once
  // so checking with zero wouldn't be enough
  bool nointeractiveAreasUsed = (interactiveAreas.size() <= 1);

  if(nointeractiveAreasUsed)
  {
    buffer->clear();
    return Cursor(buffer);
  }

  InteractiveArea::Ptr previousValid;
  for(std::map<InteractiveArea::Order::Value, InteractiveArea::WeakPtr>::iterator i=interactiveAreas.begin(); i!=interactiveAreas.end(); )
  {
    if(i->first == order.value)
    {
      break;
    }

    InteractiveArea::Ptr current = i->second.lock();
    if(current)
    {
      previousValid = current;
      ++i;
    }else
    {
      i = interactiveAreas.erase(i);
    }
  }

  if(previousValid)
  {
    Cursor c(previousValid->_endCursor);
    c.pushAtEndOfLine("\n");
    return c;
  }else
    return Cursor(buffer, buffer->prependEmptyLine(), 0);

}


}
}
}
