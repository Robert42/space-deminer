#include <framework/developer-tools/terminal/terminal-content.h>

namespace Framework {
namespace Terminal {
typedef Buffer::Line Line;


TEST(Content, calcNumberOfCharactersToUseOfLine)
{
  Buffer::Ptr buffer = Buffer::create();

  Viewport viewport(buffer);
  viewport.setSize(4, 4);

  Content content(viewport);

  EXPECT_EQ(16, static_cast<int>(content.calcNumberOfCharactersToUseOfLine(0)));
  EXPECT_EQ(12, static_cast<int>(content.calcNumberOfCharactersToUseOfLine(1)));
  EXPECT_EQ(8, static_cast<int>(content.calcNumberOfCharactersToUseOfLine(2)));
  EXPECT_EQ(4, static_cast<int>(content.calcNumberOfCharactersToUseOfLine(3)));
  EXPECT_EQ(0, static_cast<int>(content.calcNumberOfCharactersToUseOfLine(4)));
}

TEST(Content, getContent_result)
{
  Buffer::Ptr buffer = Buffer::create();

  Viewport viewport(buffer);
  viewport.setSize(4, 4);

  Content content(viewport);

  buffer->push("a");

  Optional<String> str;
  EXPECT_FALSE(content.contentAsPlainString(Format(Format::Light::BRIGHTER)));
  ASSERT_TRUE(str = content.contentAsPlainString(Format()));
  EXPECT_EQ("a", *str);
}

TEST(Content, getContent_newlines)
{
  Buffer::Ptr buffer = Buffer::create();

  Viewport viewport(buffer);
  viewport.setSize(4, 4);

  Content content(viewport);

  buffer->push("a\nb\nc");
  buffer->push("\nd", Format(Format::Light::BRIGHTER));

  Optional<String> str;
  ASSERT_TRUE(str = content.contentAsPlainString(Format(Format::Light::BRIGHTER)));
  EXPECT_EQ("\n\n\nd", *str);
}

TEST(Content, getContent_offset)
{
  Buffer::Ptr buffer = Buffer::create();

  Viewport viewport(buffer);
  viewport.setSize(16, 16);

  Content content(viewport);

  buffer->push("a\nb\nc\nhaha");
  buffer->push("d", Format(Format::Light::BRIGHTER));

  Optional<String> str;
  ASSERT_TRUE(str = content.contentAsPlainString(Format(Format::Light::BRIGHTER)));
  EXPECT_EQ("\n\n\n    d", *str);
}

TEST(Content, getContent_multiple)
{
  Buffer::Ptr buffer = Buffer::create();

  Viewport viewport(buffer);
  viewport.setSize(128, 128);

  Content content(viewport);

  buffer->push("a\nb\nc\nhaha");
  buffer->push("d", Format(Format::Light::BRIGHTER));
  buffer->push("e");
  buffer->push("f\n", Format(Format::Light::BRIGHTER));
  buffer->push("g\n\nh\ngaga");
  buffer->push("i\n", Format(Format::Light::BRIGHTER));

  Optional<String> str;
  ASSERT_TRUE(str = content.contentAsPlainString(Format(Format::Light::BRIGHTER)));
  EXPECT_EQ("\n\n\n    d f\n\n\n\n    i", *str);
  ASSERT_TRUE(str = content.contentAsPlainString(Format()));
  EXPECT_EQ("a\nb\nc\nhaha e\ng\n\nh\ngaga", *str);

  viewport.setSize(4, 4);
  viewport.moveUp(100);

  EXPECT_FALSE(content.contentAsPlainString(Format(Format::Light::BRIGHTER)));

  ASSERT_TRUE(str = content.contentAsPlainString(Format()));
  EXPECT_EQ("a\nb\nc\nhaha", *str);
}

TEST(Content, getContent_too_small_viewport)
{
  Buffer::Ptr buffer = Buffer::create();

  Viewport viewport(buffer);
  viewport.setSize(4, 4);

  Content content(viewport);

  buffer->push("a\nb\nc\nhaha");
  buffer->push("d", Format(Format::Light::BRIGHTER));
  buffer->push("e");
  buffer->push("f\n", Format(Format::Light::BRIGHTER));
  buffer->push("g\n\nh\ngaga");
  buffer->push("i\n", Format(Format::Light::BRIGHTER));

  viewport.moveUp(100);

  EXPECT_FALSE(content.contentAsPlainString(Format(Format::Light::BRIGHTER)));

  viewport.setSize(5, 4);
  viewport.moveUp(100);

  Optional<String> str;
  ASSERT_TRUE(str = content.contentAsPlainString(Format(Format::Light::BRIGHTER)));
  EXPECT_EQ("\n\n\n    d", *str);

  viewport.setSize(4, 5);
  viewport.moveUp(100);

  ASSERT_TRUE(str = content.contentAsPlainString(Format(Format::Light::BRIGHTER)));
  EXPECT_EQ("\n\n\n\nd f", *str);
}

TEST(Content, getContent_viewportWithHeight2)
{
  Buffer::Ptr buffer = Buffer::create();

  // Expected positions:
  // space-deminer dev-t
  // erminal $ print(\"Äp
  // fel heißen so :)\");
  // Äpfel heißen so :)
  // space-deminer dev-t
  // erminal $

  buffer->push("space-deminer dev-terminal $ print(\"Äpfel heißen so :)\");\n"
               "Äpfel heißen so :)\n"
               "space-deminer dev-terminal $ ");

  Viewport viewport(buffer);
  viewport.setSize(19, 2);
  viewport.moveDown(1024);

  Content content(viewport);

  Optional<String> str;
  ASSERT_TRUE(str = content.contentAsPlainString(Format()));
  EXPECT_EQ("space-deminer dev-t\nerminal $ ", *str);

  viewport.moveDown(1024);
  ASSERT_TRUE(str = content.contentAsPlainString(Format()));
  EXPECT_EQ("space-deminer dev-t\nerminal $ ", *str);

  viewport.moveUp(1);
  ASSERT_TRUE(str = content.contentAsPlainString(Format()));
  EXPECT_EQ("Äpfel heißen so :)\nspace-deminer dev-t", *str);

  viewport.moveUp(1);
  ASSERT_TRUE(str = content.contentAsPlainString(Format()));
  EXPECT_EQ("fel heißen so :)\");\nÄpfel heißen so :)", *str);

  viewport.moveUp(1);
  ASSERT_TRUE(str = content.contentAsPlainString(Format()));
  EXPECT_EQ("erminal $ print(\"Äp\nfel heißen so :)\");", *str);

  viewport.moveUp(1);
  ASSERT_TRUE(str = content.contentAsPlainString(Format()));
  EXPECT_EQ("space-deminer dev-t\nerminal $ print(\"Äp", *str);
}

TEST(Content, getContent_linebreaks)
{
  Buffer::Ptr buffer = Buffer::create();

  Viewport viewport(buffer);
  viewport.setSize(4, 4);

  Content content(viewport);

  buffer->push("abcd"
               "ef");
  buffer->push("gh"
               "ijkl"
               "m", Format(Format::Light::BRIGHTER));
  buffer->push("nopqrstuvwxyz");

  viewport.moveUp(100);


  Optional<String> str;
  ASSERT_TRUE(str = content.contentAsPlainString(Format(Format::Light::BRIGHTER)));
  EXPECT_EQ("\n  gh\nijkl\nm", *str);
}

TEST(Content, getContent_linebreaks_2)
{
  Buffer::Ptr buffer = Buffer::create();

  Viewport viewport(buffer);
  viewport.setSize(4, 4);

  Content content(viewport);

  buffer->push("abcd"
               "ef");
  buffer->push("gh"
               "ijkl", Format(Format::Light::BRIGHTER));
  buffer->push("nopqrstuvwxyz");

  viewport.moveUp(100);

  Optional<String> str;
  ASSERT_TRUE(str = content.contentAsPlainString(Format(Format::Light::BRIGHTER)));
  EXPECT_EQ("\n  gh\nijkl", *str);
}

TEST(Content, getContent_starting_int_too_large_line)
{
  String testContent = "abcd"
                       "e\n"
                       "0\n"
                       "1\n"
                       "2";
  Buffer::Ptr buffer = Buffer::create();
  Viewport viewport(buffer);
  viewport.setSize(4, 4);

  Content content(viewport);

  buffer->push(testContent);

  String expectedResult = "e\n"
                          "0\n"
                          "1\n"
                          "2";

  Optional<String> str;
  Format defaultFormat;
  str = content.contentAsPlainString(defaultFormat);
  EXPECT_EQ(static_cast<size_t>(4), viewport.viewportStart().positionInLine());
  EXPECT_EQ(expectedResult, *str);
}

TEST(TerminalContent_ContentForFormat, fillWithWhitespaceForNextSection)
{
  Content::ContentForFormat c;

  // Empty
  c.fillWithWhitespaceForNextSection(0, 0);

  EXPECT_EQ("", c.content());

  // Following on same line
  c.fillWithWhitespaceForNextSection(1, 0);
  c.fillWithWhitespaceForNextSection(1, 0);

  EXPECT_EQ("\n", c.content());
  EXPECT_EQ(1, static_cast<int>(c.lineOffset));
  EXPECT_EQ(0, static_cast<int>(c.inlineOffset));

  // Following on different lines
  c.clear();
  c.fillWithWhitespaceForNextSection(1, 0);
  c.fillWithWhitespaceForNextSection(2, 0);

  EXPECT_EQ("\n\n", c.content());
  EXPECT_EQ(2, static_cast<int>(c.lineOffset));
  EXPECT_EQ(0, static_cast<int>(c.inlineOffset));

  // Space in first line
  c.clear();
  c.fillWithWhitespaceForNextSection(0, 2);

  EXPECT_EQ("  ", c.content());
  EXPECT_EQ(0, static_cast<int>(c.lineOffset));
  EXPECT_EQ(2, static_cast<int>(c.inlineOffset));

  // Space in second line
  c.clear();
  c.fillWithWhitespaceForNextSection(1, 2);

  EXPECT_EQ("\n  ", c.content());
  EXPECT_EQ(1, static_cast<int>(c.lineOffset));
  EXPECT_EQ(2, static_cast<int>(c.inlineOffset));

  // Space in second and third line
  c.clear();
  c.fillWithWhitespaceForNextSection(1, 3);

  EXPECT_EQ("\n   ", c.content());
  EXPECT_EQ(1, static_cast<int>(c.lineOffset));
  EXPECT_EQ(3, static_cast<int>(c.inlineOffset));

  c.fillWithWhitespaceForNextSection(2, 2);

  EXPECT_EQ("\n   \n  ", c.content());
  EXPECT_EQ(2, static_cast<int>(c.lineOffset));
  EXPECT_EQ(2, static_cast<int>(c.inlineOffset));

  // Add Space multiple times into the third line
  c.clear();
  c.fillWithWhitespaceForNextSection(2, 3);

  EXPECT_EQ("\n\n   ", c.content());
  EXPECT_EQ(2, static_cast<int>(c.lineOffset));
  EXPECT_EQ(3, static_cast<int>(c.inlineOffset));

  c.fillWithWhitespaceForNextSection(2, 5);

  EXPECT_EQ("\n\n     ", c.content());
  EXPECT_EQ(2, static_cast<int>(c.lineOffset));
  EXPECT_EQ(5, static_cast<int>(c.inlineOffset));
}

TEST(TerminalContent_ContentForFormat, inject)
{
  Content::ContentForFormat c;
  String str = "0123456789abcdef";
  const String::value_type* s = str.toUtf32String().data();

  // Basic With empty
  c.inject(s+0,
           s+0,
           4);

  EXPECT_EQ("", c.content());
  EXPECT_EQ(0, static_cast<int>(c.lineOffset));
  EXPECT_EQ(0, static_cast<int>(c.inlineOffset));

  // Basic Test
  c.clear();
  c.inject(s+0,
           s+16,
           4);

  EXPECT_EQ("0123\n"
            "4567\n"
            "89ab\n"
            "cdef",
            c.content());
  EXPECT_EQ(3, static_cast<int>(c.lineOffset));
  EXPECT_EQ(4, static_cast<int>(c.inlineOffset));

  // Basic with offset 0
  c.clear();
  c.inlineOffset = 1;
  c.inject(s+0,
           s+16,
           4);

  EXPECT_EQ("012\n"
            "3456\n"
            "789a\n"
            "bcde\n"
            "f",
            c.content());
  EXPECT_EQ(4, static_cast<int>(c.lineOffset));
  EXPECT_EQ(1, static_cast<int>(c.inlineOffset));

  // Basic with offset 2
  c.clear();
  c.inlineOffset = 2;
  c.inject(s+0,
           s+16,
           4);

  EXPECT_EQ("01\n"
            "2345\n"
            "6789\n"
            "abcd\n"
            "ef",
            c.content());
  EXPECT_EQ(4, static_cast<int>(c.lineOffset));
  EXPECT_EQ(2, static_cast<int>(c.inlineOffset));

  // Basic with offset 3
  c.clear();
  c.inlineOffset = 3;
  c.inject(s+0,
           s+16,
           4);

  EXPECT_EQ("0\n"
            "1234\n"
            "5678\n"
            "9abc\n"
            "def",
            c.content());
  EXPECT_EQ(4, static_cast<int>(c.lineOffset));
  EXPECT_EQ(3, static_cast<int>(c.inlineOffset));

  // multiple times a full line
  c.clear();
  c.inject(s+0,
           s+4,
           4);
  EXPECT_EQ("0123",
            c.content());
  EXPECT_EQ(0, static_cast<int>(c.lineOffset));
  EXPECT_EQ(4, static_cast<int>(c.inlineOffset));
  c.fillWithWhitespaceForNextSection(1, 0);
  c.inject(s+4,
           s+8,
           4);
  EXPECT_EQ("0123\n"
            "4567",
            c.content());
  EXPECT_EQ(1, static_cast<int>(c.lineOffset));
  EXPECT_EQ(4, static_cast<int>(c.inlineOffset));

  // multiple times a full line without fill operation
  c.clear();
  c.inject(s+0,
           s+4,
           4);
  EXPECT_EQ("0123",
            c.content());
  EXPECT_EQ(0, static_cast<int>(c.lineOffset));
  EXPECT_EQ(4, static_cast<int>(c.inlineOffset));
  c.inject(s+4,
           s+8,
           4);
  EXPECT_EQ("0123\n"
            "4567",
            c.content());
  EXPECT_EQ(1, static_cast<int>(c.lineOffset));
  EXPECT_EQ(4, static_cast<int>(c.inlineOffset));
}


}
}
