#include <framework/developer-tools/terminal/terminal-content.h>

namespace Framework {
namespace Terminal {
typedef Buffer::Line Line;

Content::Content(Viewport& viewport)
  : viewport(viewport)
{
  dirty = true;

  viewport.signalRedrawNeeded().connect(std::bind(&Content::markDirty, this)).track(this->trackable);
}

Content::~Content()
{
}

void Content::recalculateContent()
{
  if(!dirty)
    return;
  dirty = false;

  const Cursor& viewportStart = viewport.viewportStart();
  Buffer::const_iterator lineIterator = viewport.buffer()->find(viewportStart.line());
  Buffer::const_iterator end = viewport.buffer()->end();

  // When the first line within the viewport starts within a buffer-line, the first part of the
  // buffer-line outside the viewport should be ignored.
  size_t offsetToIgnore = viewportStart.positionInLine();
  size_t currentLineOffset = 0;

  for(std::pair<const Format, ContentForFormat>& c : content)
    c.second.clear();

  while(lineIterator != end && currentLineOffset < viewport.height())
  {
    const Line::ConstPtr& line = *lineIterator;

    // Limit the characters which can be used. Any character fromt he current line with an
    // (index >= expectedEnd) would be outside the viewport.
    size_t expectedEnd = calcNumberOfCharactersToUseOfLine(currentLineOffset) + offsetToIgnore;

    for(const Line::Part& part : line->parts())
    {
      // Ignore part, if it's within the first part of the first line outside the viewport
      if(part.indexEnd <= offsetToIgnore)
        continue;

      // Calculate the range of character withing the part, which will be visible within the viewport
      size_t indexBegin = max(part.indexBegin, offsetToIgnore);
      size_t indexEnd = min(part.indexEnd, expectedEnd);

      if(indexBegin >= expectedEnd)
        return;

      ivec2 positionRelativeToStart = viewport.positionRelativeToStartOfLine(indexBegin-offsetToIgnore);

      ContentForFormat& contentForFormat = content[part.format];
      contentForFormat.fillWithWhitespaceForNextSection(currentLineOffset + positionRelativeToStart.y,
                                                        positionRelativeToStart.x);
      contentForFormat.inject(part.pointerFromIndex(indexBegin),
                              part.pointerFromIndex(indexEnd),
                              viewport.width());
    }

    currentLineOffset += viewport.heightOfLine(line) - offsetToIgnore/viewport.width();
    ++lineIterator;

    // Only the beginning of the first buffer line may be outside the viewport
    offsetToIgnore = 0;
  }
}

size_t Content::calcNumberOfCharactersToUseOfLine(size_t lineIndexRelativeToViewport) const
{
  assert(lineIndexRelativeToViewport <= viewport.height());

  return viewport.width() * (viewport.height() - lineIndexRelativeToViewport);
}



void Content::markDirty()
{
  dirty = true;
}

bool Content::isDirty() const
{
  return dirty;
}

Optional<String> Content::contentAsPlainString(Format formatting)
{
  recalculateContent();

  ContentForFormat& c = this->content[formatting];

  if(!c.isUsed())
    return nothing;

  return c.content();
}


Content::ContentForFormat::ContentForFormat()
{
  clear();
}

bool Content::ContentForFormat::isUsed() const
{
  return used;
}

const String& Content::ContentForFormat::content() const
{
  return _content;
}

void Content::ContentForFormat::clear()
{
  used = false;
  _content.clear();

  lineOffset = 0;
  inlineOffset = 0;
}


void Content::ContentForFormat::fillWithWhitespaceForNextSection(size_t lineOffset,
                                                                 size_t inlineOffset)
{
  assert(this->lineOffset <= lineOffset);
  size_t lineDifference = lineOffset - this->lineOffset;

  if(lineDifference != 0)
  {
    _content += String::fromUnivodeValue('\n', lineDifference);
    this->lineOffset = lineOffset;
    this->inlineOffset = 0;
  }

  assert(this->inlineOffset <= inlineOffset);
  size_t inlineDifference = inlineOffset-this->inlineOffset;

  if(inlineDifference != 0)
  {
    _content += String::fromUnivodeValue(' ', inlineDifference);
    this->inlineOffset = inlineOffset;
  }
}


void Content::ContentForFormat::inject(const String::value_type* begin,
                                       const String::value_type* end,
                                       size_t viewportWidth)
{
  used = true;

  assert(this->inlineOffset <= viewportWidth);

  const String::value_type* subSectionBegin = begin;
  const String::value_type* subSectionEnd;
  for(subSectionEnd = begin; subSectionEnd!=end; ++subSectionEnd)
  {
    if(this->inlineOffset==viewportWidth)
    {
      _content += String(subSectionBegin, subSectionEnd);

      if(subSectionEnd!=end)
        _content+=String::fromUnivodeValue('\n');

      subSectionBegin = subSectionEnd;
      this->inlineOffset = 1;
      ++this->lineOffset;
    }else
    {
      ++this->inlineOffset;
    }
  }

  if(subSectionBegin != subSectionEnd)
    _content += String(subSectionBegin, subSectionEnd);
}


}
}
