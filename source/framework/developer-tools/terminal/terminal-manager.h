#ifndef _FRAMEWORK_TERMINAL_MANAGER_H_
#define _FRAMEWORK_TERMINAL_MANAGER_H_

#include "./interactive-area.h"
#include <base/singleton.h>

namespace Framework {
namespace Terminal {

class Buffer;

class Manager : public SmartSingleton<Manager>
{
public:
  friend class InteractiveArea;
  friend class SmartSingleton<Manager>;

  typedef shared_ptr<Manager> Ptr;

  class Implementation
  {
  public:
    virtual shared_ptr<Private::InteractiveArea> createInteractiveArea(InteractiveArea::Order order) = 0;

    virtual ~Implementation(){}
  };

private:
  unique_ptr<Implementation> implementation;
  shared_ptr<Buffer> buffer;
  InteractiveArea _defaultOutput;

private:
  Manager();

public:
  static Ptr create();

  static const shared_ptr<Buffer>& activate();

  static InteractiveArea& defaultOutput();
};

}
}

#endif
