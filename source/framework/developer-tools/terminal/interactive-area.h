#ifndef _FRAMEWORK_TERMINAL_INTERACTIVE_AREA_H_
#define _FRAMEWORK_TERMINAL_INTERACTIVE_AREA_H_

#include <dependencies.h>

#include "buffer/terminal-format.h"

#include <base/strings/string.h>

namespace Framework {
using namespace Base;

namespace Terminal {
namespace Private {
class InteractiveArea;
class InteractiveAreaManager;
}

class Cursor;

class InteractiveArea
{
public:
  BEGIN_ENUMERATION(Order,:int)
    DEFAULT_OUTPUT = std::numeric_limits<Order::Value>::min(),
    TERMINAL_USER_INPUT = DEFAULT_OUTPUT+1,

    AUTOCOMPLETE = TERMINAL_USER_INPUT + 10,

    UNITTEST_PROGRESS = 0x1000000
  ENUMERATION_BASIC_OPERATORS(Order)
  END_ENUMERATION;

  typedef shared_ptr<InteractiveArea> Ptr;

private:
  shared_ptr<Private::InteractiveArea> internalInteractiveArea;
  const Order order;

public:
  InteractiveArea(Order order);

  static Ptr create(Order order);

public:
  void ensureBeginningOfLine();
  void writeOut(const Format& format, const String& text);
  void clear();

  String asPlainText() const;


  template<typename... argument_types>
  void writeOut(const Format& format, const String& text, const argument_types& ... arguments)
  {
    writeOut(format, String::compose(text, arguments...));
  }

  template<typename... argument_types>
  void writeOut(const String& text, const argument_types& ... arguments)
  {
    writeOut(Format(), String::compose(text, arguments...));
  }

  bool tryGetCursors(const Output<Cursor>& start, const Output<Cursor>& end) const;
};


}
}

#endif
