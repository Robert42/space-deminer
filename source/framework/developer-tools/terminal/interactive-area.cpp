#include <framework/developer-tools/terminal/interactive-area.h>
#include <framework/developer-tools/terminal/private-interactive-area.h>
#include <framework/developer-tools/terminal/terminal-manager.h>

namespace Framework {
namespace Terminal {

InteractiveArea::InteractiveArea(Order order)
  : order(order)
{
}

InteractiveArea::Ptr InteractiveArea::create(Order order)
{
  return Ptr(new InteractiveArea(order));
}


void InteractiveArea::ensureBeginningOfLine()
{
  // If the internal interactive area is not set, we are already on the beginning of the very first line
  if(!this->internalInteractiveArea)
    return;

  this->internalInteractiveArea->ensureBeginningOfLine();
}

void InteractiveArea::writeOut(const Format& format, const String& text)
{
  if(!this->internalInteractiveArea)
  {
    this->internalInteractiveArea = Manager::singleton()->implementation->createInteractiveArea(order);
    if(!this->internalInteractiveArea)
      return;
  }

  this->internalInteractiveArea->writeOut(format, text);
}

void InteractiveArea::clear()
{
  this->internalInteractiveArea.reset();
}

String InteractiveArea::asPlainText() const
{
  if(!this->internalInteractiveArea)
    return String();

  return this->internalInteractiveArea->asPlainText();
}

bool InteractiveArea::tryGetCursors(const Output<Cursor>& start, const Output<Cursor>& end) const
{
  if(!this->internalInteractiveArea)
    return false;

  start.value.reset(this->internalInteractiveArea->startCursor());
  end.value.reset(this->internalInteractiveArea->endCursor());

  return true;
}


}
}
