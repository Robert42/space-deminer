#include <dependencies.h>
#include <framework/developer-tools/terminal/terminal-viewport.h>

namespace Framework {
namespace Terminal {
typedef Buffer::Line Line;

Viewport::Viewport(const Buffer::Ptr& buffer)
  : _buffer(buffer),
    _viewportStart(buffer),
    lastPossibleViewportStart(buffer)
{
  _width = _height = 4;

  connectToBuffer();

  connectionsToBlockWhileSwappingBuffers.push_back(lastPossibleViewportStart.signalCursorWasMoved().connect(std::bind(&Viewport::ensureViewportStartIsInTheRightSpot, this)).track(this->trackable).trackManually());
  connectionsToBlockWhileSwappingBuffers.push_back(_viewportStart.signalCursorWasMoved().connect(std::bind(&Viewport::ensureViewportStartIsInTheRightSpot, this)).track(this->trackable).trackManually());
  connectionsToBlockWhileSwappingBuffers.push_back(_viewportStart.signalCursorWasMoved().connect(std::bind(&Viewport::sendRedrawNeededSignal, this)).track(this->trackable).trackManually());

  connectionsToBlockWhileSwappingBuffers.push_back(signalSizeChanged().connect(std::bind(&Viewport::recalculateLastPossibleViewportStart, this)).track(this->trackable).trackManually());
  connectionsToBlockWhileSwappingBuffers.push_back(signalSizeChanged().connect(std::bind(&Viewport::jumpToLastPossiblePoint, this)).track(this->trackable).trackManually());
  connectionsToBlockWhileSwappingBuffers.push_back(signalSizeChanged().connect(std::bind(&Viewport::sendRedrawNeededSignal, this)).track(this->trackable).trackManually());

  recalculateLastPossibleViewportStart();
}


Viewport::~Viewport()
{
}

Buffer::ConstPtr Viewport::buffer() const
{
  return _buffer;
}

const Cursor& Viewport::viewportStart() const
{
  return _viewportStart;
}

void Viewport::setSize(size_t width, size_t height)
{
  this->_width = clamp<size_t>(width, 4, 1024);
  this->_height = clamp<size_t>(height, 2, 1024);

  _signalSizeChanged();
}

size_t Viewport::width() const
{
  return _width;
}

size_t Viewport::height() const
{
  return _height;
}

ivec2 Viewport::positionRelativeToStartOfLine(size_t position) const
{
  return ivec2(position % _width, position / _width);
}



int Viewport::positionWithinViewport(const Line::ConstPtr& line) const
{
  bool invert = false;

  Line::ConstPtr a = this->viewportStart().line();
  Line::ConstPtr b = line;

  int positionOfStartcursorWithinItsLine = positionRelativeToStartOfLine(this->viewportStart().positionInLine()).y;

  if(a==b)
  {
    return -positionOfStartcursorWithinItsLine;
  }

  if(!_buffer->areThisLinesConsecutive(a, b))
  {
    exchange(&a, &b);
    invert = true;
  }

  Buffer::const_iterator i = _buffer->find(a);

  int distanceBetweenLines = 0;

  while(*i != b)
  {
    distanceBetweenLines += heightOfLine(*i);
    ++i;
  }

  if(invert)
    distanceBetweenLines = -distanceBetweenLines;

  distanceBetweenLines -= positionOfStartcursorWithinItsLine;

  return distanceBetweenLines;
}



ivec2 Viewport::positionWithinViewport(const Cursor& cursor) const
{
  return ivec2(0, positionWithinViewport(cursor.line())) + positionRelativeToStartOfLine(cursor.positionInLine());
}


Optional<ivec2> Viewport::optionalPositionWithinViewport(const Cursor& cursor)
{
  if(cursor.buffer() != _buffer)
    return nothing;

  ivec2 position = positionWithinViewport(cursor);
  int y = position.y;
  if(y>=0 && y<static_cast<int>(_height))
    return position;
  else
    return nothing;
}



int Viewport::heightOfLine(const Line::ConstPtr& line) const
{
  if(line->content().empty())
    return 1;

  return (line->length() + _width - 1) / _width;
}

void Viewport::moveCursorUp(Cursor& cursor)
{
  if(cursor.positionInLine() < _width)
  {
    Optional<Line::Ptr> l = _buffer->prevLineOf(cursor.line());
    if(!l)
      return;

    Line::Ptr prevLine = *l;

    size_t lineLength = prevLine->length();
    size_t maxLinePos = lineLength;

    if(maxLinePos > 0)
      maxLinePos -= 1;

    size_t positioninPrevLine = min<size_t>(maxLinePos%_width,
                                            cursor.positionInLine())
                                + maxLinePos - maxLinePos%_width;
    cursor.moveToPositionInBuffer(prevLine, positioninPrevLine);
    return;
  }

  cursor.moveToPositionInLine(cursor.positionInLine() - _width);
}

void Viewport::moveCursorDown(Cursor& cursor)
{
  size_t lengthOfCurrentLine = cursor.line()->length();

  if(cursor.positionInLine()-cursor.positionInLine()%_width + _width >= lengthOfCurrentLine)
  {
    Optional<Line::Ptr> l = _buffer->nextLineOf(cursor.line());
    if(!l)
      return;
    Line::Ptr nextLine = *l;

    size_t nextLineLength = nextLine->length();

    size_t positionInNextLine = min<size_t>(nextLineLength,
                                            cursor.positionInLine()%_width);
    cursor.moveToPositionInBuffer(nextLine, positionInNextLine);
    return;
  }

  cursor.moveToPositionInLine(cursor.positionInLine() + _width);
}


void Viewport::recalculateLastPossibleViewportStart()
{
  size_t l = _buffer->lastLine()->length();

  if(l > 0)
    l--;

  l -= l % _width;

  lastPossibleViewportStart.moveToPositionInBuffer(_buffer->lastLine(), l);

  for(size_t i=1; i<_height; ++i)
    moveCursorUp(lastPossibleViewportStart);
}



void Viewport::ensureViewportStartIsInTheRightSpot()
{
  if(!_buffer->containsLine(this->_viewportStart.line()) || !_buffer->containsLine(this->lastPossibleViewportStart.line()))
  {
    jumpToLastPossiblePoint();
    return;
  }

  if(!_buffer->areThisLinesConsecutive(this->_viewportStart.line(), this->lastPossibleViewportStart.line()))
  {
    jumpToLastPossiblePoint();
    return;
  }

  if(this->_viewportStart.line()==this->lastPossibleViewportStart.line() && this->_viewportStart.positionInLine()>this->lastPossibleViewportStart.positionInLine())
    _viewportStart.moveToPositionInLine(this->lastPossibleViewportStart.positionInLine());
}


void Viewport::jumpToLastPossiblePoint()
{
  this->_viewportStart = this->lastPossibleViewportStart;
}

void Viewport::sendRedrawNeededSignal()
{
  _signalRedrawNeeded();
}

Signals::Signal<void()>& Viewport::signalSizeChanged()
{

  return _signalSizeChanged;
}

Signals::Signal<void()>& Viewport::signalRedrawNeeded()
{
  return _signalRedrawNeeded;
}

void Viewport::moveUp(int lines)
{
  while(lines > 0)
  {
    moveCursorUp(_viewportStart);
    lines--;
  }
}

void Viewport::moveDown(int lines)
{
  while(lines > 0)
  {
    moveCursorDown(_viewportStart);
    lines--;
  }
}

void Viewport::move(int lines)
{
  if(lines >= 0)
    moveUp(lines);
  else
    moveDown(-lines);
}

void Viewport::connectToBuffer()
{
  disconnectFromBuffer();

  connectionsFromBuffer.push_back(_buffer->signalCleared().connect(std::bind(&Viewport::recalculateLastPossibleViewportStart, this)).track(this->trackable).trackManually());
  connectionsFromBuffer.push_back(_buffer->signalCleared().connect(std::bind(&Viewport::sendRedrawNeededSignal, this)).track(this->trackable).trackManually());
  connectionsFromBuffer.push_back(_buffer->signalLineModified().connect(std::bind(&Viewport::recalculateLastPossibleViewportStart, this)).track(this->trackable).trackManually());
  connectionsFromBuffer.push_back(_buffer->signalLineModified().connect(std::bind(&Viewport::jumpToLastPossiblePoint, this)).track(this->trackable).trackManually());
  connectionsFromBuffer.push_back(_buffer->signalLineModified().connect(std::bind(&Viewport::sendRedrawNeededSignal, this)).track(this->trackable).trackManually());
  connectionsFromBuffer.push_back(_buffer->signalLineInserted().connect(std::bind(&Viewport::recalculateLastPossibleViewportStart, this)).track(this->trackable).trackManually());
  connectionsFromBuffer.push_back(_buffer->signalLineInserted().connect(std::bind(&Viewport::jumpToLastPossiblePoint, this)).track(this->trackable).trackManually());
  connectionsFromBuffer.push_back(_buffer->signalLineInserted().connect(std::bind(&Viewport::sendRedrawNeededSignal, this)).track(this->trackable).trackManually());
  connectionsFromBuffer.push_back(_buffer->signalLineRemoved().connect(std::bind(&Viewport::recalculateLastPossibleViewportStart, this)).track(this->trackable).trackManually());
  connectionsFromBuffer.push_back(_buffer->signalLineRemoved().connect(std::bind(&Viewport::jumpToLastPossiblePoint, this)).track(this->trackable).trackManually());
  connectionsFromBuffer.push_back(_buffer->signalLineRemoved().connect(std::bind(&Viewport::sendRedrawNeededSignal, this)).track(this->trackable).trackManually());
}

void Viewport::disconnectFromBuffer()
{
  for(Signals::Connection& connection : connectionsFromBuffer)
    connection.disconnect();

  connectionsFromBuffer.clear();
}

void Viewport::blockWhileSwappingBuffers(bool block)
{
  if(block)
  {
    for(Signals::Connection& connection : connectionsToBlockWhileSwappingBuffers)
      connectionBlocker.push_back(connection.block());
  }else
  {
    connectionBlocker.clear();
  }
}

void Viewport::setBuffer(const Buffer::Ptr& buffer)
{
  blockWhileSwappingBuffers(true);

  disconnectFromBuffer();

  this->_buffer = buffer;

  _viewportStart.reset(buffer);
  lastPossibleViewportStart.reset(buffer);

  connectToBuffer();

  blockWhileSwappingBuffers(false);
  recalculateLastPossibleViewportStart();
  sendRedrawNeededSignal();
}


}
}
