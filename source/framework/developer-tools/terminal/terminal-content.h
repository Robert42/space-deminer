#ifndef _FRAMEWORK_TERMINAL_CONTENT_H_
#define _FRAMEWORK_TERMINAL_CONTENT_H_

#include <dependencies.h>

#include "terminal-viewport.h"

namespace Framework {
namespace Terminal {

class Content final : public noncopyable
{
public:
  typedef Buffer::Line Line;

private:
  class ContentForFormat final : public noncopyable
  {
  private:
    size_t lineOffset, inlineOffset;
    String _content;
    bool used;

  public:
    ContentForFormat();

    bool isUsed() const;
    void clear();

    const String& content() const;

  public:
    void fillWithWhitespaceForNextSection(size_t lineOffset,
                                          size_t inlineOffset);
    void inject(const String::value_type* begin,
                const String::value_type* end,
                size_t viewportWidth);

  private:
    FRIEND_TEST(TerminalContent_ContentForFormat, fillWithWhitespaceForNextSection);
    FRIEND_TEST(TerminalContent_ContentForFormat, inject);
  };

  FRIEND_TEST(TerminalContent_ContentForFormat, fillWithWhitespaceForNextSection);
  FRIEND_TEST(TerminalContent_ContentForFormat, inject);

public:
  Signals::Trackable trackable;

private:
  Viewport& viewport;
  bool dirty;

  std::map<Format, ContentForFormat> content;

public:
  Content(Viewport& viewport);
  ~Content();

  Optional<String> contentAsPlainString(Format formatting);

  bool isDirty() const;

private:
  void markDirty();
  void recalculateContent();

  size_t calcNumberOfCharactersToUseOfLine(size_t lineIndexRelativeToViewport) const;

  FRIEND_TEST(Content, calcNumberOfCharactersToUseOfLine);
};

}
}


#endif
