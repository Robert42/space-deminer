#ifndef _FRAMEWORK_TERMINAL_VIEWPORT_H_
#define _FRAMEWORK_TERMINAL_VIEWPORT_H_

#include <dependencies.h>

#include "terminal-cursor.h"

namespace Framework {
using namespace Base;

namespace Terminal {

class Viewport final : noncopyable
{
public:
  typedef Buffer::Line Line;

public:
  Signals::Trackable trackable;

private:
  Buffer::Ptr _buffer;

  Cursor _viewportStart;
  size_t _width, _height;

  Cursor lastPossibleViewportStart;

  std::list<Signals::Connection> connectionsFromBuffer, connectionsToBlockWhileSwappingBuffers;
  std::list<Signals::Connection::Blocker> connectionBlocker;

  Signals::CallableSignal<void()> _signalSizeChanged;
  Signals::CallableSignal<void()> _signalRedrawNeeded;

public:
  Viewport(const Buffer::Ptr& buffer);
  ~Viewport();

  Buffer::ConstPtr buffer() const;
  const Cursor& viewportStart() const;

  void setBuffer(const Buffer::Ptr& buffer);

  void setSize(size_t width, size_t height);
  size_t width() const;
  size_t height() const;

public:
  Signals::Signal<void()>& signalSizeChanged();
  Signals::Signal<void()>& signalRedrawNeeded();

  void moveUp(int lines);
  void moveDown(int lines);

  /**
   * ```
   * if(lines >= 0)
   *   moveDown(lines);
   * else
   *   moveDown(-lines);
   * ```
   */
  void move(int lines);

public:
  ivec2 positionRelativeToStartOfLine(size_t position) const;
  int heightOfLine(const Line::ConstPtr& line) const;

  int positionWithinViewport(const Line::ConstPtr& line) const;
  Optional<ivec2> optionalPositionWithinViewport(const Cursor& cursor);
  ivec2 positionWithinViewport(const Cursor& cursor) const;

private:
  void moveCursorUp(Cursor& c);
  void moveCursorDown(Cursor& c);

  FRIEND_TEST(framework_terminal_Viewport, moveCursorUp);
  FRIEND_TEST(framework_terminal_Viewport, moveCursorUp_at_last_position);
  FRIEND_TEST(framework_terminal_Viewport, moveCursorUp_at_first_position);
  FRIEND_TEST(framework_terminal_Viewport, moveCursorUp_special_case);
  FRIEND_TEST(framework_terminal_Viewport, moveCursorDown);
  FRIEND_TEST(framework_terminal_Viewport, moveCursorDown_at_last_position);
  FRIEND_TEST(framework_terminal_Viewport, moveCursorDown_at_first_position);
  FRIEND_TEST(framework_terminal_Viewport, moveCursorDown_condition);

  FRIEND_TEST(framework_terminal_Viewport, recalculateLastPossibleViewportStart);
  FRIEND_TEST(framework_terminal_Viewport, recalculateLastPossibleViewportStart_empty_buffer);
  FRIEND_TEST(framework_terminal_Viewport, recalculateLastPossibleViewportStart_too_small_buffer);

private:
  void recalculateLastPossibleViewportStart();
  void ensureViewportStartIsInTheRightSpot();

  void jumpToLastPossiblePoint();

  void sendRedrawNeededSignal();

  void connectToBuffer();
  void disconnectFromBuffer();
  void blockWhileSwappingBuffers(bool block);
};

}
}

#endif
