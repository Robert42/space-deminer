#ifndef _FRAMEWORK_TERMINAL_PRIVATE_INTERACTIVE_AREA_H_
#define _FRAMEWORK_TERMINAL_PRIVATE_INTERACTIVE_AREA_H_

#include <dependencies.h>

#include <base/strings/string.h>
#include <base/output.h>

#include "terminal-cursor.h"
#include "interactive-area.h"

namespace Framework {
using namespace Base;

namespace Terminal {
namespace Private {

class InteractiveArea : noncopyable
{
  friend class InteractiveAreaManager;

public:
  typedef Framework::Terminal::InteractiveArea::Order Order;
  typedef shared_ptr<InteractiveArea> Ptr;
  typedef weak_ptr<InteractiveArea> WeakPtr;

  bool avoidToEndWithEmptyLine;

private:
  Cursor _startCursor, _endCursor;
  bool _endingWithEmptyLine;

private:
  InteractiveArea(Cursor _startCursor);

public:
  /** Clears the whole Content to a single emptyLine of interactive Area
   *
   */
  void clearWholeArea();

  void ensureBeginningOfLine();

  void writeOut(const Format& format, const String& text);
  void writeOut(const String& text);

  const Cursor& startCursor() const;
  const Cursor& endCursor() const;

  ~InteractiveArea();

  String asPlainText() const;
};

class InteractiveAreaManager
{
private:
  Buffer::Ptr buffer;
  std::map<InteractiveArea::Order::Value, InteractiveArea::WeakPtr> interactiveAreas;

public:
  InteractiveAreaManager(const Buffer::Ptr& buffer);

  /** Always returns a valid interactive area.
   *
   * \return Returns false if an area with this order already exists.
   */
  bool tryCreateInteractiveArea(InteractiveArea::Order order, const Output<InteractiveArea::Ptr>& interactiveArea);

private:
  Cursor findRightCursorForNewInteractiveAreaWithOrder(InteractiveArea::Order order);

  FRIEND_TEST(framework_terminal_InteractiveAreaManager, findRightCursorForNewInteractiveAreaWithOrder);
};

}
}
}

#endif
