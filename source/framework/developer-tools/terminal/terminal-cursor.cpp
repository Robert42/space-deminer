#include <dependencies.h>
#include <framework/developer-tools/terminal/terminal-cursor.h>

namespace Framework {
namespace Terminal {
typedef Buffer::Line Line;

Cursor::Cursor(const Buffer::Ptr& buffer)
  : _buffer(buffer)
{
  _positionInLine = 0;
  _line = buffer->lastLine();

  moveToLastPositionInBuffer();

  connectToBufferSignals();
}

Cursor::Cursor(const Buffer::Ptr& buffer, const Line::Ptr& line, size_t positionInLine)
  : _buffer(buffer)
{
  this->_positionInLine = positionInLine;
  this->_line = line;

  connectToBufferSignals();
}

Cursor::Cursor(const Cursor& other)
  : _buffer(other._buffer)
{
  _positionInLine = other.positionInLine();
  _line = other._line;

  connectToBufferSignals();
}

void Cursor::connectToBufferSignals()
{
  disconnectFromBufferSignals();
  bufferSignalConnection_handleClearedBuffer = _buffer->signalCleared().connect(std::bind(&Cursor::handleClearedBuffer, this)).track(this->trackable).trackManually();
  bufferSignalConnection_handleModifiedLine = _buffer->signalLineModified().connect(std::bind(&Cursor::handleModifiedLine, this, _1)).track(this->trackable).trackManually();
  bufferSignalConnection_handleLineGoingToBeRemoved = _buffer->signalGoingToRemoveLine().connect(std::bind(&Cursor::handleLineGoingToBeRemoved, this, _1)).track(this->trackable).trackManually();
}

void Cursor::disconnectFromBufferSignals()
{
  bufferSignalConnection_handleClearedBuffer.disconnect();
  bufferSignalConnection_handleModifiedLine.disconnect();
  bufferSignalConnection_handleLineGoingToBeRemoved.disconnect();
}

Signals::Signal<void()>& Cursor::signalCursorWasMoved()
{
  return _signalCursorWasMoved;
}

Cursor::~Cursor()
{
}

Cursor& Cursor::operator=(Cursor& other)
{
  if(other._buffer != this->_buffer)
    throw std::logic_error("Called TerminalCursor::operator= with cursors of different buffer");

  this->moveToPositionInBuffer(other.line(), other.positionInLine());

  return *this;
}

void Cursor::reset(Buffer::Ptr buffer)
{
  disconnectFromBufferSignals();

  this->_buffer = buffer;
  this->_line = buffer->lastLine();
  this->_positionInLine = this->_line->length();

  connectToBufferSignals();
  _signalCursorWasMoved();
}

void Cursor::reset(const Cursor& other)
{
  disconnectFromBufferSignals();

  this->_buffer = other._buffer;
  this->_line = other._line;
  this->_positionInLine = other.positionInLine();

  connectToBufferSignals();
  _signalCursorWasMoved();
}

void Cursor::moveToFirstPositionInLine()
{
  if(positionInLine()==0)
    return;

  _positionInLine = 0;

  _signalCursorWasMoved();
}

void Cursor::moveToFirstPositionInBuffer()
{
  if(line()==_buffer->firstLine() && positionInLine()==0)
    return;

  _line = _buffer->firstLine();
  _positionInLine = 0;

  _signalCursorWasMoved();
}

void Cursor::moveToLastPositionInLine()
{
  size_t lastPosition = _line->length();

  if(positionInLine()==lastPosition)
    return;

  _positionInLine = lastPosition;

  _signalCursorWasMoved();
}

void Cursor::moveToLastPositionInBuffer()
{
  size_t lastPosition = _line->length();

  if(line()==_buffer->lastLine() && positionInLine()==lastPosition)
    return;

  _line = _buffer->lastLine();
  _positionInLine = lastPosition;

  _signalCursorWasMoved();
}

void Cursor::moveToPositionInLine(size_t position)
{
  position = clamp<size_t>(position, 0, _line->length());

  if(positionInLine()==position)
    return;

  _positionInLine = position;

  _signalCursorWasMoved();
}

void Cursor::moveToPositionInBuffer(const Line::Ptr& line, size_t position)
{
  position = clamp<size_t>(position, 0, line->length());

  if(this->positionInLine()==position && line==this->line())
    return;

  this->_line = line;
  this->_positionInLine = position;

  _signalCursorWasMoved();
}

Buffer::ConstPtr Cursor::buffer() const
{
  return _buffer;
}

const Line::Ptr& Cursor::line()
{
  return _line;
}

Line::ConstPtr Cursor::line() const
{
  return _line;
}

size_t Cursor::positionInLine() const
{
  return _positionInLine;
}



bool Cursor::deleteFollowingCharacter()
{
  assert(this->positionInLine() <= this->line()->length());

  if(this->positionInLine() == this->line()->length())
    return false;

  _buffer->deleteCharacterInLine(this->line(), this->positionInLine());

  return true;
}


bool Cursor::deleteLeadingCharacter()
{
  assert(this->positionInLine() <= this->line()->length());

  if(this->positionInLine() == 0)
    return false;

  size_t new_position = positionInLine() - 1;

  this->moveToPositionInLine(new_position);

  this->deleteFollowingCharacter();

  return true;
}


void Cursor::deleteCharactersTo(size_t i)
{
  while(i > this->positionInLine() && deleteFollowingCharacter())
    --i;
  while(i < this->positionInLine() && deleteLeadingCharacter());
}


bool Cursor::insertCharacter(String::value_type character)
{
  size_t new_position = positionInLine() + 1;

  _buffer->insertCharacterIntoLine(line(), positionInLine(), character);

  this->moveToPositionInLine(new_position);

  return true;
}


bool Cursor::insertString(const String& str)
{
  bool result = true;
  for(String::value_type c : str)
    result &= insertCharacter(c);
  return result;
}

void Cursor::pushAtEndOfLine(const String& text, const Format& format)
{
  Line::Ptr line = this->_buffer->push(this->line(), text, format);
  moveToPositionInBuffer(line, line->length());
}

void Cursor::clearLine()
{
  _buffer->clearLine(line());
}

void Cursor::removeLine()
{
  _buffer->removeLine(line());
}



void Cursor::handleClearedBuffer()
{
  moveToLastPositionInBuffer();
}


Optional<Line::Ptr> Cursor::nextLine() const
{
  return _buffer->nextLineOf(this->line());
}

bool Cursor::moveToNextLine()
{
  Optional<Line::Ptr> line = nextLine();

  if(line)
  {
    moveToPositionInBuffer(*line, 0);
    return true;
  }else
    return false;
}



void Cursor::handleModifiedLine(const Line::ConstPtr& modifiedLine)
{
  if(modifiedLine != line())
    return;

  if(positionInLine() > line()->length())
    moveToLastPositionInLine();
}

void Cursor::handleLineGoingToBeRemoved(const Line::ConstPtr& lineGoingToBeRemoved)
{
  if(lineGoingToBeRemoved != line())
    return;

  if(lineGoingToBeRemoved == _buffer->lastLine() && lineGoingToBeRemoved == _buffer->firstLine())
  {
    moveToFirstPositionInLine();
  }else if(lineGoingToBeRemoved == _buffer->lastLine())
  {
    Buffer::reverse_iterator iter_line = _buffer->rfind(lineGoingToBeRemoved);
    assert(iter_line != _buffer->rend());
    ++iter_line;

    moveToPositionInBuffer(*iter_line, 0);
  }else
  {
    Buffer::iterator iter_line = _buffer->find(lineGoingToBeRemoved);
    assert(iter_line != _buffer->end());
    ++iter_line;

    moveToPositionInBuffer(*iter_line, 0);
  }
}

bool Cursor::operator<(const Cursor& other) const
{
  return (*this<=other) && this->positionInLine()!=other.positionInLine();
}

bool Cursor::operator>(const Cursor& other) const
{
  return !(*this <= other);
}

bool Cursor::operator<=(const Cursor& other) const
{
  if(!_buffer->areThisLinesConsecutive(this->line(), other.line()))
    return false;

  if(this->line() != other.line())
    return true;

  return this->positionInLine() <= other.positionInLine();
}

bool Cursor::operator>=(const Cursor& other) const
{
  return !(*this < other);
}

bool Cursor::operator==(const Cursor& other) const
{
  return this->line()==other.line() && this->positionInLine()==other.positionInLine();
}

bool Cursor::operator!=(const Cursor& other) const
{
  return !(*this == other);
}


}
}
