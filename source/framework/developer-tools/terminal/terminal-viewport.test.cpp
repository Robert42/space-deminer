#include <dependencies.h>
#include <framework/developer-tools/terminal/terminal-viewport.h>

namespace Framework {
namespace Terminal {
typedef Buffer::Line Line;


TEST(framework_terminal_Viewport, getPositionRelativeToStartOfLine)
{
  Viewport viewport(Buffer::create());
  viewport.setSize(4, 4);

  // Expected positions:
  // 0123
  // 4567
  // 8

  EXPECT_EQ(ivec2(0, 0), viewport.positionRelativeToStartOfLine(0));
  EXPECT_EQ(ivec2(1, 0), viewport.positionRelativeToStartOfLine(1));
  EXPECT_EQ(ivec2(2, 0), viewport.positionRelativeToStartOfLine(2));
  EXPECT_EQ(ivec2(3, 0), viewport.positionRelativeToStartOfLine(3));
  EXPECT_EQ(ivec2(0, 1), viewport.positionRelativeToStartOfLine(4));
  EXPECT_EQ(ivec2(1, 1), viewport.positionRelativeToStartOfLine(5));
  EXPECT_EQ(ivec2(2, 1), viewport.positionRelativeToStartOfLine(6));
  EXPECT_EQ(ivec2(3, 1), viewport.positionRelativeToStartOfLine(7));
  EXPECT_EQ(ivec2(0, 2), viewport.positionRelativeToStartOfLine(8));
}

TEST(framework_terminal_Viewport, tryGetPositionWithinViewport_line)
{
  Buffer::Ptr buffer = Buffer::create();

  // Expected positions:
  // 0123
  // 4567
  // 89
  // abcd
  // efgh
  // ij
  //
  // ABCD
  // EFGH
  // IJ

  buffer->push("0123456789\nabcdefghij\n\nABCDEFGHIJ\n\n\n\n\n\n\n\n");
  std::vector<Line::ConstPtr> lines(buffer->begin(), buffer->end());

  Viewport viewport(buffer);
  viewport.setSize(4, 4);
  viewport.moveUp(100);

  // Viewport:
  // 0123
  // 4567
  // 89
  // abcd
  EXPECT_EQ(0, viewport.positionWithinViewport(lines[0]));
  EXPECT_EQ(3, viewport.positionWithinViewport(lines[1]));
  EXPECT_EQ(6, viewport.positionWithinViewport(lines[2]));
  EXPECT_EQ(7, viewport.positionWithinViewport(lines[3]));

  // Viewport:
  // 4567
  // 89
  // abcd
  // efgh
  viewport.moveDown(1);
  EXPECT_EQ(-1, viewport.positionWithinViewport(lines[0]));
  EXPECT_EQ(2, viewport.positionWithinViewport(lines[1]));
  EXPECT_EQ(5, viewport.positionWithinViewport(lines[2]));
  EXPECT_EQ(6, viewport.positionWithinViewport(lines[3]));

  // Viewport:
  // 89
  // abcd
  // efgh
  // ij
  viewport.moveDown(1);
  EXPECT_EQ(-2, viewport.positionWithinViewport(lines[0]));
  EXPECT_EQ(1, viewport.positionWithinViewport(lines[1]));
  EXPECT_EQ(4, viewport.positionWithinViewport(lines[2]));
  EXPECT_EQ(5, viewport.positionWithinViewport(lines[3]));

  // Viewport:
  // abcd
  // efgh
  // ij
  //
  viewport.moveDown(1);
  EXPECT_EQ(-3, viewport.positionWithinViewport(lines[0]));
  EXPECT_EQ(0, viewport.positionWithinViewport(lines[1]));
  EXPECT_EQ(3, viewport.positionWithinViewport(lines[2]));
  EXPECT_EQ(4, viewport.positionWithinViewport(lines[3]));

  // Viewport:
  // efgh
  // ij
  //
  // ABCD
  viewport.moveDown(1);
  EXPECT_EQ(-4, viewport.positionWithinViewport(lines[0]));
  EXPECT_EQ(-1, viewport.positionWithinViewport(lines[1]));
  EXPECT_EQ(2, viewport.positionWithinViewport(lines[2]));
  EXPECT_EQ(3, viewport.positionWithinViewport(lines[3]));

  // Viewport:
  // ij
  //
  // ABCD
  viewport.moveDown(1);
  EXPECT_EQ(-5, viewport.positionWithinViewport(lines[0]));
  EXPECT_EQ(-2, viewport.positionWithinViewport(lines[1]));
  EXPECT_EQ(1, viewport.positionWithinViewport(lines[2]));
  EXPECT_EQ(2, viewport.positionWithinViewport(lines[3]));

  // Viewport:
  //
  // ABCD
  viewport.moveDown(1);
  EXPECT_EQ(-6, viewport.positionWithinViewport(lines[0]));
  EXPECT_EQ(-3, viewport.positionWithinViewport(lines[1]));
  EXPECT_EQ(0, viewport.positionWithinViewport(lines[2]));
  EXPECT_EQ(1, viewport.positionWithinViewport(lines[3]));

  // Viewport:
  // ABCD
  viewport.moveDown(1);
  EXPECT_EQ(-7, viewport.positionWithinViewport(lines[0]));
  EXPECT_EQ(-4, viewport.positionWithinViewport(lines[1]));
  EXPECT_EQ(-1, viewport.positionWithinViewport(lines[2]));
  EXPECT_EQ(0, viewport.positionWithinViewport(lines[3]));

  // Viewport:
  viewport.moveDown(1);
  EXPECT_EQ(-8, viewport.positionWithinViewport(lines[0]));
  EXPECT_EQ(-5, viewport.positionWithinViewport(lines[1]));
  EXPECT_EQ(-2, viewport.positionWithinViewport(lines[2]));
  EXPECT_EQ(-1, viewport.positionWithinViewport(lines[3]));
}

TEST(framework_terminal_Viewport, getHeightOfLine)
{
  Viewport viewport(Buffer::create());
  viewport.setSize(4, 4);

  // Expected positions:
  // 0123
  // 4567
  // 8

  EXPECT_EQ(1, viewport.heightOfLine(Line::create("")));
  EXPECT_EQ(1, viewport.heightOfLine(Line::create("0")));
  EXPECT_EQ(1, viewport.heightOfLine(Line::create("01")));
  EXPECT_EQ(1, viewport.heightOfLine(Line::create("012")));
  EXPECT_EQ(1, viewport.heightOfLine(Line::create("0123")));
  EXPECT_EQ(1, viewport.heightOfLine(Line::create("0123")));
  EXPECT_EQ(2, viewport.heightOfLine(Line::create("01234")));
  EXPECT_EQ(2, viewport.heightOfLine(Line::create("012345")));
  EXPECT_EQ(2, viewport.heightOfLine(Line::create("0123456")));
  EXPECT_EQ(2, viewport.heightOfLine(Line::create("01234567")));
  EXPECT_EQ(3, viewport.heightOfLine(Line::create("012345678")));
}


TEST(framework_terminal_Viewport, moveCursorUp)
{
  Buffer::Ptr buffer = Buffer::create();

  // Expected positions:
  // 0123
  // 4567
  // 89
  // abcd
  // efgh
  // ij
  //
  // ABCD
  // EFGH
  // IJ

  buffer->push("0123456789\nabcdefghij\n\nABCDEFGHIJ");
  std::vector<Line::ConstPtr> lines(buffer->begin(), buffer->end());

  Viewport viewport(buffer);
  viewport.setSize(4, 4);

  Cursor a(buffer);

  a.moveToLastPositionInBuffer();

  EXPECT_EQ(lines[3], a.line());
  EXPECT_EQ(10, static_cast<int>(a.positionInLine()));

  viewport.moveCursorUp(a);
  EXPECT_EQ(lines[3], a.line());
  EXPECT_EQ(6, static_cast<int>(a.positionInLine()));

  viewport.moveCursorUp(a);
  EXPECT_EQ(lines[3], a.line());
  EXPECT_EQ(2, static_cast<int>(a.positionInLine()));

  viewport.moveCursorUp(a);
  EXPECT_EQ(lines[2], a.line());
  EXPECT_EQ(0, static_cast<int>(a.positionInLine()));

  viewport.moveCursorUp(a);
  EXPECT_EQ(lines[1], a.line());
  EXPECT_EQ(8, static_cast<int>(a.positionInLine()));

  viewport.moveCursorUp(a);
  EXPECT_EQ(lines[1], a.line());
  EXPECT_EQ(4, static_cast<int>(a.positionInLine()));

  viewport.moveCursorUp(a);
  EXPECT_EQ(lines[1], a.line());
  EXPECT_EQ(0, static_cast<int>(a.positionInLine()));

  viewport.moveCursorUp(a);
  EXPECT_EQ(lines[0], a.line());
  EXPECT_EQ(8, static_cast<int>(a.positionInLine()));

  viewport.moveCursorUp(a);
  EXPECT_EQ(lines[0], a.line());
  EXPECT_EQ(4, static_cast<int>(a.positionInLine()));

  viewport.moveCursorUp(a);
  EXPECT_EQ(lines[0], a.line());
  EXPECT_EQ(0, static_cast<int>(a.positionInLine()));

  viewport.moveCursorUp(a);
  EXPECT_EQ(lines[0], a.line());
  EXPECT_EQ(0, static_cast<int>(a.positionInLine()));

  a.moveToPositionInLine(3);
  viewport.moveCursorUp(a);
  EXPECT_EQ(lines[0], a.line());
  EXPECT_EQ(3, static_cast<int>(a.positionInLine()));
}

TEST(framework_terminal_Viewport, moveCursorUp_at_last_position)
{
  Buffer::Ptr buffer = Buffer::create();

  // Expected positions:
  // 0123
  // 4567
  // 89
  // abcd
  // efgh
  // ij
  //
  // ABCD
  // EFGH
  // IJ

  buffer->push("0123456789\nabcdefghij\n\nABCDEFGHIJ");
  std::vector<Line::ConstPtr> lines(buffer->begin(), buffer->end());

  Viewport viewport(buffer);
  viewport.setSize(4, 4);

  Cursor a(buffer);

  a.moveToLastPositionInBuffer();

  a.moveToPositionInLine(7);
  viewport.moveCursorUp(a);
  EXPECT_EQ(lines[3], a.line());
  EXPECT_EQ(3, static_cast<int>(a.positionInLine()));
}

TEST(framework_terminal_Viewport, moveCursorUp_at_first_position)
{
  Buffer::Ptr buffer = Buffer::create();

  // Expected positions:
  // 0123
  // 4567
  // 89
  // abcd
  // efgh
  // ij
  //
  // ABCD
  // EFGH
  // IJ

  buffer->push("0123456789\nabcdefghij\n\nABCDEFGHIJ");
  std::vector<Line::ConstPtr> lines(buffer->begin(), buffer->end());

  Viewport viewport(buffer);
  viewport.setSize(4, 4);

  Cursor a(buffer);

  a.moveToLastPositionInBuffer();

  a.moveToPositionInLine(8);
  viewport.moveCursorUp(a);
  EXPECT_EQ(lines[3], a.line());
  EXPECT_EQ(4, static_cast<int>(a.positionInLine()));
}

TEST(framework_terminal_Viewport, moveCursorUp_special_case)
{
  Buffer::Ptr buffer = Buffer::create();

  // Expected positions:
  // 0123
  // 4567
  // 89ab
  // ABCD
  // EFGH
  // IJ

  buffer->push("0123456789ab\nABCDEFGHIJ");
  std::vector<Line::ConstPtr> lines(buffer->begin(), buffer->end());

  Viewport viewport(buffer);
  viewport.setSize(4, 4);

  Cursor a(buffer);

  a.moveToLastPositionInBuffer();

  EXPECT_EQ(lines[1], a.line());
  EXPECT_EQ(10, static_cast<int>(a.positionInLine()));

  viewport.moveCursorUp(a);
  EXPECT_EQ(lines[1], a.line());
  EXPECT_EQ(6, static_cast<int>(a.positionInLine()));

  viewport.moveCursorUp(a);
  EXPECT_EQ(lines[1], a.line());
  EXPECT_EQ(2, static_cast<int>(a.positionInLine()));

  viewport.moveCursorUp(a);
  EXPECT_EQ(lines[0], a.line());
  EXPECT_EQ(10, static_cast<int>(a.positionInLine()));
}

TEST(framework_terminal_Viewport, narrowViewport)
{
  Buffer::Ptr buffer = Buffer::create();

  // Expected positions:
  // space-deminer dev-t
  // erminal $ print(\"Äp
  // fel heißen so :)\");
  // Äpfel heißen so :)
  // space-deminer dev-t
  // erminal $

  buffer->push("space-deminer dev-terminal $ print(\"Äpfel heißen so :)\");\n"
               "Äpfel heißen so :)\n"
               "space-deminer dev-terminal $ ");
  std::vector<Line::ConstPtr> lines(buffer->begin(), buffer->end());

  Viewport viewport(buffer);
  viewport.setSize(19, 2);
  viewport.moveDown(1024);

  Cursor a = viewport.viewportStart();
  EXPECT_EQ(lines[2], a.line());
  EXPECT_EQ(0, a.positionInLine());

  viewport.moveUp(1);
  Cursor b = viewport.viewportStart();
  EXPECT_EQ(lines[1], b.line());
  EXPECT_EQ(0, b.positionInLine());

  viewport.moveUp(1);
  Cursor c = viewport.viewportStart();
  EXPECT_EQ(lines[0], c.line());
  EXPECT_EQ(38, c.positionInLine());

  viewport.moveUp(1);
  Cursor d = viewport.viewportStart();
  EXPECT_EQ(lines[0], d.line());
  EXPECT_EQ(19, d.positionInLine());

  viewport.moveUp(1);
  Cursor e = viewport.viewportStart();
  EXPECT_EQ(lines[0], e.line());
  EXPECT_EQ(0, e.positionInLine());
}

TEST(framework_terminal_Viewport, moveCursorDown)
{
  Buffer::Ptr buffer = Buffer::create();

  // Expected positions:
  // 0123
  // 4567
  // 89
  // abcd
  // efgh
  // ij
  //
  // ABCD
  // EFGH
  // IJ

  buffer->push("0123456789\nabcdefghij\n\nABCDEFGHIJ");
  std::vector<Line::ConstPtr> lines(buffer->begin(), buffer->end());

  Viewport viewport(buffer);
  viewport.setSize(4, 4);

  Cursor a(buffer);

  a.moveToFirstPositionInBuffer();
  a.moveToPositionInLine(3);

  EXPECT_EQ(lines[0], a.line());
  EXPECT_EQ(3, static_cast<int>(a.positionInLine()));

  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[0], a.line());
  EXPECT_EQ(7, static_cast<int>(a.positionInLine()));

  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[0], a.line());
  EXPECT_EQ(10, static_cast<int>(a.positionInLine()));

  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[1], a.line());
  EXPECT_EQ(2, static_cast<int>(a.positionInLine()));

  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[1], a.line());
  EXPECT_EQ(6, static_cast<int>(a.positionInLine()));

  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[1], a.line());
  EXPECT_EQ(10, static_cast<int>(a.positionInLine()));

  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[2], a.line());
  EXPECT_EQ(0, static_cast<int>(a.positionInLine()));

  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[3], a.line());
  EXPECT_EQ(0, static_cast<int>(a.positionInLine()));

  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[3], a.line());
  EXPECT_EQ(4, static_cast<int>(a.positionInLine()));

  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[3], a.line());
  EXPECT_EQ(8, static_cast<int>(a.positionInLine()));

  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[3], a.line());
  EXPECT_EQ(8, static_cast<int>(a.positionInLine()));
}

TEST(framework_terminal_Viewport, moveCursorDown_at_last_position)
{
  Buffer::Ptr buffer = Buffer::create();

  // Expected positions:
  // 0123
  // 4567
  // 89
  // abcd
  // efgh
  // ij
  //
  // ABCD
  // EFGH
  // IJ

  buffer->push("0123456789\nabcdefghij\n\nABCDEFGHIJ");
  std::vector<Line::ConstPtr> lines(buffer->begin(), buffer->end());

  Viewport viewport(buffer);
  viewport.setSize(4, 4);

  Cursor a(buffer);

  a.moveToLastPositionInBuffer();

  a.moveToPositionInLine(3);
  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[3], a.line());
  EXPECT_EQ(7, static_cast<int>(a.positionInLine()));
}

TEST(framework_terminal_Viewport, moveCursorDown_at_first_position)
{
  Buffer::Ptr buffer = Buffer::create();

  // Expected positions:
  // 0123
  // 4567
  // 89
  // abcd
  // efgh
  // ij
  //
  // ABCD
  // EFGH
  // IJ

  buffer->push("0123456789\nabcdefghij\n\nABCDEFGHIJ");
  std::vector<Line::ConstPtr> lines(buffer->begin(), buffer->end());

  Viewport viewport(buffer);
  viewport.setSize(4, 4);

  Cursor a(buffer);

  a.moveToLastPositionInBuffer();

  a.moveToPositionInLine(0);
  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[3], a.line());
  EXPECT_EQ(4, static_cast<int>(a.positionInLine()));
}

TEST(framework_terminal_Viewport, moveCursorDown_condition)
{
  Buffer::Ptr buffer = Buffer::create();

  // Expected positions:
  // 0123
  // 4567
  // 89ab
  // 0123
  // 4567
  // 89a
  // 0123
  // 4567
  // 89
  // 0123
  // 4567
  // 8
  // 0123
  // 4567
  //
  // 0123

  buffer->push("0123456789ab\n0123456789a\n0123456789\n012345678\n01234567\n\n0123");

  buffer->push("0123456789\nabcdefghij\n\nABCDEFGHIJ");
  std::vector<Line::ConstPtr> lines(buffer->begin(), buffer->end());

  Viewport viewport(buffer);
  viewport.setSize(4, 4);

  Cursor a(buffer);

  a.moveToFirstPositionInBuffer();
  a.moveToPositionInLine(3);

  EXPECT_EQ(lines[0], a.line());
  EXPECT_EQ(3, static_cast<int>(a.positionInLine()));

  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[0], a.line());
  EXPECT_EQ(7, static_cast<int>(a.positionInLine()));

  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[0], a.line());
  EXPECT_EQ(11, static_cast<int>(a.positionInLine()));

  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[1], a.line());
  EXPECT_EQ(3, static_cast<int>(a.positionInLine()));

  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[1], a.line());
  EXPECT_EQ(7, static_cast<int>(a.positionInLine()));

  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[1], a.line());
  EXPECT_EQ(11, static_cast<int>(a.positionInLine()));

  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[2], a.line());
  EXPECT_EQ(3, static_cast<int>(a.positionInLine()));

  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[2], a.line());
  EXPECT_EQ(7, static_cast<int>(a.positionInLine()));

  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[2], a.line());
  EXPECT_EQ(10, static_cast<int>(a.positionInLine()));

  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[3], a.line());
  EXPECT_EQ(2, static_cast<int>(a.positionInLine()));

  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[3], a.line());
  EXPECT_EQ(6, static_cast<int>(a.positionInLine()));

  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[3], a.line());
  EXPECT_EQ(9, static_cast<int>(a.positionInLine()));

  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[4], a.line());
  EXPECT_EQ(1, static_cast<int>(a.positionInLine()));

  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[4], a.line());
  EXPECT_EQ(5, static_cast<int>(a.positionInLine()));

  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[5], a.line());
  EXPECT_EQ(0, static_cast<int>(a.positionInLine()));

  viewport.moveCursorDown(a);
  EXPECT_EQ(lines[6], a.line());
  EXPECT_EQ(0, static_cast<int>(a.positionInLine()));
}

TEST(framework_terminal_Viewport, recalculateLastPossibleViewportStart)
{
  Buffer::Ptr buffer = Buffer::create();

  // Expected positions:
  // 0123
  // 4567
  // 89
  // abcd
  // efgh
  // ij
  //
  // ABCD
  // EFGH
  // IJ

  buffer->push("0123456789\nabcdefghij\n\nABCDEFGHIJ");
  std::vector<Line::ConstPtr> lines(buffer->begin(), buffer->end());

  Viewport viewport(buffer);
  viewport.setSize(4, 4);

  EXPECT_EQ(lines[2], viewport.lastPossibleViewportStart.line());
  EXPECT_EQ(0, static_cast<int>(viewport.lastPossibleViewportStart.positionInLine()));

  buffer->push("KL");

  EXPECT_EQ(lines[2], viewport.lastPossibleViewportStart.line());
  EXPECT_EQ(0, static_cast<int>(viewport.lastPossibleViewportStart.positionInLine()));

  buffer->push("M");

  EXPECT_EQ(lines[3], viewport.lastPossibleViewportStart.line());
  EXPECT_EQ(0, static_cast<int>(viewport.lastPossibleViewportStart.positionInLine()));

  buffer->push("\n");

  EXPECT_EQ(lines[3], viewport.lastPossibleViewportStart.line());
  EXPECT_EQ(4, static_cast<int>(viewport.lastPossibleViewportStart.positionInLine()));
}

TEST(framework_terminal_Viewport, recalculateLastPossibleViewportStart_empty_buffer)
{
  Buffer::Ptr buffer = Buffer::create();

  Viewport viewport(buffer);
  viewport.setSize(4, 4);

  EXPECT_EQ(buffer->firstLine(), viewport.lastPossibleViewportStart.line());
  EXPECT_EQ(0, static_cast<int>(viewport.lastPossibleViewportStart.positionInLine()));
}

TEST(framework_terminal_Viewport, recalculateLastPossibleViewportStart_too_small_buffer)
{
  Buffer::Ptr buffer = Buffer::create();

  Viewport viewport(buffer);
  viewport.setSize(4, 4);

  buffer->push("a");

  EXPECT_EQ(buffer->firstLine(), viewport.lastPossibleViewportStart.line());
  EXPECT_EQ(0, static_cast<int>(viewport.lastPossibleViewportStart.positionInLine()));

  buffer->push("\nb");

  EXPECT_EQ(buffer->firstLine(), viewport.lastPossibleViewportStart.line());
  EXPECT_EQ(0, static_cast<int>(viewport.lastPossibleViewportStart.positionInLine()));

  buffer->push("\nc");

  EXPECT_EQ(buffer->firstLine(), viewport.lastPossibleViewportStart.line());
  EXPECT_EQ(0, static_cast<int>(viewport.lastPossibleViewportStart.positionInLine()));

  buffer->push("\nd");

  EXPECT_EQ(buffer->firstLine(), viewport.lastPossibleViewportStart.line());
  EXPECT_EQ(0, static_cast<int>(viewport.lastPossibleViewportStart.positionInLine()));

  buffer->push("\ne");

  std::vector<Line::ConstPtr> lines(buffer->begin(), buffer->end());

  EXPECT_EQ(lines[1], viewport.lastPossibleViewportStart.line());
  EXPECT_EQ(0, static_cast<int>(viewport.lastPossibleViewportStart.positionInLine()));
}

TEST(framework_terminal_Viewport, setBuffer)
{
  Buffer::Ptr bufferA = Buffer::create();
  Buffer::Ptr bufferB = Buffer::create();
  Viewport viewport(bufferA);

  bufferA->push("a\nb\nc\nd\ne\nf");
  bufferB->push("A\nB\nC\nD\nE\nF");

  EXPECT_EQ(bufferA->asPlainText(), viewport.buffer()->asPlainText());

  viewport.setBuffer(bufferB);

  EXPECT_EQ(bufferB->asPlainText(), viewport.buffer()->asPlainText());

  viewport.setBuffer(bufferA);

  EXPECT_EQ(bufferA->asPlainText(), viewport.buffer()->asPlainText());
}


}
}
