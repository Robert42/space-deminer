#include <framework/developer-tools/terminal.h>

#include <framework/developer-tools/terminal/private-interactive-area.h>
#include <framework/developer-tools/terminal/terminal-manager.h>

namespace Framework {
namespace Terminal {

void ensureBeginningOfLine()
{
  Manager::defaultOutput().ensureBeginningOfLine();
}

void writeOut(const Format& format, const String& text)
{
  Manager::defaultOutput().writeOut(format, text);
}

void clear()
{
  Manager::defaultOutput().clear();
}

}
}
