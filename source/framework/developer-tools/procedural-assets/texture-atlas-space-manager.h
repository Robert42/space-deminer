#ifndef FRAMEWORK_PROCEDURALASSETS_TEXTUREATLASSPACEMANAGER_H
#define FRAMEWORK_PROCEDURALASSETS_TEXTUREATLASSPACEMANAGER_H

#include <dependencies.h>

#include <base/geometry/rectangle.h>
#include <base/enum-macros.h>

namespace Framework {

using namespace Base;

namespace ProceduralAssets {
namespace Private
{

class Region final
{
public:
  ivec2 begin, end;

public:
  Region(const ivec2& begin, const ivec2& end);
  Region();

  static Region largestPossibleRegion()
  {
    return Region(ivec2(0), ivec2(std::numeric_limits<ivec2::value_type>::max()));
  }

  void compressHorizontal();
  void compressVertical();
  void compress(const bvec2& dimensions);
  void stretchHorizontal();
  void stretchVertical();

  ivec2 size()const{return end-begin;}
  int width()const{return size().x;}
  int height()const{return size().y;}

  Region operator+(const ivec2& v) const{return Region(begin+v, end+v);}
  Region operator-(const ivec2& v) const{return *this + (-v);}
  Region operator+(int i) const{return *this + ivec2(i);}
  Region operator-(int i) const{return *this + ivec2(-i);}

  bool clampTo(const ivec2& layerSize);
};

class MipmapLevel final
{
public:
  BEGIN_ENUMERATION(Cell, :uint8)
    EMPTY,
    PARTIALLY_USED,
    FULL
  ENUMERATION_BASIC_OPERATORS(Cell)
  ENUM_TO_STRING_DECLARATION(Cell)
  END_ENUMERATION;

  static_assert(sizeof(Cell)==1, "Cell is expected to use only oen byte");

public:
  QVector<Cell> cells;

private:
  ivec2 _layerSize;

public:
  void init(const ivec2& layerSize);
  void initCompressedX(const MipmapLevel& base);
  void initCompressedY(const MipmapLevel& base);

  Region clamp(Region region) const;

  Cell& cell(const ivec2& index);
  Cell::Value* cellArrayBeginningAt(const ivec2& index);
  const Cell& cell(const ivec2& index) const;
  const Cell::Value* cellArrayBeginningAt(const ivec2& index) const;

  const ivec2& layerSize() const;

  void markAsFull(Region region);

  bool isValueEverywhere(MipmapLevel::Cell cellValue, Region region) const;
  bool isRegionWithPossibleSpace(const Region& region) const;

  void updateRegion(const MipmapLevel& base, Region region = Region::largestPossibleRegion());

private:
  void updateRegionXCompressed(const MipmapLevel& base, const Region& region);
  void updateRegionYCompressed(const MipmapLevel& base, const Region& region);

  static void updateLinesXCompressed(int numberLines, int lineWidth, int targetLineStride, Cell::Value* firstTargetLine, int sourceLineStride, const Cell::Value* firstSourceLine);
  static void updateLinesYCompressed(int numberLines, int lineWidth, int targetLineStride, Cell::Value* firstTargetLine, int sourceLineStride, const Cell::Value* firstSourceLine);
  static void updateLineXCompressed(int lineWidth, Cell::Value* firstTargetLine, const Cell::Value* firstSourceLine);
  static void updateLineYCompressed(int lineWidth, Cell::Value* firstTargetLine, const Cell::Value* firstSourceLine, const Cell::Value* secondSourceLine);

  static void markLinesAsFull(int count, int lineWidth, int stride, Cell::Value* firstLine);
  static void markLineAsFull(int count, Cell::Value* cells);

  static bool areLinesOfRegionWithSpace(int count, int lineWidth, int lineStride, const Cell::Value* line);
  static bool isLineOfRegionWithSpace(int count, const Cell::Value* cells);
  static bool mayLineHaveSpace(int count, const Cell::Value* cells);

  static bool isValueEverywhereInLines(Cell::Value cellValue, int count, int lineWidth, int lineStride, const Cell::Value* line);
  static bool isValueEverywhereInLine(Cell::Value cellValue, int count, const Cell::Value* line);
};

class MipmapTable final
{
public:
  struct PathPoint
  {
    Region region;
    ivec2 level;
  };
  typedef QVector<PathPoint> Path;

  const ivec2 textureSize;
  const ivec2 logTextureSize;
  const ivec2 tableSize;
  QVector<MipmapLevel> levels;

  MipmapTable(const ivec2& textureSize);
  int arrayIndexFromTableIndex(const ivec2& textureIndex) const;
  MipmapLevel& levelForIndex(const ivec2& textureIndex);
  const MipmapLevel& levelForIndex(const ivec2& textureIndex) const;

  void forEachLevelDescendingSize(const std::function<void(MipmapLevel& level)>& forLargestLevel,
                                  const std::function<void(MipmapLevel& level, MipmapLevel& base)>& forCompressedX,
                                  const std::function<void(MipmapLevel& level, MipmapLevel& base)>& forCompressedY);

  Path calculatePathForRegionSize(const ivec2& regionSize) const;

  Optional<Region> searchRegion(const ivec2& size) const;
  void markRegionAsUsed(const Region& region);

private:
  Optional<Region> searchRegion(const Path& path, int i, const ivec2& offset, const QVector<ivec2>& shiftOrder) const;
};

} // namespace Private

class TextureAtlasSpaceManager final
{
public:
  class Item
  {
  public:
    typedef std::shared_ptr<Item> Ptr;

  public:
    Item(){}
    virtual ~Item(){}

    virtual ivec2 size() const = 0;
    virtual bool shouldBeSkipped() const {return false;}
    virtual void setPosition(const ivec2& position) const = 0;
    virtual void reject() {}

    static bool lessThan(Item* a, Item* b);
    static int compareSize(const ivec2& aSize, const ivec2& bSize);
  };

public:
  const ivec2 textureSize;

private:
  Private::MipmapTable mipmapTable;

public:
  TextureAtlasSpaceManager(const ivec2& textureSize);

  Optional<ivec2> allocate(const ivec2& regionSize);

  bool allocateSpaceForMultipleItems(QVector<Item*> items, bool sort=true);
  bool allocateSpaceForMultipleItems(const QVector<Item::Ptr>& items, bool sort=true);
};

} // namespace ProceduralAssets
} // namespace Framework

#endif // FRAMEWORK_PROCEDURALASSETS_TEXTUREATLASSPACEMANAGER_H
