#include "texture-atlas-space-manager.h"

#include <base/threads.h>

namespace Framework {
namespace ProceduralAssets {
namespace Private
{


Region::Region(const ivec2& begin, const ivec2& end)
  : begin(begin),
    end(end)
{
  assert(begin.x <= end.x);
  assert(begin.y <= end.y);
}

Region::Region()
  : Region(largestPossibleRegion())
{
}

void Region::compressHorizontal()
{
  begin.x = begin.x/2;
  end.x = (end.x+1)/2;
}

void Region::compressVertical()
{
  begin.y = begin.y/2;
  end.y = (end.y+1)/2;
}

void Region::compress(const bvec2& dimensions)
{
  if(dimensions.x)
    compressHorizontal();
  if(dimensions.y)
    compressVertical();
}

void Region::stretchHorizontal()
{
  begin.x = begin.x*2;
  end.x = end.x*2;
}

void Region::stretchVertical()
{
  begin.y = begin.y*2;
  end.y = end.y*2;
}

bool Region::clampTo(const ivec2& layerSize)
{
  if(this->end.x<=0 || this->end.y<=0 || this->begin.x>=layerSize.x || this->begin.y>=layerSize.y)
    return false;

  this->begin = max(ivec2(0), this->begin);
  this->end = min(this->end, layerSize);

  return true;
}

std::string MipmapLevel::Cell::toStdString(const Cell& cell)
{
  switch(cell.value)
  {
  case Cell::EMPTY:
    return "Cell::EMPTY";
  case Cell::PARTIALLY_USED:
    return "Cell::PARTIALLY_USED";
  case Cell::FULL:
    return "Cell::FULL";
  default:
    return "";
  }
}

void MipmapLevel::init(const ivec2& layerSize)
{
  assert(layerSize.x > 0);
  assert(layerSize.y > 0);
  assert(isPowerOfTwo(layerSize.x));
  assert(isPowerOfTwo(layerSize.y));

  this->_layerSize = layerSize;

  cells.resize(layerSize.x * layerSize.y);

  memset(cells.data(), 0, cells.size());
}

void MipmapLevel::initCompressedX(const MipmapLevel& base)
{
  init(ivec2(base.layerSize().x/2,
             base.layerSize().y));
}

void MipmapLevel::initCompressedY(const MipmapLevel& base)
{
  init(ivec2(base.layerSize().x,
             base.layerSize().y/2));
}

Region MipmapLevel::clamp(Region region) const
{
  region.clampTo(layerSize());

  return region;
}

MipmapLevel::Cell& MipmapLevel::cell(const ivec2& index)
{
  return cells[index.y * layerSize().x + index.x];
}

MipmapLevel::Cell::Value* MipmapLevel::cellArrayBeginningAt(const ivec2& index)
{
  return &cell(index).value;
}

const MipmapLevel::Cell& MipmapLevel::cell(const ivec2& index) const
{
  return cells[index.y * layerSize().x + index.x];
}

const MipmapLevel::Cell::Value* MipmapLevel::cellArrayBeginningAt(const ivec2& index) const
{
  return &cell(index).value;
}


const ivec2& MipmapLevel::layerSize() const
{
  return this->_layerSize;
}

void MipmapLevel::markAsFull(Region region)
{
  if(!region.clampTo(layerSize()))
    return;
  if(region.width()==0 || region.height()==0)
    return;

  int lineWidth = region.end.x - region.begin.x;
  int numberLines = region.end.y-region.begin.y;
  int lineStride = layerSize().x;
  Cell::Value* firstLine = cellArrayBeginningAt(region.begin);

  executeMultithreaded(0,
                       numberLines,
                       [=](int line, int count){
    markLinesAsFull(count, lineWidth, lineStride, firstLine+line*lineStride);
  });
}

bool MipmapLevel::isValueEverywhere(Cell cellValue, Region region) const
{
  if(!region.clampTo(layerSize()))
    return false;
  if(region.width()==0 || region.height()==0)
    return false;

  Cell::Value value = cellValue.value;
  int lineWidth = region.end.x-region.begin.x;
  int count = region.end.y-region.begin.y;

  const Cell::Value* firstLine = cellArrayBeginningAt(region.begin);

  int lineStride = this->layerSize().x;

  return isValueEverywhereInLines(value, count, lineWidth, lineStride, firstLine);
}

bool MipmapLevel::isRegionWithPossibleSpace(const Region& region) const
{
  Region r = region;
  if(!r.clampTo(layerSize()))
    return false;
  if(r.width()==0 || r.height()==0)
    return false;

  if(r.width() != region.width())
    return false;
  if(r.height() != region.height())
    return false;

  int lineWidth = r.width();
  int count = r.height();

  const Cell::Value* firstLine = cellArrayBeginningAt(r.begin);

  int lineStride = this->layerSize().x;

  if(!mayLineHaveSpace(lineWidth, firstLine))
    return false;
  if(!areLinesOfRegionWithSpace(count-2, lineWidth, lineStride, firstLine + lineStride))
    return false;
  return mayLineHaveSpace(lineWidth, firstLine + lineStride * (count-1));
}

void MipmapLevel::updateRegion(const MipmapLevel& base, Region region)
{
  if(!region.clampTo(layerSize()))
    return;
  if(region.width()==0 || region.height()==0)
    return;

  if(base.layerSize() == this->layerSize()*ivec2(2, 1))
  {
    updateRegionXCompressed(base, region);
  }else if(base.layerSize() == this->layerSize()*ivec2(1, 2))
  {
    updateRegionYCompressed(base, region);
  }else
  {
    throw std::logic_error("Calling updateRegion with a base, which has not the double width or height");
  }
}


inline bool mayCellHaveSpace(MipmapLevel::Cell::Value cellValue)
{
  return cellValue != MipmapLevel::Cell::FULL;
}

void MipmapLevel::updateRegionXCompressed(const MipmapLevel& base, const Region& region)
{
  int lineWidth = region.width();
  int count = region.height();

  int targetLineStride = this->layerSize().x;
  int sourceLineStride = base.layerSize().x;

  Cell::Value* firstTargetLine = this->cellArrayBeginningAt(region.begin);
  const Cell::Value* firstBaseLine = base.cellArrayBeginningAt(region.begin * ivec2(2, 1));

  executeMultithreaded(0,
                       count,
                       [=](int line, int count){
    updateLinesXCompressed(count,
                           lineWidth,
                           targetLineStride,
                           firstTargetLine + line*targetLineStride,
                           sourceLineStride,
                           firstBaseLine + line*sourceLineStride);
  });
}

void MipmapLevel::updateRegionYCompressed(const MipmapLevel& base, const Region& region)
{
  int lineWidth = region.width();
  int count = region.height();

  int targetLineStride = this->layerSize().x;
  int sourceLineStride = base.layerSize().x;

  Cell::Value* firstTargetLine = this->cellArrayBeginningAt(region.begin);
  const Cell::Value* firstBaseLine = base.cellArrayBeginningAt(region.begin * ivec2(1, 2));

  executeMultithreaded(0,
                       count,
                       [=](int line, int count){
    updateLinesYCompressed(count,
                           lineWidth,
                           targetLineStride,
                           firstTargetLine+line*targetLineStride,
                           sourceLineStride, firstBaseLine+line*sourceLineStride*2);
  });
}

void MipmapLevel::updateLinesXCompressed(int numberLines, int lineWidth, int targetLineStride, Cell::Value* firstTargetLine, int sourceLineStride, const Cell::Value* firstSourceLine)
{
  for(int i=0; i<numberLines; ++i)
  {
    updateLineXCompressed(lineWidth, firstTargetLine, firstSourceLine);
    firstTargetLine += targetLineStride;
    firstSourceLine += sourceLineStride;
  }
}

void MipmapLevel::updateLinesYCompressed(int numberLines, int lineWidth, int targetLineStride, Cell::Value* firstTargetLine, int sourceLineStride, const Cell::Value* firstSourceLine)
{
  for(int i=0; i<numberLines; ++i)
  {
    updateLineYCompressed(lineWidth, firstTargetLine, firstSourceLine, firstSourceLine+sourceLineStride);
    firstTargetLine += targetLineStride;
    firstSourceLine += sourceLineStride*2;
  }
}

inline MipmapLevel::Cell::Value updatedValue(MipmapLevel::Cell::Value a, MipmapLevel::Cell::Value b)
{
  typedef MipmapLevel::Cell Cell;

  if(a==Cell::EMPTY && b==Cell::EMPTY)
    return Cell::EMPTY;
  else if(a==Cell::FULL && b==Cell::FULL)
    return Cell::FULL;
  else
    return Cell::PARTIALLY_USED;
}

void MipmapLevel::updateLineXCompressed(int lineWidth, Cell::Value* firstTargetLine, const Cell::Value* firstSourceLine)
{
  for(int i=0, j=0; i<lineWidth; ++i, j+=2)
  {
    firstTargetLine[i] = updatedValue(firstSourceLine[j], firstSourceLine[j+1]);
  }
}

void MipmapLevel::updateLineYCompressed(int lineWidth, Cell::Value* firstTargetLine, const Cell::Value* firstSourceLine, const Cell::Value* secondSourceLine)
{
  for(int i=0; i<lineWidth; ++i)
  {
    firstTargetLine[i] = updatedValue(firstSourceLine[i], secondSourceLine[i]);
  }
}

void MipmapLevel::markLinesAsFull(int count, int lineWidth, int stride, Cell::Value* firstLine)
{
  for(int i=0; i<count; ++i)
  {
    markLineAsFull(lineWidth, firstLine);
    firstLine += stride;
  }
}


void MipmapLevel::markLineAsFull(int count, Cell::Value* cells)
{
  for(int i=0; i<count; ++i)
  {
    cells[i] = Cell::FULL;
  }
}


bool MipmapLevel::areLinesOfRegionWithSpace(int count, int lineWidth, int lineStride, const Cell::Value* line)
{
  for(int i=0; i<count; ++i)
  {
    if(!isLineOfRegionWithSpace(lineWidth, line))
      return false;
    line += lineStride;
  }

  return true;
}

bool MipmapLevel::isLineOfRegionWithSpace(int count, const Cell::Value* cells)
{
  if(!mayCellHaveSpace(cells[0]))
    return false;
  if(!isValueEverywhereInLine(Cell::EMPTY, count-2, cells+1))
    return false;
  if(!mayCellHaveSpace(cells[count-1]))
    return false;

  return true;
}


bool MipmapLevel::mayLineHaveSpace(int count, const Cell::Value* cells)
{
  for(int i=0; i<count; ++i)
  {
    if(!mayCellHaveSpace(cells[i]))
      return false;
  }

  return true;
}


bool MipmapLevel::isValueEverywhereInLines(Cell::Value cellValue, int count, int lineWidth, int lineStride, const Cell::Value* line)
{
  for(int i=0; i<count; ++i)
  {
    if(!isValueEverywhereInLine(cellValue, lineWidth, line))
      return false;
    line += lineStride;
  }

  return true;
}

bool MipmapLevel::isValueEverywhereInLine(Cell::Value cellValue, int count, const Cell::Value* line)
{
  for(int i=0; i<count; ++i)
  {
    if(line[i] != cellValue)
      return false;
  }

  return true;
}


MipmapTable::MipmapTable(const ivec2& textureSize)
  : textureSize(textureSize),
    logTextureSize(bin_log_floor(textureSize)),
    tableSize(logTextureSize+1)
{
  assert(textureSize.x > 0);
  assert(textureSize.y > 0);
  assert(isPowerOfTwo(textureSize.x));
  assert(isPowerOfTwo(textureSize.y));

  levels.resize(tableSize.x * tableSize.y);

  auto initLargest = [textureSize](MipmapLevel& level) {
    level.init(textureSize);
  };
  auto compressByX = [](MipmapLevel& level, MipmapLevel& base) {
    level.initCompressedX(base);
  };
  auto compressByY = [](MipmapLevel& level, MipmapLevel& base) {
    level.initCompressedY(base);
  };

  forEachLevelDescendingSize(initLargest, compressByX, compressByY);
}

int MipmapTable::arrayIndexFromTableIndex(const ivec2& textureIndex) const
{
  return textureIndex.y * tableSize.x + textureIndex.x;
}

MipmapLevel& MipmapTable::levelForIndex(const ivec2& textureIndex)
{
  return levels[arrayIndexFromTableIndex(textureIndex)];
}

const MipmapLevel& MipmapTable::levelForIndex(const ivec2& textureIndex) const
{
  return levels[arrayIndexFromTableIndex(textureIndex)];
}

void MipmapTable::forEachLevelDescendingSize(const std::function<void(MipmapLevel& level)>& forLargestLevel,
                                             const std::function<void(MipmapLevel& level, MipmapLevel& base)>& forCompressedX,
                                             const std::function<void(MipmapLevel& level, MipmapLevel& base)>& forCompressedY)
{
  forLargestLevel(levels[levels.size()-1]);

  for(int y = logTextureSize.y-1; y>=0; y--)
  {
    MipmapLevel& level = levelForIndex(ivec2(logTextureSize.x, y));
    MipmapLevel& base = levelForIndex(ivec2(logTextureSize.x, y+1));

    forCompressedY(level, base);
  }

  for(int y = logTextureSize.y; y>=0; y--)
  {
    for(int x = logTextureSize.x-1; x>=0; x--)
    {
      MipmapLevel& level = levelForIndex(ivec2(x, y));
      MipmapLevel& base = levelForIndex(ivec2(x+1, y));

      forCompressedX(level, base);
    }
  }
}

MipmapTable::Path MipmapTable::calculatePathForRegionSize(const ivec2& regionSize) const
{
  MipmapTable::Path path;
  path.reserve(max(tableSize.x, tableSize.y));

  ivec2 index = tableSize;
  Region region(ivec2(0), regionSize);

  do
  {
    ivec2 prevIndex = index;
    index = max(ivec2(0), index-1);

    path.prepend({region, index});

    region.compress(prevIndex-index);

  }while(index != ivec2(0));

  return path;
}


Optional<Region> MipmapTable::searchRegion(const ivec2& size) const
{
  QVector<ivec2> shiftOrder = {ivec2(0, 0), ivec2(1, 0), ivec2(0, 1), ivec2(1, 1)};
  return searchRegion(calculatePathForRegionSize(size), 0, ivec2(0), shiftOrder);
}

void MipmapTable::markRegionAsUsed(const Region& region)
{
  auto updateLargest = [&region](MipmapLevel& level) {
    level.markAsFull(region);
  };
  auto compress = [](MipmapLevel& level, MipmapLevel& base) {
    level.updateRegion(base);
  };

  forEachLevelDescendingSize(updateLargest, compress, compress);
}


Optional<Region> MipmapTable::searchRegion(const Path& path, int i, const ivec2& offset, const QVector<ivec2>& shiftOrder) const
{
  const PathPoint& p = path[i];

  if(!levelForIndex(p.level).isRegionWithPossibleSpace(p.region + offset))
    return nothing;

  if(i+1 == path.size())
    return p.region + offset;

  for(const ivec2& shift : shiftOrder)
  {
    Optional<Region> freeRegion = searchRegion(path, i+1, offset*2 + shift, shiftOrder);
    if(freeRegion)
      return freeRegion;
  }

  return  nothing;
}


} // namespace Private

using namespace Private;


TextureAtlasSpaceManager::TextureAtlasSpaceManager(const ivec2& textureSize)
  : textureSize(max(ivec2(1),round_up_to_power_of_two(textureSize))),
    mipmapTable(this->textureSize)
{
}

Optional<ivec2> TextureAtlasSpaceManager::allocate(const ivec2& regionSize)
{
  Optional<Region> result;

  result = mipmapTable.searchRegion(regionSize);

  if(!result)
    return nothing;

  mipmapTable.markRegionAsUsed(*result);

  return result->begin;
}

bool TextureAtlasSpaceManager::allocateSpaceForMultipleItems(QVector<Item*> items, bool sort)
{
  bool everythingFitted = true;

  if(sort)
  {
    qStableSort(items.begin(), items.end(), Item::lessThan);
  }

  for(Item* i : items)
  {
    if(i->shouldBeSkipped())
      continue;

    Optional<ivec2> position = allocate(i->size());

    if(!position)
    {
      everythingFitted = false;
      i->reject();
      continue;
    }

    i->setPosition(*position);
  }

  return everythingFitted;
}

bool TextureAtlasSpaceManager::allocateSpaceForMultipleItems(const QVector<Item::Ptr>& items, bool sort)
{
  QVector<Item*> itemsAsPointer;
  itemsAsPointer.reserve(items.size());

  for(const Item::Ptr& i : items)
    itemsAsPointer.append(i.get());

  return allocateSpaceForMultipleItems(itemsAsPointer, sort);
}

bool TextureAtlasSpaceManager::Item::lessThan(Item* a, Item* b)
{
  const ivec2 aSize = a->size();
  const ivec2 bSize = b->size();

  return compareSize(aSize, bSize) > 0;
}

int TextureAtlasSpaceManager::Item::compareSize(const ivec2& aSize, const ivec2& bSize)
{
  bool aIsHuge = max(aSize.x, aSize.y) > 48;
  bool bIsHuge = max(bSize.x, bSize.y) > 48;

  return compare(aIsHuge, bIsHuge,
                 aSize.x, bSize.x,
                 aSize.y, bSize.y);
}


} // namespace ProceduralAssets
} // namespace Framework
