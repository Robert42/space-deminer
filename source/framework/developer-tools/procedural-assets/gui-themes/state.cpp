#include "state.h"

#include <base/io/log.h>

namespace Framework {
namespace ProceduralAssets {
namespace GuiThemes {


Optional<String> State::singleFlagToString() const
{
  switch(this->value)
  {
  case NONE:
    return String("NONE");
  case NORMAL:
    return String("NORMAL");
  case INSENSITIVE:
    return String("INSENSITIVE");
  case HOVER:
    return String("HOVER");
  case FOCUSED:
    return String("FOCUSED");
  case PRESSED:
    return String("PRESSED");
  default:
    return nothing;
  }
}

Optional<String> State::singleFlagToString_Button() const
{
  switch(this->value)
  {
  case BUTTON_OUTSIDE_WIDGET:
    return String("BUTTON_OUTSIDE_WIDGET");
  default:
    return this->singleFlagToString();
  }
}

Optional<String> State::singleFlagToString_ToggleButton() const
{
  switch(this->value)
  {
  case TOGGLEBUTTON_ACTIVE:
    return String("TOGGLEBUTTON_ACTIVE");
  default:
    return this->singleFlagToString_Button();
  }
}

Optional<String> State::singleFlagToString_CheckButton() const
{
  switch(this->value)
  {
  case CHECKBUTTON_ACTIVE:
    return String("CHECKBUTTON_ACTIVE");
  case CHECKBUTTON_UNDEFINED:
    return String("CHECKBUTTON_UNDEFINED");
  default:
    return this->singleFlagToString_Button();
  }
}

Optional<String> State::singleFlagToString_TextField() const
{
  switch(this->value)
  {
  case TEXTBOX_READONLY:
    return String("TEXTEDIT_READONLY");
  case TEXTBOX_SELECTION:
    return String("TEXTEDIT_SELECTION");
  case TEXTBOX_FOCUSED_SELECTION:
    return String("TEXTBOX_FOCUSED_SELECTION");
  default:
    return this->singleFlagToString();
  }
}

Optional<String> State::singleFlagToString_WindowFrame() const
{
  switch(this->value)
  {
  case WINDOWFRAME_WITH_FRAME:
    return String("WINDOWFRAME_WITH_FRAME");
  case WINDOWFRAME_WITH_TITLEBAR:
    return String("WINDOWFRAME_WITH_TITLEBAR");
  default:
    return this->singleFlagToString();
  }
}

StateMask::StateMask(State state)
  : StateMask(state.value)
{
}

StateMask::StateMask(State::Value state)
    // if just the state is given, the default is to use exactly the same input state plus the set widget bits must match
  : StateMask(state, state|State::MASK_INPUT_STATE)
{
}

StateMask::StateMask(State::Value state, State::Value mask)
  : state(state),
    mask(mask)
{
}

StateMask::StateMask(State state, State mask)
  : StateMask(state.value, mask.value)
{
}

StateMask::StateMask()
  : StateMask(State::Value(0), State::Value(0))
{
}

StateMask StateMask::pressedMask()
{
  return StateMask(State::PRESSED, State::PRESSED|State::BUTTON_OUTSIDE_WIDGET);
}

bool StateMask::matchesState(State state) const
{
  return (state & this->mask) == (this->state & this->mask);
}

int StateMask::numberMatchingBits(State state) const
{
  if(!this->matchesState(state))
    return 0;

  return numberOfSetBits<State::underlying_type>(this->state & state.value);
}

bool StateMask::operator==(const StateMask& other) const
{
  return this->mask==other.mask && this->state==other.state;
}

bool StateMask::operator!=(const StateMask& other) const
{
  return *this == other;
}


int qHash(const StateMask& stateMask)
{
  return mixQHash(qHash(State(stateMask.state)), qHash(State(stateMask.mask)));
}


} // namespace GuiThemes
} // namespace ProceduralAssets
} // namespace Framework
