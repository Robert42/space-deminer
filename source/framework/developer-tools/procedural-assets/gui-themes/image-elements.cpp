#include "image-elements.h"

#include <base/color-tools.h>

namespace Framework {
namespace ProceduralAssets {
namespace GuiThemes {
namespace ImageElements {


Element::Element()
  : _parent(nullptr),
    _position(0),
    _size(0),
    _sizeRequest(0)
{
}

Element::~Element()
{
}

const vec2& Element::position() const
{
  return _position;
}

const vec2& Element::size() const
{
  return _size;
}

const vec2& Element::sizeRequest() const
{
  return _sizeRequest;
}

const Rectangle<vec2> Element::allocation() const
{
  return Rectangle<vec2>(position(), position()+size());
}

void Element::setRootSize(vec2 size)
{
  assert(!this->_parent);

  updateSizeRequest();

  if(size.x <= 0 && size.y <= 0)
  {
    size = this->sizeRequest();
  }else if(size.x <= 0)
  {
    if(this->sizeRequest().y != 0)
      size.x = size.y * this->sizeRequest().x / this->sizeRequest().y;
    else
      size.x = size.y;
  }else if(size.y <= 0)
  {
    if(this->sizeRequest().y != 0)
      size.y = size.x * this->sizeRequest().y / this->sizeRequest().x;
    else
      size.y = size.x;
  }

  _setAllocation(vec2(0), size);
}

void Element::updateSizeRequest()
{
  this->_sizeRequest = max(vec2(0.f), calcSizeRequest());
}

Optional<vec2> Element::calcHotspot()
{
  return nothing;
}

QString Element::exportToSvg(State state, const ivec2& size)
{
  QBuffer buffer;
  QSvgGenerator_Workaround svgGenerator;
  svgGenerator.setOutputDevice(&buffer);
  svgGenerator.setViewBox(QRectF(toQPointF(vec2(0.f)), toQSize(size)));
  svgGenerator.metric(QPaintDevice::PdmDevicePixelRatio);

  QPainter painter;
  painter.begin(&svgGenerator);
  this->draw(state, painter);
  painter.end();

  QByteArray byteArray = buffer.buffer();
  QTextCodec* utf8Codec = QTextCodec::codecForUtfText(byteArray, QTextCodec::codecForName("UTF-8"));
  QString svgString = utf8Codec->toUnicode(byteArray);

  //  Make sure the text is using the utf-8 text-codec
  assert(svgString.startsWith(R"(<?xml version="1.0" encoding="UTF-8")", Qt::CaseInsensitive));

  // cleaning the created svg code to increase the probability of having two exatly same svg files for comparison by ...
  // ... 1.) Remove empty groups ...
  QRegularExpression regex("<g(( |\\n)+[^=]+=\"[^\"]+\")*( |\\n)*>( |\\n)*</g>");
  regex.setPatternOptions(QRegularExpression::MultilineOption);
  svgString.replace(regex, QString());

  // ... 2.) .... remove unnecessary double newlines
  // as long as there were newlines following each other, remove them
  int length = svgString.length();
  while(svgString.replace("\n\n", "\n").length() != length) // no following newlines were found, there's no need to remove them
    length = svgString.length();

  return svgString;
}

void Element::_setAllocation(const vec2& position, const vec2& size)
{
  this->_position = position;
  this->_size = max(sizeRequest(), size);

  onAllocationChanged();
}


// ====

NullElement::Ptr NullElement::create()
{
  return Ptr(new NullElement);
}

// ====

ElementContainer::~ElementContainer()
{
  for(const Element::Ptr& c : children())
  {
    c->_parent = nullptr;
  }
}

void ElementContainer::_addChild(const Element::Ptr& element, bool prepend)
{
  ElementContainer* c = this;
  while(c)
  {
    assert(c!=element.get());
    c = c->_parent;
  }

  assert(element->_parent == nullptr);

  element->_parent = this;

  if(prepend)
    _children.prepend(element);
  else
    _children.append(element);
}

void ElementContainer::_removeChild(const Element::Ptr& element)
{
  assert(element->_parent == this);
  this->_children.remove(this->_children.indexOf(element));
  element->_parent = nullptr;
}

void ElementContainer::setChildAllocation(const Element::Ptr& child, const vec2& position, const vec2& size)
{
  assert(child->_parent == this);

  child->_setAllocation(position,
                        max(size,
                            child->sizeRequest()));
}

void ElementContainer::setChildAllocation(const Element::Ptr& child, const Rectangle<vec2>& allocation)
{
  setChildAllocation(child, allocation.min(), allocation.size());
}

void ElementContainer::updateSizeRequest()
{
  for(const Element::Ptr& c : children())
  {
    c->updateSizeRequest();
  }

  Element::updateSizeRequest();
}

Optional<vec2> ElementContainer::calcHotspot()
{
  for(const Element::Ptr& c : children())
  {
    Optional<vec2> optionalHotspot = c->calcHotspot();

    if(optionalHotspot)
    {
      vec2 hotspot = *optionalHotspot;

      return hotspot + c->position();
    }
  }

  return Element::calcHotspot();
}

void ElementContainer::draw(State state, QPainter& painter)
{
  for(const Element::Ptr& c : children())
  {
    painter.save();
    painter.translate(c->position().x, c->position().y);
    c->draw(state, painter);
    painter.restore();
  }
}

const QVector<Element::Ptr>& ElementContainer::children()
{
  return _children;
}

// ====


BaseStack::BaseStack()
{
}

void BaseStack::onAllocationChanged()
{
  Rectangle<vec2> allocation(vec2(0), this->size());
  for(const Element::Ptr& child : children())
    setChildAllocation(child, allocation);
}

vec2 BaseStack::calcSizeRequest()
{
  vec2 sizeRequest(0);
  for(const Element::Ptr& child : children())
    sizeRequest = max(sizeRequest, child->sizeRequest());
  return sizeRequest;
}

// ====

Stack::Ptr Stack::create()
{
  return Ptr(new Stack);
}

Stack::Ptr Stack::create(const Element::Ptr& child)
{
  Ptr stack = Stack::create();

  stack->push(child);

  return stack;
}

void Stack::push(const Element::Ptr& child, bool prepend)
{
  _addChild(child, prepend);
}

// ====


SwitchForState::Ptr SwitchForState::create()
{
  return Ptr(new SwitchForState);
}

void SwitchForState::insert(const StateMask& stateMask, const Element::Ptr& child)
{
  push(StateFilter::create(stateMask, child));
}

// ====

Proxy::Proxy()
{
}

void Proxy::onAllocationChanged()
{
  setChildAllocation(Rectangle<vec2>(vec2(0),
                                     size()));
}

vec2 Proxy::calcSizeRequest()
{
  return child()->sizeRequest();
}

// ====

StateFilter::StateFilter(const StateMask& mask, const Element::Ptr& child)
  : mask(mask)
{
  setChild(child);
}

StateFilter::Ptr StateFilter::create(const StateMask& mask, const Element::Ptr& child)
{
  return Ptr(new StateFilter(mask, child));
}

void StateFilter::draw(State state, QPainter &painter)
{
  if(mask.matchesState(state))
    Proxy::draw(state, painter);
}

// ====

VBox::VBox() : spacing(0.f)
{
}

VBox::Ptr VBox::create()
{
  return Ptr(new VBox);
}

VBox::Ptr VBox::create(float spacing, const std::initializer_list<Element::Ptr>& children)
{
  Ptr b(new VBox);

  b->spacing = spacing;
  for(const Element::Ptr& child : children)
    b->append(child);

  return b;
}

void VBox::append(const Element::Ptr& element, real priority)
{
  _addChild(element);
  priorities.insert(element.get(), priority);
}

void VBox::prepend(const Element::Ptr& element, real priority)
{
  _addChild(element, true);
  priorities.insert(element.get(), priority);
}

void VBox::onAllocationChanged()
{
  real totalPriority = 0.f;
  for(real p : priorities.values())
    totalPriority += p;

  const real avialableSpace = this->size().y - sizeRequest().y;

  real cursor = 0.f;
  for(const Element::Ptr& child : children())
  {
    real height = child->sizeRequest().y + avialableSpace * priorities[child.get()] / totalPriority;

    setChildAllocation(child,
                       vec2(0.f, cursor),
                       vec2(this->size().x, height));

    cursor += height + spacing;
  }
}

vec2 VBox::calcSizeRequest()
{
  vec2 sizeRequest(0);

  for(const Element::Ptr& child : children())
  {
    sizeRequest.x = max(sizeRequest.x, child->sizeRequest().x);
    sizeRequest.y += child->sizeRequest().y;
  }

  return sizeRequest + vec2(0, (children().size()-1) * spacing);
}

// ====


Bin::Bin()
{
  removeChild();
}

void Bin::setChild(const Element::Ptr& child)
{
  if(_child)
  {
    _removeChild(_child);
  }
  _child = child;
  _addChild(child);
}

void Bin::removeChild()
{
  setChild(NullElement::create());
}

const Element::Ptr& Bin::child()
{
  return _child;
}

void Bin::setChildAllocation(const vec2& position, const vec2& size)
{
  ElementContainer::setChildAllocation(child(), position, size);
}

void Bin::setChildAllocation(const Rectangle<vec2>& allocation)
{
  ElementContainer::setChildAllocation(child(), allocation);
}

// ====

Alignment::Alignment()
  : align(0.5),
    expand(0.f)
{
}

Alignment::Ptr Alignment::create()
{
  return Ptr(new Alignment);
}

Alignment::Ptr Alignment::create(const Element::Ptr& child)
{
  Alignment::Ptr a = create();
  a->setChild(child);
  return a;
}

Alignment::Ptr Alignment::create(const vec2& align, const Element::Ptr& child)
{
  Alignment::Ptr a = create(child);
  a->align = align;
  return a;
}

Alignment::Ptr Alignment::create(const vec2& align, const vec2& expand, const Element::Ptr& child)
{
  Alignment::Ptr a = create(align, child);
  a->expand = expand;
  return a;
}

void Alignment::onAllocationChanged()
{
  Rectangle<vec2> target(vec2(0), this->size());
  setChildAllocation(target.relativeAllocation(child()->sizeRequest(),
                                               this->align,
                                               this->expand));
}

vec2 Alignment::calcSizeRequest()
{
  return child()->sizeRequest();
}

// ====


Aspect::Aspect(const vec2& aspectRatio, const vec2& alignment, bool expand)
  : aspectRatio(aspectRatio),
    alignment(alignment),
    expand(expand)
{
}

Aspect::Ptr Aspect::create(const vec2& aspectRatio, const Element::Ptr& child, const vec2& alignment, bool expand)
{
  Aspect::Ptr a(new Aspect(aspectRatio, alignment, expand));

  a->setChild(child);

  return a;
}

Aspect::Ptr Aspect::create(const Element::Ptr& child, bool expand)
{
  Aspect::Ptr a(new Aspect);

  a->setChildAndSetAspect(child, expand);

  return a;
}

void Aspect::setChildAndSetAspect(const Element::Ptr& child, bool expand)
{
  ivec2 childSize = this->child()->sizeRequest();

  this->aspectRatio = childSize;
  this->expand = expand;

  setChild(child);
}

void Aspect::onAllocationChanged()
{
  Rectangle<vec2> target(vec2(0), this->size());

  setChildAllocation(target.relativeAllocation(fitSizeKeepingAspectRatio(aspectRatio, this->size(), expand),
                                               alignment));
}

vec2 Aspect::calcSizeRequest()
{
  return fitSizeKeepingAspectRatio(aspectRatio, child()->sizeRequest(), true);
}


// ====


Padding::Padding(real n, real w, real s, real e)
  : n(n), w(w), s(s), e(e)
{
}

Padding::Padding(real padding)
  : Padding(padding, padding, padding, padding)
{
}

Padding::Ptr Padding::create(real n, real w, real s, real e)
{
  return Ptr(new Padding(n, w, s, e));
}

Padding::Ptr Padding::create(real padding)
{
  return Ptr(new Padding(padding));
}

Padding::Ptr Padding::create(real n, real w, real s, real e, const Element::Ptr& child)
{
  Padding::Ptr p = create(n, w, s, e);
  p->setChild(child);
  return p;
}

Padding::Ptr Padding::create(real padding, const Element::Ptr& child)
{
  Padding::Ptr p = create(padding);
  p->setChild(child);
  return p;
}

void Padding::onAllocationChanged()
{
  setChildAllocation(Rectangle<vec2>(vec2(w, n),
                                     size() - vec2(e, s)));
}

vec2 Padding::calcSizeRequest()
{
  return vec2(w+e, n+s) + child()->sizeRequest();
}


// ====



Translate::Translate(const Vec2Palette& translation)
  : translation(translation)
{
}

Translate::Ptr Translate::create(const Vec2Palette& translation)
{
  return Ptr(new Translate(translation));
}

Translate::Ptr Translate::create(const Vec2Palette& translation, const Element::Ptr& child)
{
  Ptr v = create(translation);

  v->setChild(child);

  return v;
}

void Translate::onAllocationChanged()
{
  setChildAllocation(vec2(0), this->size());
}

vec2 Translate::calcSizeRequest()
{
  return vec2(0);
}

void Translate::draw(State state, QPainter& painter)
{
  vec2 t = translation[state];

  painter.translate(toQPointF(t));

  Bin::draw(state, painter);
}


// ====



RotateDiscrete::RotateDiscrete(int rotationSteps)
  : rotationSteps(mod(rotationSteps, 4))
{
}

RotateDiscrete::Ptr RotateDiscrete::create(int rotationSteps)
{
  return Ptr(new RotateDiscrete(rotationSteps));
}

RotateDiscrete::Ptr RotateDiscrete::create(int rotationSteps, const Element::Ptr& child)
{
  Ptr rotate = create(rotationSteps);

  rotate->setChild(child);

  return rotate;
}

void RotateDiscrete::onAllocationChanged()
{
  vec2 size(this->size());

  if(rotationSteps%2==1)
    swap(size.x,
         size.y);

  setChildAllocation(vec2(0), size);
}

vec2 RotateDiscrete::calcSizeRequest()
{
  vec2 sizeRequest(child()->sizeRequest());

  if(rotationSteps%2==1)
    swap(sizeRequest.x,
         sizeRequest.y);

  return sizeRequest;
}

void RotateDiscrete::draw(State state, QPainter& painter)
{
  painter.save();
  painter.translate(toQPointF(this->size()*0.5f));
  painter.rotate(-rotationSteps * 90.f);
  painter.translate(toQPointF(-child()->size()*0.5f));
  ElementContainer::draw(state, painter);
  painter.restore();
}

SizeRequest::SizeRequest(const vec2& newSizeRequest)
  : newSizeRequest(newSizeRequest)
{
}

SizeRequest::Ptr SizeRequest::create(const vec2& newSizeRequest)
{
  return Ptr(new SizeRequest(newSizeRequest));
}

SizeRequest::Ptr SizeRequest::create(const vec2& newSizeRequest, const Element::Ptr& child)
{
  SizeRequest::Ptr s = SizeRequest::create(newSizeRequest);
  s->setChild(child);
  return s;
}

void SizeRequest::onAllocationChanged()
{
  setChildAllocation(vec2(0), size());
}

vec2 SizeRequest::calcSizeRequest()
{
  return max(child()->sizeRequest(), newSizeRequest);
}

Shape::Shape(const ColorPalette& palette, real thickness)
  : palette(palette),
    thickness(max(0.f, thickness)),
    penJoinStyle(Qt::MiterJoin)
{
}

void Shape::onAllocationChanged()
{
}

Shape::Ptr Shape::copyWithDifferentPalette(const ColorPalette& palette) const
{
  return copyWithDifferentThickness(this->thickness, palette);
}

Shape::Ptr Shape::copyWithDifferentThickness(real thickness, const ColorPalette& palette) const
{
  Ptr shape = this->copyShape();
  shape->thickness = thickness;
  shape->palette = palette;
  return shape;
}

void Shape::draw(State state, QPainter& p)
{
  QColor color = toQColor(palette[state]);

  if(thickness > 0.f)
  {
    QPen pen(color);
    pen.setWidthF(thickness);
    pen.setMiterLimit(5);
    pen.setJoinStyle(this->penJoinStyle);
    p.setPen(pen);
    p.setBrush(Qt::NoBrush);
  }else
  {
    QBrush brush(color);
    p.setPen(Qt::NoPen);
    p.setBrush(brush);
  }

  if(penOverride)
    p.setPen(penOverride.value()[state]);
  if(brushOverride)
    p.setBrush(brushOverride.value()[state]);

  drawShape(p);
}


ShapeWithStrokeOffset::ShapeWithStrokeOffset(real strokeOffset, const ColorPalette& palette, real thickness)
  : Shape(palette,
          thickness),
    strokeOffset(strokeOffset)
{
}

Shape::Ptr ShapeWithStrokeOffset::copyShape() const
{
  return copy();
}

ShapeWithStrokeOffset::Ptr ShapeWithStrokeOffset::copyWithAdditionalStrokeOffset(real offset) const
{
  ShapeWithStrokeOffset::Ptr c = this->copy();

  c->strokeOffset += offset;

  return c;
}

ShapeWithStrokeOffset::Ptr ShapeWithStrokeOffset::copyWithAdditionalStrokeOffset(real offset, const ColorPalette& palette) const
{
  ShapeWithStrokeOffset::Ptr c = this->copyWithAdditionalStrokeOffset(offset);
  c->palette = palette;
  return c;
}

ShapeWithStrokeOffset::Ptr ShapeWithStrokeOffset::copyWithAdditionalStrokeOffset(real offset,
                                                                                 const BrushPalette& brushOverride) const
{
  ShapeWithStrokeOffset::Ptr c = this->copyWithAdditionalStrokeOffset(offset);
  c->brushOverride = brushOverride;
  return c;
}

ShapeWithStrokeOffset::Ptr ShapeWithStrokeOffset::copyWithAdditionalStrokeOffset(real offset,
                                                                                 const PenPalette& penOverride) const
{
  ShapeWithStrokeOffset::Ptr c = this->copyWithAdditionalStrokeOffset(offset);
  c->penOverride = penOverride;
  return c;
}

ShapeWithStrokeOffset::Ptr ShapeWithStrokeOffset::copyWithAdditionalStrokeOffset(real offset,
                                                                                 const BrushPalette& brushOverride,
                                                                                 const PenPalette& penOverride) const
{
  ShapeWithStrokeOffset::Ptr c = this->copyWithAdditionalStrokeOffset(offset);
  c->brushOverride = brushOverride;
  c->penOverride = penOverride;
  return c;
}

ShapeWithStrokeOffset::Ptr ShapeWithStrokeOffset::copyWithAdditionalStrokeOffset(real offset,
                                                                                 const ColorPalette& palette,
                                                                                 const BrushPalette& brushOverride) const
{
  ShapeWithStrokeOffset::Ptr c = this->copyWithAdditionalStrokeOffset(offset, palette);
  c->brushOverride = brushOverride;
  return c;
}

ShapeWithStrokeOffset::Ptr ShapeWithStrokeOffset::copyWithAdditionalStrokeOffset(real offset,
                                                                                 const ColorPalette& palette,
                                                                                 const PenPalette& penOverride) const
{
  ShapeWithStrokeOffset::Ptr c = this->copyWithAdditionalStrokeOffset(offset, palette);
  c->penOverride = penOverride;
  return c;
}

ShapeWithStrokeOffset::Ptr ShapeWithStrokeOffset::copyWithAdditionalStrokeOffset(real offset,
                                                                                 const ColorPalette& palette,
                                                                                 const BrushPalette& brushOverride,
                                                                                 const PenPalette& penOverride) const
{
  ShapeWithStrokeOffset::Ptr c = this->copyWithAdditionalStrokeOffset(offset, palette);
  c->brushOverride = brushOverride;
  c->penOverride = penOverride;
  return c;
}


TriangleShape::TriangleShape(const ColorPalette& palette)
  : Shape(palette, 0.f)
{
}

Shape::Ptr TriangleShape::copyShape() const
{
  return Ptr(new TriangleShape(this->palette));
}

Element::Ptr TriangleShape::createWithFixedAspect(int degree, const ColorPalette& palette, real topAngle, const vec2& alignment)
{
  vec2 aspect = vec2(2.f * tan(topAngle*0.5f), 1.f);

  if(mod(degree, 4) % 2 == 1)
    swap(aspect.x, aspect.y);

  return ImageElements::Aspect::create(aspect,
                                       ImageElements::RotateDiscrete::create(degree,
                                                                             Ptr(new TriangleShape(palette))),
                                       alignment);
}

void TriangleShape::drawShape(QPainter& p)
{
  QPointF points[3];

  real y1=0.f, y2=size().y;

  points[0] = toQPointF(vec2(size().x*0.5f, y1));
  points[1] = toQPointF(vec2(0.f, y2));
  points[2] = toQPointF(vec2(size().x, y2));

  p.drawPolygon(points, 3);
}

vec2 TriangleShape::calcSizeRequest()
{
  return vec2(1);
}



PathShape::PathShape(const QPainterPath& path, const ColorPalette& palette, real thickness, bool useToDetermineHotspot)
  : Shape(palette, thickness),
    path(path),
    useToDetermineHotspot(useToDetermineHotspot)
{
}

PathShape::Ptr PathShape::create(const QPainterPath& path, const ColorPalette& palette, real thickness, bool useToDetermineHotspot)
{
  return Ptr(new PathShape(path, palette, thickness, useToDetermineHotspot));
}

Shape::Ptr PathShape::copyShape() const
{
  return Ptr(new PathShape(this->path,
                           this->palette,
                           this->thickness,
                           this->useToDetermineHotspot));
}

vec2 PathShape::calcSizeRequest()
{
  return thickness + vec2(path.boundingRect().width(), path.boundingRect().height());
}

void PathShape::drawShape(QPainter& p)
{
  p.translate(offset());
  p.drawPath(this->path);
}

Optional<vec2> PathShape::calcHotspot()
{
  if(!useToDetermineHotspot)
    return Element::calcHotspot();
  return fromQPointF(offset());
}

QPointF PathShape::offset() const
{
  return -this->path.boundingRect().topLeft() + QPointF(thickness, thickness)*0.5f;
}



RectangleShape::RectangleShape(real strokeOffset,
                               const ColorPalette& palette,
                               real thickness,
                               real rounding)
  : ShapeWithStrokeOffset(strokeOffset,
                          palette,
                          thickness),
    rounding(rounding)
{
}

void RectangleShape::drawShape(QPainter& p)
{
  real rounding = this->rounding;
  Rectangle<vec2> shape(vec2(0), size());

  real extension = strokeOffset - thickness*0.5f;

  shape = shape.expandedRect(extension);
  rounding += extension;

  QRectF qRect = toQRectF(shape);

  if(rounding > 0.f)
    p.drawRoundedRect(qRect, rounding, rounding);
  else
    p.drawRect(qRect);
}

vec2 RectangleShape::calcSizeRequest()
{
  return vec2(max(0.f, rounding + strokeOffset) * 2.f + thickness);
}

RectangleShape::Ptr RectangleShape::create(real strokeOffset,
                                           const ColorPalette& palette,
                                           real thickness,
                                           real rounding)
{
  return Ptr(new RectangleShape(strokeOffset, palette, thickness, rounding));
}

ShapeWithStrokeOffset::Ptr RectangleShape::copy() const
{
  return create(strokeOffset,
                palette,
                thickness,
                rounding);
}

RectangleShapeWithDifferentRadii::RectangleShapeWithDifferentRadii(real strokeOffset,
                                                                   const ColorPalette& palette,
                                                                   real thickness,
                                                                   real roundingNW,
                                                                   real roundingSW,
                                                                   real roundingSE,
                                                                   real roundingNE)
  : ShapeWithStrokeOffset(strokeOffset,
                          palette,
                          thickness),
    roundingNW(roundingNW),
    roundingSW(roundingSW),
    roundingSE(roundingSE),
    roundingNE(roundingNE)
{
}

class RoundedRect
{
public:

  struct Corner
  {
    vec2 outerEdge;
    vec2 center;
    real radius;

    vec2 startDir, endDir;

    Corner()
      : Corner(vec2(0), 0, vec2(0), vec2(1))
    {
    }

    Corner(const vec2& outerEdge, real radius, const vec2& startPointDir, const vec2& endPointDir)
      : outerEdge(outerEdge),
        center(outerEdge - (startPointDir+endPointDir)*radius),
        radius(radius),
        startDir(startPointDir),
        endDir(endPointDir)
    {
    }

    inline real c() const
    {
      // Source: http://en.wikipedia.org/wiki/Composite_B%C3%A9zier_curve
      return 4.f * (sqrt_two - 1.f) / 3.f;
    }

    QPointF startPoint() const
    {
      return toQPointF(center + startDir*radius);
    }

    QPointF tangent1Point() const
    {
      return toQPointF(center + (startDir + endDir*c())*radius);
    }

    QPointF tangent2Point() const
    {
      return toQPointF(center + (endDir + startDir*c())*radius);
    }

    QPointF endPoint() const
    {
      return toQPointF(center + endDir*radius);
    }

    void drawArc(QPainterPath& path, int i) const
    {
      if(i==0)
        path.moveTo(startPoint());
      else
        path.lineTo(startPoint());

      if(radius > 0.f)
      {
        path.cubicTo(tangent1Point(),
                     tangent2Point(),
                     endPoint());
      }
    }
  };

public:
  Rectangle<vec2> shape;
  real roundingNW;
  real roundingSW;
  real roundingSE;
  real roundingNE;

  Corner nw;
  Corner sw;
  Corner se;
  Corner ne;

public:

  RoundedRect(const Rectangle<vec2>& shape,
              real roundingNW,
              real roundingSW,
              real roundingSE,
              real roundingNE)
    : shape(shape),
      roundingNW(roundingNW),
      roundingSW(roundingSW),
      roundingSE(roundingSE),
      roundingNE(roundingNE)
  {
    const vec2 boundingSize = shape.size();
    const real maxRadius = min(boundingSize.x, boundingSize.y)*0.5f;

    roundingNW = max(0.f, min(roundingNW, maxRadius));
    roundingSW = max(0.f, min(roundingSW, maxRadius));
    roundingSE = max(0.f, min(roundingSE, maxRadius));
    roundingNE = max(0.f, min(roundingNE, maxRadius));

    const real wEdge = shape.min().x;
    const real nEdge = shape.min().y;
    const real sEdge = shape.max().y;
    const real eEdge = shape.max().x;

    nw = Corner(vec2(wEdge, nEdge), roundingNW, vec2( 0, -1), vec2(-1,  0));
    sw = Corner(vec2(wEdge, sEdge), roundingSW, vec2(-1,  0), vec2( 0,  1));
    se = Corner(vec2(eEdge, sEdge), roundingSE, vec2( 0,  1), vec2( 1,  0));
    ne = Corner(vec2(eEdge, nEdge), roundingNE, vec2( 1,  0), vec2( 0, -1));
  }

  QPainterPath asPath() const
  {
    QPainterPath path;

    nw.drawArc(path, 0);
    sw.drawArc(path, 1);
    se.drawArc(path, 2);
    ne.drawArc(path, 3);

    path.closeSubpath();

    return path;
  }
};

void RectangleShapeWithDifferentRadii::drawShape(QPainter& p)
{
  Rectangle<vec2> shape(vec2(0), size());

  real extension = strokeOffset - thickness*0.5f;

  shape = shape.expandedRect(extension);

  p.drawPath(RoundedRect(shape,
                         roundingNW + extension,
                         roundingSW + extension,
                         roundingSE + extension,
                         roundingNE + extension).asPath());
}

vec2 RectangleShapeWithDifferentRadii::calcSizeRequest()
{
  return max(vec2(0.f),
             max(vec2(roundingNW+roundingNE,
                      roundingNW+roundingSW),
                 vec2(roundingSW+roundingSE,
                      roundingNE+roundingSE))
             + strokeOffset*2.f)
      + thickness;
}

RectangleShapeWithDifferentRadii::Ptr RectangleShapeWithDifferentRadii::create(real strokeOffset,
                                                                               const ColorPalette& palette,
                                                                               real thickness,
                                                                               real roundingNW,
                                                                               real roundingSW,
                                                                               real roundingSE,
                                                                               real roundingNE)
{
  return Ptr(new RectangleShapeWithDifferentRadii(strokeOffset,
                                                  palette,
                                                  thickness,
                                                  roundingNW,
                                                  roundingSW,
                                                  roundingSE,
                                                  roundingNE));
}

RectangleShapeWithDifferentRadii::Ptr RectangleShapeWithDifferentRadii::create(real strokeOffset,
                                                                               const ColorPalette& palette,
                                                                               real thickness,
                                                                               real roundingN,
                                                                               real roundingS)
{
  return create(strokeOffset, palette, thickness, roundingN, roundingS, roundingS, roundingN);
}

ShapeWithStrokeOffset::Ptr RectangleShapeWithDifferentRadii::copy() const
{
  return create(strokeOffset,
                palette,
                thickness,
                roundingNW,
                roundingSW,
                roundingSE,
                roundingNE);
}

// ====

InverseRectangleShape::InverseRectangleShape(const ColorPalette& palette, real rounding)
  : Shape(palette, 0.f),
    rounding(rounding)
{
}

InverseRectangleShape::Ptr InverseRectangleShape::create(const ColorPalette& palette, real rounding)
{
  return Ptr(new InverseRectangleShape(palette, rounding));
}

Shape::Ptr InverseRectangleShape::copyShape() const
{
  return Ptr(new InverseRectangleShape(this->palette, this->rounding));
}

void InverseRectangleShape::drawShape(QPainter& p)
{
  Rectangle<vec2> shape(vec2(0), size());
  RoundedRect rect(shape,
                   rounding,
                   rounding,
                   rounding,
                   rounding);

  auto drawCorner = [&p](const RoundedRect::Corner& c) {
    QPainterPath path;

    c.drawArc(path, 0);
    path.lineTo(toQPointF(c.outerEdge));
    path.closeSubpath();

    p.drawPath(path);
  };

  drawCorner(rect.nw);
  drawCorner(rect.sw);
  drawCorner(rect.se);
  drawCorner(rect.ne);
}

vec2 InverseRectangleShape::calcSizeRequest()
{
  return vec2(this->rounding*2.f);
}



// ====

CircleShape::CircleShape(real strokeOffset, const ColorPalette& palette, real thickness)
  : ShapeWithStrokeOffset(strokeOffset, palette, thickness)
{
}

CircleShape::Ptr CircleShape::create(real strokeOffset, const ColorPalette& palette, real thickness)
{
  return Ptr(new CircleShape(strokeOffset, palette, thickness));
}

void CircleShape::drawShape(QPainter& p)
{
  vec2 center = size()*0.5f;
  real radius = min(center.x, center.y) + strokeOffset - thickness*0.5f;

  Rectangle<vec2> boundingRect(center-radius, center+radius);

  QRectF qRect = toQRectF(boundingRect);

  p.drawEllipse(qRect);
}

vec2 CircleShape::calcSizeRequest()
{
  return vec2(thickness*2.f);
}

ShapeWithStrokeOffset::Ptr CircleShape::copy() const
{
  return create(strokeOffset, palette, thickness);
}


ColorPalette calcShadowColor(int shadowWidth, int i, const ColorPalette& shadowColor)
{
  float x = (i-1) / float(shadowWidth);

  float alpha = sq(1.f-x);

  return shadowColor * vec4(1,1,1,alpha);
}

Stack::Ptr createShadow(real shadowWidth,
                        const std::function<Element::Ptr(int i)>& createShadowElement)
{
  ImageElements::Stack::Ptr stack = ImageElements::Stack::create();

  for(int i=ceil(shadowWidth); i>0; --i)
    stack->push(createShadowElement(i));

  return stack;
}

Stack::Ptr createWithShadowUsingThickness(const Shape::Ptr& original,
                                          real shadowWidth,
                                          const ColorPalette& shadowColor)
{
  auto decorate = [shadowWidth](const Element::Ptr& shape, int i) {
    Element::Ptr element = shape;
    if(i!=shadowWidth)
      element = Padding::create(shadowWidth-i, shape);
    return element;
  };

  auto createShadowLayer = [&original, &shadowColor, shadowWidth, &decorate](int i) {
    Shape::Ptr shape =  original->copyWithDifferentThickness(original->thickness + 2*i,
                                                             calcShadowColor(shadowWidth, i, shadowColor));
    return decorate(shape, i);
  };

  Stack::Ptr stack = createShadow(shadowWidth,
                                  createShadowLayer);
  stack->push(decorate(original, 0));

  return stack;
}

Stack::Ptr createWithShadowUsingOffset(const ShapeWithStrokeOffset::Ptr& original,
                                       real shadowWidth,
                                       const ColorPalette& shadowColor,
                                       bool adaptStrokeOffsetOfOriginal)
{
  if(adaptStrokeOffsetOfOriginal)
    original->strokeOffset -= shadowWidth;

  auto createShadowLayer = [&original, &shadowColor, shadowWidth](int i) {
    ShapeWithStrokeOffset::Ptr shape = original->copyWithAdditionalStrokeOffset(i,
                                                                                calcShadowColor(shadowWidth, i, shadowColor));
    shape->thickness = 1.f;
    return shape;
  };

  Stack::Ptr stack = createShadow(shadowWidth,
                                  createShadowLayer);
  stack->push(original);
  return stack;
}

Stack::Ptr createWitInnerhShadowUsingOffset(const ShapeWithStrokeOffset::Ptr& original,
                                            real shadowWidth,
                                            const ColorPalette& shadowColor,
                                            int offset)
{
  auto createShadowLayer = [&original, &shadowColor, shadowWidth,offset](int i) {
    ShapeWithStrokeOffset::Ptr shape = original->copyWithAdditionalStrokeOffset(-i+2+offset,
                                                                                calcShadowColor(shadowWidth, i, shadowColor));
    shape->thickness = 1.f;
    return shape;
  };

  Stack::Ptr stack = createShadow(shadowWidth,
                                  createShadowLayer);
  stack->push(original, true);
  return stack;
}

} // namespace Elements
} // namespace GuiThemes
} // namespace ProceduralAssets
} // namespace Framework
