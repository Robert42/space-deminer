#ifndef FRAMEWORK_PROCEDURALASSETS_GUITHEMES_TEXTUREATLAS_H
#define FRAMEWORK_PROCEDURALASSETS_GUITHEMES_TEXTUREATLAS_H

#include "../texture-atlas-space-manager.h"

#include <base/io/directory.h>

namespace Framework {
namespace ProceduralAssets {
namespace GuiThemes {

class TextureAtlas final
{
public:
  typedef Rectangle<ivec2> Slice;
  typedef QMap<QString, Slice> Mapping;
  typedef QHash<QString, vec2> OffsetMapping;

public:
  String name;
  QImage texture;
  Mapping mapping;
  OffsetMapping offsetMapping;

  TextureAtlas();
  TextureAtlas(const String& name, const ivec2& size=ivec2(1));
  ~TextureAtlas();

  void registerImagesToCegui() const;

  void exportToCeguiFiles(const IO::Directory& directory) const;
  void exportCeguiImagesteFile(const IO::RegularFile& filename) const;

  void enforceInvariant();

private:
  void checkInvariant() const;
};

} // namespace GuiThemes
} // namespace ProceduralAssets
} // namespace Framework

#endif // FRAMEWORK_PROCEDURALASSETS_GUITHEMES_TEXTUREATLAS_H
