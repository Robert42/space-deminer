#ifndef FRAMEWORK_PROCEDURALASSETS_GUITHEMES_BASETHEME_H
#define FRAMEWORK_PROCEDURALASSETS_GUITHEMES_BASETHEME_H

#include "widget-looks.h"
#include "texture-atlas.h"

namespace Framework {
namespace ProceduralAssets {
namespace GuiThemes {


class BaseTheme
{
public:
  QVector<WidgetLooks::WidgetLook::Ptr> widgetLooks;
  String name;

public:
  BaseTheme(const String& name);
  virtual ~BaseTheme();

  bool drawTextureAtlas(QPainter& painter,
                        const Output<TextureAtlas::Mapping>& mapping,
                        const ivec2& size,
                        int padding=2);
  bool drawTextureAtlas(QPainter& painter,
                        const Output<TextureAtlas::Mapping>& mapping,
                        const Output<TextureAtlas::OffsetMapping>& offsetMapping,
                        const ivec2& size,
                        int padding=2);
  bool drawTextureAtlas(QPainter& painter,
                        const ivec2& size,
                        int padding=2);

  Optional<TextureAtlas> createTextureAtlas(const ivec2& size,
                                            int padding=2);
  void serializeToCeguiXml(CEGUI::XMLSerializer& xml);
  void serializeSchemeToCeguiXml(CEGUI::XMLSerializer& xml);
  void registerToCEGUI(const ivec2& size, int padding=2);

  void testExportingToSvg(const IO::RegularFile& file,
                          const ivec2& size,
                          const vec4& background=vec4(0));
  void testExportingCeguiTextureAtlas(const IO::Directory& directory,
                                      const ivec2& size);
};


} // namespace GuiThemes
} // namespace ProceduralAssets
} // namespace Framework

#endif // FRAMEWORK_PROCEDURALASSETS_GUITHEMES_BASETHEME_H
