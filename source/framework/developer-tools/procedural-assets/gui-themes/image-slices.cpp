#include "image-slices.h"
#include "widget-looks.h"

#include <CEGUI/falagard/ImagerySection.h>

namespace Framework {
namespace ProceduralAssets {
namespace GuiThemes {
namespace ImageSlices {


ImageSlice::ImageSlice()
{
  area = Area::createFullArea();
}

ImageSlice::~ImageSlice()
{
}



Aligned::Aligned(Horizontal horizontal, Vertical vertical)
  : horizontal(horizontal),
    vertical(vertical)
{
}

Aligned::Ptr Aligned::create(Horizontal horizontal, Vertical vertical)
{
  return Ptr(new Aligned(horizontal, vertical));
}

Aligned::Slices Aligned::slices(const Rect& whole, State)
{
  Slices slices;
  slices[""] = whole;
  return slices;
}

void Aligned::addImageToImagerySection(CEGUI::XMLSerializer& xml, const CEGUI::String& name, const ivec2&, State)
{
  CEGUI::String verticalAsString, horizontalAsString;

  switch(this->horizontal.value)
  {
  case Horizontal::LEFT:
    horizontalAsString = "LeftAligned";
    break;
  case Horizontal::RIGHT:
    horizontalAsString = "RightAligned";
    break;
  case Horizontal::STRECHED:
    horizontalAsString = "Stretched";
    break;
  case Horizontal::TILED:
    horizontalAsString = "Tiled";
    break;
  case Horizontal::CENTER:
  default:
    horizontalAsString = "CentreAligned";
    break;
  }

  switch(this->vertical.value)
  {
  case Vertical::TOP:
    verticalAsString = "TopAligned";
    break;
  case Vertical::BOTTOM:
    verticalAsString = "BottomAligned";
    break;
  case Vertical::STRECHED:
    verticalAsString = "Stretched";
    break;
  case Vertical::TILED:
    verticalAsString = "Tiled";
    break;
  case Vertical::CENTER:
  default:
    verticalAsString = "CentreAligned";
    break;
  }

  xml.openTag("ImageryComponent");
  area->serializeToCegui(xml);

  xml.openTag("Image");
  xml.attribute("name", name);
  xml.closeTag(); // Image

  xml.openTag("VertFormat"); // Necessary for streching the image vertically
  xml.attribute("type", verticalAsString);
  xml.closeTag(); // VertFormat

  xml.openTag("HorzFormat"); // Necessary for streching the image horizontally
  xml.attribute("type", horizontalAsString);
  xml.closeTag(); // VertFormat

  xml.closeTag(); // FrameComponent
}



Streched::Streched()
{
}

Streched::Ptr Streched::create()
{
  return Ptr(new Streched);
}

Streched::Slices Streched::slices(const Rect& whole, State)
{
  Slices slices;
  slices[""] = whole;
  return slices;
}

void Streched::addImageToImagerySection(CEGUI::XMLSerializer& xml, const CEGUI::String& name, const ivec2&, State)
{
  xml.openTag("ImageryComponent");
  area->serializeToCegui(xml);

  xml.openTag("Image");
  xml.attribute("name", name);
  xml.closeTag(); // Image

  xml.openTag("VertFormat"); // Necessary for streching the image vertically
  xml.attribute("type", "Stretched");
  xml.closeTag(); // VertFormat

  xml.openTag("HorzFormat"); // Necessary for streching the image horizontally
  xml.attribute("type", "Stretched");
  xml.closeTag(); // VertFormat

  xml.closeTag(); // FrameComponent
}



HorizontalFrame::HorizontalFrame(int leftWidth, int rightWidth)
  : leftWidth(leftWidth),
    rightWidth(rightWidth)
{
}

HorizontalFrame::Ptr HorizontalFrame::create(int leftWidth, int rightWidth)
{
  return Ptr(new HorizontalFrame(leftWidth, rightWidth));
}

HorizontalFrame::Ptr HorizontalFrame::create(int frameWidth)
{
  return create(frameWidth, frameWidth);
}

ImageSlice::Slices HorizontalFrame::slices(const Rect& whole, State)
{
  int width = whole.width();
  int height = whole.height();
  assert(width >= leftWidth+rightWidth);

  int centerWidth = width - leftWidth - rightWidth;

  Slices slices;

  if(leftWidth > 0)
    slices["left"] = whole.absoluteSubRect(ivec2(0), ivec2(leftWidth, height), true);
  if(centerWidth > 0)
    slices["center"] = whole.absoluteSubRect(ivec2(leftWidth, 0), ivec2(centerWidth, height), true);
  if(rightWidth > 0)
    slices["right"] = whole.absoluteSubRect(ivec2(-rightWidth, 0), ivec2(rightWidth, height), true);

  return slices;
}

void HorizontalFrame::addImageToImagerySection(CEGUI::XMLSerializer& xml, const CEGUI::String& name, const ivec2& size, State state)
{
  xml.openTag("FrameComponent");
  area->serializeToCegui(xml);

  for(const QString& sliceName : this->slices(Rect(ivec2(0), size), state).keys())
  {
    CEGUI::String imageName = WidgetLooks::WidgetLook::composeName(name, String::fromQString(sliceName).toCeguiString());

    xml.openTag("Image");
    xml.attribute("component", convertNameToCegui(sliceName));
    xml.attribute("name", imageName);
    xml.closeTag(); // Image
  }

  xml.closeTag(); // FrameComponent
}

CEGUI::String HorizontalFrame::convertNameToCegui(const QString& name)
{
  if(name=="left")
    return "LeftEdge";
  else if(name=="right")
    return "RightEdge";
  else if(name=="center")
    return "Background";

  assert(false && "There should be no unkown image-slice name");
  return "";
}




VerticalFrame::VerticalFrame(int topWidth, int bottomWidth)
  : topWidth(topWidth),
    bottomWidth(bottomWidth)
{
}

VerticalFrame::Ptr VerticalFrame::create(int topWidth, int bottomWidth)
{
  return Ptr(new VerticalFrame(topWidth, bottomWidth));
}

VerticalFrame::Ptr VerticalFrame::create(int frameWidth)
{
  return create(frameWidth, frameWidth);
}

ImageSlice::Slices VerticalFrame::slices(const Rect& whole, State)
{
  int width = whole.width();
  int height = whole.height();
  assert(height >= topWidth+bottomWidth);

  int centerWidth = height - topWidth - bottomWidth;

  Slices slices;

  if(topWidth > 0)
    slices["top"] = whole.absoluteSubRect(ivec2(0), ivec2(width, topWidth), true);
  if(centerWidth > 0)
    slices["center"] = whole.absoluteSubRect(ivec2(0, topWidth), ivec2(width, centerWidth), true);
  if(bottomWidth > 0)
    slices["bottom"] = whole.absoluteSubRect(ivec2(0, -bottomWidth), ivec2(width, bottomWidth), true);

  return slices;
}

void VerticalFrame::addImageToImagerySection(CEGUI::XMLSerializer& xml, const CEGUI::String& name, const ivec2& size, State state)
{
  xml.openTag("FrameComponent");
  area->serializeToCegui(xml);

  for(const QString& sliceName : this->slices(Rect(ivec2(0), size), state).keys())
  {
    CEGUI::String imageName = WidgetLooks::WidgetLook::composeName(name, String::fromQString(sliceName).toCeguiString());

    xml.openTag("Image");
    xml.attribute("component", convertNameToCegui(sliceName));
    xml.attribute("name", imageName);
    xml.closeTag(); // Image
  }

  xml.closeTag(); // FrameComponent
}

CEGUI::String VerticalFrame::convertNameToCegui(const QString& name)
{
  if(name=="top")
    return "TopEdge";
  else if(name=="bottom")
    return "BottomEdge";
  else if(name=="center")
    return "Background";

  assert(false && "There should be no unkown image-slice name");
  return "";
}




Frame::Frame(int n, int w, int s, int e)
  : n(n),
    w(w),
    s(s),
    e(e)
{
}

Frame::Ptr Frame::create(int n, int w, int s, int e)
{
  return Ptr(new Frame(n, w, s, e));
}

Frame::Ptr Frame::create(int frameWidth)
{
  return create(frameWidth, frameWidth, frameWidth, frameWidth);
}

ImageSlice::Slices Frame::slices(const Rect& whole, State)
{
  int width = whole.width();
  int height = whole.height();

  assert(width >= w+e);
  assert(height >= n+s);

  int centerWidth = width - w - e;
  int centerHeight = height - n - s;

  Slices slices;

  if(n > 0)
  {
    if(w > 0)
      slices["nw"] = whole.absoluteSubRect(ivec2(0), ivec2(w, n), true);
    if(centerWidth > 0)
      slices["n"] = whole.absoluteSubRect(ivec2(w, 0), ivec2(centerWidth, n), true);
    if(e > 0)
      slices["ne"] = whole.absoluteSubRect(ivec2(-e, 0), ivec2(e, n), true);
  }
  if(centerHeight > 0)
  {
    if(w > 0)
      slices["w"] = whole.absoluteSubRect(vec2(0, n), ivec2(w, centerHeight), true);
    if(centerWidth > 0)
      slices["center"] = whole.absoluteSubRect(vec2(w, n), ivec2(centerWidth, centerHeight), true);
    if(e > 0)
      slices["e"] = whole.absoluteSubRect(vec2(-e, n), ivec2(e, centerHeight), true);
  }
  if(s > 0)
  {
    if(w > 0)
      slices["sw"] = whole.absoluteSubRect(vec2(0, -s), ivec2(w, s), true);
    if(centerWidth > 0)
      slices["s"] = whole.absoluteSubRect(vec2(w, -s), ivec2(centerWidth, s), true);
    if(e > 0)
      slices["se"] = whole.absoluteSubRect(vec2(-e, -s), ivec2(e, s), true);
  }

  return slices;
}

void Frame::addImageToImagerySection(CEGUI::XMLSerializer& xml, const CEGUI::String& name, const ivec2& size, State state)
{
  xml.openTag("FrameComponent");
  area->serializeToCegui(xml);

  for(const QString& sliceName : this->slices(Rect(ivec2(0), size), state).keys())
  {
    CEGUI::String imageName = WidgetLooks::WidgetLook::composeName(name, String::fromQString(sliceName).toCeguiString());

    xml.openTag("Image");
    xml.attribute("component", convertNameToCegui(sliceName));
    xml.attribute("name", imageName);
    xml.closeTag(); // Image
  }

  xml.closeTag(); // FrameComponent
}

CEGUI::String Frame::convertNameToCegui(const QString& name)
{
  if(name=="n")
    return "TopEdge";
  else if(name=="w")
    return "LeftEdge";
  else if(name=="s")
    return "BottomEdge";
  else if(name=="e")
    return "RightEdge";
  else if(name=="nw")
    return "TopLeftCorner";
  else if(name=="sw")
    return "BottomLeftCorner";
  else if(name=="se")
    return "BottomRightCorner";
  else if(name=="ne")
    return "TopRightCorner";
  else if(name=="center")
    return "Background";

  assert(false && "There should be no unkown image-slice name");
  return "";
}


SwitchForStates::SwitchForStates(const SlicePalette& palette)
  : slicesForState(palette)
{
}

SwitchForStates::Ptr SwitchForStates::create(const SlicePalette& SlicePalette)
{
  return Ptr(new SwitchForStates(SlicePalette));
}

SwitchForStates::Slices SwitchForStates::slices(const Rect& whole, State state)
{
  return slicesForState[state]->slices(whole, state);
}

void SwitchForStates::addImageToImagerySection(CEGUI::XMLSerializer& xml, const CEGUI::String& name, const ivec2& size, State state)
{
  return slicesForState[state]->addImageToImagerySection(xml, name, size, state);
}


} // namespace ImageSlices
} // namespace GuiThemes
} // namespace ProceduralAssets
} // namespace Framework

