#include "palette.h"

namespace Framework {
namespace ProceduralAssets {
namespace GuiThemes {


template<typename T>
Palette<T>::Palette(const T& fallbackColor)
  : fallbackValue(fallbackColor)
{
}


template<typename T>
Palette<T>::Palette() = delete;

template<>
inline Palette<vec4>::Palette()
  : fallbackValue(vec4(1))
{
}

template<>
inline Palette<QBrush>::Palette()
  : fallbackValue(QBrush(Qt::NoBrush))
{
}

template<>
inline Palette<QPen>::Palette()
  : fallbackValue(QPen(Qt::NoPen))
{
}

template<>
inline Palette<vec2>::Palette()
  : fallbackValue(vec2(0))
{
}


template<typename T>
typename Palette<T>::Ptr Palette<T>::create(const T& fallbackColor)
{
  return Ptr(new Palette<T>(fallbackColor));
}

template<typename T>
typename Palette<T>::Ptr Palette<T>::create()
{
  return Ptr(new Palette<T>());
}


template<typename T>
T Palette<T>::colorForState(State state) const
{
  Optional<T> value = optionalFromHashMap(valueForState, state);

  if(value)
    return *value;

  Optional<StateMask> bestMask;
  for(const StateMask& stateMask : valueForStateMask.keys())
  {
    if(!stateMask.matchesState(state))
      continue;

    if(!bestMask.valid() || bestMask->numberMatchingBits(state)<stateMask.numberMatchingBits(state))
      bestMask = stateMask;
  }

  if(bestMask)
    return valueForStateMask[*bestMask];

  return fallbackValue;
}

template<typename T>
T Palette<T>::operator[](State state) const
{
  return this->colorForState(state);
}


template<typename T>
Palette<T>& Palette<T>::apply(const std::function<vec4(const vec4&)>& lambda)
{
  for(HashIterator i=valueForState.begin(); i!=valueForState.end(); ++i)
  {
    i.value() = lambda(i.value());
  }
  for(MashHashIterator i=valueForStateMask.begin(); i!=valueForStateMask.end(); ++i)
  {
    i.value() = lambda(i.value());
  }

  fallbackValue = lambda(this->fallbackValue);

  return *this;
}

template<typename T>
Palette<T>& Palette<T>::operator*=(const T& value)
{
  return this->apply([&value](const T& x){return x*value;});
}

template<typename T>
Palette<T> Palette<T>::operator*(const T& value) const
{
  Palette<T> copy = *this;

  copy *= value;

  return copy;
}



} // namespace GuiThemes
} // namespace ProceduralAssets
} // namespace Framework

