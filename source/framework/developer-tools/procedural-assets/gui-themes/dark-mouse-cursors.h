#ifndef FRAMEWORK_PROCEDURALASSETS_GUITHEMES_DARKMOUSECURSORS_H
#define FRAMEWORK_PROCEDURALASSETS_GUITHEMES_DARKMOUSECURSORS_H

#include "base-theme.h"

namespace Framework {
namespace ProceduralAssets {
namespace GuiThemes {


class DarkMouseCursors final : public BaseTheme
{
public:
  DarkMouseCursors();
  ~DarkMouseCursors();

  TextureAtlas createTextureAtlas();

  static String defaultCursor();
  static String textCursor();
  static String moveCursor();
  static String moveVerticalCursor();
  static String moveHorizontalCursor();
  static String moveDiagonalNwToSeCursor();
  static String moveDiagonalNeToSwCursor();
};


} // namespace GuiThemes
} // namespace ProceduralAssets
} // namespace Framework

#endif // FRAMEWORK_PROCEDURALASSETS_GUITHEMES_DARKMOUSECURSORS_H
