#ifndef FRAMEWORK_PROCEDURALASSETS_GUITHEMES_WIDGETLOOK_INL
#define FRAMEWORK_PROCEDURALASSETS_GUITHEMES_WIDGETLOOK_INL

#include "widget-looks.h"

namespace Framework {
namespace ProceduralAssets {
namespace GuiThemes {
namespace WidgetLooks {



template<typename... T>
inline CEGUI::String WidgetLook::composeName(const CEGUI::String& name, const T&... rest)
{
  CEGUI::String restName = composeName(rest...);

  if(name.length()==0)
  {
    return restName;
  }else if(restName.length()==0)
  {
    return name;
  }else
  {
    return name + "." + restName;
  }
}



} // namespace WidgetLooks
} // namespace GuiThemes
} // namespace ProceduralAssets
} // namespace Framework

#endif // FRAMEWORK_PROCEDURALASSETS_GUITHEMES_WIDGETLOOK_INL
