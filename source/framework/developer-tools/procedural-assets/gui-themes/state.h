#ifndef FRAMEWORK_PROCEDURALASSETS_GUITHEMES_STATE_H
#define FRAMEWORK_PROCEDURALASSETS_GUITHEMES_STATE_H

#include "../texture-atlas-space-manager.h"

namespace Framework {
namespace ProceduralAssets {
namespace GuiThemes {


#define MASK_TO_STRING(_Variation) \
String toString##_Variation() const \
{ \
  String result; \
  for(int i=0; i<sizeof(State::underlying_type)*8-1; ++i) \
  { \
    State state = State::Value(State::underlying_type(1) << i); \
    if((state.value&this->value) == 0) \
      continue; \
    Optional<String> s = state.singleFlagToString##_Variation(); \
    if(!s) \
      s = state.valueAsHexCode(); \
    result = result.append('|', *s); \
  } \
  return "(" + result + ")"; \
}

BEGIN_ENUMERATION(State,)
  NONE = 0,
  NORMAL = 1,
  INSENSITIVE = 1<<1,
  HOVER = 1<<2,
  FOCUSED = 1<<3,
  PRESSED = 1<<4,

  // Button Specific
  BUTTON_OUTSIDE_WIDGET = 1<<16,

  // ToggleButton Specific
  TOGGLEBUTTON_ACTIVE = 1<<17,

  // CheckButton Specific
  CHECKBUTTON_ACTIVE = 1<<17,
  CHECKBUTTON_UNDEFINED = 1<<18,

  // TextBox Specific
  TEXTBOX_READONLY = 1<<16,
  TEXTBOX_SELECTION = 1<<17,
  TEXTBOX_FOCUSED_SELECTION = 1<<18,

  // WindowFrame Specific
  WINDOWFRAME_WITH_FRAME = 1<<16,
  WINDOWFRAME_WITH_TITLEBAR = 1<<17,

  MASK_INPUT_STATE = 0x0000ffff,
  MASK_WIDGET_STATE = 0xffff0000,
ENUMERATION_BASIC_OPERATORS(State)
ENUMERATION_FLAG_OPERATORS(State)
ENUM_VALUE_AS_INTEGER(State)

private:
  Optional<String> singleFlagToString() const;
  Optional<String> singleFlagToString_Button() const;
  Optional<String> singleFlagToString_CheckButton() const;
  Optional<String> singleFlagToString_ToggleButton() const;
  Optional<String> singleFlagToString_TextField() const;
  Optional<String> singleFlagToString_WindowFrame() const;

public:
  MASK_TO_STRING()
  MASK_TO_STRING(_Button)
  MASK_TO_STRING(_ToggleButton)
  MASK_TO_STRING(_CheckButton)
  MASK_TO_STRING(_TextField)
  MASK_TO_STRING(_WindowFrame)
END_ENUMERATION;

#undef MASK_TO_STRING

class StateMask
{
public:
  State::Value state;

  /** The bits which are used for comparison
   */
  State::Value mask;

  StateMask();
  StateMask(State state);
  StateMask(State::Value state);
  StateMask(State state, State mask);
  StateMask(State::Value state, State::Value mask);

  static StateMask pressedMask();

  bool matchesState(State state) const;
  int numberMatchingBits(State state) const;

  bool operator==(const StateMask& other) const;
  bool operator!=(const StateMask& other) const;
};

int qHash(const StateMask& stateMask);


} // namespace GuiThemes
} // namespace ProceduralAssets
} // namespace Framework

#endif // FRAMEWORK_PROCEDURALASSETS_GUITHEMES_STATE_H
