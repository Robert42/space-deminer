#include "dark-theme.h"
#include "dark-mouse-cursors.h"
#include "joined-border.h"
#include "../texture-atlas-space-manager.h"
#include "../../system-font.h"

#include <base/color-tools.h>
#include <base/io/log.h>

#include <CEGUI/XMLSerializer.h>

namespace Framework {
namespace ProceduralAssets {
namespace GuiThemes {

ImageElements::Element::Ptr createCheckmark(const ColorPalette& palette);
ImageElements::Shape::Ptr createCross(const ColorPalette& palette=vec4(1,0,1,1));
ImageElements::Shape::Ptr createTriangle(int angle, const vec2& size, const ColorPalette& palette=vec4(1,0,1,1));

DarkTheme::DarkTheme(const Settings& settings)
  : BaseTheme(settings.name),
    largeWidgets(settings.largeWidgets)
{
  // ==== Colors ====

  const vec4 white(1, 1, 1, 1);
  const vec4 black(0, 0, 0, 1);

  const bool isDarkTheme = (settings.backgroundColor.r + settings.backgroundColor.g + settings.backgroundColor.b)/3.f < 0.65f;

  const vec4 backgroundColor = settings.backgroundColor;
  const vec4 borderColor = isDarkTheme ? white : black;
  const vec4 borderInverseColor = isDarkTheme ? black : white;
  const vec4 borderKeyColor = settings.keyColorValue(0.5f);

  const vec4 normalContrast(1, 1, 1, 0.8f);
  const vec4 hoverContrast(1, 1, 1, 1.f);
  const vec4 hoverFocusContrast = hoverContrast;
  const vec4 insensitiveContrast(1, 1, 1, 0.25f);
  const vec4 focusContrast = hoverContrast;
  const vec4 pressedContrast = vec4(vec3(0.5f), normalContrast.a);

  const vec4 toolTipContrast(1, 1, 1, 0.5f);
  const vec4 buttonShineContrast(1, 1, 1, 0.125f);

  const vec4 defaultButtonColorContrast(1, 1, 1, 0.25f);
  const vec4 buttonGlowHoverContrast(1, 1, 1, 0.0625f);

  const vec4 widgetFrameContrast(1, 1, 1, 0.25f);
  const vec4 widgetFrameHoverContrast(1, 1, 1, 0.4f);
  const vec4 inputBoxBackgroundContrast(1, 1, 1, 0.25f);
  const vec4 labelTextContrast(1, 1, 1, 1);
  const vec4 labelShadowContrast(1, 1, 1, 0.3f);

  vec4 shadowColor(0,0,0, 0.25);
  vec4 buttonPressedShadowColor(0,0,0, 0.25);

  vec4 widgetFrameColor = borderColor * widgetFrameContrast;
  vec4 widgetFrameHoverColor = borderColor * widgetFrameHoverContrast;
  vec4 widgetFrameFocusColor = borderKeyColor * widgetFrameContrast;

  vec4 labelColor = borderColor * labelTextContrast;
  vec4 labelFocusColor = borderKeyColor * labelTextContrast;
  vec4 labelInverseColor = borderInverseColor * labelTextContrast * labelShadowContrast;
  vec4 labelInsensitiveColor = black * labelTextContrast;
  vec4 labelInsensitiveShadowColor = white * labelTextContrast * normalContrast;

  vec4 textCaretColor = borderColor;

  vec4 radioButtonContentColor = labelFocusColor;

  vec4 toolTipBorderColor = borderColor * toolTipContrast;
  vec4 toolTipBackgroundColor = borderInverseColor * toolTipContrast;

  vec4 inputBoxBackgroundColor = borderInverseColor * inputBoxBackgroundContrast * normalContrast;
  vec4 inputBoxBackgroundHoverColor = glm::mix(borderInverseColor, borderKeyColor, 0.6f) * inputBoxBackgroundContrast * normalContrast;

  vec4 textSelectionBackgroundColor = glm::mix(borderKeyColor, borderInverseColor, 0.3f);
  vec4 textSelectionTextColor = borderInverseColor;

  // ==== Color Palettes ====

  ColorPalette shadowPalette;
  shadowPalette.fallbackValue = shadowColor;
  shadowPalette.valueForStateMask[State::INSENSITIVE] = shadowColor * insensitiveContrast;

  ColorPalette buttonPressedOrActiveShadow;
  buttonPressedOrActiveShadow.fallbackValue = vec4(0);
  buttonPressedOrActiveShadow.valueForStateMask[StateMask(State::PRESSED,State::PRESSED|State::INSENSITIVE)] = buttonPressedShadowColor;
  buttonPressedOrActiveShadow.valueForStateMask[StateMask(State::PRESSED|State::INSENSITIVE,State::PRESSED|State::INSENSITIVE)] = buttonPressedShadowColor * insensitiveContrast;
  buttonPressedOrActiveShadow.valueForStateMask[StateMask(State::TOGGLEBUTTON_ACTIVE,State::TOGGLEBUTTON_ACTIVE|State::INSENSITIVE)] = buttonPressedShadowColor;
  buttonPressedOrActiveShadow.valueForStateMask[StateMask(State::TOGGLEBUTTON_ACTIVE|State::INSENSITIVE,State::TOGGLEBUTTON_ACTIVE|State::INSENSITIVE)] = buttonPressedShadowColor * insensitiveContrast;

  ColorPalette widgetFramePalette;
  widgetFramePalette.fallbackValue = widgetFrameColor * normalContrast;
  widgetFramePalette.valueForStateMask[State::HOVER] = widgetFrameHoverColor * hoverContrast;
  widgetFramePalette.valueForStateMask[State::INSENSITIVE] = widgetFrameColor * insensitiveContrast;
  widgetFramePalette.valueForStateMask[State::FOCUSED] = widgetFrameFocusColor * focusContrast;

  ColorPalette buttonFramePalette = widgetFramePalette;
  buttonFramePalette.valueForStateMask[State::HOVER|State::FOCUSED] = widgetFrameFocusColor * hoverFocusContrast;
  buttonFramePalette.valueForStateMask[State::PRESSED] = widgetFrameFocusColor * pressedContrast;
  buttonFramePalette.valueForStateMask[StateMask(State::TOGGLEBUTTON_ACTIVE, State::TOGGLEBUTTON_ACTIVE)] = glm::mix(widgetFrameFocusColor, black, 0.5f) * pressedContrast;

  ColorPalette joinedButtonFramePalette = buttonFramePalette * vec4(1,1,1,0.5f);
  ColorPalette joinedButtonFrameNeutralizationPalette = joinedButtonFramePalette;
  auto calcNeutralizationColor = [backgroundColor](const vec4&) {
    return backgroundColor;
  };
  joinedButtonFrameNeutralizationPalette.apply(calcNeutralizationColor);

  ColorPalette inputBoxFramePalette = widgetFramePalette;

  ColorPalette inputBoxBackgroundPalette = inputBoxBackgroundColor;
  inputBoxBackgroundPalette.valueForStateMask[State::INSENSITIVE] = inputBoxBackgroundColor * insensitiveContrast;

  ColorPalette radioButtonBackgroundPalette = inputBoxBackgroundPalette;
  radioButtonBackgroundPalette.valueForStateMask[State::HOVER] = inputBoxBackgroundHoverColor * hoverContrast;
  radioButtonBackgroundPalette.valueForStateMask[State::HOVER|State::FOCUSED] = inputBoxBackgroundHoverColor * hoverFocusContrast;

  ColorPalette labelPalette;
  labelPalette.fallbackValue = labelColor * normalContrast;
  labelPalette.valueForStateMask[State::INSENSITIVE] = labelInsensitiveColor * insensitiveContrast;

  ColorPalette labelShadowPalette;
  labelShadowPalette.fallbackValue = labelInverseColor * normalContrast;
  labelShadowPalette.valueForStateMask[State::INSENSITIVE] = labelInsensitiveShadowColor * insensitiveContrast;

  ColorPalette radioButtonContentPalette = labelPalette;
  radioButtonContentPalette.valueForStateMask[State::FOCUSED] = radioButtonContentColor * focusContrast;
  radioButtonContentPalette.valueForStateMask[State::HOVER|State::FOCUSED] = radioButtonContentColor * hoverFocusContrast;
  radioButtonContentPalette.valueForStateMask[State::PRESSED] = radioButtonContentColor * pressedContrast;

  ColorPalette buttonTextColor = labelPalette;
  buttonTextColor.valueForStateMask[StateMask::pressedMask()] = vec4(labelColor.rgb(), 0);

  ColorPalette buttonTextShadowColor = labelShadowPalette;
  buttonTextShadowColor.valueForStateMask[StateMask::pressedMask()] = buttonTextColor.fallbackValue;

  ColorPalette tooltipBorderPalette;
  tooltipBorderPalette.fallbackValue = toolTipBorderColor;
  tooltipBorderPalette.valueForStateMask[State::INSENSITIVE] = toolTipBorderColor * insensitiveContrast;

  ColorPalette tooltipBackgroundColor;
  tooltipBackgroundColor.fallbackValue = toolTipBackgroundColor;
  tooltipBackgroundColor.valueForStateMask[State::INSENSITIVE] = toolTipBackgroundColor * insensitiveContrast;

  ColorPalette textBoxTextColorPalette;
  textBoxTextColorPalette.fallbackValue = labelColor;
  textBoxTextColorPalette.valueForStateMask[State::TEXTBOX_SELECTION] = textSelectionTextColor;

  ColorPalette buttonGlowPalette;
  buttonGlowPalette.fallbackValue = vec4(0);
  buttonGlowPalette.valueForStateMask[State::HOVER] = white * buttonGlowHoverContrast;;
  buttonGlowPalette.valueForStateMask[State::FOCUSED] = mix(borderKeyColor, white, 0.5f) * buttonGlowHoverContrast;;
  buttonGlowPalette.valueForStateMask[State::PRESSED] = borderKeyColor * buttonGlowHoverContrast;;

  ColorPalette defaultButtonColorPalette;
  defaultButtonColorPalette.fallbackValue = borderKeyColor * defaultButtonColorContrast;

  ColorPalette scrollbarButtonBackgroundPalette = backgroundColor;
  scrollbarButtonBackgroundPalette.valueForStateMask[State::HOVER] = glm::mix(backgroundColor, white, 0.125f);
  scrollbarButtonBackgroundPalette.valueForStateMask[State::PRESSED] = glm::mix(backgroundColor, black, 0.125f);
  ColorPalette scrollbarButtonBorderPalette = inputBoxFramePalette * vec4(1, 1, 1, 0.5f);

  ColorPalette scrollbarBackgroundPalette = backgroundColor * vec4(vec3(0.9f), 1);
  ColorPalette scrollbarBorderPalette = vec4(0, 0, 0, 0.125f);

  // ==== Brushes ====

  auto initGradient = [](QLinearGradient& gradient, const vec4& contrast, bool invert=false) {
    float r1 = 0.3f;
    float r2 = 0.4f;
    float r3 = 0.45f;

    float a1 = 0.25f;
    float a2 = 0.125f;

    gradient.setStart(0.f, 0.f+invert);
    gradient.setFinalStop(0.f, 1.f-invert);
    gradient.setCoordinateMode(QGradient::ObjectBoundingMode);

    gradient.setColorAt(0.00f, toQColor(vec4(1, 1, 1, 1) * contrast));
    gradient.setColorAt(r1, toQColor(vec4(1, 1, 1, a1) * contrast));
    gradient.setColorAt(r2, toQColor(vec4(1, 1, 1, a2) * contrast));
    gradient.setColorAt(r3, toQColor(vec4(1, 1, 1, 0) * contrast));
    gradient.setColorAt(1.f-r3, toQColor(vec4(0, 0, 0, 0) * contrast));
    gradient.setColorAt(1.f-r2, toQColor(vec4(0, 0, 0, a2) * contrast));
    gradient.setColorAt(1.f-r1, toQColor(vec4(0, 0, 0, a1) * contrast));
    gradient.setColorAt(1.00f, toQColor(vec4(0, 0, 0, 1) * contrast));
  };

  initGradient(buttonShininessGradient, buttonShineContrast * normalContrast);
  initGradient(buttonShininessGradientHover, buttonShineContrast * hoverContrast);
  initGradient(buttonShininessGradientPressed, buttonShineContrast * pressedContrast, true);

  buttonShininessBrush = QBrush(buttonShininessGradient);

  // ==== Brush-Palettes ====

  BrushPalette buttonShininessPalette;

  State::Value toggleMask = State::MASK_INPUT_STATE|State::TOGGLEBUTTON_ACTIVE;
  State::Value toggleMaskPressed = toggleMask|State::BUTTON_OUTSIDE_WIDGET;
  buttonShininessPalette.valueForStateMask[StateMask(State::NORMAL, toggleMask)] = buttonShininessBrush;
  buttonShininessPalette.valueForStateMask[StateMask(State::HOVER, toggleMask)] = buttonShininessGradientHover;
  buttonShininessPalette.valueForStateMask[StateMask(State::FOCUSED, toggleMask)] = buttonShininessBrush;
  buttonShininessPalette.valueForStateMask[StateMask(State::HOVER|State::FOCUSED, toggleMask)] = buttonShininessGradientHover;
  buttonShininessPalette.valueForStateMask[StateMask(State::PRESSED, toggleMaskPressed)] = buttonShininessGradientPressed;
  buttonShininessPalette.valueForStateMask[StateMask(State::PRESSED|State::BUTTON_OUTSIDE_WIDGET, toggleMaskPressed)] = buttonShininessBrush;
  buttonShininessPalette.valueForStateMask[StateMask(State::TOGGLEBUTTON_ACTIVE,State::TOGGLEBUTTON_ACTIVE)] = buttonShininessGradientPressed;

  // ==== Dimensions ====

  const float dpiFactor = settings.dpiFactor;
  const float metricsFactor = settings.metricsFactor;

  float fontHeight = 10.f * metricsFactor;

  real smallRounding = 5.f * dpiFactor;
  real buttonRounding = smallRounding;
  real inputBoxRounding = smallRounding;
  real toolButtonRounding = buttonRounding;
  real checkButtonRounding = 1.f * dpiFactor;
  real windowRoundingLarge = 16 * dpiFactor;
  real windowRoundingSmall = 4 * dpiFactor;
  real joinedBorderRounding = smallRounding;
  real shadowFrameRounding = smallRounding;

  vec2 windowtitleMargin = vec2(6, 4) * dpiFactor;
  real buttonMargin = 1*dpiFactor;
  real inpuxBoxMargin = 1*dpiFactor;

  real inputBoxPadding = 3 * metricsFactor;
  real radioButtonPadding = 5.f * metricsFactor;
  real checkmarkMixedStatePadding = 4.f * metricsFactor;
  real checkmarkPadding = 2.f * metricsFactor;

  real buttonSliceSize = buttonRounding+1;
  real comboBoxSliceSize = buttonSliceSize;

  real windowShadowWidth = 4 * dpiFactor;
  real tooltipShadowWidth = windowShadowWidth;
  real buttonInnerShadowWidth = 6.f * dpiFactor;
  real inpuBoxShadowWidth = 4 * dpiFactor;
  real radioButtonShadowWidth = 4 * dpiFactor;
  real shadowFrameWidth = 6 * dpiFactor;

  vec2 buttonSize = vec2(25) * dpiFactor;
  vec2 inputBoxSize = vec2(fontHeight+(inpuxBoxMargin+inputBoxPadding)*2);
  vec2 checkButtonSize = vec2(17) * dpiFactor;
  vec2 comboBoxSize = buttonSize;
  vec2 windowFrameSize = vec2(largeWidgets ? vec2(128, 64) : vec2(36, 32)) * dpiFactor;
  vec2 tooltipSize = vec2(largeWidgets ? vec2(22) : vec2(16)) * dpiFactor;
  vec2 joinedBorderSize = vec2(joinedBorderRounding*2.f + 3.f);
  vec2 shadowFrameInSize = vec2(shadowFrameWidth*2+3);
  vec2 shadowFrameOutSize = shadowFrameInSize + vec2(shadowFrameRounding*2);
  vec2 scrollbarSliderButtonSize = checkButtonSize;
  vec2 scrollbarButtonSize = scrollbarSliderButtonSize;
  vec2 scrollbarSize = scrollbarSliderButtonSize;

  if(largeWidgets)
  {
    buttonSize.x *= 3;
    inputBoxSize.x *= 4;
    windowFrameSize.x *= 3;
  }

  vec2 numberBoxTriangleSize = vec2(inputBoxSize.y-inputBoxPadding*2) / vec2(1, 2);
  real numberBoxButtonWidth = inputBoxSize.y;

  vec2 textBoxSelectionSize(fontHeight);
  real textCaretWidth = 2 * dpiFactor;

  real windowFrameTitleHeight = checkButtonSize.y+4*dpiFactor;
  real windowFrameMargin = windowShadowWidth + 1*dpiFactor;
  real windowSliceSizeLarge=windowRoundingLarge;
  real windowSliceSizeSmall=windowRoundingSmall+windowShadowWidth;
  ivec2 windowCloseButtonSize = ivec2(windowFrameTitleHeight, windowFrameTitleHeight);

  real smallPadding = 3 * metricsFactor;
  real windowCloseButtonShadowRadius = 2 * dpiFactor;

  // ==== Fonts ====

  const String defaultFont = systemFont.getFontName(fontHeight, SystemFont::Flags::NORMAL);

  // ==== Dimension-Palettes ====

  Vec2Palette translateIfPushed;
  translateIfPushed.valueForStateMask[StateMask::pressedMask()] = vec2(dpiFactor);

  // ==== Shape-Initializer ====

  JoinedBorder joinedBorder;

  auto createRectangle = [&](real rounding) {
    float cornerFactor = 0.82f * dpiFactor;
    return ImageElements::RectangleShapeWithDifferentRadii::create(0,
                                                                   vec4(0),
                                                                   0,
                                                                   joinedBorder.joinedCornerNW() ? cornerFactor : rounding,
                                                                   joinedBorder.joinedCornerSW() ? cornerFactor : rounding,
                                                                   joinedBorder.joinedCornerSE() ? cornerFactor : rounding,
                                                                   joinedBorder.joinedCornerNE() ? cornerFactor : rounding);
  };

  auto createCircle = [&]() {
    return ImageElements::CircleShape::create(0, vec4(0), 1);
  };

  auto initButtonFrame = [&](ImageElements::ShapeWithStrokeOffset::Ptr shape) {

    bool isJoined = joinedBorder.joinedCornerNW() || joinedBorder.joinedCornerSW() || joinedBorder.joinedCornerSE() || joinedBorder.joinedCornerNE();

    shape = shape->copy();
    shape->palette = isJoined ? joinedButtonFramePalette : buttonFramePalette;
    shape->thickness = dpiFactor;
    return shape;
  };

  auto initButtonShininess = [&](ImageElements::ShapeWithStrokeOffset::Ptr shape) {

    shape = shape->copyWithAdditionalStrokeOffset(-1, buttonShininessPalette);
    shape->thickness = 0.f;

    ImageElements::Stack::Ptr stack = ImageElements::createWitInnerhShadowUsingOffset(shape, buttonInnerShadowWidth, buttonPressedOrActiveShadow);

    shape = shape->copy();
    shape->palette = buttonGlowPalette;
    stack->push(shape);
    return stack;
  };

  auto initButton = [&]() {
    ImageElements::ShapeWithStrokeOffset::Ptr shape = createRectangle(buttonRounding);
    ImageElements::Stack::Ptr stack = ImageElements::Stack::create();
    stack->push(initButtonFrame(shape));
    stack->push(initButtonShininess(shape));
    return stack;
  };

  auto initToolButton = [&]() {
    StateMask onlyIfNotNormal(State::NONE, State::NORMAL);

    ImageElements::ShapeWithStrokeOffset::Ptr shape = createRectangle(toolButtonRounding);

    ImageElements::Stack::Ptr stack = ImageElements::Stack::create();
    stack->push(initButtonFrame(shape));
    stack->push(initButtonShininess(shape));

    return ImageElements::StateFilter::create(onlyIfNotNormal, stack);
  };

  auto initButtonContent = [&](const ImageElements::Shape::Ptr& element) {
    ImageElements::Element::Ptr conentWithShadow = ImageElements::Stack::create(ImageElements::Translate::create(vec2(1),
                                                                                                                 ImageElements::createWithShadowUsingThickness(element->copyWithDifferentPalette(labelShadowPalette),
                                                                                                                                                               windowCloseButtonShadowRadius,
                                                                                                                                                               shadowPalette)),
                                                                                ImageElements::createWithShadowUsingThickness(element->copyWithDifferentPalette(labelPalette),
                                                                                                                              windowCloseButtonShadowRadius,
                                                                                                                              shadowPalette));
    ImageElements::Element::Ptr conentCentered = ImageElements::Alignment::create(conentWithShadow);
    return ImageElements::Translate::create(translateIfPushed, conentCentered);
  };

  auto initWindowButton = [&](const ImageElements::Shape::Ptr& element) {
    ImageElements::Stack::Ptr stack = ImageElements::Stack::create();

    stack->push(initToolButton());
    stack->push(initButtonContent(element));

    return stack;
  };

  auto initButtonWithContent = [&](const ImageElements::Element::Ptr& content) {
    ImageElements::Stack::Ptr stack = ImageElements::Stack::create();
    stack->push(initButton());
    stack->push(ImageElements::Padding::create(buttonMargin,
                                               ImageElements::Translate::create(translateIfPushed, content)));
    return stack;
  };

  auto initSimpleButton = [&]() {
    ImageElements::ShapeWithStrokeOffset::Ptr shape = createRectangle(buttonRounding);
    ImageElements::Stack::Ptr stack = ImageElements::Stack::create();
    shape = shape->copy();
    stack->push(initButtonFrame(shape));
    shape->thickness = 0.f;
    stack->push(initButtonShininess(shape));
    return stack;
  };

  auto initDefaultButton = [&]() {
    ImageElements::ShapeWithStrokeOffset::Ptr shape = createRectangle(buttonRounding);
    ImageElements::Stack::Ptr stack = ImageElements::Stack::create();
    shape = shape->copy();
    stack->push(initButtonFrame(shape));
    shape->thickness = 0.f;
    stack->push(shape->copyWithAdditionalStrokeOffset(-1, defaultButtonColorPalette));
    stack->push(initButtonShininess(shape));
    return stack;
  };

  auto initInputBoxFrame = [&](ImageElements::ShapeWithStrokeOffset::Ptr shape, float relativeThickness=1.f) {
    shape = shape->copy();
    shape->palette = inputBoxFramePalette;
    shape->thickness = dpiFactor*relativeThickness;
    return shape;
  };

  auto initInputBoxBackground = [&](ImageElements::ShapeWithStrokeOffset::Ptr shape, real shadowWidth, const ColorPalette& background) {
    shape = shape->copyWithAdditionalStrokeOffset(-1);
    shape->thickness = 0.f;
    shape->palette = background;

    return ImageElements::createWitInnerhShadowUsingOffset(shape, shadowWidth, shadowColor);
  };

  auto initInputBox = [&]() {
    ImageElements::ShapeWithStrokeOffset::Ptr shape = createRectangle(inputBoxRounding);
    ImageElements::Stack::Ptr stack = ImageElements::Stack::create();

    stack->push(initInputBoxBackground(shape, inpuBoxShadowWidth, inputBoxBackgroundPalette));
    stack->push(initInputBoxFrame(shape));

    return stack;
  };


  auto initRadioButton = [&](ImageElements::ShapeWithStrokeOffset::Ptr shape, ImageElements::Element::Ptr content, float relativeThickness=1.f) {

    ImageElements::Stack::Ptr stack = ImageElements::Stack::create();

    content = ImageElements::Translate::create(translateIfPushed, content);

    stack->push(initInputBoxBackground(shape, radioButtonShadowWidth, radioButtonBackgroundPalette));
    stack->push(initInputBoxFrame(shape, relativeThickness));
    stack->push(content);

    return stack;
  };

  auto initTextBoxSelection = [&]() {
    return ImageElements::RectangleShape::create(0, textSelectionBackgroundColor, 0);
  };


  auto initWindowFrameWithFrame = [&](real nRadius) {

    ImageElements::ShapeWithStrokeOffset::Ptr shape = ImageElements::RectangleShapeWithDifferentRadii::create(0, buttonFramePalette, 1, nRadius, windowShadowWidth);
    ImageElements::Stack::Ptr stack = ImageElements::createWithShadowUsingOffset(shape,
                                                                                 windowShadowWidth,
                                                                                 shadowPalette);
    real strokeOffset = -1;
    ImageElements::ShapeWithStrokeOffset::Ptr windowBackgroundShape = shape->copyWithAdditionalStrokeOffset(strokeOffset--, backgroundColor);
    windowBackgroundShape->thickness = 0.f;
    stack->push(windowBackgroundShape);

    return stack;
  };

  auto initWindowFrameWithoutFrame = [&]() {
    return ImageElements::RectangleShape::create(0, backgroundColor, 0);
  };

  auto initWindowFrame = [&]() {
    ImageElements::Stack::Ptr stack = ImageElements::Stack::create();

    stack->push(ImageElements::StateFilter::create(StateMask(State::WINDOWFRAME_WITH_TITLEBAR|State::WINDOWFRAME_WITH_FRAME,
                                                             State::MASK_WIDGET_STATE),
                                                   initWindowFrameWithFrame(windowRoundingLarge)));
    stack->push(ImageElements::StateFilter::create(StateMask(State::WINDOWFRAME_WITH_FRAME,
                                                             State::MASK_WIDGET_STATE),
                                                   initWindowFrameWithFrame(windowRoundingSmall)));
    stack->push(ImageElements::StateFilter::create(StateMask(State::WINDOWFRAME_WITH_TITLEBAR,
                                                             State::MASK_WIDGET_STATE),
                                                   initWindowFrameWithoutFrame()));
    stack->push(ImageElements::StateFilter::create(StateMask(State::NONE,
                                                             State::MASK_WIDGET_STATE),
                                                   initWindowFrameWithoutFrame()));

    return stack;
  };

  auto initToolTip = [&]() {

    ImageElements::ShapeWithStrokeOffset::Ptr shape = ImageElements::RectangleShape::create(0, tooltipBorderPalette, 1);
    ImageElements::Stack::Ptr stack = ImageElements::createWithShadowUsingOffset(shape, tooltipShadowWidth, shadowPalette);

    real strokeOffset = -1.f;

    ImageElements::ShapeWithStrokeOffset::Ptr windowBackgroundShape = shape->copyWithAdditionalStrokeOffset(strokeOffset--, tooltipBackgroundColor);
    windowBackgroundShape->thickness = 0.f;
    stack->push(windowBackgroundShape);

    return stack;
  };

  auto initJoinedBorderFrame = [&]() {
    ImageElements::Stack::Ptr stack = ImageElements::Stack::create();
    stack->push(ImageElements::InverseRectangleShape::create(joinedButtonFrameNeutralizationPalette, joinedBorderRounding));
    stack->push(ImageElements::RectangleShape::create(0, joinedButtonFrameNeutralizationPalette, dpiFactor));
    stack->push(ImageElements::RectangleShape::create(0, inputBoxFramePalette, dpiFactor, joinedBorderRounding));

    return stack;
  };

  auto initShadowFrameIn = [&]() {
    return ImageElements::createWitInnerhShadowUsingOffset(ImageElements::RectangleShape::create(0, vec4(0), 0.f, shadowFrameRounding),
                                                           shadowFrameWidth,
                                                           shadowColor);
  };

  auto initShadowFrameOut = [&]() {
    return ImageElements::createWithShadowUsingOffset(ImageElements::RectangleShape::create(0, vec4(0), 0.f, shadowFrameRounding+shadowFrameWidth),
                                                      shadowFrameWidth,
                                                      shadowColor);
  };

  auto initScrollbarSliderButton = [&]() {
    return ImageElements::Stack::create(ImageElements::RectangleShape::create(0, scrollbarButtonBackgroundPalette, 0),
                                        ImageElements::RectangleShape::create(0, scrollbarButtonBorderPalette, dpiFactor));
  };

  auto initScrollbarButton = [&](int angle) {
    return ImageElements::Stack::create(ImageElements::RectangleShape::create(0, scrollbarButtonBackgroundPalette, 0),
                                        ImageElements::RectangleShape::create(0, scrollbarButtonBorderPalette, dpiFactor),
                                        initButtonContent(createTriangle(angle, numberBoxTriangleSize)));
  };

  auto initScrollbar = [&](bool) {
    return ImageElements::Stack::create(ImageElements::RectangleShape::create(0, scrollbarBackgroundPalette, 0),
                                        ImageElements::RectangleShape::create(0, scrollbarBorderPalette, dpiFactor));
  };

  // ==== Image Slices ====

  ImageSlices::SwitchForStates::SlicePalette windowFrameSlicePalette(ImageSlices::Streched::create());
  windowFrameSlicePalette.valueForStateMask[StateMask(State::WINDOWFRAME_WITH_TITLEBAR|State::WINDOWFRAME_WITH_FRAME,
                                                      State::MASK_WIDGET_STATE)] = ImageSlices::Frame::create(windowSliceSizeLarge, windowSliceSizeLarge, windowSliceSizeSmall, windowSliceSizeLarge);
  windowFrameSlicePalette.valueForStateMask[StateMask(State::WINDOWFRAME_WITH_FRAME,
                                                      State::MASK_WIDGET_STATE)] = ImageSlices::Frame::create(windowSliceSizeSmall);

  ImageSlices::ImageSlice::Ptr dummyImageSlice = ImageSlices::Streched::create();
  ImageSlices::ImageSlice::Ptr buttonSlices = ImageSlices::Frame::create(buttonSliceSize);
  ImageSlices::ImageSlice::Ptr comboBoxSlices = ImageSlices::Frame::create(comboBoxSliceSize, comboBoxSliceSize, comboBoxSliceSize, checkButtonSize.x);
  ImageSlices::ImageSlice::Ptr checkButtonSlices = ImageSlices::Aligned::create(ImageSlices::Aligned::Horizontal::LEFT, ImageSlices::Aligned::Vertical::CENTER);
  ImageSlices::ImageSlice::Ptr windowSlices = ImageSlices::SwitchForStates::create(windowFrameSlicePalette);
  ImageSlices::ImageSlice::Ptr tooltipSlices = ImageSlices::Frame::create(tooltipShadowWidth+dpiFactor);
  ImageSlices::ImageSlice::Ptr inputBoxImageSlice = ImageSlices::Frame::create(max(inputBoxPadding, inputBoxRounding));
  ImageSlices::ImageSlice::Ptr inputBoxSelectionImageSlice = ImageSlices::Frame::create(dpiFactor);
  ImageSlices::ImageSlice::Ptr joinedBorderImageSlices = ImageSlices::Frame::create(joinedBorderRounding+1);
  ImageSlices::ImageSlice::Ptr shadowFrameInImageSlices = ImageSlices::Frame::create(shadowFrameWidth+1);
  ImageSlices::ImageSlice::Ptr shadowFrameOutImageSlices = ImageSlices::Frame::create(shadowFrameWidth+shadowFrameRounding+1);
  ImageSlices::ImageSlice::Ptr scrollbarSliderButtonImageSlices = ImageSlices::Frame::create(dpiFactor + 1);
  ImageSlices::ImageSlice::Ptr scrollbarImageSlices = scrollbarSliderButtonImageSlices;

  // ==== Areas ====

  Area::Ptr fullArea = Area::createFullArea();
  Area::Ptr labelTextArea = Area::createFullAreaWithBorder(0, 0, 0, 0);
  Area::Ptr labelTextShadowArea = Area::createFullAreaWithBorder(1, 1, -1, -1);
  Area::Ptr buttonTextArea = Area::createFullArea();
  Area::Ptr buttonTextShadowArea = Area::createFullAreaWithBorder(1, 1, -1, -1);
  Area::Ptr radioButtonTextArea = Area::createFullAreaWithBorder(0, checkButtonSize.x+smallPadding, 0, 0);
  Area::Ptr radioButtonTextShadowArea = Area::createFullAreaWithBorder(1, checkButtonSize.x+smallPadding+1, -1, -1);

  Area::Ptr windowClientNoTitleNoFrame = Area::createFullArea();
  Area::Ptr windowClientNoTitleWithFrame = Area::createFullAreaWithBorder(windowFrameMargin);
  Area::Ptr windowClientWithTitleNoFrame = Area::createFullAreaWithBorder(windowFrameTitleHeight+2*windowtitleMargin.y, 0, 0, 0);
  Area::Ptr windowClientWithTitleWithFrame = Area::createFullAreaWithBorder(windowFrameTitleHeight+windowFrameMargin+2*windowtitleMargin.y, windowFrameMargin, windowFrameMargin, windowFrameMargin);
  Area::Ptr windowTitleArea = Area::createAlignedArea(ivec2(0, windowFrameTitleHeight), // minSize
                                                      vec2(1, 0), // relativeSize
                                                      vec2(0,0), // relativePosition
                                                      windowFrameMargin+windowtitleMargin.y,
                                                      windowFrameMargin + windowtitleMargin.x,
                                                      windowFrameMargin+windowtitleMargin.y,
                                                      windowFrameMargin + windowCloseButtonSize.x+windowtitleMargin.x+2); // border
  Area::Ptr windowCloseButtonArea = Area::createAlignedArea(windowCloseButtonSize, // minSize
                                                            vec2(0, 0), // relativeSize
                                                            vec2(1,0), // relativePosition
                                                            windowFrameMargin+windowtitleMargin.y,
                                                            windowFrameMargin+windowtitleMargin.x,
                                                            windowFrameMargin+windowtitleMargin.y,
                                                            windowFrameMargin+windowtitleMargin.x); // border
  Area::Ptr tooltipClientArea = Area::createFullAreaWithBorder(tooltipShadowWidth);

  Area::Ptr inputBoxContentArea = Area::createFullAreaWithBorder(inputBoxPadding);
  Area::Ptr textBoxTextArea = inputBoxContentArea;

  Area::Ptr numberBoxTextArea = Area::createFullAreaWithBorder(inputBoxPadding,
                                                               inputBoxPadding+numberBoxButtonWidth,
                                                               inputBoxPadding,
                                                               inputBoxPadding+numberBoxButtonWidth);
  Area::Ptr numberButtonUpArea = Area::createAlignedArea(ivec2(numberBoxButtonWidth, 0), vec2(0, 1), vec2(1,0.5f), 0);
  Area::Ptr numberButtonDownArea = Area::createAlignedArea(ivec2(numberBoxButtonWidth, 0), vec2(0, 1), vec2(0,0.5f), 0);

  Area::Ptr scrollbarVerticalSliderArea = Area::createFullAreaWithBorder(scrollbarButtonSize.y, 0, scrollbarButtonSize.y, 0);
  Area::Ptr scrollbarHorizontalSliderArea = Area::createFullAreaWithBorder(0, scrollbarButtonSize.x, 0, scrollbarButtonSize.x);
  Area::Ptr scrollbarUpButtonArea = Area::createAlignedArea(scrollbarButtonSize, vec2(1, 0), vec2(0.5f, 0.f), 0);
  Area::Ptr scrollbarDownButtonArea = Area::createAlignedArea(scrollbarButtonSize, vec2(1, 0), vec2(0.5f, 1.f), 0);
  Area::Ptr scrollbarRightButtonArea = Area::createAlignedArea(scrollbarButtonSize, vec2(0, 1), vec2(1.f, 0.5f), 0);
  Area::Ptr scrollbarLeftButtonArea = Area::createAlignedArea(scrollbarButtonSize, vec2(0, 1), vec2(0.f, 0.5f), 0);

  // ==== Texts ====

  typedef WidgetLooks::WidgetLook::Text Text;
  Text labelText(defaultFont, Text::VerticalAlignment::CENTER, Text::HorizontalAlignment::LEFT, labelPalette, labelTextArea);
  Text labelTextShadow(labelText.font, Text::VerticalAlignment::CENTER, Text::HorizontalAlignment::LEFT, labelShadowPalette, labelTextShadowArea);
  Text textBoxText(labelText.font, Text::VerticalAlignment::CENTER, Text::HorizontalAlignment::LEFT, labelPalette, textBoxTextArea);
  Text numberBoxText(labelText.font, Text::VerticalAlignment::CENTER, Text::HorizontalAlignment::RIGHT, labelPalette, fullArea);
  Text buttonText(labelText.font, Text::VerticalAlignment::CENTER, Text::HorizontalAlignment::CENTER, buttonTextColor, buttonTextArea);
  Text buttonTextShadow(labelText.font, Text::VerticalAlignment::CENTER, Text::HorizontalAlignment::CENTER, buttonTextShadowColor, buttonTextShadowArea);
  Text windowTitleText(labelText.font, Text::VerticalAlignment::CENTER, Text::HorizontalAlignment::CENTER, labelPalette, buttonTextArea);
  Text windowTitleShadowText(labelText.font, Text::VerticalAlignment::CENTER, Text::HorizontalAlignment::CENTER, labelShadowPalette, buttonTextShadowArea);
  Text radioButtonText(labelText.font, Text::VerticalAlignment::CENTER, Text::HorizontalAlignment::LEFT, labelPalette, radioButtonTextArea);
  Text radioButtonTextShadow(labelText.font, Text::VerticalAlignment::CENTER, Text::HorizontalAlignment::LEFT, labelShadowPalette, radioButtonTextShadowArea);

  // ==== Mouse-Cursors ====

  typedef WidgetLooks::WidgetLook::MouseCursor MouseCursor;
  MouseCursor mouseCursorNtoS(DarkMouseCursors::moveVerticalCursor());
  MouseCursor mouseCursorWtoE(DarkMouseCursors::moveHorizontalCursor());
  MouseCursor mouseCursorNWtoSE(DarkMouseCursors::moveDiagonalNwToSeCursor());
  MouseCursor mouseCursorNEtoSW(DarkMouseCursors::moveDiagonalNeToSwCursor());
  MouseCursor mouseCursorText(DarkMouseCursors::textCursor());

  // ==== Widget-Looks ====


  // Text-Box (Text-Area-Only)
  widgetLooks.append(WidgetLooks::TextBox::create("number-box.text-area" + joinedBorder.formatPostfix(),
                                                  numberBoxText,
                                                  ImageElements::NullElement::create(),
                                                  initTextBoxSelection(),
                                                  inputBoxSize,
                                                  textBoxSelectionSize,
                                                  dummyImageSlice,
                                                  inputBoxSelectionImageSlice,
                                                  textCaretColor,
                                                  textCaretWidth,
                                                  0,
                                                  textBoxTextColorPalette,
                                                  mouseCursorText));

  // Button-Up (Number-Box)
  widgetLooks.append(WidgetLooks::Button::create("number-box.button-up",
                                                 initWindowButton(createTriangle(-1, numberBoxTriangleSize)),
                                                 Text::noText(),
                                                 Text::noText(),
                                                 windowCloseButtonSize));

  // Button-Down (Number-Box)
  widgetLooks.append(WidgetLooks::Button::create("number-box.button-down",
                                                 initWindowButton(createTriangle(1, numberBoxTriangleSize)),
                                                 Text::noText(),
                                                 Text::noText(),
                                                 windowCloseButtonSize));

  // Scrollbar (Slider-Button)
  widgetLooks.append(WidgetLooks::SliderButton::create("scrollbar.vertical.slider-button",
                                                       initScrollbarSliderButton(),
                                                       scrollbarSliderButtonSize,
                                                       scrollbarSliderButtonImageSlices,
                                                       true));
  widgetLooks.append(WidgetLooks::SliderButton::create("scrollbar.horizontal.slider-button",
                                                       initScrollbarSliderButton(),
                                                       scrollbarSliderButtonSize,
                                                       scrollbarSliderButtonImageSlices,
                                                       false));

  // Scrollbar (Buttons)
  widgetLooks.append(WidgetLooks::Button::create("scrollbar.button-up",
                                                 initScrollbarButton(0),
                                                 Text::noText(),
                                                 Text::noText(),
                                                 windowCloseButtonSize));
  widgetLooks.append(WidgetLooks::Button::create("scrollbar.button-left",
                                                 initScrollbarButton(1),
                                                 Text::noText(),
                                                 Text::noText(),
                                                 windowCloseButtonSize));
  widgetLooks.append(WidgetLooks::Button::create("scrollbar.button-down",
                                                 initScrollbarButton(2),
                                                 Text::noText(),
                                                 Text::noText(),
                                                 windowCloseButtonSize));
  widgetLooks.append(WidgetLooks::Button::create("scrollbar.button-right",
                                                 initScrollbarButton(3),
                                                 Text::noText(),
                                                 Text::noText(),
                                                 windowCloseButtonSize));

  // Scrollbar
  widgetLooks.append(WidgetLooks::Scrollbar::create("scrollbar.vertical",
                                                    initScrollbar(true),
                                                    scrollbarSize,
                                                    scrollbarImageSlices,
                                                    scrollbarVerticalSliderArea,
                                                    scrollbarDownButtonArea,
                                                    scrollbarUpButtonArea,
                                                    true,
                                                    "dark-theme.scrollbar.vertical.slider-button",
                                                    "dark-theme.scrollbar.button-down",
                                                    "dark-theme.scrollbar.button-up"));
  widgetLooks.append(WidgetLooks::Scrollbar::create("scrollbar.horizontal",
                                                    initScrollbar(false),
                                                    scrollbarSize,
                                                    scrollbarImageSlices,
                                                    scrollbarHorizontalSliderArea,
                                                    scrollbarRightButtonArea,
                                                    scrollbarLeftButtonArea,
                                                    false,
                                                    "dark-theme.scrollbar.vertical.slider-button",
                                                    "dark-theme.scrollbar.button-right",
                                                    "dark-theme.scrollbar.button-left"));

  for(JoinedBorder jb : {JoinedBorder::NONE, JoinedBorder::ALL})
  {
    joinedBorder = jb;

    // Button
    widgetLooks.append(WidgetLooks::Button::create("button" + joinedBorder.formatPostfix(),
                                                   initSimpleButton(),
                                                   buttonText,
                                                   buttonTextShadow,
                                                   buttonSize,
                                                   buttonSlices));
    // Default-Button
    widgetLooks.append(WidgetLooks::Button::create("default-button" + joinedBorder.formatPostfix(),
                                                   initDefaultButton(),
                                                   buttonText,
                                                   buttonTextShadow,
                                                   buttonSize,
                                                   buttonSlices));
    // Toggle-Button
    widgetLooks.append(WidgetLooks::ToogleButton::create("toggle-button" + joinedBorder.formatPostfix(),
                                                         initSimpleButton(),
                                                         buttonText,
                                                         buttonTextShadow,
                                                         buttonSize,
                                                         buttonSlices));
    // Tool-Button
    widgetLooks.append(WidgetLooks::Button::create("tool-button" + joinedBorder.formatPostfix(),
                                                   initToolButton(),
                                                   buttonText,
                                                   buttonTextShadow,
                                                   buttonSize,
                                                   buttonSlices));

    // Combobox
    widgetLooks.append(WidgetLooks::Button::create("combobox" + joinedBorder.formatPostfix(),
                                                   initButtonWithContent(ImageElements::Padding::create(4.f, ImageElements::VBox::create(3,
                                                                                                                                         ImageElements::TriangleShape::createWithFixedAspect(0, radioButtonContentPalette, glm::radians(90.f), vec2(1, 1)),
                                                                                                                                         ImageElements::TriangleShape::createWithFixedAspect(2, radioButtonContentPalette, glm::radians(90.f), vec2(1, 0))))),
                                                   radioButtonText,
                                                   radioButtonTextShadow,
                                                   comboBoxSize,
                                                   comboBoxSlices));

    // Text-Box
    widgetLooks.append(WidgetLooks::TextBox::create("text-box" + joinedBorder.formatPostfix(),
                                                    textBoxText,
                                                    initInputBox(),
                                                    initTextBoxSelection(),
                                                    inputBoxSize,
                                                    textBoxSelectionSize,
                                                    inputBoxImageSlice,
                                                    inputBoxSelectionImageSlice,
                                                    textCaretColor,
                                                    textCaretWidth,
                                                    inputBoxPadding,
                                                    textBoxTextColorPalette,
                                                    mouseCursorText));

    // Number-Box
    widgetLooks.append(WidgetLooks::NumberBox::create("number-box" + joinedBorder.formatPostfix(),
                                                      initInputBox(),
                                                      inputBoxSize,
                                                      inputBoxImageSlice,
                                                      "dark-theme.number-box.text-area",
                                                      "dark-theme.number-box.button-up",
                                                      "dark-theme.number-box.button-down",
                                                      numberBoxTextArea,
                                                      numberButtonUpArea,
                                                      numberButtonDownArea));
  }

  // Joined-Border
  widgetLooks.append(WidgetLooks::Decoration::create("joined-border",
                                                     initJoinedBorderFrame(),
                                                     joinedBorderSize,
                                                     joinedBorderImageSlices));

  // Frame Shadow In
  widgetLooks.append(WidgetLooks::Decoration::create("shadow-frame.in",
                                                     initShadowFrameIn(),
                                                     shadowFrameInSize,
                                                     shadowFrameInImageSlices));

  // Frame Shadow Out
  widgetLooks.append(WidgetLooks::Decoration::create("shadow-frame.out",
                                                     initShadowFrameOut(),
                                                     shadowFrameOutSize,
                                                     shadowFrameOutImageSlices));



  // Label
  widgetLooks.append(WidgetLooks::Label::create("label",
                                                 labelText,
                                                 labelTextShadow));

  // Radio-Button
  widgetLooks.append(WidgetLooks::ToogleButton::create("radio-button",
                                                       initRadioButton(createCircle(),
                                                                       ImageElements::SwitchForState::create(StateMask(State::TOGGLEBUTTON_ACTIVE, State::TOGGLEBUTTON_ACTIVE),
                                                                                                             ImageElements::Padding::create(radioButtonPadding, ImageElements::CircleShape::create(0, radioButtonContentPalette, 0))),
                                                                       1.2f),
                                                       radioButtonText,
                                                       radioButtonTextShadow,
                                                       checkButtonSize,
                                                       checkButtonSlices));
  // Check-Button
  widgetLooks.append(WidgetLooks::CheckButton::create("check-button",
                                                      initRadioButton(createRectangle(checkButtonRounding),
                                                                            ImageElements::SwitchForState::create(StateMask(State::CHECKBUTTON_ACTIVE, State::CHECKBUTTON_ACTIVE|State::CHECKBUTTON_UNDEFINED),
                                                                                                                  ImageElements::Padding::create(checkmarkPadding, createCheckmark(radioButtonContentPalette)),
                                                                                                                  StateMask(State::CHECKBUTTON_UNDEFINED, State::CHECKBUTTON_ACTIVE|State::CHECKBUTTON_UNDEFINED),
                                                                                                                  ImageElements::Padding::create(checkmarkMixedStatePadding, ImageElements::RectangleShape::create(0, radioButtonContentPalette, 0)))),
                                                      radioButtonText,
                                                      radioButtonTextShadow,
                                                      checkButtonSize,
                                                      checkButtonSlices));

  // Tooltip
  widgetLooks.append(WidgetLooks::Tooltip::create("tooltip",
                                                  tooltipClientArea,
                                                  initToolTip(),
                                                  tooltipSize,
                                                  tooltipSlices));

  // WindowFrame
  widgetLooks.append(WidgetLooks::WindowFrame::create("window-frame",
                                                      initWindowFrame(),
                                                      windowFrameSize,
                                                      windowSlices,
                                                      windowClientNoTitleNoFrame,
                                                      windowClientNoTitleWithFrame,
                                                      windowClientWithTitleNoFrame,
                                                      windowClientWithTitleWithFrame,
                                                      windowTitleArea,
                                                      windowCloseButtonArea,
                                                      mouseCursorNtoS,
                                                      mouseCursorWtoE,
                                                      mouseCursorNWtoSE,
                                                      mouseCursorNEtoSW,
                                                      "dark-theme.window-titlebar",
                                                      "dark-theme.window-button-close"));

  // Window-Titlebar
  widgetLooks.append(WidgetLooks::WindowTitleBar::create("window-titlebar",
                                                         windowTitleText,
                                                         windowTitleShadowText,
                                                         ImageElements::RectangleShape::create(0, vec4(1, 0, 1, 1),0)));

  // Window-Button-Close
  widgetLooks.append(WidgetLooks::Button::create("window-button-close",
                                                 initWindowButton(createCross()),
                                                 Text::noText(),
                                                 Text::noText(),
                                                 windowCloseButtonSize));
}

void DarkTheme::registerToCEGUI()
{
  BaseTheme::registerToCEGUI(targetSize());
}

ivec2 DarkTheme::targetSize() const
{
  return largeWidgets ? largeSize() : smallSize();
}

ivec2 DarkTheme::smallSize()
{
  return ivec2(512, 512);
}

ivec2 DarkTheme::largeSize()
{
  return ivec2(1024, 512);
}


float hueOfKeyColor(DarkTheme::Settings::KeyColor keyColor)
{
  switch(keyColor.value)
  {
  case DarkTheme::Settings::KeyColor::ORANGE:
    return 0.075f;
  case DarkTheme::Settings::KeyColor::BLUE:
    return 0.6f;
  case DarkTheme::Settings::KeyColor::RED:
    return 0.;
  case DarkTheme::Settings::KeyColor::GREEN:
  default:
    return 0.25f;
  }
}

float saturationOfKeyColor(DarkTheme::Settings::KeyColor)
{
  return 1.f;
}

vec4 DarkTheme::Settings::keyColorValue(float luminance) const
{
  return colorFromHsl(hueOfKeyColor(this->keyColor),
                      saturationOfKeyColor(this->keyColor),
                      luminance);
}

DarkTheme::Settings::KeyColor DarkTheme::Settings::randomKeyColor()
{
  static KeyColor::Value keyColor = static_cast<KeyColor::Value>(round(glm::linearRand<float>(KeyColor::GREEN, KeyColor::RED)));

  return keyColor;
}

// ====

void testExportingDarkTheme(const std::string& filename, bool largeWidgets, bool fillBackground)
{
  DarkTheme::Settings settings;
  settings.largeWidgets = largeWidgets;
  DarkTheme theme(settings);

  theme.testExportingToSvg(IO::RegularFile(String::fromUtf8(filename)),
                           theme.targetSize(),
                           fillBackground ? settings.backgroundColor : vec4(0));
}

void testDarkThemeTextureAtlas(const std::string& directory, bool largeWidgets)
{
  DarkTheme::Settings settings;
  settings.largeWidgets = largeWidgets;
  DarkTheme theme(settings);

  theme.testExportingCeguiTextureAtlas(IO::Directory(String::fromUtf8(directory)), theme.targetSize());
}

void testDarkThemeXml(const std::string& xmlFilename)
{
  DarkTheme theme;

  std::ofstream file;

  file.open(xmlFilename);

  CEGUI::XMLSerializer xml(file);

  theme.serializeToCeguiXml(xml);
}

class Checkmark : public ImageElements::Shape
{
public:
  Checkmark(const ColorPalette& palette)
    : Shape(palette, 0)
  {
  }

  void drawShape(QPainter& p)
  {
    QPointF points[6];
    points[0] = toQPointF(vec2(0, 0.5339f) * size());
    points[1] = toQPointF(vec2(0.3637f, 0.9114f) * size());
    points[2] = toQPointF(vec2(1.f, 0.2812f) * size());
    points[3] = toQPointF(vec2(0.8761f, 0.1526f) * size());
    points[4] = toQPointF(vec2(0.3637f, 0.6596f) * size());
    points[5] = toQPointF(vec2(0.1248f, 0.4063f) * size());
    p.drawPolygon(points, 6);
  }

  Shape::Ptr copyShape() const
  {
    return Ptr(new Checkmark(palette));
  }

  vec2 calcSizeRequest() override
  {
    return vec2(1);
  }
};

ImageElements::Element::Ptr createCheckmark(const ColorPalette& palette)
{
  return Checkmark::Ptr(new Checkmark(palette));
}

class Cross : public ImageElements::PathShape
{
public:
  Cross(const ColorPalette& palette)
    : PathShape(shape(), palette, 0)
  {
  }

  static QPainterPath shape()
  {
    float size = 11.f;

    QPolygonF polygon;

    mat2 m(1);
    mat2 rot90(0, -1, 1, 0);
    auto addPoint = [&polygon,&m,size](vec2 p) {
      p -= vec2(0.5f);
      p = m * p;
      p += vec2(0.5f);
      p *= size;
      polygon.append(QPointF(p.x, p.y));
    };
    auto addPoints = [&addPoint]() {
      addPoint(vec2(0.5f, 0.3333f));
      addPoint(vec2(0.1666f, 0.f));
      addPoint(vec2(0.f, 0.f));
      addPoint(vec2(0.f, 0.1666f));
    };

    vec2 startPoint(0.1666666f, 0.3333f);

    addPoint(startPoint);
    m = m * rot90;
    addPoints();
    m = m * rot90;
    addPoints();
    m = m * rot90;
    addPoints();
    m = m * rot90;
    addPoints();
    addPoint(startPoint);

    QPainterPath path;
    path.addPolygon(polygon);

    return path;
  }
};

ImageElements::Shape::Ptr createCross(const ColorPalette& palette)
{
  return Cross::Ptr(new Cross(palette));
}

class Triangle : public ImageElements::PathShape
{
public:
  Triangle(const ColorPalette& palette, int angle, const vec2& size)
    : PathShape(shape(angle, size), palette, 0)
  {
  }

  static QPainterPath shape(int angle, const vec2& size)
  {
    QPolygonF polygon;

    angle = mod(angle, 4);

    mat2 m(1);
    mat2 rot90(0, -1, 1, 0);
    auto addPoint = [&polygon,&m,size](vec2 p) {
      p -= vec2(0.5f);
      p *= size;
      p = m * p;
      p += size * vec2(0.5f);
      polygon.append(QPointF(p.x, p.y));
    };
    auto addPoints = [&addPoint]() {
      addPoint(vec2(0.5f, 0.0f));
      addPoint(vec2(0.0f, 1.0f));
      addPoint(vec2(1.0f, 1.0f));
    };

    for(int i=0; i<angle; ++i)
      m = m * rot90;
    addPoints();

    QPainterPath path;
    path.addPolygon(polygon);

    return path;
  }
};

ImageElements::Shape::Ptr createTriangle(int angle, const vec2& size, const ColorPalette& palette)
{
  return Triangle::Ptr(new Triangle(palette, angle, size));
}


} // namespace GuiThemes
} // namespace ProceduralAssets
} // namespace Framework
