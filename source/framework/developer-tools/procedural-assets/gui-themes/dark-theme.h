#ifndef FRAMEWORK_PROCEDURALASSETS_GUITHEMES_DARKTHEME_H
#define FRAMEWORK_PROCEDURALASSETS_GUITHEMES_DARKTHEME_H

#include "base-theme.h"

namespace Framework {
namespace ProceduralAssets {
namespace GuiThemes {

class DarkTheme : public BaseTheme
{
public:
  const bool largeWidgets;

  QLinearGradient buttonShininessGradient;
  QLinearGradient buttonShininessGradientHover;
  QLinearGradient buttonShininessGradientPressed;
  QBrush buttonShininessBrush;

  struct Settings
  {
    BEGIN_ENUMERATION(KeyColor,)
      GREEN,
      ORANGE,
      BLUE,
      RED
    ENUMERATION_BASIC_OPERATORS(KeyColor)
    END_ENUMERATION;

    Settings(){}

    /**
     * @brief The name of the theme
     */
    String name = "dark-theme";

    /**
     * @brief - true, if the esulting texture atlas is meant to be esthetic, ignoring memory usage.
     * - false, if it's going to be used only internal. This won't have any effect ingame, so prever using false
     */
    bool largeWidgets = false;

    /**
     * @brief The dpi factor of the theme. Use 1 for FullHD Displays and 2 for UltraHD displays.
     */
    float dpiFactor = 1.f;

    /**
     * @brief Affects the size if the widgets, without affecting the texture atlas
     */
    float metricsFactor = dpiFactor;

    vec4 backgroundColor = vec4(vec3(0.22f), 1);
    KeyColor keyColor = randomKeyColor();

    vec4 keyColorValue(float luminance) const;

    static KeyColor randomKeyColor();
  };

public:
  DarkTheme(const Settings& settings = Settings());

  void registerToCEGUI();

  ivec2 targetSize() const;

  static ivec2 smallSize();
  static ivec2 largeSize();
};

} // namespace GuiThemes
} // namespace ProceduralAssets
} // namespace Framework

#endif // FRAMEWORK_PROCEDURALASSETS_GUITHEMES_DARKTHEME_H
