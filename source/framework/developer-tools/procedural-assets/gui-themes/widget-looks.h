#ifndef FRAMEWORK_PROCEDURALASSETS_GUITHEMES_WIDGETLOOK_H
#define FRAMEWORK_PROCEDURALASSETS_GUITHEMES_WIDGETLOOK_H

#include "image-elements.h"
#include "image-slices.h"

namespace Framework {
namespace ProceduralAssets {
namespace GuiThemes {
namespace WidgetLooks {



class WidgetLook
{
public:
  class Image
  {
  public:
    ImageElements::Element::Ptr element;
    ImageSlices::ImageSlice::Ptr slice;

  public:
    Image(const ImageElements::Element::Ptr& imageElements,
          const ImageSlices::ImageSlice::Ptr& imageSlice)
      : element(imageElements),
        slice(imageSlice)
    {
    }

    Image()
    {
    }
  };

  class Text
  {
  public:
    BEGIN_ENUMERATION(VerticalAlignment,)
      TOP,
      CENTER,
      BUTTOM
    ENUMERATION_BASIC_OPERATORS(VerticalAlignment)
    CEGUI::String toCegui() const
    {
      switch(this->value)
      {
      case TOP:
        return "TopAligned";
      case CENTER:
        return "CentreAligned";
      case BUTTOM:
        return "BottomAligned";
      default:
        return "TopAligned";
      }
    }
    END_ENUMERATION;
    BEGIN_ENUMERATION(HorizontalAlignment,)
      LEFT,
      CENTER,
      RIGHT,
      JUSTIFIED,
      WORDWRAP_LEFT,
      WORDWRAP_CENTER,
      WORDWRAP_RIGHT,
      WORDWRAP_JUSTIFIED,
    ENUMERATION_BASIC_OPERATORS(HorizontalAlignment)
    CEGUI::String toCegui() const
    {
      switch(this->value)
      {
      case LEFT:
        return "LeftAligned";
      case CENTER:
        return "CentreAligned";
      case RIGHT:
        return "RightAligned";
      case JUSTIFIED:
        return "Justified";
      case WORDWRAP_LEFT:
        return "WordWrapLeftAligned";
      case WORDWRAP_CENTER:
        return "WordWrapCentreAligned";
      case WORDWRAP_RIGHT:
        return "WordWrapRightAligned";
      case WORDWRAP_JUSTIFIED:
        return "WordWrapJustified";
      default:
        return "LeftAligned";
      }
    }
    END_ENUMERATION;

    String font;
    VerticalAlignment verticalAlignment;
    HorizontalAlignment horizontalAlignment;
    ColorPalette color;
    Area::Ptr area;

    Text()
      : Text("")
    {
    }

    Text(const String& font,
         VerticalAlignment verticalAlignment=VerticalAlignment::TOP,
         HorizontalAlignment horizontalAlignment=HorizontalAlignment::LEFT,
         const ColorPalette& color=vec4(1),
         Area::Ptr area=Area::createFullArea())
      : font(font),
        verticalAlignment(verticalAlignment),
        horizontalAlignment(horizontalAlignment),
        color(color),
        area(area)
    {
    }

    static Text noText()
    {
      return Text("");
    }
  };

  class MouseCursor
  {
  public:
    String name;

    MouseCursor()
    {
    }

    MouseCursor(const String& name)
      : name(name)
    {
    }
  };

public:
  typedef std::shared_ptr<WidgetLook> Ptr;

  typedef QMap<QString, Image> ImageMap;
  typedef QMap<QString, Text> TextMap;
  typedef QMap<QString, MouseCursor> MouseCursorMap;

public:
  const String name;
  QVector<State> states;

protected:
  WidgetLook(const String& name,
             const QVector<State>& states);

public:
  virtual ~WidgetLook();

  virtual ImageMap allImages(State state) const = 0;
  virtual MouseCursorMap allCeguiMouseCursors() const {return MouseCursorMap();}

  virtual Optional<CEGUI::String> ceguiTargetType() const = 0;
  virtual Optional<CEGUI::String> ceguiRenderer() const = 0;

  void serializeToCeguiXml(CEGUI::XMLSerializer& xml, const CEGUI::String& themeName);

  TextMap allTexts() const;
  TextMap allTextsForState(State state) const;
  virtual ImageMap allImagesWithoutState() const;

protected:
  virtual TextMap _allTexts() const;
  virtual TextMap _allTextsForState(State state) const;
  virtual void prependCeguiXmlStuff_PropertyDefinition(CEGUI::XMLSerializer& xml);
  virtual void prependCeguiXmlStuff_NamedAreas(CEGUI::XMLSerializer& xml);
  virtual void prependCeguiXmlStuff_Child(CEGUI::XMLSerializer& xml);

public:
  CEGUI::String fomatImageName(const CEGUI::String& themeName,
                               const CEGUI::String& widgetName,
                               State state,
                               const CEGUI::String& sliceName);
  virtual CEGUI::String getCeguiStateName(State state) const = 0;

  static CEGUI::String composeName(const CEGUI::String& name);

  template<typename... T>
  static CEGUI::String composeName(const CEGUI::String& name, const T&... rest);

private:
  static TextMap cleanUpTextMap(TextMap t);
};

class Label final : public WidgetLook
{
public:
  typedef std::shared_ptr<Label> Ptr;

  Text label;
  Text labelShadow;

protected:
  Label(const String& name,
        const Text& label,
        const Text& labelShadow);

public:
  static Ptr create(const String& name,
                    const Text& label,
                    const Text& labelShadow);

  CEGUI::String getCeguiStateName(State state) const override;
  Optional<CEGUI::String> ceguiTargetType() const override;
  Optional<CEGUI::String> ceguiRenderer() const override;
  TextMap _allTexts() const override;
  ImageMap allImages(State state) const override;
};


class SingleImageElement : public WidgetLook
{
public:
  typedef std::shared_ptr<SingleImageElement> Ptr;

public:
  ImageElements::Element::Ptr imageElement;
  ImageSlices::ImageSlice::Ptr imageSlice;

protected:
  SingleImageElement(const String& name,
                     const ImageElements::Element::Ptr& imageElement,
                     const QVector<State>& states,
                     const ivec2& size,
                     const ImageSlices::ImageSlice::Ptr& imageSlice);

public:
  ImageMap allImages(State state) const override;
};


class Decoration : public SingleImageElement
{
public:
  typedef std::shared_ptr<Decoration> Ptr;

public:
  Decoration(const String& name,
             const ImageElements::Element::Ptr& imageElement,
             const ivec2& size,
             const ImageSlices::ImageSlice::Ptr& imageSlice);

  static Ptr create(const String& name,
                    const ImageElements::Element::Ptr& imageElement,
                    const ivec2& size,
                    const ImageSlices::ImageSlice::Ptr& imageSlice);

  CEGUI::String getCeguiStateName(State state) const override;
  Optional<CEGUI::String> ceguiTargetType() const override;
  Optional<CEGUI::String> ceguiRenderer() const override;
};


class TextBox : public WidgetLook
{
public:
  typedef std::shared_ptr<TextBox> Ptr;

  Text text;
  vec4 caretColor;
  int caretWidth;

  ImageElements::Element::Ptr backgroundImageElement;
  ImageElements::Element::Ptr selectionImageElement;
  ImageElements::Element::Ptr caretImageElement;
  ImageSlices::ImageSlice::Ptr backgroundImageSlice;
  ImageSlices::ImageSlice::Ptr selectionImageSlice;
  ImageSlices::ImageSlice::Ptr caretImageSlice;
  int padding;

  ColorPalette textColor;

  MouseCursor mouseCursor;

protected:
  TextBox(const String& name,
          const Text& text,
          const ImageElements::Element::Ptr& background,
          const ImageElements::Element::Ptr& selection,
          const ivec2& backgroundSize,
          const ivec2& selectionSize,
          const ImageSlices::ImageSlice::Ptr& backgroundImageSlice,
          const ImageSlices::ImageSlice::Ptr& selectionImageSlice,
          vec4 caretColor,
          int caretWidth,
          int padding,
          const ColorPalette& textColor,
          const MouseCursor& mouseCursor);

public:
  static Ptr create(const String& name,
                    const Text& text,
                    const ImageElements::Element::Ptr& background,
                    const ImageElements::Element::Ptr& selection,
                    const ivec2& size,
                    const ivec2& selectionSize,
                    const ImageSlices::ImageSlice::Ptr& backgroundImageSlice,
                    const ImageSlices::ImageSlice::Ptr& selectionImageSlice,
                    vec4 caretColor,
                    int caretWidth,
                    int padding,
                    const ColorPalette& textColor,
                    const MouseCursor& mouseCursor);

  CEGUI::String getCeguiStateName(State state) const override;
  Optional<CEGUI::String> ceguiTargetType() const override;
  Optional<CEGUI::String> ceguiRenderer() const override;
  ImageMap allImages(State state) const override;
  ImageMap allImagesWithoutState() const override;

  void prependCeguiXmlStuff_PropertyDefinition(CEGUI::XMLSerializer& xml) override;
  void prependCeguiXmlStuff_NamedAreas(CEGUI::XMLSerializer& xml) override;

  MouseCursorMap allCeguiMouseCursors() const override;
};


class NumberBox final : public Decoration
{
public:
  typedef std::shared_ptr<NumberBox> Ptr;

  String textBoxName, buttonUpName, buttonDownName;
  Area::Ptr textBoxArea, buttonUpArea, buttonDownArea;

  NumberBox(const String& name,
            const ImageElements::Element::Ptr& imageElement,
            const ivec2& size,
            const ImageSlices::ImageSlice::Ptr& imageSlice,
            const String& textBoxName,
            const String& buttonUpName,
            const String& buttonDownName,
            const Area::Ptr& textBoxArea,
            const Area::Ptr& buttonUpArea,
            const Area::Ptr& buttonDownArea);

  static Ptr create(const String& name,
                    const ImageElements::Element::Ptr& imageElement,
                    const ivec2& size,
                    const ImageSlices::ImageSlice::Ptr& imageSlice,
                    const String& textBoxName,
                    const String& buttonUpName,
                    const String& buttonDownName,
                    const Area::Ptr& textBoxArea,
                    const Area::Ptr& buttonUpArea,
                    const Area::Ptr& buttonDownArea);

  Optional<CEGUI::String> ceguiTargetType() const override;
  Optional<CEGUI::String> ceguiRenderer() const override;

  void prependCeguiXmlStuff_Child(CEGUI::XMLSerializer& xml) override;
};


class MouseCursor final : public SingleImageElement
{
public:
  typedef std::shared_ptr<MouseCursor> Ptr;

protected:
  MouseCursor(const String& name,
              const ImageElements::Element::Ptr& imageElement,
              const ivec2& size,
              const ImageSlices::ImageSlice::Ptr& imageSlice);

public:
  static Ptr create(const String& name,
                    const ImageElements::Element::Ptr& imageElement,
                    const ivec2& size = ivec2(0),
                    const ImageSlices::ImageSlice::Ptr& imageSlice = ImageSlices::Streched::create());

  CEGUI::String getCeguiStateName(State state) const override;
  Optional<CEGUI::String> ceguiTargetType() const override;
  Optional<CEGUI::String> ceguiRenderer() const override;
};


class Button : public SingleImageElement
{
public:
  typedef std::shared_ptr<Button> Ptr;

  Text label;
  Text labelShadow;

protected:
  Button(const String& name,
         const ImageElements::Element::Ptr& imageElement,
         const Text& text,
         const Text& textShadow,
         const ivec2& size,
         const ImageSlices::ImageSlice::Ptr& imageSlice);

public:
  static Ptr create(const String& name,
                    const ImageElements::Element::Ptr& imageElement,
                    const Text& text,
                    const Text& textShadow,
                    const ivec2& size = ivec2(0),
                    const ImageSlices::ImageSlice::Ptr& imageSlice = ImageSlices::Streched::create());

  CEGUI::String getCeguiStateName(State state) const override;
  Optional<CEGUI::String> ceguiTargetType() const override;
  Optional<CEGUI::String> ceguiRenderer() const override;
  TextMap _allTexts() const override;

  static CEGUI::String ceguiStateNameForState(State state);
  static QVector<State> statesForButton();
};


class SliderButton : public SingleImageElement
{
public:
  typedef std::shared_ptr<SliderButton> Ptr;

  bool vertical;

protected:
  SliderButton(const String& name,
               const ImageElements::Element::Ptr& imageElement,
               const ivec2& size,
               const ImageSlices::ImageSlice::Ptr& imageSlice,
               bool vertical);

public:
  static Ptr create(const String& name,
                    const ImageElements::Element::Ptr& imageElement,
                    const ivec2& size,
                    const ImageSlices::ImageSlice::Ptr& imageSlice,
                    bool vertical);

  CEGUI::String getCeguiStateName(State state) const override;
  Optional<CEGUI::String> ceguiTargetType() const override;
  Optional<CEGUI::String> ceguiRenderer() const override;

  void prependCeguiXmlStuff_PropertyDefinition(CEGUI::XMLSerializer& xml);
};


class Scrollbar : public SingleImageElement
{
public:
  typedef std::shared_ptr<Scrollbar> Ptr;

  Area::Ptr sliderArea, upButtonArea, downButtonArea;
  bool vertical;
  String sliderName, upButtonName, downButtonName;

protected:
  Scrollbar(const String& name,
            const ImageElements::Element::Ptr& imageElement,
            const ivec2& size,
            const ImageSlices::ImageSlice::Ptr& imageSlice,
            const Area::Ptr& sliderArea,
            const Area::Ptr& upButtonArea,
            const Area::Ptr& downButtonArea,
            bool vertical,
            const String& sliderName,
            const String& upButtonName,
            const String& downButtonName);

public:
  static Ptr create(const String& name,
                    const ImageElements::Element::Ptr& imageElement,
                    const ivec2& size,
                    const ImageSlices::ImageSlice::Ptr& imageSlice,
                    const Area::Ptr& sliderArea,
                    const Area::Ptr& upButtonArea,
                    const Area::Ptr& downButtonArea,
                    bool vertical,
                    const String& sliderName,
                    const String& upButtonName,
                    const String& downButtonName);

  CEGUI::String getCeguiStateName(State state) const override;
  Optional<CEGUI::String> ceguiTargetType() const override;
  Optional<CEGUI::String> ceguiRenderer() const override;

  void prependCeguiXmlStuff_PropertyDefinition(CEGUI::XMLSerializer& xml) override;
  void prependCeguiXmlStuff_NamedAreas(CEGUI::XMLSerializer& xml) override;
  void prependCeguiXmlStuff_Child(CEGUI::XMLSerializer& xml) override;
};


class ToogleButton : public Button
{
public:
  typedef std::shared_ptr<ToogleButton> Ptr;

protected:
  ToogleButton(const String& name,
               const ImageElements::Element::Ptr& imageElement,
               const Text& text,
               const Text& textShadow,
               const ivec2& size,
               const ImageSlices::ImageSlice::Ptr& imageSlice);

public:
  static Ptr create(const String& name,
                    const ImageElements::Element::Ptr& imageElement,
                    const Text& text,
                    const Text& textShadow,
                    const ivec2& size = ivec2(0),
                    const ImageSlices::ImageSlice::Ptr& imageSlice = ImageSlices::Streched::create());

  CEGUI::String getCeguiStateName(State state) const override;
  Optional<CEGUI::String> ceguiTargetType() const override;
  Optional<CEGUI::String> ceguiRenderer() const override;
};


class CheckButton : public ToogleButton
{
public:
  typedef std::shared_ptr<CheckButton> Ptr;

protected:
  CheckButton(const String& name,
              const ImageElements::Element::Ptr& imageElement,
              const Text& text,
              const Text& textShadow,
              const ivec2& size,
              const ImageSlices::ImageSlice::Ptr& imageSlice);

public:
  static Ptr create(const String& name,
                    const ImageElements::Element::Ptr& imageElement,
                    const Text& text,
                    const Text& textShadow,
                    const ivec2& size = ivec2(0),
                    const ImageSlices::ImageSlice::Ptr& imageSlice = ImageSlices::Streched::create());

  CEGUI::String getCeguiStateName(State state) const override;
};


class WindowFrame : public SingleImageElement
{
public:
  typedef std::shared_ptr<WindowFrame> Ptr;

  GuiThemes::Area::Ptr clientNoTitleNoFrame;
  GuiThemes::Area::Ptr clientNoTitleWithFrame;
  GuiThemes::Area::Ptr clientWithTitleNoFrame;
  GuiThemes::Area::Ptr clientWithTitleWithFrame;
  GuiThemes::Area::Ptr titleBarArea;
  GuiThemes::Area::Ptr closeButtonArea;

  MouseCursor mouseCursorNtoS, mouseCursorWtoE, mouseCursorNWtoSE, mouseCursorNEtoSW;

  String windowTitleBarName, windowCloseButtonName;

protected:
  WindowFrame(const String& name,
              const ImageElements::Element::Ptr& imageElement,
              const ivec2& size,
              const ImageSlices::ImageSlice::Ptr& imageSlice,
              const Area::Ptr& clientNoTitleNoFrame,
              const Area::Ptr& clientNoTitleWithFrame,
              const Area::Ptr& clientWithTitleNoFrame,
              const Area::Ptr& clientWithTitleWithFrame,
              const Area::Ptr& titleBarArea,
              const Area::Ptr& closeButtonArea,
              MouseCursor mouseCursorNtoS,
              MouseCursor mouseCursorWtoE,
              MouseCursor mouseCursorNWtoSE,
              MouseCursor mouseCursorNEtoSW,
              const String& windowTitleBarName,
              const String& windowCloseButtonName);

public:
  static Ptr create(const String& name,
                    const ImageElements::Element::Ptr& imageElement,
                    const ivec2& size,
                    const ImageSlices::ImageSlice::Ptr& imageSlice,
                    const GuiThemes::Area::Ptr& clientNoTitleNoFrame,
                    const GuiThemes::Area::Ptr& clientNoTitleWithFrame,
                    const GuiThemes::Area::Ptr& clientWithTitleNoFrame,
                    const GuiThemes::Area::Ptr& clientWithTitleWithFrame,
                    const GuiThemes::Area::Ptr& titleBarArea,
                    const GuiThemes::Area::Ptr& closeButton,
                    MouseCursor mouseCursorNtoS,
                    MouseCursor mouseCursorWtoE,
                    MouseCursor mouseCursorNWtoSE,
                    MouseCursor mouseCursorNEtoSW,
                    const String& windowTitleBarName,
                    const String& windowCloseButtonName);

  CEGUI::String getCeguiStateName(State state) const override;
  Optional<CEGUI::String> ceguiTargetType() const override;
  Optional<CEGUI::String> ceguiRenderer() const override;

  MouseCursorMap allCeguiMouseCursors() const override;

private:
  void prependCeguiXmlStuff_NamedAreas(CEGUI::XMLSerializer& xml) override;
  void prependCeguiXmlStuff_Child(CEGUI::XMLSerializer& xml) override;
};

class WindowTitleBar : public SingleImageElement
{
public:
  typedef std::shared_ptr<WindowTitleBar> Ptr;

  Text text;
  Text textShadow;

public:
  WindowTitleBar(const String& name,
                 const Text& text,
                 const Text& textShadow,
                 const ImageElements::Element::Ptr& imageElement,
                 const ivec2& size,
                 const ImageSlices::ImageSlice::Ptr& imageSlice);

public:
  static Ptr create(const String& name,
                    const Text& text,
                    const Text& textShadow,
                    const ImageElements::Element::Ptr& imageElement=ImageElements::NullElement::create(),
                    const ivec2& size = ivec2(0),
                    const ImageSlices::ImageSlice::Ptr& imageSlice = ImageSlices::Streched::create());

  CEGUI::String getCeguiStateName(State state) const override;
  Optional<CEGUI::String> ceguiTargetType() const override;
  Optional<CEGUI::String> ceguiRenderer() const override;
  TextMap _allTexts() const override;
};

class Tooltip : public SingleImageElement
{
public:
  typedef std::shared_ptr<Tooltip> Ptr;

  GuiThemes::Area::Ptr clientArea;

public:
  Tooltip(const String& name,
          const GuiThemes::Area::Ptr& clientArea,
          const ImageElements::Element::Ptr& imageElement,
          const ivec2& size,
          const ImageSlices::ImageSlice::Ptr& imageSlice);

public:
  static Ptr create(const String& name,
                    const Area::Ptr& clientArea,
                    const ImageElements::Element::Ptr& imageElement,
                    const ivec2& size = ivec2(0),
                    const ImageSlices::ImageSlice::Ptr& imageSlice = ImageSlices::Streched::create());

  CEGUI::String getCeguiStateName(State state) const override;
  Optional<CEGUI::String> ceguiTargetType() const override;
  Optional<CEGUI::String> ceguiRenderer() const override;

private:
  void prependCeguiXmlStuff_NamedAreas(CEGUI::XMLSerializer& xml) override;
};



} // namespace WidgetLooks
} // namespace GuiThemes
} // namespace ProceduralAssets
} // namespace Framework

#include "widget-looks.inl"

#endif // FRAMEWORK_PROCEDURALASSETS_GUITHEMES_WIDGETLOOK_H
