#include "joined-border.h"

namespace Framework {
namespace ProceduralAssets {
namespace GuiThemes {


JoinedBorder::JoinedBorder(bool n, bool w, bool s, bool e)
{
  this->value = NONE;
  if(n)
    this->value |= N;
  if(w)
    this->value |= W;
  if(s)
    this->value |= S;
  if(e)
    this->value |= E;
}

bool JoinedBorder::joinedN() const
{
  return this->value & N;
}

bool JoinedBorder::joinedW() const
{
  return this->value & W;
}

bool JoinedBorder::joinedS() const
{
  return this->value & S;
}

bool JoinedBorder::joinedE() const
{
  return this->value & E;
}

bool JoinedBorder::joinedCornerNW() const
{
  return this->joinedN() || this->joinedW();
}

bool JoinedBorder::joinedCornerSW() const
{
  return this->joinedS() || this->joinedW();
}

bool JoinedBorder::joinedCornerSE() const
{
  return this->joinedS() || this->joinedE();
}

bool JoinedBorder::joinedCornerNE() const
{
  return this->joinedN() || this->joinedE();
}

QVector<JoinedBorder> JoinedBorder::enumerateAllPermutations()
{
  QVector<JoinedBorder> allPermutations;
  allPermutations.reserve(16);

  for(int i=0; i<16; ++i)
    allPermutations.append(JoinedBorder::Value(i));

  return allPermutations;
}

String JoinedBorder::format() const
{
  String result;

  if(this->value & N)
    result += "n";
  if(this->value & W)
    result += "w";
  if(this->value & S)
    result += "s";
  if(this->value & E)
    result += "e";

  return result;
}

String JoinedBorder::formatPostfix() const
{
  if(this->value == NONE)
    return "";
  return ".joined-border-" + this->format();
}

} // namespace GuiThemes
} // namespace ProceduralAssets
} // namespace Framework

