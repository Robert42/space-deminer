#ifndef FRAMEWORK_PROCEDURALASSETS_GUITHEMES_PALETTE_H
#define FRAMEWORK_PROCEDURALASSETS_GUITHEMES_PALETTE_H

#include "state.h"


namespace Framework {
namespace ProceduralAssets {
namespace GuiThemes {


template<typename T>
class Palette final
{
public:
  typedef Palette<T> this_type;
  typedef std::shared_ptr<this_type> Ptr;

public:
  typedef QHash<State, T> Hash;
  typedef QHash<StateMask, T> MaskHash;
  typedef typename Hash::iterator HashIterator;
  typedef typename MaskHash::iterator MashHashIterator;

  Hash valueForState;
  MaskHash valueForStateMask;
  T fallbackValue;

public:
  Palette();
  Palette(const T& fallbackValue);

  static Ptr create(const T& fallbackValue);
  static Ptr create();

  T colorForState(State state) const;
  T operator[](State state) const;

  Palette<T>& apply(const std::function<vec4(const vec4&)>& lambda);
  Palette<T>& operator*=(const T& value);
  Palette<T> operator*(const T& value) const;
};

typedef Palette<vec4> ColorPalette;
typedef Palette<QBrush> BrushPalette;
typedef Palette<QPen> PenPalette;
typedef Palette<vec2> Vec2Palette;


} // namespace GuiThemes
} // namespace ProceduralAssets
} // namespace Framework

#include "palette.inl"

#endif // FRAMEWORK_PROCEDURALASSETS_GUITHEMES_PALETTE_H
