#ifndef FRAMEWORK_PROCEDURALASSETS_GUITHEMES_JOINEDBORDER_H
#define FRAMEWORK_PROCEDURALASSETS_GUITHEMES_JOINEDBORDER_H

#include "../texture-atlas-space-manager.h"

namespace Framework {
namespace ProceduralAssets {
namespace GuiThemes {

BEGIN_ENUMERATION(JoinedBorder,)
  NONE = 0,
  N = 1,
  W = 2,
  S = 4,
  E = 8,
  ALL = 15
ENUMERATION_BASIC_OPERATORS(JoinedBorder)
ENUMERATION_FLAG_OPERATORS(JoinedBorder)
  JoinedBorder(bool n, bool w, bool s, bool e);

  bool joinedN() const;
  bool joinedW() const;
  bool joinedS() const;
  bool joinedE() const;

  bool joinedCornerNW() const;
  bool joinedCornerSW() const;
  bool joinedCornerSE() const;
  bool joinedCornerNE() const;

  static QVector<JoinedBorder> enumerateAllPermutations();
  String format() const;
  String formatPostfix() const;
END_ENUMERATION;

} // namespace GuiThemes
} // namespace ProceduralAssets
} // namespace Framework

#endif // FRAMEWORK_PROCEDURALASSETS_GUITHEMES_JOINEDBORDER_H
