#include "base-theme.h"
#include "../texture-atlas-space-manager.h"

#include <base/io/log.h>
#include <base/color-tools.h>

#include <CEGUI/XMLSerializer.h>
#include <CEGUI/falagard/WidgetLookManager.h>
#include <CEGUI/SchemeManager.h>

namespace Framework {
namespace ProceduralAssets {
namespace GuiThemes {


BaseTheme::BaseTheme(const String& name)
  : name(name)
{
}

BaseTheme::~BaseTheme()
{
}

typedef QHash<QString, ivec2> RedudancyPreventionMap;
typedef WidgetLooks::WidgetLook::Image Image;
typedef ImageSlices::ImageSlice::Slices Slices;

class WidgetLookTextureAtlasItem : public TextureAtlasSpaceManager::Item
{
public:
  const State state;
  const WidgetLooks::WidgetLook::Ptr widgetLook;
  const Image image;
  const QString subImagePrefix;
  QPainter& painter;
  const int padding;
  RedudancyPreventionMap& redudancyMap;
  TextureAtlas::Mapping& sliceMapping;
  TextureAtlas::OffsetMapping& offsetMapping;
  const QString redudancyKey;

private:
  WidgetLookTextureAtlasItem(State state,
                             const WidgetLooks::WidgetLook::Ptr& widgetLook,
                             const Image& image,
                             const QString& subImagePrefix,
                             QPainter& painter,
                             int padding,
                             const InOutput<RedudancyPreventionMap>& redudancyMap,
                             const InOutput<TextureAtlas::Mapping>& mapping,
                             const InOutput<TextureAtlas::OffsetMapping>& offsetMapping)
    : state(state),
      widgetLook(widgetLook),
      image(image),
      subImagePrefix(subImagePrefix),
      painter(painter),
      padding(padding),
      redudancyMap(redudancyMap.value),
      sliceMapping(mapping.value),
      offsetMapping(offsetMapping.value),
      redudancyKey(image.element->exportToSvg(this->state, this->size()))
  {
  }

  ivec2 size() const override
  {
    ivec2 size =  ivec2(ceil(image.element->size())) + padding*2;

    assert(size.x >= 0);
    assert(size.y >= 0);

    return size;
  }

  bool shouldBeSkipped() const override
  {
    Optional<ivec2> pos = optionalFromHashMap(redudancyMap, redudancyKey);

    if(pos)
    {
      _setPosition(*pos);
      return true;
    }

    return false;
  }

  void setPosition(const ivec2& position) const override
  {
    painter.save();
    painter.translate(toQPoint(position+padding));
    image.element->draw(state, painter);
    painter.restore();

    redudancyMap[redudancyKey] = position;

    _setPosition(position);
  }

private:
  void _setPosition(const ivec2& position) const
  {
    const CEGUI::String widgetName = widgetLook->name.toCeguiString();
    const CEGUI::String sliceName = String::fromQString(subImagePrefix).toCeguiString();

    const QString subimageName = String::fromCeguiString(widgetLook->fomatImageName("", widgetName, state, sliceName)).toQString();

    TextureAtlas::Slice subImage = TextureAtlas::Slice(ivec2(padding), size()-padding) + ivec2(position);

    Slices slices = image.slice->slices(subImage, state);
    for(Slices::const_iterator i=slices.begin(); i!=slices.end(); ++i)
    {
      const QString scliceName = i.key().length()>0 ? subimageName + "." + i.key() : subimageName;
      sliceMapping[scliceName] = i.value();
    }

    Optional<vec2> optionalHotspot = image.element->calcHotspot();
    if(optionalHotspot)
    {
      vec2 hotspot = *optionalHotspot;
      if(hotspot.x || hotspot.y)
        offsetMapping[subimageName] = -round(hotspot);
    }
  }

public:
  static Ptr create(State state,
                    const WidgetLooks::WidgetLook::Ptr& widgetLook,
                    const Image& image,
                    const QString& subImagePrefix,
                    QPainter& painter,
                    int padding,
                    const InOutput<RedudancyPreventionMap>& redudancyMap,
                    const InOutput<TextureAtlas::Mapping>& mapping,
                    const InOutput<TextureAtlas::OffsetMapping>& offsetMapping)
  {
    return Ptr(new WidgetLookTextureAtlasItem(state, widgetLook, image, subImagePrefix, painter, padding, redudancyMap, mapping, offsetMapping));
  }
};

bool BaseTheme::drawTextureAtlas(QPainter& painter,
                                 const Output<TextureAtlas::Mapping>& mapping,
                                 const Output<TextureAtlas::OffsetMapping>& offsetMapping,
                                 const ivec2& size,
                                 int padding)
{
  RedudancyPreventionMap alreadySetAllocations;
  QVector<TextureAtlasSpaceManager::Item::Ptr> items;
  items.reserve(widgetLooks.size());

  for(const WidgetLooks::WidgetLook::Ptr& look : widgetLooks)
  {
    for(State state : look->states)
    {
      if(look->getCeguiStateName(state).empty()
         && state!=State::NONE)  // The state NONE is the only state allowing to have an empty name (used by mouse cursors)
        continue;

      WidgetLooks::WidgetLook::ImageMap allImageElements = look->allImages(state);
      for(WidgetLooks::WidgetLook::ImageMap::const_iterator i=allImageElements.begin(); i!=allImageElements.end(); i++)
      {
        const QString& subImagePrefix = i.key();
        const Image& image = i.value();

        if(image.element->isNull())
          continue;

        items.append(WidgetLookTextureAtlasItem::create(state,
                                                        look,
                                                        image,
                                                        subImagePrefix,
                                                        painter,
                                                        padding,
                                                        inout(alreadySetAllocations),
                                                        inout(mapping.value),
                                                        inout(offsetMapping.value)));
      }
    }

    WidgetLooks::WidgetLook::ImageMap allImageElements = look->allImagesWithoutState();
    for(WidgetLooks::WidgetLook::ImageMap::const_iterator i=allImageElements.begin(); i!=allImageElements.end(); i++)
    {
      const QString& subImagePrefix = i.key();
      const Image& image = i.value();

      if(image.element->isNull())
        continue;

      items.append(WidgetLookTextureAtlasItem::create(State::NONE,
                                                      look,
                                                      image,
                                                      subImagePrefix,
                                                      painter,
                                                      padding,
                                                      inout(alreadySetAllocations),
                                                      inout(mapping.value),
                                                      inout(offsetMapping.value)));
    }
  }

  TextureAtlasSpaceManager textureAtlasManager(size);

  return textureAtlasManager.allocateSpaceForMultipleItems(items, true);
}

bool BaseTheme::drawTextureAtlas(QPainter& painter,
                                 const Output<TextureAtlas::Mapping>& mapping,
                                 const ivec2& size,
                                 int padding)
{
  TextureAtlas::OffsetMapping dummyMapping;

  return drawTextureAtlas(painter, mapping, out(dummyMapping), size, padding);
}

bool BaseTheme::drawTextureAtlas(QPainter& painter,
                                 const ivec2& size,
                                 int padding)
{
  TextureAtlas::Mapping dummyMapping;

  return drawTextureAtlas(painter, out(dummyMapping), size, padding);
}

Optional<TextureAtlas> BaseTheme::createTextureAtlas(const ivec2& size,
                                                     int padding)
{
  TextureAtlas textureAtlas(name, size);
  QPainter painter(&textureAtlas.texture);

  painter.setRenderHints(QPainter::SmoothPixmapTransform|QPainter::Antialiasing);

  if(!drawTextureAtlas(painter, out(textureAtlas.mapping), out(textureAtlas.offsetMapping), size, padding))
  {
    IO::Log::logWarning("BaseTheme::createTextureAtlas: Texture Size %0 is too small for the theme %1", size, name);
    return nothing;
  }

  return textureAtlas;
}

void BaseTheme::registerToCEGUI(const ivec2& size, int padding)
{
  Optional<TextureAtlas> textureAtlas = createTextureAtlas(size, padding);

  if(!textureAtlas)
    return;

  // register texture-atlas
  textureAtlas->registerImagesToCegui();

  // register looknfeel
  {
    std::ostringstream stringStream;
    CEGUI::XMLSerializer xml(stringStream, 2);

    serializeToCeguiXml(xml);

    CEGUI::WidgetLookManager::getSingleton().parseLookNFeelSpecificationFromString(stringStream.str());
  }

  // register scheme
  {
    std::ostringstream stringStream;
    CEGUI::XMLSerializer xml(stringStream, 2);

    serializeSchemeToCeguiXml(xml);

    CEGUI::SchemeManager::getSingleton().createFromString(stringStream.str());
  }
}

void BaseTheme::serializeToCeguiXml(CEGUI::XMLSerializer& xml)
{
  const CEGUI::String themeName = this->name.toCeguiString();

  xml.openTag("Falagard");
  xml.attribute("version", "7");

  for(const WidgetLooks::WidgetLook::Ptr& look : this->widgetLooks)
  {
    look->serializeToCeguiXml(xml, themeName);
  }

  xml.closeTag();
}

void BaseTheme::serializeSchemeToCeguiXml(CEGUI::XMLSerializer& xml)
{
  const CEGUI::String themeName = this->name.toCeguiString();

  xml.openTag("GUIScheme");
  xml.attribute("version", "5");
  xml.attribute("name", themeName);

  xml.openTag("WindowRendererSet");
  xml.attribute("filename", "CEGUICoreWindowRendererSet");
  xml.closeTag(); // WindowRendererSet

  for(const WidgetLooks::WidgetLook::Ptr& look : this->widgetLooks)
  {
    if(!look->ceguiTargetType())
      continue;
    if(!look->ceguiRenderer())
      continue;

    xml.openTag("FalagardMapping");

    const CEGUI::String name = themeName+"."+look->name.toCeguiString();

    xml.attribute("windowType", name);
    xml.attribute("targetType", *look->ceguiTargetType());
    xml.attribute("renderer", *look->ceguiRenderer());
    xml.attribute("lookNFeel", name);

    xml.closeTag(); // FalagardMapping
  }

  xml.closeTag(); // GUIScheme
}

void BaseTheme::testExportingToSvg(const IO::RegularFile& file, const ivec2& size, const vec4& background)
{
  QSvgGenerator_Workaround svgGenerator;
  svgGenerator.setFileName(file.pathAsQString());
  svgGenerator.setTitle(this->name.toQString());
  svgGenerator.setViewBox(QRectF(toQPointF(vec2(0.f)), toQSize(size)));
  svgGenerator.metric(QPaintDevice::PdmDevicePixelRatio);

  QPainter painter;
  painter.begin(&svgGenerator);
  if(background.a > 0.f)
    painter.fillRect(svgGenerator.viewBox(), QBrush(toQColor(background))); // Background
  if(!this->drawTextureAtlas(painter, size))
    IO::Log::logWarning("DarkTheme Exporter: Texture Size %0 is too small for the theme %1", size, name);
  painter.end();
}

void BaseTheme::testExportingCeguiTextureAtlas(const IO::Directory& directory, const ivec2& size)
{
  Optional<TextureAtlas> resultingTextureAtlas = this->createTextureAtlas(size);

  if(!resultingTextureAtlas)
    return;

  TextureAtlas textureAtlas = resultingTextureAtlas.value();

  textureAtlas.exportToCeguiFiles(directory);
}


} // namespace GuiThemes
} // namespace ProceduralAssets
} // namespace Framework
