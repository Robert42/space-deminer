#include "texture-atlas.h"

#include <base/io/regular-file.h>
#include <base/io/log.h>

#include <CEGUI/ImageManager.h>
#include <CEGUI/BasicImage.h>
#include <CEGUI/Texture.h>
#include <CEGUI/Renderer.h>
#include <CEGUI/System.h>

namespace Framework {
namespace ProceduralAssets {
namespace GuiThemes {



class QImageDensePixelAccessor
{
private:
  QImage image;
  QVector<uint8> dataCopy;

public:
  int byteCount;
  const void* pixels;

public:
  QImageDensePixelAccessor(const QImage& image)
  {
    if(image.format() == QImage::Format_RGBA8888)
      this->image = image;
    else
      this->image = image.convertToFormat(QImage::Format_RGBA8888);

    this->byteCount = 4*this->image.width()*this->image.height();

    if(this->image.byteCount() == this->byteCount)
    {
      this->pixels = this->image.bits();
    }else
    {
      this->dataCopy.resize(this->byteCount);
      this->pixels = this->dataCopy.data();

      int height = this->image.height();
      int lineStride = 4*this->image.width();

      uint8* targetPointer = this->dataCopy.data();

      for(int i=0; i<height; ++i)
      {
        memcpy(targetPointer,
               this->image.scanLine(i),
               lineStride);
        targetPointer += lineStride;
      }
    }
  }
};



TextureAtlas::TextureAtlas(const String& name, const ivec2& size)
  : name(name),
    texture(size.x, size.y, QImage::Format_RGBA8888)
{
  // clean the image, to make sure all pixels have an alpha of 0
  texture.fill(0);

  enforceInvariant();
}


TextureAtlas::TextureAtlas()
  : TextureAtlas("")
{
}

TextureAtlas::~TextureAtlas()
{
}


void TextureAtlas::registerImagesToCegui() const
{
  checkInvariant();

  QImageDensePixelAccessor imageAccess(this->texture);
  CEGUI::Renderer& renderer = *CEGUI::System::getSingleton().getRenderer();

  const CEGUI::String ceguiName = name.toCeguiString();
  const CEGUI::Sizef textureSize(this->texture.width(),
                                 this->texture.height());

  CEGUI::Texture* ceguiTexture = &renderer.createTexture(ceguiName);

  ceguiTexture->loadFromMemory(imageAccess.pixels,
                              textureSize,
                              CEGUI::Texture::PF_RGBA);

  for(TextureAtlas::Mapping::const_iterator i=mapping.begin(); i!=mapping.end(); ++i)
  {
    QString name = i.key();
    Slice slice = i.value();
    vec2 offset = hashValue(offsetMapping, name, vec2(0));

    CEGUI::BasicImage& image = reinterpret_cast<CEGUI::BasicImage&>(CEGUI::ImageManager::getSingleton().create("BasicImage", ceguiName + "." + String::fromQString(name).toCeguiString()));
    image.setTexture(ceguiTexture);
    image.setArea(CEGUI::Rectf(slice.min().x, slice.min().y, slice.max().x, slice.max().y));
    image.setOffset(CEGUI::Vector2f(offset.x, offset.y));
  }
}


void TextureAtlas::exportToCeguiFiles(const IO::Directory& directory) const
{
  const IO::RegularFile textureFile = directory | (name + ".png");
  const IO::RegularFile imagesetFile = directory | (name + ".imageset");

  texture.save(textureFile.pathAsQString());

  exportCeguiImagesteFile(imagesetFile);
}

void TextureAtlas::exportCeguiImagesteFile(const IO::RegularFile& filename) const
{
  QFile file(filename.pathAsQString());

  if(!file.open(QFile::WriteOnly | QIODevice::Text))
  {
    IO::Log::logError("Couldn't write to file: %0", filename);
    return;
  }

  QTextStream textStream(&file);

  textStream << "<Imageset autoScaled='vertical' "
             << "imagefile='" << filename.filename().toQString() << "' "
             << "name='" << name.toQString() << "' "
             << "version='2'>\n";

  for(TextureAtlas::Mapping::const_iterator i=mapping.begin(); i!=mapping.end(); ++i)
  {
    QString name = i.key();
    Slice slice = i.value();
    vec2 offset = hashValue(offsetMapping, name, vec2(0));

    textStream << "  <Image name='" <<  name << "' "
               << "width='" << slice.width() << "' "
               << "height='" << slice.height() << "' "
               << "xpos='" << slice.min().x << "' "
               << "ypos='" << slice.min().y << "' ";
    if(offset.x != 0)
      textStream << "xOffset='" << offset.x << "' ";
    if(offset.y != 0)
      textStream << "yOffset='" << offset.y << "' ";
    textStream << "/>\n";
  }
  textStream << "</Imageset>\n";
}

void TextureAtlas::enforceInvariant()
{
  if(this->texture.format() != QImage::Format_RGBA8888)
    this->texture = this->texture.convertToFormat(QImage::Format_RGBA8888);

  checkInvariant();
}

void TextureAtlas::checkInvariant() const
{
  assert(this->texture.format() == QImage::Format_RGBA8888);
}



} // namespace GuiThemes
} // namespace ProceduralAssets
} // namespace Framework

