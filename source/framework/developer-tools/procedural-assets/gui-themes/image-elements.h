#ifndef FRAMEWORK_PROCEDURALASSETS_GUITHEMES_ELEMENT_H
#define FRAMEWORK_PROCEDURALASSETS_GUITHEMES_ELEMENT_H

#include "palette.h"

#include <base/optional.h>

namespace Framework {
namespace ProceduralAssets {
namespace GuiThemes {
namespace ImageElements {

class ElementContainer;

class Element
{
public:
  typedef std::shared_ptr<Element> Ptr;

private:
  friend class ElementContainer;
  ElementContainer* _parent;
  vec2 _position;
  vec2 _size;
  vec2 _sizeRequest;

public:
  Element();
  virtual ~Element();

  virtual void draw(State state, QPainter& painter) = 0;
  virtual void onAllocationChanged() = 0;
  virtual vec2 calcSizeRequest() = 0;
  virtual void updateSizeRequest();

  virtual bool isNull() const {return false;}

  virtual Optional<vec2> calcHotspot();

  QString exportToSvg(State state, const ivec2& size);

public:
  const vec2& position() const;
  const vec2& size() const;
  const vec2& sizeRequest() const;
  const Rectangle<vec2> allocation() const;

  void setRootSize(vec2 size);

private:
  void _setAllocation(const vec2& position, const vec2& size);

};

class NullElement final : public Element
{
public:
  void draw(State, QPainter&) final override {}
  void onAllocationChanged() final override {}
  vec2 calcSizeRequest() final override {return vec2(0);}

  bool isNull() const final override {return true;}

  static Ptr create();
};

class ElementContainer : public Element
{
public:
  typedef std::shared_ptr<ElementContainer> Ptr;

private:
  QVector<Element::Ptr> _children;

protected:
  void _addChild(const Element::Ptr& element, bool prepend=false);
  void _removeChild(const Element::Ptr& element);

  void setChildAllocation(const Element::Ptr& child, const vec2& position, const vec2& size);
  void setChildAllocation(const Element::Ptr& child, const Rectangle<vec2>& allocation);
  void updateSizeRequest() final override;
  Optional<vec2> calcHotspot() override;

public:
  ~ElementContainer();

  void draw(State state, QPainter& painter) override;
  const QVector<Element::Ptr>& children();
};

class BaseStack : public ElementContainer
{
public:
  typedef std::shared_ptr<BaseStack> Ptr;

protected:
  BaseStack();

public:
  void onAllocationChanged() override;
  vec2 calcSizeRequest() override;
};

class Stack : public BaseStack
{
public:
  typedef std::shared_ptr<Stack> Ptr;

public:
  static Ptr create();

  static Ptr create(const Element::Ptr& element);

  template<typename... T>
  static Ptr create(const Element::Ptr& element, const T&... children)
  {
    Ptr stack = Stack::create(children...);

    stack->push(element, true);

    return stack;
  }

  void push(const Element::Ptr& child, bool prepend=false);
};

class SwitchForState : public Stack
{
public:
  typedef std::shared_ptr<SwitchForState> Ptr;

public:
  static Ptr create();

  template<typename... T>
  static Ptr create(const StateMask& stateMask, const Element::Ptr& element, const T&... children)
  {
    Ptr swirchForState = SwitchForState::create(children...);

    swirchForState->insert(stateMask, element);

    return swirchForState;
  }

  void insert(const StateMask& stateMask, const Element::Ptr& child);
};

class VBox final : public ElementContainer
{
public:
  typedef std::shared_ptr<VBox> Ptr;

private:
  QHash<const Element*, real> priorities;

public:
  float spacing;

public:
  VBox();

  static Ptr create();
  static Ptr create(float spacing,
                    const std::initializer_list<Element::Ptr>& children);

  static Ptr create(float spacing)
  {
    return create(spacing, {});
  }

  template<typename... T>
  static Ptr create(float spacing, const Element::Ptr& element, const T&... children)
  {
    Ptr swirchForState = create(spacing, children...);

    swirchForState->prepend(element);

    return swirchForState;
  }

  void append(const Element::Ptr& element, real priority=1.f);
  void prepend(const Element::Ptr& element, real priority=1.f);

  void onAllocationChanged() override;
  vec2 calcSizeRequest() override;
};

class Bin : public ElementContainer
{
private:
  Element::Ptr _child;

public:
  Bin();

  void setChild(const Element::Ptr& child);
  void removeChild();

  const Element::Ptr& child();

protected:
  void setChildAllocation(const vec2& position, const vec2& size);
  void setChildAllocation(const Rectangle<vec2>& allocation);
};

class Proxy : public Bin
{
protected:
  Proxy();

  void onAllocationChanged() final override;
  vec2 calcSizeRequest() final override;
};

class StateFilter final : public Proxy
{
public:
  typedef std::shared_ptr<StateFilter> Ptr;

private:
  StateMask mask;

public:
  StateFilter(const StateMask& mask, const Element::Ptr& child);
  static Ptr create(const StateMask& mask, const Element::Ptr& child);

  void draw(State state, QPainter &painter) override;
};

class Alignment final : public Bin
{
public:
  typedef std::shared_ptr<Alignment> Ptr;

public:
  vec2 align;
  vec2 expand;

public:
  Alignment();

  static Ptr create();
  static Ptr create(const Element::Ptr& child);
  static Ptr create(const vec2& align, const Element::Ptr& child);
  static Ptr create(const vec2& align, const vec2& expand, const Element::Ptr& child);

  void onAllocationChanged() override;
  vec2 calcSizeRequest() override;
};

class Aspect final : public Bin
{
public:
  typedef std::shared_ptr<Aspect> Ptr;

public:
  vec2 aspectRatio;
  vec2 alignment;
  bool expand;

public:
  Aspect(const vec2& aspectRatio=vec2(1.f), const vec2& alignment=vec2(0.5f), bool expand=false);

  static Ptr create(const vec2& aspectRatio, const Element::Ptr& child, const vec2& alignment=vec2(0.5f), bool expand = false);
  static Ptr create(const Element::Ptr& child, bool expand = false);

  void setChildAndSetAspect(const Element::Ptr& child, bool expand = false);

  void onAllocationChanged() override;
  vec2 calcSizeRequest() override;
};

class Padding final : public Bin
{
public:
  typedef std::shared_ptr<Padding> Ptr;

public:
  real n, w, s, e;

public:
  Padding(real n, real w, real s, real e);
  Padding(real padding);

  static Ptr create(real n, real w, real s, real e);
  static Ptr create(real padding);
  static Ptr create(real n, real w, real s, real e, const Element::Ptr& child);
  static Ptr create(real padding, const Element::Ptr& child);

  void onAllocationChanged() override;
  vec2 calcSizeRequest() override;
};

class Translate final : public Bin
{
public:
  typedef std::shared_ptr<Translate> Ptr;

public:
  const Vec2Palette translation;

public:
  Translate(const Vec2Palette& translation);

  static Ptr create(const Vec2Palette& translation);
  static Ptr create(const Vec2Palette& translation, const Element::Ptr& child);

  void onAllocationChanged() override;
  vec2 calcSizeRequest() override;
  void draw(State state, QPainter& painter) override;
};

class RotateDiscrete final : public Bin
{
public:
  typedef std::shared_ptr<RotateDiscrete> Ptr;

public:
  const int rotationSteps;

public:
  RotateDiscrete(int rotationSteps);

  static Ptr create(int rotationSteps);
  static Ptr create(int rotationSteps, const Element::Ptr& child);

  void onAllocationChanged() override;
  vec2 calcSizeRequest() override;
  void draw(State state, QPainter& painter) override;
};

class SizeRequest final : public Bin
{
public:
  typedef std::shared_ptr<SizeRequest> Ptr;

public:
  vec2 newSizeRequest;

public:
  SizeRequest(const vec2& newSizeRequest);

  static Ptr create(const vec2& newSizeRequest);
  static Ptr create(const vec2& newSizeRequest, const Element::Ptr& child);

  void onAllocationChanged() final override;
  vec2 calcSizeRequest() final override;
};

class Shape : public Element
{
public:
  typedef std::shared_ptr<Shape> Ptr;

public:
  ColorPalette palette;
  real thickness;

  Optional<PenPalette> penOverride;
  Optional<BrushPalette> brushOverride;

  Qt::PenJoinStyle penJoinStyle;

public:
  Shape(const ColorPalette& palette, real thickness);

public:
  void draw(State state, QPainter& p) final override;
  void onAllocationChanged() final override;

  virtual void drawShape(QPainter& p) = 0;

  virtual Ptr copyShape() const = 0;
  Ptr copyWithDifferentPalette(const ColorPalette& palette) const;
  Ptr copyWithDifferentThickness(real thickness, const ColorPalette& palette) const;
};

class ShapeWithStrokeOffset : public Shape
{
public:
  typedef std::shared_ptr<ShapeWithStrokeOffset> Ptr;

public:
  real strokeOffset;

public:
  /**
   * @param strokeOffset the offset of the stroke.
   *     - 0 means, that the strike will touch the border of the element, and also
   *       completely included within it's bounding rectangle.
   *     - positive values mean, that, the the stroke will be drawn outside the elements bounding rectangle
   * @param palette
   * @param thickness
   * @return
   */
  ShapeWithStrokeOffset(real strokeOffset, const ColorPalette& palette, real thickness);

public:
  virtual Ptr copy() const = 0;
  Shape::Ptr copyShape() const final override;
  Ptr copyWithAdditionalStrokeOffset(real offset) const;
  Ptr copyWithAdditionalStrokeOffset(real offset, const ColorPalette& palette) const;
  Ptr copyWithAdditionalStrokeOffset(real offset, const BrushPalette& brushOverride) const;
  Ptr copyWithAdditionalStrokeOffset(real offset, const PenPalette& penOverride) const;
  Ptr copyWithAdditionalStrokeOffset(real offset, const BrushPalette& brushOverride, const PenPalette& penOverride) const;
  Ptr copyWithAdditionalStrokeOffset(real offset, const ColorPalette& palette, const BrushPalette& brushOverride) const;
  Ptr copyWithAdditionalStrokeOffset(real offset, const ColorPalette& palette, const PenPalette& penOverride) const;
  Ptr copyWithAdditionalStrokeOffset(real offset, const ColorPalette& palette, const BrushPalette& brushOverride, const PenPalette& penOverride) const;
};

class TriangleShape final : public Shape
{
public:
  typedef std::shared_ptr<TriangleShape> Ptr;

public:
  vec2 calcSizeRequest() final override;

  TriangleShape(const ColorPalette& palette);

  void drawShape(QPainter& p) override;
  Shape::Ptr copyShape() const final override;
  static Element::Ptr createWithFixedAspect(int degree, const ColorPalette& palette, real topAngle=glm::radians(90.f), const vec2& alignment=vec2(0.5f));
};

class PathShape : public Shape
{
public:
  typedef std::shared_ptr<PathShape> Ptr;

public:
  QPainterPath path;
  bool useToDetermineHotspot;

public:
  PathShape(const QPainterPath& path, const ColorPalette& palette, real thickness, bool useToDetermineHotspot=false);
  static Ptr create(const QPainterPath& path, const ColorPalette& palette, real thickness, bool useToDetermineHotspot=false);

  vec2 calcSizeRequest() final override;
  void drawShape(QPainter& p) override;
  Optional<vec2> calcHotspot() override;

  Shape::Ptr copyShape() const final override;
  QPointF offset() const;
};

class RectangleShape final : public ShapeWithStrokeOffset
{
public:
  typedef std::shared_ptr<RectangleShape> Ptr;

public:
  real rounding;

public:
  void drawShape(QPainter& p) override;
  vec2 calcSizeRequest() final override;

public:
  RectangleShape(real strokeOffset, const ColorPalette& palette, real thickness=1.f, real rounding=-INFINITY);

  static Ptr create(real strokeOffset, const ColorPalette& palette, real thickness=1.f, real rounding=-INFINITY);

  ShapeWithStrokeOffset::Ptr copy() const override;
};

class RectangleShapeWithDifferentRadii final : public ShapeWithStrokeOffset
{
public:
  typedef std::shared_ptr<RectangleShapeWithDifferentRadii> Ptr;

public:
  real roundingNW;
  real roundingSW;
  real roundingSE;
  real roundingNE;

public:
  void drawShape(QPainter& p) override;
  vec2 calcSizeRequest() final override;

public:
  RectangleShapeWithDifferentRadii(real strokeOffset,
                                   const ColorPalette& palette,
                                   real thickness,
                                   real roundingNW,
                                   real roundingSW,
                                   real roundingSE,
                                   real roundingNE);

  static Ptr create(real strokeOffset,
                    const ColorPalette& palette,
                    real thickness,
                    real roundingNW,
                    real roundingSW,
                    real roundingSE,
                    real roundingNE);
  static Ptr create(real strokeOffset,
                    const ColorPalette& palette,
                    real thickness,
                    real roundingN,
                    real roundingS);

  ShapeWithStrokeOffset::Ptr copy() const override;
};

class InverseRectangleShape final : public Shape
{
public:
  typedef std::shared_ptr<InverseRectangleShape> Ptr;

public:
  real rounding;

public:
  void drawShape(QPainter& p) override;
  vec2 calcSizeRequest() final override;

public:
  InverseRectangleShape(const ColorPalette& palette, real rounding);

  static Ptr create(const ColorPalette& palette, real rounding);

  Shape::Ptr copyShape() const override;
};

class CircleShape final : public ShapeWithStrokeOffset
{
public:
  typedef std::shared_ptr<CircleShape> Ptr;

public:
  void drawShape(QPainter& p) override;
  vec2 calcSizeRequest() final override;

public:
  CircleShape(real strokeOffset, const ColorPalette& palette, real thickness=1.f);

  static Ptr create(real strokeOffset, const ColorPalette& palette, real thickness=1.f);

  ShapeWithStrokeOffset::Ptr copy() const override;
};


Stack::Ptr createShadow(int shadowWidth,
                        const std::function<Element::Ptr(int i)>& createShadowElement);
Stack::Ptr createWithShadowUsingThickness(const Shape::Ptr& original,
                                          real shadowWidth,
                                          const ColorPalette& shadowColor);
Stack::Ptr createWithShadowUsingOffset(const ShapeWithStrokeOffset::Ptr& original,
                                       real shadowWidth,
                                       const ColorPalette& shadowColor,
                                       bool adaptStrokeOffsetOfOriginal=true);

Stack::Ptr createWitInnerhShadowUsingOffset(const ShapeWithStrokeOffset::Ptr& original,
                                            real shadowWidth,
                                            const ColorPalette& shadowColor,
                                            int offset=-1);


} // namespace Elements
} // namespace GuiThemes
} // namespace ProceduralAssets
} // namespace Framework

#endif // FRAMEWORK_PROCEDURALASSETS_GUITHEMES_ELEMENT_H
