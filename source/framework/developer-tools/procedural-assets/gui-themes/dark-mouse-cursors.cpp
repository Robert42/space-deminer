#include "dark-mouse-cursors.h"

#include <base/io/regular-file.h>

namespace Framework {
namespace ProceduralAssets {
namespace GuiThemes {
namespace MouseShape {

QPainterPath defaultMouseCursor()
{
  QPainterPath path(QPointF(0,10));

  path.lineTo(QPointF(0,25));
  path.lineTo(QPointF(8,17));
  path.lineTo(QPointF(18,17));
  path.lineTo(QPointF(0,0));

  path.closeSubpath();

  return path;
}

QPainterPath byMirroringOneQuarter(const QVector<QPointF>& points, const QTransform& transform=QTransform())
{
  int n = points.length();

  QPainterPath path(transform.map(points[0]));

  for(int i=0; i<n-1; ++i)
    path.lineTo(transform.map(QPointF(points[i].x(), points[i].y())));
  for(int i=n-1; i>0; --i)
    path.lineTo(transform.map(QPointF(-points[i].x(), points[i].y())));
  for(int i=0; i<n-1; ++i)
    path.lineTo(transform.map(QPointF(-points[i].x(), -points[i].y())));
  for(int i=n-1; i>0; --i)
    path.lineTo(transform.map(QPointF(points[i].x(), -points[i].y())));

  path.closeSubpath();

  return path;
}

QPainterPath byMirroringOneHalf(const QVector<QPointF>& points, const QTransform& transform=QTransform())
{
  int n = points.length();

  QPainterPath path(transform.map(points[0]));

  for(int i=0; i<n-1; ++i)
    path.lineTo(transform.map(QPointF(points[i].x(), points[i].y())));
  for(int i=n-1; i>0; --i)
    path.lineTo(transform.map(QPointF(-points[i].x(), points[i].y())));

  path.closeSubpath();

  return path;
}

QPainterPath textBarMouseCursor()
{
  QVector<QPointF> points = {QPointF(1.f,  0.f),
                             QPointF(1.f,  9.5f),
                             QPointF(3.f,  9.5f),
                             QPointF(3.f, 11.5f),
                             QPointF(0.f, 11.5f)};

  return byMirroringOneQuarter(points);
}

QPainterPath mouseCursor_directionArrow_Stroke(real rotateDegree=0.f)
{
  QVector<QPointF> points = {QPointF(1.f, 0.f),
                             QPointF(1.f, 5.3f),
                             QPointF(0.f, 6.0f)};

  QTransform transform;
  transform.reset();
  transform.rotate(rotateDegree);

  return byMirroringOneQuarter(points, transform);
}

QPainterPath mouseCursor_directionArrow_Cap(real rotateDegree=0.f)
{
  QVector<QPointF> points = {QPointF(0.f, 7.5f),
                             QPointF(6.f, 5.3f),
                             QPointF(0.f, 12.5f)};

  QTransform transform;
  transform.reset();
  transform.rotate(rotateDegree);

  return byMirroringOneHalf(points, transform);
}

} // namespace MouseShape

ImageElements::Element::Ptr pathShapeFor(const QPainterPath& path)
{
  auto createShape = [&path](real color, real alpha, real thickness, bool roundJoin) {
    ImageElements::PathShape::Ptr p = ImageElements::PathShape::create(path,
                                                                       vec4(vec3(color), alpha),
                                                                       thickness,
                                                                       true);

    if(roundJoin)
      p->penJoinStyle = Qt::RoundJoin;
    else
      p->penJoinStyle = Qt::MiterJoin;

    return ImageElements::Alignment::create(p);
  };

  return ImageElements::Stack::create(createShape(0, 0.1f, 5, true),
                                      createShape(0, 0.15f, 4, true),
                                      createShape(0, 0.175f, 3, true),
                                      createShape(0, 0.2f, 2, false),
                                      createShape(0, 0.5f, 0, false),
                                      createShape(1, 1.0f, 1, false));
}

#define THEME_NAME "dark-mouse-cursor"
#define DEFAULT_CURSOR "default"
#define TEXT_CURSOR "text-bar"
#define CURSOR_MOVE_VERTICAL "move-vertical"
#define CURSOR_MOVE_HORIZONTAL "move-horizontal"
#define CURSOR_MOVE_DIAGONAL_NE_TO_SW "move-ne<->sw"
#define CURSOR_MOVE_DIAGONAL_NW_TO_SE "move-nw<->se"
#define CURSOR_MOVE "move"

DarkMouseCursors::DarkMouseCursors()
  : BaseTheme(THEME_NAME)
{
  widgetLooks.append(WidgetLooks::MouseCursor::create(DEFAULT_CURSOR,
                                                      pathShapeFor(MouseShape::defaultMouseCursor()),
                                                      vec2(-1)));

  widgetLooks.append(WidgetLooks::MouseCursor::create(TEXT_CURSOR,
                                                      pathShapeFor(MouseShape::textBarMouseCursor()),
                                                      vec2(-1)));

  widgetLooks.append(WidgetLooks::MouseCursor::create(CURSOR_MOVE_VERTICAL,
                                                      pathShapeFor(MouseShape::mouseCursor_directionArrow_Cap() |
                                                                   MouseShape::mouseCursor_directionArrow_Cap(180) |
                                                                   MouseShape::mouseCursor_directionArrow_Stroke()),
                                                      vec2(-1)));

  widgetLooks.append(WidgetLooks::MouseCursor::create(CURSOR_MOVE_HORIZONTAL,
                                                      pathShapeFor(MouseShape::mouseCursor_directionArrow_Cap(90) |
                                                                   MouseShape::mouseCursor_directionArrow_Cap(270) |
                                                                   MouseShape::mouseCursor_directionArrow_Stroke(90)),
                                                      vec2(-1)));

  widgetLooks.append(WidgetLooks::MouseCursor::create(CURSOR_MOVE_DIAGONAL_NE_TO_SW,
                                                      pathShapeFor(MouseShape::mouseCursor_directionArrow_Cap(45) |
                                                                   MouseShape::mouseCursor_directionArrow_Cap(225) |
                                                                   MouseShape::mouseCursor_directionArrow_Stroke(45)),
                                                      vec2(-1)));

  widgetLooks.append(WidgetLooks::MouseCursor::create(CURSOR_MOVE_DIAGONAL_NW_TO_SE,
                                                      pathShapeFor(MouseShape::mouseCursor_directionArrow_Cap(135) |
                                                                   MouseShape::mouseCursor_directionArrow_Cap(315) |
                                                                   MouseShape::mouseCursor_directionArrow_Stroke(135)),
                                                      vec2(-1)));

  widgetLooks.append(WidgetLooks::MouseCursor::create(CURSOR_MOVE,
                                                      pathShapeFor(MouseShape::mouseCursor_directionArrow_Cap() |
                                                                   MouseShape::mouseCursor_directionArrow_Cap(180) |
                                                                   MouseShape::mouseCursor_directionArrow_Stroke()|
                                                                   MouseShape::mouseCursor_directionArrow_Cap(90) |
                                                                   MouseShape::mouseCursor_directionArrow_Cap(270) |
                                                                   MouseShape::mouseCursor_directionArrow_Stroke(90)),
                                                      vec2(-1)));
}

DarkMouseCursors::~DarkMouseCursors()
{
}

TextureAtlas DarkMouseCursors::createTextureAtlas()
{
  return *BaseTheme::createTextureAtlas(ivec2(128, 64));
}

String DarkMouseCursors::defaultCursor()
{
  return THEME_NAME "." DEFAULT_CURSOR;
}

String DarkMouseCursors::textCursor()
{
  return THEME_NAME "." TEXT_CURSOR;
}

String DarkMouseCursors::moveCursor()
{
  return THEME_NAME "." CURSOR_MOVE;
}

String DarkMouseCursors::moveVerticalCursor()
{
  return THEME_NAME "." CURSOR_MOVE_VERTICAL;
}

String DarkMouseCursors::moveHorizontalCursor()
{
  return THEME_NAME "." CURSOR_MOVE_HORIZONTAL;
}

String DarkMouseCursors::moveDiagonalNwToSeCursor()
{
  return THEME_NAME "." CURSOR_MOVE_DIAGONAL_NW_TO_SE;
}

String DarkMouseCursors::moveDiagonalNeToSwCursor()
{
  return THEME_NAME "." CURSOR_MOVE_DIAGONAL_NE_TO_SW;
}


void testExportingMouseCursor(const std::string& filename, const ivec2& size)
{
  DarkMouseCursors theme;

  theme.testExportingToSvg(IO::RegularFile(String::fromUtf8(filename)), size);
}

void testMouseCursorTextureAtlas(const std::string& directory, const ivec2& size)
{
  DarkMouseCursors theme;

  theme.testExportingCeguiTextureAtlas(IO::Directory(String::fromUtf8(directory)), size);
}


} // namespace GuiThemes
} // namespace ProceduralAssets
} // namespace Framework

