#ifndef FRAMEWORK_PROCEDURALASSETS_GUITHEMES_IMAGE_SLICES_H
#define FRAMEWORK_PROCEDURALASSETS_GUITHEMES_IMAGE_SLICES_H

#include "../texture-atlas-space-manager.h"
#include "area.h"

namespace Framework {
namespace ProceduralAssets {
namespace GuiThemes {
namespace ImageSlices {


class ImageSlice
{
public:
  typedef std::shared_ptr<ImageSlice> Ptr;
  typedef Rectangle<ivec2> Rect;
  typedef QMap<QString, Rect> Slices;

public:
  Area::Ptr area;

public:
  ImageSlice();
  virtual ~ImageSlice();

  virtual Slices slices(const Rect& whole, State state) = 0;

  virtual void addImageToImagerySection(CEGUI::XMLSerializer& xml, const CEGUI::String& name, const ivec2& size, State state) = 0;
};


class Aligned final : public ImageSlice
{
public:
  typedef std::shared_ptr<Aligned> Ptr;

  BEGIN_ENUMERATION(Horizontal,)
    LEFT,
    RIGHT,
    CENTER,
    STRECHED,
    TILED,
  ENUMERATION_BASIC_OPERATORS(Horizontal)
  END_ENUMERATION;

  BEGIN_ENUMERATION(Vertical,)
    TOP,
    BOTTOM,
    CENTER,
    STRECHED,
    TILED,
  ENUMERATION_BASIC_OPERATORS(Vertical)
  END_ENUMERATION;

  Horizontal horizontal;
  Vertical vertical;

  Aligned(Horizontal horizontal=Horizontal::CENTER, Vertical vertical=Vertical::CENTER);
  static Ptr create(Horizontal horizontal=Horizontal::CENTER, Vertical vertical=Vertical::CENTER);

  Slices slices(const Rect& whole, State state) final override;

  void addImageToImagerySection(CEGUI::XMLSerializer& xml, const CEGUI::String& name, const ivec2& size, State state) override;
};


class Streched final : public ImageSlice
{
public:
  typedef std::shared_ptr<Streched> Ptr;

  Streched();
  static Ptr create();

  Slices slices(const Rect& whole, State state) final override;

  void addImageToImagerySection(CEGUI::XMLSerializer& xml, const CEGUI::String& name, const ivec2& size, State state) override;
};


class HorizontalFrame final : public ImageSlice
{
public:
  typedef std::shared_ptr<HorizontalFrame> Ptr;

public:
  const int leftWidth, rightWidth;

  HorizontalFrame(int leftWidth, int rightWidth);

  static Ptr create(int leftWidth, int rightWidth);
  static Ptr create(int frameWidth);

  Slices slices(const Rect& whole, State state) final override;

  void addImageToImagerySection(CEGUI::XMLSerializer& xml, const CEGUI::String& name, const ivec2& size, State state) override;

private:
  static CEGUI::String convertNameToCegui(const QString& name);
};


class VerticalFrame final : public ImageSlice
{
public:
  typedef std::shared_ptr<VerticalFrame> Ptr;

public:
  const int topWidth, bottomWidth;

  VerticalFrame(int topWidth, int bottomWidth);

  static Ptr create(int topWidth, int bottomWidth);
  static Ptr create(int frameWidth);

  Slices slices(const Rect& whole, State state) final override;

  void addImageToImagerySection(CEGUI::XMLSerializer& xml, const CEGUI::String& name, const ivec2& size, State state) override;

private:
  static CEGUI::String convertNameToCegui(const QString& name);
};


class Frame final : public ImageSlice
{
public:
  typedef std::shared_ptr<Frame> Ptr;

public:
  const int n, w, s, e;

  Frame(int n, int w, int s, int e);

  static Ptr create(int n, int w, int s, int e);
  static Ptr create(int frameWidth);

  Slices slices(const Rect& whole, State state) final override;

  void addImageToImagerySection(CEGUI::XMLSerializer& xml, const CEGUI::String& name, const ivec2& size, State state) override;

private:
  static CEGUI::String convertNameToCegui(const QString& name);
};

class SwitchForStates : public ImageSlice
{
public:
  typedef std::shared_ptr<SwitchForStates> Ptr;
  typedef Palette<ImageSlice::Ptr> SlicePalette;

public:
  const SlicePalette slicesForState;

  SwitchForStates(const SlicePalette& palette);

  static Ptr create(const SlicePalette& palette);

  Slices slices(const Rect& whole, State state) final override;
  void addImageToImagerySection(CEGUI::XMLSerializer& xml, const CEGUI::String& name, const ivec2& size, State state) override;
};


} // namespace ImageSlices
} // namespace GuiThemes
} // namespace ProceduralAssets
} // namespace Framework

#endif // FRAMEWORK_PROCEDURALASSETS_GUITHEMES_IMAGE_SLICES_H
