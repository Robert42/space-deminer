#ifndef FRAMEWORK_PROCEDURALASSETS_GUITHEMES_AREA_H
#define FRAMEWORK_PROCEDURALASSETS_GUITHEMES_AREA_H

#include "palette.h"


namespace Framework {
namespace ProceduralAssets {
namespace GuiThemes {

class Area
{
private:
  class RelativeArea;
  class RelativeAreaWithBorder;

public:
  typedef std::shared_ptr<Area> Ptr;

public:
  Area();
  virtual ~Area();

  void serializeToCegui(CEGUI::XMLSerializer& xml);

protected:
  virtual void serializeToCeguiImplementation(CEGUI::XMLSerializer& xml) = 0;

public:
  static Ptr createRelativeArea(const Rectangle<vec2>& relativeSize = Rectangle<vec2>(vec2(0), vec2(1)));
  static Ptr createFullArea();
  static Ptr createRelativeAreaWithBorder(int n, int w, int s, int e, const Rectangle<vec2>& relativeSize = Rectangle<vec2>(vec2(0), vec2(1)));
  static Ptr createRelativeAreaWithBorder(int border, const Rectangle<vec2>& relativeSize = Rectangle<vec2>(vec2(0), vec2(1)));
  static Ptr createFullAreaWithBorder(int n, int w, int s, int e);
  static Ptr createFullAreaWithBorder(int border);
  static Ptr createAlignedArea(const ivec2& minSize, const vec2& relativeSize, const vec2& relativePosition, int borderN, int borderW, int borderS, int borderE);
  static Ptr createAlignedArea(const ivec2& minSize, const vec2& relativeSize, const vec2& relativePosition, int border);
};

} // namespace GuiThemes
} // namespace ProceduralAssets
} // namespace Framework

#endif // FRAMEWORK_PROCEDURALASSETS_GUITHEMES_AREA_H
