#include "area.h"

#include <CEGUI/XMLSerializer.h>

namespace Framework {
namespace ProceduralAssets {
namespace GuiThemes {


class Area::RelativeArea : public Area
{
public:
  const Rectangle<vec2> relativeArea;

  RelativeArea(const Rectangle<vec2>& relativeArea)
    : relativeArea(relativeArea)
  {
  }

  void serializeToCeguiImplementation(CEGUI::XMLSerializer& xml) final override
  {
    xml.openTag("Dim");
    xml.attribute("type", "LeftEdge");

    xml.openTag("AbsoluteDim");
    xml.attribute("value", toString(relativeArea.min().x).toCeguiString());
    xml.closeTag();

    xml.closeTag(); // Dim

    xml.openTag("Dim");
    xml.attribute("type", "TopEdge");

    xml.openTag("AbsoluteDim");
    xml.attribute("value", toString(relativeArea.min().y).toCeguiString());
    xml.closeTag();

    xml.closeTag(); // Dim

    xml.openTag("Dim");
    xml.attribute("type", "RightEdge");

    xml.openTag("UnifiedDim");
    xml.attribute("type", "Width");
    xml.attribute("scale", toString(relativeArea.max().x).toCeguiString());
    xml.closeTag();

    xml.closeTag(); // Dim

    xml.openTag("Dim");
    xml.attribute("type", "BottomEdge");

    xml.openTag("UnifiedDim");
    xml.attribute("type", "Height");
    xml.attribute("scale", toString(relativeArea.max().y).toCeguiString());
    xml.closeTag();

    xml.closeTag(); // Dim
  }
};

class Area::RelativeAreaWithBorder : public Area
{
public:
  int n, w, s, e;
  const Rectangle<vec2> relativeArea;

  RelativeAreaWithBorder(int n, int w, int s, int e, const Rectangle<vec2>& relativeArea)
    : n(n),
      w(w),
      s(s),
      e(e),
      relativeArea(relativeArea)
  {
  }

  void serializeToCeguiImplementation(CEGUI::XMLSerializer& xml) final override
  {
    xml.openTag("Dim");
    xml.attribute("type", "LeftEdge");

    xml.openTag("UnifiedDim");
    xml.attribute("type", "Width");
    xml.attribute("scale", toString(relativeArea.min().x).toCeguiString());
    xml.attribute("offset", toString(w).toCeguiString());
    xml.closeTag();

    xml.closeTag(); // Dim

    xml.openTag("Dim");
    xml.attribute("type", "TopEdge");

    xml.openTag("UnifiedDim");
    xml.attribute("type", "Height");
    xml.attribute("scale", toString(relativeArea.min().y).toCeguiString());
    xml.attribute("offset", toString(n).toCeguiString());
    xml.closeTag();

    xml.closeTag(); // Dim

    xml.openTag("Dim");
    xml.attribute("type", "RightEdge");

    xml.openTag("UnifiedDim");
    xml.attribute("type", "Width");
    xml.attribute("scale", toString(relativeArea.max().x).toCeguiString());
    xml.attribute("offset", toString(-e).toCeguiString());
    xml.closeTag();

    xml.closeTag(); // Dim

    xml.openTag("Dim");
    xml.attribute("type", "BottomEdge");

    xml.openTag("UnifiedDim");
    xml.attribute("type", "Height");
    xml.attribute("scale", toString(relativeArea.max().y).toCeguiString());
    xml.attribute("offset", toString(-s).toCeguiString());
    xml.closeTag();

    xml.closeTag(); // Dim
  }
};

class AlignedArea : public Area
{
public:
  ivec2 minSize;
  vec2 relativeSize;
  vec2 relativePosition;
  int borderN;
  int borderW;
  int borderS;
  int borderE;

  AlignedArea(const ivec2& minSize, const vec2& relativeSize, const vec2& relativePosition, int borderN, int borderW, int borderS, int borderE)
    : minSize(minSize),
      relativeSize(relativeSize),
      relativePosition(relativePosition),
      borderN(borderN),
      borderW(borderW),
      borderS(borderS),
      borderE(borderE)
  {
  }

  static void serializeSizeToCeguiImplementation(int minSize, real relativeSize, real relativePosition, int borderStart, int borderEnd, const CEGUI::String& widgetSize, CEGUI::XMLSerializer& xml)
  {
    (void) relativePosition;

    // size = minSize + (relativeSize * (widgetSize - (borderStart + borderEnd + minSize))))

    xml.openTag("OperatorDim"); // (minSize) + (relativeSize * (widgetSize - (borderStart + borderEnd))))
    xml.attribute("op", "Add");

    xml.openTag("AbsoluteDim");
    xml.attribute("value", toString(minSize).toCeguiString()); // minSize
    xml.closeTag();


    xml.openTag("OperatorDim"); // (relativeSize) * (widgetSize - (borderStart + borderEnd)))
    xml.attribute("op", "Multiply");

    xml.openTag("AbsoluteDim"); // relativeSize
    xml.attribute("value", toString(relativeSize).toCeguiString());
    xml.closeTag();

    xml.openTag("OperatorDim"); // (widgetSize) - (borderStart + borderEnd)
    xml.attribute("op", "Subtract");


    xml.openTag("WidgetDim");
    xml.attribute("dimension", widgetSize); // widgetSize
    xml.closeTag(); // WidgetDim

    xml.openTag("AbsoluteDim");
    xml.attribute("value", toString(borderStart+borderEnd+minSize).toCeguiString()); // borderStart + borderEnd
    xml.closeTag();


    xml.closeTag(); // OperatorDim
    xml.closeTag(); // OperatorDim
    xml.closeTag(); // OperatorDim
  }

  static void serializePositionToCeguiImplementation(int minSize, real relativeSize, real relativePosition, int borderStart, int borderEnd, const CEGUI::String& widgetSize, CEGUI::XMLSerializer& xml)
  {
    // x = borderStart + (relativePosition * (widgetSize - (borderStart + borderEnd + size)))

    xml.openTag("OperatorDim"); // (borderStart) + (relativePosition * (widgetSize - (borderStart + borderEnd + size)))
    xml.attribute("op", "Add");

    xml.openTag("AbsoluteDim");
    xml.attribute("value", toString(borderStart).toCeguiString()); // borderStart
    xml.closeTag();

    xml.openTag("OperatorDim"); // (relativePosition) * (widgetSize - (borderStart + borderEnd + size))
    xml.attribute("op", "Multiply");

    xml.openTag("AbsoluteDim");
    xml.attribute("value", toString(relativePosition).toCeguiString()); // relativePosition
    xml.closeTag();

    xml.openTag("OperatorDim"); // (widgetSize) - (borderStart + borderEnd + size)
    xml.attribute("op", "Subtract");

    xml.openTag("WidgetDim");
    xml.attribute("dimension", widgetSize); // widgetSize
    xml.closeTag(); // WidgetDim

    xml.openTag("OperatorDim"); // (borderStart + borderEnd) + (size)
    xml.attribute("op", "Add");

    xml.openTag("AbsoluteDim");
    xml.attribute("value", toString(borderStart+borderEnd).toCeguiString()); // borderStart + borderEnd
    xml.closeTag();

    serializeSizeToCeguiImplementation(minSize, relativeSize, relativePosition, borderStart, borderEnd, widgetSize, xml);

    xml.closeTag(); // OperatorDim
    xml.closeTag(); // OperatorDim
    xml.closeTag(); // OperatorDim
    xml.closeTag(); // OperatorDim
  }

  void serializeToCeguiImplementation(CEGUI::XMLSerializer& xml) final override
  {
    xml.openTag("Dim");
    xml.attribute("type", "LeftEdge");
    serializePositionToCeguiImplementation(minSize.x, relativeSize.x, relativePosition.x, borderW, borderE, "Width", xml);
    xml.closeTag(); // Dim

    xml.openTag("Dim");
    xml.attribute("type", "Width");
    serializeSizeToCeguiImplementation(minSize.x, relativeSize.x, relativePosition.x, borderW, borderE, "Width", xml);
    xml.closeTag(); // Dim

    xml.openTag("Dim");
    xml.attribute("type", "TopEdge");
    serializePositionToCeguiImplementation(minSize.y, relativeSize.y, relativePosition.y, borderN, borderS, "Height", xml);
    xml.closeTag(); // Dim

    xml.openTag("Dim");
    xml.attribute("type", "Height");
    serializeSizeToCeguiImplementation(minSize.y, relativeSize.y, relativePosition.y, borderN, borderS, "Height", xml);
    xml.closeTag(); // Dim
  }
};


Area::Area()
{
}

Area::~Area()
{
}

void Area::serializeToCegui(CEGUI::XMLSerializer& xml)
{
  xml.openTag("Area");
  this->serializeToCeguiImplementation(xml);
  xml.closeTag(); // Area
}

Area::Ptr Area::createRelativeArea(const Rectangle<vec2>& relativeSize)
{
  return Ptr(new RelativeArea(relativeSize));
}

Area::Ptr Area::createFullArea()
{
  return createRelativeArea();
}

Area::Ptr Area::createRelativeAreaWithBorder(int n, int w, int s, int e, const Rectangle<vec2>& relativeSize)
{
  if(n==0 && w==0 && s==0 && e==0)
    return createRelativeArea(relativeSize);

  return Ptr(new RelativeAreaWithBorder(n, w, s, e, relativeSize));
}

Area::Ptr Area::createRelativeAreaWithBorder(int border, const Rectangle<vec2>& relativeSize)
{
  return createRelativeAreaWithBorder(border, border, border, border, relativeSize);
}

Area::Ptr Area::createFullAreaWithBorder(int n, int w, int s, int e)
{
  return createRelativeAreaWithBorder(n, w, s, e);
}

Area::Ptr Area::createFullAreaWithBorder(int border)
{
  return createFullAreaWithBorder(border, border, border, border);
}

Area::Ptr Area::createAlignedArea(const ivec2& minSize, const vec2& relativeSize, const vec2& relativePosition, int borderN, int borderW, int borderS, int borderE)
{
  return Ptr(new AlignedArea(minSize, relativeSize, relativePosition, borderN, borderW, borderS, borderE));
}

Area::Ptr Area::createAlignedArea(const ivec2& minSize, const vec2& relativeSize, const vec2& relativePosition, int border)
{
  return createAlignedArea(minSize, relativeSize, relativePosition, border, border, border, border);
}


} // namespace GuiThemes
} // namespace ProceduralAssets
} // namespace Framework

