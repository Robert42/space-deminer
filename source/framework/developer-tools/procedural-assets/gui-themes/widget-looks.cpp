#include "widget-looks.h"
#include "area.h"

#include <base/color-tools.h>

#include <CEGUI/XMLSerializer.h>

namespace Framework {
namespace ProceduralAssets {
namespace GuiThemes {
namespace WidgetLooks {

void setChildOutsideClientArea(CEGUI::XMLSerializer& xml);


WidgetLook::WidgetLook(const String& name, const QVector<State>& states)
  : name(name),
    states(states)
{
}

WidgetLook::~WidgetLook()
{
}

void WidgetLook::serializeToCeguiXml(CEGUI::XMLSerializer& xml, const CEGUI::String& themeName)
{
  CEGUI::String widgetName = composeName(themeName, this->name.toCeguiString());

  xml.openTag("WidgetLook");
  xml.attribute("name", widgetName);

  prependCeguiXmlStuff_PropertyDefinition(xml);

  WidgetLooks::WidgetLook::MouseCursorMap allMouseCursors = this->allCeguiMouseCursors();
  for(WidgetLooks::WidgetLook::MouseCursorMap::const_iterator i=allMouseCursors.begin(); i!=allMouseCursors.end(); i++)
  {
    const QString& key = i.key();
    const MouseCursor& mouseCursor = i.value();

    xml.openTag("Property");
    xml.attribute("name", String::fromQString(key).toCeguiString());
    xml.attribute("value", mouseCursor.name.toCeguiString());
    xml.closeTag(); // Property
  }

  prependCeguiXmlStuff_NamedAreas(xml);
  prependCeguiXmlStuff_Child(xml);

  ImageMap imagesWithoutState = this->allImagesWithoutState();
  for(WidgetLooks::WidgetLook::ImageMap::const_iterator i=imagesWithoutState.begin(); i!=imagesWithoutState.end(); i++)
  {
    CEGUI::String name = String::fromQString(i.key()).toCeguiString();
    const Image& image = i.value();

    xml.openTag("ImagerySection");
    xml.attribute("name", name);

    CEGUI::String imageName = composeName(widgetName, name);
    image.slice->addImageToImagerySection(xml, imageName, image.element->size(), State::NONE);

    xml.closeTag(); // ImagerySection
  }

  for(State state : states)
  {
    if(getCeguiStateName(state).empty())
      continue;

    const CEGUI::String stateName = getCeguiStateName(state);
    const CEGUI::String imageryName = stateName + "_imagery";

    xml.openTag("ImagerySection");
    xml.attribute("name", imageryName);

    WidgetLooks::WidgetLook::ImageMap allImageElements = this->allImages(state);
    for(WidgetLooks::WidgetLook::ImageMap::const_iterator i=allImageElements.begin(); i!=allImageElements.end(); i++)
    {
      const QString& subImageName = i.key();
      const Image& image = i.value();

      if(image.element->isNull())
        continue;

      CEGUI::String imageName = composeName(widgetName, stateName, String::fromQString(subImageName).toCeguiString());
      image.slice->addImageToImagerySection(xml, imageName, image.element->size(), state);
    }

    xml.closeTag(); // ImagerySection
  }

  TextMap allTexts = this->allTexts();
  for(WidgetLooks::WidgetLook::TextMap::const_iterator i=allTexts.begin(); i!=allTexts.end(); i++)
  {
    const CEGUI::String& textName = String::fromQString(i.key()).toCeguiString();
    const Text& text = i.value();

    xml.openTag("ImagerySection");
    xml.attribute("name", textName);
    xml.openTag("TextComponent");

    text.area->serializeToCegui(xml);

    xml.openTag("VertFormat");
    xml.attribute("type", text.verticalAlignment.toCegui());
    xml.closeTag(); // VertFormat
    xml.openTag("HorzFormat");
    xml.attribute("type", text.horizontalAlignment.toCegui());
    xml.closeTag(); // HorzFormat

    xml.closeTag(); // TextComponent
    xml.closeTag(); // ImagerySection
  }

  for(State state : states)
  {
    if(getCeguiStateName(state).empty())
      continue;

    const CEGUI::String stateName = getCeguiStateName(state);
    const CEGUI::String imageryName = stateName + "_imagery";

    xml.openTag("StateImagery");
    xml.attribute("name", getCeguiStateName(state));

    xml.openTag("Layer");
    xml.openTag("Section");
    xml.attribute("look", widgetName);
    xml.attribute("section", imageryName);
    xml.closeTag(); // Section

    TextMap allTexts = this->allTextsForState(state);
    for(WidgetLooks::WidgetLook::TextMap::const_iterator i=allTexts.begin(); i!=allTexts.end(); i++)
    {
      CEGUI::String name = String::fromQString(i.key()).toCeguiString();
      const Text& text = i.value();

      vec4 color = text.color[state];

      if(color.a==0.f)
        continue;

      xml.openTag("Section");
      xml.attribute("section", name);

      CEGUI::String colorCode = colorToHexCode(color.argb()).toCeguiString();
      xml.openTag("Colours");
      xml.attribute("topLeft", colorCode);
      xml.attribute("topRight", colorCode);
      xml.attribute("bottomLeft", colorCode);
      xml.attribute("bottomRight", colorCode);
      xml.closeTag(); // Colours

      xml.closeTag(); // Section
    }
    xml.closeTag(); // Layer

    xml.closeTag(); // StateImagery
  }

  xml.closeTag(); // WidgetLook
}

WidgetLook::TextMap WidgetLook::_allTexts() const
{
  return TextMap();
}

WidgetLook::TextMap WidgetLook::_allTextsForState(State) const
{
  return _allTexts();
}

void WidgetLook::prependCeguiXmlStuff_PropertyDefinition(CEGUI::XMLSerializer&)
{
}

void WidgetLook::prependCeguiXmlStuff_NamedAreas(CEGUI::XMLSerializer&)
{
}

void WidgetLook::prependCeguiXmlStuff_Child(CEGUI::XMLSerializer&)
{
}

CEGUI::String WidgetLook::fomatImageName(const CEGUI::String& themeName,
                                         const CEGUI::String& widgetName,
                                         State state,
                                         const CEGUI::String& sliceName)
{
  CEGUI::String stateName = getCeguiStateName(state);

  return composeName(themeName, widgetName, stateName, sliceName);
}

CEGUI::String WidgetLook::composeName(const CEGUI::String& name)
{
  return name;
}

WidgetLook::TextMap WidgetLook::allTexts() const
{
  return cleanUpTextMap(_allTexts());
}

WidgetLook::TextMap WidgetLook::allTextsForState(State state) const
{
  return cleanUpTextMap(_allTextsForState(state));
}

WidgetLook::ImageMap WidgetLook::allImagesWithoutState() const
{
  return ImageMap();
}

WidgetLook::TextMap WidgetLook::cleanUpTextMap(TextMap t)
{
  for(WidgetLooks::WidgetLook::TextMap::iterator i=t.begin(); i!=t.end();)
  {
    if(i->font.empty())
      i = t.erase(i);
    else
      ++i;
  }

  return t;
}


// ==== Label ====


Label::Label(const String& name,
             const Text& label,
             const Text& labelShadow)
  : WidgetLook(name,
               QVector<State>({State::NORMAL,
                               State::INSENSITIVE})),
    label(label),
    labelShadow(labelShadow)
{
}

Label::Ptr Label::create(const String& name,
                         const Text& label,
                         const Text& labelShadow)
{
  return Ptr(new Label(name,
                       label,
                       labelShadow));
}

CEGUI::String Label::getCeguiStateName(State state) const
{
  switch(State::underlying_type(state.value))
  {
  case State::NORMAL:
    return "Enabled";
  case State::INSENSITIVE:
    return "Disabled";
  default:
      return "";
  }
}

Optional<CEGUI::String> Label::ceguiTargetType() const
{
  return CEGUI::String("DefaultWindow");
}

Optional<CEGUI::String> Label::ceguiRenderer() const
{
  return CEGUI::String("Core/Default");
}

Label::TextMap Label::_allTexts() const
{
  TextMap map;

  map["0-label-shadow"] = this->labelShadow;
  map["1-label"] = this->label;

  return map;
}

Label::ImageMap Label::allImages(State) const
{
  return ImageMap();
}


// ==== SingleImageElement ====


SingleImageElement::SingleImageElement(const String& name,
                                       const ImageElements::Element::Ptr& imageElement,
                                       const QVector<State>& states,
                                       const ivec2& size,
                                       const ImageSlices::ImageSlice::Ptr& imageSlice)
  : WidgetLook(name,
               states),
    imageElement(imageElement),
    imageSlice(imageSlice)
{
  imageElement->setRootSize(size);
}

WidgetLook::ImageMap SingleImageElement::allImages(State) const
{
  ImageMap map;

  map[""] = Image(imageElement, imageSlice);

  return map;
}


// ==== TextBox ====


Decoration::Decoration(const String& name,
                       const ImageElements::Element::Ptr& imageElement,
                       const ivec2& size,
                       const ImageSlices::ImageSlice::Ptr& imageSlice)
  : SingleImageElement(name,
                       imageElement,
                       QVector<State>({State::NORMAL, State::INSENSITIVE}),
                       size,
                       imageSlice)
{
}

Decoration::Ptr Decoration::create(const String& name,
                                   const ImageElements::Element::Ptr& imageElement,
                                   const ivec2& size,
                                   const ImageSlices::ImageSlice::Ptr& imageSlice)
{
  return Ptr(new Decoration(name, imageElement, size, imageSlice));
}

CEGUI::String Decoration::getCeguiStateName(State state) const
{
  switch(state.value)
  {
  case State::NORMAL:
    return "Enabled";
  case State::INSENSITIVE:
    return "Disabled";
  default:
    return "";
  }
}

Optional<CEGUI::String> Decoration::ceguiTargetType() const
{
  return CEGUI::String("DefaultWindow");
}

Optional<CEGUI::String> Decoration::ceguiRenderer() const
{
  return CEGUI::String("Core/Default");
}


// ==== TextBox ====


TextBox::TextBox(const String& name,
                 const Text& text,
                 const ImageElements::Element::Ptr& background,
                 const ImageElements::Element::Ptr& selection,
                 const ivec2& size,
                 const ivec2& selectionSize,
                 const ImageSlices::ImageSlice::Ptr& backgroundImageSlice,
                 const ImageSlices::ImageSlice::Ptr& selectionImageSlice,
                 vec4 caretColor,
                 int caretWidth,
                 int padding,
                 const ColorPalette& textColor,
                 const MouseCursor& mouseCursor)
  : WidgetLook(name,
               QVector<State>({State::NORMAL, State::INSENSITIVE, State::TEXTBOX_READONLY, State::TEXTBOX_SELECTION, State::TEXTBOX_FOCUSED_SELECTION})),
    text(text),
    caretColor(caretColor),
    caretWidth(caretWidth),
    backgroundImageElement(background),
    selectionImageElement(selection),
    caretImageElement(ImageElements::RectangleShape::create(0, caretColor, 0)),
    backgroundImageSlice(backgroundImageSlice),
    selectionImageSlice(selectionImageSlice),
    caretImageSlice(ImageSlices::VerticalFrame::create(1)),
    padding(padding),
    textColor(textColor),
    mouseCursor(mouseCursor)
{
  Area::Ptr caretArea = Area::createAlignedArea(ivec2(caretWidth, 0), vec2(0, 1), vec2(0), 0, -1, 0, 0);

  backgroundImageElement->setRootSize(size);
  selectionImageElement->setRootSize(selectionSize);
  caretImageElement->setRootSize(ivec2(caretWidth, backgroundImageElement->size().y-padding*2));

  caretImageSlice->area = caretArea;
}

TextBox::Ptr TextBox::create(const String& name,
                             const Text& text,
                             const ImageElements::Element::Ptr& background,
                             const ImageElements::Element::Ptr& selection,
                             const ivec2& size,
                             const ivec2& selectionSize,
                             const ImageSlices::ImageSlice::Ptr& backgroundImageSlice,
                             const ImageSlices::ImageSlice::Ptr& selectionImageSlice,
                             vec4 caretColor,
                             int caretWidth,
                             int padding,
                             const ColorPalette& textColor,
                             const MouseCursor& mouseCursor)
{
  return Ptr(new TextBox(name,
                         text,
                         background,
                         selection,
                         size,
                         selectionSize,
                         backgroundImageSlice,
                         selectionImageSlice,
                         caretColor,
                         caretWidth,
                         padding,
                         textColor,
                         mouseCursor));
}

CEGUI::String TextBox::getCeguiStateName(State state) const
{
  switch(State::underlying_type(state.value))
  {
  case State::NORMAL:
    return "Enabled";
  case State::INSENSITIVE:
    return "Disabled";
  case State::TEXTBOX_READONLY:
    return "ReadOnly";
  case State::TEXTBOX_SELECTION:
    return "InactiveSelection";
  case State::TEXTBOX_FOCUSED_SELECTION:
    return "ActiveSelection";
  default:
      return "";
  }
}

TextBox::ImageMap TextBox::allImages(State state) const
{
  ImageMap m;

  if(state == State::TEXTBOX_SELECTION || state == State::TEXTBOX_FOCUSED_SELECTION)
    m["selection"] = Image(selectionImageElement, selectionImageSlice);
  else
    m["background"] = Image(backgroundImageElement, backgroundImageSlice);

  return m;
}

TextBox::ImageMap TextBox::allImagesWithoutState() const
{
  ImageMap m;
  m["Caret"] = Image(caretImageElement, caretImageSlice);
  return m;
}

Optional<CEGUI::String> TextBox::ceguiTargetType() const
{
  return CEGUI::String("CEGUI/Editbox");
}

Optional<CEGUI::String> TextBox::ceguiRenderer() const
{
  return CEGUI::String("Core/Editbox");
}

void TextBox::prependCeguiXmlStuff_PropertyDefinition(CEGUI::XMLSerializer& xml)
{
  xml.openTag("PropertyDefinition");
  xml.attribute("name", "NormalTextColour");
  xml.attribute("initialValue", colorToHexCode(textColor[State::NORMAL].argb()).toCeguiString());
  xml.attribute("redrawOnWrite", "true");
  xml.attribute("type", "ColourRect");
  xml.closeTag();

  xml.openTag("PropertyDefinition");
  xml.attribute("name", "SelectedTextColour");
  xml.attribute("initialValue", colorToHexCode(textColor[State::TEXTBOX_SELECTION].argb()).toCeguiString());
  xml.attribute("redrawOnWrite", "true");
  xml.attribute("type", "ColourRect");
  xml.closeTag();
}

void TextBox::prependCeguiXmlStuff_NamedAreas(CEGUI::XMLSerializer& xml)
{
  Area::Ptr textArea = Area::createFullAreaWithBorder(padding);

  xml.openTag("NamedArea");
  xml.attribute("name", "TextArea");
  textArea->serializeToCegui(xml);
  xml.closeTag(); // NamedArea
}

TextBox::MouseCursorMap TextBox::allCeguiMouseCursors() const
{
  MouseCursorMap m;

  m["MouseCursorImage"] = this->mouseCursor;

  return m;
}


// ==== NumberBox ====

NumberBox::NumberBox(const String& name,
                     const ImageElements::Element::Ptr& imageElement,
                     const ivec2& size,
                     const ImageSlices::ImageSlice::Ptr& imageSlice,
                     const String& textBoxName,
                     const String& buttonUpName,
                     const String& buttonDownName,
                     const Area::Ptr& textBoxArea,
                     const Area::Ptr& buttonUpArea,
                     const Area::Ptr& buttonDownArea)
  : Decoration(name,
               imageElement,
               size,
               imageSlice),
    textBoxName(textBoxName),
    buttonUpName(buttonUpName),
    buttonDownName(buttonDownName),
    textBoxArea(textBoxArea),
    buttonUpArea(buttonUpArea),
    buttonDownArea(buttonDownArea)
{
}

NumberBox::Ptr NumberBox::create(const String& name,
                                 const ImageElements::Element::Ptr& imageElement,
                                 const ivec2& size,
                                 const ImageSlices::ImageSlice::Ptr& imageSlice,
                                 const String& textBoxName,
                                 const String& buttonUpName,
                                 const String& buttonDownName,
                                 const Area::Ptr& textBoxArea,
                                 const Area::Ptr& buttonUpArea,
                                 const Area::Ptr& buttonDownArea)
{
  return Ptr(new NumberBox(name,
                           imageElement,
                           size,
                           imageSlice,
                           textBoxName,
                           buttonUpName,
                           buttonDownName,
                           textBoxArea,
                           buttonUpArea,
                           buttonDownArea));
}

Optional<CEGUI::String> NumberBox::ceguiTargetType() const
{
  return CEGUI::String("CEGUI/Spinner");
}

Optional<CEGUI::String> NumberBox::ceguiRenderer() const
{
  return CEGUI::String("Core/Default");
}

void NumberBox::prependCeguiXmlStuff_Child(CEGUI::XMLSerializer& xml)
{
  xml.openTag("Child");
  xml.attribute("nameSuffix", "__auto_editbox__");
  xml.attribute("type", this->textBoxName.toCeguiString());
  textBoxArea->serializeToCegui(xml);
  xml.closeTag(); // Child

  xml.openTag("Child");
  xml.attribute("nameSuffix", "__auto_incbtn__");
  xml.attribute("type", this->buttonUpName.toCeguiString());
  buttonUpArea->serializeToCegui(xml);
  xml.closeTag(); // Child

  xml.openTag("Child");
  xml.attribute("nameSuffix", "__auto_decbtn__");
  xml.attribute("type", this->buttonDownName.toCeguiString());
  buttonDownArea->serializeToCegui(xml);
  xml.closeTag(); // Child
}

// ==== MouseCursor ====


MouseCursor::MouseCursor(const String& name,
                         const ImageElements::Element::Ptr& imageElement,
                         const ivec2& size,
                         const ImageSlices::ImageSlice::Ptr& imageSlice)
  : SingleImageElement(name,
                       imageElement,
                       QVector<State>({State::NONE}),
                       size,
                       imageSlice)
{
}

MouseCursor::Ptr MouseCursor::create(const String& name,
                                     const ImageElements::Element::Ptr& imageElement,
                                     const ivec2& size,
                                     const ImageSlices::ImageSlice::Ptr& imageSlice)
{
  return Ptr(new MouseCursor(name,
                             imageElement,
                             size,
                             imageSlice));
}

CEGUI::String MouseCursor::getCeguiStateName(State state) const
{
  (void)state;
  return "";
}

Optional<CEGUI::String> MouseCursor::ceguiTargetType() const
{
  return nothing;
}

Optional<CEGUI::String> MouseCursor::ceguiRenderer() const
{
  return nothing;
}

// ==== Button ====

Button::Button(const String& name,
               const ImageElements::Element::Ptr& imageElement,
               const Text& text,
               const Text& textShadow,
               const ivec2& size,
               const ImageSlices::ImageSlice::Ptr& imageSlice)
  : SingleImageElement(name,
                       imageElement,
                       Button::statesForButton(),
                       size,
                       imageSlice),
    label(text),
    labelShadow(textShadow)
{
}

Button::Ptr Button::create(const String& name,
                           const ImageElements::Element::Ptr& imageElement,
                           const Text& text,
                           const Text& textShadow,
                           const ivec2& size,
                           const ImageSlices::ImageSlice::Ptr& imageSlice)
{
  return Ptr(new Button(name,
                        imageElement,
                        text,
                        textShadow,
                        size,
                        imageSlice));
}

QVector<State> Button::statesForButton()
{
  return QVector<State>({State::NORMAL,
                         State::INSENSITIVE,
                         State::HOVER,
                         State::FOCUSED,
                         State::HOVER | State::FOCUSED,
                         State::PRESSED,
                         State::PRESSED | State::BUTTON_OUTSIDE_WIDGET});
}

WidgetLook::TextMap Button::_allTexts() const
{
  TextMap map;

  map["0-label-shadow"] = this->labelShadow;
  map["1-label"] = this->label;

  return map;
}

CEGUI::String Button::ceguiStateNameForState(State state)
{
  switch(State::underlying_type(state.value))
  {
  case State::NORMAL:
    return "Normal";
  case State::INSENSITIVE:
    return "Disabled";
  case State::HOVER:
    return "Hover";
  case State::PRESSED:
    return "Pushed";
  case State::PRESSED | State::BUTTON_OUTSIDE_WIDGET:
    return "PushedOff";
  case State::FOCUSED: // CEGUI doesn't suport this state
  default:
      return "";
  }
}

CEGUI::String Button::getCeguiStateName(State state) const
{
  return Button::ceguiStateNameForState(state);
}

Optional<CEGUI::String> Button::ceguiTargetType() const
{
  return CEGUI::String("CEGUI/PushButton");
}

Optional<CEGUI::String> Button::ceguiRenderer() const
{
  return CEGUI::String("Core/Button");
}


// ==== SliderButton ====


SliderButton::SliderButton(const String& name,
                           const ImageElements::Element::Ptr& imageElement,
                           const ivec2& size,
                           const ImageSlices::ImageSlice::Ptr& imageSlice,
                          bool vertical)
  : SingleImageElement(name,
                       imageElement,
                       Button::statesForButton(),
                       size,
                       imageSlice),
    vertical(vertical)
{
}

SliderButton::Ptr SliderButton::create(const String& name,
                                       const ImageElements::Element::Ptr& imageElement,
                                       const ivec2& size,
                                       const ImageSlices::ImageSlice::Ptr& imageSlice,
                                       bool vertical)
{
  return Ptr(new SliderButton(name, imageElement, size, imageSlice, vertical));
}

CEGUI::String SliderButton::getCeguiStateName(State state) const
{
  return Button::ceguiStateNameForState(state);
}

Optional<CEGUI::String> SliderButton::ceguiTargetType() const
{
  return CEGUI::String("CEGUI/Thumb");
}

Optional<CEGUI::String> SliderButton::ceguiRenderer() const
{
  return CEGUI::String("Core/Button");
}

void SliderButton::prependCeguiXmlStuff_PropertyDefinition(CEGUI::XMLSerializer& xml)
{
  xml.openTag("Property");
  xml.attribute("name", "VertFree");
  xml.attribute("value", this->vertical ? "true" : "false");
  xml.closeTag();

  xml.openTag("Property");
  xml.attribute("name", "HorzFree");
  xml.attribute("value", this->vertical ? "true" : "false");
  xml.closeTag();
}


// ==== Scrollbar ====

Scrollbar::Scrollbar(const String& name,
                     const ImageElements::Element::Ptr& imageElement,
                     const ivec2& size,
                     const ImageSlices::ImageSlice::Ptr& imageSlice,
                     const Area::Ptr& sliderArea,
                     const Area::Ptr& upButtonArea,
                     const Area::Ptr& downButtonArea,
                     bool vertical,
                     const String& sliderName,
                     const String& upButtonName,
                     const String& downButtonName)
  : SingleImageElement(name,
                       imageElement,
                       QVector<State>({State::NORMAL, State::INSENSITIVE}),
                       size,
                       imageSlice),
    sliderArea(sliderArea),
    upButtonArea(upButtonArea),
    downButtonArea(downButtonArea),
    vertical(vertical),
    sliderName(sliderName),
    upButtonName(upButtonName),
    downButtonName(downButtonName)
{
}


Scrollbar::Ptr Scrollbar::create(const String& name,
                                 const ImageElements::Element::Ptr& imageElement,
                                 const ivec2& size,
                                 const ImageSlices::ImageSlice::Ptr& imageSlice,
                                 const Area::Ptr& sliderArea,
                                 const Area::Ptr& upButtonArea,
                                 const Area::Ptr& downButtonArea,
                                 bool vertical,
                                 const String& sliderName,
                                 const String& upButtonName,
                                 const String& downButtonName)
{
  return Ptr(new Scrollbar(name,
                           imageElement,
                           size,
                           imageSlice,
                           sliderArea,
                           upButtonArea,
                           downButtonArea,
                           vertical,
                           sliderName,
                           upButtonName,
                           downButtonName));
}

CEGUI::String Scrollbar::getCeguiStateName(State state) const
{
  switch(State::underlying_type(state.value))
  {
  case State::NORMAL:
    return "Enabled";
  case State::INSENSITIVE:
    return "Disabled";
  default:
      return "";
  }
}

Optional<CEGUI::String> Scrollbar::ceguiTargetType() const
{
  return CEGUI::String("CEGUI/Scrollbar");
}

Optional<CEGUI::String> Scrollbar::ceguiRenderer() const
{
  return CEGUI::String("Core/Scrollbar");
}

void Scrollbar::prependCeguiXmlStuff_PropertyDefinition(CEGUI::XMLSerializer& xml)
{
  xml.openTag("Property");
  xml.attribute("name", "VerticalScrollbar");
  xml.attribute("value", this->vertical ? "true" : "false");
  xml.closeTag();
}

void Scrollbar::prependCeguiXmlStuff_NamedAreas(CEGUI::XMLSerializer& xml)
{
  xml.openTag("NamedArea");
  xml.attribute("name", "ThumbTrackArea");
  this->sliderArea->serializeToCegui(xml);
  xml.closeTag(); // NamedArea
}

void Scrollbar::prependCeguiXmlStuff_Child(CEGUI::XMLSerializer& xml)
{
  xml.openTag("Child");
  xml.attribute("nameSuffix", "__auto_thumb__");
  xml.attribute("type", this->sliderName.toCeguiString());

  xml.openTag("Area");
  xml.openTag("Dim");
  xml.attribute("type", vertical ? "Height" : "Width");
  xml.openTag("UnifiedDim");
  xml.attribute("type", vertical ? "Height" : "Width");
  xml.attribute("scale", "0.125");
  xml.closeTag(); // UnifiedDim
  xml.closeTag(); // Dim
  xml.closeTag(); // Area

  xml.openTag("Property");
  xml.attribute("name", "MinSize");
  vec2 minSize = vertical ? vec2(0, this->imageElement->size().y) : vec2(this->imageElement->size().x, 0);
  xml.attribute("value", String::compose("{{0, %0},{0,%1}}", minSize.x, minSize.y).toCeguiString());
  xml.closeTag(); // Property

  xml.closeTag(); // Child

  xml.openTag("Child");
  xml.attribute("nameSuffix", "__auto_incbtn__");
  xml.attribute("type", this->upButtonName.toCeguiString());
  this->upButtonArea->serializeToCegui(xml);
  xml.closeTag(); // Child

  xml.openTag("Child");
  xml.attribute("nameSuffix", "__auto_decbtn__");
  xml.attribute("type", this->downButtonName.toCeguiString());
  this->downButtonArea->serializeToCegui(xml);
  xml.closeTag(); // Child
}

// ==== ToogleButton ====


ToogleButton::ToogleButton(const String& name,
                           const ImageElements::Element::Ptr& imageElement,
                           const Text& text,
                           const Text& textShadow,
                           const ivec2& size,
                           const ImageSlices::ImageSlice::Ptr& imageSlice)
  : Button(name,
           imageElement,
           text,
           textShadow,
           size,
           imageSlice)
{
  this->states.append(State::TOGGLEBUTTON_ACTIVE | State::NORMAL);
  this->states.append(State::TOGGLEBUTTON_ACTIVE | State::FOCUSED);
  this->states.append(State::TOGGLEBUTTON_ACTIVE | State::INSENSITIVE);
  this->states.append(State::TOGGLEBUTTON_ACTIVE | State::HOVER);
  this->states.append(State::TOGGLEBUTTON_ACTIVE | State::NORMAL);
  this->states.append(State::TOGGLEBUTTON_ACTIVE | State::PRESSED);
  this->states.append(State::TOGGLEBUTTON_ACTIVE | State::PRESSED | State::BUTTON_OUTSIDE_WIDGET);
}

ToogleButton::Ptr ToogleButton::create(const String& name,
                                       const ImageElements::Element::Ptr& imageElement,
                                       const Text& text,
                                       const Text& textShadow,
                                       const ivec2& size,
                                       const ImageSlices::ImageSlice::Ptr& imageSlice)
{
  return Ptr(new ToogleButton(name,
                              imageElement,
                              text,
                              textShadow,
                              size,
                              imageSlice));
}

CEGUI::String ToogleButton::getCeguiStateName(State state) const
{
  switch(State::underlying_type(state.value))
  {
  case State::TOGGLEBUTTON_ACTIVE | State::NORMAL:
    return "SelectedNormal";
  case State::TOGGLEBUTTON_ACTIVE | State::INSENSITIVE:
    return "SelectedDisabled";
  case State::TOGGLEBUTTON_ACTIVE | State::HOVER:
    return "SelectedHover";
  case State::TOGGLEBUTTON_ACTIVE | State::PRESSED:
    return "SelectedPushed";
  case State::TOGGLEBUTTON_ACTIVE | State::PRESSED | State::BUTTON_OUTSIDE_WIDGET:
    return "SelectedPushedOff";
  case State::TOGGLEBUTTON_ACTIVE | State::FOCUSED: // CEGUI doesn't suport this state
  default:
    return Button::getCeguiStateName(state);
  }
}

Optional<CEGUI::String> ToogleButton::ceguiTargetType() const
{
  return CEGUI::String("CEGUI/ToggleButton");
}

Optional<CEGUI::String> ToogleButton::ceguiRenderer() const
{
  return CEGUI::String("Core/ToggleButton");
}


// ==== CheckButton ====


CheckButton::CheckButton(const String& name,
                         const ImageElements::Element::Ptr& imageElement,
                         const Text& text,
                         const Text& textShadow,
                         const ivec2& size,
                         const ImageSlices::ImageSlice::Ptr& imageSlice)
  : ToogleButton(name,
                 imageElement,
                 text,
                 textShadow,
                 size,
                 imageSlice)
{
  this->states.append(State::CHECKBUTTON_UNDEFINED | State::NORMAL);
  this->states.append(State::CHECKBUTTON_UNDEFINED | State::INSENSITIVE);
  this->states.append(State::CHECKBUTTON_UNDEFINED | State::HOVER);
  this->states.append(State::CHECKBUTTON_UNDEFINED | State::NORMAL);
  this->states.append(State::CHECKBUTTON_UNDEFINED | State::PRESSED | State::BUTTON_OUTSIDE_WIDGET);
}

CheckButton::Ptr CheckButton::create(const String& name,
                                     const ImageElements::Element::Ptr& imageElement,
                                     const Text& text,
                                     const Text& textShadow,
                                     const ivec2& size,
                                     const ImageSlices::ImageSlice::Ptr& imageSlice)
{
  return Ptr(new CheckButton(name,
                             imageElement,
                             text,
                             textShadow,
                             size,
                             imageSlice));
}

CEGUI::String CheckButton::getCeguiStateName(State state) const
{
  return ToogleButton::getCeguiStateName(state);
}


// ==== WindowFrame ====


WindowFrame::WindowFrame(const String& name,
                         const ImageElements::Element::Ptr& imageElement,
                         const ivec2& size,
                         const ImageSlices::ImageSlice::Ptr& imageSlice,
                         const Area::Ptr& clientNoTitleNoFrame,
                         const Area::Ptr& clientNoTitleWithFrame,
                         const Area::Ptr& clientWithTitleNoFrame,
                         const Area::Ptr& clientWithTitleWithFrame,
                         const Area::Ptr& titleBarArea,
                         const Area::Ptr& closeButtonArea,
                         MouseCursor mouseCursorNtoS,
                         MouseCursor mouseCursorWtoE,
                         MouseCursor mouseCursorNWtoSE,
                         MouseCursor mouseCursorNEtoSW,
                         const String& windowTitleBarName,
                         const String& windowCloseButtonName)
  : SingleImageElement(name,
                       imageElement,
                       QVector<State>({State::NORMAL,
                                       State::INSENSITIVE,
                                       State::FOCUSED,
                                       State::NORMAL | State::WINDOWFRAME_WITH_FRAME,
                                       State::INSENSITIVE | State::WINDOWFRAME_WITH_FRAME,
                                       State::FOCUSED | State::WINDOWFRAME_WITH_FRAME,
                                       State::NORMAL | State::WINDOWFRAME_WITH_FRAME | State::WINDOWFRAME_WITH_TITLEBAR,
                                       State::INSENSITIVE | State::WINDOWFRAME_WITH_FRAME | State::WINDOWFRAME_WITH_TITLEBAR,
                                       State::FOCUSED | State::WINDOWFRAME_WITH_FRAME | State::WINDOWFRAME_WITH_TITLEBAR,
                                       State::NORMAL | State::WINDOWFRAME_WITH_TITLEBAR,
                                       State::INSENSITIVE | State::WINDOWFRAME_WITH_TITLEBAR,
                                       State::FOCUSED | State::WINDOWFRAME_WITH_TITLEBAR}),
                       size,
                       imageSlice),
    clientNoTitleNoFrame(clientNoTitleNoFrame),
    clientNoTitleWithFrame(clientNoTitleWithFrame),
    clientWithTitleNoFrame(clientWithTitleNoFrame),
    clientWithTitleWithFrame(clientWithTitleWithFrame),
    titleBarArea(titleBarArea),
    closeButtonArea(closeButtonArea),
    mouseCursorNtoS(mouseCursorNtoS),
    mouseCursorWtoE(mouseCursorWtoE),
    mouseCursorNWtoSE(mouseCursorNWtoSE),
    mouseCursorNEtoSW(mouseCursorNEtoSW),
    windowTitleBarName(windowTitleBarName),
    windowCloseButtonName(windowCloseButtonName)
{
}

WindowFrame::Ptr WindowFrame::create(const String& name,
                                     const ImageElements::Element::Ptr& imageElement,
                                     const ivec2& size,
                                     const ImageSlices::ImageSlice::Ptr& imageSlice,
                                     const Area::Ptr& clientNoTitleNoFrame,
                                     const Area::Ptr& clientNoTitleWithFrame,
                                     const Area::Ptr& clientWithTitleNoFrame,
                                     const Area::Ptr& clientWithTitleWithFrame,
                                     const Area::Ptr& titleBarArea,
                                     const Area::Ptr& closeButtonArea,
                                     MouseCursor mouseCursorNtoS,
                                     MouseCursor mouseCursorWtoE,
                                     MouseCursor mouseCursorNWtoSE,
                                     MouseCursor mouseCursorNEtoSW,
                                     const String& windowTitleBarName,
                                     const String& windowCloseButtonName)
{
  return Ptr(new WindowFrame(name,
                             imageElement,
                             size,
                             imageSlice,
                             clientNoTitleNoFrame,
                             clientNoTitleWithFrame,
                             clientWithTitleNoFrame,
                             clientWithTitleWithFrame,
                             titleBarArea,
                             closeButtonArea,
                             mouseCursorNtoS,
                             mouseCursorWtoE,
                             mouseCursorNWtoSE,
                             mouseCursorNEtoSW,
                             windowTitleBarName,
                             windowCloseButtonName));
}

CEGUI::String WindowFrame::getCeguiStateName(State state) const
{
  switch(State::underlying_type(state.value))
  {
  case State::FOCUSED | State::WINDOWFRAME_WITH_FRAME | State::WINDOWFRAME_WITH_TITLEBAR:
    return "ActiveWithTitleWithFrame";
  case State::NORMAL | State::WINDOWFRAME_WITH_FRAME | State::WINDOWFRAME_WITH_TITLEBAR:
    return "InactiveWithTitleWithFrame";
  case State::INSENSITIVE | State::WINDOWFRAME_WITH_FRAME | State::WINDOWFRAME_WITH_TITLEBAR:
    return "DisabledWithTitleWithFrame";
  case State::FOCUSED | State::WINDOWFRAME_WITH_TITLEBAR:
    return "ActiveWithTitleNoFrame";
  case State::NORMAL | State::WINDOWFRAME_WITH_TITLEBAR:
    return "InactiveWithTitleNoFrame";
  case State::INSENSITIVE | State::WINDOWFRAME_WITH_TITLEBAR:
    return "DisabledWithTitleNoFrame";
  case State::NORMAL | State::WINDOWFRAME_WITH_FRAME:
    return "ActiveNoTitleWithFrame";
  case State::INSENSITIVE | State::WINDOWFRAME_WITH_FRAME:
    return "InactiveNoTitleWithFrame";
  case State::FOCUSED | State::WINDOWFRAME_WITH_FRAME:
    return "DisabledNoTitleWithFrame";
  case State::FOCUSED:
    return "ActiveNoTitleNoFrame";
  case State::NORMAL:
    return "InactiveNoTitleNoFrame";
  case State::INSENSITIVE:
    return "DisabledNoTitleNoFrame";
  default:
    return "";
  }
}

void WindowFrame::prependCeguiXmlStuff_NamedAreas(CEGUI::XMLSerializer& xml)
{
  xml.openTag("NamedArea");
  xml.attribute("name", "ClientNoTitleNoFrame");
  clientNoTitleNoFrame->serializeToCegui(xml);
  xml.closeTag(); // NamedArea

  xml.openTag("NamedArea");
  xml.attribute("name", "ClientNoTitleWithFrame");
  clientNoTitleWithFrame->serializeToCegui(xml);
  xml.closeTag(); // NamedArea

  xml.openTag("NamedArea");
  xml.attribute("name", "ClientWithTitleNoFrame");
  clientWithTitleNoFrame->serializeToCegui(xml);
  xml.closeTag(); // NamedArea

  xml.openTag("NamedArea");
  xml.attribute("name", "ClientWithTitleWithFrame");
  clientWithTitleWithFrame->serializeToCegui(xml);
  xml.closeTag(); // NamedArea
}


void WindowFrame::prependCeguiXmlStuff_Child(CEGUI::XMLSerializer& xml)
{
  xml.openTag("Child");
  xml.attribute("nameSuffix", "__auto_titlebar__");
  xml.attribute("type", this->windowTitleBarName.toCeguiString());
  titleBarArea->serializeToCegui(xml);
  setChildOutsideClientArea(xml); // Necessary, otherwise the titlebar will be placed inside the client area
  xml.closeTag(); // Child

  xml.openTag("Child");
  xml.attribute("nameSuffix", "__auto_closebutton__");
  xml.attribute("type", this->windowCloseButtonName.toCeguiString());
  closeButtonArea->serializeToCegui(xml);
  setChildOutsideClientArea(xml); // Necessary, otherwise the close-button will be placed inside the client area
  xml.closeTag(); // Child
}

Optional<CEGUI::String> WindowFrame::ceguiTargetType() const
{
  return CEGUI::String("CEGUI/FrameWindow");
}

Optional<CEGUI::String> WindowFrame::ceguiRenderer() const
{
  return CEGUI::String("Core/FrameWindow");
}

WindowFrame::MouseCursorMap WindowFrame::allCeguiMouseCursors() const
{
  MouseCursorMap m;

  m["NSSizingCursorImage"] = this->mouseCursorNtoS;
  m["EWSizingCursorImage"] = this->mouseCursorWtoE;
  m["NWSESizingCursorImage"] = this->mouseCursorNWtoSE;
  m["NESWSizingCursorImage"] = this->mouseCursorNEtoSW;

  return m;
}

// ==== WindowFrame ====


WindowTitleBar::WindowTitleBar(const String& name,
                               const Text& text, const Text& textShadow,
                               const ImageElements::Element::Ptr& imageElement,
                               const ivec2& size,
                               const ImageSlices::ImageSlice::Ptr& imageSlice)
  : SingleImageElement(name,
                       imageElement,
                       QVector<State>({State::NORMAL,
                                       State::INSENSITIVE,
                                       State::FOCUSED}),
                       size,
                       imageSlice),
    text(text),
    textShadow(textShadow)
{
}

WindowTitleBar::Ptr WindowTitleBar::create(const String& name,
                                           const Text& text,
                                           const Text& textShadow,
                                           const ImageElements::Element::Ptr& imageElement,
                                           const ivec2& size,
                                           const ImageSlices::ImageSlice::Ptr& imageSlice)
{
  return Ptr(new WindowTitleBar(name, text, textShadow, imageElement, size, imageSlice));
}

CEGUI::String WindowTitleBar::getCeguiStateName(State state) const
{
  switch(State::underlying_type(state.value))
  {
  case State::FOCUSED:
    return "Active";
  case State::NORMAL:
    return "Inactive";
  case State::INSENSITIVE:
    return "Disabled";
  default:
    return "";
  }
}

WidgetLook::TextMap WindowTitleBar::_allTexts() const
{
  TextMap map;

  map["0-text-shadow"] = this->textShadow;
  map["1-text"] = this->text;

  return map;
}

Optional<CEGUI::String> WindowTitleBar::ceguiTargetType() const
{
  return CEGUI::String("CEGUI/Titlebar");
}

Optional<CEGUI::String> WindowTitleBar::ceguiRenderer() const
{
  return CEGUI::String("Core/Titlebar");
}


// ==== Tooltip ====


Tooltip::Tooltip(const String& name,
                 const Area::Ptr& clientArea,
                 const ImageElements::Element::Ptr& imageElement,
                 const ivec2& size,
                 const ImageSlices::ImageSlice::Ptr& imageSlice)
  : SingleImageElement(name,
                       imageElement,
                       QVector<State>({State::NORMAL,
                                       State::INSENSITIVE}),
                       size,
                       imageSlice),
    clientArea(clientArea)
{
}

Tooltip::Ptr Tooltip::create(const String& name,
                             const Area::Ptr& clientArea,
                             const ImageElements::Element::Ptr& imageElement,
                             const ivec2& size,
                             const ImageSlices::ImageSlice::Ptr& imageSlice)
{
  return Ptr(new Tooltip(name, clientArea, imageElement, size, imageSlice));
}

CEGUI::String Tooltip::getCeguiStateName(State state) const
{
  switch(State::underlying_type(state.value))
  {
  case State::NORMAL:
    return "Enabled";
  case State::INSENSITIVE:
    return "Disabled";
  default:
    return "";
  }
}

Optional<CEGUI::String> Tooltip::ceguiTargetType() const
{
  return CEGUI::String("CEGUI/Tooltip");
}

Optional<CEGUI::String> Tooltip::ceguiRenderer() const
{
  return CEGUI::String("Core/Tooltip");
}

void Tooltip::prependCeguiXmlStuff_NamedAreas(CEGUI::XMLSerializer& xml)
{
  xml.openTag("NamedArea");
  xml.attribute("name", "TextArea");
  clientArea->serializeToCegui(xml);
  xml.closeTag(); // NamedArea
}

// ==== Utilities ====

void setChildOutsideClientArea(CEGUI::XMLSerializer& xml)
{
  xml.openTag("Property");
  xml.attribute("name", "NonClient");
  xml.attribute("value", "True");
  xml.closeTag(); // Property
}


} // namespace WidgetLooks
} // namespace GuiThemes
} // namespace ProceduralAssets
} // namespace Framework

