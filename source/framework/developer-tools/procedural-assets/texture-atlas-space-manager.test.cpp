#include "texture-atlas-space-manager.h"

#include <base/threads.h>

namespace Framework {
namespace ProceduralAssets {
namespace Private
{


TEST(framework_procedualassets_private_Region, compressHorizontal)
{
  Region a(ivec2(3), ivec2(11));

  a.compressHorizontal();

  EXPECT_EQ(1, a.begin.x);
  EXPECT_EQ(6, a.end.x);

  a = Region(ivec2(2), ivec2(3));

  a.compressHorizontal();

  EXPECT_EQ(1, a.begin.x);
  EXPECT_EQ(2, a.end.x);

  a = Region(ivec2(3), ivec2(4));

  a.compressHorizontal();

  EXPECT_EQ(1, a.begin.x);
  EXPECT_EQ(2, a.end.x);
}

TEST(framework_procedualassets_private_Region, compressVertical)
{
  Region a(ivec2(3), ivec2(11));

  a.compressVertical();

  EXPECT_EQ(1, a.begin.y);
  EXPECT_EQ(6, a.end.y);

  a = Region(ivec2(2), ivec2(3));

  a.compressVertical();

  EXPECT_EQ(1, a.begin.y);
  EXPECT_EQ(2, a.end.y);

  a = Region(ivec2(3), ivec2(4));

  a.compressVertical();

  EXPECT_EQ(1, a.begin.y);
  EXPECT_EQ(2, a.end.y);
}

TEST(framework_procedualassets_private_Region, stretchHorizontal)
{
  Region a(ivec2(1), ivec2(2));

  a.stretchHorizontal();

  EXPECT_EQ(2, a.begin.x);
  EXPECT_EQ(4, a.end.x);
}

TEST(framework_procedualassets_private_Region, stretchVertical)
{
  Region a(ivec2(1), ivec2(2));

  a.stretchVertical();

  EXPECT_EQ(2, a.begin.y);
  EXPECT_EQ(4, a.end.y);
}


TEST(framework_procedualassets_private_MipmapLevel, markAsFull)
{
  typedef MipmapLevel::Cell Cell;
  MipmapLevel level;
  level.init(ivec2(256));

  Region fullRegion(ivec2(0), ivec2(256));
  Region smallPart(ivec2(13, 64), ivec2(33, 128));
  Region w(ivec2(0), ivec2(13, 256));
  Region e(ivec2(33, 0), ivec2(256));
  Region n(ivec2(13, 0), ivec2(33, 64));
  Region s(ivec2(13, 128), ivec2(33, 256));

  EXPECT_TRUE(level.isValueEverywhere(Cell::EMPTY, fullRegion));

  level.markAsFull(smallPart);

  EXPECT_FALSE(level.isValueEverywhere(Cell::FULL, fullRegion));
  EXPECT_FALSE(level.isValueEverywhere(Cell::EMPTY, fullRegion));
  EXPECT_TRUE(level.isValueEverywhere(Cell::EMPTY, w));
  EXPECT_TRUE(level.isValueEverywhere(Cell::EMPTY, e));
  EXPECT_TRUE(level.isValueEverywhere(Cell::EMPTY, n));
  EXPECT_TRUE(level.isValueEverywhere(Cell::EMPTY, s));
  EXPECT_TRUE(level.isValueEverywhere(Cell::FULL, smallPart));

  EXPECT_FALSE(level.isValueEverywhere(Cell::FULL, Region(ivec2(1024), ivec2(2048))));
  EXPECT_FALSE(level.isValueEverywhere(Cell::FULL, Region(ivec2(4,2), ivec2(4,2))));
  EXPECT_FALSE(level.isValueEverywhere(Cell::PARTIALLY_USED, Region(ivec2(1024), ivec2(2048))));
  EXPECT_FALSE(level.isValueEverywhere(Cell::PARTIALLY_USED, Region(ivec2(4,2), ivec2(4,2))));
  EXPECT_FALSE(level.isValueEverywhere(Cell::EMPTY, Region(ivec2(1024), ivec2(2048))));
  EXPECT_FALSE(level.isValueEverywhere(Cell::EMPTY, Region(ivec2(4,2), ivec2(4,2))));
}


TEST(framework_procedualassets_private_MipmapLevel, isRegionWithPossibleSpace)
{
  typedef MipmapLevel::Cell Cell;

  const ivec2 interestingPoint(5, 3);

  const Region fullRegion(ivec2(0), ivec2(32, 8));
  const Region nw(interestingPoint, interestingPoint+ivec2(13, 3));
  const Region se(ivec2(0, 0), interestingPoint+1);
  const Region sw(ivec2(nw.begin.x, se.begin.y), ivec2(nw.end.x, se.end.y));
  const Region ne(ivec2(se.begin.x, nw.begin.y), ivec2(se.end.x, nw.end.y));
  const Region center(ivec2(1), ivec2(21, 7));
  const Region n(ivec2(center.begin.x, nw.begin.y), center.end);
  const Region w(ivec2(nw.begin.x, center.begin.y), center.end);
  const Region s(center.begin, ivec2(center.end.x, se.end.y));
  const Region e(center.begin, ivec2(se.end.x, center.end.y));

  MipmapLevel level;
  level.init(fullRegion.end);

  EXPECT_TRUE(level.isRegionWithPossibleSpace(Region(ivec2(0), ivec2(32, 8))));
  EXPECT_FALSE(level.isRegionWithPossibleSpace(Region(ivec2(0), ivec2(33, 8))));
  EXPECT_FALSE(level.isRegionWithPossibleSpace(Region(ivec2(0), ivec2(32, 9))));
  EXPECT_FALSE(level.isRegionWithPossibleSpace(Region(ivec2(-1,  0), ivec2(32, 8))));
  EXPECT_FALSE(level.isRegionWithPossibleSpace(Region(ivec2( 0, -1), ivec2(32, 8))));

  EXPECT_TRUE(level.isRegionWithPossibleSpace(fullRegion));
  EXPECT_TRUE(level.isRegionWithPossibleSpace(nw));
  EXPECT_TRUE(level.isRegionWithPossibleSpace(se));
  EXPECT_TRUE(level.isRegionWithPossibleSpace(sw));
  EXPECT_TRUE(level.isRegionWithPossibleSpace(ne));
  EXPECT_TRUE(level.isRegionWithPossibleSpace(center));
  EXPECT_TRUE(level.isRegionWithPossibleSpace(n));
  EXPECT_TRUE(level.isRegionWithPossibleSpace(w));
  EXPECT_TRUE(level.isRegionWithPossibleSpace(s));
  EXPECT_TRUE(level.isRegionWithPossibleSpace(e));
  EXPECT_TRUE(level.isRegionWithPossibleSpace(nw-1));
  EXPECT_TRUE(level.isRegionWithPossibleSpace(se+1));
  EXPECT_TRUE(level.isRegionWithPossibleSpace(nw+1));
  EXPECT_TRUE(level.isRegionWithPossibleSpace(level.clamp(se-1)));

  level.cell(interestingPoint) = Cell::PARTIALLY_USED;

  EXPECT_FALSE(level.isRegionWithPossibleSpace(fullRegion));
  EXPECT_TRUE(level.isRegionWithPossibleSpace(nw));
  EXPECT_TRUE(level.isRegionWithPossibleSpace(se));
  EXPECT_TRUE(level.isRegionWithPossibleSpace(sw));
  EXPECT_TRUE(level.isRegionWithPossibleSpace(ne));
  EXPECT_TRUE(level.isRegionWithPossibleSpace(n));
  EXPECT_TRUE(level.isRegionWithPossibleSpace(w));
  EXPECT_TRUE(level.isRegionWithPossibleSpace(s));
  EXPECT_TRUE(level.isRegionWithPossibleSpace(e));
  EXPECT_FALSE(level.isRegionWithPossibleSpace(center));
  EXPECT_FALSE(level.isRegionWithPossibleSpace(nw-1));
  EXPECT_FALSE(level.isRegionWithPossibleSpace(se+1));
  EXPECT_TRUE(level.isRegionWithPossibleSpace(nw+1));
  EXPECT_TRUE(level.isRegionWithPossibleSpace(level.clamp(se-1)));

  level.cell(interestingPoint) = Cell::FULL;

  EXPECT_FALSE(level.isRegionWithPossibleSpace(fullRegion));
  EXPECT_FALSE(level.isRegionWithPossibleSpace(nw));
  EXPECT_FALSE(level.isRegionWithPossibleSpace(se));
  EXPECT_FALSE(level.isRegionWithPossibleSpace(sw));
  EXPECT_FALSE(level.isRegionWithPossibleSpace(ne));
  EXPECT_FALSE(level.isRegionWithPossibleSpace(center));
  EXPECT_FALSE(level.isRegionWithPossibleSpace(n));
  EXPECT_FALSE(level.isRegionWithPossibleSpace(w));
  EXPECT_FALSE(level.isRegionWithPossibleSpace(s));
  EXPECT_FALSE(level.isRegionWithPossibleSpace(e));
  EXPECT_FALSE(level.isRegionWithPossibleSpace(nw-1));
  EXPECT_FALSE(level.isRegionWithPossibleSpace(se+1));
  EXPECT_TRUE(level.isRegionWithPossibleSpace(nw+1));
  EXPECT_TRUE(level.isRegionWithPossibleSpace(level.clamp(se-1)));

  EXPECT_FALSE(level.isRegionWithPossibleSpace(Region(ivec2(1024), ivec2(2048))));
  EXPECT_FALSE(level.isRegionWithPossibleSpace(Region(ivec2(4,2), ivec2(4,2))));
}

TEST(framework_procedualassets_private_MipmapLevel, updateRegion)
{
  typedef MipmapLevel::Cell Cell;
  MipmapLevel smallerX, smallerY, base;
  base.init(ivec2(32, 8));
  smallerX.initCompressedX(base);
  smallerY.initCompressedY(base);

  base.cell(ivec2(0)) = Cell::FULL;
  base.cell(ivec2(0, 1)) = Cell::FULL;
  base.cell(ivec2(1, 0)) = Cell::FULL;

  base.cell(ivec2(8, 4)) = Cell::FULL;
  base.cell(ivec2(22, 4)) = Cell::PARTIALLY_USED;
  base.cell(ivec2(25, 7)) = Cell::FULL;

  smallerX.updateRegion(base);
  smallerY.updateRegion(base);

  EXPECT_EQ(Cell::FULL, smallerX.cell(ivec2(0)));
  EXPECT_EQ(Cell::FULL, smallerY.cell(ivec2(0)));

  EXPECT_EQ(Cell::PARTIALLY_USED, smallerX.cell(ivec2(4, 4)));
  EXPECT_EQ(Cell::PARTIALLY_USED, smallerX.cell(ivec2(11, 4)));
  EXPECT_EQ(Cell::PARTIALLY_USED, smallerX.cell(ivec2(12, 7)));

  EXPECT_EQ(Cell::PARTIALLY_USED, smallerY.cell(ivec2(8, 2)));
  EXPECT_EQ(Cell::PARTIALLY_USED, smallerY.cell(ivec2(22, 2)));
  EXPECT_EQ(Cell::PARTIALLY_USED, smallerY.cell(ivec2(25, 3)));
}

TEST(framework_procedualassets_private_MipmapLevel, updateRegion_bad_base)
{
  MipmapLevel a, b;
  a.init(ivec2(16, 32));
  b.init(ivec2(32, 64));

  EXPECT_THROW(a.updateRegion(b, Region(ivec2(0), ivec2(1))), std::logic_error);
}

TEST(framework_procedualassets_private_MipmapLevel, valueMixtures)
{
  typedef MipmapLevel::Cell Cell;
  MipmapLevel smallerX, smallerY, base;
  base.init(ivec2(16));
  smallerX.initCompressedX(base);
  smallerY.initCompressedY(base);

  QVector<Cell> cellValues = {Cell::EMPTY, Cell::PARTIALLY_USED, Cell::FULL};

  const ivec2 xStart(0, 0);
  const ivec2 yStart(2, 0);
  const ivec2 xDir(0, 1);
  const ivec2 yDir(1, 0);

  const int n = 9;

  for(int i : range(0, n-1))
  {
    base.cell(xStart + xDir*i)        = cellValues[Cell::Value(i/3)];
    base.cell(xStart + xDir*i + yDir) = cellValues[Cell::Value(i%3)];
    base.cell(yStart + yDir*i)        = cellValues[Cell::Value(i/3)];
    base.cell(yStart + yDir*i + xDir) = cellValues[Cell::Value(i%3)];
  }

  smallerX.updateRegion(base);
  smallerY.updateRegion(base);

  for(int i : range(0, n-1))
  {
    Cell expectedCellValue;
    if(i == 0)
      expectedCellValue = Cell::EMPTY;
    else if(i == n-1)
      expectedCellValue = Cell::FULL;
    else
      expectedCellValue = Cell::PARTIALLY_USED;

    ASSERT_EQ(expectedCellValue, smallerX.cell(xStart + xDir*i));
    ASSERT_EQ(expectedCellValue, smallerY.cell(yStart + yDir*i));
  }
}

void printContent(const MipmapLevel& l)
{
  typedef MipmapLevel::Cell Cell;
  for(Cell c : l.cells)
    std::cout << c.value << " ";
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << std::endl;
}

TEST(framework_procedualassets_private_MipmapLevel, updateRegion_invalid_region)
{
  typedef MipmapLevel::Cell Cell;
  MipmapLevel smaller, base;

  base.init(ivec2(4));
  smaller.init(ivec2(4, 2));

  base.markAsFull(Region(ivec2(0), ivec2(4)));

  smaller.updateRegion(base, Region(ivec2(12), ivec2(16)));

  EXPECT_TRUE(smaller.isValueEverywhere(Cell::EMPTY, Region(ivec2(0), ivec2(16))));

  smaller.updateRegion(base, Region(ivec2(1), ivec2(1)));

  EXPECT_TRUE(smaller.isValueEverywhere(Cell::EMPTY, Region(ivec2(0), ivec2(16))));

  smaller.updateRegion(base, Region(ivec2(-4), ivec2(16)));

  EXPECT_TRUE(smaller.isValueEverywhere(Cell::FULL, Region(ivec2(0), ivec2(16))));
}

TEST(framework_procedualassets_private_MipmapLevel, updateRegion_updateWithOffset)
{
  typedef MipmapLevel::Cell Cell;
  MipmapLevel smaller, base;

  base.init(ivec2(8, 16));
  smaller.init(ivec2(8));

  base.markAsFull(Region(ivec2(0), ivec2(16)));

  smaller.updateRegion(base, Region(ivec2(1, 2), ivec2(4, 6)));

  EXPECT_TRUE(smaller.isValueEverywhere(Cell::FULL, Region(ivec2(1, 2), ivec2(4, 6))));
  EXPECT_TRUE(smaller.isValueEverywhere(Cell::EMPTY, Region(ivec2(0), ivec2(1, 8))));
  EXPECT_TRUE(smaller.isValueEverywhere(Cell::EMPTY, Region(ivec2(0), ivec2(8, 2))));
  EXPECT_TRUE(smaller.isValueEverywhere(Cell::EMPTY, Region(ivec2(0, 6), ivec2(8, 8))));
  EXPECT_TRUE(smaller.isValueEverywhere(Cell::EMPTY, Region(ivec2(4, 0), ivec2(8, 8))));
}



TEST(framework_procedualassets_private_MipmapTable, forEachLevelDescendingSize)
{
  MipmapTable table(ivec2(ivec2(16, 8)));

  const int BASE = 0;
  const int X = 1;
  const int Y = 2;

  QVector<ivec4> order;
  QVector<int> type;

  auto initLargest = [&](MipmapLevel& level) {
    order.append(ivec4(level.layerSize(), ivec2(-1)));
    type.append(BASE);
  };
  auto compressByX = [&](MipmapLevel& level, MipmapLevel& base) {
    order.append(ivec4(level.layerSize(), base.layerSize()));
    type.append(X);
  };
  auto compressByY = [&](MipmapLevel& level, MipmapLevel& base) {
    order.append(ivec4(level.layerSize(), base.layerSize()));
    type.append(Y);
  };

  table.forEachLevelDescendingSize(initLargest, compressByX, compressByY);

  int i=0;

  ASSERT_EQ(20, order.size());
  ASSERT_EQ(20, type.size());
  EXPECT_EQ(ivec2(16, 8), order[i].xy());
  EXPECT_EQ(ivec2(-1),    order[i].zw());
  EXPECT_EQ(BASE, type[0]);
  i++;

  EXPECT_EQ(ivec2(16, 4), order[i].xy());
  EXPECT_EQ(ivec2(16, 8), order[i].zw());
  EXPECT_EQ(Y, type[i]);
  i++;

  EXPECT_EQ(ivec2(16, 2), order[i].xy());
  EXPECT_EQ(ivec2(16, 4), order[i].zw());
  EXPECT_EQ(Y, type[i]);
  i++;

  EXPECT_EQ(ivec2(16, 1), order[i].xy());
  EXPECT_EQ(ivec2(16, 2), order[i].zw());
  EXPECT_EQ(Y, type[i]);
  i++;

  //

  EXPECT_EQ(ivec2( 8, 8), order[i].xy());
  EXPECT_EQ(ivec2(16, 8), order[i].zw());
  EXPECT_EQ(X, type[i]);
  i++;

  EXPECT_EQ(ivec2( 4, 8), order[i].xy());
  EXPECT_EQ(ivec2( 8, 8), order[i].zw());
  EXPECT_EQ(X, type[i]);
  i++;

  EXPECT_EQ(ivec2( 2, 8), order[i].xy());
  EXPECT_EQ(ivec2( 4, 8), order[i].zw());
  EXPECT_EQ(X, type[i]);
  i++;

  EXPECT_EQ(ivec2( 1, 8), order[i].xy());
  EXPECT_EQ(ivec2( 2, 8), order[i].zw());
  EXPECT_EQ(X, type[i]);
  i++;

  //

  EXPECT_EQ(ivec2( 8, 4), order[i].xy());
  EXPECT_EQ(ivec2(16, 4), order[i].zw());
  EXPECT_EQ(X, type[i]);
  i++;

  EXPECT_EQ(ivec2( 4, 4), order[i].xy());
  EXPECT_EQ(ivec2( 8, 4), order[i].zw());
  EXPECT_EQ(X, type[i]);
  i++;

  EXPECT_EQ(ivec2( 2, 4), order[i].xy());
  EXPECT_EQ(ivec2( 4, 4), order[i].zw());
  EXPECT_EQ(X, type[i]);
  i++;

  EXPECT_EQ(ivec2( 1, 4), order[i].xy());
  EXPECT_EQ(ivec2( 2, 4), order[i].zw());
  EXPECT_EQ(X, type[i]);
  i++;

  //

  EXPECT_EQ(ivec2( 8, 2), order[i].xy());
  EXPECT_EQ(ivec2(16, 2), order[i].zw());
  EXPECT_EQ(X, type[i]);
  i++;

  EXPECT_EQ(ivec2( 4, 2), order[i].xy());
  EXPECT_EQ(ivec2( 8, 2), order[i].zw());
  EXPECT_EQ(X, type[i]);
  i++;

  EXPECT_EQ(ivec2( 2, 2), order[i].xy());
  EXPECT_EQ(ivec2( 4, 2), order[i].zw());
  EXPECT_EQ(X, type[i]);
  i++;

  EXPECT_EQ(ivec2( 1, 2), order[i].xy());
  EXPECT_EQ(ivec2( 2, 2), order[i].zw());
  EXPECT_EQ(X, type[i]);
  i++;

  //

  EXPECT_EQ(ivec2( 8, 1), order[i].xy());
  EXPECT_EQ(ivec2(16, 1), order[i].zw());
  EXPECT_EQ(X, type[i]);
  i++;

  EXPECT_EQ(ivec2( 4, 1), order[i].xy());
  EXPECT_EQ(ivec2( 8, 1), order[i].zw());
  EXPECT_EQ(X, type[i]);
  i++;

  EXPECT_EQ(ivec2( 2, 1), order[i].xy());
  EXPECT_EQ(ivec2( 4, 1), order[i].zw());
  EXPECT_EQ(X, type[i]);
  i++;

  EXPECT_EQ(ivec2( 1, 1), order[i].xy());
  EXPECT_EQ(ivec2( 2, 1), order[i].zw());
  EXPECT_EQ(X, type[i]);
  i++;
}

TEST(framework_procedualassets_private_MipmapTable, markRegionAsUsed)
{
  typedef MipmapLevel::Cell Cell;
  MipmapTable table(ivec2(ivec2(4)));

  typedef QVector<Cell> V;
  const Cell O = Cell::EMPTY;
  const Cell F = Cell::FULL;
  const Cell p = Cell::PARTIALLY_USED;

  V v = {O,O,O,O,
         O,O,O,O,
         O,O,O,O,
         O,O,O,O,};
  EXPECT_EQ(v, table.levelForIndex(ivec2(2, 2)).cells);

  table.markRegionAsUsed(Region(ivec2(0, 0), ivec2(1, 1)));
  table.markRegionAsUsed(Region(ivec2(2, 2), ivec2(5, 5)));
  table.markRegionAsUsed(Region(ivec2(1, 3), ivec2(2, 4)));

  v = {F,O,O,O,
       O,O,O,O,
       O,O,F,F,
       O,F,F,F,};
  EXPECT_EQ(v, table.levelForIndex(ivec2(2, 2)).cells);

  v = {p,O,O,O,
       O,p,F,F,};
  EXPECT_EQ(v, table.levelForIndex(ivec2(2, 1)).cells);

  v = {p,p,p,p,};
  EXPECT_EQ(v, table.levelForIndex(ivec2(2, 0)).cells);

  v = {p,O,
       O,O,
       O,F,
       p,F,};
  EXPECT_EQ(v, table.levelForIndex(ivec2(1, 2)).cells);

  v = {p,
       O,
       p,
       p,};
  EXPECT_EQ(v, table.levelForIndex(ivec2(0, 2)).cells);

  v = {p,O,
       p,F,};
  EXPECT_EQ(v, table.levelForIndex(ivec2(1, 1)).cells);

  v = {p};
  EXPECT_EQ(v, table.levelForIndex(ivec2(0, 0)).cells);
}

TEST(framework_procedualassets_private_MipmapTable, calculatePathForRegion)
{
  typedef MipmapTable::Path Path;
  MipmapTable table(ivec2(2048, 512));

  Path path = table.calculatePathForRegionSize(ivec2(75, 25));

  ASSERT_EQ(12, path.size());

  int i=0;
  EXPECT_EQ(ivec2(0, 0), path[i].level);
  EXPECT_EQ(ivec2(0), path[i].region.begin);
  EXPECT_EQ(ivec2(1), path[i].region.end);
  i++;

  EXPECT_EQ(ivec2(1, 0), path[i].level);
  EXPECT_EQ(ivec2(0), path[i].region.begin);
  EXPECT_EQ(ivec2(1), path[i].region.end);
  i++;

  EXPECT_EQ(ivec2(2, 0), path[i].level);
  EXPECT_EQ(ivec2(0), path[i].region.begin);
  EXPECT_EQ(ivec2(1), path[i].region.end);
  i++;

  EXPECT_EQ(ivec2(3, 1), path[i].level);
  EXPECT_EQ(ivec2(0), path[i].region.begin);
  EXPECT_EQ(ivec2(1), path[i].region.end);
  i++;

  EXPECT_EQ(ivec2(4, 2), path[i].level);
  EXPECT_EQ(ivec2(0), path[i].region.begin);
  EXPECT_EQ(ivec2(1), path[i].region.end);
  i++;

  EXPECT_EQ(ivec2(5, 3), path[i].level);
  EXPECT_EQ(ivec2(0), path[i].region.begin);
  EXPECT_EQ(ivec2(2, 1), path[i].region.end);
  i++;

  EXPECT_EQ(ivec2(6, 4), path[i].level);
  EXPECT_EQ(ivec2(0), path[i].region.begin);
  EXPECT_EQ(ivec2(3, 1), path[i].region.end);
  i++;

  EXPECT_EQ(ivec2(7, 5), path[i].level);
  EXPECT_EQ(ivec2(0), path[i].region.begin);
  EXPECT_EQ(ivec2(5, 2), path[i].region.end);
  i++;

  EXPECT_EQ(ivec2(8, 6), path[i].level);
  EXPECT_EQ(ivec2(0), path[i].region.begin);
  EXPECT_EQ(ivec2(10, 4), path[i].region.end);
  i++;

  EXPECT_EQ(ivec2(9, 7), path[i].level);
  EXPECT_EQ(ivec2(0), path[i].region.begin);
  EXPECT_EQ(ivec2(19, 7), path[i].region.end);
  i++;

  EXPECT_EQ(ivec2(10, 8), path[i].level);
  EXPECT_EQ(ivec2(0), path[i].region.begin);
  EXPECT_EQ(ivec2(38, 13), path[i].region.end);
  i++;

  EXPECT_EQ(ivec2(11, 9), path[i].level);
  EXPECT_EQ(ivec2(0), path[i].region.begin);
  EXPECT_EQ(ivec2(75, 25), path[i].region.end);
  i++;
}


TEST(framework_procedualassets_private_MipmapTable, calculatePathForRegion_2)
{
  typedef MipmapTable::Path Path;
  typedef MipmapTable::PathPoint PathPoint;
  MipmapTable table(ivec2(128, 512));

  Path path = table.calculatePathForRegionSize(ivec2(2, 511));

  ASSERT_EQ(10, path.size());

  for(int i=0; i<path.size(); ++i)
  {
    const PathPoint& p = path[i];
    EXPECT_LE(p.region.end.x, 128) << "i: " << i;
    EXPECT_LE(p.region.end.y, 512) << "i: " << i;
  }
}

TEST(framework_procedualassets_private_MipmapTable, searchRegion)
{
  MipmapTable table(ivec2(128, 512));

  EXPECT_FALSE(table.searchRegion(ivec2(130, 2)));
  EXPECT_FALSE(table.searchRegion(ivec2(2, 513)));
  EXPECT_TRUE(table.searchRegion(ivec2(2, 511)));

  QVector<ivec2> expectedPositions = {ivec2(0),
                                      ivec2(0, 50),
                                      ivec2(5, 50),
                                      ivec2(0, 55),
                                      ivec2(5, 55),
                                      ivec2(10, 50),
                                      ivec2(12, 50),
                                      ivec2(14, 50),
                                      ivec2(10, 52),
                                      ivec2(12, 52),
                                      ivec2(16, 50),
                                      ivec2(10, 54),
                                     };
  QVector<ivec2> usedSizes = {ivec2(100, 50),
                              ivec2(5),
                              ivec2(5),
                              ivec2(5),
                              ivec2(75, 25),
                              ivec2(2, 2),
                              ivec2(2, 2),
                              ivec2(2, 2),
                              ivec2(2, 2),
                              ivec2(2, 2),
                              ivec2(25, 5),
                              ivec2(6, 1),
                             };

  ASSERT_EQ(usedSizes.size(), expectedPositions.size());

  for(int i=0; i<expectedPositions.size(); ++i)
  {
    const ivec2& expectedOffset = expectedPositions[i];
    const ivec2& currentSize = usedSizes[i];
    Optional<Region> result;

    result = table.searchRegion(currentSize);
    ASSERT_TRUE(result.valid());
    ASSERT_EQ(expectedOffset, result.value().begin);
    ASSERT_EQ(expectedOffset+currentSize, result.value().end);

    table.markRegionAsUsed(*result);
  }
}


} // namespace Private
} // namespace ProceduralAssets
} // namespace Framework
