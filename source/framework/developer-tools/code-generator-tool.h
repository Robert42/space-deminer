#ifndef FRAMEWORK_CODEGENERATORTOOL_H
#define FRAMEWORK_CODEGENERATORTOOL_H

#include "code-node-editor.h"

#include <framework/developer-tools/tweak-ui/tweak-ui-bar.h>

namespace Framework {


class CodeGeneratorTool final : noncopyable
{
public:
  Signals::Trackable trackable;
private:
  Framework::TweakUI::Bar::Ptr bar;
  CodeNodeEditor nodeEditor;
public:
  CodeGeneratorTool();
  ~CodeGeneratorTool();

private:
  void quit();
  void quitNow();

  void updateWindowSize();
};


} // namespace Framework

#endif // FRAMEWORK_CODEGENERATORTOOL_H
