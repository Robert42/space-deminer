#include <dependencies.h>
#include <framework/developer-tools/developer-settings.h>
#include <base/io/special-directories.h>
#include <base/strings/string.h>

namespace Framework {


DeveloperSettings::Input::Input()
{
  grabMouse = true;
  hideMouseCursor = true;
  grabKeyboard = true;
  grabJoystick = true;
  alwaysShowIngameMouseCursor = false;
}


DeveloperSettings::Graphic::Graphic()
{
  allowFullscreen = true;
}


DeveloperSettings::Terminal::Terminal()
{
  enable = false;
  alwaysUseTerminalMouseMode = false;
}


DeveloperSettings::Samples::Samples()
{
  loadSample = false;
}


DeveloperSettings::DeveloperSettings(const IO::Directory& configDirectory)
{
  IO::RegularFile configFile = configDirectory | "developer-settings.cfg";

  if(configFile.exists())
    loadFromFile(configFile);
}


void DeveloperSettings::loadFromFile(const IO::RegularFile& developerSettingsFile)
{
  Ogre::ConfigFile configFile;

  configFile.loadDirect(developerSettingsFile.pathAsOgreString());

  Ogre::ConfigFile::SectionIterator sectionIterator = configFile.getSectionIterator();

  while(sectionIterator.hasMoreElements())
  {
    Ogre::String groupName = sectionIterator.peekNextKey();

    Ogre::ConfigFile::SettingsMultiMap* settingsOfCurrentSection = sectionIterator.getNext();

    Ogre::ConfigFile::SettingsMultiMap::iterator settingsIterator = settingsOfCurrentSection->begin();
    while(settingsIterator != settingsOfCurrentSection->end())
    {
      Ogre::String setting = settingsIterator->first;
      Ogre::String value = settingsIterator->second;

      handleSetting(groupName, setting, value);

      ++settingsIterator;
    }
  }
}


void DeveloperSettings::handleSetting(const Ogre::String& groupName, const Ogre::String& setting, const Ogre::String& value)
{
  if(groupName == "Terminal")
  {
    if(setting == "enable")
      this->terminal.enable = value!="false";
    else if(setting == "debug-mouse-modes")
      this->terminal.alwaysUseTerminalMouseMode = value!="false";
    else
      std::cout<<"WARNING: Unknown developer setting `"<<setting<<"`"<<std::endl;
  }else if(groupName == "Graphic")
  {
    if(setting == "allow-fullscreen")
      this->graphic.allowFullscreen = value!="false";
    else
      std::cout<<"WARNING: Unknown developer setting `"<<setting<<"`"<<std::endl;
  }else if(groupName == "Input")
  {
    if(setting == "grab-joystick")
      this->input.grabJoystick = value!="false";
    else if(setting == "grab-keyboard")
      this->input.grabKeyboard = value!="false";
    else if(setting == "grab-mouse")
      this->input.grabMouse = value!="false";
    else if(setting == "hide-mouse-cursor")
      this->input.hideMouseCursor = value!="false";
    else if(setting == "always-show-ingame-cursor")
      this->input.alwaysShowIngameMouseCursor = value!="false";
    else
      std::cout<<"WARNING: Unknown developer setting `"<<setting<<"`"<<std::endl;
  }else if(groupName == "Samples")
  {
    if(setting == "load-sample")
    {
      this->samples.loadSample = true;
      this->samples.sampleToLoad = value;
    }else
      std::cout<<"WARNING: Unknown developer setting `"<<setting<<"`"<<std::endl;
  }else
  {
    std::cout<<"WARNING: Unknown developer setting group `"<<groupName<<"`"<<std::endl;
  }
}


}
