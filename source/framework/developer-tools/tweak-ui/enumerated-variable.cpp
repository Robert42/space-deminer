#include "enumerated-variable.h"

namespace Framework {
namespace TweakUI {


EnumeratedVariable::EnumeratedVariable(const char* typeName, const char* name, const Bar::Ptr& bar, const char* def)
  : bar(bar),
    name(name)
{
  _currentIndex = 0;

  ::TweakUI::TwAddVarCB(bar->wrappedObject(),
                        name,
                        ::TweakUI::TwDefineEnumFromString(typeName, nullptr),
                        setter,
                        getter,
                        this,
                        def);
}


EnumeratedVariable::~EnumeratedVariable()
{
  bar->removeVar(name.c_str());
}


EnumeratedVariable::Ptr EnumeratedVariable::create(const char* typeName, const char* name, const Bar::Ptr& bar, const char* def)
{
  return Ptr(new EnumeratedVariable(typeName, name, bar, def));
}


void EnumeratedVariable::add(int index, const std::string& text)
{
  itemsText[index] = text;

  if(itemsText.size() == 1)
    _currentIndex = index;

  updateEnumeration();
}


int EnumeratedVariable::append(const std::string& text)
{
  int index = itemsText.size();

  while(itemsText.contains(index))
    ++index;

  add(index, text);

  return index;
}


void EnumeratedVariable::remove(const std::string& text)
{
  remove(indexOf(text));
}


void EnumeratedVariable::remove(int i)
{
  itemsText.remove(i);

  updateEnumeration();
}


void EnumeratedVariable::clear()
{
  itemsText.clear();

  updateEnumeration();
}


int EnumeratedVariable::currentIndex() const
{
  return _currentIndex;
}


void EnumeratedVariable::setCurrentIndex(int index)
{
  if(!itemsText.contains(index))
    return;

  if(currentIndex() != index)
  {
    _currentIndex = index;

    _signalIndexChanged(currentIndex());
    _signalTextChanged(itemsText[currentIndex()]);
  }
}


int EnumeratedVariable::indexOf(const std::string& text, int fallback) const
{
  int i = itemsText.values().indexOf(text);

  if(i<0)
    return fallback;

  return itemsText.keys()[i];
}


void EnumeratedVariable::setCurrentText(const std::string& text)
{
  EnumeratedVariable::setCurrentIndex(indexOf(text));
}


std::string EnumeratedVariable::currentText(const std::string& fallback) const
{
  if(!itemsText.contains(currentIndex()))
    return fallback;

  return itemsText.value(currentIndex());
}


Signals::Signal<void(int)>& EnumeratedVariable::signalIndexChanged()
{
  return _signalIndexChanged;
}


Signals::Signal<void(const std::string&)>& EnumeratedVariable::signalTextChanged()
{
  return _signalTextChanged;
}


std::string EnumeratedVariable::generateTweakUIEnumParamterValue() const
{
  std::string enumString;
  bool first = true;

  QList<int> keys = itemsText.keys();
  QList<std::string> values = itemsText.values();

  for(size_t i=0; i<keys.size(); ++i)
  {
    if(!first)
      enumString += ", ";
    enumString += std::to_string(keys[i]) + " {" + values[i] + "}";
    first = false;
  }
  return enumString;
}

void EnumeratedVariable::getter(void* value, void* object)
{
  *reinterpret_cast<int*>(value) = reinterpret_cast<EnumeratedVariable*>(object)->currentIndex();
}


void EnumeratedVariable::setter(const void* value, void* object)
{
  reinterpret_cast<EnumeratedVariable*>(object)->setCurrentIndex(*reinterpret_cast<const int*>(value));
}

void EnumeratedVariable::updateEnumeration()
{
  // TweakUI doesn't allow setting an empty value for the paramter enum
  if(!itemsText.empty())
  {
    std::string enumValues = generateTweakUIEnumParamterValue();
    bar->setVarParam(name.c_str(), "enum", enumValues.c_str());
  }
}


} // namespace TweakUI
} // namespace Framework
