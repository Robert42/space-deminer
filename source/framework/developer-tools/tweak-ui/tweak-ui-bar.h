#ifndef FRAMEWORK_TWEAKUI_TWEAKUIBAR_H
#define FRAMEWORK_TWEAKUI_TWEAKUIBAR_H

#include "tweak-ui-layers.h"

#include <base/enum-macros.h>

namespace Framework {
namespace TweakUI {

template<typename T>
class TweakTypeIdFor;

#define DEFAULT_TWEAK_TYPE_FOR(CPP_TYPE, TW_TYPE) \
template<> \
class TweakTypeIdFor<CPP_TYPE> \
{ \
public: \
  static ::TweakUI::TwType id() \
  { \
    return ::TweakUI::TW_TYPE; \
  } \
};

DEFAULT_TWEAK_TYPE_FOR(float, TW_TYPE_FLOAT)
DEFAULT_TWEAK_TYPE_FOR(double, TW_TYPE_DOUBLE)
DEFAULT_TWEAK_TYPE_FOR(bool, TW_TYPE_BOOLCPP)
DEFAULT_TWEAK_TYPE_FOR(char, TW_TYPE_CHAR)
DEFAULT_TWEAK_TYPE_FOR(int8, TW_TYPE_INT8)
DEFAULT_TWEAK_TYPE_FOR(uint8, TW_TYPE_UINT8)
DEFAULT_TWEAK_TYPE_FOR(int16, TW_TYPE_INT16)
DEFAULT_TWEAK_TYPE_FOR(uint16, TW_TYPE_UINT16)
DEFAULT_TWEAK_TYPE_FOR(int32, TW_TYPE_INT32)
DEFAULT_TWEAK_TYPE_FOR(uint32, TW_TYPE_UINT32)
DEFAULT_TWEAK_TYPE_FOR(char*, TW_TYPE_CDSTRING)
DEFAULT_TWEAK_TYPE_FOR(std::string, TW_TYPE_STDSTRING)
DEFAULT_TWEAK_TYPE_FOR(quaternion, TW_TYPE_QUAT4F)
DEFAULT_TWEAK_TYPE_FOR(vec2, TW_TYPE_VECTOR2)
DEFAULT_TWEAK_TYPE_FOR(vec3, TW_TYPE_VECTOR3)
DEFAULT_TWEAK_TYPE_FOR(vec4, TW_TYPE_VECTOR4)
DEFAULT_TWEAK_TYPE_FOR(ivec2, TW_TYPE_INT_VECTOR2)
DEFAULT_TWEAK_TYPE_FOR(ivec3, TW_TYPE_INT_VECTOR3)
DEFAULT_TWEAK_TYPE_FOR(ivec4, TW_TYPE_INT_VECTOR4)
DEFAULT_TWEAK_TYPE_FOR(::TweakUI::CubicCurve, TW_TYPE_CUBICCURVE)

class Root;

class Bar final
{
public:
  typedef shared_ptr<Bar> Ptr;
  typedef weak_ptr<Bar> WeakPtr;

private:
  class ButtonCallbackWrapper;
  class VarCallbackWrapper;

private:
  static int nVisibleBars;
  void incrVisibleBars();
  void decrVisibleBars();

  ::TweakUI::TwBar* twBar;
  shared_ptr<Root> root;

  std::list<shared_ptr<ButtonCallbackWrapper>> buttonCallbackWrapper;
  std::list<shared_ptr<VarCallbackWrapper>> varCallbackWrapper;

public:
  Layers::Layer* _layer;

private:
  Bar(const char* name);

public:
  ~Bar();

  static Ptr create(const char* name);

  const char* name() const;

  void setTopBar();
  void setBottomBar();
  void forceRefresh();

  ::TweakUI::TwBar* wrappedObject();

  bool isActive() const;

public:
  /** @name Bar Parameters
   **/
  //@{

  BEGIN_ENUMERATION(TextBrightness,)
    LIGHT,
    DARK
  ENUMERATION_BASIC_OPERATORS(TextBrightness)
  END_ENUMERATION;

  template<typename T>
  void setParam(const char* paramName, const T& value);

  template<typename T>
  T param(const char* paramName) const;

  std::string label() const;
  void setLabel(const char* label);

  std::string help() const;
  void setHelp(const char* help);
  void setHelp(const std::string& help);

  vec4 color() const;
  float alpha() const;
  void setColor(const ivec3& color);
  void setColor(const ivec4& color);
  void setColor(const vec3& color);
  void setColor(const vec4& color);
  void setAlpha(uint8 alpha);
  void setAlpha(float alpha);

  TextBrightness textBrightness() const;
  void setTextBrightness(TextBrightness brightness);
  void toggleTextBrightness();

  ivec2 position() const;
  void setPosition(const ivec2& position);

  ivec2 size() const;
  void setSize(const ivec2& size);

  int valuesWidth() const;
  void setValuesWidth(int valuesWidth=-1);
  void setValuesWidthFit();

  float refreshRate() const;
  void setRefreshRate(float refreshRate);

  bool isVisible() const;
  void setVisible(bool visible);

  bool isIconified() const;
  void setIconified(bool iconified);

  bool isIconifiable() const;
  void setIconifiable(bool iconifiable);

  bool isMovable() const;
  void setMovable(bool movable);

  bool isResizable() const;
  void setResizable(bool resizable);

  bool isAlwaysBottom() const;
  void setAlwaysBottom(bool alwaysBottom);

  bool isAlwaysTop() const;
  void setAlwaysTop(bool alwaysTop);

  //@}
public:

  /** @name Variables
   **/
  //@{
  void removeVar(const char* name);
  void removeAllVars();

  void openGroup(const char* name, bool open=true);
  void closeGroup(const char* name);

  void addVarAs(const char* name, ::TweakUI::TwType type, void* var, const char* def="");
  void addVarAs(const char* name, ::TweakUI::TwType type, const void* var, const char* def="");
  void addVarWithCallbackAs(const char* name, ::TweakUI::TwType type, const std::function<void(void*)>& getter, const std::function<void(const void*)>& setter, const char* def="");

  template<typename T, typename T_in=T, typename T_return=T>
  void addVarWithIntuitiveCallbackAs(const char* name,
                                     ::TweakUI::TwType type,
                                     const std::function<T_return()>& getter,
                                     const std::function<void(T_in)>& setter,
                                     const char* def="");

  template<typename T>
  void addVar(const char* name, T* var, const char* def="");

  template<typename T>
  void addVar(const char* name, const T* var, const char* def="");

  void addVar(const char* name, quaternion* var, const char* def="", bool blenderCoordinates=true);
  void addVar(const char* name, const quaternion* var, const char* def="", bool blenderCoordinates=true);


  void addVarAsDirection(const char* name, vec3* var, const char* def="", bool blenderCoordinates=true);
  void addVarAsDirection(const char* name, const vec3* var, const char* def="", bool blenderCoordinates=true);


  template<typename T>
  void addVarAsColor(const char* name, T* var, const char* def="");

  template<typename T>
  void addVarAsColor(const char* name, const T* var, const char* def="");


  void addVarAsVector(const char* name,
                      vec2* var,
                      const char* def="",
                      const vec2& min=vec2(-std::numeric_limits<vec2::value_type>::max()),
                      const vec2& max=vec2(std::numeric_limits<vec2::value_type>::max()),
                      const vec2& step=vec2(0.001f),
                      const ivec2& precision=ivec2(3));
  void addVarAsVector(const char* name,
                      const vec2* var,
                      const char* def="",
                      const vec2& min=vec2(-std::numeric_limits<vec2::value_type>::max()),
                      const vec2& max=vec2(std::numeric_limits<vec2::value_type>::max()),
                      const vec2& step=vec2(0.001f),
                      const ivec2& precision=ivec2(3));
  void addVarAsVector(const char* name,
                      vec3* var,
                      const char* def="",
                      const vec3& min=vec3(-std::numeric_limits<vec3::value_type>::max()),
                      const vec3& max=vec3(std::numeric_limits<vec3::value_type>::max()),
                      const vec3& step=vec3(0.001f),
                      const ivec3& precision=ivec3(3));
  void addVarAsVector(const char* name,
                      const vec3* var,
                      const char* def="",
                      const vec3& min=vec3(-std::numeric_limits<vec3::value_type>::max()),
                      const vec3& max=vec3(std::numeric_limits<vec3::value_type>::max()),
                      const vec3& step=vec3(0.001f),
                      const ivec3& precision=ivec3(3));
  void addVarAsVector(const char* name,
                      vec4* var,
                      const char* def="",
                      const vec4& min=vec4(-std::numeric_limits<vec4::value_type>::max()),
                      const vec4& max=vec4(std::numeric_limits<vec4::value_type>::max()),
                      const vec4& step=vec4(0.001f),
                      const ivec4& precision=ivec4(3));
  void addVarAsVector(const char* name,
                      const vec4* var,
                      const char* def="",
                      const vec4& min=vec4(-std::numeric_limits<vec4::value_type>::max()),
                      const vec4& max=vec4(std::numeric_limits<vec4::value_type>::max()),
                      const vec4& step=vec4(0.001f),
                      const ivec4& precision=ivec4(3));


  void addVarAsVector(const char* name,
                      ivec2* var,
                      const char* def="",
                      const ivec2& min=ivec2(std::numeric_limits<ivec2::value_type>::min()),
                      const ivec2& max=ivec2(std::numeric_limits<ivec2::value_type>::max()),
                      const ivec2& step=ivec2(1));
  void addVarAsVector(const char* name,
                      const ivec2* var,
                      const char* def="",
                      const ivec2& min=ivec2(std::numeric_limits<ivec2::value_type>::min()),
                      const ivec2& max=ivec2(std::numeric_limits<ivec2::value_type>::max()),
                      const ivec2& step=ivec2(1));
  void addVarAsVector(const char* name,
                      ivec3* var,
                      const char* def="",
                      const ivec3& min=ivec3(std::numeric_limits<ivec3::value_type>::min()),
                      const ivec3& max=ivec3(std::numeric_limits<ivec3::value_type>::max()),
                      const ivec3& step=ivec3(1));
  void addVarAsVector(const char* name,
                      const ivec3* var,
                      const char* def="",
                      const ivec3& min=ivec3(std::numeric_limits<ivec3::value_type>::min()),
                      const ivec3& max=ivec3(std::numeric_limits<ivec3::value_type>::max()),
                      const ivec3& step=ivec3(1));
  void addVarAsVector(const char* name,
                      ivec4* var,
                      const char* def="",
                      const ivec4& min=ivec4(std::numeric_limits<ivec4::value_type>::min()),
                      const ivec4& max=ivec4(std::numeric_limits<ivec4::value_type>::max()),
                      const ivec4& step=ivec4(1));
  void addVarAsVector(const char* name,
                      const ivec4* var,
                      const char* def="",
                      const ivec4& min=ivec4(std::numeric_limits<ivec4::value_type>::min()),
                      const ivec4& max=ivec4(std::numeric_limits<ivec4::value_type>::max()),
                      const ivec4& step=ivec4(1));


  void addSeperator(const char* name=nullptr, const char* def=nullptr);
  void addButton(const char* name, const std::function<void()>& callback, const char* def="");

  template<typename T>
  void setVarParam(const char* varName, const char* paramName, const T& value) const;

  template<typename T>
  T varParam(const char* varName, const char* paramName) const;

private:
  void setBlenderCoordinates(const char* varName);
  //@}
};

} // namespace TweakUI
} // namespace Framework

#include "tweak-ui-bar.inl"

#endif // FRAMEWORK_TWEAKUI_TWEAKUIBAR_H
