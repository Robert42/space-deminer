#ifndef FRAMEWORK_TWEAKUI_GLOBALPARAMETERS_H
#define FRAMEWORK_TWEAKUI_GLOBALPARAMETERS_H

#include "private/tweak-ui-parameters.h"

namespace Framework {
namespace TweakUI {
namespace Global {


enum IconPos
{
  ICONPOS_BOTTOM_LEFT,
  ICONPOS_BOTTOM_RIGHT,
  ICONPOS_TOP_LEFT,
  ICONPOS_TOP_RIGHT
};

enum Alignment
{
  HORIZONTAL,
  VERTICAL
};

enum FontSize : int
{
  FONT_SMALL = 1,
  FONT_MIDDLE = 2,
  FONT_LARGE = 3
};

enum ButtonAlign
{
  BUTTONALIGN_LEFT,
  BUTTONALIGN_CENTER,
  BUTTONALIGN_RIGHT
};


template<typename T>
void setParam(const char* paramName, const T& value)
{
  Private::setVarParam(nullptr, nullptr, paramName, value);
}


template<typename T>
T param(const char* paramName)
{
  return Private::varParam<T>(nullptr, nullptr, paramName);
}


std::string helpText();
void setHelpText(const char* help);
void setHelpText(const std::string& help);


IconPos iconPos();
void setIconPos(IconPos iconPos);


Alignment iconAlign();
void setIconAlign(Alignment alignment);


ivec2 iconMargin();
void setIconMargin(const ivec2& margin);


FontSize fontSize();
void setFontSize(FontSize fontSize);


bool isFontResizable();
void setFontResizable(bool resizable);


bool isContained();
void setContained(bool contained);


bool isOverlap();
void setOverlap(bool overlap);


ButtonAlign buttonAlign();
void setButtonAlign(ButtonAlign buttonAlign);


} // namespace TweakUI
} // namespace TweakUI
} // namespace Framework

#endif // FRAMEWORK_TWEAKUI_GLOBALPARAMETERS_INL
