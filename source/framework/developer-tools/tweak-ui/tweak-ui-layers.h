#ifndef FRAMEWORK_TWEAKUI_TWEAKUILAYERS_H
#define FRAMEWORK_TWEAKUI_TWEAKUILAYERS_H

#include <base/containers/layers.h>
#include <base/strings/string.h>

namespace Framework {

using namespace Base;

namespace TweakUI {

class Bar;

class Layers final
{
public:
  typedef shared_ptr<Layers> Ptr;

  class Layer : public Containers::BaseLayers::Layer
  {
  public:
    typedef shared_ptr<Layer> Ptr;
    typedef shared_ptr<const Layer> ConstPtr;

  private:
    QList<Bar*> bars;
    QSet<Bar*> barsToBeRestoredVisible;
    std::string _helpText;

    Layer();

  public:
    ~Layer();

    static Ptr create();

    void addBar(const std::shared_ptr<Bar>& bar);
    void addBar(Bar* bar);
    void removeBar(Bar* bar);

    void setHelpText(const std::string& text);
    void setHelpText(const String& text);
    const std::string& helpText() const;

  private:
    void handleShown();
    void handleHidden();
  };

public:
  Signals::Trackable trackable;

private:
  Containers::Layers<Layer> layers;

public:
  static Layer::Ptr other, sampleBrowser, nodeEditor, textureBrowser;

private:
  Layers();

public:
  static Ptr create();
};

} // namespace TweakUI
} // namespace Framework

#endif // FRAMEWORK_TWEAKUI_TWEAKUILAYERS_H
