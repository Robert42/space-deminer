#ifndef FRAMEWORK_TWEAKUI_TWEAKUIPARAMETERS_H
#define FRAMEWORK_TWEAKUI_TWEAKUIPARAMETERS_H

#include <dependencies.h>

namespace Framework {

using namespace Base;

namespace TweakUI {
namespace Private {

void setVarParam(::TweakUI::TwBar* twBar, const char* varName, const char* paramName, bool value);
void setVarParam(::TweakUI::TwBar* twBar, const char* varName, const char* paramName, int value);
void setVarParam(::TweakUI::TwBar* twBar, const char* varName, const char* paramName, const ivec2& value);
void setVarParam(::TweakUI::TwBar* twBar, const char* varName, const char* paramName, const ivec3& value);
void setVarParam(::TweakUI::TwBar* twBar, const char* varName, const char* paramName, const ivec4& value);
void setVarParam(::TweakUI::TwBar* twBar, const char* varName, const char* paramName, float value);
void setVarParam(::TweakUI::TwBar* twBar, const char* varName, const char* paramName, double value);
void setVarParam(::TweakUI::TwBar* twBar, const char* varName, const char* paramName, const vec2& value);
void setVarParam(::TweakUI::TwBar* twBar, const char* varName, const char* paramName, const vec3& value);
void setVarParam(::TweakUI::TwBar* twBar, const char* varName, const char* paramName, const vec4& value);
void setVarParam(::TweakUI::TwBar* twBar, const char* varName, const char* paramName, const char* value);

void varParam(::TweakUI::TwBar* twBar, const char* varName, const char* paramName, bool& value);
void varParam(::TweakUI::TwBar* twBar, const char* varName, const char* paramName, int& value);
void varParam(::TweakUI::TwBar* twBar, const char* varName, const char* paramName, ivec2& value);
void varParam(::TweakUI::TwBar* twBar, const char* varName, const char* paramName, ivec3& value);
void varParam(::TweakUI::TwBar* twBar, const char* varName, const char* paramName, ivec4& value);
void varParam(::TweakUI::TwBar* twBar, const char* varName, const char* paramName, float& value);
void varParam(::TweakUI::TwBar* twBar, const char* varName, const char* paramName, double& value);
void varParam(::TweakUI::TwBar* twBar, const char* varName, const char* paramName, vec2& value);
void varParam(::TweakUI::TwBar* twBar, const char* varName, const char* paramName, vec3& value);
void varParam(::TweakUI::TwBar* twBar, const char* varName, const char* paramName, vec4& value);
void varParam(::TweakUI::TwBar* twBar, const char* varName, const char* paramName, std::string& value);



template<typename T>
inline T varParam( ::TweakUI::TwBar* twBar, const char* varName, const char* paramName)
{
  T temp;
  varParam(twBar, varName, paramName, temp);
  return temp;
}


}
}
}

#endif
