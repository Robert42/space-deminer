#include "tweak-ui-parameters.h"

namespace Framework {
namespace TweakUI {
namespace Private {

using namespace ::TweakUI;


void setVarParam(TwBar* twBar, const char* varName, const char* paramName, bool value)
{
  setVarParam(twBar, varName, paramName, static_cast<int>(value));
}


void setVarParam(TwBar* twBar, const char* varName, const char* paramName, int value)
{
  TwSetParam(twBar, varName, paramName, TW_PARAM_INT32, 1, &value);
}


void setVarParam(TwBar* twBar, const char* varName, const char* paramName, const ivec2& value)
{
  TwSetParam(twBar, varName, paramName, TW_PARAM_INT32, 2, &value[0]);
}


void setVarParam(TwBar* twBar, const char* varName, const char* paramName, const ivec3& value)
{
  TwSetParam(twBar, varName, paramName, TW_PARAM_INT32, 3, &value[0]);
}


void setVarParam(TwBar* twBar, const char* varName, const char* paramName, const ivec4& value)
{
  TwSetParam(twBar, varName, paramName, TW_PARAM_INT32, 4, &value[0]);
}


void setVarParam(TwBar* twBar, const char* varName, const char* paramName, float value)
{
  TwSetParam(twBar, varName, paramName, TW_PARAM_FLOAT, 1, &value);
}


void setVarParam(TwBar* twBar, const char* varName, const char* paramName, double value)
{
  TwSetParam(twBar, varName, paramName, TW_PARAM_DOUBLE, 1, &value);
}


void setVarParam(TwBar* twBar, const char* varName, const char* paramName, const vec2& value)
{
  TwSetParam(twBar, varName, paramName, TW_PARAM_FLOAT, 2, &value[0]);
}


void setVarParam(TwBar* twBar, const char* varName, const char* paramName, const vec3& value)
{
  TwSetParam(twBar, varName, paramName, TW_PARAM_FLOAT, 3, &value[0]);
}


void setVarParam(TwBar* twBar, const char* varName, const char* paramName, const vec4& value)
{
  TwSetParam(twBar, varName, paramName, TW_PARAM_FLOAT, 4, &value[0]);
}


void setVarParam(TwBar* twBar, const char* varName, const char* paramName, const char* value)
{
  TwSetParam(twBar, varName, paramName, TW_PARAM_CSTRING, 1, value);
}


void varParam(TwBar* twBar, const char* varName, const char* paramName, bool& value)
{
  int tmp;
  varParam(twBar, varName, paramName, tmp);
  value = tmp!=0;
}


void varParam(TwBar* twBar, const char* varName, const char* paramName, int& value)
{
  TwGetParam(twBar, varName, paramName, TW_PARAM_INT32, 1, &value);
}


void varParam(TwBar* twBar, const char* varName, const char* paramName, ivec2& value)
{
  TwGetParam(twBar, varName, paramName, TW_PARAM_INT32, 2, &value[0]);
}


void varParam(TwBar* twBar, const char* varName, const char* paramName, ivec3& value)
{
  TwGetParam(twBar, varName, paramName, TW_PARAM_INT32, 3, &value[0]);
}


void varParam(TwBar* twBar, const char* varName, const char* paramName, ivec4& value)
{
  TwGetParam(twBar, varName, paramName, TW_PARAM_INT32, 4, &value[0]);
}


void varParam(TwBar* twBar, const char* varName, const char* paramName, float& value)
{
  TwGetParam(twBar, varName, paramName, TW_PARAM_FLOAT, 1, &value);
}


void varParam(TwBar* twBar, const char* varName, const char* paramName, double& value)
{
  TwGetParam(twBar, varName, paramName, TW_PARAM_DOUBLE, 1, &value);
}


void varParam(TwBar* twBar, const char* varName, const char* paramName, vec2& value)
{
  TwGetParam(twBar, varName, paramName, TW_PARAM_FLOAT, 2, &value[0]);
}


void varParam(TwBar* twBar, const char* varName, const char* paramName, vec3& value)
{
  TwGetParam(twBar, varName, paramName, TW_PARAM_FLOAT, 3, &value[0]);
}


void varParam(TwBar* twBar, const char* varName, const char* paramName, vec4& value)
{
  TwGetParam(twBar, varName, paramName, TW_PARAM_FLOAT, 4, &value[0]);
}


void varParam(TwBar* twBar, const char* varName, const char* paramName, std::string& value)
{
  char temp[128];
  TwGetParam(twBar, varName, paramName, TW_PARAM_CSTRING, sizeof(temp), temp);
  value = temp;
}


}
}
}
