#include "tweak-ui-bar.h"
#include "tweak-ui-global-parameters.h"

namespace Framework {
namespace TweakUI {

Layers::Layer::Ptr Layers::other;
Layers::Layer::Ptr Layers::sampleBrowser;
Layers::Layer::Ptr Layers::nodeEditor;
Layers::Layer::Ptr Layers::textureBrowser;

Layers::Layers()
{
  layers.pushTopLayer(other = Layer::create());
  layers.pushTopLayer(sampleBrowser = Layer::create());
  layers.pushTopLayer(nodeEditor = Layer::create());
  layers.pushTopLayer(textureBrowser = Layer::create());
}

Layers::Ptr Layers::create()
{
  return Ptr(new Layers);
}

// ====

Layers::Layer::Layer()
  : Containers::BaseLayers::Layer(true)
{
  signalGloballyHidden().connect(std::bind(&Layers::Layer::handleHidden, this)).trackManually();
  signalGloballyShown().connect(std::bind(&Layers::Layer::handleShown, this)).trackManually();
}

Layers::Layer::Ptr Layers::Layer::create()
{
  return Ptr(new Layer);
}

Layers::Layer::~Layer()
{
  signalGloballyHidden().disconnectAll();
  signalGloballyShown().disconnectAll();

  for(Bar* bar : bars)
  {
    bar->_layer = nullptr;
  }
}

void Layers::Layer::addBar(const std::shared_ptr<Bar>& bar)
{
  addBar(bar.get());
}

void Layers::Layer::addBar(Bar* bar)
{
  if(bar->_layer != nullptr)
    bar->_layer->removeBar(bar);
  bar->_layer = this;

  bars.append(bar);

  if(!isGloballyVisible() && bar->isVisible())
  {
    bar->setVisible(false);
    barsToBeRestoredVisible.insert(bar);
  }

  if(isGloballyVisible() && !bar->isVisible())
  {
    bar->setVisible(true);
  }
}

void Layers::Layer::removeBar(Bar* bar)
{
  bar->_layer = nullptr;

  bars.removeAll(bar);
  barsToBeRestoredVisible.remove(bar);
}

void Layers::Layer::handleShown()
{
  if(!helpText().empty())
    Global::setHelpText(helpText());
  for(Bar* bar : bars)
  {
    if(barsToBeRestoredVisible.contains(bar))
      bar->setVisible(true);
  }
  barsToBeRestoredVisible.clear();
}

void Layers::Layer::handleHidden()
{
  barsToBeRestoredVisible.clear();
  for(Bar* bar : bars)
  {
    if(bar->isVisible())
    {
      barsToBeRestoredVisible.insert(bar);
      bar->setVisible(false);
    }
  }
}

void Layers::Layer::setHelpText(const std::string& text)
{
  _helpText = text;

  if(isGloballyVisible())
    Global::setHelpText(helpText());
}

void Layers::Layer::setHelpText(const String& text)
{
  setHelpText(text.toUtf8String());
}

const std::string& Layers::Layer::helpText() const
{
  return _helpText;
}

} // namespace TweakUI
} // namespace Framework
