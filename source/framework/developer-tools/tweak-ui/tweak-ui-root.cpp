#include "tweak-ui-root.h"
#include "tweak-ui-layers.h"

#include <framework/developer-tools/developer-hud/tweak-ui/roto-cursor-resetter.h>
#include <framework/developer-tools/developer-hud/tweak-ui/gorilla/tweak-ui-renderer.h>
#include <framework/developer-tools/developer-hud/tweak-ui/gorilla/texture-atlas.h>
#include <framework/window/render-window.h>
#include <base/optional.h>

#include <TweakUI.h>
#include <external/TweakUI/src/TwGraph.h>

namespace Framework {
namespace TweakUI {

using namespace ::TweakUI;

Optional<TwKeySpecial> translateKeyCode(const KeyCode& code);
ETwKeyModifier translateKeyModifier();

class ClipboardHandler : public ::TweakUI::ClipboardHandler
{
public:
  void setClipboardValue(const std::string& _String) override
  {
    RenderWindow::setClipboardValue(String::fromAnsi(_String));
  }

  bool getClipboardValue(std::string* _String) override
  {
    if(!RenderWindow::hasClipboardValue())
      return false;
    *_String = RenderWindow::clipboardValue().toAnsiString();
    return true;
  }
}clipboardHandler;

TwMouseButtonID getTwMouseButtonID(MouseButton mouseButton);

class GorillaTweakUIGraph : public ITwGraph
{
private:
  struct Vertex
  {
  public:
    bool useColour;
    Ogre::ColourValue colour;
    Ogre::Vector2 position;
    Ogre::Vector2 uv;

    Vertex()
      : useColour(false),
        colour(Ogre::ColourValue::White)
    {
    }
  };
  class TextObj
  {
  public:
    ::Gorilla::buffer<Vertex> vertices, backgroundVertices;
  };

public:
  Gorilla::TweakUiRenderer* renderer;
  Gorilla::TextureAtlas* textureAtlas;
  const CTexFont* currentFontTexture;
  bool isDrawing;

  Ogre::Vector2 lineOffset;
  Ogre::Vector2 scissorPosOffset;
  Ogre::Vector2 scissorSizeOffset;

public:
  GorillaTweakUIGraph(Gorilla::TweakUiRenderer* renderer);

  int Init() override;
  int Shut() override;

  void BeginDraw(int, int) override;
  void EndDraw() override;

  bool IsDrawing() override;
  void Restore() override;

  void DrawLine(int _X0, int _Y0, int _X1, int _Y1, color32 _Color0, color32 _Color1, bool _AntiAliased=false) override;
  void DrawLine(int _X0, int _Y0, int _X1, int _Y1, color32 _Color, bool _AntiAliased=false) override;
  void DrawRect(int _X0, int _Y0, int _X1, int _Y1, color32 _Color00, color32 _Color10, color32 _Color01, color32 _Color11) override;
  void DrawRect(int _X0, int _Y0, int _X1, int _Y1, color32 _Color) override;
  void DrawTriangles(int _NumTriangles, int* _Vertices, color32* _Colors, Cull _CullMode) override;

  void* NewTextObj();
  void DeleteTextObj(void* _TextObj);

  void BuildText(void* _TextObj, const std::string* _TextLines, color32* _LineColors, color32* _LineBgColors, int _NbLines, const CTexFont* _Font, int _Sep, int _BgWidth) override;
  void DrawText(void* _TextObj, int _X, int _Y, color32 _Color, color32 _BgColor) override;

  void ChangeViewport(int _X0, int _Y0, int _Width, int _Height, int _OffsetX, int _OffsetY) override;
  void RestoreViewport() override;
  void SetScissor(int _X0, int _Y0, int _Width, int _Height) override;

  void pushQuads(const ::Gorilla::buffer<Vertex>& vertices, const Ogre::Vector2& offset, const Ogre::ColourValue& color);

private:
  void loadFont(const CTexFont* _Font);
};

inline void updateWindowSize()
{
  ivec2 size = RenderWindow::size();

  TwWindowSize(size.x, size.y);
}


class RootImplementation final : public Root
{
public:
  Signals::Trackable trackable;

private:
  Signals::Connection windowResizeConnection;

  Signals::Connection mouseMoveConnection;
  Signals::Connection mouseButtonPressedConnection;
  Signals::Connection mouseButtonReleasedConnection;
  Signals::Connection mouseWheelConnection;
  Signals::Connection keyPressedConnection;
  Signals::Connection keyReleasedConnection;
  Signals::Connection unicodeEnteredConnection;

  shared_ptr<RotoCursorResetter> rotoCursorResetter;

  DeveloperHud::Layer& devHudLayer;
  shared_ptr<Layers> barLayers;

public:
  RootImplementation(Gorilla::TweakUiRenderer* renderer, DeveloperHud::Layer* devHudLayer);

  ~RootImplementation();

private:
  void startHandlingInput() override;
  void stopHandlingInput() override;

  void setVisible(bool show) override;

private:
  bool handleMouseMove();
  bool handleMouseButtonAction(bool pressed, MouseButton mouseButton);
  bool handleMouseWheel();
  bool handleKeyPressed(KeyCode keyCode);
  bool handleKeyReleased(KeyCode keyCode);
  bool handleTextInput(String::value_type character);
};


RootImplementation::RootImplementation(Gorilla::TweakUiRenderer* renderer, DeveloperHud::Layer* devHudLayer)
  : devHudLayer(*devHudLayer)
{
  ::TweakUI::ClipboardHandler::handler = &clipboardHandler;

  rotoCursorResetter = RotoCursorResetter::create();

  ::TweakUI::TwSetCustomGraph(new GorillaTweakUIGraph(renderer));
  ::TweakUI::TwInit(TW_OPENGL, NULL);

  updateWindowSize();

  windowResizeConnection = RenderWindow::signalWindowResized().connect(std::bind(updateWindowSize)).trackManually();

  barLayers = Layers::create();
}


RootImplementation::~RootImplementation()
{
  barLayers.reset();

  stopHandlingInput();

  windowResizeConnection.disconnect();

  ::TweakUI::TwDeleteAllBars();
  ::TweakUI::TwTerminate();
}

void RootImplementation::stopHandlingInput()
{
  mouseMoveConnection.disconnect();
  mouseButtonPressedConnection.disconnect();
  mouseButtonReleasedConnection.disconnect();
  mouseWheelConnection.disconnect();
  keyPressedConnection.disconnect();
  keyReleasedConnection.disconnect();
  unicodeEnteredConnection.disconnect();
}


Root::Ptr Root::create(Gorilla::TweakUiRenderer* renderer, DeveloperHud::Layer* devHudLayer)
{
  return setSingleton(Ptr(new RootImplementation(renderer, devHudLayer)));
}


void Root::show()
{
  setVisible(true);
}


void Root::hide()
{
  setVisible(false);
}


void RootImplementation::startHandlingInput()
{
  stopHandlingInput();

  InputDevice::ActionPriority priority = InputDevice::ActionPriority::TWEAK_UI;

  mouseMoveConnection = Mouse::signalMouseMoved().connect(priority, std::bind(&RootImplementation::handleMouseMove, this)).track(this->trackable);
  mouseButtonPressedConnection = Mouse::signalButtonPressed().connect(priority, std::bind(&RootImplementation::handleMouseButtonAction, this, true, _1)).track(this->trackable);
  mouseButtonReleasedConnection = Mouse::signalButtonReleased().connect(priority, std::bind(&RootImplementation::handleMouseButtonAction, this, false, _1)).track(this->trackable);
  mouseWheelConnection = Mouse::signalMouseWheelMoved().connect(priority, std::bind(&RootImplementation::handleMouseWheel, this)).track(this->trackable);
  keyPressedConnection = Keyboard::signalKeyPressed().connect(priority, std::bind(&RootImplementation::handleKeyPressed, this, _1)).track(this->trackable);
  keyReleasedConnection = Keyboard::signalKeyReleased().connect(priority, std::bind(&RootImplementation::handleKeyReleased, this, _1)).track(this->trackable);
  unicodeEnteredConnection = Keyboard::signalUnicodeEntered().connect(priority, std::bind(&RootImplementation::handleTextInput, this, _1)).track(this->trackable);
}


bool RootImplementation::handleMouseMove()
{
  ivec2 mousePosition = Mouse::absolutePosition();

  if(TwMouseMotion(mousePosition.x, mousePosition.y))
  {
    rotoCursorResetter->afterHandlingMouseMovement();
    return true;
  }

  return false;
}


bool RootImplementation::handleMouseButtonAction(bool pressed, MouseButton mouseButton)
{
  TwMouseAction action = pressed ? TW_MOUSE_PRESSED : TW_MOUSE_RELEASED;
  TwMouseButtonID buttonId;

  switch(mouseButton.value)
  {
  case MouseButton::LEFT:
    buttonId = TW_MOUSE_LEFT;
    break;
  case MouseButton::RIGHT:
    buttonId = TW_MOUSE_RIGHT;
    break;
  case MouseButton::MIDDLE:
    buttonId = TW_MOUSE_MIDDLE;
    break;
  default:
    return false;
  }

  if(TwMouseButton(action, buttonId))
  {
    rotoCursorResetter->afterHandlingMouseButton(pressed, mouseButton);
    return true;
  }

  return false;
}


bool RootImplementation::handleMouseWheel()
{
  static int currentWheelPosition = 0;
  currentWheelPosition += Mouse::wheelMovement();

  return TwMouseWheel(currentWheelPosition);
}


bool RootImplementation::handleKeyPressed(KeyCode keyCode)
{
  Optional<EKeySpecial> key = translateKeyCode(keyCode);

  if(!key)
    return false;

  return TwKeyPressed(*key, translateKeyModifier());
}


bool RootImplementation::handleTextInput(String::value_type character)
{
  /* Decided not to use the text events to handle alphanumeric inputs,
   * because it would get error prone to handle shortcuts using key modifiers
   * which might not send text events when having certain modifiers on certain
   * systems pressed.
  */
  if(character > 127 ||
     character=='^' ||
     (character>='a' && character<='z') ||
     (character>='A' && character<='Z') ||
     (character>='0' && character<='9'))
    return false;

  return TwKeyPressed(int(character), translateKeyModifier());
}


bool RootImplementation::handleKeyReleased(KeyCode)
{
  return false;
}


void RootImplementation::setVisible(bool show)
{
  devHudLayer.setVisible(show);
}


GorillaTweakUIGraph::GorillaTweakUIGraph(Gorilla::TweakUiRenderer* renderer)
  : renderer(renderer),
    textureAtlas(renderer->textureAtlas())
{
  currentFontTexture = nullptr;
  isDrawing = false;

  Ogre::RenderSystemCapabilities* capabilities = Ogre::Root::getSingleton().getRenderSystem()->createRenderSystemCapabilities();

  lineOffset = Ogre::Vector2(0.f, 0.f);
  scissorPosOffset = Ogre::Vector2(0.f, 0.f);
  scissorSizeOffset = Ogre::Vector2(0.f, 0.f);

  if(capabilities->getVendor() == Ogre::GPU_INTEL)
  {
    lineOffset = Ogre::Vector2(0.f, 0.5f);
    scissorSizeOffset = Ogre::Vector2(0.f, -1.f);
  }
  if(capabilities->getVendor() == Ogre::GPU_NVIDIA)
  {
    scissorSizeOffset = Ogre::Vector2(0.f, -1.f);
  }

  OGRE_DELETE capabilities;
}

int GorillaTweakUIGraph::Init()
{
  return 1;
}

int GorillaTweakUIGraph::Shut()
{
  return 1;
}

void GorillaTweakUIGraph::BeginDraw(int, int)
{
  isDrawing = true;
}

void GorillaTweakUIGraph::EndDraw()
{
  isDrawing = false;
}

bool GorillaTweakUIGraph::IsDrawing()
{
  return isDrawing;
}

void GorillaTweakUIGraph::Restore()
{
}


void GorillaTweakUIGraph::DrawLine(int _X0, int _Y0, int _X1, int _Y1, color32 _Color0, color32 _Color1, bool _AntiAliased)
{
  Ogre::ColourValue color[2];
  color[0].setAsARGB(_Color0);
  color[1].setAsARGB(_Color1);

  renderer->pushLine(Ogre::Vector2(_X0, _Y0)+lineOffset,
                     Ogre::Vector2(_X1, _Y1)+lineOffset,
                     color[0],
                     color[1],
                     1.0f,
                     _AntiAliased);
}

void GorillaTweakUIGraph::DrawLine(int _X0, int _Y0, int _X1, int _Y1, color32 _Color, bool _AntiAliased)
{
  Ogre::ColourValue color;
  color.setAsARGB(_Color);

  renderer->pushLine(Ogre::Vector2(_X0, _Y0)+lineOffset,
                     Ogre::Vector2(_X1, _Y1)+lineOffset,
                     color,
                     1.0f,
                     _AntiAliased);
}

void GorillaTweakUIGraph::DrawRect(int _X0, int _Y0, int _X1, int _Y1, color32 _Color00, color32 _Color10, color32 _Color01, color32 _Color11)
{
  Ogre::Vector2 positions[4] = {Ogre::Vector2(_X0, _Y0),
                                Ogre::Vector2(_X0, _Y1),
                                Ogre::Vector2(_X1, _Y1),
                                Ogre::Vector2(_X1, _Y0)
                               };
  Ogre::ColourValue color[4];
  color[0].setAsARGB(_Color00);
  color[1].setAsARGB(_Color01);
  color[2].setAsARGB(_Color11);
  color[3].setAsARGB(_Color10);
  renderer->pushQuad(positions, color);
}

void GorillaTweakUIGraph::DrawRect(int _X0, int _Y0, int _X1, int _Y1, color32 _Color)
{
  Ogre::Vector2 positions[4] = {Ogre::Vector2(_X0, _Y0),
                                Ogre::Vector2(_X0, _Y1),
                                Ogre::Vector2(_X1, _Y1),
                                Ogre::Vector2(_X1, _Y0)
                               };
  Ogre::ColourValue color;
  color.setAsARGB(_Color);
  renderer->pushQuad(positions, color);
}

void GorillaTweakUIGraph::DrawTriangles(int _NumTriangles, int* _Vertices, color32* _Colors, Cull _CullMode)
{
  for(int i=0; i<_NumTriangles; ++i)
  {
    Ogre::Vector2 positions[3] = {Ogre::Vector2(_Vertices[i*6+0], _Vertices[i*6+1]),
                                  Ogre::Vector2(_Vertices[i*6+2], _Vertices[i*6+3]),
                                  Ogre::Vector2(_Vertices[i*6+4], _Vertices[i*6+5])
                                 };
    Ogre::ColourValue colors[3];
    for(int j=0; j<3; ++j)
      colors[j].setAsARGB(_Colors[i*3+j]);

    if(_CullMode != CULL_NONE)
    {
      Ogre::Vector2 a = positions[1]-positions[0];
      Ogre::Vector2 b = positions[2]-positions[0];
      Ogre::Vector3 c = Ogre::Vector3(a.x, a.y, 0.f).crossProduct(Ogre::Vector3(b.x, b.y, 0.f));

      Ogre::Real zDirection = c.dotProduct(Ogre::Vector3(0.f, 0.f, 1.f));

      if(_CullMode==CULL_CCW && zDirection<0.f)
        continue;
      if(_CullMode==CULL_CW && zDirection>0.f)
        continue;
    }

    renderer->pushTriangle(positions, colors);
  }
}


void* GorillaTweakUIGraph::NewTextObj()
{
  return new TextObj;
}

void GorillaTweakUIGraph::DeleteTextObj(void* _TextObj)
{
  TextObj* textObj = static_cast<TextObj*>(_TextObj);
  delete textObj;
}

void GorillaTweakUIGraph::BuildText(void* _TextObj, const std::string* _TextLines, color32* _LineColors, color32* _LineBgColors, int _NbLines, const CTexFont* _Font, int _Sep, int _BgWidth)
{
  loadFont(_Font);

  TextObj* textObj = static_cast<TextObj*>(_TextObj);

  textObj->vertices.remove_all();
  textObj->backgroundVertices.remove_all();

  ivec2 lineStart(0, 0);
  for(int lineId=0; lineId<_NbLines; ++lineId)
  {
    const std::string& textLine = _TextLines[lineId];

    ivec2 characterNW = lineStart;
    ivec2 characterSE = characterNW+ivec2(0, _Font->m_CharHeight);

    Vertex lineVertex;

    if(_LineColors && (_LineColors[lineId]&0xff000000)>0)
    {
      lineVertex.useColour = true;
      lineVertex.colour.setAsARGB(_LineColors[lineId]);
    }else
    {
      lineVertex.useColour = false;
    }

    if(_BgWidth>0)
    {
      Vertex backGroundVertex;
      backGroundVertex.uv = renderer->getSolidUV();

      if(_LineBgColors)
      {
        backGroundVertex.useColour = true;
        backGroundVertex.colour.setAsARGB(_LineBgColors[lineId]);
      }else
      {
        backGroundVertex.useColour = false;
      }

      ivec2 backSE = ivec2(characterNW.x+_BgWidth, characterSE.y);

      backGroundVertex.position = Ogre::Vector2(characterNW.x, characterNW.y);
      textObj->backgroundVertices.push_back(backGroundVertex);
      backGroundVertex.position = Ogre::Vector2(characterNW.x, backSE.y);
      textObj->backgroundVertices.push_back(backGroundVertex);
      backGroundVertex.position = Ogre::Vector2(backSE.x, backSE.y);
      textObj->backgroundVertices.push_back(backGroundVertex);
      backGroundVertex.position = Ogre::Vector2(backSE.x, characterNW.y);
      textObj->backgroundVertices.push_back(backGroundVertex);
    }

    for(std::string::value_type c : textLine)
    {
      size_t index = static_cast<size_t>(c);

      int characterWidth = _Font->m_CharWidth[index];
      characterSE.x += characterWidth;

      Ogre::Vector2 uvNW(_Font->m_CharU0[index], _Font->m_CharV0[index]);
      Ogre::Vector2 uvSE(_Font->m_CharU1[index], _Font->m_CharV1[index]);

      lineVertex.position = Ogre::Vector2(characterNW.x, characterNW.y);
      lineVertex.uv = Ogre::Vector2(uvNW.x, uvNW.y);
      textObj->vertices.push_back(lineVertex);
      lineVertex.position = Ogre::Vector2(characterNW.x, characterSE.y);
      lineVertex.uv = Ogre::Vector2(uvNW.x, uvSE.y);
      textObj->vertices.push_back(lineVertex);
      lineVertex.position = Ogre::Vector2(characterSE.x, characterSE.y);
      lineVertex.uv = Ogre::Vector2(uvSE.x, uvSE.y);
      textObj->vertices.push_back(lineVertex);
      lineVertex.position = Ogre::Vector2(characterSE.x, characterNW.y);
      lineVertex.uv = Ogre::Vector2(uvSE.x, uvNW.y);
      textObj->vertices.push_back(lineVertex);

      characterNW.x += characterWidth;
    }

    lineStart.y += _Sep+_Font->m_CharHeight;
  }
}

void GorillaTweakUIGraph::DrawText(void* _TextObj, int _X, int _Y, color32 _Color, color32 _BgColor)
{
  TextObj* textObj = static_cast<TextObj*>(_TextObj);

  Ogre::ColourValue color;
  Ogre::Vector2 offset(_X, _Y);

  color.setAsARGB(_BgColor);
  pushQuads(textObj->backgroundVertices, offset, color);

  color.setAsARGB(_Color);
  pushQuads(textObj->vertices, offset, color);
}

void GorillaTweakUIGraph::pushQuads(const ::Gorilla::buffer<Vertex>& vertices, const Ogre::Vector2& offset, const Ogre::ColourValue& color)
{
  size_t nQuads = vertices.size() / 4;
  size_t j=0;

  for(size_t i=0; i<nQuads; ++i)
  {
    const Vertex& a = vertices[j++];
    const Vertex& b = vertices[j++];
    const Vertex& c = vertices[j++];
    const Vertex& d = vertices[j++];

    Ogre::ColourValue colors[4];
    Ogre::Vector2 positions[4] = {a.position+offset, b.position+offset, c.position+offset, d.position+offset};
    Ogre::Vector2 uvs[4] = {a.uv, b.uv, c.uv, d.uv};

    if(color.a > 0)
    {
      for(int i=0; i<4; ++i)
        colors[i] = color;
    }else if(a.useColour)
    {
      colors[0] = a.colour;
      colors[1] = b.colour;
      colors[2] = c.colour;
      colors[3] = d.colour;
    }else
    {
      continue;
    }

    renderer->pushQuad(positions, colors, uvs);
  }
}


void GorillaTweakUIGraph::ChangeViewport(int _X0, int _Y0, int _Width, int _Height, int _OffsetX, int _OffsetY)
{
  renderer->setViewport(_X0, _Y0, _Width, _Height, _OffsetX, _OffsetY);
}

void GorillaTweakUIGraph::RestoreViewport()
{
  renderer->setInfiniteViewport();
}

void GorillaTweakUIGraph::SetScissor(int _X0, int _Y0, int _Width, int _Height)
{
  if(_X0==0 && _Y0==0 && _Width==0 && _Height==0)
    renderer->setInfiniteScissor();
  else
    renderer->setScissor(_X0+scissorPosOffset.x,
                         _Y0+scissorPosOffset.y,
                         _Width+scissorSizeOffset.y,
                         _Height+scissorSizeOffset.y);
}

void GorillaTweakUIGraph::loadFont(const CTexFont* _Font)
{
  if(_Font == currentFontTexture)
    return;

  currentFontTexture = _Font;

  // Set a whitepixel

  vec2 maxUV(0.f, 0.f);
  for(int i=0; i<256; ++i)
  {
    maxUV = max(maxUV, max(vec2(_Font->m_CharU0[i], _Font->m_CharV0[i]), vec2(_Font->m_CharU1[i], _Font->m_CharV1[i])));
  }

  ivec2 textureSize(_Font->m_TexWidth, _Font->m_TexHeight);

  bool canWriteWhitePixelsAtTheRight = textureSize.x *(1.f-maxUV.x)>5;
  bool canWriteWhitePixelsAtTheBottom = textureSize.y*(1.f-maxUV.y)>5;

  assert(canWriteWhitePixelsAtTheRight || canWriteWhitePixelsAtTheBottom);

  std::vector<unsigned char> textureData(_Font->m_TexBytes, _Font->m_TexBytes+textureSize.x*textureSize.y);

  ivec2 whitePoint = textureSize-ivec2(3);

  auto setToWhite = [&textureData,textureSize](const ivec2 &coord){
    textureData[coord.y*textureSize.y + coord.x] = 255;
  };
  auto reformat = [&textureData,textureSize](const ivec2 &coord){
    unsigned char& currentPixel = textureData[coord.y*textureSize.y + coord.x];
    currentPixel = ((currentPixel>>4)<<4) | 0x0f;
  };

  ivec2 pixel;
  for(pixel.x=-1; pixel.x<=1; ++pixel.x)
    for(pixel.y=-1; pixel.y<=1; ++pixel.y)
      setToWhite(whitePoint+pixel);

  for(pixel.x=0; pixel.x<=255; ++pixel.x)
    for(pixel.y=0; pixel.y<=255; ++pixel.y)
      reformat(pixel);

  textureAtlas->exchangeTexture(textureSize, textureData, whitePoint);
}


Optional<TwKeySpecial> translateKeyCode(const KeyCode& keyCode)
{
  KeyCode::Value code = keyCode.value;
  switch(code)
  {
  case KeyCode::BACKSPACE:
    return TW_KEY_BACKSPACE;
  case KeyCode::TAB:
    return TW_KEY_TAB;
  //case ???:
  //  return TW_KEY_CLEAR;
  case KeyCode::RETURN:
    return TW_KEY_RETURN;
  case KeyCode::PAUSE:
    return TW_KEY_PAUSE;
  case KeyCode::ESCAPE:
    return TW_KEY_ESCAPE;
  case KeyCode::SPACE:
    return TW_KEY_SPACE;
  case KeyCode::DELETE:
    return TW_KEY_DELETE;
  case KeyCode::ARROW_UP:
    return TW_KEY_UP;
  case KeyCode::ARROW_DOWN:
    return TW_KEY_DOWN;
  case KeyCode::ARROW_LEFT:
    return TW_KEY_LEFT;
  case KeyCode::ARROW_RIGHT:
    return TW_KEY_RIGHT;
  case KeyCode::INSERT:
    return TW_KEY_INSERT;
  case KeyCode::HOME:
    return TW_KEY_HOME;
  case KeyCode::END:
    return TW_KEY_END;
  case KeyCode::PAGEUP:
    return TW_KEY_PAGE_UP;
  case KeyCode::PAGEDOWN:
    return TW_KEY_PAGE_DOWN;
  case KeyCode::F1:
    return TW_KEY_F1;
  case KeyCode::F2:
    return TW_KEY_F2;
  case KeyCode::F3:
    return TW_KEY_F3;
  case KeyCode::F4:
    return TW_KEY_F4;
  case KeyCode::F5:
    return TW_KEY_F5;
  case KeyCode::F6:
    return TW_KEY_F6;
  case KeyCode::F7:
    return TW_KEY_F7;
  case KeyCode::F8:
    return TW_KEY_F8;
  case KeyCode::F9:
    return TW_KEY_F9;
  case KeyCode::F10:
    return TW_KEY_F10;
  case KeyCode::F11:
    return TW_KEY_F11;
  case KeyCode::F12:
    return TW_KEY_F12;
  default:
    if(code >= KeyCode::A && code <= KeyCode::Z)
    {
      char base;

      if(Keyboard::isModifierDown(KeyModifier::SHIFT) || Keyboard::isModifierDown(KeyModifier::CAPSLOCK))
        base = 'A';
      else
        base = 'a';

      return TwKeySpecial(int(code-KeyCode::A)+base);
    }
    if(code >= KeyCode::KEYPAD_0 && code <= KeyCode::KEYPAD_9)
    {
      return TwKeySpecial(int(code-KeyCode::KEYPAD_0)+'0');
    }
    if(code >= KeyCode::NUMPAD_0 && code <= KeyCode::NUMPAD_9)
    {
      return TwKeySpecial(int(code-KeyCode::NUMPAD_0)+'0');
    }
    return nothing;
  }
}

ETwKeyModifier translateKeyModifier()
{
  int m = 0;

  if(Keyboard::isModifierDown(KeyModifier::SHIFT))
    m |= TW_KMOD_SHIFT;
  if(Keyboard::isModifierDown(KeyModifier::CONTROL))
    m |= TW_KMOD_CTRL;
  if(Keyboard::isModifierDown(KeyModifier::ALT))
    m |= TW_KMOD_ALT;
  if(Keyboard::isModifierDown(KeyModifier::GUI))
    m |= TW_KMOD_META;

  return ETwKeyModifier(m);
}


} // namespace TweakUI
} // namespace Framework
