#ifndef FRAMEWORK_TWEAKUI_ENUMERATEDVARIABLE_H
#define FRAMEWORK_TWEAKUI_ENUMERATEDVARIABLE_H

#include <base/signals/signal.h>

#include "tweak-ui-bar.h"

namespace Framework {

using namespace Base;

namespace TweakUI {

class EnumeratedVariable final : noncopyable
{
public:
  typedef std::shared_ptr<EnumeratedVariable> Ptr;

private:
  QMap<int, std::string> itemsText;
  int _currentIndex;
  const Bar::Ptr bar;
  const std::string name;

  Signals::CallableSignal<void(int)> _signalIndexChanged;
  Signals::CallableSignal<void(const std::string&)> _signalTextChanged;

public:
  EnumeratedVariable(const char* typeName, const char* name, const Bar::Ptr& bar, const char* def=nullptr);
  ~EnumeratedVariable();

  static Ptr create(const char* typeName, const char* name, const Bar::Ptr& bar, const char* def=nullptr);

  void add(int index, const std::string& text);
  int append(const std::string& text);

  void remove(const std::string& text);
  void remove(int i);

  void clear();

  int indexOf(const std::string& text, int fallback=-1) const;

public:
  int currentIndex() const;
  void setCurrentIndex(int index);

  void setCurrentText(const std::string& text);
  std::string currentText(const std::string& fallback=std::string()) const;

  Signals::Signal<void(int)>& signalIndexChanged();
  Signals::Signal<void(const std::string&)>& signalTextChanged();

private:
  std::string generateTweakUIEnumParamterValue() const;

  static void getter(void* value, void* object);
  static void setter(const void* value, void* object);

  void updateEnumeration();
};

} // namespace TweakUI
} // namespace Framework

#endif // FRAMEWORK_TWEAKUI_ENUMERATEDVARIABLE_H
