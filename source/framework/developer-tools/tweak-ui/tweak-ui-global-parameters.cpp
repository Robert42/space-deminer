#include "tweak-ui-global-parameters.h"

namespace Framework {
namespace TweakUI {
namespace Global {


std::string helpText()
{
  return param<std::string>("help");
}

void setHelpText(const char* help)
{
  setParam("help", help);
}

void setHelpText(const std::string& help)
{
  setHelpText(help.c_str());
}


IconPos iconPos()
{
  std::string str = param<std::string>("iconpos");

  if(str=="tl" || str=="topleft")
    return ICONPOS_TOP_LEFT;
  else if(str=="tr" || str=="topright")
    return ICONPOS_TOP_RIGHT;
  else if(str=="br" || str=="bottomright")
    return ICONPOS_BOTTOM_RIGHT;
  else
    return ICONPOS_BOTTOM_LEFT;
}


void setIconPos(IconPos iconPos)
{
  switch(iconPos)
  {
  case ICONPOS_TOP_LEFT:
    setParam("iconpos", "tl");
    break;
  case ICONPOS_TOP_RIGHT:
    setParam("iconpos", "tr");
    break;
  case ICONPOS_BOTTOM_RIGHT:
    setParam("iconpos", "br");
    break;
  default:
    setParam("iconpos", "bl");
  }
}


Alignment iconAlign()
{
  std::string str = param<std::string>("iconalign");

  if(!str.empty() && str[0]=='h')
    return HORIZONTAL;
  else
    return VERTICAL;
}

void setIconAlign(Alignment alignment)
{
  switch(alignment)
  {
  case HORIZONTAL:
    setParam("iconalign", "horizontal");
    break;
  case VERTICAL:
  default:
    setParam("iconalign", "vertical");
  }
}


ivec2 iconMargin()
{
  return param<ivec2>("iconmargin");
}


void setIconMargin(const ivec2& margin)
{
  setParam<ivec2>("iconmargin", margin);
}


FontSize fontSize()
{
  return static_cast<FontSize>(param<int>("fontsize"));
}


void setFontSize(FontSize fontSize)
{
  setParam<int>("fontsize", fontSize);
}


bool isFontResizable()
{
  return param<bool>("fontresizable");
}


void setFontResizable(bool resizable)
{
  setParam("fontresizable", resizable);
}


bool isContained()
{
  return param<bool>("contained");
}


void setContained(bool contained)
{
  setParam("contained", contained);
}


bool isOverlap()
{
  return param<bool>("overlap");
}


void setOverlap(bool overlap)
{
  setParam<bool>("overlap", overlap);
}


ButtonAlign buttonAlign()
{
  std::string str = param<std::string>("iconpos");

  if(str=="left")
    return BUTTONALIGN_LEFT;
  else if(str=="center")
    return BUTTONALIGN_CENTER;
  else
    return BUTTONALIGN_RIGHT;
}


void setButtonAlign(ButtonAlign buttonAlign)
{
  switch(buttonAlign)
  {
  case BUTTONALIGN_LEFT:
    setParam("buttonalign", "left");
    break;
  case BUTTONALIGN_CENTER:
    setParam("buttonalign", "center");
    break;
  case BUTTONALIGN_RIGHT:
  default:
    setParam("buttonalign", "right");
  }
}


} // namespace TweakUI
} // namespace TweakUI
} // namespace Framework
