#ifndef FRAMEWORK_TWEAKUI_TWEAKUIROOT_H
#define FRAMEWORK_TWEAKUI_TWEAKUIROOT_H

#include <framework/developer-tools/developer-hud.h>

#include <base/singleton.h>

namespace Framework {
namespace TweakUI {

namespace Gorilla {
class TweakUiRenderer;
}

class Root : public SharedSingleton<Root>
{
  friend class Bar;
public:
  typedef shared_ptr<Root> Ptr;

protected:
  Root(){}

public:
  virtual ~Root(){}

  static Ptr create(Gorilla::TweakUiRenderer* renderer, DeveloperHud::Layer* devHudLayer);

public:
  virtual void startHandlingInput() = 0;
  virtual void stopHandlingInput() = 0;

  virtual void setVisible(bool show = true) = 0;
  void show();
  void hide();
};

} // namespace TweakUI
} // namespace Framework

#endif // FRAMEWORK_TWEAKUI_TWEAKUIROOT_H
