#ifndef FRAMEWORK_TWEAKUI_TWEAKUIBAR_INL
#define FRAMEWORK_TWEAKUI_TWEAKUIBAR_INL

#include "tweak-ui-bar.h"
#include "private/tweak-ui-parameters.h"

namespace Framework {
namespace TweakUI {


class Bar::ButtonCallbackWrapper
{
private:
  std::function<void()> callback;
public:
  const std::string name;

private:
  ButtonCallbackWrapper(const char* name, const std::function<void()>& callback)
    : callback(callback),
      name(name)
  {
  }

public:
  static shared_ptr<ButtonCallbackWrapper> create(const char* name, const std::function<void()>& callback)
  {
    return shared_ptr<ButtonCallbackWrapper>(new ButtonCallbackWrapper(name, callback));
  }

  static void call(void* wrapper)
  {
    reinterpret_cast<ButtonCallbackWrapper*>(wrapper)->callback();
  }
};


class Bar::VarCallbackWrapper
{
private:
  std::function<void(void*)> getter;
  std::function<void(const void*)> setter;
public:
  const std::string name;

private:
  VarCallbackWrapper(const char* name, const std::function<void(void*)>& getter, const std::function<void(const void*)>& setter)
    : getter(getter),
      setter(setter),
      name(name)
  {
  }

public:
  static shared_ptr<VarCallbackWrapper> create(const char* name, const std::function<void(void*)>& getter, const std::function<void(const void*)>& setter)
  {
    return shared_ptr<VarCallbackWrapper>(new VarCallbackWrapper(name, getter, setter));
  }

  static void callGetter(void* value, void* wrapper)
  {
    reinterpret_cast<VarCallbackWrapper*>(wrapper)->getter(value);
  }

  static void callSetter(const void* value, void* wrapper)
  {
    reinterpret_cast<VarCallbackWrapper*>(wrapper)->setter(value);
  }
};


template<typename T>
void Bar::addVar(const char* name, T* var, const char* def)
{
  ::TweakUI::TwType type = TweakTypeIdFor<T>::id();

  addVarAs(name, type, var, def);
}

template<typename T>
void Bar::addVar(const char* name, const T* var, const char* def)
{
  ::TweakUI::TwType type = TweakTypeIdFor<T>::id();

  addVarAs(name, type, var, def);
}


template<typename T>
class ColorTypeIdFor;

template<>
class ColorTypeIdFor<vec3>
{
public:
  static ::TweakUI::TwType id()
  {
    return ::TweakUI::TW_TYPE_COLOR3F;
  }
};

template<>
class ColorTypeIdFor<vec4>
{
public:
  static ::TweakUI::TwType id()
  {
    return ::TweakUI::TW_TYPE_COLOR4F;
  }
};

template<typename T>
void Bar::addVarAsColor(const char* name, T* var, const char* def)
{
  ::TweakUI::TwType type = ColorTypeIdFor<T>::id();

  addVarAs(name, type, var, def);
}

template<typename T>
void Bar::addVarAsColor(const char* name, const T* var, const char* def)
{
  ::TweakUI::TwType type = ColorTypeIdFor<T>::id();

  addVarAs(name, type, var, def);
}


template<typename T>
void Bar::setParam(const char* paramName, const T& value)
{
  setVarParam(nullptr, paramName, value);
}


template<typename T>
T Bar::param(const char* paramName) const
{
  return varParam<T>(nullptr, paramName);
}


template<typename T>
void Bar::setVarParam(const char* varName, const char* paramName, const T& value) const
{
  Private::setVarParam(twBar, varName, paramName, value);
}


template<typename T>
inline T Bar::varParam(const char* varname, const char* paramName) const
{
  T temp;
  Private::varParam(twBar, varname, paramName, temp);
  return temp;
}


template<typename T, typename T_in, typename T_return>
void Bar::addVarWithIntuitiveCallbackAs(const char* name, ::TweakUI::TwType type, const std::function<T_return()>& getter, const std::function<void(T_in)>& setter, const char* def)
{
  auto getterWrapper = [getter](void* valueOutPtr){
    *static_cast<T*>(valueOutPtr) = getter();
  };
  auto setterWrapper = [setter](const void* valueInPtr){
    setter(*static_cast<const T*>(valueInPtr));
  };

  addVarWithCallbackAs(name,
                       type,
                       getterWrapper,
                       setterWrapper,
                       def);
}


} // namespace TweakUI
} // namespace Framework

#endif // FRAMEWORK_TWEAKUI_TWEAKUIBAR_H
