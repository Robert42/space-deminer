#include "tweak-ui-bar.h"
#include "tweak-ui-root.h"
#include "tweak-ui-layers.h"

#include <TweakUI.h>

namespace Framework {
namespace TweakUI {

using namespace ::TweakUI;

int Bar::nVisibleBars = 0;


void Bar::incrVisibleBars()
{
  root->show();
  ++nVisibleBars;
}


void Bar::decrVisibleBars()
{
  --nVisibleBars;
  if(nVisibleBars==0)
    root->hide();
}


Bar::Bar(const char* name)
{
  twBar = TwNewBar(name);

  root = Root::singleton();

  incrVisibleBars();

  _layer = nullptr;
}


Bar::~Bar()
{
  if(_layer)
    _layer->removeBar(this);

  if(isVisible())
    decrVisibleBars();

  TwDeleteBar(twBar);
}


const char* Bar::name() const
{
  return TwGetBarName(twBar);
}


void Bar::setTopBar()
{
  TwSetTopBar(twBar);
}


void Bar::setBottomBar()
{
  TwSetBottomBar(twBar);
}


void Bar::forceRefresh()
{
  TwRefreshBar(twBar);
}


::TweakUI::TwBar* Bar::wrappedObject()
{
  return this->twBar;
}


bool Bar::isActive() const
{
  return TwGetActiveBar() == twBar;
}


void Bar::removeVar(const char* name)
{
  TwRemoveVar(this->twBar, name);

  std::remove_if(buttonCallbackWrapper.begin(),
                 buttonCallbackWrapper.end(),
  [name](const shared_ptr<ButtonCallbackWrapper>& wrapper){
    return wrapper->name==name;
  });
  std::remove_if(varCallbackWrapper.begin(),
                 varCallbackWrapper.end(),
  [name](const shared_ptr<VarCallbackWrapper>& wrapper){
    return wrapper->name==name;
  });
}


void Bar::removeAllVars()
{
  TwRemoveAllVars(this->twBar);
  buttonCallbackWrapper.clear();
  varCallbackWrapper.clear();
}


void Bar::openGroup(const char* name, bool open)
{
  TwDefine((std::string(this->name())+'/'+name + " opened=" + (open ? "true" : "false")).c_str());
}


void Bar::closeGroup(const char* name)
{
  openGroup(name, false);
}


void Bar::addVarAs(const char* name, ::TweakUI::TwType type, void* var, const char* def)
{
  TwAddVarRW(twBar, name, type, var, def);
}


void Bar::addVarAs(const char* name, ::TweakUI::TwType type, const void* var, const char* def)
{
  TwAddVarRO(twBar, name, type, var, def);
}


void Bar::addVarWithCallbackAs(const char* name, ::TweakUI::TwType type, const std::function<void(void*)>& getter, const std::function<void(const void*)>& setter, const char* def)
{
  shared_ptr<VarCallbackWrapper> wrapper = VarCallbackWrapper::create(name, getter, setter);

  varCallbackWrapper.push_back(wrapper);

  ::TweakUI::TwAddVarCB(twBar,
                        name,
                        type,
                        &VarCallbackWrapper::callSetter,
                        &VarCallbackWrapper::callGetter,
                        wrapper.get(),
                        def);
}


void Bar::addSeperator(const char* name, const char* def)
{
  TwAddSeparator(twBar, name, def);
}


void Bar::addButton(const char* name, const std::function<void()>& callback, const char* def)
{
  shared_ptr<ButtonCallbackWrapper> wrapper = ButtonCallbackWrapper::create(name, callback);

  buttonCallbackWrapper.push_back(wrapper);

  TwAddButton(twBar, name, &ButtonCallbackWrapper::call, wrapper.get(), def);
}


void Bar::setLabel(const char* label)
{
  setParam("label", label);
}


std::string Bar::label() const
{
  std::string label = param<std::string>("label");

  if(label.empty())
    return name();

  return label;
}


std::string Bar::help() const
{
  return param<std::string>("help");
}


void Bar::setHelp(const char* help)
{
  setParam("help", help);
}


void Bar::setHelp(const std::string& help)
{
  setHelp(help.c_str());
}


vec4 Bar::color() const
{
  ivec3 rgb8Color = param<ivec3>("color");

  return vec4(vec3(rgb8Color)/255.f, alpha());
}


float Bar::alpha() const
{
  return param<int>("alpha") / 255.f;
}


void Bar::setColor(const ivec3& color)
{
  setParam("color", color);
}


void Bar::setAlpha(uint8 alpha)
{
  setParam("alpha", static_cast<int>(alpha));
}


void Bar::setColor(const ivec4& color)
{
  setColor(color.rgb());
  setAlpha(uint8(color.a));
}


void Bar::setColor(const vec3& color)
{
  setColor(ivec3(glm::clamp(round(color*255.f), 0.f, 255.f)));
}


void Bar::setColor(const vec4& color)
{
  setColor(color.rgb());
  setAlpha(color.a);
}


void Bar::setAlpha(float alpha)
{
  setAlpha(uint8(clamp(round(alpha*255), 0.f, 255.f)));
}


Bar::TextBrightness Bar::textBrightness() const
{
  return param<std::string>("text")=="dark" ? TextBrightness::DARK : TextBrightness::LIGHT;
}


void Bar::setTextBrightness(TextBrightness brightness)
{
  setParam("text", brightness==TextBrightness::LIGHT ? "light" : "dark");
}


void Bar::toggleTextBrightness()
{
  if(textBrightness()==TextBrightness::DARK)
    setTextBrightness(TextBrightness::LIGHT);
  else
    setTextBrightness(TextBrightness::DARK);
}


ivec2 Bar::position() const
{
  return param<ivec2>("position");
}


void Bar::setPosition(const ivec2& position)
{
  setParam<ivec2>("position", position);
}


ivec2 Bar::size() const
{
  return param<ivec2>("size");
}


void Bar::setSize(const ivec2& size)
{
  setParam<ivec2>("size", size);
}


int Bar::valuesWidth() const
{
  return param<int>("valueswidth");
}


void Bar::setValuesWidth(int valuesWidth)
{
  if(valuesWidth > 0)
    setParam("valueswidth", valuesWidth);
  else
    setValuesWidthFit();
}


void Bar::setValuesWidthFit()
{
  setParam("valueswidth", "fit");
}


float Bar::refreshRate() const
{
  return param<float>("refresh");
}


void Bar::setRefreshRate(float refreshRate)
{
  return setParam<float>("refresh", refreshRate);
}


bool Bar::isVisible() const
{
  return param<bool>("visible");
}


void Bar::setVisible(bool visible)
{
  if(isVisible() != visible)
  {
    if(visible)
      incrVisibleBars();
    else
      decrVisibleBars();

    setParam<bool>("visible", visible);
  }
}


bool Bar::isIconified() const
{
  return param<bool>("iconified");
}


void Bar::setIconified(bool iconified)
{
  setParam<bool>("iconified", iconified);
}


bool Bar::isIconifiable() const
{
  return param<bool>("iconifiable");
}


void Bar::setIconifiable(bool iconifiable)
{
  setParam<bool>("iconifiable", iconifiable);
}


bool Bar::isMovable() const
{
  return param<bool>("movable");
}


void Bar::setMovable(bool movable)
{
  setParam<bool>("movable", movable);
}


bool Bar::isResizable() const
{
  return param<bool>("resizable");
}


void Bar::setResizable(bool resizable)
{
  setParam<bool>("resizable", resizable);
}


bool Bar::isAlwaysTop() const
{
  return param<bool>("alwaystop");
}


void Bar::setAlwaysTop(bool alwaysTop)
{
  setParam("alwaystop", alwaysTop);
}


bool Bar::isAlwaysBottom() const
{
  return param<bool>("alwaysbottom");
}


void Bar::setAlwaysBottom(bool alwaysBottom)
{
  setParam("alwaysbottom", alwaysBottom);
}


template<typename T>
void setupVecComponentInt(Bar* bar, const char* name, const std::string& component, typename T::value_type min, typename T::value_type max, typename T::value_type step)
{
  std::string componentName = name+component;
  const char* componentNameCStr = componentName.c_str();

  if(min!=std::numeric_limits<vec2::value_type>::min())
    bar->setVarParam(componentNameCStr, "min", min);
  if(max!=std::numeric_limits<vec2::value_type>::max())
    bar->setVarParam(componentNameCStr, "max", max);

  if(min!=std::numeric_limits<vec2::value_type>::max() && step>=0)
    bar->setVarParam(componentNameCStr, "step", step);
}


template<typename T>
void setupVecComponentFloat(Bar* bar, const char* name, const std::string& component, typename T::value_type min, typename T::value_type max, typename T::value_type step, int precision)
{
  std::string componentName = name+component;
  const char* componentNameCStr = componentName.c_str();

  if(min!=std::numeric_limits<vec2::value_type>::max())
    bar->setVarParam(componentNameCStr, "min", min);
  if(max!=std::numeric_limits<vec2::value_type>::max())
    bar->setVarParam(componentNameCStr, "max", max);

  if(step!=std::numeric_limits<vec2::value_type>::max() && step>=0)
    bar->setVarParam(componentNameCStr, "step", step);

  if(precision>=0)
    bar->setVarParam((name+component).c_str(), "precision", precision);
}


void Bar::setBlenderCoordinates(const char* varName)
{
  setVarParam(varName, "axisx", "x");
  setVarParam(varName, "axisy", "-z");
  setVarParam(varName, "axisz", "y");
}


void Bar::addVar(const char* name, quaternion* var, const char* def, bool blenderCoordinates)
{
  addVar<quaternion>(name, var, def);
  if(blenderCoordinates)
    setBlenderCoordinates(name);
}


void Bar::addVar(const char* name, const quaternion* var, const char* def, bool blenderCoordinates)
{
  addVar<quaternion>(name, var, def);
  if(blenderCoordinates)
    setBlenderCoordinates(name);
}


void Bar::addVarAsDirection(const char* name, vec3* var, const char* def, bool blenderCoordinates)
{
  addVarAs(name, ::TweakUI::TW_TYPE_DIR3F, var, def);
  if(blenderCoordinates)
    setBlenderCoordinates(name);
}


void Bar::addVarAsDirection(const char* name, const vec3* var, const char* def, bool blenderCoordinates)
{
  addVarAs(name, ::TweakUI::TW_TYPE_DIR3F, var, def);
  if(blenderCoordinates)
    setBlenderCoordinates(name);
}


void Bar::addVarAsVector(const char* name, vec2* var, const char* def, const vec2& min, const vec2& max, const vec2& step, const ivec2& precision)
{
  addVar(name, var, def);

  setupVecComponentFloat<vec2>(this, name, ".x", min.x, max.x, step.x, precision.x);
  setupVecComponentFloat<vec2>(this, name, ".y", min.y, max.y, step.y, precision.y);
}


void Bar::addVarAsVector(const char* name, const vec2* var, const char* def, const vec2& min, const vec2& max, const vec2& step, const ivec2& precision)
{
  addVar(name, var, def);

  setupVecComponentFloat<vec2>(this, name, ".x", min.x, max.x, step.x, precision.x);
  setupVecComponentFloat<vec2>(this, name, ".y", min.y, max.y, step.y, precision.y);
}


void Bar::addVarAsVector(const char* name, vec3* var, const char* def, const vec3& min, const vec3& max, const vec3& step, const ivec3& precision)
{
  addVar(name, var, def);

  setupVecComponentFloat<vec3>(this, name, ".x", min.x, max.x, step.x, precision.x);
  setupVecComponentFloat<vec3>(this, name, ".y", min.y, max.y, step.y, precision.y);
  setupVecComponentFloat<vec3>(this, name, ".z", min.z, max.z, step.z, precision.z);
}


void Bar::addVarAsVector(const char* name, const vec3* var, const char* def, const vec3& min, const vec3& max, const vec3& step, const ivec3& precision)
{
  addVar(name, var, def);

  setupVecComponentFloat<vec3>(this, name, ".x", min.x, max.x, step.x, precision.x);
  setupVecComponentFloat<vec3>(this, name, ".y", min.y, max.y, step.y, precision.y);
  setupVecComponentFloat<vec3>(this, name, ".z", min.z, max.z, step.z, precision.z);
}


void Bar::addVarAsVector(const char* name, vec4* var, const char* def, const vec4& min, const vec4& max, const vec4& step, const ivec4& precision)
{
  addVar(name, var, def);

  setupVecComponentFloat<vec4>(this, name, ".x", min.x, max.x, step.x, precision.x);
  setupVecComponentFloat<vec4>(this, name, ".y", min.y, max.y, step.y, precision.y);
  setupVecComponentFloat<vec4>(this, name, ".z", min.z, max.z, step.z, precision.z);
  setupVecComponentFloat<vec4>(this, name, ".w", min.w, max.w, step.w, precision.w);
}


void Bar::addVarAsVector(const char* name, const vec4* var, const char* def, const vec4& min, const vec4& max, const vec4& step, const ivec4& precision)
{
  addVar(name, var, def);

  setupVecComponentFloat<vec4>(this, name, ".x", min.x, max.x, step.x, precision.x);
  setupVecComponentFloat<vec4>(this, name, ".y", min.y, max.y, step.y, precision.y);
  setupVecComponentFloat<vec4>(this, name, ".z", min.z, max.z, step.z, precision.z);
  setupVecComponentFloat<vec4>(this, name, ".w", min.w, max.w, step.w, precision.w);
}


void Bar::addVarAsVector(const char* name, ivec2* var, const char* def, const ivec2& min, const ivec2& max, const ivec2& step)
{
  addVar(name, var, def);

  setupVecComponentInt<ivec2>(this, name, ".x", min.x, max.x, step.x);
  setupVecComponentInt<ivec2>(this, name, ".y", min.y, max.y, step.y);
}


void Bar::addVarAsVector(const char* name, const ivec2* var, const char* def, const ivec2& min, const ivec2& max, const ivec2& step)
{
  addVar(name, var, def);

  setupVecComponentInt<ivec2>(this, name, ".x", min.x, max.x, step.x);
  setupVecComponentInt<ivec2>(this, name, ".y", min.y, max.y, step.y);
}


void Bar::addVarAsVector(const char* name, ivec3* var, const char* def, const ivec3& min, const ivec3& max, const ivec3& step)
{
  addVar(name, var, def);

  setupVecComponentInt<ivec3>(this, name, ".x", min.x, max.x, step.x);
  setupVecComponentInt<ivec3>(this, name, ".y", min.y, max.y, step.y);
  setupVecComponentInt<ivec3>(this, name, ".z", min.z, max.z, step.z);
}


void Bar::addVarAsVector(const char* name, const ivec3* var, const char* def, const ivec3& min, const ivec3& max, const ivec3& step)
{
  addVar(name, var, def);

  setupVecComponentInt<ivec3>(this, name, ".x", min.x, max.x, step.x);
  setupVecComponentInt<ivec3>(this, name, ".y", min.y, max.y, step.y);
  setupVecComponentInt<ivec3>(this, name, ".z", min.z, max.z, step.z);
}


void Bar::addVarAsVector(const char* name, ivec4* var, const char* def, const ivec4& min, const ivec4& max, const ivec4& step)
{
  addVar(name, var, def);

  setupVecComponentInt<ivec4>(this, name, ".x", min.x, max.x, step.x);
  setupVecComponentInt<ivec4>(this, name, ".y", min.y, max.y, step.y);
  setupVecComponentInt<ivec4>(this, name, ".z", min.z, max.z, step.z);
  setupVecComponentInt<ivec4>(this, name, ".w", min.w, max.w, step.w);
}


void Bar::addVarAsVector(const char* name, const ivec4* var, const char* def, const ivec4& min, const ivec4& max, const ivec4& step)
{
  addVar(name, var, def);

  setupVecComponentInt<ivec4>(this, name, ".x", min.x, max.x, step.x);
  setupVecComponentInt<ivec4>(this, name, ".y", min.y, max.y, step.y);
  setupVecComponentInt<ivec4>(this, name, ".z", min.z, max.z, step.z);
  setupVecComponentInt<ivec4>(this, name, ".w", min.w, max.w, step.w);
}


Bar::Ptr Bar::create(const char* name)
{
  return Ptr(new Bar(name));
}


} // namespace TweakUI
} // namespace Framework
