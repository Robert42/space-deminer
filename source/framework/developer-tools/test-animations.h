#ifndef FRAMEWORK_TESTANIMATIONS_H
#define FRAMEWORK_TESTANIMATIONS_H

#include <framework/developer-tools/gui-system/ingame/flat/window-manager.h>
#include <framework/developer-tools/gui-system/window.h>

#include <framework/developer-tools/gui-system/animations/timeline-animation.h>
#include <framework/developer-tools/gui-system/animations/fall-animation.h>

namespace Framework {

class TestAnimations final : public noncopyable
{
private:
  class AnimationSlot;
  class AnimationSample;

  typedef Gui::Animation::FadeInterpolation FadeInterpolation;
  typedef Gui::Animations::FreeFallAnimation::PhysicDescription::InterpolationPreset FreeFallPreset;

public:
  Signals::Trackable trackable;

private:
  Gui::Ingame::Flat::WindowManager windowManager;
  Gui::Window::Ptr testWindow;

  Gui::Animations::TimelineAnimation::Ptr timeAnimation;

  QList< std::shared_ptr<AnimationSlot> > animationSlots;

public:
  TestAnimations();
  ~TestAnimations();

  void reset();

  std::shared_ptr<AnimationSample> createConstantSample();
  std::shared_ptr<AnimationSample> createSimpleInterpolationSample(const FadeInterpolation& interpolation);
  std::shared_ptr<AnimationSample> createFreeFallInterpolationSample(const FreeFallPreset& freeFallPreset);

private:
  Gui::Widget::Ptr createGui();

  Gui::LayoutPtr createLegendLabel(const std::shared_ptr<AnimationSlot>& slot);

  void drawFunctions(Gui::Painter& painter, const Rectangle<vec2>& canvasArea);

  void _setAnimationSample(const std::shared_ptr<AnimationSample>& animationSample);

  Gui::Widget::Ptr createSampleButton(const Translation& label, const std::function< std::shared_ptr<AnimationSample>() >& creator);
};

} // namespace Framework

#endif // FRAMEWORK_TESTANIMATIONS_H
