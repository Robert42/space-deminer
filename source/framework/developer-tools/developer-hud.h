#ifndef _FRAMEWORK_DEVELOPER_HUD_H_
#define _FRAMEWORK_DEVELOPER_HUD_H_

#include <dependencies.h>

#include <base/signals/trackable.h>
#include <base/scripting/script-index.h>

#include <framework/developer-tools/terminal/buffer/terminal-format.h>

namespace Framework {
using namespace Base;

class RenderWindow;
class FrameTimer;
class Application;

namespace DeveloperHudElements {
class TerminalHud;
}
namespace Editor {
namespace Gorilla {
class BlenderStyleToolLayer;
}
}
namespace TweakUI {
namespace Gorilla {
class TextureAtlas;
}
}

class DeveloperHud : public noncopyable
{
public:
  class Layer;
  class MonospaceLayer;

private:
  class OwnSilverback : public Gorilla::Silverback
  {
  public:
    void registerOwnTextureAtlas(const Ogre::String& name, Gorilla::TextureAtlas* atlas);
  };

  Ogre::SceneManager* sceneManager;
  Ogre::Camera* camera;
  Ogre::Viewport* viewport;

  OwnSilverback* gorillaSilverback_;
  Gorilla::Screen* gorillaScreenNodeEditor_;
  Gorilla::Screen* gorillaScreenBlenderStyleToolLayer_;
  Gorilla::Screen* gorillaScreenMouseCursor_;
  Gorilla::Screen* gorillaScreenAntTweakBar_;
  Gorilla::Screen* gorillaScreenIngameEditor_;
  Gorilla::Screen* gorillaScreenMonospace_;
  Gorilla::Screen* gorillaScreenTweakUIMouseCursor_;

  TweakUI::Gorilla::TextureAtlas* tweakUITextureAtlas;

  std::list<std::shared_ptr<Layer>> allLayer;

  std::shared_ptr<DeveloperHudElements::TerminalHud> terminalHud;
  std::shared_ptr<Editor::Gorilla::BlenderStyleToolLayer> _blenderStyleToolLayer;

protected:
  DeveloperHud();

public:
  virtual ~DeveloperHud();

public:
  static shared_ptr<DeveloperHud> create();

public:
  Gorilla::Silverback& gorillaSilverback();
  Gorilla::Screen& gorillaScreenNodeEditor();
  Gorilla::Screen& gorillaScreenBlenderStyleToolLayer();
  Gorilla::Screen& gorillaScreenAntTweakBar();
  Gorilla::Screen& gorillaScreenIngameEditor();
  Gorilla::Screen& gorillaScreenMonospace();
  Gorilla::Screen& gorillaScreenMouseCursor();
  Gorilla::Screen& gorillaScreenTweakUIMouseCursor();

  void addLayer(const std::shared_ptr<Layer>& layer);
  void registerTerminalCommands();

  std::shared_ptr<Editor::Gorilla::BlenderStyleToolLayer>& blenderStyleToolLayer();
};

class DeveloperHud::Layer : public noncopyable
{
public:
  typedef std::shared_ptr<Layer> Ptr;

  BEGIN_ENUMERATION(ZLayer, :uint)
    MOUSECURSOR = std::numeric_limits<uint>::max(),
    TERMINAL = MOUSECURSOR-1,
    FPS = TERMINAL-1,
    INGAME_EDITOR = FPS-1,
    TWEAK_UI = INGAME_EDITOR-1,

    INGAME_GUI = 1024
  ENUMERATION_BASIC_OPERATORS(ZLayer)
  END_ENUMERATION;

public:
  Gorilla::Screen& gorillaScreen;

private:
  Gorilla::Layer* gorillaLayer_;

protected:
  Layer(ZLayer z, Gorilla::Screen& gorillaScreen);

public:
  virtual ~Layer();

public:
  Gorilla::Layer& gorillaLayer();

  virtual void registerTerminalCommands(const std::shared_ptr<Base::Scripting::Index>& scriptIndex);

  virtual void show();
  virtual void hide();
  void setVisible(bool visible);
  bool isVisible();
};

class DeveloperHud::MonospaceLayer : public Layer
{
public:
  typedef std::shared_ptr<MonospaceLayer> Ptr;

  class Font
  {
  private:
    const int _lineHeight;
    int _characterWidth, _lineSpacing;

  public:
    typedef Terminal::Format::Light Light;

    Font(Gorilla::Layer& gorillaLayer, int lineHeight);

    Gorilla::Layer& gorillaLayer;
    int scaleFactor;

    int characterWidth() const;
    int lineSpacing() const;

    Gorilla::GlyphData* gorillaGlyphData(Light light) const;
    int gorillaIndex(Light light) const;

    int lineHeight() const;
  };

  static const int freemono_14_dark = 140;
  static const int freemono_14_normal = 141;
  static const int freemono_14_bright = 142;
  static const int freemono_16_dark = 160;
  static const int freemono_16_normal = 161;
  static const int freemono_16_bright = 162;

public:
  MonospaceLayer(ZLayer z, DeveloperHud& hud);

public:
  Font fontSmall, fontLarge;
};


}

#endif
