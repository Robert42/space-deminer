#ifndef FRAMEWORK_TWEAKUI_GORILLA_VIEWPORTRENDERER_H
#define FRAMEWORK_TWEAKUI_GORILLA_VIEWPORTRENDERER_H

#include <base/increment-in-lifetime.h>

#include "scissor.h"
#include "primitive-renderer.h"

namespace Framework {
namespace GorillaHelper {

class ViewportRenderer : public PrimitiveRenderer
{
public:
  typedef IncrementInLifeTime<int> ClippingLock;
  typedef IncrementInLifeTime<int> IgnoreViewportLock;

private:
  Scissor viewport;
  Scissor scissor;
  Ogre::Vector2 offset;

  Scissor internScissor;
  Ogre::Vector2 internOffset;

  int numberClippingLocks;
  int numberIgnoreViewportLocks;

public:
  ViewportRenderer();

public:
  void setScissor(const Scissor& scissor);
  void setViewport(const Scissor& viewport, const Ogre::Vector2& offset);
  void setScissor(int x, int y, int width, int height);
  void setViewport(int x, int y, int width, int height, int offsetX, int offsetY);
  void setInfiniteScissor();
  void setInfiniteViewport();

  /** Disables clipping with in theis viewport for the lifetime of the returned object.
     *
     * @note You can copy the objects. Then the clipping will also wait for the other objects to be destroyed.
     **/
  ClippingLock disableClipping();

public:
  void _pushTriangle(size_t indexA, size_t indexB, size_t indexC, const Ogre::Vector2* positions, const Ogre::ColourValue* colors, const Ogre::Vector2* uvs) override;

private:
  void _updateInternScissor();

  IgnoreViewportLock _ignoreViewport();
};


} // namespace GorillaHelper
} // namespace Framework

#endif // FRAMEWORK_TWEAKUI_GORILLA_VIEWPORTRENDERER_H
