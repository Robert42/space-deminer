#include "scissor.h"

#include <framework/window/render-window.h>
#include <base/interpolation.h>

namespace Framework {
namespace GorillaHelper {

Scissor::Scissor()
  : Scissor(Ogre::Vector2(0, 0), Ogre::Vector2(1, 1))
{
  _infiniteLarge = true;
}

Scissor::Scissor(const Rectangle<vec2>& scissor)
  : Scissor(Ogre::Vector2_cast(scissor.min()), Ogre::Vector2_cast(scissor.max()))
{
}

Scissor::Scissor(const Ogre::Vector2& min, const Ogre::Vector2& max)
  : _infiniteLarge(false)
{
  setScissor(min, max);
}

Scissor::Scissor(const Scissor& scissor)
  : Scissor(scissor.min(), scissor.max())
{
  this->_infiniteLarge = scissor.infiniteLarge();
}

const Ogre::Vector2& Scissor::min() const
{
  return _min;
}

const Ogre::Vector2& Scissor::max() const
{
  return _max;
}

bool Scissor::infiniteLarge() const
{
  return _infiniteLarge;
}

void Scissor::setInfiniteLarge()
{
  this->_infiniteLarge = true;
}

void Scissor::setScissor(const Ogre::Vector2& min, const Ogre::Vector2& max)
{
  this->_infiniteLarge = false;
  if(min.x>=max.x || min.y>=max.y)
  {
    this->_min = Ogre::Vector2(-1600, -1600);
    this->_max = Ogre::Vector2(-1600, -1600);
  }else
  {
    this->_min = min;
    this->_max = max;
  }
}

Scissor& Scissor::operator = (const Scissor& scissor)
{
  setScissor(scissor.min(), scissor.max());
  this->_infiniteLarge = scissor.infiniteLarge();
  return *this;
}

Scissor& Scissor::operator &= (const Scissor& scissor)
{
  if(this->infiniteLarge())
    return *this = scissor;
  if(scissor.infiniteLarge())
    return *this;

  Ogre::Vector2 min = Base::max(this->min(), scissor.min());
  Ogre::Vector2 max = Base::min(this->max(), scissor.max());

  if(min.x>max.x || min.y>max.y)
  {
    min = Ogre::Vector2(-1601, -1601);
    max = Ogre::Vector2(-1600, -1600);
  }

  bool infiniteLarge = this->infiniteLarge() && scissor.infiniteLarge();

  setScissor(min, max);

  this->_infiniteLarge = infiniteLarge;

  return *this;
}

Scissor& Scissor::operator += (const Ogre::Vector2& vector)
{
  setScissor(_min+vector, _max+vector);
  return *this;
}

Scissor& Scissor::operator -= (const Ogre::Vector2& vector)
{
  setScissor(_min-vector, _max-vector);
  return *this;
}

Scissor::Polygon Scissor::clip(const Polygon& polygon)
{
  return _clipE(_clipS(_clipW(_clipN(polygon))));
}

Scissor::Polygon Scissor::_clipN(const Polygon& polygon)
{
  return _clip(1, Mask::onlyN(), min().y, polygon);
}

Scissor::Polygon Scissor::_clipW(const Polygon& polygon)
{
  return _clip(0, Mask::onlyW(), min().x, polygon);
}

Scissor::Polygon Scissor::_clipS(const Polygon& polygon)
{
  return _clip(1, Mask::onlyS(), max().y, polygon);
}

Scissor::Polygon Scissor::_clipE(const Polygon& polygon)
{
  return _clip(0, Mask::onlyE(), max().x, polygon);
}

inline bool isOutsideEdge(Scissor::Mask pointMask, Scissor::Mask edgeMask)
{
  return pointMask.hasCommonBits(edgeMask);
}

Scissor::Polygon Scissor::_clip(size_t dimension, Mask edgeMask, Ogre::Real edgePosition, const Polygon& polygon)
{
  Polygon clippedPolygon;

  for(int i=0; i<polygon.nVertices; ++i)
  {
    bool isOutsideClippedEdge = isOutsideEdge(polygon.masks[i], edgeMask);

    if(isOutsideClippedEdge)
    {
      int previousIndex = polygon.prevIndex(i);
      int nextIndex = polygon.nextIndex(i);

      _addClippedToPolygon(dimension, edgeMask, edgePosition, polygon, i, clippedPolygon, previousIndex);
      _addClippedToPolygon(dimension, edgeMask, edgePosition, polygon, i, clippedPolygon, nextIndex);
    }else
    {
      clippedPolygon.appendVertex(polygon.masks[i],
                                  polygon.positions[i],
                                  polygon.colors[i],
                                  polygon.uvs[i]);
    }
  }

  return clippedPolygon;
}

void Scissor::_addClippedToPolygon(size_t dimension,
                                   Mask edgeMask,
                                   Ogre::Real edgePosition,
                                   const Polygon& polygon,
                                   int indexOfPointOutsideEdge,
                                   Polygon& targetPolygon,
                                   int indexOfNeighboringPoint)
{
  assert(isOutsideEdge(polygon.masks[indexOfPointOutsideEdge], edgeMask));

  bool isNeighborOutsideClippedEdge = isOutsideEdge(polygon.masks[indexOfNeighboringPoint], edgeMask);

  if(isNeighborOutsideClippedEdge)
    return;

  Ogre::Real start = polygon.positions[indexOfPointOutsideEdge][dimension];
  Ogre::Real end = polygon.positions[indexOfNeighboringPoint][dimension];

  assert(start-end != 0.f);

  Ogre::Real relativeEdgePosition = interpolationWeightOf(edgePosition, start, end);

  assert(relativeEdgePosition >= 0.f && relativeEdgePosition<=1.f);

  Ogre::Vector2 cutPosition = interpolate(relativeEdgePosition,
                                          polygon.positions[indexOfPointOutsideEdge],
                                          polygon.positions[indexOfNeighboringPoint]);
  Ogre::ColourValue cutColor = interpolate(relativeEdgePosition,
                                           polygon.colors[indexOfPointOutsideEdge],
                                           polygon.colors[indexOfNeighboringPoint]);
  Ogre::Vector2 cutUV = interpolate(relativeEdgePosition,
                                    polygon.uvs[indexOfPointOutsideEdge],
                                    polygon.uvs[indexOfNeighboringPoint]);
  Mask cutMask = Mask::maskFor(*this, cutPosition);
  targetPolygon.appendVertex(cutMask,
                             cutPosition,
                             cutColor,
                             cutUV);
}

// ==== Scissor::Mask ====

Scissor::Mask::Mask(uint32 bits)
  : _bits(bits)
{
}


Scissor::Mask::Mask() : Mask(0)
{
}


Scissor::Mask Scissor::Mask::maskFor(const Scissor& scissor, const Ogre::Vector2& vertex)
{
  Mask mask(0);

  if(!scissor.infiniteLarge())
  {
    if(vertex.x < scissor.min().x)
      mask |= onlyW();
    if(vertex.x > scissor.max().x)
      mask |= onlyE();
    if(vertex.y < scissor.min().y)
      mask |= onlyN();
    if(vertex.y > scissor.max().y)
      mask |= onlyS();
  }

  return mask;
}


uint32 Scissor::Mask::bits() const
{
  return this->_bits;
}


bool Scissor::Mask::hasCommonBits(Mask otherMask) const
{
  return 0 != (otherMask.bits() & this->bits());
}


bool Scissor::Mask::containsN() const
{
  return 0 != (onlyN().bits() & this->bits());
}

bool Scissor::Mask::containsW() const
{
  return 0 != (onlyW().bits() & this->bits());
}

bool Scissor::Mask::containsS() const
{
  return 0 != (onlyS().bits() & this->bits());
}

bool Scissor::Mask::containsE() const
{
  return 0 != (onlyE().bits() & this->bits());
}

bool Scissor::Mask::isInCenter() const
{
  return bits() == 0;
}

bool Scissor::Mask::operator==(Mask otherMask)const
{
  return bits() == otherMask.bits();
}

bool Scissor::Mask::operator!=(Mask otherMask)const
{
  return bits() != otherMask.bits();
}


Scissor::Mask Scissor::Mask::onlyN()
{
  return Mask(0x0100);
}

Scissor::Mask Scissor::Mask::onlyW()
{
  return Mask(0x0001);
}

Scissor::Mask Scissor::Mask::onlyS()
{
  return Mask(0x1000);
}

Scissor::Mask Scissor::Mask::onlyE()
{
  return Mask(0x0010);
}


Scissor::Mask Scissor::Mask::operator | (Mask other) const
{
  return Mask(this->bits() | other.bits());
}

Scissor::Mask Scissor::Mask::operator & (Mask other) const
{
  return Mask(this->bits() & other.bits());
}

Scissor::Scissor::Mask Scissor::Mask::operator ^ (Mask other) const
{
  return Mask(this->bits() ^ other.bits());
}


Scissor::Mask& Scissor::Mask::operator |= (Mask other)
{
  this->_bits |= other._bits;
  return *this;
}

Scissor::Mask& Scissor::Mask::operator &= (Mask other)
{
  this->_bits &= other._bits;
  return *this;
}

Scissor::Mask& Scissor::Mask::operator ^= (Mask other)
{
  this->_bits ^= other._bits;
  return *this;
}

// ==== Scissor::Polygon ====

Scissor::Polygon::Polygon()
{
  nVertices = 0;
}

void Scissor::Polygon::setToTriangle(Mask maskA, Mask maskB, Mask maskC,
                                     int indexA, int indexB, int indexC,
                                     const Ogre::Vector2* positions,
                                     const Ogre::ColourValue* colors,
                                     const Ogre::Vector2* uvs)
{
  this->nVertices = 3;

  this->masks[0] = maskA;
  this->masks[1] = maskB;
  this->masks[2] = maskC;

  this->positions[0] = positions[indexA];
  this->positions[1] = positions[indexB];
  this->positions[2] = positions[indexC];

  this->colors[0] = colors[indexA];
  this->colors[1] = colors[indexB];
  this->colors[2] = colors[indexC];

  this->uvs[0] = uvs[indexA];
  this->uvs[1] = uvs[indexB];
  this->uvs[2] = uvs[indexC];
}

void Scissor::Polygon::appendVertex(Mask mask,
                                    const Ogre::Vector2& position,
                                    const Ogre::ColourValue& color,
                                    const Ogre::Vector2& uv)
{
  assert(this->nVertices<12);

  int i = this->nVertices;

  this->masks[i] = mask;
  this->positions[i] = position;
  this->colors[i] = color;
  this->uvs[i] = uv;

  this->nVertices++;
}

void Scissor::Polygon::appendVertex(const Scissor& scissor,
                                    const Ogre::Vector2& position,
                                    const Ogre::ColourValue& color,
                                    const Ogre::Vector2& uv)
{
  appendVertex(Mask::maskFor(scissor, position),
               position,
               color,
               uv);
}

int Scissor::Polygon::prevIndex(int i) const
{
  // Module works the best with positive numbers, so in case
  // i is 0, the add simply nVertices (modulo equivalent to 0)
  // to avoid i-1 getting < 0
  return (i+nVertices-1) % nVertices;
}

int Scissor::Polygon::nextIndex(int i) const
{
  return (i+1) % nVertices;
}


} // namespace GorillaHelper
} // namespace Framework
