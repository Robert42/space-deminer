#ifndef FRAMEWORK_TWEAKUI_GORILLA_SCISSOR_H
#define FRAMEWORK_TWEAKUI_GORILLA_SCISSOR_H

#include <dependencies.h>

#include <base/signals.h>
#include <base/geometry/rectangle.h>

namespace Framework {
using namespace Base;

namespace GorillaHelper {

/**
   * A simple clipper based on the Cohen-Sutherland algorithm.
   */
class Scissor final
{
public:
  class Mask
  {
  private:
    uint32 _bits;

    Mask(uint32 bits);

  public:
    Mask();

    static Mask maskFor(const Scissor& scissor, const Ogre::Vector2& vertex);

    uint32 bits() const;

    bool hasCommonBits(Mask otherMask) const;
    bool containsN() const;
    bool containsW() const;
    bool containsS() const;
    bool containsE() const;

    bool isInCenter() const;

    bool operator==(Mask otherMask)const;
    bool operator!=(Mask otherMask)const;

    static Mask onlyN();
    static Mask onlyW();
    static Mask onlyS();
    static Mask onlyE();

    Mask operator | (Mask other) const;
    Mask operator & (Mask other) const;
    Mask operator ^ (Mask other) const;

    Mask& operator |= (Mask other);
    Mask& operator &= (Mask other);
    Mask& operator ^= (Mask other);
  };

  class Polygon
  {
  public:
    int nVertices;
    Mask masks[12];
    Ogre::Vector2 positions[12];
    Ogre::ColourValue colors[12];
    Ogre::Vector2 uvs[12];

  public:
    Polygon();
    void setToTriangle(Mask maskA, Mask maskB, Mask maskC,
                       int indexA, int indexB, int indexC,
                       const Ogre::Vector2* positions,
                       const Ogre::ColourValue* colors,
                       const Ogre::Vector2* uvs);
    void appendVertex(Mask mask,
                      const Ogre::Vector2& position,
                      const Ogre::ColourValue& color,
                      const Ogre::Vector2& uv);
    void appendVertex(const Scissor& scissor,
                      const Ogre::Vector2& position,
                      const Ogre::ColourValue& color = Ogre::ColourValue::White,
                      const Ogre::Vector2& uv = Ogre::Vector2(0, 0));

    int prevIndex(int i) const;
    int nextIndex(int i) const;
  };

public:
  Signals::Trackable trackable;

private:
  Ogre::Vector2 _min, _max;
  bool _infiniteLarge : 1;

public:
  Scissor();
  Scissor(const Rectangle<vec2>& scissor);
  Scissor(const Ogre::Vector2& min, const Ogre::Vector2& max);
  Scissor(const Scissor& scissor);

public:
  const Ogre::Vector2& min() const;
  const Ogre::Vector2& max() const;

  bool infiniteLarge() const;
  void setInfiniteLarge();

  void setScissor(const Ogre::Vector2& min, const Ogre::Vector2& max);

  Polygon clip(const Polygon& polygon);

  Scissor& operator = (const Scissor& scissor);
  Scissor& operator &= (const Scissor& scissor);
  Scissor& operator += (const Ogre::Vector2& vector);
  Scissor& operator -= (const Ogre::Vector2& vector);

private:
  Polygon _clipN(const Polygon& polygon);
  Polygon _clipW(const Polygon& polygon);
  Polygon _clipS(const Polygon& polygon);
  Polygon _clipE(const Polygon& polygon);

  Polygon _clip(size_t dimension,
                Mask edgeMask,
                Ogre::Real edgePosition,
                const Polygon& polygon);
  void _addClippedToPolygon(size_t dimension,
                            Mask edgeMask,
                            Ogre::Real edgePosition,
                            const Polygon& polygon,
                            int indexOfPointOutsideEdge,
                            Polygon& targetPolygon,
                            int indexOfNeighboringPoint);

};

} // namespace GorillaHelper
} // namespace Framework

#endif // FRAMEWORK_TWEAKUI_GORILLA_SCISSOR_H
