#ifndef FRAMEWORK_GORILLAHELPER_PRIMITIVERENDERER_H
#define FRAMEWORK_GORILLAHELPER_PRIMITIVERENDERER_H

#include <base/geometry/rectangle.h>
#include <base/geometry/rounded-rectangle.h>
#include <base/strings/string.h>
#include <framework/developer-tools/missing-character-renderer.h>
#include <framework/developer-tools/gui-system/base-line-drawer.h>

namespace Framework {

using namespace Base;

namespace GorillaHelper {

class PrimitiveRenderer : public Gorilla::CustomRenderer
{
public:
  class OutlinedLineStripDrawer : public Gui::OutlinedLineDrawer
  {
  protected:
    PrimitiveRenderer& primitiveRenderer;

  public:
    OutlinedLineStripDrawer(PrimitiveRenderer& primitiveRenderer);

    void drawQuad(const vec2* positions, const vec4* colors, const vec2* uvs) final override;
    real antialiasingFactorForPoint(const vec2& point, const vec2& antialiasingDirection) final override;
  };

  class LineStripDrawer final : public OutlinedLineStripDrawer
  {
  public:
    LineStripDrawer(PrimitiveRenderer& primitiveRenderer);

    void drawMainQuad(const vec2* positions, const vec4* colors, const vec2* uvs) final override;
  };

  // Note the AbstractFont class implementation is based on the Gorilla::Caption class
  class AbstractFont
  {
  private:
    real _scale;

  public:
    Gorilla::GlyphData* const _gorillaGlyphData;
    const bool monoSpaceFont : 1;
    bool roundPositition : 1;
    real verticalGlyphOffset;
    real _lineSpacing;
    vec2 cursor;
    real verticalCharacterReplacementOffset;

    MissingCharacterRenderer missingCharacterRenderer;

  public:
    AbstractFont(Gorilla::GlyphData* gorillaGlyphData, bool monoSpaceFont=false);

    static real lineSpacingFor(Gorilla::GlyphData* gorillaGlyphData);

    real baseline() const;
    real lineHeight() const;
    real lineSpacing() const;
    real spaceLength() const;

    real scale() const;
    void setScale(real scale);

  public:
    real textLineWidth(const String& string);

    int scanTextLine(const String& string,
                     const std::function<void(Gorilla::Glyph& glyph)>& handleGlyph,
                     const std::function<void(String::value_type letter)>& handleUnknownCharacter);

  private:
    void _reInit();
  };

  class Font final : public AbstractFont
  {
  // Note the Font class implementation is based on the Gorilla::Caption class
  private:
    PrimitiveRenderer& primitiveRenderer;

  public:
    vec4 color;

  public:
    Font(PrimitiveRenderer& primitiveRenderer, uint glyphDataIndex, bool monoSpaceFont=false);

    /**
     * @param string
     * @return The number of drawn Characters
     */
    int drawTextLine(const String& string);

  private:
    void drawGlyph(Gorilla::Glyph& glyph, const vec2& texelOffset);
    void drawUnknownCharacter(String::value_type character);
  };

public:
  PrimitiveRenderer();

  void drawTriangle(const vec4& color,
                    const vec2& a,
                    const vec2& b,
                    const vec2& c);

  void drawRectangle(const Rectangle<vec2>& shape,
                     const vec4& color=vec4(1),
                     float border = 0.f);
  void drawRectangle(const Rectangle<vec2>& shape,
                     const Rectangle<vec2>& uv,
                     const vec4& color=vec4(1),
                     float border = 0.f);
  void drawRectangle(const vec2& positionCorner1,
                     const vec2& positionCorner2,
                     const vec4& color=vec4(1),
                     float border = 0.f);
  void drawRectangle(const vec2& positionCorner1,
                     const vec2& positionCorner2,
                     const Rectangle<vec2>& uv,
                     const vec4& color=vec4(1),
                     float border = 0.f);
  void drawRectangle(const vec2& positionCorner1,
                     const vec2& positionCorner2,
                     const vec2& uvCorner1,
                     const vec2& uvCorner2,
                     const vec4& color=vec4(1),
                     float border = 0.f);
  void drawFilledRectangle(const Rectangle<vec2>& shape,
                           const vec4& colorUpper,
                           const vec4& colorLower);
  void drawFilledRectangle(const vec2& positionCorner1,
                           const vec2& positionCorner2,
                           const vec2& uvCorner1,
                           const vec2& uvCorner2,
                           const vec4& color1,
                           const vec4& color2);
  void drawRoundedRectangle(const RoundedRectangle& shape,
                            const vec4& color=vec4(1),
                            float border = 0.f,
                            float strokeOffset = 0.f,
                            float axisAlignedAntialiasing = DEFAULT_ANTIALIASING_WIDTH,
                            float roundingAntialiazing = DEFAULT_ANTIALIASING_WIDTH);
  void drawQuad(const vec2* positions, const vec4* colors, const vec2* uvs);
  void drawQuad(const vec2* positions, const vec4& color);
  void drawLine(const vec2& a,
                const vec2& b,
                const vec2& uvA,
                const vec2& uvB,
                const vec4& colorA,
                const vec4& colorB,
                real thickness=1.f,
                real antialiasing = DEFAULT_ANTIALIASING_WIDTH);
  void drawLine(const vec2& a,
                const vec2& b,
                const vec2& uvA,
                const vec2& uvB,
                const vec4& colorA,
                const vec4& colorB,
                real thickness,
                real antialiasingA,
                real antialiasingB);
  void drawLine(const vec2& a,
                const vec2& b,
                const vec4& colorA,
                const vec4& colorB,
                real thickness=1.f,
                real antialiasing = DEFAULT_ANTIALIASING_WIDTH);
  void drawLine(const vec2& a,
                const vec2& b,
                const vec4& colorA,
                const vec4& colorB,
                real thickness,
                real antialiasingA,
                real antialiasingB);
  void drawLine(const vec2& a,
                const vec2& b,
                const vec4& color,
                real thickness=1.f,
                real antialiasing = DEFAULT_ANTIALIASING_WIDTH);

  void drawLineStrip(const QVector<vec2>& vertices,
                     const vec4& color,
                     real thickness=1.f,
                     real antialiasing = DEFAULT_ANTIALIASING_WIDTH,
                     bool closed=false);
  void drawLineLoop(const QVector<vec2>& vertices,
                    const vec4& color,
                    real thickness=1.f,
                    real antialiasing = DEFAULT_ANTIALIASING_WIDTH);

  void drawCircle(const Circle& circle,
                  const vec4& color,
                  float border = 0.f,
                  real antialiasing = DEFAULT_ANTIALIASING_WIDTH);
  void drawCircleBorder(const Circle& circle,
                        const vec4& color,
                        float thickness=sqrt_two,
                        real antialiasing = DEFAULT_ANTIALIASING_WIDTH);
  void drawCircleBorder(const Circle& circle,
                        const std::function<vec2(real)>& uvForAngle,
                        const vec4& color,
                        float thickness=sqrt_two,
                        real antialiasing = DEFAULT_ANTIALIASING_WIDTH);
  void drawArc(const Circle& circle,
               const vec4& color,
               float border = 0.f,
               real fromAngle = 0.f,
               real toAngle = two_pi,
               real antialiasing = DEFAULT_ANTIALIASING_WIDTH,
               real centerAntialiasing = DEFAULT_ANTIALIASING_WIDTH);
  void drawArcBorder(const Circle& circle,
                     const vec4& color,
                     float thickness=sqrt_two,
                     real fromAngle = 0.f,
                     real toAngle = two_pi,
                     real antialiasing = DEFAULT_ANTIALIASING_WIDTH,
                     real centerAntialiasing = DEFAULT_ANTIALIASING_WIDTH,
                     bool connectEnds = false);
  void drawArcBorder(const Circle& circle,
                     const std::function<vec2(real)>& uvForAngle,
                     const vec4& color,
                     float thickness=sqrt_two,
                     real fromAngle = 0.f,
                     real toAngle = two_pi,
                     real antialiasing = DEFAULT_ANTIALIASING_WIDTH,
                     real centerAntialiasing = DEFAULT_ANTIALIASING_WIDTH,
                     bool connectEnds = false);

  virtual real anisotropicAntialiasingRadiusAtVertex(const vec2& point,
                                                     const vec2& antialiasingDirection) const;

protected:
  typedef Gorilla::Vertex _Vertex;
  void _pushVertex(const _Vertex &vertex) override;

private:
  void _drawRectangleBorder(const Rectangle<vec2>& shape,
                            const Rectangle<vec2>& uv,
                            const vec4& color,
                            float border);
  void _drawRectangleBorder(const Rectangle<vec2>& shape,
                            const Rectangle<vec2>& innerShape,
                            const Rectangle<vec2>& uv,
                            const Rectangle<vec2>& innerUv,
                            const vec4& color);

  static real _arcAntialiasing(real angle, real antialiasing, real centerAntialiasing);
  static void _forEachEdgeOfCircle(const Circle& circle,
                                  const std::function<void(real,real)>& function,
                                  real fromAngle=0.f,
                                  real toAngle=two_pi);
};

} // namespace GorillaHelper
} // namespace Framework

#endif // FRAMEWORK_GORILLAHELPER_PRIMITIVERENDERER_H
