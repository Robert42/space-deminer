#include "primitive-renderer.h"

#include <base/interpolation.h>
#include <base/geometry/interval.h>
#include <base/geometry/matrix3-transformations.h>
#include <base/io/log.h>

namespace Framework {
namespace GorillaHelper {


PrimitiveRenderer::PrimitiveRenderer()
{
}


void PrimitiveRenderer::drawTriangle(const vec4& color,
                                     const vec2& a,
                                     const vec2& b,
                                     const vec2& c)
{
  Ogre::Vector2 positions[4] = {Ogre::Vector2(a.x, a.y),
                                Ogre::Vector2(b.x, b.y),
                                Ogre::Vector2(c.x, c.y)
                               };

  this->pushTriangle(positions, Ogre::ColourValue_cast(color));
}


void PrimitiveRenderer::drawRectangle(const Rectangle<vec2>& shape,
                                      const Rectangle<vec2>& uv,
                                      const vec4& color,
                                      float border)
{
  if(border != 0.f)
  {
    _drawRectangleBorder(shape,
                         uv,
                         color,
                         border);
    return;
  }


  drawRectangle(shape.min(),
                shape.max(),
                uv,
                color,
                border);
}

void PrimitiveRenderer::drawRectangle(const Rectangle<vec2>& shape,
                                      const vec4& color,
                                      float border)
{
  drawRectangle(shape.min(),
                shape.max(),
                color,
                border);
}

void PrimitiveRenderer::drawRectangle(const vec2& positionCorner1,
                                      const vec2& positionCorner2,
                                      const vec4& color,
                                      float border)
{
  vec2 solidUV = vec2_cast(getSolidUV());

  drawRectangle(positionCorner1,
                positionCorner2,
                Rectangle<vec2>(solidUV, solidUV),
                color,
                border);
}


void PrimitiveRenderer::drawRectangle(const vec2& positionCorner1,
                                      const vec2& positionCorner2,
                                      const Rectangle<vec2>& uv,
                                      const vec4& color,
                                      float border)
{
  drawRectangle(positionCorner1,
                positionCorner2,
                uv.min(),
                uv.max(),
                color,
                border);
}


void PrimitiveRenderer::drawRectangle(const vec2& positionCorner1,
                                      const vec2& positionCorner2,
                                      const vec2& uvCorner1,
                                      const vec2& uvCorner2,
                                      const vec4& color,
                                      float border)
{
  if(border != 0.f)
  {
    _drawRectangleBorder(Rectangle<vec2>(positionCorner1, positionCorner2),
                         Rectangle<vec2>(uvCorner1, uvCorner2),
                         color,
                         border);
    return;
  }

  drawFilledRectangle(positionCorner1,
                      positionCorner2,
                      uvCorner1,
                      uvCorner2,
                      color,
                      color);
}


void PrimitiveRenderer::drawFilledRectangle(const Rectangle<vec2>& shape,
                                            const vec4& colorUpper,
                                            const vec4& colorLower)
{
  vec2 uv = vec2_cast(getSolidUV());
  drawFilledRectangle(shape.min(), shape.max(), uv, uv, colorUpper, colorLower);
}


void PrimitiveRenderer::drawFilledRectangle(const vec2& positionCorner1,
                                            const vec2& positionCorner2,
                                            const vec2& uvCorner1,
                                            const vec2& uvCorner2,
                                            const vec4& color1,
                                            const vec4& color2)
{
  Ogre::Vector2 positions[4] = {Ogre::Vector2(positionCorner1.x, positionCorner1.y),
                                Ogre::Vector2(positionCorner1.x, positionCorner2.y),
                                Ogre::Vector2(positionCorner2.x, positionCorner2.y),
                                Ogre::Vector2(positionCorner2.x, positionCorner1.y)
                               };
  Ogre::Vector2 uvs[4] = {Ogre::Vector2(uvCorner1.x, uvCorner1.y),
                          Ogre::Vector2(uvCorner1.x, uvCorner2.y),
                          Ogre::Vector2(uvCorner2.x, uvCorner2.y),
                          Ogre::Vector2(uvCorner2.x, uvCorner1.y)
                         };
  Ogre::ColourValue colour1 = Ogre::ColourValue_cast(color1);
  Ogre::ColourValue colour2 = Ogre::ColourValue_cast(color2);
  Ogre::ColourValue colors[4] = {colour1, colour2, colour2, colour1};

  this->pushQuad(positions, colors, uvs);
}


void PrimitiveRenderer::drawQuad(const vec2* positions, const vec4* colors, const vec2* uvs)
{
  Ogre::Vector2 ogrePositions[4] = {Ogre::Vector2_cast(positions[0]),
                                    Ogre::Vector2_cast(positions[1]),
                                    Ogre::Vector2_cast(positions[2]),
                                    Ogre::Vector2_cast(positions[3])
                               };
  Ogre::Vector2 ogreUvs[4] = {Ogre::Vector2_cast(uvs[0]),
                              Ogre::Vector2_cast(uvs[1]),
                              Ogre::Vector2_cast(uvs[2]),
                              Ogre::Vector2_cast(uvs[3])
                             };
  Ogre::ColourValue ogreColors[4] = {Ogre::ColourValue_cast(colors[0]),
                                     Ogre::ColourValue_cast(colors[1]),
                                     Ogre::ColourValue_cast(colors[2]),
                                     Ogre::ColourValue_cast(colors[3])};

  this->pushQuad(ogrePositions, ogreColors, ogreUvs);
}

void PrimitiveRenderer::drawQuad(const vec2* positions, const vec4& color)
{
  Ogre::Vector2 ogrePositions[4] = {Ogre::Vector2_cast(positions[0]),
                                    Ogre::Vector2_cast(positions[1]),
                                    Ogre::Vector2_cast(positions[2]),
                                    Ogre::Vector2_cast(positions[3])
                               };
  Ogre::Vector2 ogreUvs[4] = {getSolidUV(),
                              getSolidUV(),
                              getSolidUV(),
                              getSolidUV()
                             };
  Ogre::ColourValue ogreColors[4] = {Ogre::ColourValue_cast(color),
                                     Ogre::ColourValue_cast(color),
                                     Ogre::ColourValue_cast(color),
                                     Ogre::ColourValue_cast(color)};

  this->pushQuad(ogrePositions, ogreColors, ogreUvs);
}

void PrimitiveRenderer::_pushVertex(const _Vertex &vertex)
{
  Gorilla::CustomRenderer::_pushVertex(vertex);
}

void PrimitiveRenderer::_drawRectangleBorder(const Rectangle<vec2>& shape,
                                             const Rectangle<vec2>& uv,
                                             const vec4& color,
                                             float border)
{
  const vec2 uvBorder(uv.size() * border / shape.size());

  const Rectangle<vec2> innerShape(shape.min() + border, shape.max() - border);
  const Rectangle<vec2> innerUv(uv.min() + uvBorder, uv.max() - uvBorder);

  if(border>0)
    _drawRectangleBorder(shape, innerShape, uv, innerUv, color);
  else
    _drawRectangleBorder(innerShape, shape, innerUv, uv, color);
}


void PrimitiveRenderer::drawRoundedRectangle(const RoundedRectangle& rectangle,
                                             const vec4& color,
                                             float border,
                                             float strokeOffset,
                                             float axisAlignedAntialiasing,
                                             float roundingAntialiazing)
{
  Rectangle<vec2> rectangleShape = rectangle.rectangle();
  real radius = rectangle.radius();

  rectangleShape = rectangleShape.expandedRect(strokeOffset);
  radius += strokeOffset;

  vec2 rectSize = rectangleShape.size();
  radius = min(radius, 0.5f * min(rectSize.x, rectSize.y));
  vec2 p = rectangleShape.min() + vec2(radius);

  if(border > 0.f)
  {
    LineStripDrawer drawer(*this);

    drawer.antialiasing = axisAlignedAntialiasing;
    drawer.color = color;
    drawer.thickness = border;

    drawer.beginLoop();

    drawer.drawArcBorder(Circle(p, radius), // NW
                         pi+half_pi, pi, roundingAntialiazing);
    p.y += rectSize.y - radius*2;
    drawer.drawArcBorder(Circle(p, radius), // SW
                         pi, half_pi, roundingAntialiazing);
    p.x += rectSize.x - radius*2;
    drawer.drawArcBorder(Circle(p, radius), // SE
                         half_pi, 0.f, roundingAntialiazing);
    p.y -= rectSize.y - radius*2;
    drawer.drawArcBorder(Circle(p, radius), // NE
                         two_pi, pi+half_pi, roundingAntialiazing);

    drawer.end();
  }else
  {
    drawArc(Circle(p, radius), color, 0.f, pi, pi+half_pi, 0.f, roundingAntialiazing); // NW
    p.y += rectSize.y - radius*2;
    drawArc(Circle(p, radius), color, 0.f, half_pi, pi, 0.f, roundingAntialiazing); // SW
    p.x += rectSize.x - radius*2;
    drawArc(Circle(p, radius), color, 0.f, 0.f, half_pi, 0.f, roundingAntialiazing); // SE
    p.y -= rectSize.y - radius*2;
    drawArc(Circle(p, radius), color, 0.f, pi+half_pi, two_pi, 0.f, roundingAntialiazing); // NE

    Rectangle<vec2> innerVerticalRectangle(rectangleShape.min() + vec2(radius, 0),
                                           rectangleShape.max() - vec2(radius, 0));
    Rectangle<vec2> innerRectangleWest(rectangleShape.min() + vec2(0, radius),
                                       vec2(rectangleShape.min().x + radius, rectangleShape.max().y - radius));
    Rectangle<vec2> innerRectangleEast = innerRectangleWest + vec2(rectangleShape.size().x - radius, 0);

    drawRectangle(innerVerticalRectangle, color, border);
    drawRectangle(innerRectangleWest, color, border);
    drawRectangle(innerRectangleEast, color, border);
  }
}


void PrimitiveRenderer::_drawRectangleBorder(const Rectangle<vec2>& outerShape,
                                             const Rectangle<vec2>& innerShape,
                                             const Rectangle<vec2>& outerUv,
                                             const Rectangle<vec2>& innerUv,
                                             const vec4& color)
{
  /*
   * a-------------------------------+
   * |                               |
   * |   c-----------------------+   f
   * |   |                       |   |
   * |   |                       |   |
   * |   |                       |   |
   * |   |                       |   |
   * |   |                       |   |
   * e   +-----------------------d   |
   * |                               |
   * +-------------------------------b
   *
   * +-------------------------------+
   * |             n                 |
   * +---+-----------------------+---+
   * |   |                       |   |
   * |   |                       |   |
   * | w |                       | e |
   * |   |                       |   |
   * |   |                       |   |
   * +---+-----------------------+---+
   * |             s                 |
   * +-------------------------------+
   *
   */

#define POINTS \
  const vec2& a = o.min(); \
  const vec2& b = o.max(); \
  const vec2& c = i.min(); \
  const vec2& d = i.max(); \
  const vec2 e(a.x, d.y); \
  const vec2 f(b.x, c.y); \
  (void)e; \
  (void)f;

  auto n = [](const Rectangle<vec2>& o, const Rectangle<vec2>& i) {
    POINTS
    return Rectangle<vec2>(a, f);
  };
  auto w = [](const Rectangle<vec2>& o, const Rectangle<vec2>& i) {
    POINTS
    return Rectangle<vec2>(e, c);
  };
  auto s = [](const Rectangle<vec2>& o, const Rectangle<vec2>& i) {
    POINTS
    return Rectangle<vec2>(e, b);
  };
  auto e = [](const Rectangle<vec2>& o, const Rectangle<vec2>& i) {
    POINTS
    return Rectangle<vec2>(d, f);
  };


  drawRectangle(n(outerShape, innerShape),
                n(outerUv, innerUv),
                color);
  drawRectangle(w(outerShape, innerShape),
                w(outerUv, innerUv),
                color);
  drawRectangle(s(outerShape, innerShape),
                s(outerUv, innerUv),
                color);
  drawRectangle(e(outerShape, innerShape),
                e(outerUv, innerUv),
                color);
}

void PrimitiveRenderer::drawLine(const vec2& a,
                                 const vec2& b,
                                 const vec2& uvA,
                                 const vec2& uvB,
                                 const vec4& colorA,
                                 const vec4& colorB,
                                 real thickness,
                                 real antialiasing)
{
  drawLine(a, b, uvA, uvB, colorA, colorB, thickness, antialiasing, antialiasing);
}

void PrimitiveRenderer::drawLine(const vec2& a,
                                 const vec2& b,
                                 const vec2& uvA,
                                 const vec2& uvB,
                                 const vec4& colorA,
                                 const vec4& colorB,
                                 real thickness,
                                 real antialiasingA,
                                 real antialiasingB)
{
  vec2 antialiasingDirection = perpendicular90Degree(normalize(b-a));

  return this->pushLine(Ogre::Vector2_cast(a),
                        Ogre::Vector2_cast(b),
                        Ogre::Vector2_cast(uvA),
                        Ogre::Vector2_cast(uvB),
                        Ogre::ColourValue_cast(colorA),
                        Ogre::ColourValue_cast(colorB),
                        thickness,
                        thickness,
                        antialiasingA * anisotropicAntialiasingRadiusAtVertex(a, antialiasingDirection),
                        antialiasingB * anisotropicAntialiasingRadiusAtVertex(b, antialiasingDirection));
}

void PrimitiveRenderer::drawLine(const vec2& a,
                                 const vec2& b,
                                 const vec4& colorA,
                                 const vec4& colorB,
                                 real thickness,
                                 real antialiasing)
{
  drawLine(a, b, colorA, colorB, thickness, antialiasing, antialiasing);
}

void PrimitiveRenderer::drawLine(const vec2& a,
                                 const vec2& b,
                                 const vec4& colorA,
                                 const vec4& colorB,
                                 real thickness,
                                 real antialiasingA,
                                 real antialiasingB)
{
  vec2 solidUV = vec2_cast(getSolidUV());
  drawLine(a, b, solidUV, solidUV, colorA, colorB, thickness, antialiasingA, antialiasingB);
}

void PrimitiveRenderer::drawLine(const vec2& a,
                                 const vec2& b,
                                 const vec4& color,
                                 real thickness,
                                 real antialiasing)
{
  drawLine(a, b, color, color, thickness, antialiasing, antialiasing);
}



void PrimitiveRenderer::drawLineStrip(const QVector<vec2>& vertices,
                                      const vec4& color,
                                      real thickness,
                                      real antialiasing,
                                      bool closed)
{
  int n = vertices.size();
  for(int i=n-1+closed; i>=1; --i)
  {
    const vec2& a = vertices[i%n];
    const vec2& b = vertices[(i-1)%n];

    drawLine(a, b, color, thickness, antialiasing);
  }
}

void PrimitiveRenderer::drawLineLoop(const QVector<vec2>& vertices,
                                     const vec4& color,
                                     real thickness,
                                     real antialiasing)
{
  drawLineStrip(vertices, color, thickness, antialiasing, true);
}

void PrimitiveRenderer::drawCircle(const Circle& circle,
                                   const vec4& color,
                                   float border,
                                   real antialiasing)
{
  drawArc(circle, color, border, 0.f, two_pi, antialiasing);
}

void PrimitiveRenderer::drawArc(const Circle& circle,
                                const vec4& color,
                                float border,
                                real fromAngle,
                                real toAngle,
                                real antialiasing,
                                real centerAntialiasing)
{
  if(border > 0)
  {
    drawArcBorder(circle, color, border, fromAngle, toAngle, antialiasing, centerAntialiasing);
    return;
  }

  Ogre::ColourValue colour = Ogre::ColourValue_cast(color);
  Ogre::ColourValue transparentColour = Ogre::ColourValue_cast(vec4(color.rgb(), 0.f));
  Ogre::ColourValue antialiasingColours[4] = {colour,
                                              transparentColour,
                                              transparentColour,
                                              colour};

  _forEachEdgeOfCircle(circle, [this, &antialiasingColours, &colour, &circle, antialiasing, centerAntialiasing](real angle1, real angle2){
    vec2 v1 = circle.edgePointForAngle(angle1);
    vec2 v2 = circle.edgePointForAngle(angle2);

    real antialiasing1 = _arcAntialiasing(angle1, antialiasing, centerAntialiasing);
    real antialiasing2 = _arcAntialiasing(angle2, antialiasing, centerAntialiasing);

    vec2 antialiasing1Offset = Circle::directionForAngle(angle1)*antialiasing1;
    vec2 antialiasing2Offset = Circle::directionForAngle(angle2)*antialiasing2;

    v1 -= antialiasing1Offset;
    v2 -= antialiasing2Offset;

    Ogre::Vector2 vertices[6] = {Ogre::Vector2_cast(v1),
                                 Ogre::Vector2_cast(v1+antialiasing1Offset),
                                 Ogre::Vector2_cast(v2+antialiasing2Offset),
                                 Ogre::Vector2_cast(v2),
                                 Ogre::Vector2_cast(circle.centerPosition()),
                                 Ogre::Vector2_cast(v1)};

    pushTriangle(vertices+3, colour);

    if(antialiasing1 || antialiasing2)
    {
      pushQuad(vertices, antialiasingColours);
    }
  }, fromAngle, toAngle);
}


void PrimitiveRenderer::drawCircleBorder(const Circle& circle,
                                         const vec4& color,
                                         float thickness,
                                         real antialiasing)
{
  drawArcBorder(circle, color, thickness, 0.f, two_pi, antialiasing, antialiasing, true);
}


void PrimitiveRenderer::drawCircleBorder(const Circle& circle,
                                         const std::function<vec2(real)>& uvForAngle,
                                         const vec4& color,
                                         float thickness,
                                         real antialiasing)
{
  LineStripDrawer lineDrawer(*this);

  lineDrawer.color = color;
  lineDrawer.thickness = thickness;
  lineDrawer.antialiasing = antialiasing;

  lineDrawer.begin();
  lineDrawer.drawArcBorder(circle,
                           uvForAngle,
                           0,
                           two_pi,
                           antialiasing,
                           false);
  lineDrawer.endFacingLastAndFirstTangent();
}

void PrimitiveRenderer::drawArcBorder(const Circle& circle,
                                      const vec4& color,
                                      float thickness,
                                      real fromAngle,
                                      real toAngle,
                                      real antialiasing,
                                      real centerAntialiasing,
                                      bool connectEnds)
{
 vec2 solidUV = vec2_cast(getSolidUV());
 drawArcBorder(circle, [solidUV](real)->vec2{return solidUV;}, color, thickness, fromAngle, toAngle, antialiasing, centerAntialiasing, connectEnds);
}


void PrimitiveRenderer::drawArcBorder(const Circle& circle,
                                      const std::function<vec2(real)>& uvForAngle,
                                      const vec4& color,
                                      float thickness,
                                      real fromAngle,
                                      real toAngle,
                                      real antialiasing,
                                      real centerAntialiasing,
                                      bool connectEnds)
{
  LineStripDrawer lineDrawer(*this);

  lineDrawer.color = color;
  lineDrawer.thickness = thickness;
  lineDrawer.antialiasing = antialiasing;

  if(connectEnds)
    lineDrawer.beginLoop();
  else
    lineDrawer.begin();
  lineDrawer.drawArcBorder(circle,
                           uvForAngle,
                           fromAngle,
                           toAngle,
                           centerAntialiasing,
                           connectEnds);
  lineDrawer.end();
}


real PrimitiveRenderer::_arcAntialiasing(real angle, real antialiasing, real centerAntialiasing)
{
  return Gui::BaseLineDrawer::_arcAntialiasing(angle, antialiasing, centerAntialiasing);
}

void PrimitiveRenderer::_forEachEdgeOfCircle(const Circle& circle,
                                            const std::function<void(real,real)>& function,
                                            real fromAngle,
                                            real toAngle)
{
  Gui::BaseLineDrawer::_forEachEdgeOfCircle(circle, function, fromAngle, toAngle);
}


real PrimitiveRenderer::anisotropicAntialiasingRadiusAtVertex(const vec2& point,
                                                              const vec2& antialiasingDirection) const
{
  (void)point;
  (void)antialiasingDirection;
  return 1.f;
}

// ====

PrimitiveRenderer::OutlinedLineStripDrawer::OutlinedLineStripDrawer(PrimitiveRenderer& primitiveRenderer)
  : primitiveRenderer(primitiveRenderer)
{
  this->uv = vec2_cast(primitiveRenderer.getSolidUV());
  outline.uv = this->uv;
}


void PrimitiveRenderer::OutlinedLineStripDrawer::drawQuad(const vec2* positions,
                                                          const vec4* colors,
                                                          const vec2* uvs)
{
  primitiveRenderer.drawQuad(positions,
                             colors,
                             uvs);
}


real PrimitiveRenderer::OutlinedLineStripDrawer::antialiasingFactorForPoint(const vec2& point,
                                                                            const vec2& antialiasingDirection)
{
  return primitiveRenderer.anisotropicAntialiasingRadiusAtVertex(point, antialiasingDirection);
}


PrimitiveRenderer::LineStripDrawer::LineStripDrawer(PrimitiveRenderer& primitiveRenderer)
  : OutlinedLineStripDrawer(primitiveRenderer)
{
}

void PrimitiveRenderer::LineStripDrawer::drawMainQuad(const vec2* positions,
                                                      const vec4* colors,
                                                      const vec2* uvs)
{
  primitiveRenderer.drawQuad(positions,
                             colors,
                             uvs);
}


// ====


PrimitiveRenderer::AbstractFont::AbstractFont(Gorilla::GlyphData* gorillaGlyphData, bool monoSpaceFont)
  : _gorillaGlyphData(gorillaGlyphData),
    monoSpaceFont(monoSpaceFont),
    roundPositition(false),
    verticalGlyphOffset(0.f),
    _lineSpacing(lineSpacingFor(_gorillaGlyphData)),
    cursor(0),
    verticalCharacterReplacementOffset(0.f)
{
  assert(_gorillaGlyphData);

  if(!_gorillaGlyphData)
    IO::Log::logError("PrimitiveRenderer::AbstractFont::AbstractFont(): No Glyph data");

  _scale = 1.f;

  _reInit();
}

void PrimitiveRenderer::AbstractFont::_reInit()
{
  if(monoSpaceFont)
    missingCharacterRenderer.numberDependentMetrics.initForMonoFont(vec2(_gorillaGlyphData->mMonoWidth,
                                                                         _gorillaGlyphData->mLineHeight)*scale());
  else
    missingCharacterRenderer.numberDependentMetrics.initForFontHeight(_gorillaGlyphData->mLineHeight*scale());
}

real PrimitiveRenderer::AbstractFont::lineSpacingFor(Gorilla::GlyphData* gorillaGlyphData)
{
  real lineSpacing = gorillaGlyphData->mLineSpacing;

  if(lineSpacing != 0.f)
    return lineSpacing;
  else
    return floor(abs(gorillaGlyphData->mLineHeight - gorillaGlyphData->mBaseline) * 0.333f);
}

real PrimitiveRenderer::AbstractFont::baseline() const
{
  return _gorillaGlyphData->mBaseline * scale();
}

real PrimitiveRenderer::AbstractFont::lineHeight() const
{
  return _gorillaGlyphData->mLineHeight * scale();
}

real PrimitiveRenderer::AbstractFont::lineSpacing() const
{
  return _lineSpacing * scale();
}

real PrimitiveRenderer::AbstractFont::spaceLength() const
{
  return _gorillaGlyphData->mSpaceLength * scale();
}

real PrimitiveRenderer::AbstractFont::scale() const
{
  return _scale;
}

void PrimitiveRenderer::AbstractFont::setScale(real scale)
{
  if(this->_scale != scale)
  {
    this->_scale = scale;
    _reInit();
  }
}

real PrimitiveRenderer::AbstractFont::textLineWidth(const String& string)
{
  const vec2 startCursor = this->cursor;

  scanTextLine(string,
               [](const Gorilla::Glyph&){},
               [](String::value_type){});

  real width = abs(this->cursor.x - startCursor.x);

  this->cursor = startCursor;

  return width;
}

// Note this implementation is based on the Gorilla::Caption class
int PrimitiveRenderer::AbstractFont::scanTextLine(const String& string,
                                          const std::function<void(Gorilla::Glyph& glyph)>& handleGlyph,
                                          const std::function<void(String::value_type letter)>& handleUnknownCharacter)
{
  if(!_gorillaGlyphData)
    return 0;

  const real spaceLength = this->_gorillaGlyphData->mSpaceLength * scale();
  const real letterSpacing = this->_gorillaGlyphData->mLetterSpacing * scale();
  const real monoWidth = this->_gorillaGlyphData->mMonoWidth * scale();

  Interval<String::value_type> knownGlyphs(this->_gorillaGlyphData->mRangeBegin,
                                           this->_gorillaGlyphData->mRangeEnd);

  String::value_type previousCharacter = 0;
  String::value_type currentCharacter = 0;

  size_t i;
  for(i=0; i<string.length(); ++i)
  {
    previousCharacter = currentCharacter;
    currentCharacter = string[i];

    if(currentCharacter == '\n')
      break;
    if(currentCharacter == ' ')
    {
      cursor.x += spaceLength;
      continue;
    }

    Gorilla::Glyph* glyph = nullptr;
    bool knownGlyph = knownGlyphs.contains(currentCharacter) &&
                      (glyph = this->_gorillaGlyphData->getGlyph(currentCharacter));

    if(knownGlyph)
    {
      real kerning = glyph->getKerning(previousCharacter) * scale();
      if(!kerning)
        kerning = letterSpacing;

      handleGlyph(*glyph);

      if(monoSpaceFont)
        cursor.x += monoWidth;
      else
        cursor.x += glyph->glyphAdvance * scale() + kerning;
    }else
    {
      handleUnknownCharacter(currentCharacter);

      if(monoSpaceFont)
        cursor.x += monoWidth;
      else
        cursor.x += missingCharacterRenderer.numberDependentMetrics.metrics(currentCharacter).frameSize().x + letterSpacing;
    }
  }

  return i;
}


// ====


PrimitiveRenderer::Font::Font(PrimitiveRenderer& primitiveRenderer, uint glyphDataIndex, bool monoSpaceFont)
  : AbstractFont(primitiveRenderer.layer()._getGlyphData(glyphDataIndex), monoSpaceFont),
    primitiveRenderer(primitiveRenderer),
    color(1)
{
  missingCharacterRenderer.drawTriangle = std::bind(&PrimitiveRenderer::drawTriangle, &primitiveRenderer, std::cref(color), _1, _2, _3);
}

// Note this implementation is based on the Gorilla::Caption class
int PrimitiveRenderer::Font::drawTextLine(const String& string)
{
  const vec2 texelOffset = vec2(primitiveRenderer.layer()._getTexelX(),
                                primitiveRenderer.layer()._getTexelY());

  return scanTextLine(string,
                      std::bind(&PrimitiveRenderer::Font::drawGlyph, this, _1, std::cref(texelOffset)),
                      std::bind(&PrimitiveRenderer::Font::drawUnknownCharacter, this, _1));
}


// Note this implementation is based on the Gorilla::Caption class
void PrimitiveRenderer::Font::drawGlyph(Gorilla::Glyph& glyph, const vec2& texelOffset)
{
  const real monoWidth = this->_gorillaGlyphData->mMonoWidth * scale();

  vec2 glyphSize = vec2(glyph.glyphWidth, glyph.glyphHeight) * scale();

  Rectangle<vec2> position(-texelOffset,
                           glyphSize);
  position += vec2(0, glyph.verticalOffset + verticalGlyphOffset) * scale() + cursor;
  if(roundPositition)
    position = round(position);

  if(monoSpaceFont)
    position += vec2(floor((monoWidth - glyphSize.x)*0.5f),
                     0);
  if(roundPositition)
    position = round(position);

  Rectangle<vec2> uv(vec2(glyph.uvLeft, glyph.uvTop),
                     vec2(glyph.uvRight, glyph.uvBottom));

  primitiveRenderer.drawRectangle(position, uv, color);
}

void PrimitiveRenderer::Font::drawUnknownCharacter(String::value_type character)
{
  missingCharacterRenderer.matrix = Geometry::translate3(cursor + vec2(0, verticalCharacterReplacementOffset));
  missingCharacterRenderer.drawMissingCharacterWithFrame(character);
}


} // namespace GorillaHelper
} // namespace Framework
