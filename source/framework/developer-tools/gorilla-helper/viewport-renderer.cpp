#include "viewport-renderer.h"

namespace Framework {
namespace GorillaHelper {

ViewportRenderer::ViewportRenderer()
{
  internOffset = Ogre::Vector2::ZERO;
  offset = Ogre::Vector2::ZERO;
  numberClippingLocks = 0;
  numberIgnoreViewportLocks = 0;
}

void ViewportRenderer::setScissor(const Scissor& scissor)
{
  this->scissor = scissor;
  _updateInternScissor();
}

void ViewportRenderer::setViewport(const Scissor& viewport, const Ogre::Vector2& offset)
{
  this->viewport = viewport;
  this->offset = offset;
  _updateInternScissor();
}

void ViewportRenderer::setScissor(int x, int y, int width, int height)
{
  Ogre::Vector2 min(x, y);
  Ogre::Vector2 size(width, height);
  Ogre::Vector2 max = min + size;
  this->scissor.setScissor(min, max);
  _updateInternScissor();
}

void ViewportRenderer::setViewport(int x, int y, int width, int height, int offsetX, int offsetY)
{
  Ogre::Vector2 min(x, y);
  Ogre::Vector2 size(width, height);
  Ogre::Vector2 max = min + size;
  this->viewport.setScissor(min, max);
  this->offset = Ogre::Vector2(offsetX, offsetY);
  _updateInternScissor();
}

void ViewportRenderer::setInfiniteScissor()
{
  this->scissor.setInfiniteLarge();
  _updateInternScissor();
}

void ViewportRenderer::setInfiniteViewport()
{
  this->viewport.setInfiniteLarge();
  this->offset = Ogre::Vector2(0, 0);
  _updateInternScissor();
}

void ViewportRenderer::_updateInternScissor()
{
  internScissor = viewport;
  internScissor &= scissor;
  internOffset = viewport.min() + offset;
}

void ViewportRenderer::_pushTriangle(size_t indexA, size_t indexB, size_t indexC, const Ogre::Vector2* positions, const Ogre::ColourValue* colors, const Ogre::Vector2* uvs)
{
  if(numberIgnoreViewportLocks || internScissor.infiniteLarge())
  {
    CustomRenderer::_pushTriangle(indexA, indexB, indexC, positions, colors, uvs);
    return;
  }

  Ogre::Vector2 p[4];
  p[indexA] = positions[indexA]+internOffset;
  p[indexB] = positions[indexB]+internOffset;
  p[indexC] = positions[indexC]+internOffset;

  if(numberClippingLocks)
  {
    CustomRenderer::_pushTriangle(indexA, indexB, indexC, p, colors, uvs);
    return;
  }

  Scissor::Mask maskA = Scissor::Mask::maskFor(internScissor, p[indexA]);
  Scissor::Mask maskB = Scissor::Mask::maskFor(internScissor, p[indexB]);
  Scissor::Mask maskC = Scissor::Mask::maskFor(internScissor, p[indexC]);

  bool completelyOutsideScissor = (maskA & maskB & maskC).bits() != 0;

  if(completelyOutsideScissor)
    return;

  bool completelyInsideScissor = (maskA | maskB | maskC).bits() == 0;

  if(completelyInsideScissor)
  {
    CustomRenderer::_pushTriangle(indexA, indexB, indexC, p, colors, uvs);
    return;
  }

  Scissor::Polygon polygon;
  polygon.setToTriangle(maskA, maskB, maskC,
                        indexA, indexB, indexC,
                        p,
                        colors,
                        uvs);

  polygon = internScissor.clip(polygon);

  IgnoreViewportLock ignoreViewportLock = _ignoreViewport();
  CustomRenderer::pushConvexPolygon(polygon.nVertices,
                                    polygon.positions,
                                    polygon.colors,
                                    polygon.uvs);
  (void)ignoreViewportLock;
}

ViewportRenderer::ClippingLock ViewportRenderer::disableClipping()
{
  return ClippingLock(numberClippingLocks);
}

ViewportRenderer::IgnoreViewportLock ViewportRenderer::_ignoreViewport()
{
  return IgnoreViewportLock(numberIgnoreViewportLocks);
}

} // namespace GorillaHelper
} // namespace Framework
