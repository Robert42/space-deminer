#include "scissor.h"

namespace Framework {
namespace GorillaHelper {


TEST(framework_tweakui_gorilla_Scissor_Mask, getMaskFor)
{
  Scissor scissor(Ogre::Vector2(0, 0), Ogre::Vector2(5, 2));

  EXPECT_TRUE(Scissor::Mask::maskFor(scissor, Ogre::Vector2( 2, 1)).isInCenter());
  EXPECT_EQ(Scissor::Mask::onlyS(), Scissor::Mask::maskFor(scissor, Ogre::Vector2( 2, 3)));
  EXPECT_EQ(Scissor::Mask::onlyW(), Scissor::Mask::maskFor(scissor, Ogre::Vector2(-1, 1)));
}


TEST(framework_tweakui_gorilla_Scissor, clip)
{
  Scissor scissor(Ogre::Vector2(0, 0), Ogre::Vector2(5, 2));

  Scissor::Polygon triangle;
  triangle.appendVertex(scissor, Ogre::Vector2( 2, 1));
  triangle.appendVertex(scissor, Ogre::Vector2( 2, 3));
  triangle.appendVertex(scissor, Ogre::Vector2(-1, 1));

  Scissor::Polygon clipped = scissor.clip(triangle);

  EXPECT_EQ(5, clipped.nVertices);
  EXPECT_EQ(Ogre::Vector2(2, 1), clipped.positions[0]);
  EXPECT_EQ(Ogre::Vector2(2, 2), clipped.positions[1]);
}


}
}
