#ifndef FRAMEWORK_SYSTEMFONT_H
#define FRAMEWORK_SYSTEMFONT_H

#include <base/io/file.h>
#include <base/enum-macros.h>


namespace Framework {

using namespace Base;

class SystemFont
{
public:
  BEGIN_ENUMERATION(Flags,:int)
    NORMAL = 0,
    BOLD = 1,
    MONOSPACE = 2,
  ENUMERATION_BASIC_OPERATORS(Flags)
  ENUMERATION_FLAG_OPERATORS(Flags)
  END_ENUMERATION;

private:
  class Style
  {
  public:
    IO::File fontFile;
    CEGUI::String resourceGroup, fontName;

    // The reason for using a set of strings instead of floats is to catch up for
    // different float values, which would result in the same name, to avoid nexpected
    // behavior, when trying to create two different fonts with the same name
    QSet<String> loadedSizes;

    void init(Style& defaultStyle, const IO::File& fontFile);

    CEGUI::String loadSize(float size);

    bool isInitialized() const;
  };

private:
  Style style[4];

public:
  SystemFont();


  String getFontName(float size, Flags flags=Flags::NORMAL);

private:
  void init();

  static IO::File findFontFile(Flags flags);

  // This function must be implemented once for each operating system
  static IO::File findFontFile(bool bold, bool monospace);
};

extern SystemFont systemFont;


} // namespace Framework

#endif // FRAMEWORK_SYSTEMFONTS_H
