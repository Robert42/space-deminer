#include "code-generator-tool.h"

#include <framework/application.h>
#include <framework/developer-tools/developer-tools.h>

namespace Framework {

CodeGeneratorTool::CodeGeneratorTool()
{
  bar = TweakUI::Bar::create("Code-Generator");
  TweakUI::Layers::nodeEditor->addBar(bar);

  bar->setHelp(Editor::Base::ObjectEditor2D::Controller::shortcutHelpText());

  bar->addButton("Close", std::bind(&CodeGeneratorTool::quit, this), "key=ESCAPE help='Closes the Code Generator.'");

  RenderWindow::signalWindowResized().connect(std::bind(&CodeGeneratorTool::updateWindowSize, this)).track(this->trackable);

  updateWindowSize();

  TweakUI::Layers::nodeEditor->show();
}

CodeGeneratorTool::~CodeGeneratorTool()
{
  TweakUI::Layers::nodeEditor->hide();
}


void CodeGeneratorTool::quit()
{
  FrameSignals::callOnce(std::bind(&CodeGeneratorTool::quitNow, this));
}


void CodeGeneratorTool::quitNow()
{
  Application::developerTools().quitCodeGenerator();
}

void CodeGeneratorTool::updateWindowSize()
{
  nodeEditor.view()->setViewportSize(RenderWindow::size());
}


} // namespace Framework
