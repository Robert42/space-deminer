#include "texture-viewer.h"

#include <base/io/log.h>

namespace Framework {


class TextureViewer::TextureModel : public Editor::Base::Editor2D::Model
{
public:
  TextureModel()
  {
  }

  void setTexture(const Ogre::TexturePtr& tex)
  {
    _boundingRect.set(Rectangle<vec2>(vec2(0), vec2(tex->getWidth(), tex->getHeight())));
  }
};


QMap<TextureViewer::Usage::Value, Gorilla::Screen*> TextureViewer::_gorillaScreens;


TextureViewer::TextureViewer(Usage usage)
  : Layer(Layer::ZLayer::Value(0),
          *_gorillaScreens[usage.value]),
    usage(usage)
{
  model = std::shared_ptr<TextureModel>(new TextureModel);
  Editor::Base::Editor2D::View::Ptr view = Editor::Base::Editor2D::View::create(model);
  view->addConstraint_ClampPositionToModelBoundingBox();

  controller = Editor::Base::Editor2D::Controller::create(view);
  inputHandler = Editor::BlenderStyleInput::Editor2D::InputHandler::create(controller,
                                                                           InputDevice::ActionPriority::TEXTURE_VIEWER,
                                                                           InputDevice::ActionPriority::TEXTURE_VIEWER_BLOCKER);
  renderer = new Renderer(view);

  gorillaLayer().appendCustomRenderer(renderer);

  _checkerboard = Checkerboard::NONE;
  setCheckerboard(Checkerboard::MIDDLE);
  _displayMode = DisplayMode::NORMAL;
  _displayFilter = DisplayFilter::NONE;
  _displayHighlight = DisplayHighlight::ALL;
  rangeStart = 0.f;
  rangeEnd = 1.f;

  renderer->setDisplayMode(this->displayMode());
  renderer->setDisplayFilter(this->displayFilter());
  renderer->setDisplayHighlight(this->displayHighlight());
  renderer->setDisplayRange(this->displayRangeStart(), this->displayRangeEnd());

  show();
}


TextureViewer::~TextureViewer()
{
}


void TextureViewer::setTexture(const Ogre::String& name)
{
  Ogre::TexturePtr tex = Ogre::TextureManager::getSingleton().getByName(name);

  if(tex.isNull())
  {
    IO::Log::logError("TextureViewer::setTexture: The texture %0 is not loaded yet.", name);
    return;
  }

  Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().getByName(materialName());
  material->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setTextureName(name);

  model->setTexture(tex);
  _signalTextureChanged(tex);
}


void TextureViewer::setCheckerboard(Checkerboard checkerboard)
{
  if(checkerboard != this->checkerboard())
  {
    this->_checkerboard = checkerboard;

    float dark = 0.25;
    float light = 1.0f-dark;

    vec4& lightTile = renderer->lightTile;
    vec4& darkTile = renderer->darkTile;

    switch(checkerboard.value)
    {
    case Checkerboard::BLACK:
      lightTile = darkTile = vec4(0.f, 0.f, 0.f, 1.f);
      break;
    case Checkerboard::GREY:
      lightTile = darkTile = vec4(.5f, .5f, .5f, 1.f);
      break;
    case Checkerboard::WHITE:
      lightTile = darkTile = vec4(1.f, 1.f, 1.f, 1.f);
      break;
    case Checkerboard::DARK:
      lightTile = vec4(dark, dark, dark, 1.f);
      darkTile = vec4(0.f, 0.f, 0.f, 1.f);
      break;
    case Checkerboard::MIDDLE:
      lightTile = vec4(light, light, light, 1.f);
      darkTile = vec4(dark, dark, dark, 1.f);
      break;
    case Checkerboard::LIGHT:
      lightTile = vec4(1.f, 1.f, 1.f, 1.f);
      darkTile = vec4(light, light, light, 1.f);
      break;
    case Checkerboard::NONE:
    default:
      lightTile = darkTile = vec4(0.f, 0.f, 0.f, 0.f);
    }

    renderer->makeDirty();
  }
}


void TextureViewer::setCheckerboardByIndex(int index)
{
  setCheckerboard(static_cast<Checkerboard::Value>(index));
}


void TextureViewer::setGrayBackground(bool grayBackground)
{
  if(renderer->grayBackground != grayBackground)
  {
    renderer->grayBackground = grayBackground;
    renderer->makeDirty();
  }
}


bool TextureViewer::grayBackground() const
{
  return renderer->grayBackground;
}


TextureViewer::Checkerboard TextureViewer::checkerboard() const
{
  return _checkerboard;
}

void TextureViewer::setDisplayMode(DisplayMode mode)
{
  if(this->displayMode() != mode)
  {
    this->_displayMode = mode;
    renderer->setDisplayMode(mode);
    renderer->makeDirty();
  }
}

void TextureViewer::setDisplayModeByIndex(int index)
{
  setDisplayMode(static_cast<DisplayMode::Value>(index));
}

TextureViewer::DisplayMode TextureViewer::displayMode() const
{
  return this->_displayMode;
}

void TextureViewer::setDisplayFilter(DisplayFilter filter)
{
  if(this->displayFilter()!= filter)
  {
    this->_displayFilter = filter;
    renderer->setDisplayFilter(filter);
    renderer->makeDirty();
  }
}

void TextureViewer::setDisplayFilterByIndex(int index)
{
  setDisplayFilter(static_cast<DisplayFilter::Value>(index));
}

TextureViewer::DisplayFilter TextureViewer::displayFilter() const
{
  return this->_displayFilter;
}

void TextureViewer::setDisplayRangeStart(real displayRangeStart)
{
  if(this->rangeStart!= displayRangeStart)
  {
    this->rangeStart = displayRangeStart;
    renderer->setDisplayRange(this->rangeStart, this->rangeEnd);
    renderer->makeDirty();
  }
}

real TextureViewer::displayRangeStart() const
{
  return this->rangeStart;
}

void TextureViewer::setDisplayRangeEnd(real displayRangeEnd)
{
  if(this->rangeEnd!= displayRangeEnd)
  {
    this->rangeEnd = displayRangeEnd;
    renderer->setDisplayRange(this->rangeStart, this->rangeEnd);
    renderer->makeDirty();
  }
}

real TextureViewer::displayRangeEnd() const
{
  return this->rangeEnd;
}

void TextureViewer::setDisplayHighlight(DisplayHighlight highlight)
{
  if(this->_displayHighlight!= highlight)
  {
    this->_displayHighlight = highlight;
    renderer->setDisplayHighlight(highlight);
    renderer->makeDirty();
  }
}

void TextureViewer::setDisplayHighlightByIndex(int index)
{
  setDisplayHighlight(static_cast<DisplayHighlight::Value>(index));
}

TextureViewer::DisplayHighlight TextureViewer::displayHighlight() const
{
  return _displayHighlight;
}


Ogre::String TextureViewer::materialName() const
{
  switch(usage.value)
  {
  case Usage::TEXTURE_BROWSER:
  default:
    return "Gorilla2D.texture-viewer-browser";
  }
}


const Editor::Base::Editor2D::View::Ptr& TextureViewer::view()
{
  return this->renderer->view;
}


void TextureViewer::setInvertY(bool invertY)
{
  view()->setInvertY(invertY);
}


bool TextureViewer::invertY() const
{
  return this->renderer->view->invertY();
}


Signals::Signal<void(const Ogre::TexturePtr& texture)>& TextureViewer::signalTextureChanged()
{
  return _signalTextureChanged;
}



const real BLINK_TIME = 0.2f;


TextureViewer::Renderer::Renderer(const Editor::Base::Editor2D::View::Ptr& view)
  : view(view),
    renderer(view)
{
  temporaryInvisible = false;
  noHighlight = true;
  grayBackground = false;

  renderer.signalMarkedAsDirty().connect(std::bind(&Renderer::makeDirty, this)).trackManually();
  timer.signal().connect(std::bind(&TextureViewer::Renderer::blink, this)).trackManually();
}


void TextureViewer::Renderer::setDisplayMode(DisplayMode mode)
{
  this->displayParameter.r = static_cast<real>(mode.value);
}


void TextureViewer::Renderer::setDisplayFilter(DisplayFilter filter)
{
  this->displayParameter.g = static_cast<real>(filter.value);
}


void TextureViewer::Renderer::setDisplayRange(real begin, real end)
{
  this->displayParameter.b = static_cast<real>(begin);
  if(end!=begin)
    this->displayParameter.a = 1.f / static_cast<real>(end-begin);
  else
    this->displayParameter.a = 0.f;

  this->highlightParameter.b = this->displayParameter.b;
  this->highlightParameter.a = this->displayParameter.a;
}


void TextureViewer::Renderer::setDisplayHighlight(DisplayHighlight highlight)
{
  noHighlight = false;

  switch(highlight.value)
  {
  case DisplayHighlight::NONE:
    noHighlight = true;
    highlightParameter.r = 0.f;
    highlightParameter.g = 0.f;
    break;
  case DisplayHighlight::NOT_A_NUMBER:
    highlightParameter.r = 1.f;
    highlightParameter.g = 0.f;
    break;
  case DisplayHighlight::INFINITE_VALUE:
    highlightParameter.r = 2.f;
    highlightParameter.g = 0.f;
    break;
  case DisplayHighlight::LOWER_0:
    highlightParameter.r = 0.f;
    highlightParameter.g = 1.f;
    break;
  case DisplayHighlight::GREATER_1:
    highlightParameter.r = 0.f;
    highlightParameter.g = 2.f;
    break;
  case DisplayHighlight::ALL:
    highlightParameter.r = 3.f;
    highlightParameter.g = 3.f;
    break;
  case DisplayHighlight::OUTSIDE_RANGE:
    highlightParameter.r = 0.f;
    highlightParameter.g = 3.f;
    break;
  case DisplayHighlight::NUMERIC_ERRORS:
  default:
    highlightParameter.r = 3.f;
    highlightParameter.g = 0.f;
  }

  if(!noHighlight)
  {
    temporaryInvisible = false;
    timer.start(BLINK_TIME);
  }else
  {
    timer.stop();
  }
}


void TextureViewer::Renderer::_redrawImplementation()
{
  vec4 background(0.5f, 0.5f, 0.5f, 1.f);

  const Rectangle<vec2> viewport(vec2(0), view->viewportSize());
  const Rectangle<vec2> textureRectangle(view->transformFromModelToScreenSpace(view->model->boundingRect().rectangle()));
  const vec2 textureRectangleA(view->transformFromModelToScreenSpace(view->model->boundingRect().rectangle().min()));
  const vec2 textureRectangleB(view->transformFromModelToScreenSpace(view->model->boundingRect().rectangle().max()));

  this->mVertices.remove_all();

  // Tell the shader to draw the background
  this->zPosition = 1.f;
  // Draw the background
  if(this->grayBackground)
  {
    this->drawRectangle(viewport,
                        viewport,
                        encodeTileColors(background, background));
  }

  // Draw the checkerboard
  this->drawRectangle(textureRectangle,
                      textureRectangle - textureRectangle.min(),
                      encodeTileColors(lightTile, darkTile));

  // Tell the shader to draw the foreground
  this->zPosition = 0.f;
  this->drawRectangle(textureRectangleA,
                      textureRectangleB,
                      Rectangle<vec2>(vec2(0), vec2(1)),
                      displayParameter);

  // highlight bad pixels
  if(!noHighlight)
  {
    this->zPosition = temporaryInvisible ? -2.f : -1.f;
    this->drawRectangle(textureRectangleA,
                        textureRectangleB,
                        Rectangle<vec2>(vec2(0), vec2(1)),
                        highlightParameter);
  }
}

void TextureViewer::Renderer::makeDirty()
{
  GorillaHelper::ViewportRenderer::makeDirty();
}

vec4 TextureViewer::Renderer::encodeTileColors(const vec4& lightTile, const vec4& darkTile)
{
  return vec4(lightTile.r, darkTile.r, 0.f, lightTile.a);
}

void TextureViewer::Renderer::blink()
{
  temporaryInvisible = !temporaryInvisible;
  makeDirty();
}


} // namespace Framework
