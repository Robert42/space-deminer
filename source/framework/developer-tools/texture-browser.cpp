#include "texture-browser.h"

#include <framework/developer-tools/terminal.h>
#include <framework/developer-tools/tweak-ui/tweak-ui-layers.h>

#include <framework/developer-tools/developer-tools.h>
#include <framework/application.h>
#include <framework/window/render-window.h>
#include <framework/frame-signals.h>

namespace Framework {


TextureBrowser::TextureBrowser() :
  textureViewer(TextureViewer::Usage::TEXTURE_BROWSER),
  textureSize(0),
  gamma(1.f),
  fsaa(1),
  hasAlpha(false)
{
  textureViewer.signalTextureChanged().connect(std::bind(&TextureBrowser::showTextureInfo, this, _1)).track(this->trackable);
  Editor::Base::Editor2D::View* view = textureViewer.view().get();

  bar = TweakUI::Bar::create("Texture-Browser");
  TweakUI::Layers::textureBrowser->addBar(bar);

  bar->setHelp("The Texture-Browser allows you to browse through all textures currently used.\n"+Editor::Base::Editor2D::Controller::shortcutHelpText());

  allTextures = TweakUI::EnumeratedVariable::create("All2DTextures", "Texture", bar, "help='This dropdown menu allows you to choose which texture to show.' keyincr=pgdown keydecr=pgup");

  bar->addButton("Update List", std::bind(&TextureBrowser::updateList, this), "key=ctrl+r help='Updates the list of all avialable textures.'");
  bar->addSeperator();
  backgrounds = TweakUI::EnumeratedVariable::create("TextureViewerBackgrounds", "Background", bar, "help='This dropdown menu allows you to choose whoch background is shown.'");
  backgrounds->append("None");
  backgrounds->append("Dark Tiles");
  backgrounds->append("Default");
  backgrounds->append("Light Tiles");
  backgrounds->append("Black");
  backgrounds->append("Grey");
  backgrounds->append("White");
  bar->addVarWithIntuitiveCallbackAs<bool, bool, bool>("UseGrayBackground",
                                                       ::TweakUI::TW_TYPE_BOOLCPP,
                                                       std::bind(&TextureViewer::grayBackground, &textureViewer),
                                                       std::bind(&TextureViewer::setGrayBackground, &textureViewer, _1),
                                                       "help='If activated a gray background gets drawn behind the texture, to reduce distraction by the ingame content.'");
  displayMode = TweakUI::EnumeratedVariable::create("TextureViewerDisplayMode", "Display-Mode", bar, "help='The display mode allows to focus you more on certain aspects of the texture, for example certain channels.'");
  displayMode->append("Normal");
  displayMode->append("Ignore Alpha");
  displayMode->append("Red Channel");
  displayMode->append("Green Channel");
  displayMode->append("Blue Channel");
  displayMode->append("Alpha Channel");
  bar->addVarWithIntuitiveCallbackAs<real, real, real>("Black-Level",
                                                       ::TweakUI::TW_TYPE_FLOAT,
                                                       std::bind(&TextureViewer::displayRangeStart, &textureViewer),
                                                       std::bind(&TextureViewer::setDisplayRangeStart, &textureViewer, _1),
                                                       "help='The black level is the value being display as black' "
                                                       "step=0.001 "
                                                       "precision=3 ");
  bar->addVarWithIntuitiveCallbackAs<real, real, real>("White-Level",
                                                       ::TweakUI::TW_TYPE_FLOAT,
                                                       std::bind(&TextureViewer::displayRangeEnd, &textureViewer),
                                                       std::bind(&TextureViewer::setDisplayRangeEnd, &textureViewer, _1),
                                                       "help='The black level is the value being display as white' "
                                                       "step=0.001 "
                                                       "precision=3 ");
  displayFilter = TweakUI::EnumeratedVariable::create("TextureViewerDisplayFilter", "Display-Filter", bar, "help='The display filter allows to filter the texture when viewing. For example undo premultiplying of colors'");
  displayFilter->append("Normal");
  displayFilter->append("Seperate -> Premultiply");
  displayFilter->append("Premultiply-> Seperate");
  displayFilter->append("False Color");
  displayHighlight = TweakUI::EnumeratedVariable::create("TextureViewerDisplayHighlight", "Display-Highlight", bar, "help='The display highlight allows to highlight problematic pixels'");
  displayHighlight->append("None");
  displayHighlight->append("All");
  displayHighlight->append("<0");
  displayHighlight->append(">1");
  displayHighlight->append("<0 or >1");
  if(Ogre::Root::getSingleton().getRenderSystem()->getDriverVersion().major>=3)
  {
    displayHighlight->append("Nan");
    displayHighlight->append("Inf");
    displayHighlight->append("Numeric Errors");
  }
  bar->addVarWithIntuitiveCallbackAs<bool, bool, bool>("InvertY",
                                                       ::TweakUI::TW_TYPE_BOOLCPP,
                                                       std::bind(&TextureViewer::invertY, &textureViewer),
                                                       std::bind(&TextureViewer::setInvertY, &textureViewer, _1),
                                                       "help='Flip the image display vertically' ");
  bar->addSeperator();
  bar->addVar("format", const_cast<const std::string*>(&textureFormat), "help='The format of the current texture'");
  bar->addVar("hasAlpha", const_cast<const bool*>(&hasAlpha));
  bar->addVarAsVector("size", const_cast<const ivec2*>(&textureSize), "help='The size of the current texture.'", ivec2(0));
  bar->addVar("gamma", const_cast<const float*>(&gamma), "help='The gamma value applied on the texture.'");
  bar->addVar("fsaa", const_cast<const int*>(&fsaa));
  bar->addSeperator();
  bar->addButton("Close", std::bind(&TextureBrowser::quit, this), "key=ESCAPE help='Closes the Texture Browser.'");

  updateList();

  RenderWindow::signalWindowResized().connect(std::bind(&TextureBrowser::updateWindowSize, this)).track(this->trackable);

  allTextures->signalTextChanged().connect(std::bind(&TextureViewer::setTexture, &textureViewer, _1)).trackManually();
  allTextures->signalTextChanged().connect(std::bind(&Editor::Base::Editor2D::View::resetView, view, 1.f)).trackManually();
  allTextures->setCurrentIndex(0);
  textureViewer.setTexture(allTextures->currentText());

  backgrounds->signalIndexChanged().connect(std::bind(&TextureViewer::setCheckerboardByIndex, &textureViewer, _1)).trackManually();
  backgrounds->setCurrentIndex(textureViewer.checkerboard().value);

  displayMode->signalIndexChanged().connect(std::bind(&TextureViewer::setDisplayModeByIndex, &textureViewer, _1)).trackManually();
  displayMode->setCurrentIndex(textureViewer.displayMode().value);

  displayFilter->signalIndexChanged().connect(std::bind(&TextureViewer::setDisplayFilterByIndex, &textureViewer, _1)).trackManually();
  displayFilter->setCurrentIndex(textureViewer.displayFilter().value);

  displayHighlight->signalIndexChanged().connect(std::bind(&TextureViewer::setDisplayHighlightByIndex, &textureViewer, _1)).trackManually();
  displayHighlight->setCurrentIndex(textureViewer.displayHighlight().value);

  updateWindowSize();

  view->resetView();

  TweakUI::Layers::textureBrowser->show();
}


TextureBrowser::~TextureBrowser()
{
  TweakUI::Layers::textureBrowser->hide();
}


void TextureBrowser::updateList()
{
  std::string currentTexture = allTextures->currentText();

  std::set<Ogre::String> allNames;

  Ogre::TextureManager::ResourceMapIterator i = Ogre::TextureManager::getSingleton().getResourceIterator();
  while(i.hasMoreElements())
  {
    Ogre::String name = i.getNext()->getName();
    allNames.insert(name);
  }

  allTextures->clear();
  for(const Ogre::String& name : allNames)
    allTextures->append(name);
  allTextures->setCurrentText(currentTexture);
}


void TextureBrowser::quit()
{
  FrameSignals::callOnce(std::bind(&TextureBrowser::quitNow, this));
}


void TextureBrowser::quitNow()
{
  Application::developerTools().quitTextureBrowser();
}


void TextureBrowser::showTextureInfo(const Ogre::TexturePtr& tex)
{
  textureFormat = Ogre::PixelUtil::getFormatName(tex->getFormat());
  textureSize.x = tex->getWidth();
  textureSize.y = tex->getHeight();

  hasAlpha = tex->hasAlpha();

  if(tex->isHardwareGammaEnabled())
  {
    bar->setVarParam("gamma", "visible", "true");
    gamma = tex->getGamma();
  }else
  {
    bar->setVarParam("gamma", "visible", "false");
  }

  fsaa = tex->getFSAA();
  if(fsaa)
  {
    bar->setVarParam("fsaa", "visible", "true");
  }else
  {
    bar->setVarParam("fsaa", "visible", "false");
  }
}

void TextureBrowser::updateWindowSize()
{
  textureViewer.view()->setViewportSize(RenderWindow::size());
}


} // namespace Framework
