#ifndef FRAMEWORK_TESTSPATIALGUI_H
#define FRAMEWORK_TESTSPATIALGUI_H

#include <dependencies.h>

#include <framework/developer-tools/gui-system/ingame/spatial/window-manager.h>
#include <framework/developer-tools/gui-system/window.h>
#include <framework/developer-tools/gui-system/layout.h>

namespace Framework {

class TestSpatialGui final : public noncopyable
{
public:
  Signals::Trackable trackable;

private:
  Gui::Ingame::Spatial::WindowManager windowManager;
  Gui::Window::Ptr testWindow;
  Coordinate cameraCoordinate;

public:
  TestSpatialGui();
  ~TestSpatialGui();

public:
  bool updateCameraCoordinate();

public:
  static Gui::Widget::Ptr createTestGui();
};

} // namespace Framework

#endif // FRAMEWORK_TESTSPATIALGUI_H
