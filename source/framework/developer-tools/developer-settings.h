#ifndef _FRAMEWORK_DEVELOPER_SETTINGS_H_
#define _FRAMEWORK_DEVELOPER_SETTINGS_H_

#include <dependencies.h>

#include <base/io/directory.h>
#include <base/io/regular-file.h>

namespace Framework {
using namespace Base;


class DeveloperSettings
{
public:
  class Input
  {
  public:
    bool grabMouse;
    bool hideMouseCursor;
    bool grabKeyboard;
    bool grabJoystick;
    bool alwaysShowIngameMouseCursor;

  public:
    Input();
  };

  class Graphic
  {
  public:
    bool allowFullscreen;

  public:
    Graphic();
  };

  class Terminal
  {
  public:
    bool enable;
    bool alwaysUseTerminalMouseMode;

    Terminal();
  };

  class Samples
  {
  public:
    bool loadSample;
    Ogre::String sampleToLoad;

    Samples();
  };

public:
  Input input;
  Graphic graphic;
  Terminal terminal;
  Samples samples;

public:
  DeveloperSettings(const IO::Directory& configDirectory);

private:
  void loadFromFile(const IO::RegularFile& developerSettingsFile);
  void handleSetting(const Ogre::String& groupName, const Ogre::String& setting, const Ogre::String& value);
};


}

#endif
