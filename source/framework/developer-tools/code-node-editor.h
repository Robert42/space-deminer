#ifndef FRAMEWORK_CODENODEEDITOR_H
#define FRAMEWORK_CODENODEEDITOR_H

#include <framework/developer-tools/developer-hud.h>
#include <framework/developer-tools/editor/base/object-editor-2d/renderer.h>
#include <framework/developer-tools/editor/base/object-editor-2d/controller.h>
#include <framework/developer-tools/editor/gorilla/object-editor-2d/blender-style-tool-renderer.h>
#include <framework/developer-tools/editor/blender-style-input/object-editor-2d/input-handler.h>

namespace Framework {

class CodeNodeEditor : public DeveloperHud::Layer
{
private:
  class NodeModel;
  class Renderer;

  std::shared_ptr<NodeModel> model;
  std::shared_ptr<Editor::BlenderStyleInput::ObjectEditor2D::InputHandler> inputHandler;
  std::shared_ptr<Editor::Base::ObjectEditor2D::Controller> controller;
  std::shared_ptr<Editor::Base::ObjectEditor2D::Selection> selection;
  std::shared_ptr<Editor::Gorilla::ObjectEditor2D::BlenderStyleToolRenderer> blenderStyleToolRenderer;
  Renderer* renderer;

public:
  CodeNodeEditor();
  ~CodeNodeEditor();

  std::shared_ptr<Editor::Base::ObjectEditor2D::View> view();
};

} // namespace Framework

#endif // FRAMEWORK_CODENODEEDITOR_H
