#include "system-font.h"

namespace Framework {

IO::File fcMatchCommand(QStringList arguments)
{
  QString program = "fc-match";
  arguments.prepend("--format=%{file}");

  QProcess process;
  process.start(program, arguments, QIODevice::ReadOnly);
  if(!process.waitForFinished(5000))
  {
    throw std::runtime_error("Couldn't get the linux fonts.\nfc-match did not return within 5 seconds.");
  }

  if(process.exitCode() == QProcess::CrashExit)
  {
    throw std::runtime_error("Couldn't get the linux fonts\nfc-match crashed");
  }

  QByteArray byteArray = process.readAllStandardOutput();
  QTextCodec* utf8Codec = QTextCodec::codecForUtfText(byteArray, QTextCodec::codecForName("UTF-8"));
  QString standartOutput = utf8Codec->toUnicode(byteArray);

  return IO::File(String::fromQString(standartOutput).toPath());
}

IO::File SystemFont::findFontFile(bool bold, bool monospace)
{
  if(monospace)
  {
    if(bold)
      return fcMatchCommand(QStringList({"monospace:weight=bold"}));
    else
      return fcMatchCommand(QStringList({"monospace"}));
  }else
  {
    if(bold)
      return fcMatchCommand(QStringList({"bold"}));
    else
      return fcMatchCommand(QStringList());
  }
}


} // namespace Framework

