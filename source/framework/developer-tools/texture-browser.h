#ifndef FRAMEWORK_TEXTUREBROWSER_H
#define FRAMEWORK_TEXTUREBROWSER_H

#include "texture-viewer.h"
#include "tweak-ui/enumerated-variable.h"

namespace Framework {


class TextureBrowser final : public noncopyable
{
public:
  Signals::Trackable trackable;

private:
  Framework::TweakUI::Bar::Ptr bar;
  Framework::TweakUI::EnumeratedVariable::Ptr allTextures, backgrounds, displayMode, displayFilter, displayHighlight;

  TextureViewer textureViewer;

  ivec2 textureSize;
  std::string textureFormat;
  float gamma;
  int fsaa;
  bool hasAlpha;

public:
  TextureBrowser();
  ~TextureBrowser();

public:
  void quit();
  void updateList();

private:
  void quitNow();
  void updateWindowSize();

  void showTextureInfo(const Ogre::TexturePtr& tex);
};


} // namespace Framework

#endif // FRAMEWORK_TEXTUREBROWSER_H
