#include <framework/extern-gui.h>
#include <base/io/special-directories.h>
#include <base/strings/string.h>
#include <base/strings/application-argument-faker.h>
#include <base/threads.h>

#include <QApplication>
#include <QSplashScreen>
#include <QPixmap>
#include <QMessageBox>


namespace Framework {

class ExternQtGui : public ExternGui
{
public:
  unique_ptr<QApplication> qApplication;
  unique_ptr<QSplashScreen> qSplashScreen;

  ApplicationArgumentFaker applicationArguments;

  ExternQtGui()
    : applicationArguments({})
  {
    qApplication.reset(new QApplication(*applicationArguments.argc(),
                                        applicationArguments.argv()));
  }

  void showSplashscreenImpl(const IO::RegularFile& imageFile) override
  {
    if(!imageFile.exists())
      return;


    QPixmap pixmap;
    pixmap.load(imageFile.pathAsQString());

    qSplashScreen.reset(new QSplashScreen(pixmap));
    qSplashScreen->show();

    handleEvents();
  }

  void hideSplashscreenImpl() override
  {
    qSplashScreen.reset();
  }

  void fatalErrorDialogImpl(const String& message, const String& informativeText, const String& details) override
  {
    hideSplashscreenImpl();

    QMessageBox messageBox;
    messageBox.setWindowTitle("Fatal Error");
    messageBox.setText(message.toQString());
    if(!informativeText.empty())
      messageBox.setInformativeText(informativeText.toQString());
    if(!details.empty())
      messageBox.setDetailedText(details.toQString());
    messageBox.setIcon(QMessageBox::Critical);
    messageBox.exec();
  }

private:
  void handleEvents(int nMiliseconds = 100)
  {
    for(int i=0; i<nMiliseconds; ++i)
    {
      qApp->processEvents();

      Base::sleepFor(std::chrono::milliseconds(1));
    }
  }
};


ExternGui::Ptr ExternGui::create()
{
  return Ptr(new ExternQtGui());
}


void ExternGui::showSplashscreen(const IO::RegularFile& imageFile)
{
  singleton().showSplashscreenImpl(imageFile);
}


void ExternGui::hideSplashscreen()
{
  singleton().hideSplashscreenImpl();
}


void ExternGui::fatalErrorDialog(const String& message, const String& informativeText, const String& details)
{
  singleton().fatalErrorDialogImpl(message, informativeText, details);
}


} // namespace Framework
