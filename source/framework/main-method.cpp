#include <dependencies.h>
#include <base/io/fatal-errorlog.h>
#include <base/runtime-assertion.h>
#include <framework/extern-gui.h>

int mainMethod();
int tryRunningMainMethod();

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32

// Source:
// http://msdn.microsoft.com/en-us/library/windows/desktop/ff381406%28v=vs.85%29.aspx
// http://msdn.microsoft.com/en-us/library/windows/desktop/aa383745%28v=vs.85%29.aspx

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
  return tryRunningMainMethod();
}

#else

int main(int, char**)
{
  return tryRunningMainMethod();
}

#endif

using Base::IO::writeFatalErrorLog;
using Framework::ExternGui;

int tryRunningMainMethod()
{
#if OGRE_PLATFORM == OGRE_PLATFORM_LINUX
  setenv("LC_NUMERIC", "C", 1);
#endif

  int returnCode = EXIT_FAILURE;

  ExternGui::Ptr externGui = ExternGui::create();

  try
  {
    returnCode = mainMethod();

    Base::RuntimeAssertion::throwExceptionIfDeveloperWarningWasRaised();
  }catch(const Ogre::Exception& exception)
  {
    externGui->fatalErrorDialog("A fatal error occured!",
                                Base::String::fromOgreString(exception.getDescription()),
                                Base::String::fromOgreString(exception.getFullDescription()));

    writeFatalErrorLog("*** UNCATCHED EXCEPTION (Ogre::exception) ***\n" +
                       exception.getFullDescription(),
                       false);
  }catch(const boost::exception& exception)
  {
    externGui->fatalErrorDialog("A fatal error occured!",
                                "Fatal Error: Uncatched boost::exception",
                                Base::String::fromUtf8(boost::diagnostic_information(exception)));

    writeFatalErrorLog("*** UNCATCHED EXCEPTION (boost::exception) ***\n" +
                       boost::diagnostic_information(exception));
  }catch(const std::exception& exception)
  {
    externGui->fatalErrorDialog("A fatal error occured!",
                                exception.what(),
                                "");

    writeFatalErrorLog("*** UNCATCHED EXCEPTION (std::exception) ***\n" +
                       std::string(exception.what()));
  }

  return returnCode;
}
