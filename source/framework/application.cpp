#include <dependencies.h>
#include <framework/advanced-graphics/samples/sample-manager.h>
#include <framework/application.h>
#include <framework/window/render-window.h>
#include <framework/plugin-manager.h>
#include <framework/developer-tools/developer-settings.h>
#include <framework/developer-tools/developer-hud.h>
#include <framework/developer-tools/developer-tools.h>
#include <framework/developer-tools/terminal.h>
#include <framework/developer-tools/terminal/terminal-manager.h>
#include <framework/developer-tools/gui-system/types.h>
#include <framework/developer-tools/gui-system/tags.h>
#include <framework/developer-tools/gui-system/animation-slot-ids.h>
#include <framework/developer-tools/gui-system/animations/ingame-time-counter.h>
#include <framework/extern-gui.h>
#include <framework/frame-timer.h>
#include <framework/scripting/initialization.h>
#include <framework/window/user-input.h>
#include <base/io/log.h>
#include <base/io/special-directories.h>

namespace Framework {


using Base::IO::Log;


IO::Directory searchAssetsDirectory(const String& applicationName);
bool isAssetsDirectory(const IO::Directory& directoryCandidate);


Application::Application(const String& applicationName)
  : BaseApplication(applicationName)
{
  terminalManager = Terminal::Manager::create();

  this->_assetsDirectory = searchAssetsDirectory(applicationName);
  Log::log("Found assets-directory: %0", this->assetsDirectory());

  this->_developerSettings = shared_ptr<DeveloperSettings>(new DeveloperSettings(this->configDirectory()));
}


Application::~Application()
{
  this->trackable.clear();

  this->_currentApplet.reset();

  this->_developerTools.reset();
  this->_developerHud.reset();
  this->_ceguiSystem.reset();
  this->frameTimer.reset();
  this->_renderWindow.reset();

  this->terminalManager.reset();
}


void Application::run()
{
  ExternGui::showSplashscreen(splashScreenFile());

  this->_renderWindow = RenderWindow::create(windowTitle());

  this->renderWindow().run(*this);
}

void Application::startup()
{
  BaseApplication::startup();

  Gui::registerTypes();
  Gui::initTags();
  Gui::initAnimationSlots();

  Framework::Scripting::initializeScripting();

  this->frameTimer = FrameTimer::create();

  this->loadPlugins();

  this->loadEngineConfiguration();

  this->renderWindow().createOgreRenderWindow();

  Keyboard::signalKeyPressed().connect(InputDevice::ActionPriority::FALLBACK,
                                       std::bind(&Application::handleKey, this, _1)).track(this->trackable);

  this->initializeResourceGroups();

  Ogre::Root::getSingleton().addFrameListener(&this->frameSignals);

  _ceguiSystem = std::shared_ptr<CeguiSystem>(new CeguiSystem);

  this->frameSignals.signalRenderingQueued().connect(std::bind(&Application::quitIfWindowClosed, this)).track(this->trackable);

  this->_frametimeAnimation = Gui::Animations::IngameTimeCounter::create();

  this->_developerHud = DeveloperHud::create();
  this->_developerTools = std::shared_ptr<DeveloperTools>(new DeveloperTools);

  developerHud().registerTerminalCommands();

  if(developerSettings().samples.loadSample)
    startSampleBrowser();
  else
    startMainApplet();

  this->frameTimer->reset();

  // The following line has to remain being the last think, the startup function does.
  ExternGui::hideSplashscreen();

#ifdef SPACEDEMINER_DEVELOPER
  executeDeveloperStartupScript();
#endif
}

void Application::initializeResourceGroups()
{
  initializeResourceLocations();
  Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}

void Application::renderOneFrame()
{
  Ogre::Root::getSingleton().renderOneFrame(frameTimer->averageTime());

  frameTimer->update();
}

const Gui::Animation::Ptr& Application::frametimeAnimation()
{
  return singleton()._frametimeAnimation;
}

void Application::quitIfWindowClosed()
{
  if(RenderWindow::isClosed())
    quit();
}


DeveloperHud& Application::developerHud()
{
  return *singleton()._developerHud;
}


DeveloperTools& Application::developerTools()
{
  return *singleton()._developerTools;
}


const IO::Directory& Application::assetsDirectory()
{
  return singleton()._assetsDirectory;
}


const Applet::Ptr& Application::currentApplet()
{
  return singleton()._currentApplet;
}


void Application::setCurrentApplet(const Applet::Ptr& applet)
{
  singleton()._currentApplet = applet;
}


RenderWindow& Application::renderWindow()
{
  return *singleton()._renderWindow;
}


const DeveloperSettings& Application::developerSettings()
{
  return *singleton()._developerSettings;
}


void Application::loadEngineConfiguration()
{
  Ogre::Root& ogreRoot = Ogre::Root::getSingleton();

  if(ogreRoot.restoreConfig())
    return;

  ExternGui::hideSplashscreen();
  if(ogreRoot.showConfigDialog())
  {
    ExternGui::showSplashscreen(splashScreenFile());
    return;
  }

  throw new Ogre::InternalErrorException(0,
                                         "setting up the graphic configuration failed.",
                                         "Application::run()",
                                         __FILE__,
                                         __LINE__);
}


bool Application::handleKey(KeyCode key)
{
  if(key == KeyCode::ESCAPE)
  {
    quit();
    return true;
  }

  return false;
}


void Application::quit()
{
  renderWindow().close();
}


void Application::openSampleBrowser(const std::string& sampleName)
{
  setCurrentApplet(Applet::Ptr());
  AdvancedGraphics::Samples::SampleManager::loadSample(sampleName);
}


void Application::openSampleBrowser()
{
  openSampleBrowser(developerSettings().samples.sampleToLoad);
}


void Application::startSampleBrowser()
{
  openSampleBrowser();
}


void Application::startMainApplet()
{
  setCurrentApplet(Applet::Ptr());
  setCurrentApplet(singleton().createMainApplet());
}


Application* Application::singletonPtr()
{
  return PublicSingleton<Application>::singletonPtr();
}

Application& Application::singleton()
{
  return PublicSingleton<Application>::singleton();
}

void Application::executeDeveloperStartupScript()
{
/*
// An example for a startup script executing the tests and hiding the terminal afterwards
u
Terminal::visible=false;
*/
/*
// An example for a startup script executing the tests and quitting the application afterwards
u
exit();
*/
#ifdef SPACEDEMINER_DEVELOPER
  if(!this->developerSettings().terminal.enable)
    return;

  IO::File startupScript = this->configDirectory() | "dev-startup-script.as";

  if(!startupScript.exists())
    return;

  QFile file(startupScript.pathAsQString());

  if(!file.open(QFile::ReadOnly | QFile::Text))
    return;

  QTextStream textStream(&file);

  if(textStream.atEnd())
    return;

  Keyboard::injectKeyDown(KeyCode::OPENTERMINAL);
  Keyboard::injectKeyUp(KeyCode::OPENTERMINAL);

  while(!textStream.atEnd())
  {
    QString line = textStream.readLine();

    // Don't waste time executing empty of commented out lines
    if(line.trimmed().length() == 0)
      continue;
    if(line.trimmed().startsWith("//"))
      continue;

    Keyboard::injectText(String::fromQString(line));
    Keyboard::injectKeyDown(KeyCode::RETURN);
    Keyboard::injectKeyUp(KeyCode::RETURN);
  }

#endif
}


IO::Directory searchAssetsDirectory(const String& applicationName)
{
  IO::Directory prefix = IO::SpecialDirectories::binaryPrefix();

  IO::Directory candidate = prefix / "share" / applicationName / "assets";
  if(isAssetsDirectory(candidate))
    return candidate;

  candidate = prefix / "assets";
  if(isAssetsDirectory(candidate))
    return candidate;

  throw Ogre::FileNotFoundException(0,
                                    "Couldn't find the assets directory.",
                                    "searchAssetsDirectory",
                                    __FILE__,
                                    __LINE__);
}


bool isAssetsDirectory(const IO::Directory& directoryCandidate)
{
  if(!directoryCandidate.exists())
    return false;

  if(!(directoryCandidate / "config" | "resource-locations.cfg").exists())
    return false;

  return true;
}


}
