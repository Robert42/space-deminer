#ifndef _FRAMEWORK_PRIVATERENDERINGLOOP_H_
#define _FRAMEWORK_PRIVATERENDERINGLOOP_H_

#include <framework/frame-signals.h>

namespace Framework {
using namespace Base;

class Application;

class PrivateRenderLoop final : public noncopyable
{
private:
  FrameSignals::Blocker frameSignalBlocker;
  Ogre::Timer timer;

  Signals::CallableSignal<void()> _signalRenderOneFrame;

public:
  const unsigned long milliSecondsToWait;

public:
  PrivateRenderLoop(float frameRate);
  PrivateRenderLoop();

public:
  void update();

  Signals::Signal<void()>& signalRenderOneFrame();
};


}

#endif
