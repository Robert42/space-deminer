# Space-Deminer

A small 2D space shooter with RPG elements. Your task ist to eliminate intelligent Mines created in the past galactic war.

This game is beeing developed and not playable until now.

## Acknowledgement

### Integrated Source Code Snippets
The source files

- `/source/src/framework/private/external/*`
- `/source/src/framework/private/render-window-impl/ogre-ois/extern/*`
- `/source/src/framework/window/implementation/ogre-ois/renderloop-macintosh-impl.mm`
- `/source/cmake-modules/MacOSXBundleTemplates/MacOSXBundleInfo.plist.in`

are mostly taken from other open source projects.

- `/source/src/framework/private/external/Gorilla` [Grorilla](http://www.ogre3d.org/tikiwiki/tiki-index.php?page=Gorilla), a very simple HUD-GUI-System.
- `/source/src/framework/private/external/gtest-1.6.0` [Google Test Framework](http://code.google.com/p/googletest/)

### Integrated Third Party Source Code

- We are using the googletest-framework. For easy usage, we have integrated the whole googletest source into the directory ./source/external/gtest-1.6.0

- For der development HUD, we are using the dejavu font texture-atlas taken from the [Gorilla](https://github.com/betajaen/gorilla) source. See the [license](https://github.com/betajaen/gorilla/blob/d164e9e76decbc397247476471f19b18282cd725/dejavu.licence) of the font.
- For the Terminal, we are using the FreeMono Font (see the fonts directory in the [art Reprository](https://bitbucket.org/Robert42/space-deminer-art/src/6fd5ece44a9784f33ebc62286788ac8d19632323/font/freefont-otf?at=develop)) is used. The font was converted into a texture atlas originally written by [the BFG](http://www.ogre3d.org/tikiwiki/tiki-index.php?page=Gorilla#Utility) written by Calder (Author of QuickGUI).
- For Unicode conversions and 2D-Vectors, we are using header-files from [SFML](www.sfml-dev.org). Please note that we are only using a very small subset of the sfml librtary files. Please check the [sfml website](www.sfml-dev.org) for more information.
- For scripting, we are using [Anglescript](http://www.angelcode.com/angelscript/)
- The function pointer based state machine in /source/base/state-machines/function-pointer-based is mostly taken from the book Game Programming Gems 3: Section 3: Article: "Function Pointer-Based, Embedded Finite-State Machines" From Charles Farris, VR1 Entertainment, Inc.  
It has been adapted to our conventions.
- for testing, [CEGUI](http://cegui.org.uk/) xml-schemas are included to /development/cegui/xml-schemas 

### Integrated CMake Source Code
- The CMake file /source/cmake-modules/search-boost.cmake is mostly based on the CMakeLists.txt you can find in the Ogre3D Wiki. See search-boost.cmake for more information.
- The CMake file /source/cmake-modules/PCHSupport.cmake is mostly taken from the [github site of Lars Christensen](https://gist.github.com/larsch/573926#file-precompiledheader-cmake). See the cmake file for more information.

- All cmake files in the directory /source/cmake-modules/find are taken from the ogre project, with the following exceptions:
    - FindSDL2.cmake - it is taken from https://github.com/dolphin-emu/dolphin/blob/master/CMakeTests/FindSDL2.cmake (see commit https://github.com/dolphin-emu/dolphin/commit/80c15f21b4556394330918d32e30b6c1bbbce42e#diff-5315a84c3f94a61f2c4a5837860ce128)

- The python script /source/cmake-modules/python/choose-build-mode.py is using a Codesnippet taken from [Stackoverflow](http://stackoverflow.com/a/11621141/2301866)


## License

### The Sourceode and the compiled Software

Copyright (C) 2013  Robert Hildebrandt

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

### The Assets

All assets are stored in the data directory and are licensed Creative Common License (CC-BY). See the readme file of the data directory for more information and for exceptions.

The assets are licensed under the Creative Commons Attribution 3.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
