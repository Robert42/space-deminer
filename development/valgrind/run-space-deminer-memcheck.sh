#!/bin/bash

valgrind \
--gen-suppressions=all \
--track-origins=yes --leak-check=full --show-reachable=yes \
--demangle=yes \
--log-file=$HOME/.space-deminer/valgrind-memcheck.log --suppressions=./memcheck-suppressions.supp \
../../bin/space-deminer
