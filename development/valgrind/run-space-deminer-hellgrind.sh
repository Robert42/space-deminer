#!/bin/bash

valgrind \
--gen-suppressions=all \
--tool=helgrind \
--demangle=yes \
--log-file=$HOME/.space-deminer/valgrind-hellgrind.log --suppressions=./hellgrind-suppressions.supp \
../../bin/space-deminer
